<script type="text/javascript">
	$(document).ready(function () {
		CH.setDepositRange(parseInt('{setting("ticket_range", "controller", "coinflip")}'));
		CH.setMaxDepositItems(parseInt('{setting("max_deposit_items", "controller", "coinflip")}'));
	});
</script>

<script>
	$("title").html("{$title}");
</script>

<div class="row m-4" id="coinflips-container">
    <div class="col-12 m-auto">
        <table id="coinflip-stats-table" class="mb-5 color-white">
            <tr>
                <td>
                    <div class="row color-white border-right">
                        <div class="coinflip-stats">
                            <img data-side="1" class="side selected side-1" src="/Template/Static/Image/money.png">
                            <span class="white"><span id="coinflip-stats-totalvalue" class="stats-value m-0">0.00</span><br> TOTAL VALUE</span>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="row color-white border-right">
                        <div class="coinflip-stats">
                            <img data-side="1" class="side selected side-1" src="/Template/Static/Image/ak.png">
                            <span class="white"><span id="coinflip-stats-totalitems" class="stats-value m-0">0</span><br> TOTAL ITEMS</span>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="row color-white border-right">
                        <div class="coinflip-stats">
                            <img data-side="1" class="side selected side-1" src="/Template/Static/Image/join.png">
                            <span class="white"><span id="coinflip-stats-count" class="stats-value m-0">0</span> <br> JOINABLE GAMES</span>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="row color-white">
                        <div class="coinflip-stats button-yellow-gradient" {if is_login()} style="width:290px;" {/if}>
                            {if isVerified()}
                                <a href="javascript:CH.openDepositCreateWindow();" class="button-wave" id="coinflip-deposit-button">Create a coinflip</a>
                            {elseif is_login()}
                                <a href="#trade-url-modal" class="button-wave" data-toggle="modal" id="coinflip-deposit-button">Set Trade URL to deposit</a>
                            {else}
                                <a href="/login" data-ignore="1" class="button-wave" id="coinflip-deposit-button">Login to deposit</a>
                            {/if}
                        </div>
                    </div>
                </td>
            </tr>
        </table>

        <table id="coinflips-list" class="mt-4 mb-4">
            <tbody id="coinflips">

            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="coinflip-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <div id="coinflip-animated-winner-coin" style="display:none;"><img src="/Template/Static/Image/transparent.png"></div>
                <table>
                    <colgroup>
                        <col span="3" style="width: 40%">
                        <col span="1" style="width: 20%">
                        <col span="3" style="width: 40%">
                    </colgroup>
                    <tr>
                        <td colspan="2">
                            <div class="player p1">
                                <img src="https://placehold.it/80x80" class="avatar">
                                <div class="coin"></div>
                            </div>
                        </td>
                        <td colspan="3">
                            <div id="coinflip-coin"></div>
                        </td>
                        <td colspan="2">
                            <div class="player p2">
                                <img src="https://placehold.it/80x80" class="avatar">
                                <div class="coin"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="coinflip-player-1-name"><h2></h2></td>
                        <td colspan="1"></td>
                        <td colspan="3" class="coinflip-player-2-name"><h2></h2></td>
                    </tr>
                    <tr>
                        <td class="player1-value"></td>
                        <td></td>
                        <td class="player1-chance">50%</td>
                        <td colspan="1"></td>
                        <td class="player2-value"></td>
                        <td></td>
                        <td class="player2-chance">50%</td>
                    </tr>
                </table>
                <table id="coinflip-modal-item-table">
                    <tr class="item">
                        <td><img src="https://placehold.it/300x300"></td>
                        <td>Placehold</td>
                        <td>0</td>
                        <td></td>
                        <td><img src="https://placehold.it/300x300"></td>
                        <td>Placehold</td>
                        <td>0</td>
                    </tr>
                </table>
                <div class="coinflip-info"></div>
            </div>
        </div>
    </div>
</div>