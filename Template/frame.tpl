<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CSGOFrog.com Try your luck!</title>
    <link rel="stylesheet" href="/Template/Static/CSS/bootstrap.min.css">
    <link rel="stylesheet" href="/Template/Static/CSS/style.css?v=17">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
</head>
<body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/Template/Static/JScript/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js"></script>
<script src="/Template/Static/JScript/jquery.slimscroll.min.js"></script>
<script src="/Template/Static/JScript/jcanvas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js" type="text/javascript"></script>

<script type="text/javascript">
    var g_sessionid = "{token()}";
    var g_serverauth = "{getServerAuth()}";
    var g_userid = parseInt('{user("id")}');
    var g_steamid = '{user("steamID")}';
    var g_lang = {json_encode(language("steamweb", "client_lang"))};
    var g_serverip = '{setting("serverip", "component", "SteamWeb")}';
    var g_sitename = '{setting("name", "application", "general")}';
    var timefix = Math.round(Date.now() / 1000) - {time()};
</script>

<script src="/Template/Static/JScript/basic.js?v=4"></script>
<script src="/Template/Static/JScript/custom.js?v=51"></script>

<script type="text/javascript">
    $(document).ready(function () {
        {foreach $message->getError() as $msg}
            showError("{$msg}");
        {/foreach}
        {foreach $message->getInfo() as $msg}
            showInfo("{$msg}");
        {/foreach}
        {if is_login()}
            {if user("tradetoken") == ""}
                var $disclaimer = $('.disclaimer');
                $disclaimer[0].scrollIntoView();
                $disclaimer.slideDown(500);
            {/if}
        {/if}
    });
</script>

<div class="content">
    <nav class="navbar navbar-expand-lg navbar-dark" style="position: relative;z-index: 10;">
        <a class="navbar-brand" href="/"><img src="/Template/Static/Image/logo.png"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item {if $path.0 == ""}active{/if}">
                    <a class="nav-link" href="/">High Rollers</a>
                </li>
                <!--<li class="nav-item {if $path.0 == "small"}active{/if}">
                    <a class="nav-link" href="/small">Small Jackpot</a>
                </li>-->
                <li class="nav-item {if $path.0 == "coinflip"}active{/if}">
                    <a class="nav-link" href="/coinflip">Coinflip</a>
                </li>
                <li class="nav-item {if $path.0 == "upgrade"}active{/if}">
                    <a class="nav-link" href="/upgrade">Upgrade</a>
                </li>
                <li class="nav-item {if $path.0 == "top"}active{/if}">
                    <a class="nav-link" href="/top">Leaderboard</a>
                </li>
                <li class="nav-item {if $path.1 == "history"}active{/if}">
                    <a class="nav-link" href="/stats/history">Game history</a>
                </li>
                <li class="nav-item {if $path.0 == "faq"}active{/if}">
                    <a class="nav-link" href="/faq">FAQ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://twitter.com/CSGOFROGJACKPOT" target="_blank">Twitter</a>
                </li>
            </ul>
            <div class="my-2 my-lg-0 user">
                {if is_login()}
                    <img src="{getSteamSessionUser("avatarmedium")}" class="avatar">
                    <span class="profile">
                                    {getSteamSessionUser("personaname")}<br><br>
                                    <small><a href="/account">ACCOUNT</a></small>&nbsp;&nbsp;
                                    <small><a href="/stats/user/{getSteamSessionUser("steamid")}">PROFILE</a></small>
                                </span>
                    <a href="/logout" data-ignore="1" class="logout mt-2">
                        <img src="/Template/Static/Image/logout.png">
                    </a>
                {else}
                    <div class="steam-login">
                        <a href="/login" data-ignore="1"><img src="/Template/Static/Image/steam.png"></a>
                    </div>
                {/if}
            </div>
        </div>
    </nav>

    <div class="row mr-0 {if $path.0 != "upgrade"}hidden{/if}" id="activity-stream" style="position: relative;z-index: 10;">
        <div class="col"></div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div id="chat-header">
                <div class="col" id="chat-header-online">
                    <img src="/Template/Static/Image/online_dot.png">
                    <span id="online-count">0</span> online
                </div>
            </div>
            <div class="col-9 p-0">
                <div id="main">
                    <div id="main-jackpot" style="{if $path.0 != ""}display:none;{/if}">
                        {includetemplate="Main/Main.tpl"}
                    </div>
                    <!--<div id="main-jackpot-small" style="{if $path.0 != "small"}display:none;{/if}">
                        {includetemplate="Small/Main.tpl"}
                    </div>-->
                    <div id="main-coinflip" style="{if $path.0 != "coinflip"}display:none;{/if}">
                        {includetemplate="Coinflip/Main.tpl"}
                    </div>
                    <div id="main-upgrade" style="{if $path.0 != "upgrade"}display:none;{/if}">
                        {includetemplate="Upgrade/Main.tpl"}
                    </div>
                    <div id="main-text" style="{if $path.0 == "" || $path.0 == "coinflip"}display:none;{/if}">
                        {if $path.0 != "" && $path.0 != "small" && $path.0 != "coinflip" && $path.0 != "upgrade"}
                            {$main}
                        {/if}
                    </div>
                </div>
            </div>
            <div class="col-3" id="chat-container">
                <div class="row d-block mr-0">
                    <div class="col-10 float-right p-0">
                        <div id="sample-message" class="message my-2" style="display: none;">
                            <div class="avatar-parent">
                                <img class="avatar" src="">
                                <span class="level-white"></span>
                                <span class="chat-admin"></span>
                            </div>
                            <div class="content p-2">
                                <span class="msg-content"></span>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div id="chat-block" class="row" style="margin-top:26px;">
                            <div class="col pr-3" id="messages">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3" id="form-send-message">
                <div class="row d-block mr-0">
                    <div class="col-10 float-right p-0">
                        {if isVerified()}
                            <input id="input-message" placeholder="Enter Message" />
                            <img id="send-message-image" class="input-send-msg" src="/Template/Static/Image/send_msg.png" />
                        {elseif is_login()}
                            <input id="input-message" placeholder="Enter Tradeurl First!" readonly />
                        {else}
                            <input id="input-message" placeholder="Login to write a message" readonly />
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="deposit-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div>
                    <div class="row info-selected">
                        <div class="left-items">
                            <div class="items-value">
                                Selected : $ <span class="white value">$0.00</span>
                            </div>
                            <div class="items-value">
                                Items Selected : <span class="white"><span class="quantity1">0</span>/<span class="quantity2"></span></span>
                            </div>
                        </div>
                        <div class="choose-side">
                            <img data-side="1" class="side selected side-1" src="/Template/Static/Image/coin_t.png">
                            VS
                            <img data-side="2" class="side side-2" src="/Template/Static/Image/coin_ct.png">
                        </div>
                        <div class="right-items">
                            <a href="#" class="disabled" id="confirm-deposit" onclick="TM.sendDepositTrade()">Confirm deposit</a>
                            <div class="items-value">
                                Inventory Total : $ <span class="white total">$0.00</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body items">
                <div class="items-container"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="deposit-modal-success">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="col-12 text-center" style="color: rgb(34, 190, 94);">
                        <h2>Success!</h2>
                    </div>
                </div>
            </div>
            <div class="modal-body items">
                <div class="float-left">
                    <i class="far fa-check-circle fa-7x" style="color: rgb(34, 190, 94);"></i>
                </div>
                <div class="text-center">
                    Trade offer sent successfully!<br>
                    <a href="https://trade.opskins.com/trade-offers" target="_blank" id="trade-offer-link">Accept it by clicking here!</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="winner-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row w-100">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="col-12 text-center" style="color: rgb(34, 190, 94);">
                        <h2 class="text-green-gradient">Congratulations!</h2>
                    </div>
                </div>
            </div>
            <div class="modal-body items">
                <div class="text-center">
                    You won a <span id="winner-modal-type">pot</span> valued <span id="winner-modal-value">0.00</span> <i class="fa fa-database"></i> with a chance of <span id="winner-modal-chance">0</span>%! We sent you a trade offer with your items!<br>
                    <a href="https://trade.opskins.com/trade-offers" target="_blank" id="#withdraw-link">Accept it by clicking here!</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="trade-url-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <div class="col-12 text-center">
                    Set-up your trade URL
                </div>
                <div class="col-12">
                    Please enter your <a href="https://trade.opskins.com/settings" target="_blank">OPSkins ExpressTrade Trade URL</a> below
                </div>
                <div class="col-12">
                    <input type="text" class="input-control" id="opskins_url" value="{if user("tradetoken") != ""}https://trade.opskins.com/t/0000/{user("tradetoken")}{/if}">
                    <a href="#" id="update-trade-url" onclick="TM.updateTradeUrl();">Update</a>
                </div>
            </div>
        </div>
    </div>
</div>

{noparse}
    <script>
        (function (i, s, o, g, r, a, m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)}, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-84815155-3', 'auto');
        ga('send', 'pageview');

    </script>
{/noparse}
</body>
</html>