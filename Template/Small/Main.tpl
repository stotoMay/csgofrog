<script type="text/javascript">
    function selectJackpotSetting(setting) {
        $(".jackpot-tab").removeClass("active");
        $("#jackpot-tab-" + setting["id"]).addClass("active");

        JH.setJackpotGameID("public:" + setting["id"], setting);
    }

    $(document).ready(function () {
        selectJackpotSetting({json_encode($jackpotsettings.0)})
    });
</script>

<script>
    $("title").html("{$title}");
</script>

<div class="row m-4" id="jackpot-container">

</div>

<div class="jackpot-footer">
</div>