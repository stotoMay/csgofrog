{foreach $items as $item}
    <div class="item {if $item.disabled}deposit-disabled{else}deposit-active{/if}" id="deposit-item-{$item.inventoryID}" data-price="{$item.price}" onclick="TM.onDepositItemClick('{$item.inventoryID}')" style="{if $item.data.color}border-top-color:{$item.data.color};{/if}">
        <span class="exterior">
            {$item.exterior}
        </span>
        <span class="name">
            {$item.weapon} {$item.name}
        </span>
        <span class="value">
            {number_format($item.price)} $
        </span>
        <img src="{$item.image}">
    </div>
{/foreach}
{if count($items) == 0}
    <div style="display:block;text-align: center">You have no items in your inventory</div>
{/if}
<div style="display:block;clear:both"></div>