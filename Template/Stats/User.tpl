<script>
    $("title").html("{$title}");
</script>

<div class="row m-4">
    <div class="col-12 m-auto mt-4">
        <div class="profile">
            <h1>{$user.personaname} (<a href="https://steamcommunity.com/profiles/{$user.steamid}" style="color:white;" target="_blank">Steam Profile</a>)</h1>
            <div class="row">
                <div class="col-4 text-center">
                    <img src="{$user.avatarfull}" alt="{$user.personaname}" />
                </div>
                <div class="col-8">
                    <table class="table table-bordered">
                        <tr>
                            <td>Games</td>
                            <td>{$stats.games}</td>
                        </tr>
                        <tr>
                            <td>Wins</td>
                            <td>{$stats.wins}</td>
                        </tr>
                        <tr>
                            <td>Win rate</td>
                            <td>{number_format(multiply($stats.winrate, 100))}%</td>
                        </tr>
                        <tr>
                            <td>Totally won</td>
                            <td>{number_format(divide($stats.ticketswon, 100))} <i class="fa fa-database"></i></td>
                        </tr>
                        <tr>
                            <td>Level</td>
                            <td id="user-level">0</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $('#user-level').html(getLevel(parseInt('{$userxp}')))
        </script>

        {if count($jackpots) > 0}
            <h1>Jackpot History</h1>

            <table class="table-yellow">
                <tr>
                    <th class="text-center">Game</th>
                    <th class="text-center">Chance</th>
                    <th class="text-center">Bank sum</th>
                    <th class="text-center">Date</th>
                    <th></th>
                </tr>
                {foreach $jackpots as $jackpot}
                    <tr>
                        <td>
                            <b>Jackpot #{$jackpot.jackpotID}</b>
                        </td>
                        <td class="text-center">{$jackpot.chance}%</td>
                        <td class="text-center">
                            <span class="white">{number_format(divide($jackpot.value, 100))} <i class="fa fa-database"></i></span>
                        </td>
                        <td class="text-center">{datetimeformat($jackpot.finished)}</td>
                        <td>
                            {if $jackpot.won}
                                <span class="yellow">Win</span>
                            {else}
                                Lose
                            {/if}
                        </td>
                    </tr>
                {/foreach}
            </table>

            <div class="center-block mt-4">
                <ul class="pagination mx-auto">
                    {if $jackpot_page == 1}
                        <li class="page-item disabled"><a class="page-link">&laquo;</a></li>
                    {else}
                        <li class="page-item"><a class="page-link" href="/stats/user/{$user.steamid}?jackpot_page={sum($jackpot_page, -1)}">&laquo;</a></li>
                    {/if}
                    <li class="page-item disabled"><a class="page-link">{$jackpot_page}</a></li>
                    {if count($jackpots) < 10}
                        <li class="page-item disabled"><a class="page-link">&raquo;</a></li>
                    {else}
                        <li class="page-item"><a class="page-link" href="/stats/user/{$user.steamid}?jackpot_page={sum($jackpot_page, 1)}">&raquo;</a></li>
                    {/if}
                </ul>
            </div>
        {/if}

        {if count($coinflips) > 0}
            <h1>Coinflip History</h1>

            <table class="table-yellow">
                <thead>
                <tr>
                    <th class="text-center">Game</th>
                    <th class="text-center">Players</th>
                    <th class="text-center">Value</th>
                    <th class="text-center">Date</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {foreach $coinflips as $coinflip}
                    <tr>
                        <td>
                            <strong><span>Coinflip</span> #{$coinflip.coinflipID}</strong>
                        </td>
                        <td>
                            <a href="/stats/user/{$coinflip.player_1.user.steamid}" style="font-size: 0">
                                <img src="{$coinflip.player_1.user.avatar}" title="{$coinflip.player_1.user.personaname}">
                            </a>
                            &nbsp; vs &nbsp;
                            <a href="/stats/user/{$coinflip.player_2.user.steamid}" style="font-size: 0">
                                <img src="{$coinflip.player_2.user.avatar}" title="{$coinflip.player_2.user.personaname}">
                            </a>
                        </td>
                        <td><span>{number_format(divide($coinflip.player_1.tickets, 100))} <i class="fa fa-database"></i></span></td>
                        <td>{datetimeformat($coinflip.finished)}</td>
                        {if $coinflip.winner.userID == $userID}
                            <td><span class="yellow">Win</span></td>
                        {else}
                            <td>Lose</td>
                        {/if}
                    </tr>
                {/foreach}
                </tbody>
            </table>

            <div class="center-block mt-4">
                <ul class="pagination mx-auto">
                    {if $coinflip_page == 1}
                        <li class="page-item disabled"><a class="page-link">&laquo;</a></li>
                    {else}
                        <li class="page-item"><a class="page-link" href="/stats/user/{$user.steamid}?coinflip_page={sum($coinflip_page, -1)}">&laquo;</a></li>
                    {/if}
                    <li class="page-item disabled"><a class="page-link">{$coinflip_page}</a></li>
                    {if count($coinflips) < 10}
                        <li class="page-item disabled"><a class="page-link">&raquo;</a></li>
                    {else}
                        <li class="page-item"><a class="page-link" href="/stats/user/{$user.steamid}?coinflip_page={sum($coinflip_page, 1)}">&raquo;</a></li>
                    {/if}
                </ul>
            </div>
        {/if}

        {if count($upgrades) > 0}
            <h1>Upgrade History</h1>

            <table class="table-yellow">
                <thead>
                <tr>
                    <th>Game</th>
                    <th>Deposited</th>
                    <th>Upgraded to</th>
                    <th>Date</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {foreach $upgrades as $upgrade}
                    <tr>
                        <td>
                            <strong><span>Upgrade</span> #{$upgrade.upgradeID}</strong>
                        </td>
                        <td>
                            {foreach $upgrade.deposited as $item}
                                <div class="standard-item">
                                    <div class="standard-item-image"><img src="{$item.image}"></div>
                                    <div class="standard-item-price">{number_format(divide($item.tickets, 100))}</div>
                                </div>
                            {/foreach}
                        </td>
                        <td>
                            <div class="standard-item">
                                <div class="standard-item-image"><img src="{$upgrade.item.image}"></div>
                                <div class="standard-item-price">{number_format(divide($upgrade.item.tickets, 100))}</div>
                            </div>
                        </td>
                        <td>{datetimeformat($upgrade.dateline)}</td>
                        {if $upgrade.won}
                            <td><img style="width:48px;height:48px;" src="/Template/Static/Image/upgrade_win.png"></td>
                        {else}
                            <td><img style="width:48px;height:48px;" src="/Template/Static/Image/upgrade_loose.png"></td>
                        {/if}
                    </tr>
                {/foreach}
                </tbody>
            </table>

            <div class="center-block mt-4">
                <ul class="pagination mx-auto">
                    {if $upgrade_page == 1}
                        <li class="page-item disabled"><a class="page-link">&laquo;</a></li>
                    {else}
                        <li class="page-item"><a class="page-link" href="/stats/user/{$user.steamid}?upgrade_page={sum($upgrade_page, -1)}">&laquo;</a></li>
                    {/if}
                    <li class="page-item disabled"><a class="page-link">{$upgrade_page}</a></li>
                    {if count($upgrades) < 10}
                        <li class="page-item disabled"><a class="page-link">&raquo;</a></li>
                    {else}
                        <li class="page-item"><a class="page-link" href="/stats/user/{$user.steamid}?upgrade_page={sum($upgrade_page, 1)}">&raquo;</a></li>
                    {/if}
                </ul>
            </div>
        {/if}
    </div>
</div>