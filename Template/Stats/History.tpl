<script>
    $("title").html("{$title}");
</script>

<div class="row m-4">
    <div class="col-12 m-auto mt-4">
        <table class="table-yellow">
            <tr>
                <th class="text-center">Game</th>
                <th class="text-center">Winner</th>
                <th class="text-center">Chance</th>
                <th class="text-center">Bank sum</th>
                <th class="text-center">Date</th>
            </tr>
            {foreach $jackpots as $jackpot}
                <tr>
                    <td>
                        <b> #{$jackpot.jackpotID}</b>
                    </td>
                    <td>
                        <a href="/stats/user/{$jackpot.steamID}">
                            <img class="avatar" src="{$jackpot.steam.avatarmedium}" alt="{$jackpot.steam.personaname}" />
                        </a>
                        <a href="/stats/user/{$jackpot.steamID}">{$jackpot.steam.personaname}</a>
                    </td>
                    <td class="text-center">{number_format(multiply($jackpot.chance, 100))}%</td>
                    <td class="text-center">
                        <span class="white">{number_format(divide($jackpot.value, 100))} <i class="fa fa-database"></i></span>
                    </td>
                    <td class="text-center">{datetimeformat($jackpot.dateline)}</td>
                </tr>
            {/foreach}
        </table>

        <div class="center-block mt-4">
            <ul class="pagination mx-auto">
                {if $page == 1}
                    <li class="page-item disabled"><a class="page-link">&laquo;</a></li>
                {else}
                    <li class="page-item"><a class="page-link" href="/stats/history/{sum($page, -1)}">&laquo;</a></li>
                {/if}
                <li class="page-item disabled"><a class="page-link">{$page}</a></li>
                {if $is_lastpage}
                    <li class="page-item disabled"><a class="page-link">&raquo;</a></li>
                {else}
                    <li class="page-item"><a class="page-link" href="/stats/history/{sum($page, 1)}">&raquo;</a></li>
                {/if}
            </ul>
        </div>
    </div>
</div>