<script type="text/javascript">
    function selectJackpotSetting(setting) {
        $(".jackpot-tab").removeClass("active");
        $("#jackpot-tab-" + setting["id"]).addClass("active");

        JH.setJackpotGameID("public:" + setting["id"], setting);
    }

    $(document).ready(function () {
        selectJackpotSetting({json_encode($jackpotsettings.0)})
    });
</script>

<script>
    $("title").html("{$title}");
</script>

<div class="row m-4" id="jackpot-container">
    <div class="col-12 m-auto">
        <div class="row mt-3">

            <div class="col-10 px-2 m-auto">
                <div class="wheel-container jackpot-row">
                    <div class="info-wheel">
                        <b>GAME <span id="game_id">#00000</span>,</b> prize pool <b><span id="total-value">$0.00</span></b>
                    </div>
                    <div class="game winner" id="game-winner">
                        <div class="raffle">
                            <div class="avatars">
                                <div class="pot_roll_avatars"></div>
                            </div>
                            <div class="raffle-overlay text-center color-yellow position-relative hidden">
                                <i class="fa fa-chevron-up"></i>
                                <div class="winner-box"></div>
                            </div>
                        </div>
                    </div>
                    <div id="testProgress">
                        <div class="yellowBG"></div>
                    </div>
                    <div class="winner-info hidden">
                        <div class="winner-info-container text-center mt-4">
                            <div>
                              <span>WINNER</span>
                            </div>
                            <div class="text-yellow-gradient" style="font-size: 30px">
                                <span class="nickname"></span>
                            </div>
                            <div class="text-green-gradient mt-4">
                                 <span>CONGRATULATIONS!</span>
                            </div>
                            <div>
                                <span class="game_value">0.00</span>
                            </div>
                            <div class="text-yellow-gradient mt-4" style="font-size: 18px">
                                PLAYER WON WITH <span><span class="win_chance"></span></span> CHANCE
                            </div>
                        </div>
                    </div>
                    <div class="info-wheel mt-3">
                        <div class="col min-deposit">
                            {foreach $jackpotsettings as $setting}
                                <span class="min-depo border-0 color-white" id="jackpot-tab-{$setting.id}" href='javascript:selectJackpotSetting({json_encode($setting)});'>
                                {if $setting.min_deposit_value == "0"}
                                {else}
                                    MINIMUM DEPOSIT <br>
                                    <span class="color-yellow">{number_format(divide($setting.min_deposit_value, 100))} $</span>
                                {/if}
                            </span>
                            {/foreach}
                        </div>
                        <div class="button-yellow-gradient mt-3 deposit-items-button" {if is_login()} style="width:290px;" {/if}>
                            {if isVerified()}
                                <a href="#" class="button-wave depositItems" id="jackpot-deposit-button">Deposit items</a>
                            {elseif is_login()}
                                <a href="#" class="button-wave" data-toggle="modal" data-target="#trade-url-modal" data-ignore="true" id="jackpot-deposit-button">Set Trade URL to deposit</a>
                            {else}
                                <a href="/login" data-ignore="1" class="button-wave" id="jackpot-deposit-button">Login to deposit</a>
                            {/if}
                        </div>

                        <div id="nb-items-deposited-container">
                            <span style="font-size:16px;">TOTAL ITEMS DEPOSITED</span>
                            <br>
                            <b><span id="nb-items-deposited">0</span>/<span id="max-items">150</span></b>
                        </div>
                    </div>
                    <div id="gamers-section" class="mt-5">
                        <table id="gamers-table"></table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="jackpot-footer">

</div>