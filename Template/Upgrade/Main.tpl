<script type="text/javascript">
    function selectJackpotSetting(setting) {
        $(".jackpot-tab").removeClass("active");
        $("#jackpot-tab-" + setting["id"]).addClass("active");

        JH.setJackpotGameID("public:" + setting["id"], setting);
    }

    $(document).ready(function () {
        selectJackpotSetting({json_encode($jackpotsettings.0)})
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        UH.setUpgradeFee(parseFloat('{setting("fees", "controller", "upgrade")}'));
        UH.setDepositSettings(parseFloat('{setting("min_deposit_value", "controller", "upgrade")}'), 0, parseInt('{setting("max_deposit_items", "controller", "upgrade")}'), parseFloat('{setting("min_item_value", "controller", "upgrade")}'))
    });
</script>

<div class="row m-4">
    <div class="col-12 m-auto">

        <div class="row mb-4">
            <div class="col-4">
                <div id="upgrade-deposit-container" class="mx-auto" onclick="UH.showDeposit()">
                    <div id="upgrade-deposit-items">
                        <div id="upgrade-deposit-item-1" class="upgrade-deposit-item">
                            <div id="upgrade-deposit-item-1-image" class="upgrade-deposit-item-image"></div>
                            <div id="upgrade-deposit-item-1-price" class="upgrade-deposit-item-price"></div>
                        </div>
                        <div id="upgrade-deposit-item-2" class="upgrade-deposit-item">
                            <div id="upgrade-deposit-item-2-image" class="upgrade-deposit-item-image"></div>
                            <div id="upgrade-deposit-item-2-price" class="upgrade-deposit-item-price"></div>
                        </div>
                        <div id="upgrade-deposit-item-3" class="upgrade-deposit-item">
                            <div id="upgrade-deposit-item-3-image" class="upgrade-deposit-item-image"></div>
                            <div id="upgrade-deposit-item-3-price" class="upgrade-deposit-item-price"></div>
                        </div>
                        <div id="upgrade-deposit-item-4" class="upgrade-deposit-item">
                            <div id="upgrade-deposit-item-4-image" class="upgrade-deposit-item-image"></div>
                            <div id="upgrade-deposit-item-4-price" class="upgrade-deposit-item-price"></div>
                        </div>
                        <div id="upgrade-deposit-item-5" class="upgrade-deposit-item">
                            <div id="upgrade-deposit-item-5-image" class="upgrade-deposit-item-image"></div>
                            <div id="upgrade-deposit-item-5-price" class="upgrade-deposit-item-price"></div>
                        </div>
                        <div id="upgrade-deposit-item-6" class="upgrade-deposit-item">
                            <div id="upgrade-deposit-item-6-image" class="upgrade-deposit-item-image"></div>
                            <div id="upgrade-deposit-item-6-price" class="upgrade-deposit-item-price"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="upgrade-select-text">SELECT THE ITEMS YOU WANT TO UPGRADE</div>
                    <div class="upgrade-select-image"></div>
                    <div class="upgrade-select-totalprice">
                        <span class="white"><span class="upgrade-select-totalprice-value">0.00</span> <img src="/Template/Static/Image/coins.png" ></span>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div id="upgrade-result"><img id="upgrade-animation" src="/Template/Static/Image/upgrade_animation/anim_0.png" class="animated"></div>
                <div id="upgrade-button-continue">
                    <a href="javascript:UH.updateItems();">CONTINUE UPGRADE</a>
                </div>
            </div>
            <div class="col-4">
                <div id="upgrade-market-container" class="mx-auto" onclick="UH.showMarket()">
                    <div id="upgrade-market-item">
                        <div id="upgrade-market-item-image"></div>
                    </div>
                    <div class="upgrade-select-text">SELECT THE ITEM YOU WANT TO UPGRADE TO</div>
                    <div class="upgrade-select-image"></div>
                    <div class="upgrade-select-totalprice">
                        <span class="white"><span class="upgrade-select-totalprice-value">0.00</span>$ <img src="/Template/Static/Image/coins.png" ></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-4">
                <div class="upgrade-box">
                    <div class="win-chance row mx-0 border-left">
                        <div class="col-12">
                            <span class="color-white">WIN CHANCE : </span> <span class="text-green-gradient"><span id="upgrade-percentage"> 0.00</span> %</span>
                        </div>
                        <div class="col-12">
                            <span class="color-white">MULTIPLIER : </span> <span class="text-yellow-gradient"><span id="upgrade-percentage"> 0.00</span> %</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="upgrade-box">
                    <div class="row d-block" id="upgrade-box-submit">
                        <div class="button-green-gradient">
                            <a href="javascript:UH.upgrade()">
                                <span class="text-green-gradient">UPGRADE</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="upgrade-box">
                    <div id="upgrade-box-header">
                        <div class="row mx-0">
                            <div class="col px-1">
                                <a href="javascript:UH.autoselectItem(1.5);">1.5x</a>
                            </div>
                            <div class="col px-1">
                                <a href="javascript:UH.autoselectItem(2);">2x</a>
                            </div>
                            <div class="col px-1">
                                <a href="javascript:UH.autoselectItem(5);">5x</a>
                            </div>
                            <div class="col px-1">
                                <a href="javascript:UH.autoselectItem(10);">10x</a>
                            </div>
                            <div class="col px-1">
                                <a href="javascript:UH.autoselectItem(20);">20x</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <span onclick="UH.showDeposit()" id="upgrade-box" class="upgrade-box-deposit white"><img src="/Template/Static/Image/deposit.png" /> DEPOSIT</span>
                            </div>
                            <div class="col">
                                <span onclick="UH.withdraw();" id="upgrade-box" class="upgrade-box-withdraw white"><img src="/Template/Static/Image/withdraw.png" /> WITHDRAW</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div id="upgrade-box">
                                    <div class="row mx-0 text-center">
                                        <div class="col-12">
                                            {foreach $jackpotsettings as $setting}
                                                <a class="min-depo border-0" id="jackpot-tab-{$setting.id}" href='javascript:selectJackpotSetting({json_encode($setting)});'>
                                                    {if $setting.min_deposit_value == "0"}
                                                    {else}
                                                        MINIMUM DEPOSIT {number_format(divide($setting.min_deposit_value, 100))} <i class="fa fa-database"></i>
                                                    {/if}
                                                </a>
                                            {/foreach}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row my-4">
            <div class="col-12 m-auto mt-4">

                <table id="upgrade-withdraw-history-table" style="display:none;" class="table-yellow mb-4">
                    <thead>
                    <tr>
                        <th>GAME</th>
                        <th>ITEMS</th>
                        <th></th>
                        <th>PRICE</th>
                        <th>RESULTS</th>
                        <th>DATE</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="upgrade-market-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <input type="text" id="upgrade-market-search" placeholder="Search Item">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="clearfix"></div>
            </div>
            <div class="modal-body items">
                <div id="upgrade-market-items">
                    <div class="items-container"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<img src="/Template/Static/Image/upgrade_animation/anim_0.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/anim_1.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/anim_2.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/anim_3.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/anim_4.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/anim_5.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/anim_6.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/anim_7.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/anim_8.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/anim_9.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/anim_10.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/anim_11.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/anim_12.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/anim_13.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/anim_14.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/anim_15.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/upgrade_win_1.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/upgrade_win_2.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/upgrade_loose_1.png" style="display:none">
<img src="/Template/Static/Image/upgrade_animation/upgrade_loose_2.png" style="display:none">