// Updated by May Poghosyan 02.2019
function openWinnerWindow(type, value_won, chance_won) {
	$("#winner-modal-value").html(toPriceFormat(value_won / 100));
	$("#winner-modal-chance").html(toPriceFormat(chance_won * 100));
	$("#winner-modal-type").html(type);

	$("#winner-modal").modal("show").find(".modal-dialog").css("z-index", 10);

	setTimeout(function () {
		$("#winner-modal").fireworks("init");
	}, 500);
}

function ChatManager() {
	let self = this;

	WS.on("onChatMessage", function (data) {
		self.addMessage(data)
	});

	WS.on("onChatLoad", function (data) {
		self.setMessages(data)
	});

	this.setMessages = function (data) {
		$('#messages').html("");

		for (let i = 0; i < data.length; i++) {
			self.addMessage(data[i], false);
		}
	};

	this.addMessage = function (data, animate) {
		let $msg;
		$msg = $($('#sample-message')[0]).clone().removeAttr("id");
		if (data["type"] == "user" || data["type"] == "admin") {

			$msg.find('.avatar').attr("src", data['user']['avatarmedium']);
			let xp = data['user']['xp'] ? data['user']['xp'] : 0;
			let level = getLevel(xp);

			$msg.find('.level-white').html(
				'<span>level ' + level + '</span>'
			);
			let status = data['user']['rank'] === "Normal" ? '' : data['user']['rank']+' ';
			let msg = '<a href="/stats/user/' + data['user']["steamid"] + '" style="color:#fff"><b ' + (status ? 'style="color:' + data['user']['color'] + '"' : '') + '>' + status + '</b>' + data["user"]['personaname'] + '</a>: ' + data['msg'];
			$msg.find('.msg-content').append(msg);
		}
		else if (data["type"] == "broadcast") {
			let msg = data['msg'];
			$msg.find('.msg-content').append(msg);
		}

		assignPageLoad($msg);

		let $container = $('#messages');
		if (animate === false) {
			$container.append($msg.show()).slimScroll({scrollTo: $container[0].scrollHeight});
		}
		else {
			$msg.addClass("animated").addClass("bounceIn");
			$container.append($msg.show()).slimScroll({scrollTo: $container[0].scrollHeight, animate: true});
		}
	};

	this.sendMessage = function (e) {
		let $chatinput = $("#input-message");
		if (e) {
			e.preventDefault();
		}

		if ($chatinput.val().length == 0 || $chatinput.val().length > 255) {
			return;
		}

		WS.emit("chat_message", $chatinput.val());
		$chatinput.val("");
	};

	this.setChatLobby = function (name) {
		if (name == lobby) {
			return;
		}

		$("#messages").html("");
		setCookie("chatlobby", name);
		lobby = name;
		WS.emit("join_lobby", name);

		self.selectChatLobby();
	};

	this.selectChatLobby = function () {
		$(".chat-lobby").removeClass("active");
		$(".chat-lobby-" + lobby).addClass("active");
	};

	this.refreshLobby = function () {
		WS.emit("join_lobby", lobby);
		this.selectChatLobby();
	};

	let lobby = getCookie("chatlobby");
	if (!lobby) {
		lobby = "default";
	}

	$('.ca1').click(function () {
		$('.ca2').fadeIn();
		$('.chat_icon').fadeOut();
		$('#chat').addClass('c_active');
		$('#chat_msg').focus().val('');
	});
	$('.ca2').click(function () {
		$('.ca2').fadeOut();
		$('#chat').removeClass('c_active');
	});
	function initHeight() {
		let $chat_messages = $('#messages').css("padding-top", 26);
		$chat_messages.height($(window).height() - $("#form-send-message").height() - 26);
		$chat_messages.slimScroll({
			height: $chat_messages.height() + "px",
			width: '100%'
		}).css("padding", 0);
	}

	$( window ).resize(initHeight);

	$("#input-message").bind("keyup", function (e) {
		if (e.keyCode != 13) {
			return;
		}
		CM.sendMessage();
	});

	$("#send-message-image").click(function() {
		CM.sendMessage();
	});

	WS.on("onConnectionCreate", function () {
		self.refreshLobby();
	});

	initHeight();
}

function TradeManager() {
	let self = this;

	let gamemode = null;
	let gameID = null;
	let deposititems = [];
	let depositvalue = 0;

	let min_deposit_value = 0;
	let max_deposit_items = 0;
	let max_deposit_value = 0;
	let min_item_value = 0;

	WS.on("onDepositSuccess", function (data) {
		$(".deposit-success-id").html(data["depositID"]);
		$(".deposit-success-value").html(toPriceFormat(data["value"] / 100));
		$("#trade-offer-link").attr("href", "https://trade.opskins.com/trade-offers/" + data["offerID"] + "/");

		$("#deposit-modal-success").modal('show');
		$("#deposit-modal").modal('hide');
	});

	WS.on("onDepositError", function (data) {
		$("#deposit-modal-success").modal('hide');
		if (data["message"] != "trade_cancled") {
			showError(g_lang[data["message"]] ? g_lang[data["message"]] : g_lang["invalid_deposit"]);
		}
	});

	WS.on("onDepositFinish", function (data) {
		$("#deposit-modal-success").modal('hide');
	});

	WS.on("onWithdrawTrade", function (data) {
		$(".deposit-success-id").html(data["withdrawID"]);
		$("#withdraw-link").attr("href", "https://trade.opskins.com/trade-offers/" + data["offerID"] + "/");
		setTimeout(function () {
			$("#withdraw-link").attr("href", "https://trade.opskins.com/trade-offers");
		}, 20000);
	});

	this.openDepositWindow = function (_gamemode, _gameID, _min_deposit_value, _max_deposit_value, _max_deposit_items, _min_item_value) {
		gamemode = _gamemode;
		gameID = _gameID;
		deposititems = [];
		depositvalue = 0;

		min_item_value = 0;
		min_deposit_value = 0;
		max_deposit_value = 0;
		max_deposit_items = 100;

		if (typeof _min_deposit_value == "number" && !isNaN(_min_deposit_value)) {
			min_deposit_value = _min_deposit_value * 100
		}
		if (typeof _max_deposit_value == "number" && !isNaN(_max_deposit_value)) {
			max_deposit_value = _max_deposit_value * 100
		}
		if (typeof _max_deposit_items == "number" && !isNaN(_max_deposit_items)) {
			max_deposit_items = _max_deposit_items
		}
		if (typeof _min_item_value == "number" && !isNaN(_min_item_value)) {
			min_item_value = _min_item_value * 100
		}

		$("#deposit-inventory-loading").show();
		$("#confirm-deposit").addClass("disabled");

		self.refreshDepositInventory();

		$("#deposit-maxvalue").html(max_deposit_value ? (max_deposit_value / 100).toFixed(2) + ' <i class="fa fa-database"></i>' : "&infin;");
		$("#deposit-minvalue").html((min_deposit_value / 100).toFixed(2));
		$("#deposit-maxitems").html(max_deposit_items);

		$("#deposit-modal").modal('show');
		$("#deposit-modal .items-value .value").html("0.00");

		$("#deposit-modal .choose-side").hide();
		if(_gamemode == "coinflip" && _gameID == "create:1") {
			$("#deposit-modal .choose-side").show();
		}

		updateDisabledItems();
	};

	this.updateGameID = function (_gameID) {
		gameID = _gameID;
	};

	this.closeDepositWindow = function (depositID) {
		if (typeof depositID == "undefined") {
			$("#deposit").slideUp(500).removeClass();
		}
		else {
			$("#deposit.deposit-" + depositID).slideUp(500).removeClass()
		}
	};

	this.sendDepositTrade = function () {

		if (deposititems.length == 0 || gamemode == "" || depositvalue < min_deposit_value) {
			return;
		}
		$("#deposit-window-success").hide();
		$("#deposit-window-loading").show();
		$("#deposit-window-deposit").hide();

		$("#deposit-modal").modal("hide");

		WS.emit("deposit", {"items": deposititems, "gamemode": gamemode, "gameID": gameID})
	};

	this.cancelDepositTrade = function (depositID) {
		WS.emit("cancel_deposit", depositID);
		self.closeDepositWindow();
	};

	this.refreshDepositInventory = function () {
		$("#deposit-inventory-loading").show();
		$("#deposit-modal .items .items-container").html("<div class='text-center' style='display:block;'><img src='/Template/Static/Image/loading-yellow.gif'></div>");

		$.ajax({
			method: 'POST',
			dataType: 'html',
			url: '/deposit/inventory',
			success: function (data) {
				$("#deposit-modal .items .items-container").html(data);

				updateDisabledItems();
			}
		});
	};

	this.onDepositItemClick = function (inventoryID) {
		let $item = $("#deposit-item-" + inventoryID);

		if ($item.hasClass("deposit-disabled")) {
			return;
		}

		let index = deposititems.indexOf(inventoryID);
		let price = Math.floor(parseFloat($item.attr("data-price")) * 100);

		if (index > -1) {
			deposititems.splice(index, 1);
			$item.removeClass("selected");
			depositvalue -= price;
		}
		else {
			if (deposititems.length >= max_deposit_items) {
				return;
			}
			if (depositvalue + price > max_deposit_value && max_deposit_value != 0) {
				return;
			}

			deposititems.push(inventoryID);
			$item.addClass("selected");
			depositvalue += price;
		}

		if(depositvalue >= min_deposit_value && depositvalue != 0)
		{
			$("#confirm-deposit").removeClass("disabled");
		}
		else
		{
			$("#confirm-deposit").addClass("disabled");
		}

		$("#deposit-modal .items-value .value").html(toPriceFormat(depositvalue / 100));
		$("#deposit-modal .items-value .quantity1").html(deposititems.length);

		updateDisabledItems();
	};

	function updateDisabledItems() {
		let totalQuantity = 0;
		let totalAmount = 0;
		$("#deposit-modal").find(".deposit-active").each(function () {
			totalQuantity++;
			let $this = $(this);
			let tickets = Math.floor(parseFloat($this.attr("data-price"))*100);
			totalAmount += parseFloat($this.attr("data-price"));
			if(((depositvalue + tickets > max_deposit_value && max_deposit_value != 0) || deposititems.length >= max_deposit_items || tickets < min_item_value) && !$this.hasClass("selected"))
			{
				$this.addClass("disabled");
			} else {
				$this.removeClass("disabled");
			}
		});
		$("#deposit-modal .items-value .quantity2").html(totalQuantity);
		$("#deposit-modal .items-value .total").html(totalAmount.toFixed(2));
	}

	this.updateTradeUrl = function () {
		$.ajax({
			method: 'POST',
			dataType: 'json',
			url: '/account/updatetradeurl',
			data: {"tradeurl": $('#opskins_url').val(), "terms": 1, "token": g_sessionid},
			success: function (data) {
				if (data.success) {
					$('.disclaimer').slideUp(500);
					location.reload();
				}
				else {
					$('#opskins_url').attr("placeholder", data.message).val("");
					return;
				}
				g_serverauth = data["accesstoken"];
				WS.emit("auth", g_serverauth);
			}
		});
	};
}

function RandomGenerator(seed) {
	var currenthash = seed;

	this.random = function () {
		currenthash = sha1(currenthash);

		return parseInt(currenthash.slice(0, 8), 16) / Math.pow(2, 32);
	};

	function sha1(str) {
		var rotate_left = function (n, s) {
			return ((n << s) | (n >>> (32 - s)));
		};

		var cvt_hex = function (val) {
			var str = '';
			var i;
			var v;

			for (i = 7; i >= 0; i--) {
				v = (val >>> (i * 4)) & 0x0f;
				str += v.toString(16);
			}
			return str;
		};

		var blockstart;
		var i, j;
		var W = new Array(80);
		var H0 = 0x67452301;
		var H1 = 0xEFCDAB89;
		var H2 = 0x98BADCFE;
		var H3 = 0x10325476;
		var H4 = 0xC3D2E1F0;
		var A, B, C, D, E;
		var temp;

		str = utf8_encode(str);
		var str_len = str.length;

		var word_array = [];
		for (i = 0; i < str_len - 3; i += 4) {
			j = str.charCodeAt(i) << 24 | str.charCodeAt(i + 1) << 16 | str.charCodeAt(i + 2) << 8 | str.charCodeAt(i + 3);
			word_array.push(j);
		}

		switch (str_len % 4) {
			case 0:
				i = 0x080000000;
				break;
			case 1:
				i = str.charCodeAt(str_len - 1) << 24 | 0x0800000;
				break;
			case 2:
				i = str.charCodeAt(str_len - 2) << 24 | str.charCodeAt(str_len - 1) << 16 | 0x08000;
				break;
			case 3:
				i = str.charCodeAt(str_len - 3) << 24 | str.charCodeAt(str_len - 2) << 16 | str.charCodeAt(str_len - 1) <<
					8 | 0x80;
				break;
		}

		word_array.push(i);

		while ((word_array.length % 16) != 14) {
			word_array.push(0);
		}

		word_array.push(str_len >>> 29);
		word_array.push((str_len << 3) & 0x0ffffffff);

		for (blockstart = 0; blockstart < word_array.length; blockstart += 16) {
			for (i = 0; i < 16; i++) {
				W[i] = word_array[blockstart + i];
			}
			for (i = 16; i <= 79; i++) {
				W[i] = rotate_left(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1);
			}

			A = H0;
			B = H1;
			C = H2;
			D = H3;
			E = H4;

			for (i = 0; i <= 19; i++) {
				temp = (rotate_left(A, 5) + ((B & C) | (~B & D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
				E = D;
				D = C;
				C = rotate_left(B, 30);
				B = A;
				A = temp;
			}

			for (i = 20; i <= 39; i++) {
				temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
				E = D;
				D = C;
				C = rotate_left(B, 30);
				B = A;
				A = temp;
			}

			for (i = 40; i <= 59; i++) {
				temp = (rotate_left(A, 5) + ((B & C) | (B & D) | (C & D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
				E = D;
				D = C;
				C = rotate_left(B, 30);
				B = A;
				A = temp;
			}

			for (i = 60; i <= 79; i++) {
				temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
				E = D;
				D = C;
				C = rotate_left(B, 30);
				B = A;
				A = temp;
			}

			H0 = (H0 + A) & 0x0ffffffff;
			H1 = (H1 + B) & 0x0ffffffff;
			H2 = (H2 + C) & 0x0ffffffff;
			H3 = (H3 + D) & 0x0ffffffff;
			H4 = (H4 + E) & 0x0ffffffff;
		}

		temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);
		return temp.toLowerCase();
	}

	function utf8_encode(argString) {
		if (argString === null || typeof argString === 'undefined') {
			return ''
		}

		var string = (argString + '');
		var utftext = '';
		var start;
		var end;

		start = end = 0;
		var stringl = string.length;
		for (var n = 0; n < stringl; n++) {
			var c1 = string.charCodeAt(n);
			var enc = null;

			if (c1 < 128) {
				end++
			} else if (c1 > 127 && c1 < 2048) {
				enc = String.fromCharCode(
					(c1 >> 6) | 192, (c1 & 63) | 128
				)
			} else if ((c1 & 0xF800) !== 0xD800) {
				enc = String.fromCharCode(
					(c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
				)
			} else {
				if ((c1 & 0xFC00) !== 0xD800) {
					throw new RangeError('Unmatched trail surrogate at ' + n)
				}
				var c2 = string.charCodeAt(++n);
				if ((c2 & 0xFC00) !== 0xDC00) {
					throw new RangeError('Unmatched lead surrogate at ' + (n - 1))
				}
				c1 = ((c1 & 0x3FF) << 10) + (c2 & 0x3FF) + 0x10000;
				enc = String.fromCharCode(
					(c1 >> 18) | 240, ((c1 >> 12) & 63) | 128, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
				)
			}
			if (enc !== null) {
				if (end > start) {
					utftext += string.slice(start, end)
				}
				utftext += enc;
				start = end = n + 1
			}
		}

		if (end > start) {
			utftext += string.slice(start, stringl)
		}

		return utftext
	}
}

function JackpotHandler() {
	let self = this;

	let gametime = 60;
	let max_items = 0;
	let jackpotID = 0;
	let roundhash = "";

	let totalvalue = 0;
	let itemcount = 0;

	let starttime = 0;

	let players = [];

	let jackpot_disabled = false;
	let jackpot_joinqueue = $({});

	let depositsettings = {};
	let jackpotGameID = null;
	this.setJackpotGameID = function(gameID, data) {
		if(jackpotGameID == gameID) {
			return;
		}

		self.clearJackpot();

		depositsettings = data;
		jackpotGameID = gameID;

		WS.emit("set_gamemode", {"gamemode": "jackpot", "gameID": jackpotGameID});
	};

	WS.on("onConnectionCreate", function () {
		WS.emit("set_gamemode", {"gamemode": "jackpot", "gameID": jackpotGameID});
	});

	WS.on("onJackpotLoad", function (data) {
		self.setJackpot(data)
	});

	WS.on("onJackpotPlayerJoin", function (data) {
		self.addPlayer(data);
	});

	WS.on("onJackpotTimerStart", function (data) {
		self.startJackpotTimer(data);
	});

	WS.on("onJackpotEnd", function (data) {
		self.startWinnerAnimation2(data);
	});

	WS.on("onJackpotStart", function (data) {
		self.restartJackpot(data);
	});

	WS.on("onJackpotValueUpdate", function (data) {
		let gameID = data["gameID"];

		if (gameID.startsWith("public:") !== true) {
			return;
		}

		$(".jackpot-value-" + gameID.replace("public:", "")).html(toPriceFormat(data["tickets"] / 100))
	});

	this.playerLayout = function(steamId, avatar, percentage, price, colorId, personaname) {
		return '<a href="/stats/user/' + steamId + '" target="_blank" class="user color-' + colorId + '" id="user-' + steamId + '" data-steamid="' + steamId + '" title="' + personaname + '">' +
			'<img src="' + avatar + '">' +
			'<div class="percentage">' +
			percentage + '%' +
			'</div>' +
			'<div class="price">' +
			'' + price + '' +
			'</div>' +
			'</a>';
	};

	this.GamerItemLayout = function(image, name, rarityColor, price) {
		return '<div class="item" title="' + name + '" ' + (rarityColor ? 'style="border-top: 2px solid ' + rarityColor + '"' : '') + '>' +
			'    <div style="width:100px;height:100px;background-repeat: no-repeat; background-size: contain; background-position: 50% 50%;background-image: url(' + image + ');"></div>' +
			'    <div class="price" >$ ' + price.toFixed(2) + '</div>' +
			'</div>';
	};
	this.testAddPlayer = function(data) {

		totalvalue += parseInt(data["tickets"]);
		$(".game_value").html(toPriceFormat(totalvalue / 100));

	}
	this.addPlayer = function (data, noAnimation) {
		if (jackpot_disabled) {
			jackpot_joinqueue.queue("joinqueue", function (next) {
				JH.addPlayer(data);
				next();
			});
			return;
		}

		players.push(data);

		let tickets = parseInt(data['tickets']);
		totalvalue += tickets;
		let percentage = tickets / (totalvalue)*100;
		let price = tickets / 100;

		let user = data["user"];
		let layout = '' +
			'<tr class="animated bounceInDown">' +
			'<td><img src="' + user["avatarmedium"] + '" alt="" class="avatar"></td>' +
			'<td>' +
			'<div style="font-size:21px;">' + user["personaname"] + '</div>' +
			'<div class="color-yellow" style="font-size:18px;">BET AMOUNT ' + price + '$</div>' +
			'</td>' +
			'<td style="min-width:500px;">'
		for (let i = 0; i < Math.min(data["items"].length, 4); i++) {
			let item = data["items"][i];
			let image = item['image'];
			let $item = this.GamerItemLayout(image,
			item["market_name"],
			item['color'],
			item['tickets']/100);
			layout += $item;
		}
		itemcount += data["items"].length;
		layout += '</td>' +
			'<td class="items-ticket">' +
			'<div class="color-yellow">' + data["items"].length + ' ITEMS, CHANCE <span class="chance">' + percentage.toFixed(2) + '</span></div>' +
			'<div style="font-size:13px;">TICKET FROM #1 TO #<span>' + data["tickets"] + '</span> </div>' +
			'</td>' +
			'</tr>' +
			'<tr class="bg-transparent emptyRow"><td></td><td></td><td></td><td></td></tr>'
		$("#gamers-table").prepend(layout);

		for (let i = 0; i < players.length; i++) {
			var player = players[i];
			let $gamerRow = $( "#gamers-table tr:nth-child(" + ((players.length-i)*2-1) + ") td:nth-child(4) .chance" );
			$gamerRow.html(toPriceFormat((parseInt(player["tickets"]) * 100) / totalvalue) + "%");
		}

		$("#nb-items-deposited").html(itemcount);
		$("#total-value").html((totalvalue/100).toFixed(2) + '$');

	};

	this.startWinnerAnimation2 = function (data) {

		var $winner_info_container = $(".winner-info-container");
		var deposited_tickets = 0;

		$winner_info_container.find(".nickname").html(data["user"]["personaname"]);
        for (var i = 0; i < players.length; i++) {
			if (parseInt(players[i]["user"]["steamid"]) != parseInt(data["user"]["steamid"])) {
				continue;
			}

			deposited_tickets += parseInt(players[i]["tickets"]);
		}
		$winner_info_container.find(".win_chance").html(toPriceFormat((deposited_tickets / totalvalue) * 100) + "%");
		$winner_info_container.find(".game_value").html((totalvalue/100).toFixed(2) + '$');

		$(".deposit-items-button").hide();
		$(".min-deposit").hide();
		var $rollavatars = $(".pot_roll_avatars").html("").stop();
		var _avatarcount = 50;
		var $winnerimg = null;
		var generator = new RandomGenerator(data["secret"]);
		$(".pot_roll_avatars").css("width", _avatarcount * 104 + 'px');
		for (i = 0; i < _avatarcount; i++) {
			var $img = $(document.createElement("img"));

			if (i == _avatarcount - 10) {
				$img.attr({
					"src": data["user"]["avatarmedium"],
					"alt": data["user"]["personaname"],
					"class": "gamer-avatar",
					"id": "winner"
				});
				$winnerimg = $img;
			} else {
				var winningticket = Math.ceil(generator.random() * totalvalue);
				var currentticket = 0;

				for (var k = 0; k < players.length; k++) {
					if (currentticket + 1 <= winningticket && currentticket + parseInt(players[k]["tickets"]) >= winningticket) {
						$img.attr({"src": players[k]["user"]["avatarmedium"], "alt": players[k]["user"]["personaname"], "class": "gamer-avatar"})
					}
					currentticket += parseInt(players[k]["tickets"]);
				}
			}

			$rollavatars.append($img);
		}

		$("#game-winner").show();
		$(".raffle-overlay").show();

		$(".avatars").scrollLeft(0);
		$(".avatars").animate({scrollLeft: 104 * (_avatarcount - 14) - 5}, {
			duration: 6000,
			easing: "easeOutCirc",
			complete: onRollFinish
		});

		function onRollFinish() {
			$('.winner-info').show();
			$("#nb-items-deposited-container").hide();
			$("#winner").css("box-shadow", "0px 0px 40px #ffcc00");
			$(".avatars").attr("style", "background:none;");
		}
	};

	this.startJackpotTimer = function (ts) {
		starttime = parseInt(ts);

		let timeleft = starttime + gametime + timefix - Math.round(Date.now() / 1000);
		let progression = timeleft / gametime;

		if(progression > 1) {
			progression = 1;
		}

		let width = $("#testProgress").width();

		$(".yellowBG").css("width", width*progression)
		$(".yellowBG").animate({
			width:0
		}, timeleft*1000, "linear")
	};

	this.restartJackpot = function (data) {
		jackpot_disabled = true;

		setTimeout(() => {

			JackpotCanvas.reset();
			$(".winner-info").hide();
			$("#nb-items-deposited-container").show();
			$(".deposit-items-button").show();
			$(".min-deposit").show();
			$(".jackpot-footer").html("");

			let width = $("#testProgress").width();
			$(".yellowBG").animate({
				width: width
			}, 1000, "linear")

			JH.setJackpot(data);
			jackpot_disabled = false;
			jackpot_joinqueue.dequeue("joinqueue");
		}, 12000);
	};

	this.clearJackpot = function () {
		$(".jackpot-winner").hide();
		$(".deposit-items-button").show();
		$(".min-deposit").show();
		$("#user-chance").html("0.00%");
		$("#game-info").show();
		$(".avatars").attr("style", "");
		$(".pot_roll_avatars").html("");
		$('.winner-info').hide();
		$("#nb-items-deposited-container").show();
		$(".jackpot-footer").html("");
		$("#roulette-time-open-minutes").html("000");
		$("#roulette-time-open-seconds").html("00");
		$(".hash-key").html("--");
		$(".game_value").html("0.00");
		$("#game_id").html("#0");
		$(".max-items").html("0");
		$('.progress-bar .progress').css('width', 0);
		$(".items_count").html(0);

	};

	this.setJackpot = function (data) {
		$("#user-chance").html("0.00%");
		$(".jackpot-winner").hide();
		$(".deposit-items-button").show();
		$(".min-deposit").show();
		$(".jackpot-footer").html("Hash : " + data['roundhash']);
		$(".pot_roll_avatars").html("");
		$(".raffle-overlay").hide();
		gametime = parseInt(data["gametime"]);
		max_items = parseInt(data["max_items"]);
		jackpotID = parseInt(data["jackpotID"]);
		totalvalue = 0;
		players = [];
		roundhash = data["roundhash"];

		// reset canvas
		JackpotCanvas.players = [];
		JackpotCanvas.totalValue = 0;
		itemcount = 0;

		JackpotCanvas.reset(function () {
			$("#total-value").html("0.00$");
			$("#game_id").html("#" + data["jackpotID"]);
			$("#max-items").html(data["max_items"]);
			$("#jackpot-container #gamers-table").html("");
			$("#nb-items-deposited").html(0);

			for (let i = 0; i < data["players"].length; i++) {
				JH.addPlayer(data["players"][i], true);
			}
			if(data["starttime"]) {
				setTimeout(function () {
					JH.startJackpotTimer(data["starttime"])
				}, 200);
			}

			if (data["winner"]) {
				setTimeout(function () {
					JH.startWinnerAnimation2(data["winner"]);
				}, 500);
			}

		});
	};

	$("#jackpot-deposit-button").click(function() {
		if($("#jackpot-deposit-button").attr("data-ignore")) {
			return;
		}

		TM.openDepositWindow('jackpot', jackpotGameID, parseInt(depositsettings["min_deposit_value"]) / 100, parseInt(depositsettings["max_deposit_value"]) / 100, parseInt(depositsettings["max_deposit_items"]));
	});
}

function CoinflipHandler() {
	let self = this;

	let games = [];
	let openCoinflipID = null;

	let deposit_range = 10;
	let max_deposit_items = 10;
	let coinflipWinnerTimer = 2000;

	let biggestCoinflip = null;

	// preload winner animations
	let winner_t_image = new Image();
	let winner_ct_image = new Image();

	winner_t_image.src = "/Template/Static/Image/t1.gif";
	winner_ct_image.src = "/Template/Static/Image/ct1.gif";

	WS.on("onConnectionCreate", function () {
		WS.emit("set_gamemode", "coinflip");

		$.ajax({
			method: 'GET',
			dataType: 'json',
			url: '/coinflip/jsondata',
			success: function (data) {
				if(!data["success"]) {
					return;
				}

				$("#activity-stream .col").html("");
				for (let i = 0; i < data["coinflips"].length; i++) {
					$("#activity-stream .col").append(self.activityStreamLayout(data["coinflips"][i]));
				}

				biggestCoinflip = data["top"];
				renderCoinflipStats();
			}
		});
	});

	WS.on("onCoinflipLoad", function (data) {
		self.setCoinflips(data);
		renderCoinflipStats();
	});

	WS.on("onCoinflipCreate", function (data) {
		self.addCoinflip(data);
		let owner = data["player_1"] ? data["player_1"]["user"]["userID"] : data["player_2"]["user"]["userID"];
		if (owner == g_userid) {
			self.openCoinflipWindow(data["coinflipID"]);
		}
		renderCoinflipStats();
	});

	WS.on("onCoinflipPlayerJoin", function (data) {
		self.addPlayer(data["coinflipID"], data);

		if (data["user"]["userID"] == g_userid && !self.isCoinflipWindowOpen()) {
			self.openCoinflipWindow(data["coinflipID"]);
		}
	});

	WS.on("onCoinflipEnd", function (data) {
		self.setCoinflipWinner(data["coinflipID"], data);
		self.animateWinner(data["coinflipID"]);
	});

	WS.on("onCoinflipEnable", function (data) {
		self.enableCoinflip(data);
	});

	WS.on("onCoinflipDisable", function (data) {
		self.disableCoinflip(data["coinflipID"], data["user"], data["disabletime"]);
	});

	function renderCoinflipStats() {
		let total_value = 0;
		let own_coinflips = 0;
		let number_of_coinflips = 0;
		let totalitems = 0

		for(let i = 0; i < games.length; i++) {
			let game = games[i];

			if(game["winner"]) {
				continue;
			}

			number_of_coinflips += 1;

			if(game["player_1"]) {
				if(game["player_1"]["user"]["userID"] == g_userid) {
					own_coinflips += game["player_1"]["tickets"]
				}

				total_value += parseInt(game["player_1"]["tickets"]);
				totalitems += game["player_1"]["items"].length
			}

			if(game["player_2"]) {
				if(game["player_2"]["user"]["userID"] == g_userid) {
					own_coinflips += game["player_2"]["tickets"]
				}

				total_value += parseInt(game["player_2"]["tickets"]);
				totalitems += game["player_2"]["items"].length
			}
		}

		$("#coinflip-stats-totalvalue").html(toPriceFormat(total_value / 100) + '$');
		$("#coinflip-stats-totalitems").html(totalitems);
		$("#coinflip-stats-items").html(toPriceFormat(own_coinflips / 100) + '$');
		$("#coinflip-stats-count").html(number_of_coinflips);

		if(!biggestCoinflip) {
			return;
		}

		$("#coinflip-stats-biggest-avatar").attr({"src": biggestCoinflip["winner"]["user"]["avatar"], "title": biggestCoinflip["winner"]["user"]["personaname"]});
		$("#coinflip-stats-biggest-user").attr({"href": "/stats/user/" + biggestCoinflip["winner"]["user"]["steamid"]});
		$("#coinflip-stats-biggest-value").html(toPriceFormat((parseInt(biggestCoinflip["player_1"]["tickets"]) + parseInt(biggestCoinflip["player_2"]["tickets"])) / 100));
	};

	this.openDepositCreateWindow = function () {
		$(".choose-side .side").removeClass("selected");
		$(".choose-side .side-1").addClass("selected");
		TM.openDepositWindow('coinflip', 'create:1', parseFloat('{setting("min_deposit_value", "controller", "coinflip")}'), 0, parseInt('{setting("max_deposit_items", "controller", "coinflip")}'));
	};

	this.setDepositRange = function (range) {
		deposit_range = range;
	};

	this.setMaxDepositItems = function (itemcount) {
		max_deposit_items = itemcount;
	};

	this.activityStreamLayout = function(coinflipdata) {
		let winneruser = coinflipdata["winner"];
		if(winneruser["user"]) {
			winneruser = winneruser["user"];
		}

		let coin_side = "t";
		if(coinflipdata["player_2"]["user"]["userID"] == winneruser["userID"]) {
			coin_side = "ct";
		}

		let nbItems = coinflipdata["player_1"]["items"].length + coinflipdata["player_2"]["items"].length;

		let allitems = coinflipdata["player_1"]["items"].concat(coinflipdata["player_2"]["items"]);
		let bestItem = allitems[0];

		for(let i = 0; i < allitems.length; i++) {
			if(parseInt(allitems[i]["tickets"]) > parseInt(bestItem["tickets"])) {
				bestItem = allitems[i];
			}
		}

		return '' +
			'<div class="activity animated bounceIn" id="coinflip-activity-stream-' + coinflipdata["coinflipID"] + '">' +
			'    <img class="prize" src="' + bestItem['image'] + '" title="' + bestItem['market_name'] + '">' +
			'    <img class="avatar" src="' + winneruser['avatar'] + '" title="' + winneruser['personaname'] + '">' +
			'</div>'
	};

	this.layoutCoinflip = function(coinflipId, player1, player2) {
		let cf = '' +
			'<tr id="coinflip-' + coinflipId + '" class="animated bounceIn" data-tickets="' + (player1 ? player1['tickets'] : player2['tickets'])+ '">' +
			'   <td>' +
			'       <div class="player p1">';
		if(player1) {
			cf += '     <img class="avatar" src="' + player1['user']['avatar'] + '">' +
				'       <div class="coin t"></div>';
		}
		cf += ' </div>' +
			'   <div class="player py-2"> <span>VS</span> </div>    ' +
			'       <div class="player p2">';
		if(player2) {
			cf += '' +
				'       <div class="coin ct"></div>' +
				'       <img class="avatar" src="' + player2['user']['avatar'] + '">';
		}
		cf += '     </div>' +
			'   </td>' +
			'   <td class="items">' +
			'   </td>' +
			'   <td>' +
			'       <div class="timer"></div>' +
			'   </td>' +
			'   <td>' +
			'       <span class="total-value">$0</span><br>' +
			'       <span class="value-needed"></span>' +
			'   </td>' +
			'   <td>' +
			'       <a href="#" class="view"><i class="fa fa-eye"></i></a>' +
			'   </td>' +
			'   <td>' +
			'       <a href="#" class="join"><span>JOIN GAME</span></a>' +
			'       <a href="#" class="waiting view" style="display:none"><span>WAITING</span></a>' +
			'       <a href="#" class="in-progress view" style="display:none"><span>IN PROGRESS</span></a>' +
			'   </td>' +
			'</tr><tr class="bg-transparent emptyRow"><td colspan="6"></td></tr>';
		return cf;
	};

	this.setCoinflips = function (data) {
		games = [];
		$("#coinflips").html("");

		for (let i = 0; i < data.length; i++) {
			CH.addCoinflip(data[i]);
		}
	};

	this.addCoinflip = function (data) {
		games.push(data);

		let $table = $("#coinflips");
		let is_added = false;

		let $coinfliprow = this.layoutCoinflip(data["coinflipID"], data["player_1"], data['player_2']);

		let rows = $table.find("tr");
		for (let i = 0; i < rows.length; i++) {
			let $this = $(rows[i]);
			let coinflipTickets = parseInt($this.attr("data-tickets"));

			if (isNaN(coinflipTickets) || coinflipTickets == 0) {
				continue;
			}

			if (coinflipTickets < parseInt(data["tickets"])) {
				is_added = true;
				$this.before($coinfliprow);

				break;
			}
		}

		if (is_added === false) {
			$table.append($coinfliprow);
		}

		updateCoinflip(data["coinflipID"]);
		$("#coinflip-" + data["coinflipID"]).show();

		if (data["winner"]) {
			if(data["tickets"] >= 500) {
				setTimeout(function () {
					CH.removeCoinflip(data["coinflipID"])
				}, 120000)
			}
			else {
				setTimeout(function () {
					CH.removeCoinflip(data["coinflipID"])
				}, 16000)
			}
		}
	};

	this.removeCoinflip = function (coinflipID) {
		for (let i = 0; i < games.length; i++) {
			if (games[i]["coinflipID"] != coinflipID) {
				continue;
			}

			games.splice(i, 1);
			break;
		}

		$("#coinflip-" + coinflipID).slideUp(200, function () {
			$("#coinflip-" + coinflipID).next().remove();
			$("#coinflip-" + coinflipID).remove();
		});
	};

	this.openCoinflipWindow = function (coinflipID) {
		openCoinflipID = parseInt(coinflipID);
		updateCoinflip(coinflipID);
		$("#coinflip-modal").modal("show");
	};

	this.closeCoinflipWindow = function () {
		$("#coinflip-window").fadeOut(500);
		openCoinflipID = null;
	};

	this.isCoinflipWindowOpen = function () {
		return openCoinflipID !== null;
	};

	this.addPlayer = function (coinflipID, data) {
		let coinflip = getCoinflip(coinflipID);
		let team = 2;
		if(coinflip['player_2']) {
			team = 1;
		}

		setCoinflip(coinflipID, "player_joining", null);
		setCoinflip(coinflipID, "disabled", false);
		setCoinflip(coinflipID, "disabletime", 0);
		setCoinflip(coinflipID, "player_" + team, data);

		updateCoinflip(coinflipID);
	};

	this.setCoinflipWinner = function (coinflipID, winner) {
		setCoinflip(coinflipID, "winner", winner);
	};

	this.animateWinner = function (coinflipID) {
		let winner_number;
		let coinflip = getCoinflip(coinflipID);

		if (coinflip["winner"]["user"]["steamid"] == coinflip["player_1"]["user"]["steamid"]) {
			winner_number = 1;
		}
		else {
			winner_number = 2;
		}

		if (coinflipID == openCoinflipID) {
			$("#coinflip-animated-winner-coin").show().find("img")[0].src = "/Template/Static/Image/transparent.png";
			$("#coinflip-animated-winner-coin").show().find("img")[0].src = "/Template/Static/Image/" + (winner_number == 1 ? "t" : "ct") + "1.gif";
			$("#coinflip-coin").html('');
		}

		$("#coinflip-" + coinflipID).find(".timer").html('<div style="display:block;width:64px;height:64px;"></div>');

		setTimeout(function () {
			coinflip = getCoinflip(coinflipID);

			if(coinflip["winner"]["user"]["userID"] == g_userid) {
				let totalvalue = coinflip["player_1"]["tickets"] + coinflip["player_2"]["tickets"];
				setTimeout(function () {
					openWinnerWindow("coinflip", totalvalue, coinflip["player_" + winner_number]["tickets"] / totalvalue);
				}, 1000);
			}

			if(!biggestCoinflip) {
				biggestCoinflip = coinflip;
			}

			let currentCoinflipValue = parseInt(coinflip["player_1"]["tickets"]) + parseInt(coinflip["player_2"]["tickets"]);
			let biggestCoinflipValue = parseInt(biggestCoinflip["player_1"]["tickets"]) + parseInt(biggestCoinflip["player_2"]["tickets"]);

			if(currentCoinflipValue > biggestCoinflipValue) {
				biggestCoinflip = coinflip;
			}

			renderCoinflipStats();

			if($("#activity-stream .col #coinflip-activity-stream-"+coinflip["coinflipID"]).length == 0) {
				$("#activity-stream .col").prepend(self.activityStreamLayout(coinflip));
			}

            $("#coinflip-" + coinflipID).find(".timer").html('<img style="width:32px;height:32px" src="/Template/Static/Image/coin_' + (coinflip["winner"]["user"]["userID"] == coinflip["player_1"]["user"]["userID"] ? "t" : "ct") + '.png">');

			if(coinflipID == openCoinflipID) {
				let $hidden_info = $(document.createElement("div")).html(
					'<b>Secret:</b> ' + coinflip["winner"]["secret"] +
					'<br><b>Percentage:</b> ' + parseFloat((coinflip["winner"]["percentage"]) * 100).toPrecision(9) + "%"
				).hide();

				$("#coinflip-modal .coinflip-info").html("<b>Hash:</b> " + coinflip["roundhash"]).append($hidden_info);
				$hidden_info.slideDown(500);
			}
		}, coinflipWinnerTimer);

		if(coinflip["tickets"] >= 500) {
			setTimeout(function () {
				CH.removeCoinflip(coinflipID)
			}, 120000)
		}
		else {
			setTimeout(function () {
				CH.removeCoinflip(coinflipID)
			}, 36000)
		}
	};

	this.disableCoinflip = function (coinflipID, user, disabletime) {
		setCoinflip(coinflipID, "player_joining", user);
		setCoinflip(coinflipID, "disabled", true);
		setCoinflip(coinflipID, "disabletime", disabletime);
		updateCoinflip(coinflipID);
	};

	this.enableCoinflip = function (coinflipID) {
		setCoinflip(coinflipID, "player_joining", null);
		setCoinflip(coinflipID, "disabled", false);
		setCoinflip(coinflipID, "disabletime", 0);
		updateCoinflip(coinflipID);
	};

	function rowItemLayout(item1, item2) {
		let layout = '' +
			'<tr class="item">';
		if(item1) {
			let first_row1 = '';
			let second_row1 = '';
			if (item1["market_name"].indexOf(" (") !== -1) {
				first_row1 = item1["market_name"].substr(0, item1["market_name"].indexOf(" ("));
				second_row1 = item1["market_name"].substring(item1["market_name"].lastIndexOf(" (") + 1, item1["market_name"].lastIndexOf(")") + 1)
			} else {
				first_row1 = item1["market_name"];
			}
			layout += '' +
				'<td style="background-color: #2b2d33"><img src="' + item1['image'] + '"></td>' +
				'<td colspan="2" class="coinflip-market-name px-2" style="background-color: #2b2d33">' +
				'<div class="text-left">' + first_row1 + '</div>' +
				'<div style="overflow:hidden;font-size:12px">' +
				'<span style="float:left;">' + second_row1 + '</span>' +
				'<span style="float:right;color:#606060;">' + (item1['tickets']/100).toFixed(2) + '$</span>' +
				'</div>' +
				'</td>';
		}
		else {
			layout += '' +
				'<td></td><td colspan="2"></td>';
		}
		layout += '<td class="bg-transparent"></td>';
		if(item2) {
			let first_row2 = '';
			let second_row2 = '';
			if (item2["market_name"].indexOf(" (") !== -1) {
				first_row2 = item2["market_name"].substr(0, item2["market_name"].indexOf(" ("));
				second_row2 = item2["market_name"].substring(item2["market_name"].lastIndexOf(" (") + 1, item2["market_name"].lastIndexOf(")") + 1)
			} else {
				first_row2 = item2["market_name"];
			}
			layout += '' +
				'<td style="background-color: #2b2d33">' +
				'<img src="' + item2['image'] + '">' +
				'</td>' +
				'<td colspan="2" class="coinflip-market-name px-2" style="background-color: #2b2d33">' +
				'<div class="text-left">' + first_row2 + '</div>' +
				'<div style="overflow:hidden;font-size:12px">' +
				'<span style="float:left;">' + second_row2 + '</span>' +
				'<span style="float:right;color:#606060;">' + (item2['tickets']/100).toFixed(2) + '$</span>' +
				'</div>' +
				'</td>';
		}
		else {
			layout += '' +
				'<td></td><td colspan="2"></td>';
		}
		layout += '</tr><tr class="bg-transparent emptyRow"></tr>';
		return layout;

	}

	function updateCoinflip(coinflipID) {
		let i;
		let coinflip = getCoinflip(coinflipID);

		let joinfunction = function () {
			let tickets = coinflip["player_1"] ? coinflip["player_1"]['tickets'] : coinflip["player_2"]['tickets'];
			TM.openDepositWindow("coinflip", coinflipID, Math.floor(tickets * (1 - (deposit_range / 100))) / 100, Math.floor(tickets * (1 + (deposit_range / 100))) / 100, max_deposit_items)
		};

		let player_left = null;
		let player_right = null;

		if(coinflip["player_1"]) {
			if(coinflip["player_1"]["number"] == 1) {
				player_left = coinflip["player_1"];
				player_left["side"] = "t";
			}
			else {
				player_right = coinflip["player_1"];
				player_right["side"] = "t";
			}
		}

		if(coinflip["player_2"]) {
			if(coinflip["player_2"]["number"] == 1) {
				player_left = coinflip["player_2"];
				player_left["side"] = "ct";
			}
			else {
				player_right = coinflip["player_2"];
				player_right["side"] = "ct";
			}
		}

		if (openCoinflipID == coinflipID) {
			let $coinflip_info = $("#coinflip-modal .coinflip-info").html("<b>Hash:</b> " + coinflip["roundhash"]);

			if(coinflip["winner"]) {
				$coinflip_info.append('<br><b>Secret:</b> ' + coinflip["winner"]["secret"]);
				$coinflip_info.append('<br><b>Percentage:</b> ' + (parseFloat(coinflip["winner"]["percentage"]) * 100).toPrecision(9) + "%");
			}
			else {
				$("#coinflip-animated-winner-coin").hide();
			}

			let itemsPlayer1 = [];
			let itemsPlayer2 = [];

			if(player_left) {
				$("#coinflip-modal .player.p1 .avatar").attr("src", player_left["user"]["avatarfull"]);
				$("#coinflip-modal .player.p1 .coin").removeClass("t").removeClass("ct").addClass(player_left["side"]);
				$(".player1-value").html((player_left["tickets"]/100).toFixed(2) + ' <i class="fa fa-database"></i>');
				itemsPlayer1 = player_left["items"];
				itemsPlayer1.sort(function (a, b) {
					return b["tickets"] - a["tickets"];
				});
				$("#coinflip-modal .coinflip-player-1-name").html('<a href="/stats/user/'+player_left["user"]["steamid"]+'">'+ player_left["user"]["personaname"] +'</a>')
			}

			if(player_right) {
				$("#coinflip-modal .player.p2 .avatar").attr("src", player_right["user"]["avatarfull"]);
				$("#coinflip-modal .player.p2 .coin").removeClass("t").removeClass("ct").addClass(player_right["side"]);
				$(".player2-value").html((player_right["tickets"]/100).toFixed(2) + ' <i class="fa fa-database"></i>');
				itemsPlayer2 = player_right["items"];
				itemsPlayer2.sort(function (a, b) {
					return b["tickets"] - a["tickets"];
				});
				$("#coinflip-modal .coinflip-player-2-name").html('<a href="/stats/user/'+player_right["user"]["steamid"]+'">'+ player_right["user"]["personaname"] +'</a>')
			}
			else {

			}

			if(!player_left || !player_right) {
				let team = 2;
				let missingTeam = 1;
				if(player_left) {
					team = 1;
					missingTeam = 2;
				}
				$(".player" + team + "-chance").html("100%");
				$(".player" + missingTeam + "-chance").html("0%");
				$(".player" + missingTeam + "-value").html("0 <i class=\"fa fa-database\"></i>");
				if(coinflip['player_joining']) {
					$("#coinflip-modal .player.p" + missingTeam + " .avatar").attr("src", coinflip['player_joining']["avatarfull"]);
					$("#coinflip-modal .player.p" + missingTeam).removeClass("hidden");
				}
				else {
					$("#coinflip-modal .player.p" + missingTeam).addClass("hidden");
					$("#coinflip-modal .coinflip-player-" + missingTeam + "-name").html("");
                    $("#coinflip-modal .player1-value" + missingTeam).addClass("hidden");
                }
			}
			else
			{
				let totaltickets = parseInt(player_left["tickets"]) + parseInt(player_right["tickets"]);
				let chance1 = (parseInt(player_left["tickets"]) / totaltickets) * 100;
				let chance2 = (parseInt(player_right["tickets"]) / totaltickets) * 100;

				$(".player1-chance").html(chance1.toFixed(2) + "%");
				$(".player2-chance").html(chance2.toFixed(2) + "%");
				$(".player1-value").html((player_left["tickets"] / 100).toFixed(2) + ' <i class="fa fa-database"></i>');
				$(".player2-value").html((player_right["tickets"] / 100).toFixed(2) + ' <i class="fa fa-database"></i>');
			}

			let nbRowsItems = Math.max(itemsPlayer1.length, itemsPlayer2.length);
			$("#coinflip-modal .item").remove();
			$("#coinflip-modal .bg-transparent.emptyRow").remove();
			for(let i = 0; i < nbRowsItems; i++) {
				if (itemsPlayer1[i]) {
					$("#coinflip-modal #coinflip-modal-item-table").append(rowItemLayout(itemsPlayer1[i], itemsPlayer2[i]));
				}
			}

			if (coinflip["winner"] && coinflip["winner"]["finished"] + timefix + (coinflipWinnerTimer / 1000) <= Math.round(Date.now() / 1000)) {
				let winnerCoin = "t";
				if (coinflip["winner"]["user"]["steamid"] == coinflip["player_2"]["user"]["steamid"]) {
					winnerCoin = "ct";
				}
				$("#coinflip-coin").html(
					'<div class="coinflip-coin" style="width:100px;height:100px;margin: auto;">' +
					'<img class="front" src="/Template/Static/Image/coin_' + winnerCoin + '.png">' +
					'</div>'
				);
			}

			if (!coinflip["player_joining"] && (!coinflip["player_1"] || !coinflip["player_2"]) && !coinflip["winner"]) {
				$("#coinflip-coin").html("");
			}

			assignPageLoad($("#coinflip-modal"))
		}

		let totalValue = 0;
		let itemlist = [];
		let $row = $("#coinflip-" + coinflipID);

		if(player_left) {
			$row.find(".player.p1").html('<img src="' + player_left["user"]["avatarmedium"] + '" class="avatar" title="' + player_left["user"]["personaname"] + '"><div class="coin '+player_left["side"]+'"></div>');

			itemlist = itemlist.concat(player_left["items"]);
			totalValue += parseInt(player_left['tickets']);
		}
		else {
			$row.find(".player.p1").html('');
		}

		if(player_right || coinflip["player_joining"]) {
			let player = player_right ? player_right['user'] : coinflip["player_joining"];
			$row.find(".player.p2").html('<img src="' + player["avatarmedium"] + '" class="avatar" title="' + player["personaname"] + '"><div class="coin '+ (player_left["side"] == "t" ? "ct" : "t") +'"></div>');
			if(player_right) {
				itemlist = itemlist.concat(player_right["items"]);
				totalValue += parseInt(player_right['tickets']);
			}
		}
		else {
			$row.find(".player.p2").html('');
		}

		itemlist.sort(function (a, b) {
			return b["tickets"] - a["tickets"];
		});

		let $items = $row.find(".items").html("");
		for (i = 0; i < Math.min(itemlist.length, 4); i++) {
			let $item = $(document.createElement("img")).attr({
				"src": (itemlist[i]["image"] ? itemlist[i]["image"] : itemlist[i]["image"]), "class": "item",
				"title": itemlist[i]["market_name"] + " - " + toPriceFormat(itemlist[i]["tickets"] / 100)
			});

			$items.append($item);
		}
		if (itemlist.length > 4) {
			$items.append('<span style="margin-left:15px;color:#aaa;font-size:14px;">+' + (itemlist.length - 4) + ' ITEMS</span>')
		}

		$row.find(".total-value").html(toPriceFormat(parseInt(player_left["tickets"]) / 100) + ' <span>$</span>');
		if((!coinflip['player_1'] || !coinflip['player_2']) && !coinflip['player_joining']) {
			$row.find(".value-needed").html(
				"Needs: " +
				toPriceFormat(Math.floor(totalValue * (1 - (deposit_range / 100))) / 100) +
				" - " +
				toPriceFormat(Math.floor(totalValue * (1 + (deposit_range / 100))) / 100)
			);
		}
		else
		{
			$row.find(".value-needed").html('');
		}
		let $join = $row.find(".join");
		let $progress = $row.find(".in-progress");
		let $timer = $row.find(".timer");

		if (coinflip["winner"] && coinflip["winner"]["finished"] + timefix + (coinflipWinnerTimer / 1000) <= Math.round(Date.now() / 1000)) {
			$join.hide();
			$timer.addClass("coinflip-disabled").removeClass("coinflip-finished");
			$timer.show().html(
				'<img style="width:32px;height:32px" src="/Template/Static/Image/coin_' + (coinflip["winner"]["user"]["userID"] == coinflip["player_1"]["user"]["userID"] ? "t" : "ct") + '.png">'
			);
		}
		else if (coinflip["winner"] && coinflip["winner"]["finished"] + timefix + (coinflipWinnerTimer / 1000) > Math.round(Date.now() / 1000)) {
			CH.animateWinner(coinflipID);
		}
		else if (coinflip["player_1"] && coinflip["player_2"]) {
			$join.hide();
			$timer.removeClass("coinflip-disabled").addClass("coinflip-finished");
			$timer.show().html("");
		}
		else if (coinflip["player_joining"]) {
			$join.hide();
			$timer.addClass("coinflip-disabled").removeClass("coinflip-finished");
			$timer.show().html("");
		}
		else {
			$join.show().unbind().bind("click", joinfunction);
			$progress.hide();
			$timer.hide();
		}

		$row.find(".view").unbind().bind("click", function () {
			CH.openCoinflipWindow(coinflipID);
		});

		if (coinflip["winner"]) {
			CH.setCoinflipWinner(coinflipID, coinflip["winner"]);
		}
	}

	function getCoinflip(coinflipID) {
		for (let i = 0; i < games.length; i++) {
			if (games[i]["coinflipID"] != coinflipID) {
				continue;
			}

			return games[i];
		}
	}

	function setCoinflip(coinflipID, key, value) {
		for (let i = 0; i < games.length; i++) {
			if (games[i]["coinflipID"] != coinflipID) {
				continue;
			}

			games[i][key] = value;
		}
	}

	function updateTimer() {
		for (let i = 0; i < games.length; i++) {
			let timeleft;
			let game = games[i];
			let $row = $("#coinflip-" + game["coinflipID"]);

			let $join = $row.find(".join");
			let $timer = $row.find(".timer");
			let $timer_window = $("#coinflip-coin");

			if (game['player_1'] && game["player_2"]) {
				$join.hide();
				$timer.removeClass("coinflip-disabled").addClass("coinflip-finished");

				if (game["winner"]) {
					continue;
				}

				timeleft = 10 - parseInt(Date.now() / 1000 - timefix - Math.max(game["player_2"]["joined"], game["player_1"]["joined"]));
				if (timeleft <= 0) {
					timeleft = 0;
				}
				if($timer.find(".timer-starting").length === 0) {
					$timer.html('<canvas class="timer-starting" width="38" height="38"></canvas>');
					let $join = $row.find(".join");
					let $wait = $row.find(".waiting");
					let $progress = $row.find(".in-progress");

					$join.hide();
					$wait.show();
					$progress.hide();
					$timer.find(".timer-starting").drawArc({
						strokeStyle: '#0c0',
						strokeWidth: 2,
						x: 19, y: 19,
						radius: 17,
						start: (1 - (timeleft / 10)) * 360, end: 360,
						layer: true,
						name: "arcTimer"
					}).animateLayer("arcTimer", {
						start: 360,
						end: 360
					}, timeleft*1000, "linear");

					$timer.find(".timer-starting").drawText({
						fillStyle: '#0c0',
						fontSize: '16px',
						text: timeleft,
						x: 19, y: 19,
						align: 'center',
						maxWidth: 38,
						layer: true,
						name: "timeLeft"
					});
				}
				if (openCoinflipID === game["coinflipID"]) {
					if($timer_window.find(".timer-starting").length === 0) {
						$timer_window.html('<canvas class="timer-starting" width="100" height="100"></canvas>');
						$timer_window.find(".timer-starting").drawArc({
							strokeStyle: '#0c0',
							strokeWidth: 2,
							x: 50, y: 50,
							radius: 48,
							start: (1 - (timeleft / 10)) * 360, end: 360,
							layer: true,
							name: "arcTimer"
						}).animateLayer("arcTimer", {
							start: 360,
							end: 360
						}, timeleft*1000);
						$timer_window.find(".timer-starting").drawText({
							fillStyle: '#0c0',
							fontSize: '30px',
							text: timeleft,
							x: 50, y: 50,
							align: 'center',
							maxWidth: 100,
							layer: true,
							name: "timeLeft"
						});
					}
					$timer_window.find(".timer-starting").setLayer("timeLeft", {
						text: timeleft
					}).drawLayers();
				}
				$timer.find(".timer-starting").setLayer("timeLeft", {
					text: timeleft
				}).drawLayers();

			}
			else if (game["player_joining"]) {
				$join.hide();
				$timer.addClass("coinflip-disabled").removeClass("coinflip-finished");

				if (game["winner"]) {
					continue;
				}

				timeleft = 90 - parseInt(Date.now() / 1000 - timefix - game["disabletime"]);
				if (timeleft <= 0) {
					timeleft = 0;
				}
				if($timer.find(".timer-joining").length === 0) {
					$timer.html('<canvas class="timer-joining" width="38" height="38"></canvas>');
					let $join = $row.find(".join");
					let $wait = $row.find(".waiting");
					let $progress = $row.find(".in-progress");

					$join.hide();
					$wait.hide();
					$progress.show();
					$timer.find(".timer-joining").drawArc({
						strokeStyle: '#c00',
						strokeWidth: 2,
						x: 19, y: 19,
						radius: 17,
						start: (1 - (timeleft / 90)) * 360, end: 360,
						layer: true,
						name: "arcTimer"
					}).animateLayer("arcTimer", {
						start: 360,
						end: 360
					}, timeleft*1000);
					$timer.find(".timer-joining").drawText({
						fillStyle: '#c00',
						fontSize: '16px',
						text: timeleft,
						x: 19, y: 19,
						align: 'center',
						maxWidth: 38,
						layer: true,
						name: "timeLeft"
					});
				}
				if (openCoinflipID === game["coinflipID"]) {
					if($timer_window.find(".timer-joining").length === 0) {
						$timer_window.html('<canvas class="timer-joining" width="100" height="100"></canvas>');
						$timer_window.find(".timer-joining").drawArc({
							strokeStyle: '#c00',
							strokeWidth: 2,
							x: 50, y: 50,
							radius: 48,
							start: (1 - (timeleft / 90)) * 360, end: 360,
							layer: true,
							name: "arcTimer"
						}).animateLayer("arcTimer", {
							start: 360,
							end: 360
						}, timeleft*1000);
						$timer_window.find(".timer-joining").drawText({
							fillStyle: '#c00',
							fontSize: '30px',
							text: timeleft,
							x: 50, y: 50,
							align: 'center',
							maxWidth: 100,
							layer: true,
							name: "timeLeft"
						});
					}
					$timer_window.find(".timer-joining").setLayer("timeLeft", {
						text: timeleft
					}).drawLayers();
				}
				$timer.find(".timer-joining").setLayer("timeLeft", {
					text: timeleft
				}).drawLayers();
			}
		}
	}

	setInterval(updateTimer, 1000);
}

let JackpotCanvas = {};
JackpotCanvas.players = [];
JackpotCanvas.totalValue = 0;
JackpotCanvas.colors = ["#F80000", "#24715E", "#439E00", "#fc1dca", "#004BF5", "#9600FF",
	"#00C54F", "#21F0C0", "#F4530B", "#FFCC00", "#167EFF", "#3CDE0A", "#F2075A",
	"#CA430E", "#1591AB", "#601098", "#2D5796", "#B99E06", "#980202", "#4509A8"];
JackpotCanvas.addPlayer = function(steamId, name, bet, noAnimation) {
	JackpotCanvas.players.push({
		steamId: steamId,
		name: name,
		bet: bet
	});
	JackpotCanvas.totalValue += bet;
	JackpotCanvas.draw(noAnimation);
};

JackpotCanvas.reset = function(callback) {
	JackpotCanvas.initTimer();
	if(callback) {callback();}
};

JackpotCanvas.initTimer = function() {

	let width = $("#testProgress").width();
	let widthBG = $(".yellowBG").width();
	if (widthBG === 0) {
        $(".yellowBG").animate({
            width: width
        }, 1000, "linear")
    }

};

JackpotCanvas.startTimer = function() {
	JackpotCanvas.rollAt = new Date();
	JackpotCanvas.rollAt = JackpotCanvas.rollAt.setSeconds(JackpotCanvas.rollAt.getSeconds() + 90);
	let timeLeft = Math.max(JackpotCanvas.rollAt - Date.now(), 0);
};

function UpgradeHandler() {
	let self = this;

	let deposititems = [];
	let depositvalue = 0;

	let upgradeitem = null;
	let upgradevalue = 0;

	let marketitems = [];

	let audio = {
		"win": new Audio('/Template/Static/Audio/upgrade_win.mp4'),
		"loose": new Audio('/Template/Static/Audio/upgrade_loose.mp4')
	};

	WS.on("onConnectionCreate", function () {
		WS.emit("set_gamemode", "upgrade");
		updateUpgradeItems();

        $.ajax({
            method: 'GET',
            dataType: 'json',
            url: '/upgrade/jsondata',
            success: function (data) {
                let i;

				if(!data["success"]) {
					return;
				}

				deposititems = data["deposit"];
				marketitems = data["market"];

				// TODO
				$("#activity-stream .col").html("");
				for(let i = 0; i < data["history"].length; i++) {
					$("#activity-stream .col").append(self.activityStreamLayout(data["history"][i]));
				}

				updateUpgradeItems();
				renderMarket();

				if(data["withdraw"].length > 0) {
					$("#upgrade-withdraw-history-table").show();

					let $table = $("#upgrade-withdraw-history-table tbody").html("");
					for(i = 0; i < data["withdraw"].length; i++) {
						let trade = data["withdraw"][i];

						$table.append(self.withdrawTradeLayout(trade));

						updateTradeStatus(trade["id"], trade["status"])
					}
				}
		    }
		});
	});

	WS.on("onUpgradeRefundError", function (data) {
		showError("We currently cannot refund this item. Please contact the support or try it again later")
	});

	WS.on("onUpgradeWithdrawCreate", function (data) {
		$("#upgrade-withdraw-history-table").show();

		$("#upgrade-withdraw-history-table tbody").prepend(self.withdrawTradeLayout(data));
		updateTradeStatus(data["id"], data["status"])
	});

	WS.on("onUpgradeWithdrawStatusUpdate", function (data) {
		updateTradeStatus(data["id"], data["status"])
	});

	WS.on("onUpgradePublish", function (data) {
		$("#activity-stream .col").prepend(self.activityStreamLayout(data));
	});

	WS.on("onUpgradeItemDeposit", function (data) {
		deposititems = data;
		updateUpgradeItems();
	});

	WS.on("onUpgradeSuccess", function (data) {
		if(data["won"]) {
			deposititems = [data["item"]];
			upgradeitem = null;
			upgradevalue = 0;

			self.animateUpgrade("win");
		}
		else {
			deposititems = [];
			self.animateUpgrade("loose");
		}
	});

	WS.on("onUpgradeError", function (data) {
		showError(g_lang[data] ? g_lang[data] : g_lang["invalid_deposit"]);
	});

	let min_deposit_value = 0;
	let max_deposit_value = 0;
	let max_deposit_items = 1;
	let min_item_value = 0;

	let upgrade_fee = 0;
	this.setUpgradeFee = function (fee) {
		upgrade_fee = parseFloat(fee);
		updateUpgradeItems();
	};

	this.setDepositSettings = function(_min_deposit_value, _max_deposit_value, _max_deposit_items, _min_item_value) {
		min_deposit_value = _min_deposit_value;
		max_deposit_value = _max_deposit_value;
		max_deposit_items = _max_deposit_items;
		min_item_value = _min_item_value;
	};

	this.showDeposit = function () {
		let max_dv = max_deposit_value;

		if(upgradevalue > 0) {
			max_dv = Math.min(max_dv, upgradevalue / 100);

			if(max_dv == 0) {
				max_dv = upgradevalue / 100;
			}
		}

		if(max_dv - depositvalue / 100 > 0) {
			max_dv = max_dv - depositvalue / 100;
		}

		TM.openDepositWindow("upgrade", "deposit", min_deposit_value, max_dv, max_deposit_items - deposititems.length, min_item_value)
	};

	this.showMarket = function () {
		renderMarket();
		$("#upgrade-market-modal").modal("show");
	};

	this.activityStreamLayout = function(upgradedata) {
		if(!upgradedata["won"]) {
			return '';
		}

		return '' +
			'<div class="activity animated bounceIn">' +
			'    <img class="prize" src="' + upgradedata["item"]['image'] + '" title="' + upgradedata["item"]['market_name'] + '">' +
			'</div>'
	};

	this.withdrawTradeLayout = function (trade) {
		return '<tr id="upgrade-withdraw-'+trade["id"]+'">' +
			'<td>#'+trade["id"]+'</td>' +
			'<td>'+trade["item"]["market_name"]+'</td>' +
			'<td><img src="'+trade["item"]["image"]+'" style="width:32px;background:#0b0b0b;padding:2px;"></td>' +
			'<td>'+toPriceFormat(trade["tickets"] / 100)+' <i class="fa fa-database"></i></td>' +
			'<td class="upgrade-tradestatus text-green-gradient"></td>' +
			'<td>' + (new Date(trade["dateline"] * 1000)).toLocaleString() + '</td>' +
			'</tr>';
	};

	this.autoselectItem = function(multiplier) {
		if(depositvalue == 0) {
			return;
		}

		upgradeitem = null;
		upgradevalue = 0;
		for(let i = 0; i < marketitems.length; i++) {
			let marketitem = marketitems[i];
			let itemprice = parseFloat(marketitem["price"]) * 100;

			if(itemprice / depositvalue > 1 && itemprice / depositvalue > multiplier - 0.05 && itemprice / depositvalue < multiplier + 0.05) {
				upgradeitem = marketitem;
				upgradevalue = itemprice;

				break;
			}
		}

		updateUpgradeItems();
	};

	this.withdraw = function () {
		if(deposititems.length == 0) {
			return;
		}

		deposititems = [];
		depositvalue = 0;

		updateUpgradeItems();

		WS.emit("withdraw_upgrade_items", null);
	};

	this.upgrade = function () {
		if(deposititems.length == 0) {
			return;
		}

		WS.emit("upgrade", {
			"appID": upgradeitem["appID"], "contextID": upgradeitem["contextID"], "market_name": upgradeitem["market_name"],
			"tickets": upgradevalue, "roll_type": "under"
		});
	};

	this.refundTrade = function (tradeID) {
		WS.emit("refund_upgrade_withdraw", tradeID);
	};

	this.updateItems = updateUpgradeItems;

	let animationTimer = 0;

	this.animateUpgrade = function (type) {
		let animationstep = 1;

		audio[type].play();

		function animate() {
			if(animationstep <= 15) {
				$("#upgrade-animation").attr("src", "/Template/Static/Image/upgrade_animation/anim_"+animationstep+".png");

				animationstep++;
				setTimeout(animate, 300);
			}
			else if(animationstep <= 22) {
				if(animationstep % 2 == 0) {
					$("#upgrade-animation").attr("src", "/Template/Static/Image/upgrade_animation/upgrade_"+type+"_1.png");
				}
				else {
					$("#upgrade-animation").attr("src", "/Template/Static/Image/upgrade_animation/upgrade_"+type+"_2.png");
				}

				animationstep++;
				animationTimer = setTimeout(animate, 300);
			}
			else {
				$("#upgrade-button-continue").show();
			}
		}

		animate();
	};

	function updateUpgradeItems() {
		let i;

		// update middle part
		$("#upgrade-button-continue").hide();
		$("#upgrade-animation").attr("src", "/Template/Static/Image/upgrade_animation/anim_0.png");
		clearTimeout(animationTimer);

		depositvalue = 0;
		for(i = 0; i < deposititems.length; i++) {
			depositvalue += parseInt(deposititems[i]["tickets"])
		}

		// deposititems
		for(i = 1; i <= 6; i++) {
			if(deposititems.length >= i) {
				let item = deposititems[i - 1];

				$("#upgrade-deposit-item-" + i + "-image").html('<img src="'+item["image"]+'" />');
				$("#upgrade-deposit-item-" + i + "-price").html(toPriceFormat(item["tickets"] / 100));
			}
			else {
				$("#upgrade-deposit-item-" + i + "-image").html('');
				$("#upgrade-deposit-item-" + i + "-price").html('');
			}
		}

		$("#upgrade-deposit-container .upgrade-select-totalprice-value").html(toPriceFormat(depositvalue / 100));

		if(deposititems.length == 0) {
			$("#upgrade-deposit-container .upgrade-select-text").show();
			$("#upgrade-deposit-container .upgrade-select-image").show();
		}
		else {
			$("#upgrade-deposit-container .upgrade-select-text").hide();
			$("#upgrade-deposit-container .upgrade-select-image").hide();
		}

		// market item

		if(upgradeitem) {
			$("#upgrade-market-item-image").html('<img src="'+upgradeitem["image"]+'" />');

			$("#upgrade-market-container .upgrade-select-text").hide();
			$("#upgrade-market-container .upgrade-select-image").hide();
		}
		else {
			$("#upgrade-market-item-image").html('');

			$("#upgrade-market-container .upgrade-select-text").show();
			$("#upgrade-market-container .upgrade-select-image").show();
		}

		$("#upgrade-market-container .upgrade-select-totalprice-value").html(toPriceFormat(upgradevalue / 100));

		// general
		if(upgradevalue > depositvalue && depositvalue > 0) {
			let a = upgradevalue - depositvalue;
			let b = depositvalue;
			let f = (upgrade_fee / 100) * depositvalue;
			$("#upgrade-percentage").html(toPriceFormat(((b - f) / (a + b)) * 100))
		}
		else {
			$("#upgrade-percentage").html("0.00")
		}
	}

	$("#upgrade-market-items").slimScroll({
		height: "500px",
		width: '100%'
	}).css("padding", 0);

	function renderMarket() {
		let $container = $("#upgrade-market-modal .items-container").html("");

		for(let i = 0; i < marketitems.length; i++) {
			if(marketitems[i]["price"] * 100 < depositvalue) {
				continue;
			}

			let split = $("#upgrade-market-search").val().split(" ");
			let match = true;
			for(let n = 0; n < split.length; n++) {
				if(!split[n]) {
					continue;
				}

				if(marketitems[i]["market_name"].toLowerCase().search(split[n].toLowerCase()) == -1) {
					match = false;
				}
			}

			if(!match) {
				continue;
			}

			let $item = $(
				'<div class="item">' +
                    '<div class="item-bordered">' +
                        '<span class="value">' + toPriceFormat(marketitems[i]["price"]) + '</span>' +
                        '<img src="' + marketitems[i]["image"] + '">' +
                    '</div>' +
                    '<span class="name">' + marketitems[i]["market_name"] + '</span>' +
				'</div>');

			$container.append($item);
			if (!((i+1) % 6)) {
                $container.append('<div class="clearfix"></div>')
            }

			(function ($elem, itemdata) {
				$elem.click(function () {
					upgradeitem = itemdata;
					upgradevalue = parseInt(Math.round(itemdata["price"] * 100));
					$("#upgrade-market-modal").modal("hide");
					updateUpgradeItems();
				});
			})($item, marketitems[i]);
		}
	}

	function updateTradeStatus(tradeID, status) {
		let tradestatus;

		if(status == 1) {
			tradestatus = "Withdraw requested";
		}
		else if(status == 2) {
			tradestatus = "Item needs to be bought (please wait a couple of minutes)"
		}
		else if(status == 3) {
			tradestatus = "Withdraw was sent successfully";
		}
		else if(status == 5) {
			tradestatus = "There was an error while sending the item to you. Please contact the support or if you want to get alternative items click <a href='javascript:UH.refundTrade(\"" + tradeID + "\");'>here</a> to refund";
		}
		else if(status == 6) {
			tradestatus = "withdraw refunded";
		}
		else {
			tradestatus = "unknown"
		}

		$("#upgrade-withdraw-"+tradeID+" .upgrade-tradestatus").html(tradestatus);
	}

	let searchtimer = 0;
	$("#upgrade-market-search").bind("keyup", function (e) {
		clearTimeout(searchtimer);
		searchtimer = setTimeout(function () {
			renderMarket()
		}, 500);
	});
}

// ajax pageload
let pageloadajax = null;
function gotoPage(url, history)
{
	$(".modal").modal("hide");

	let $jackpot_container = $("#main-jackpot");
	let $jackpot_container_small = $("#main-jackpot-small");
	let $coinflip_container = $("#main-coinflip");
	let $upgrade_container = $("#main-upgrade");
	let $text_container = $("#main-text");

	let $nav = $("nav.navbar");
	$nav.find("a").each(function ()
	{
		let $a = $(this);

		if(url == $a.attr("href"))
		{
			$a.parent().addClass("active");
		}
		else if($a.parent().hasClass("active"))
		{
			$a.parent().removeClass("active");
		}
	});

	if(pageloadajax)
	{
		pageloadajax.abort();
	}

	if(url == "/") {
		$jackpot_container_small.hide()
		$coinflip_container.hide();
		$upgrade_container.hide();
		$text_container.hide();
		$jackpot_container.show();

		if(history) {
			window.history.pushState({
				"title": "High Rollers - CSGOFrog.com",
				"url": url
			}, "", url);
		}

		$("title").html("High Rollers - CSGOFrog.com");

		return;
	}
	else if(url == "/small") {
		$jackpot_container.hide();
		$coinflip_container.hide()
		$upgrade_container.hide();
		$text_container.hide();
		$jackpot_container_small.show();

		if(history) {
			window.history.pushState({
				"title": " Small Jackpot- CSGOFrog.com",
				"url": url
			}, "", url);
		}

		$("title").html("Small Jackpot - CSGOFrog.com");

		return;
	}
	else if(url == "/coinflip") {
		$jackpot_container.hide();
		$jackpot_container_small.hide()
		$upgrade_container.hide();
		$text_container.hide();
		$coinflip_container.show();

		if(history) {
			window.history.pushState({
				"title": " Coinflip- CSGOFrog.com",
				"url": url
			}, "", url);
		}

		$("title").html("Coinflip - CSGOFrog.com");

		return;
	}
	else if(url == "/upgrade") {
		$jackpot_container.hide();
		$jackpot_container_small.hide()
		$coinflip_container.hide();
		$text_container.hide();
		$upgrade_container.show();

		if(history) {
			window.history.pushState({
				"title": "Upgrade - CSGOFrog.com",
				"url": url
			}, "", url);
		}

		$("title").html("Upgrade - CSGOFrog.com");

		return;
	}

	$jackpot_container.hide();
	$jackpot_container_small.hide()
	$coinflip_container.hide();
	$upgrade_container.hide();
	$text_container.html('<div class="row title-band mr-0"><div class="col"><img src="/Template/Static/Image/loading-black.gif" /></div></div>').show();

	let urlSplit = url.split("?");
	let ajaxUrl = (urlSplit[0] == "/") ? "/main.tpl" : urlSplit[0] + ".tpl";

	ajaxUrl += "?ts=" + (Date.now());
	if(urlSplit.length > 1) {
		ajaxUrl +=  "&" + urlSplit[1];
	}

	pageloadajax = $.ajax(ajaxUrl, {dataType: "html"}).done(function (html) {
		pageloadajax = null;

		$("body").scrollTop(0);

		if (typeof ga == 'function') {
			ga('send', 'pageview', url);
		}

		$text_container.html(html);
		assignPageLoad($text_container);

		if(history) {
			window.history.pushState({
				"title": $("title").html(),
				"url": url
			}, "", url);
		}
	}).fail(function () {
		pageloadajax = null;
		showError("Error while loading the page!")
	});
}

function onPageLoad(e)
{
	if (e.which == 2)
	{
		return true;
	}

	let  $this = $(this);
	if($this.attr("href").startsWith("http") || $this.attr("href").startsWith("javascript:")) {
		return true;
	}

	if($this.attr("target") == "_blank") {
		return true;
	}

	if($this.attr("href") == "#" || $this.attr("data-ignore")) {
		return true;
	}

	gotoPage($this.attr("href"), true);

	return false;
}

function assignPageLoad($div)
{
	if (!$div)
	{
		return;
	}

	if (typeof $div.attr("href") != "undefined")
	{
		$div.unbind("click", onPageLoad).bind("click", onPageLoad);
	}
	else
	{
		$div.find("a").unbind("click", onPageLoad).bind("click", onPageLoad);
	}
}

window.onpopstate = function (e) {
	if (e.state) {
		gotoPage(e.state["url"], false);
	}
};

var CM, WS, TM, JH, CH, UH;
$( window ).scroll(function(e) {
	var scroll = $(document).scrollTop();
	var nav = $("nav").height();
	var stream = $("#activity-stream").height();

});
$(document).ready(function () {

	let width = $("#testProgress").width();
	$(".yellowBG").animate({
		width: width
	}, 1000, "linear");

	$(".nav-link").click(function() {
		if ($(this).attr("href") === '/upgrade') {
			$("#activity-stream").removeClass("hidden")
		} else {
			$("#activity-stream").addClass("hidden")
		}
	});
	setTimeOffset();
	assignPageLoad($("body"));

	// init classes
	WS = new WebsocketHandler(g_serverip, g_serverauth);

	TM = new TradeManager();

	JH = new JackpotHandler();
	CH = new CoinflipHandler();
	UH = new UpgradeHandler();

	CM = new ChatManager();

	WS.connect();

	// JackpotCanvas.initialDraw();
	JackpotCanvas.reset();

	// global websocket events
	WS.on("onAuthentication", function (data) {
		g_steamid = data['steamID'];
	});

	WS.on("onOnlineCounterUpdate", function (data) {
		$("#online-count").html(data)
	});

	// init modal animations
	let $modal = $(".modal");
	$modal.on('show.bs.modal', function (e) {
		$(this).find('.modal-dialog').attr('class', 'modal-dialog flipInX animated');
	});
	$modal.on('hide.bs.modal', function (e) {
		$(this).find('.modal-dialog').attr('class', 'modal-dialog zoomOutUp animated');
		$("#winner-modal").fireworks("destroy");
	});

	// jackpot scroller
	$(".items-scroller .right-arrow").click(function() {
		$(this).parent().find(".items").animate( { scrollLeft: '+=200' }, 500);
	});
	$(".items-scroller .left-arrow").click(function() {
		$(this).parent().find(".items").animate( { scrollLeft: '-=200' }, 500);
	});

	$("#deposit-modal .items").slimScroll({
		height: "400px",
		width: '100%'
	}).css("padding", 0);

	// coinflip events
	$(".choose-side .side").click(function() {
		$(".choose-side .side").removeClass("selected");
		TM.updateGameID("create:" + $(this).data("side"));
		$(this).addClass("selected");
	});
});

function toPriceFormat(number) {
	var str = parseFloat(number) + "";
	var parts = str.split(".");
	if (parts.length == 1) {
		return parts[0] + ".00"
	}
	else if (parts.length == 2) {
		if (parts[1].length == 1) {
			return parts[0] + "." + parts[1] + "0";
		}
		else {
			return parseFloat(number).toFixed(2);
		}
	}
	else {
		return "0.00";
	}
}
