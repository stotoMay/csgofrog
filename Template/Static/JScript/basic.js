function setTimeOffset() {
    let now = new Date();
    let later = new Date();

    let jan = new Date(now.getFullYear(), 0, 1);
    let jul = new Date(later.getFullYear(), 6, 1);
    let offset = Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());

    later.setTime(now.getTime() + 365 * 24 * 60 * 60 * 1000);
    document.cookie = 'timezoneOffset=' + (-60 * offset) + '; expires=' + later + '; path=/';
}

function getCookie(name) {
    let value = "; " + document.cookie;
    let parts = value.split("; " + name + "=");
    if (parts.length == 2) return decodeURIComponent(parts.pop().split(";").shift());
}

function setCookie(name, value, expire) {
    if (typeof expire == "undefined") {
        document.cookie = name + '=' + value + '; path=/';
    }
    else {
        var date = new Date();
        date.setTime(date.getTime() + expire * 1000);
        document.cookie = name + '=' + encodeURIComponent(value) + '; expires=' + date + '; path=/';
    }
}

function toPriceFormat(number) {
    var str = parseFloat(number) + "";
    var parts = str.split(".");
    if (parts.length == 1) {
        return parts[0] + ".00"
    }
    else if (parts.length == 2) {
        if (parts[1].length == 1) {
            return parts[0] + "." + parts[1] + "0";
        }
        else {
            return parseFloat(number).toFixed(2);
        }
    }
    else {
        return "0.00";
    }
}

function print_call_stack() {
  var stack = new Error().stack;
  console.log("PRINTING CALL STACK");
  console.log( stack );
}

function showError(txt) {
    new Noty({
        type: 'error',
        text: txt,
    }).show();
}

function showInfo(txt) {
    new Noty({
        type: 'success',
        text: txt,
    }).show();
}

function getLevel(xp) {
    return parseInt((Math.sqrt(parseInt(xp) + 2500) - 45) / 5);
}

function getXP(level) {
    return parseInt(25 * Math.pow(level, 2) + 450 * level - 475);
}

function getLevelColor(level)
{
    let color;

    if(level < 10) {
        color = "#fe3838";
    } else if(level < 20){
		color = '#0089ec';
	} else if(level < 30){
		color = '#2ecc71';
	} else if(level < 40){
		color = '#f39c12';
	} else if(level < 50){
		color = '#2c900b';
	} else if(level < 60){
		color = '#c0c0c0';
	} else if(level < 70){
		color = '#5f0004';
	} else if(level < 80){
		color = '#75018a';
	} else if(level < 90){
		color = '#8a015a';
	} else if(level < 100){
		color = '#b74747';
	} else if(level < 200){
		color = '#16a085';
	} else if(level < 300){
		color = '#2c3e50';
	} else if(level < 400){
		color = '#9eb138';
	} else if(level < 500){
		color = '#d35400';
	} else {
		color = '#ffd700';
	}

    return color+"";
}

function EventManager() {
    let listeners = {};

    this.addListener = function (name, fn) {
        if (!listeners[name] || listeners[name].length == 0) {
            listeners[name] = [];
        }

        listeners[name].push(fn);
    };

    this.removeListener = function (name, fn) {
        if (typeof fn == "undefined") {
            listeners[name] = [];
        }
        else {
            var foundAt = -1;
            for (var i = 0, len = listeners.length; i < len && foundAt === -1; i++) {
                if (listeners[i] === fn) {
                    foundAt = i;
                }
            }

            if (foundAt >= 0) {
                listeners.splice(foundAt, 1);
            }
        }
    };

    this.fire = function (name, data) {
        if (!listeners[name]) {
            return;
        }

        for (var i = 0; i < listeners[name].length; i++) {
            listeners[name][i](data);
        }
    }
}

function WebsocketHandler(serverIP, authcode) {
    let eventManager = new EventManager();

    let websocket;

    let running = false;
    let wasclosed = false;
    let printed = false;

    let errorcount = 0;

    this.on = function (name, fn) {
        eventManager.addListener(name, fn);
    };

    this.fire = function (name, args) {
        eventManager.fire(name, args);
    };

    this.remove = function (name, fn) {
        eventManager.removeListener(name, fn)
    };

    this.emit = function (a, b) {
        writeMessage(a, b);
    };

    this.connect = function () {
        init();
    };

    this.close = function() {
        websocket.close();
    };

    function writeMessage(ac, da) {
        if (typeof da == "undefined") {
            da = null;
        }

        if (running) {
            websocket.send(JSON.stringify({"action": ac, "data": da}));
        }
        else {
            setTimeout(function () {
                writeMessage(ac, da);
            }, 200)
        }
    }

    function init() {
        websocket = new SockJS(serverIP, ["websocket", "xhr-streaming", "iframe-eventsource", "iframe-htmlfile", "xhr-polling", "iframe-xhr-polling", "jsonp-polling"]);
        websocket.onopen = function () {
            running = true;

            if (authcode) {
                writeMessage("auth", g_serverauth);
            }

            eventManager.fire("onConnectionCreate");

            errorcount = 0;
            wasclosed = false;
        };
        websocket.onmessage = function (ev) {
            let json = ev.data;
            eventManager.fire(json["action"], json["data"]);
        };
        websocket.onclose = function () {
            if (wasclosed === false) {
                eventManager.fire("onConnectionClose");
            }

            running = false;
            wasclosed = true;

            if (errorcount > 3 && !printed) {
                printed = true;
                showError(g_lang["lostconnection"]);
            }

            setTimeout(init, 1000);
            errorcount++;
        };
        websocket.onerror = function () {
            if (wasclosed === false) {
                eventManager.fire("onConnectionClose");
            }

            wasclosed = true;
            running = false;

            if (errorcount > 3 && !printed) {
                printed = true;
                showError(g_lang["lostconnection"]);
            }

            setTimeout(init, 10000);
            errorcount++;
        };
    }
}
