<script>
    $("title").html("{$title}");
</script>

<div class="row m-4">
    <div class="col-12 m-auto mt-4">
        <form method="post" action="/account">
            <table class="table table-vertical table-borderless">
                <tr>
                    <td style="max-width: 320px">OPSkins ExpressTrade Trade URL (<a target="_blank" href='https://trade.opskins.com/settings' data-ignore="1" style="color:#FCBB31;">What's my link?</a>)</td>
                    <td><input class="input-control" type="text" id="tradeurl" value="{$tradeurl}" name="tradeurl"></td>
                    <td><input type="submit" value="Update" class="button-wave button-yellow-gradient" style="max-width: 140px" >
                    </td>
                </tr>
            </table>
            <input type="hidden" name="token" value="{token()}">
        </form>

        <h1 class="mt-4">Level Progression</h1>

        <div class="row color-white">
            <div class="col text-center">
                Level <span id="level">0</span>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="progress-bar" style="height:20px;">
                    <div class="progress" id="level-progression">

                    </div>
                </div>
            </div>
        </div>
        <div class="row color-white">
            <div class="col text-left" id="level-xp">0</div>
            <div class="col text-right" id="level-totalxp">0</div>
        </div>

        <script>
            $(document).ready(function() {
                let xp = parseInt('{$xp}');

                let level = getLevel(xp);
                let xp_start = getXP(level);
                let xp_end = getXP(level + 1);
                let progression = (xp - xp_start) / (xp_end - xp_start);

                $("#level-progression").css("width", progression * 100 + "%");
                $("#level-xp").html(xp - xp_start);
                $("#level-totalxp").html(xp_end - xp_start);
                $("#level").html(level);
            });
        </script>

        <h1 class="mt-4">Last Transactions</h1>

        <table class="table-yellow">
            <thead>
            <tr>
                <th></th>
                <th>Transaction ID</th>
                <th>Value</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody>
            {foreach $transactions as $transaction}
                <tr>
                    <td style="text-align: center">
                        {if $transaction.mode == "deposit"}
                            DEPOSIT
                        {elseif $transaction.mode == "withdraw"}
                            WITHDRAW
                        {/if}
                    </td>
                    <td>#{$transaction.id}</td>
                    <td>{number_format(divide($transaction.value, 100))} <i class="fa fa-database"></i></td>
                    <td>{datetimeformat($transaction.dateline)}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>

        <div class="center-block mt-4">
            <ul class="pagination mx-auto">
                {if $transaction_page == 1}
                    <li class="page-item disabled"><a class="page-link">&laquo;</a></li>
                {else}
                    <li class="page-item"><a class="page-link" href="/account?transaction_page={sum($transaction_page, -1)}">&laquo;</a></li>
                {/if}
                <li class="page-item disabled"><a class="page-link">{$transaction_page}</a></li>
                {if count($transactions) < 15}
                    <li class="page-item disabled"><a class="page-link">&raquo;</a></li>
                {else}
                    <li class="page-item"><a class="page-link" href="/account?transaction_page={sum($transaction_page, 1)}">&raquo;</a></li>
                {/if}
            </ul>
        </div>
    </div>
</div>