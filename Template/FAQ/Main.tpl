<script>
    $("title").html("{$title}");
</script>

<div class="row m-4" id="coinflips-container">
    <div class="col-12 m-auto" style="color: white;">

        <h1>What is this?</h1>

        <p>CsGoFrog is a service, where participants pay in their items (skins), and when we have 100 skins in total the
            system randomly determines one winner, who gets all deposited items. The winner is determined randomly, the
            chance to win depends on the cost of skins you deposited.</p>

        <h1>How CsGoFrog works</h1>

        <p>The more items you bet and the more expensive they are, the more is the chance that you will get a
            jackpot!<br>Even if you invest just 1 <i class="fa fa-database"></i> you get a chance to win a
            jackpot!</p>

        <ul>
            <li>You add you items via the "Deposit items" button, which sends Trade to our bot. You can bet maximum
                    of 50 skins at once, all together worth not less than 1 <i class="fa fa-database"></i></li>
            <li>The deposited items are transferred into tickets, based on their price. For each cent of the price of
                    an item you get 1 ticket (1 cent - 1 ticket). Your chance to win depends on the number of
                    tickets.</li>
            <li>After reaching the threshold of 100 skins, we collect all issued tickets and randomly determine one
                    winner. However, the priority goes to those participants, who have more tickets than others do.</li>
        </ul>

        <h1>Rules and features</h1>

        <ol>
            <li>Maximum deposit - 50 items per trade. There is no upper limit of the price of an item and there is no
                minimal deposit.
            </li>
            <li>In order to develop our website and hold various contests we collect - 7% service fee from the cost of
                all items in each game.
            </li>
            <li>Every time you send your items, you agree with the Terms and Conditions of this website.</li>
            <li>We accept only items for VGO. In addition, we can guarantee the correct pricing of your items only if
                they are presented on the Steam Community Market. Otherwise, the price may be assessed incorrectly.
            </li>
            <li>It is prohibited to bet with souvenir items and souvenir sets; such trades will not be accepted.</li>
            <li>You are covered by a warranty to get your items within half an hour after the game is closed. Once this
                period expires we are not responsible for lost items.
            </li>

        </ol>

        <h1 id="fair">Provably Fair</h1>

        <table style="width:100%;">
            <tr>
                <td>RoundHash</td>
                <td>The round hash is the sha1 hashed version of the RoundToken and the WinnerPercentage. It can be seen
                    throughout the whole round, and it is used to verify that nothing was manipulated.
                </td>
            </tr>
            <tr>
                <td>RoundToken</td>
                <td>The round token is a token, generated at the beginning of the round but only shown at the end. The
                    token combinded with the percentage is the unhashed value of the hash.
                </td>
            </tr>
            <tr>
                <td>WinnerPercentage</td>
                <td>The winner percentage is a percentage between 0-100%. Using the winner percentage, the winning
                    ticket is calculated. (reminder: the person owning the winning ticket wins the round)
                </td>
            </tr>
            <tr>
                <td>TotalTickets</td>
                <td>The tickets of all players of a certain round combined (reminder: $0.01 = 1 ticket)</td>
            </tr>
        </table>
        <br>

        <div style="color:#d1d1d1;background:#0b0b0b;display: block;border: 1px dashed #97979D;padding: 10px;margin-bottom: 10px;font-family: monospace;font-size: 12px;">
            WinnerTicket <span style='color:#d2cd86; '>=</span>
            <span style='color:#e66170; font-weight:bold; '>ceil</span><span style='color:#d2cd86; '>(</span>TotalTickets
            <span style='color:#d2cd86; '>*</span> <span style='color:#d2cd86; '>(</span>WinnerPercentage
            <span style='color:#d2cd86; '>/</span>
            <span style='color:#008c00; '>100</span><span style='color:#d2cd86; '>)</span><span style='color:#d2cd86; '>)</span>
        </div>

        <div style="color:#d1d1d1;background:#0b0b0b;display: block;border: 1px dashed #97979D;padding: 10px;margin-bottom: 10px;font-family: monospace;font-size: 12px;">
            RoundHash <span style='color:#d2cd86; '>=</span> sha1<span style='color:#d2cd86; '>(</span>RoundToken
            <span style='color:#d2cd86; '>+</span>
            <span style='color:#02d045; '>"</span><span style='color:#00c4c4; '>:</span><span style='color:#02d045; '>"</span>
            <span style='color:#d2cd86; '>+</span> <span style='color:#d2cd86; '>(</span>WinnerPercentage
            <span style='color:#d2cd86; '>/</span>
            <span style='color:#008c00; '>100</span><span style='color:#d2cd86; '>)</span><span style='color:#d2cd86; '>)</span>
        </div>

        <h1>Support</h1>

        <p>Please, before asking your question to our support service be sure to read this FAQ, where you most likely
            will find answers to your questions</p>

        <div class="faq-section">

            <div class="faq-item">
                <div class="question">
                    <span>Q:</span> I did not get all items after I won!
                </div>
                <div class="answer">
                    <span>A:</span> We take a small commission from each game 7%
                </div>
            </div>

            <div class="faq-item">
                <div class="question">
                    <span>Q:</span> I did not get my prize!
                </div>
                <div class="answer">
                    <span>A:</span> Sending items may take up to 30 minutes (depending on the workload of our bots)
                </div>
            </div>

            <div class="faq-item">
                <div class="question">
                    <span>Q:</span> How can I contact you ?
                </div>
                <div class="answer">
                    <span>A:</span> You can contact us on Facebook and Twitter :<br />
                    <a href="https://twitter.com/CSGOFROGJACKPOT">https://twitter.com/CSGOFROGJACKPOT</a><br />
                    <a href="https://www.facebook.com/CSGO-FROG-618713828334046/">https://www.facebook.com/CSGO-FROG-618713828334046/</a>
                </div>
            </div>
        </div>
    </div>
</div>