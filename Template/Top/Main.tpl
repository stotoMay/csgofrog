<script>
    $("title").html("{$title}");
</script>

<div class="row m-4">
    <div class="col-12 m-auto">
        <table class="table-yellow">
            <tr>
                <th>Rank</th>
                <th>Player</th>
                <th>Games played</th>
                <th>Total won</th>
            </tr>
            {foreach $ranking as $player}
                <tr>
                    <td>#{$player.rank}</td>
                    <td>
                        <a href="/stats/user/{$player.steam.steamid}"><img src="{$player.steam.avatarmedium}" alt="{$player.steam.personaname}" class="avatar"/></a>
                        <a href="/stats/user/{$player.steam.steamid}">{$player.steam.personaname}</a>
                    </td>
                    <td class="text-center">{$player.games_played}</td>
                    <td><span>{number_format(divide($player.win, 100))} <i class="fa fa-database"></i></span></td>
                </tr>
            {/foreach}
        </table>
    </div>
</div>