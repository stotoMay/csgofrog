<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Controller;

use App\Router\Controller;
use Component\Steam\User\User;
use Component\Steam\SteamFactory;

class Main extends Controller
{
    /**
     * @var \Model\Main
     */
    private $model;

    private $steam;

    public static function __info__()
    {
        return ["uniqname" => "website", "setting" => ["giveaway_url" => "text"], "tracking" => true];
    }

    public function __init__()
    {
        $this->model = $this->getModel("Model/Main");
        $this->steam = SteamFactory::getSteamCore();
    }

    public function main()
    {
        $template = $this->theme->draw("Main/Main.tpl");

        if($this->browser->user() && $this->browser->user()->getColumn("tradetoken") != "")
        {
            $template->assignVar("tradeurl", $this->steam->api()->user()->buildTradeURL($this->browser->user()->getColumn("tradetoken")));
        }

        return ajaxresponse($template, "Jackpot", $this->request, $this->theme);
    }
}
