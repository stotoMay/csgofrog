<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Controller;

use App\Router\Controller;
use Component\Steam\Core;
use Component\Steam\SteamFactory;
use Component\SteamGamble\GambleFactory;
use Component\SteamGamble\Gamemodes\Jackpot\Statistic;

class Stats extends Controller
{
    /**
     * @var \Model\Stats
     */
    private $model;
    /**
     * @var Core
     */
    private $steam;
    /**
     * @var \Component\SteamGamble\Classes\Core
     */
    private $gamble;

    public static function __info__()
    {
        return ["tracking" => true];
    }

    public function __init__()
    {
        $this->model = $this->getModel("Model/Stats");
        $this->steam = SteamFactory::getSteamCore();
        $this->gamble = GambleFactory::getGambleCore();
    }

    public function history($page = 1)
    {
        $template = $this->theme->draw("Stats/History.tpl");

        $maxpages = $this->model->getHistoryPages(10);
        $page = (is_numeric($page) && $page > 1 && $page <= $maxpages) ? (int)$page : 1;

        $template->assignVar("jackpots", $this->model->getHistory($this->steam, $page * 10 - 10, 10));
        $template->assignVar("page", $page);
        $template->assignVar("is_lastpage", $page == $maxpages);

        return ajaxresponse($template, "Game history", $this->request, $this->theme);
    }

    public function user($steamID)
    {
        $userID = $this->model->getUserID($steamID);
        if($userID === false)
        {
            redirect("/", $this->request);
        }

        $template = $this->theme->draw("Stats/User.tpl");

        $user = $this->steam->api()->user()->getPlayerSummaries($steamID)[0];

        $template->assignVar("user", $user);
        $template->assignVar("userID", $userID);
        $template->assignVar("stats", $this->model->getUserStats($userID));
        $template->assignVar("userxp", $this->model->getXP($userID));

        $jackpot_page = $this->request->input("jackpot_page", 1, false, v()->integer()->min(0));
        $jackpots = $this->model->getUserJackpotHistory($userID, $jackpot_page);
        $template->assignVar("jackpots", $jackpots);
        $template->assignVar("jackpot_page", $jackpot_page);

        $coinflip_page = $this->request->input("coinflip_page", 1, false, v()->integer()->min(0));
        $coinflips = $this->gamble->statistic()->gamemode("coinflip")->getCoinflipHistory($coinflip_page, 10, $userID);
        $template->assignVar("coinflips", $coinflips);
        $template->assignVar("coinflip_page", $coinflip_page);

        $upgrade_page = $this->request->input("upgrade_page", 1, false, v()->integer()->min(0));
        $upgrades = $this->gamble->statistic()->gamemode("upgrade")->getUpgradeHistory($upgrade_page, 10, $userID);
        $template->assignVar("upgrades", $upgrades);
        $template->assignVar("upgrade_page", $upgrade_page);

        return ajaxresponse($template, $user["personaname"], $this->request, $this->theme);
    }
}