<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Controller;

use App\Router\Controller;
use Component\Steam\Core;
use Component\Steam\SteamFactory;
use Component\SteamGamble\GambleFactory;

class Upgrade extends Controller
{
    /**
     * @var \Model\Upgrade
     */
    private $model;
    /**
     * @var Core
     */
    private $steam;
    /**
     * @var \Component\SteamGamble\Classes\Core
     */
    private $gamble;

    public static function __info__()
    {
        return ["tracking" => true];
    }

    public function __init__()
    {
        $this->model = $this->getModel("Model/Upgrade");
        $this->steam = SteamFactory::getSteamCore();
        $this->gamble = GambleFactory::getGambleCore();
    }

    public function main()
    {
        return ajaxresponse($this->theme->draw("Upgrade/Main.tpl"), "Upgrade", $this->request, $this->theme);
    }

    public function jsondata() {
        $upgradehistory = $this->gamble->statistic()->gamemode("upgrade")->getUpgradeHistory(1, 15, NULL, true);

        if(!is_array($upgradehistory))
        {
            return response(json_encode(["success" => false]), $this->request);
        }

        if($this->browser->isLogin()) {
            $withdrawhistory = $this->gamble->statistic()->gamemode("upgrade")->getWithdrawHistory($this->browser->getUserID(), 1, 15);
            $depositeditems = $this->model->getUpgradeDeposit($this->steam, $this->browser->getUserID());
        }
        else {
            $withdrawhistory =  [];
            $depositeditems = [];
        }

        $upgrademarket = $this->model->getUpgradeMarket($this->steam);

        return response(json_encode(["success" => true, "history" => $upgradehistory, "deposit" => $depositeditems, "market" => $upgrademarket, "withdraw" => $withdrawhistory]), $this->request)->contentType("text/json");
    }
}