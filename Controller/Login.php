<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Controller;

use App\Router\Controller;
use App\User\Exception\UserNotFoundException;
use App\User\User;
use Component\Steam\Exception\APIException;
use Component\SteamWeb\SteamWebFactory;

class Login extends Controller
{
    /**
     * @var \Model\Login
     */
    private $model;
    /**
     * @var \Component\Steam\Core
     */
    private $steam;
    /**
     * @var \Component\SteamWeb\Classes\Core
     */
    private $web;

    private $loginserver;
    private $return_url;

    public static function __info__()
    {
        return [
            "setting" => ["loginserver" => "text", "return_url" => "text"], "uniqname" => "login", "tracking" => true
        ];
    }

    public function __init__()
    {
        $this->model = $this->getModel("Model/Login");
        $this->web = SteamWebFactory::getCore();
        $this->steam = $this->web->steam();

        if(empty($this->getSetting("loginserver")))
        {
            $this->loginserver = $this->request->domain();
        }
        else
        {
            $this->loginserver = $this->getSetting("loginserver");
        }

        if(empty($this->getSetting("return_url")))
        {
            $this->return_url = $this->request->domain()."login/verify";
        }
        else
        {
            $this->return_url = $this->getSetting("return_url");
        }
    }

    public function main()
    {
        if($this->browser->isLogin())
        {
            return;
        }

        $openid = new \LightOpenID($this->loginserver);
        $openid->returnUrl = $this->return_url;

        if(!$openid->mode)
        {
            $openid->identity = 'http://steamcommunity.com/openid';

            redirect($openid->authUrl(), $this->request);
        }
        else
        {
            redirect("/", $this->request);
        }
    }

    public function verify()
    {
        $redirecturl = "/";
        try
        {
            $openid = new \LightOpenID($this->loginserver);
            $openid->returnUrl = $this->return_url;
            $openid->identity = 'https://steamcommunity.com/openid';

            if($openid->mode != "cancel" && $openid->mode && $openid->validate())
            {
                preg_match("/^https:\/\/steamcommunity\.com\/openid\/id\/(7[0-9]{15,25}+)$/", $openid->identity, $matches);
                $steamID = $matches[1];

                try
                {
                    $steam = $this->steam->api()->user()->getPlayerSummaries($steamID, false)[0];

                    $userID = $this->model->getUserID($steamID);
                    if(!is_numeric($userID))
                    {
                        $userID = $this->model->createUser($steamID, $steam["personaname"]);
                        $this->browser->createLogin($userID, (time() + 3600 * 24 * 365 * 3));

                        $params = [$this->request, $userID];
                        $this->app->component()->setHook("onAccountCreation", $params);

                        $redirecturl = "/";
                    }
                    else
                    {
                        $this->model->updateUser($steamID, $steam["personaname"]);
                        $this->browser->createLogin($userID, (time() + 3600 * 24 * 365 * 3));
                    }

                    try
                    {
                        $user = new User($userID);

                        if($user->isBanned())
                        {
                            redirect("/", $this->request)->error("You are banned from this website!");
                        }
                    }
                    catch(UserNotFoundException $e)
                    {

                    }

                    if(!$this->web->websocket()->isVerified($userID))
                    {
                        $redirecturl = "/";
                    }
                }
                catch(APIException $e)
                {
                    redirect("/", $this->request)->error("An error occurred while fetching your steam profile data/" . $openid->identity . "/" . $steamID . "/" . $e->getMessage() . $e->getLine());
                }
            }
        }
        catch(\Exception $e)
        {
            redirect("/", $this->request)->error("There was an error while your login!");
        }

        redirect($redirecturl, $this->request);
    }
}
