<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Controller;

use App\Router\Controller;
use Component\Steam\Bot\Bot;
use Component\Steam\Exception\APIException;
use Component\Steam\Exception\BotNotFoundException;
use Component\Steam\SteamFactory;
use Component\SteamMarket\Exception\OPSkinsAPIException;
use Component\SteamMarket\OPSkins\API;

class Deposit extends Controller
{
    /**
     * @var \Model\Deposit
     */
    private $model;
    /**
     * @var \Component\Steam\Core
     */
    private $steam;

    /**
     * @var API
     */
    private $opskins;

    public static function __info__()
    {
        return ["tracking" => true];
    }

    public function __init__()
    {
        $this->model = $this->getModel("Model/Deposit");
        $this->steam = SteamFactory::getSteamCore();
        $this->opskins = new API("opskins_apikey"); 
    }

    public function inventory()
    {
        if(!$this->browser->isLogin())
        {
            return "";
        }

        $this->request->session()->closeSessionWrite();
        $template = $this->theme->draw("inventory.tpl");
        $steamID = $this->browser->user()->getColumn("steamID");

        $inventoryIDs = [
            ["appID" => 1912, "contextID" => 1, "opskinsID" => 1],
            ["appID" => 1000001, "contextID" => 1, "opskinsID" => 13],
            ["appID" => 1000003, "contextID" => 1, "opskinsID" => 15],
            ["appID" => 1000007, "contextID" => 1, "opskinsID" => 19],
            ["appID" => 1000008, "contextID" => 1, "opskinsID" => 20],
            ["appID" => 1000009, "contextID" => 1, "opskinsID" => 21],
            ["appID" => 1000010, "contextID" => 1, "opskinsID" => 22],
        ];

        $items = [];

        foreach($inventoryIDs as $inventoryID) {
            try
            {
                $inventory = $this->opskins->trade()->getUserInventory($steamID, $inventoryID["opskinsID"]);
            }
            catch(OPSkinsAPIException $e)
            {
                exit("We are currently experiencing problems fetching your opskins inventory!");
            }

            $tmpitems = $this->model->saveInventory($this->browser->getUserID(), $steamID, $inventoryID["appID"], $inventoryID["contextID"], $inventory);
            $this->steam->item()->filling($tmpitems);

            $items = array_merge($items, $tmpitems);
        }

        $forbidden_matches = array_clear(explode(";", strtolower($this->setting->getComponent("SteamGamble", "forbidden_matches"))));

        foreach($items as $key => $item)
        {
            $items[$key]["disabled"] = false;
            $splittedName = explode("|", $item["market_name"]);
            if(count($splittedName) > 1) {
                $items[$key]["weapon"] = $splittedName[0];
                $items[$key]['name'] = $splittedName[1];
            }
            else
            {
                $items[$key]['name'] = $item["market_name"];
            }
            preg_match('/(.+)\((.+)\)/', $items[$key]['name'], $matches);
            if(count($matches) === 3) {
                $exterior = $matches[2];
                $exterior_initals = "";
                $words = preg_split('/[\s,_-]+/', $exterior);
                foreach($words as $word) {
                    $exterior_initals .= $word[0];
                }
                $items[$key]['exterior'] = $exterior_initals;
                $items[$key]['name'] = $matches[1];
            }
            foreach($forbidden_matches as $match)
            {
                if(empty($match))
                {
                    continue;
                }

                if(is_int(strpos(strtolower($item["market_name"]), $match)))
                {
                    $items[$key]["disabled"] = true;
                }
            }

            if($this->model->createMarketItem($item) && $item["suggested_price"]) {
                $items[$key]["price"] = $item["suggested_price"];
                $item = $items[$key];
            }

            if(empty($item["image"]))
            {
                $this->model->setItemImage($item["appID"], $item["market_name"], $item["raw_image"]);
                $items[$key]["image"] = $item["raw_image"];
                $item = $items[$key];
            }

            if($item["price"] < $this->setting->getComponent("SteamGamble", "min_item_price") || empty($item["price"]))
            {
                $items[$key]["disabled"] = true;
            }

            if(!$this->setting->getComponent("SteamGamble", "allow_cases") && is_int(strpos(strtolower($item["name"]), "case")) && $item["price"] < 0.10)
            {
                $items[$key]["disabled"] = true;
            }
        }

        usort($items, function($a, $b) {
            if($a["disabled"] && $b["disabled"])
            {
                return $b["price"] * 100 - $a["price"] * 100;
            }
            elseif($a["disabled"])
            {
                return 1;
            }
            elseif($b["disabled"])
            {
                return -1;
            }
            else
            {
                return $b["price"] * 100 - $a["price"] * 100;
            }
        });

        $template->assignVar("items", $items);

        return response($template, $this->request);
    }
}
