<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Controller;

use App\Router\Controller;
use Component\Steam\Exception\APIException;
use Component\Steam\SteamFactory;
use Component\Steam\User\User;
use Component\SteamGamble\Classes\Core;
use Component\SteamGamble\GambleFactory;

class Account extends Controller
{
    /**
     * @var \Model\Account
     */
    private $model;
    /**
     * @var \Component\Steam\Core
     */
    private $steam;
    /**
     * @var Core
     */
    private $gamble;

    public static function __info__()
    {
        return ["tracking" => true];
    }

    public function __init__()
    {
        $this->auth("/");
        $this->steam = SteamFactory::getSteamCore();
        $this->gamble = GambleFactory::getGambleCore();
        $this->model = $this->getModel("Model/Account");
    }

    public function main()
    {
        $template = $this->theme->draw("Account/Main.tpl");

        if($this->request->has("tradeurl"))
        {
            $response = json_decode($this->updatetradeurl()->output(), true);

            if($response["success"])
            {
                redirect("/account", $this->request);
            }
            else
            {
                $this->printError($response["message"]);
            }
        }

        if($this->browser->user()->getColumn("tradetoken") != "")
        {
            $template->assignVar("tradeurl", $this->steam->api()->user()->buildTradeURL($this->browser->user()->getColumn("tradetoken")));
        }

        $template->assignVar("xp", $this->browser->user()->getColumn("xp"));

        $transaction_page = $this->request->input("transaction_page", 1, false, v()->integer()->min(1));
        $template->assignVar("transaction_page", $transaction_page);
        $template->assignVar("transactions", $this->gamble->statistic()->getTransactions($this->browser->getUserID(), $transaction_page, 15, false));

        return ajaxresponse($template, "Account", $this->request, $this->theme);
    }

    public function updatetradeurl()
    {
        $response = ["success" => false, "message" => $this->language->getMessage("invalid_tradeurl")];

        if($this->request->has("tradeurl") && is_string($this->request->input("tradeurl")))
        {
            $tradeurl = $this->steam->api()->user()->splitTradeURL($this->request->input("tradeurl"));

            if(isset($tradeurl))
            {
                if(!empty($tradeurl["token"]))
                {
                    $this->model->setTradeToken($this->browser->user()->getUserID(), $tradeurl["token"]);
                    $this->model->agreeTerms($this->browser->user()->getUserID());

                    $response = ["success" => true, "accesstoken" => $this->gamble->web()->websocket()->getAuthenticationToken($this->browser)];
                }
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function updatename()
    {
        try
        {
            $player = $this->steam->api()->user()->getPlayerSummaries($this->browser->user()->getColumn("steamID"), false)[0];

            return response(json_encode([
                "success" => true,
                "response" => is_int(strpos(strtolower($player["personaname"]), strtolower($this->setting->getComponent("Jackpot", "fee_name_match")))) ? 1 : 0
            ]), $this->request)->contentType("text/json");
        }
        catch(APIException $e)
        {
            return response(json_encode([
                "success" => true, "response" => false
            ]), $this->request)->contentType("text/json");
        }
    }
}