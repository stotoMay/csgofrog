<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Controller;

use App\Router\Controller;

class Logout extends Controller
{
    public static function __info__()
    {
        return ["tracking" => true];
    }

    public function __init__()
    {
        $this->auth("/login/authmessage");
    }

    public function main()
    {
        $this->browser->destroyLogin();

        redirect("/", $this->request)->keepFormat();
    }
}