<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2017 Fabian Emilius
 */

include(__DIR__."/App/Bootstrap/Bootstrap.php");

$app = App\Bootstrap\Factory::getApplication();
$request = App\HTTP\Request::createFromGlobals();

echo $app->router()->resolve($request);
