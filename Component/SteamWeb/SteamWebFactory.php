<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamWeb;

use App\Core\App;
use Component\SteamWeb\Classes\Core;

class SteamWebFactory
{
    private static $core = NULL;

    /**
     * @return Core
     */
    public static function getCore()
    {
        return self::$core;
    }

    public static function buildSteamWeb(App $app, \Component\Steam\Core $steam)
    {
        self::$core = new Core($app, $steam);
    }
}