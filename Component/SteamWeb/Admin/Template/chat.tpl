<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("steamweb", "title_chat")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="SteamWeb/navigation.tpl"}
    </div>
    <div class="panel-body">
        <form action="/admin/steamweb/chat?lobby={$lobby}" method="post">
            <div class="input-group">
                <input type="text" class="form-control" placeholder='Enter Time' name="searchtime">
                <div class="input-group-btn ">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
            <input type="hidden" name="token" value="{token()}"> <input type="hidden" name="mode" value="{$mode}">
        </form>
        <hr>
        <div class="btn-group btn-group-justified">
            {foreach $lobbies as $data}
                <a href="/admin/steamweb/chat?lobby={$data}" class="btn btn-default {if $data == $lobby}active{/if}">{strtoupper($data)}</a>
            {/foreach}
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-striped table-smallpadding">
                <tbody>
                {foreach $chat.results as $data}
                    <tr>
                        <td style="white-space: nowrap;">{datetimeformat($data.dateline)}</td>
                        <td><img src="{$data.steam.avatar}" /></td>
                        <td style="white-space: nowrap;">[{$data.rank}]<a href="/admin/user/view/{$data.userID}" style="color:{$data.color};">{$data.steam.personaname}</a>
                        </td>
                        <td>{$data.message}</td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>

        <div class="center-block">
            {pagination($chat.total, $chat.current, $pageurl)}
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>