$(document).ready(function () {
    var $button = $("#server-stop-button");
    var btntext = $button.html();

    var websocket = new WebsocketHandler(g_serverip, g_serverauth);

    websocket.addListener("onConnectionClose", function () {
        print_info(g_lang_restart_success);
        $button.html(btntext);
    });

    $button.click(function () {
        if($button.hasClass("disabled"))
        {
            return;
        }

        $button.html('<i class="fa fa-spin fa-circle-o-notch"></i>').addClass("disabled");
        websocket.sendMessage("chat_message", "/stop")
    });
});
