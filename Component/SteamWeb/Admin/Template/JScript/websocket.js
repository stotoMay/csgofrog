function EventManager() {
    var listeners = {};

    this.addListener = function (name, fn) {
        if (!listeners[name] || listeners[name].length == 0) {
            listeners[name] = [];
        }

        listeners[name].push(fn);
    };

    this.removeListener = function (name, fn) {
        if(typeof fn == "undefined")
        {
            listeners[name] = [];
        }
        else
        {
            var foundAt = -1;
            for (var i = 0, len = listeners.length; i < len && foundAt === -1; i++) {
                if (listeners[i] === fn) {
                    foundAt = i;
                }
            }

            if (foundAt >= 0) {
                listeners.splice(foundAt, 1);
            }
        }
    };

    this.fire = function (name, data) {
        if(!listeners[name])
        {
            return;
        }

        for(var i = 0; i < listeners[name].length; i++)
        {
            listeners[name][i](data);
        }
    }
}

function WebsocketHandler(serverIP, authcode) {
    var eventManager = new EventManager();

    this.addListener = function (name, fn) {
        eventManager.addListener(name, fn);
    };

    this.removeListener = function (name, fn) {
        eventManager.removeListener(name, fn)
    };

    this.sendMessage = writeMessage;

    var websocket;
    var running = false;
    var wasclosed = false;
    var errorcount = 0;

    init();

    function writeMessage(ac, da) {
        if (typeof da == "undefined") {
            da = null;
        }

        if (running) {
            websocket.send(JSON.stringify({"action": ac, "data": da}));
        }
        else {
            setTimeout(function () {
                writeMessage(ac, da);
            }, 200)
        }
    }

    function init() {
        websocket = new SockJS(serverIP, ["websocket", "xhr-streaming", "iframe-eventsource", "iframe-htmlfile", "xhr-polling", "iframe-xhr-polling", "jsonp-polling"]);
        websocket.onopen = function () {
            running = true;

            if (authcode) {
                writeMessage("auth", g_serverauth);
            }

            eventManager.fire("onConnectionCreate");

            errorcount = 0;
            wasclosed = false;
        };
        websocket.onmessage = function (ev) {
            var json = ev.data;
            eventManager.fire(json["action"], json["data"]);
        };
        websocket.onclose = function () {
            if(wasclosed === false)
            {
                eventManager.fire("onConnectionClose");
            }

            running = false;
            wasclosed = true;

            setTimeout(init, 1000);
            errorcount++;
        };
        websocket.onerror = function () {
            if(wasclosed === false)
            {
                eventManager.fire("onConnectionClose");
            }

            wasclosed = true;
            running = false;

            setTimeout(init, 10000);
            errorcount++;
        };
    }
}