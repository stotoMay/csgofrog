<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Lobby</th>
            <th>Date</th>
            <th>Message</th>
        </tr>
        </thead>
        <tbody>
        {foreach $messages as $data}
            <tr>
                <td>{strtoupper($data.lobby)}</td>
                <td style="white-space: nowrap;">{datetimeformat($data.dateline)}</td>
                <td>{$data.message}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
</div>