<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("steamweb", "title_user")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="SteamWeb/navigation.tpl"}
    </div>
    <div class="panel-body">
        <form action="/admin/steamweb/" method="post">
            <div class="input-group">
                <input type="text" class="form-control" placeholder='{language("steamweb", "steamid")}' name="steamID" id="srch-term">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
            <input type="hidden" name="token" value="{token()}"> <input type="hidden" name="mode" value="{$mode}">
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th>Name</th>
                    <th>Tradetoken</th>
                    <th>Created at</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {foreach $users.results as $user}
                    <tr>
                        <td>#{$user.userID}</td>
                        <td><img src="{$user.steam.avatar}"></td>
                        <td>{$user.steam.personaname}</td>
                        <td>{$user.tradetoken}</td>
                        <td>{datetimeformat($user.created)}</td>
                        <td><a href="/admin/user/view/{$user.userID}" class="btn btn-default">View Details</a></td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
        </div>

        <div class="center-block">
            {pagination($users.total, $users.current, $pageurl)}
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>