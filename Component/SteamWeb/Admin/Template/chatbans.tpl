<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("admin", "ban", "title_main")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}
<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="SteamWeb/navigation.tpl"}
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <form method="post" action="/admin/steamweb/chatbans?page={$page}">
                <table class="table">
                    <thead>
                    <tr>
                        <th>{language("admin", "ban", "banid")}</th>
                        <th>{language("admin", "ban", "username")}</th>
                        <th>{language("admin", "ban", "created")}</th>
                        <th>{language("admin", "ban", "expire")}</th>
                        <th>{language("admin", "ban", "banner")}</th>
                        <th></th>
                    </tr>
                    </thead>
                    {foreach $banlist.results as $ban}
                        <tr>
                            <td>#{$ban.id}</td>
                            <td><a href="/admin/user/view/{$ban.userID}">{$ban.username}</a></td>
                            <td>{datetimeformat($ban.created)}</td>
                            <td>
                                {if $ban.expire}
                                    {datetimeformat($ban.expire)}
                                {else}
                                    {language("admin", "ban", "expire_never")}
                                {/if}
                            </td>
                            <td><a href="/admin/user/view/{$ban.bannerID}">{$ban.banner}</a></td>
                            <td>
                                <button class="btn btn-primary" name="banID" value="{$ban.id}">{language("admin", "ban", "delete")}</button>
                            </td>
                        </tr>
                    {/foreach}
                </table>
                <input type="hidden" name="token" value="{token()}">
            </form>
            <div class="center-block">{pagination($banlist.total, $banlist.current, $pageurl)}</div>
        </div>
    </div>
</div>
{includetemplate="footer.tpl"}
</body>
</html>