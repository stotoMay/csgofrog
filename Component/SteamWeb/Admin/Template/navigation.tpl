<ul class="nav nav-tabs">
    <li class="{if $path.2 == ""}active{/if}"><a href="/admin/steamweb/">{language("steamweb", "page_user")}</a></li>
    {if permission("chat", "controller", "steamweb") == 1}
        <li class="{if $path.2 == "chat"}active{/if}"><a href="/admin/steamweb/chat/">{language("steamweb", "page_chat")}</a></li>
    {/if}
    {if permission("chat_ban", "component", "SteamWeb") == 1}
        <li class="{if $path.2 == "chatbans"}active{/if}"><a href="/admin/steamweb/chatbans/">{language("steamweb", "page_chatbans")}</a></li>
    {/if}
    {if permission("log", "controller", "steamweb") == 1}
        <li class="{if $path.2 == "log"}active{/if}"><a href="/admin/steamweb/log/">{language("steamweb", "page_log")}</a></li>
    {/if}
</ul>