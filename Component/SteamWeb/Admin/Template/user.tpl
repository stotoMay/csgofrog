<link rel="stylesheet" type="text/css" href="/Component/Steam/Admin/Template/Static/CSS/steam.css">

<div class="panel panel-default">
    <div class="panel-heading">Steam Gamble</div>
    <div class="panel-body">
        <div class="col-md-3">
            <div class="center-block" style="margin-bottom: 20px;">
                <img src="{$user.avatarfull}" class="img-thumbnail">
            </div>
        </div>
        <div class="col-md-9">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <td>Steam ID</td>
                        <td>{$user.steamid}</td>
                    </tr>
                    <tr>
                        <td>Steam Name</td>
                        <td>{$user.personaname}</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <a href="{$tradeurl}" class="btn-margin" target="_blank">{$tradeurl}</a></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <a href="{$user.profileurl}" target="_blank">{$user.profileurl}</a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <a href="https://steamcommunity.com/profiles/{$user.steamid}" target="_blank">https://steamcommunity.com/profiles/{$user.steamid}/</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

{if permission("chat_ban", "component", "SteamWeb")}
    {if $chatban}
        <div class="panel panel-default">
            <div class="panel-heading">Chat Ban</div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <tr>
                        <td>This user is banned from chat until</td>
                        <td>
                            {if $chatban.expire}
                                {datetimeformat($chatban.expire)}
                            {else}
                                FOREVER
                            {/if}
                        </td>
                    </tr>
                </table>

                <form method="post" action="/admin/steamweb/chatbans">
                    <button class="btn btn-primary btn-block" name="banID" value="{$chatban.id}">Remove Ban</button>
                    <input type="hidden" name="token" value="{token()}">
                </form>
            </div>
        </div>
    {/if}
{/if}

{if permission("chat", "controller", "steamweb")}
    <div class="panel panel-default">
        <div class="panel-heading">Chat Messages</div>
        <div class="panel-body">
            {pagination_ajax(string("/admin/steamweb/chatmessages/", $user.userID))}
        </div>
    </div>
{/if}

{sethook("pageSteamUserProfile", $user.userID)}