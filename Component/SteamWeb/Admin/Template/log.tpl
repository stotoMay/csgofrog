<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    {if permission("server_stop", "component", "SteamWeb") == 1}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js"></script>
        <script src="/Component/Steam/Admin/Template/Static/JScript/steam.js"></script>
        <script src="/Component/SteamWeb/Admin/Template/JScript/websocket.js"></script>

        <script type="text/javascript">
            var g_serverauth = "{getServerAuth()}";
            var g_serverip = '{setting("serverip", "component", "SteamWeb")}';

            var g_lang_restart_success = '{language("steamweb", "message", "stopped")}';
        </script>

        <script src="/Component/SteamWeb/Admin/Template/JScript/restart.js"></script>
    {/if}

    <title>{language("steamweb", "title_log")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="SteamWeb/navigation.tpl"}
    </div>
    <div class="panel-body">
        {if permission("server_stop", "component", "SteamWeb") == 1}
            <a href="javascript:" id="server-stop-button" class="btn btn-danger btn-block">Stop Server</a>

            <hr>
        {/if}

        <form action="/admin/steamweb/log" method="post">
            <div class="input-group">
                <input type="text" class="form-control" placeholder='Enter Time' name="searchtime">
                <div class="input-group-btn ">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
            <input type="hidden" name="token" value="{token()}"> <input type="hidden" name="mode" value="{$mode}">
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-smallpadding table-borderless">
                {foreach $logs.results as $log}
                    <tr>
                        <td style="white-space: nowrap;">[{datetimeformat($log.dateline)}]</td>
                        <td>{$log.message}</td>
                    </tr>
                {/foreach}
            </table>
        </div>
        <div class="center-block">
            {pagination($logs.total, $logs.current, $pageurl)}
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>