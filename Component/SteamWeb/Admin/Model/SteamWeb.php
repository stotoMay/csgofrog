<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamWeb\Admin\Model;

use App\Router\Model;
use Component\Steam\Core;

class SteamWeb extends Model
{
    private $user = [];

    public function getSteamUserList(Core $steam, $page = 1, $size = 50)
    {
        $totalusers = $this->database->select("user")->numRows();

        $userlist = $this->database->select("user")->order("id", "desc")->limit(($page-1)*$size, $size)->fetchAssoc();
        $users = [];
        foreach($userlist as $user)
        {
            $users[] = [
                "steam" => $steam->api()->user()->getPlayerSummaries($user["steamID"])[0],
                "userID" => $user["id"], "tradetoken" => $user["tradetoken"], "created" => $user["created"]
            ];
        }

        return ["results" => $users, "current" => $page, "total" => ceil($totalusers / $size)];
    }

    public function getChatLobbies()
    {
        $rows = $this->database->select("chat")->column(["lobby"])->where(["lobby[!]" => NULL, "dateline[>=]" => time() - 3600 * 24 * 31])->group("lobby")->fetchAssoc();

        $lobbies = [];
        foreach($rows as $row)
        {
            $lobbies[] = $row["lobby"];
        }

        return $lobbies;
    }

    public function getChatBans($page, $size = 50)
    {
        $total = $this->database->select("ban_chat")->where(["OR" => ["expire[>=]" => time(), "expire" => NULL]])->numRows();
        $results = $this->database->select("ban_chat")
            ->column(["ban_chat.id", "userID", "steamID", "bannerID", "expire", "ban_chat.created", "user.username"])
            ->leftJoin("user", ["user.id[=]ban_chat.userID"])
            ->order("ban_chat.id", "desc")
            ->limit(($page-1)*$size, $size)->where(["OR" => ["expire[>=]" => time(), "expire" => NULL]])->fetchAssoc();

        foreach($results as $key => $value)
        {
            $results[$key]["banner"] = $this->getUser($value["bannerID"])["username"];
        }

        return ["total" => ceil($total / $size), "current" => $page, "results" => $results];
    }

    public function getUserChatHistory($userID, $page = 1, $size = 10)
    {
        $totalmessages = $this->database->select("chat")->where(["userID" => $userID])->numRows();

        $messages = $this->database->select("chat")
            ->column(["message", "dateline", "lobby"])
            ->where(["userID" => $userID])
            ->order("chat.id", "desc")->limit(intval(($page-1)*$size), intval($size))->fetchAssoc();

        return ["results" => $messages, "total" => ceil($totalmessages/$size), "current" => $page];
    }

    public function removeChatBan($banID)
    {
        return $this->database->update("ban_chat")->set(["expire" => time() - 1])->where(["id" => $banID, "OR" => ["expire[>=]" => time(), "expire" => NULL]])->getAffectedRows() > 0;
    }

    public function getUserChatBan($userID)
    {
        $info = $this->database->select("ban_chat")
            ->column(["id", "expire"])
            ->where(["userID" => $userID, "OR" => ["expire[>=]" => time(), "expire" => NULL]])
            ->fetchRow();

        return $info;
    }

    public function searchLobbyPage($lobby, $time, $size = 50)
    {
        $visiblerows = $this->database->select("chat")->where(["lobby" => $lobby, "dateline[>]" => $time])->numRows();

        return floor($visiblerows/$size) + 1;
    }

    public function searchLogPage($time, $size = 50)
    {
        $visiblerows = $this->database->select("serverlog")->where(["dateline[>]" => $time])->numRows();

        return floor($visiblerows/$size) + 1;
    }

    public function getChatHistory(Core $steam, $lobby, $page = 1, $size = 50, $timestamp = NULL)
    {
        if(!isset($timestamp))
        {
            $timestamp = time();
        }

        $totalmessages = $this->database->select("chat")->where(["lobby" => $lobby, "dateline[<=]" => $timestamp])->numRows();

        $messages = $this->database->select("chat")
            ->column(["chat.userID", "user.steamID", "user.groupID", "group.color", "group.name" => "rank", "chat.message", "chat.dateline"])
            ->leftJoin("user", ["user.id[=]chat.userID"])
            ->leftJoin("group", ["user.groupID[=]group.id"])
            ->where(["lobby" => $lobby, "dateline[<=]" => $timestamp])
            ->order("chat.id", "desc")->limit(intval(($page-1)*$size), intval($size))->fetchAssoc();

        foreach($messages as $key => $row)
        {
            $messages[$key]["steam"] = $steam->api()->user()->getPlayerSummaries($row["steamID"])[0];
        }

        return ["results" => $messages, "total" => ceil($totalmessages/$size), "current" => $page];
    }

    public function getServerLog($page = 1, $size = 50, $timestamp = NULL)
    {
        if(!isset($timestamp))
        {
            $timestamp = time();
        }

        $totalpages = $this->database->select("serverlog")->numRows();

        $logs = $this->database->select("serverlog")
            ->where(["dateline[<=]" => $timestamp])
            ->order("id", "desc")
            ->limit((intval($page)-1)*$size, $size)->fetchAssoc();

        return ["results" => $logs, "current" => $page, "total" => ceil($totalpages/$size)];
    }

    public function getUser($userID)
    {
        if(!is_array($this->user[$userID]))
        {
            $this->user[$userID] = $this->database->select("user")->where(["id" => $userID])->fetchRow();
        }

        return $this->user[$userID];
    }
}