<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Router\Controller;

use Component\Steam\Core;
use Component\Steam\SteamFactory;
use Component\Steam\User\User;
use Component\SteamWeb\SteamWebFactory;

class SteamWeb extends Controller
{
    /**
     * @var \Component\SteamWeb\Admin\Model\SteamWeb
     */
    private $model;
    private $admin;
    /**
     * @var Core
     */
    private $steam;
    private $web;

    public static function __info__()
    {
        return [
            "uniqname" => "steamweb", "tracking" => true, "priority" => 80,
            "permission" => ["view" => "bool", "log" => "bool", "chat" => "bool"], "icon" => "steam",
            "requirement" => [["application", "general", "admin_control_panel"], ["controller", "steamweb", "view"]]
        ];
    }

    public function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("Component/SteamWeb/Admin/Model/SteamWeb");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));

        $this->steam = SteamFactory::getSteamCore();
        $this->web = SteamWebFactory::getCore();
    }

    public function main()
    {
        $template = $this->theme->draw("SteamWeb/main.tpl");

        if($this->request->has("steamID"))
        {
            $steamID = $this->steam->api()->user()->getSteamID64($this->request->input("steamID"));
            $userID = $this->steam->getUserID($steamID);

            if(is_numeric($userID))
            {
                redirect("/admin/user/view/".$userID, $this->request);
            }
            else
            {
                $this->printError($this->language->get(["steamweb", "message", "user_not_found"]));
            }
        }

        $page = $this->request->input("page", 1, false, v()->integer()->min(1));
        $template->assignVar("pageurl", "/admin/steamweb/?page=%p");

        $template->assignVar("users", $this->model->getSteamUserList($this->steam, $page));

        return response($template, $this->request);
    }

    public function chat()
    {
        if($this->getPermission("chat") != 1)
        {
            redirect("/admin/steamweb", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("SteamWeb/chat.tpl");

        $lobbies = $this->model->getChatLobbies();
        $timestamp = NULL;

        $lobby = $this->request->input("lobby", $lobbies[0], false, v()->textstring());
        $page = $this->request->input("page", 1, false, v()->integer()->min(1));

        if($this->request->has("searchtime"))
        {
            $time = strtotime($this->request->input("searchtime"));

            if($time !== false)
            {
                $time -= $this->request->timezoneOffset();
                $page = $this->model->searchLobbyPage($lobby, $time);
                $timestamp = time();
            }
            else
            {
                $this->printError($this->language->get(["steamweb", "message", "invalid_time"]));
            }
        }

        if(!isset($timestamp))
        {
            $timestamp = $this->request->input("timestamp", time(), false, v()->integer()->min(1));
        }

        if($page == 1)
        {
            $timestamp = time();
        }
        $template->assignVar("pageurl", "/admin/steamweb/chat/?page=%p&timestamp=".$timestamp."&lobby=".$lobby);
        $template->assignVar("lobby", $lobby);
        $template->assignVar("lobbies", $lobbies);

        $template->assignVar("chat", $this->model->getChatHistory($this->steam, $lobby, $page, 50, $timestamp));

        return response($template, $this->request);
    }

    public function chatmessages($userID)
    {
        if($this->browser->user()->permission()->get("trades", "controller", "steamgamble") == 1)
        {
            $this->request->session()->closeSessionWrite();

            $page = $this->request->input("page", 1, false, v()->integer());
            $messages = $this->model->getUserChatHistory($userID, $page, 10);

            $template = $this->theme->draw("SteamWeb/chat_messages.tpl");

            $template->assignVar("userID", $userID);
            $template->assignVar("messages", $messages["results"]);

            $json = ["success" => true, "content" => $template->parse(), "current" => $page, "total" => $messages["total"]];
        }
        else
        {
            $json = ["success" => false];
        }

        return response(json_encode($json, JSON_HEX_QUOT | JSON_HEX_TAG), $this->request)->contentType("text/json");
    }

    public function chatbans()
    {
        if($this->browser->user()->permission()->getComponent("SteamWeb", "chat_ban") != 1)
        {
            redirect("/admin/steamweb", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("SteamWeb/chatbans.tpl");

        $page = $this->request->input("page", 1, false, v()->integer()->min(1));
        if($this->request->has("banID", true, v()->integer()))
        {
            if($this->model->removeChatBan($this->request->input("banID")))
            {
                $this->printInfo($this->language->get(["admin", "ban", "message", "deleted"]));
            }
            else
            {
                $this->printError($this->language->getMessage("standard_error"));
            }
        }

        $template->assignVar("banlist", $this->model->getChatBans($page));
        $template->assignVar("pageurl", "/admin/steamweb/chatbans?page=%p");
        $template->assignVar("page", $page);

        return response($template, $this->request);
    }

    public function log()
    {
        if($this->getPermission("log") != 1)
        {
            redirect("/admin/steamweb", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("SteamWeb/log.tpl");

        $timestamp = NULL;
        $page = $this->request->input("page", 1, false, v()->integer()->min(1));

        if($this->request->has("searchtime"))
        {
            $time = strtotime($this->request->input("searchtime"));

            if($time !== false)
            {
                $time -= $this->request->timezoneOffset();
                $page = $this->model->searchLogPage($time);
                $timestamp = time();
            }
            else
            {
                $this->printError($this->language->get(["steamweb", "message", "invalid_time"]));
            }
        }

        if(!isset($timestamp))
        {
            $timestamp = $this->request->input("timestamp", time(), false, v()->integer()->min(1));
        }

        if($page == 1)
        {
            $timestamp = time();
        }
        $template->assignVar("pageurl", "/admin/steamweb/log?page=%p&timestamp=".$timestamp);

        $template->assignVar("logs", $this->model->getServerLog($page, 50, $timestamp));

        return response($template, $this->request);
    }

    public function user($userID)
    {
        $template = $this->theme->draw("SteamWeb/user.tpl");

        $user = $this->model->getUser($userID);

        if(!is_array($user))
        {
            return "";
        }

        if($this->getPermission("view") != 1)
        {
            return "";
        }

        $steamuser = new User($user["steamID"]);
        $steamdata = array_merge(["userID" => $user["id"]], $this->steam->api()->user()->getPlayerSummaries($user["steamID"])[0]);

        $template->assignVar("user", $steamdata);
        $template->assignVar("tradeurl", $this->steam->api()->user()->buildTradeURL($user["tradetoken"]));
        $template->assignVar("chatban", $this->model->getUserChatBan($userID));

        return response($template, $this->request);
    }
}