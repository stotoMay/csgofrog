<?php
/**
 * @param string|\App\Theme\Template $template
 * @param string $title
 * @param \App\HTTP\Request $request
 * @param \App\Theme\Theme $theme
 *
 * @return \App\Theme\Template|string
 * @throws \App\Core\Exception\FileNotFoundException
 * @throws \App\Theme\Exception\ParseException
 * @throws \App\Theme\Exception\TemplateNotFoundException
 */
function ajaxresponse($template, $title, $request, $theme)
{
    $app = \App\Bootstrap\Factory::getApplication();

    $title .= " - CSGOFrog.com";
    $jackpotsettings = $app->database()->select("jackpot_setting")->fetchAssoc();

    if(is_object($template))
    {
        $template->assignVar("title", $title);
        $template->assignVar("jackpotsettings", $jackpotsettings);
    }

    if($request->format() == "tpl")
    {
        return response($template, $request);
    }
    else
    {
        if(is_object($template))
        {
            $data = $template->parse();
        }
        else
        {
            $data = $template;
        }

        $template = $theme->draw("frame.tpl")->assignVar("main", $data)->assignVar("title", $title);
        $template->assignVar("jackpotsettings", $jackpotsettings);

        return response($template, $request);
    }
}