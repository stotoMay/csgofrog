<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamWeb\Classes;

class Chat
{
    private $web;

    public function __construct(Core $web)
    {
        $this->web = $web;
    }
}