<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamWeb\Classes;

use App\Core\App;

class Core
{
    private $app;
    private $steam;

    private $websocket;

    public function __construct(App $app, \Component\Steam\Core $steam)
    {
        $this->app = $app;
        $this->steam = $steam;

        $this->websocket = new Websocket($this);
    }

    /**
     * @return Websocket
     */
    public function websocket()
    {
        return $this->websocket;
    }

    public function app()
    {
        return $this->app;
    }

    public function steam()
    {
        return $this->steam;
    }
}