<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamWeb\Classes;

use App\HTTP\Browser;
use App\User\Exception\UserNotFoundException;
use App\User\User;
use Component\SteamWeb\Exception\WebsocketMethodNotFoundException;

class Websocket
{
    private $web;
    private $methods;

    public function __construct(Core $web)
    {
        $this->web = $web;
        $this->methods = [];
    }

    /**
     * @param string $key
     * @param int $browserID
     *
     * @return false|int
     * @throws \App\Database\Exception\ParseException
     */
    public function verifyAuthenticationToken($key, &$browserID = NULL)
    {
        $key = $this->web->app()->database()->select("serverauth")->where(["token" => $key])->fetchAssoc()[0];

        if(!is_array($key))
        {
            return false;
        }

        $session = $this->web->app()->database()->select("session")->where([
            "browserID" => $key["browserID"], "userID" => $key["userID"], "expire[>=]" => time()
        ])->fetchAssoc()[0];

        if(is_array($session))
        {
            $browserID = $key["browserID"];

            return $session["userID"];
        }
        else
        {
            return false;
        }
    }

    /**
     * @param Browser $browser
     *
     * @return string
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function getAuthenticationToken(Browser $browser)
    {
        if(!$browser->isLogin() || !$this->isVerified($browser->getUserID()))
        {
            return NULL;
        }

        $key = $this->web->app()->database()->select("serverauth")->where([
            "browserID" => $browser->getBrowserID(), "userID" => $browser->getUserID(),
            "dateline[>=]" => (time() - 24 * 3600)
        ])->fetchAssoc()[0];

        if(is_array($key))
        {
            return $key["token"];
        }
        else
        {
            $accesskey = str_token();
            $this->web->app()->database()->insert("serverauth")->set([
                "token" => $accesskey, "browserID" => $browser->getBrowserID(),
                "userID" => $browser->getUserID(), "dateline" => time()
            ]);

            return $accesskey;
        }
    }

    /**
     * @param int $userID
     *
     * @return bool
     */
    public function isVerified($userID)
    {
        $user = new User($userID);

        return !empty($user->getColumn("tradetoken"));
    }

    /**
     * @param int $userID
     * @param int $browserID
     *
     * @return array|false
     */
    public function getWebsocketUser($userID, $browserID = NULL)
    {
        try
        {
            $user = new User($userID);
        }
        catch(UserNotFoundException $e)
        {
            return false;
        }

        $data = $this->web->app()->database()->select("group")->where(["id" => $user->getColumn("groupID")])->fetchRow();
        $color = $data["color"];
        if($data["color"] == "#000000")
        {
            $color = NULL;
        }

        $group = ["name" => $data["name"], "color" => $color];

        $websiteban = $this->web->app()->database()->select("ban")
            ->where(["OR" => ["expire[>=]" => time(), "expire" => NULL], "userID" => $userID])
            ->fetchRow();
        if(!is_array($websiteban))
        {
            $websiteban = 0;
        }
        else
        {
            $websiteban = $websiteban["expire"];
        }

        $chatban = $this->web->app()->database()->select("ban_chat")
            ->where(["OR" => ["expire[>=]" => time(), "expire" => NULL], "userID" => $userID])
            ->fetchRow();
        if(!is_array($chatban))
        {
            $chatban = 0;
        }
        else
        {
            $chatban = $chatban["expire"];
        }

        return [
            "userID" => $userID, "color" => $group["color"], "xp" => $user->getColumn("xp"),
            "steam" => $this->web->steam()->api()->user()->getPlayerSummaries($user->getColumn("steamID"))[0],
            "rank" => $group["name"], "browserID" => $browserID, "websiteban" => $websiteban, "chatban" => $chatban,
            "permission" => $user->permission()->getPermissions("component", "SteamWeb"),
            "tradetoken" => $user->getColumn("tradetoken")
        ];
    }

    /**
     * @param $userID
     *
     * @return array|false
     */
    public function getUserData($userID)
    {
        try
        {
            $user = new User($userID);
        }
        catch(UserNotFoundException $e)
        {
            return false;
        }

        $data = $this->web->app()->database()->select("group")->where(["id" => $user->getColumn("groupID")])->fetchRow();
        $color = $data["color"];
        if($data["color"] == "#000000")
        {
            $color = NULL;
        }

        $data = ["rank" => $data["name"], "color" => $color, "userID" => $userID, "xp" => $user->getColumn("xp")];
        $data = array_merge($data, $this->web->steam()->api()->user()->getPlayerSummaries($user->getColumn("steamID"))[0], $data);

        return $data;
    }

    /**
     * @param string $name
     * @param string $identifer
     *
     * @param $callable
     */
    public function registerInterfaceMethod($name, $callable, $identifer)
    {
        $this->methods[strtolower($identifer)][strtolower($name)] = $callable;
    }

    /**
     * @param string $identifer
     * @param string $method
     * @param array $args
     *
     * @return mixed
     *
     * @throws WebsocketMethodNotFoundException
     */
    public function callInterfaceMethod($identifer, $method, $args = [])
    {
        $callable = $this->methods[strtolower($identifer)][strtolower($method)];

        if(!is_callable($callable))
        {
            throw new WebsocketMethodNotFoundException();
        }

        if(is_array($callable))
        {
            $reflection = new \ReflectionMethod($callable[0], $callable[1]);
            $argcount = $reflection->getNumberOfRequiredParameters();
        }
        else
        {
            $reflection = new \ReflectionFunction($callable);
            $argcount = $reflection->getNumberOfRequiredParameters();
        }

        if($argcount > count($args))
        {
            throw new WebsocketMethodNotFoundException();
        }

        return call_user_func_array($callable, $args);
    }
}