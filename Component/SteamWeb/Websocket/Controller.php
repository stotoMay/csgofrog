<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamWeb\Websocket;

use App\User\User;
use Component\Steam\Core;
use Component\Steam\Exception\APIException;
use Component\Steam\SteamFactory;
use Component\SteamWeb\Exception\WebsocketMethodNotFoundException;
use Component\SteamWeb\SteamWebFactory;

class Controller extends \App\Router\Controller
{
    /**
     * @var Core
     */
    private $steam;
    /**
     * @var \Component\SteamWeb\Classes\Core
     */
    private $web;
    /**
     * @var Model
     */
    private $model;

    public static function __info__()
    {

    }

    public function __init__()
    {
        $this->steam = SteamFactory::getSteamCore();
        $this->web = SteamWebFactory::getCore();
        $this->model = $this->getModel("Component/SteamWeb/Websocket/Model");
    }

    public function servertime()
    {
        $this->request->session()->closeSessionWrite();

        if($this->verifyToken($this->request->input("apikey", NULL, false)))
        {
            $response = ["success" => true, "response" => time()];
        }
        else
        {
            $response = ["success" => false];
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function authenticate($auth)
    {
        $this->request->session()->closeSessionWrite();

        $response = ["success" => false];
        if($this->verifyToken($this->request->input("apikey", NULL, false)))
        {
            $userID = $this->web->websocket()->verifyAuthenticationToken($auth, $browserID);

            if(is_numeric($userID))
            {
                $data = $this->web->websocket()->getWebsocketUser($userID, $browserID);

                if($data["websiteban"] !== NULL && $data["websiteban"] <= time())
                {
                    $response = ["success" => true, "response" => $data];
                }
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function userdata($userID)
    {
        $this->request->session()->closeSessionWrite();

        $response = ["success" => false];
        if($this->verifyToken($this->request->input("apikey", NULL, false)))
        {
            if(is_numeric($userID))
            {
                $response = ["success" => true, "response" => $this->web->websocket()->getWebsocketUser($userID)];
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function language()
    {
        $this->request->session()->closeSessionWrite();

        $response = ["success" => false];
        if($this->verifyToken($this->request->input("apikey", NULL, false)))
        {
            $response = ["success" => true, "response" => $this->language->get(["steamweb", "server_lang"])];
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function chat($lobby)
    {
        $this->request->session()->closeSessionWrite();

        $response = ["success" => false];
        if($this->verifyToken($this->request->input("apikey", NULL, false)))
        {
            $response = ["success" => true, "response" => $this->model->getChatMessages($this->web, 25, $lobby)];
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function getmarketinfo($appID)
    {
        $this->request->session()->closeSessionWrite();

        $response = ["success" => false];
        if($this->verifyToken($this->request->input("apikey", NULL, false)))
        {
            try
            {
                $response = ["success" => true, "response" => $this->steam->api()->market()->getMarketInfo($appID)];
            }
            catch(APIException $e)
            {
                $response = ["success" => false, "message" => $e->getMessage()];
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function updatemarketpage($appID, $page)
    {
        $this->request->session()->closeSessionWrite();

        $response = ["success" => false];
        if($this->verifyToken($this->request->input("apikey", NULL, false)))
        {
            try
            {
                $items = $this->steam->api()->market()->getMarketPage($page, $appID, "name", "asc")->toArray(false);

                foreach($items as $item)
                {
                    $this->steam->item()->setMarketItem($item["appID"], $item["market_name"], $item["market_volume"], $item["price"], $item["name"], $item["color"], $item["background"], $item["raw_image"]);
                }

                $response = ["success" => true];
            }
            catch(APIException $e)
            {
                $response = ["success" => false, "message" => $e->getMessage()];
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function enablemaintenance()
    {
        $this->request->session()->closeSessionWrite();

        $response = ["success" => false];

        if($this->verifyToken($this->request->input("apikey", NULL, false)))
        {
            $this->model->enableMaintenance();
            $response["success"] = true;
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function __call($name, $arguments)
    {
        $this->request->session()->closeSessionWrite();

        $defaultresponse = response(json_encode(["success" => false]), $this->request)->statusCode(500)->contentType("text/json");

        if(!$this->request->has("apikey", false) || !$this->verifyToken($this->request->input("apikey", NULL, false)))
        {
            return $defaultresponse;
        }

        try
        {
            $args = $arguments;
            $args[0] = $this->request;

            return response(json_encode($this->web->websocket()->callInterfaceMethod($name, $arguments[0], $args)), $this->request)->contentType("text/json");
        }
        catch(WebsocketMethodNotFoundException $e)
        {
            return $defaultresponse;
        }
    }

    private function verifyToken($key)
    {
        return $this->getToken() == $key;
    }

    public function getToken()
    {
        return $this->setting->getComponent("SteamWeb", "api");
    }
}