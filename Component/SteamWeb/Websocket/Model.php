<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamWeb\Websocket;

use Component\SteamWeb\Classes\Core;

class Model extends \App\Router\Model
{
    public function userExists($userID)
    {
        return $this->database->select("user")->where(["id" => $userID])->exists();
    }

    public function getGroupData($groupID)
    {
        $group = $this->database->select("group")->where(["id" => $groupID])->fetchRow();

        $color = $group["color"];
        if($group["color"] == "#000000")
        {
            $color = NULL;
        }

        return ["name" => $group["name"], "color" => $color];
    }

    public function enableMaintenance()
    {
        $this->database->update("setting")->set(["value" => 1])->where(["type" => "application", "typeID" => "general", "name" => "maintenance"]);
    }

    public function getChatMessages(Core $web, $limit = 25, $lobby = NULL)
    {
        $rows = $this->database->select("chat")->where(["lobby" => $lobby])->order("id", "desc")->limit(0, $limit)
            ->fetchAssoc();

        $messages = [];
        foreach($rows as $row)
        {
            $messages[] = ["type" => "user", "msg" => $row["message"], "user" => $web->websocket()->getUserData($row["userID"]), "dateline" => $row["dateline"]];
        }

        $messages = array_reverse($messages);

        return $messages;
    }
}