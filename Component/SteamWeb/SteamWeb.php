<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamWeb;

use App\Component\Parent\Component;
use App\HTTP\Browser;
use App\Theme\Theme;
use Component\Steam\Core;

class SteamWeb extends Component
{
    private $web;

    public static function info()
    {
        return [
            "setting" => [
                "api" => "text", "serverip" => "text",
                "currency_symbol_before" => "text", "currency_symbol_after" => "text",
            ],
            "permission" => [
                "chat_write" => "bool", "chat_url" => "bool", "chat_spam" => "bool", "see_online_list" => "bool",
                "chat_broadcast" => "bool", "chat_admin" => "bool", "chat_clear" => "bool",
                "websitestatus" => "bool", "server_refresh" => "bool", "server_stop" => "bool",
                "debug" => "bool", "reload" => "bool", "chat_ban" => "bool", "website_ban" => "bool",
            ],
            "language" => ["english" => "Language/English.lang"]
        ];
    }

    public function onEnable()
    {
        include(__DIR__."/function.php");

        if(!class_exists('App\\Admin\\Controller\\SteamWeb', false))
        {
            include(base_dir("Component/SteamWeb/Admin/Controller/SteamWeb.php"));
            $this->app->router()->initController('\\App\\Admin\\Controller\\SteamWeb');
        }

        $this->app->router()->bindController("/serverinterface/", 'Component\\SteamWeb\\Websocket\\Controller', "/Component/SteamWeb/Admin/Template/");
    }

    public function onApplicationFinish()
    {

    }

    /**
     * @param $steam Core
     */
    public function onSteamLoad($steam)
    {
        SteamWebFactory::buildSteamWeb($this->app, $steam);

        $this->web = SteamWebFactory::getCore();

        $params = [$steam, $this->web];
        $this->app->component()->setHook("onSteamWebLoad", $params);
    }

    /**
     * @param Theme $theme
     *
     * @throws \App\Core\Exception\FolderNotFoundException
     * @throws \App\Core\Exception\InvalidArgumentException
     * @throws \App\Theme\Exception\FolderNotFoundException
     */
    public function onPageload($theme)
    {
        $theme->registerFolder("Component/SteamWeb/Admin/Template", "SteamWeb/");

        $theme->assignFunction("isVerified", function($args)
        {
            $web = SteamWebFactory::getCore();
            /**
             * @var Browser $browser
             */
            $browser = $args[3];

            if(!$browser->isLogin())
            {
                return false;
            }

            return $web->websocket()->isVerified($browser->getUserID());
        });

        $theme->assignFunction("getServerAuth", function($args)
        {
            $web = SteamWebFactory::getCore();
            /**
             * @var Browser $browser
             */
            $browser = $args[3];

            return $web->websocket()->getAuthenticationToken($browser);
        });
    }

    public function pageUserProfile($args, $user)
    {
        $controller = new \App\Admin\Controller\SteamWeb($args[2], $args[3], $args[0], $args[4], $args[1]);

        $template = "";
        if(method_exists($controller, "user") && is_callable([$controller, "user"]))
        {
            $template = (string)$controller->user($user["id"]);
        }

        return $template;
    }
}