<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamMarket\Update;

use App\Tools\Request;
use Component\Steam\Core;
use Component\SteamMarket\Exception\UpdateException;

class SteamAnalyst
{
    private $steam;
    private $apikey;

    public function __construct(Core $steam, $apikey)
    {
        $this->steam = $steam;
        $this->apikey = $apikey;
    }

    public function update()
    {
        if(!is_int(array_search("730", $this->steam->getAppIDs())))
        {
            throw new UpdateException();
        }

        $url = new Request("http://api.steamanalyst.com/apiV2.php?key=".$this->apikey);
        $url->execute();

        if($url->getStatusCode() != 200)
        {
            throw new UpdateException();
        }

        $data = $url->getResponseContent();
        $items = @json_decode($data, true);

        if(!is_array($items))
        {
            throw new UpdateException();
        }

        $items = $items["results"];

        if(!is_array($items) || count($items) == 0)
        {
            throw new UpdateException();
        }

        foreach($items as $item)
        {
            $this->steam->item()->queueMarket(730, $item["market_name"]);
        }

        $updated = 0;
        $created = 0;
        foreach($items as $key => $item)
        {
            $items[$key] = array_merge($this->steam->item()->getMarket(730, $item["market_name"]), $item);
            $item = $items[$key];
            $price = isset($item["suggested_amount_min"]) ? str_replace(",", "", $item["suggested_amount_min"]) : $item["avg_price_7_days"];
            $volume = str_replace(",", "", $item["avg_volume"]);

            if(empty($item["background"]) && empty($item["color"]) && !empty($item["img"]))
            {
                $image = str_replace("http://steamcommunity-a.akamaihd.net/economy/image/", "", $item["img"]);
                $color = is_int(strpos($item["market_name"], "StatTrak")) ? "#CF6A32" : (is_int(strpos($item["market_name"], "Souvenir")) ? "#FFD700" : "#D2D2D2");
                $this->steam->item()->createMarketItem(730, $item["market_name"], $price, $image, $color, NULL, NULL, $volume);
                $created++;
            }
            elseif(!empty($price) && $item["market_price"] != $price)
            {
                $this->steam->item()->addItemPrice(730, $item["market_name"], $price, $volume);
                $updated++;
            }
        }

        return ["updated" => $updated, "created" => $created];
    }
}