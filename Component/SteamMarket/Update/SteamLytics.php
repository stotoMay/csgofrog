<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamMarket\Update;

use App\Core\App;
use App\Tools\Request;
use Component\Steam\Core;
use Component\SteamMarket\Exception\UpdateException;

class SteamLytics
{
    private $app;
    private $steam;

    private $key;

    public function __construct(App $app, Core $steam, $apikey)
    {
        $this->app = $app;
        $this->steam = $steam;

        $this->key = $apikey;
    }

    public function updateSafePrices()
    {
        $bannedItems = [];
        $removeable = [];
        $items = $this->app->database()->select("steam_price")->column(["market_name", "userID"])
            ->leftJoin("steam_market", ["steam_market.id[=]steam_price.itemID"])->fetchAssoc();
        foreach($items as $item)
        {
            if($item["userID"] == 0)
            {
                $removeable[] = $item["market_name"];
            }

            $bannedItems[] = $item["market_name"];
        }

        $url = new Request("http://api.csgo.steamlytics.xyz/v2/pricelist?key=".$this->key);
        $url->execute();

        if($url->getStatusCode() != 200)
        {
            throw new UpdateException();
        }

        $data = @json_decode($url->getResponseContent(), true);

        if(!is_array($data) || !$data["success"])
        {
            throw new UpdateException();
        }

        $updated = 0;
        foreach($data["items"] as $item)
        {
            if($item["safe_price"] != 0)
            {
                $this->steam->item()->addItemPrice(730, $item["name"], $item["safe_price"]);
            }

            if($item["safe_price"] == 0 || ($item["ongoing_price_manipulation"] && $item["safe_price"] > 200) || $item["first_seen"] >= time() - 3600 * 24 * 2)
            {
                if(!is_int(array_search($item["name"], $bannedItems)))
                {
                    $marketItem = $this->app->database()->select("steam_market")->column(["id"])->where(["appID" => 730, "market_name" => $item["name"]])->fetchRow();

                    if(empty($marketItem))
                    {
                        continue;
                    }

                    $this->app->database()->insert("steam_price")->set([
                        "itemID" => $marketItem["id"], "price" => 0, "userID" => 0, "created" => time()
                    ]);
                }

                continue;
            }
            elseif(is_int(array_search($item["name"], $removeable)))
            {
                $marketItem = $this->app->database()->select("steam_market")->column(["id"])->where(["appID" => 730, "market_name" => $item["name"]])->fetchRow();

                if(empty($marketItem))
                {
                    continue;
                }

                $this->app->database()->delete("steam_price")->where(["itemID" => $marketItem["id"], "userID" => 0]);
            }

            $updated++;
        }

        return ["created" => 0, "updated" => $updated];
    }
}