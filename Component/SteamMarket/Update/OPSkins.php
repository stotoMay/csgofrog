<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamMarket\Update;

use App\Core\App;
use Component\Steam\Core;
use Component\SteamMarket\Exception\OPSkinsAPIException;
use Component\SteamMarket\OPSkins\API;

class OPSkins
{
    private $app;
    private $steam;
    private $api;

    public function __construct(App $app, Core $steam)
    {
        $this->app = $app;
        $this->steam = $steam;
        $this->api = new API();
    }

    public function updateSuggestedPrices($updateAppID = NULL)
    {
        $created = 0;
        $updated = 0;

        foreach($this->steam->getAppIDs() as $appID)
        {
            if(isset($updateAppID) && $updateAppID != $appID)
            {
                continue;
            }

            try
            {
                $suggested_prices = $this->api->pricing()->getSuggestedPrices($appID);
                $lowest_prices = $this->api->pricing()->getAllLowestListPrices($appID);

                if(empty($suggested_prices)) {
                    $items = $lowest_prices;
                }
                else if(empty($lowest_prices)) {
                    $items = $suggested_prices;
                }
                else {
                    $items = array_merge_recursive($lowest_prices, $suggested_prices);
                }
            }
            catch(OPSkinsAPIException $e)
            {
                continue;
            }

            foreach($items as $market_name => $data)
            {
                if(empty($data["op_7_day"]) && empty($data["op_30_day"]))
                {
                    continue;
                }

                $prices = [];
                if(!empty($data["op_7_day"]))
                {
                    $prices[] = $data["op_7_day"];
                }
                elseif(!empty($data["op_30_day"]))
                {
                    $prices[] = $data["op_30_day"];
                }

                if(!empty($data["price"]))
                {
                    $prices[] = $data["price"];
                }

                if(count($prices) == 0)
                {
                    continue;
                }

                $volume = !empty($data["quantity"]) ? $data["quantity"] : NULL;
                $price = max(3, round(min($prices)*(100/85)));

                $this->steam->item()->addItemPrice($appID, $market_name, $price / 100, $volume);
                $updated++;
            }
        }

        return ["updated" => $updated, "created" => $created];
    }

    public function updatePriceHistory($lastupdate = 0)
    {
        $created = 0;
        $updated = 0;

        foreach($this->steam->getAppIDs() as $appID)
        {
            $tmp = "";
            $stream = fopen("https://opskins.com/pricelist/".$appID.".json", "rb");
            while(!feof($stream))
            {
                $items = [];
                $opencount = 0;

                $buffer = fread($stream, 1024*1024);
                $tmp .= $buffer;
                $chunk = "";

                foreach(str_split($tmp) as $chr)
                {
                    if($chr == "{")
                    {
                        $opencount++;
                    }
                    elseif($chr == "}")
                    {
                        $opencount--;
                    }
                    elseif($chr == "," && $opencount == 0)
                    {
                        $items[] = $chunk;
                        $chunk = "";
                    }

                    if($chr != "," || $opencount > 0)
                    {
                        $chunk .= $chr;
                    }
                }

                $tmp = $chunk;

                if($tmp[0] == "{")
                {
                    $tmp = substr($tmp, 1);
                }

                if(count($items) > 0)
                {
                    $itemdata = json_decode("{".implode(",", $items)."}", true);

                    if(!is_array($itemdata))
                    {
                        continue;
                    }

                    foreach($itemdata as $market_name => $data)
                    {
                        $insertdata = [];
                        foreach($data as $date => $value)
                        {
                            $timestamp = strtotime($date) + 3600 * 6;

                            if($timestamp < $lastupdate)
                            {
                                continue;
                            }

                            $value["price"] = max($value["price"], 3);

                            if($value["price"] > 10)
                            {
                                $value["price"] = round((100/85) * $value["price"]);
                            }

                            $insertdata[] = [
                                "itemID" => $this->steam->item()->getItemID($appID, $market_name),
                                "price" => $value["price"], "volume" => $value["volume"], "dateline" => $timestamp
                            ];
                        }

                        if(count($insertdata) == 0)
                        {
                            continue;
                        }

                        if(count($insertdata) > 0)
                        {
                            $this->app->database()->insert("steam_price_history")->set($insertdata);
                            $this->steam->item()->updateItemPrice($appID, $market_name);
                        }

                        $updated++;
                    }
                }
            }
        }

        return ["updated" => $updated, "created" => $created];
    }
}