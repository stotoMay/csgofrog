<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamMarket\Update;

use App\Tools\Request;
use Component\Steam\Core;
use Component\SteamMarket\Exception\UpdateException;

class CSGOFAST
{
    private $steam;

    public function __construct(Core $steam)
    {
        $this->steam = $steam;
    }

    public function update()
    {
        if(!is_int(array_search("730", $this->steam->getAppIDs())))
        {
            throw new UpdateException();
        }

        $url = new Request("https://api.csgofast.com/price/all");
        $url->execute();

        if($url->getStatusCode() != 200)
        {
            throw new UpdateException();
        }

        $data = $url->getResponseContent();
        $items = @json_decode($data, true);

        if(!is_array($items))
        {
            throw new UpdateException();
        }

        $updated = 0;
        foreach($items as $market_name => $price)
        {
            $this->steam->item()->addItemPrice("730", $market_name, $price);
            $updated++;
        }

        return ["updated" => $updated, "created" => 0];
    }
}