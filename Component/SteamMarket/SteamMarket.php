<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */
 
namespace Component\SteamMarket;

use App\Component\Parent\Component;
use App\Theme\Theme;
use Component\Steam\Core;
use Component\Steam\Item\ItemList;
use Component\SteamMarket\Exception\UpdateException;
use Component\SteamMarket\Update\CSGOFAST;
use Component\SteamMarket\Update\OPSkins;
use Component\SteamMarket\Update\SteamAnalyst;
use Component\SteamMarket\Update\SteamLytics;

class SteamMarket extends Component
{
    const MARKET_ENABLED = false;

    /**
     * @var Core
     */
    private $steam;

    public static function info()
    {
        return ["language" => ["English" => "Language/English.lang"]];
    }

    public function onEnable()
    {
        if(!class_exists('Component\\Steam\\Steam'))
        {
            throw new \ErrorException("component SteamMarket needs Steam");
        }

        $controllerList = ["SteamMarket", "OPSkins"];

        foreach($controllerList as $controller)
        {
            if(!class_exists('App\\Admin\\Controller\\'.$controller, false))
            {
                include(base_dir("Component/SteamMarket/Admin/Controller/".$controller.".php"));
                $this->app->router()->initController('\\App\\Admin\\Controller\\'.$controller);
            }
        }
    }

    /**
     * @param $steam Core
     */
    public function onSteamLoad($steam)
    {
        $this->steam = $steam;
    }

    /**
     * @param Theme $theme
     */
    public function onPageload($theme)
    {
        $theme->assignVar("is_market_enabled", self::MARKET_ENABLED);
    }

    /**
     * @param int $botID
     * @param ItemList $items
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function onBotInventoryLoad($botID, &$items)
    {
        $assetIDs = [];

        foreach($items as $item)
        {
            $assetIDs[] = $item->getAssetID();
        }

        if(count($assetIDs) > 0)
        {
            $botitems = $this->app->database()->select("market")
                ->column(["appID", "contextID", "assetID"])
                ->where(["assetID" => $assetIDs, "botID" => $botID])->fetchAssoc();

            foreach($botitems as $item)
            {
                $items->removeAsset($item["appID"], $item["contextID"], $item["assetID"]);
            }
        }
    }

    /**
     * @param int $botID
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function onSteamBotDelete($botID)
    {
        $this->app->database()->delete("market")->where(["botID" => $botID]);
    }

    /**
     * @param $steam Core
     * @param $web \Component\SteamWeb\Classes\Core
     */
    public function onSteamWebLoad($steam, $web)
    {
        $this->steam = $steam;

        $web->websocket()->registerInterfaceMethod("updateprices", [$this, "_updateItemPrices"], "market");
        $web->websocket()->registerInterfaceMethod("updateopskinsprices", [$this, "_updateOpskinItemPrices"], "market");

        if(is_int(array_search("730", $steam->getAppIDs())))
        {
            $web->websocket()->registerInterfaceMethod("updatecsgofastprices", [$this, "_updateCSGOFASTItemPrices"], "market");
            $web->websocket()->registerInterfaceMethod("updatesteamanalystprices", [$this, "_updateSteamAnalystItemPrices"], "market");
            $web->websocket()->registerInterfaceMethod("updatesteamlyticsprices", [$this, "_updateSteamLyticsItemPrices"], "market");
        }
    }

    public function _updateItemPrices()
    {
        $created = 0;
        $updated = 0;

        foreach($this->steam->getAppIDs() as $appID)
        {
            if($appID == "730")
            {
                if(!empty($this->app->setting()->getController("steammarket", "steamanalyst")))
                {
                    $data = $this->_updateSteamAnalystItemPrices();

                    $created += $data["created"];
                    $updated += $data["updated"];

                    continue;
                }
                elseif(!empty($this->app->setting()->getController("steammarket", "steamlytics")))
                {
                    $data = $this->_updateSteamLyticsItemPrices();

                    $created += $data["created"];
                    $updated += $data["updated"];

                    continue;
                }
            }

            $data = $this->_updateOpskinItemPrices($appID);

            $created += $data["created"];
            $updated += $data["updated"];
        }

        return ["success" => true, "response" => ["created" => $created, "updated" => $updated]];
    }

    public function _updateOpskinItemPrices($appID = NULL)
    {
        $opskins = new OPSkins($this->app, $this->steam);

        try
        {
            $response = ["success" => true, "response" => $opskins->updateSuggestedPrices($appID)];
        }
        catch(UpdateException $e)
        {
            $response = ["success" => false];
        }

        return $response;
    }

    public function _updateCSGOFASTItemPrices()
    {
        $csgofast = new CSGOFAST($this->steam);

        try
        {
            $response = ["success" => true, "response" => $csgofast->update()];
        }
        catch(UpdateException $e)
        {
            $response = ["success" => false];
        }

        return $response;
    }

    public function _updateSteamAnalystItemPrices()
    {
        try
        {
            $apikey = $this->app->setting()->getController("steammarket", "steamanalyst");

            if(empty($apikey))
            {
                throw new UpdateException();
            }

            $steamanalyst = new SteamAnalyst($this->steam, $apikey);

            $response = ["success" => true, "response" => $steamanalyst->update()];
        }
        catch(UpdateException $e)
        {
            $response = ["success" => false];
        }

        return $response;
    }

    public function _updateSteamLyticsItemPrices()
    {
        try
        {
            $apikey = $this->app->setting()->getController("steammarket", "steamlytics");

            if(empty($apikey))
            {
                throw new UpdateException();
            }

            $steamlytics = new SteamLytics($this->app, $this->steam, $apikey);

            $response = ["success" => true, "response" => $steamlytics->updateSafePrices()];
        }
        catch(UpdateException $e)
        {
            $response = ["success" => false];
        }

        return $response;
    }
}