<?php

namespace Component\SteamMarket\OPSkins;
class Trade {
    public function __construct(API $api)
    {
        $this->api = $api;
    }

    public function getUserInventory($steamId, $appID = 1)
    {
        $pages = 1;
        $items = [];
        for($i = 0; $i < $pages; $i++) {
            $response = $this->api->getRequest("ITrade", "GetUserInventoryFromSteamId", [
                "steam_id" => $steamId,
                "app_id" => $appID,
                "page" => $i + 1,
                "per_page" => 500
            ], 1, true, "-trade");

            $pages = ceil($response["total"] / 500);
            $items = array_merge($items, $response["items"]);
        }

        return $items;
    }
}