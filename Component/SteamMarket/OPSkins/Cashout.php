<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamMarket\OPSkins;

class Cashout
{
    private $api;

    public function __construct(API $api)
    {
        $this->api = $api;
    }

    public function getAddress($processor)
    {
        return $this->api->getRequest("ICashout", "GetAddress", ["processor" => $processor]);
    }

    public function setAddress($processor, $address, $twofactor_code = NULL)
    {
        $args = ["processor" => $processor, "address" => $address];

        if(isset($twofactor_code))
        {
            $args["twofactor_code"] = $twofactor_code;
        }

        return $this->api->postRequest("ICashout", "SetAddress", $args);
    }

    public function getPendingCashouts()
    {
        return $this->api->getRequest("ICashout", "GetPendingCashouts");
    }

    public function cancelPendingCashout($cashoutID)
    {
        return $this->api->postRequest("ICashout", "CancelPendingCashout", ["cashoutid" => $cashoutID]);
    }

    public function getCashoutableBalance()
    {
        return $this->api->getRequest("ICashout", "GetCashoutableBalance");
    }

    public function getBitcoinInstantCashoutRate($cashoutID)
    {
        return $this->api->getRequest("ICashout", "GetBitcoinInstantCashoutRate", ["cashoutid" => $cashoutID]);
    }

    public function requestPayPal($amount, $priority = false)
    {
        $priority = $priority ? 1 : 0;

        return $this->api->postRequest("ICashout", "RequestPayPal", ["amount" => $amount, "priority" => $priority]);
    }

    public function requestBitcoin($amount, $priority = false)
    {
        $priority = $priority ? 1 : 0;

        return $this->api->postRequest("ICashout", "RequestBitcoin", ["amount" => $amount, "priority" => $priority]);
    }

    public function requestSkrill($amount, $priority = false)
    {
        $priority = $priority ? 1 : 0;

        return $this->api->postRequest("ICashout", "RequestSkrill", ["amount" => $amount, "priority" => $priority]);
    }
}