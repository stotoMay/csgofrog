<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamMarket\OPSkins;

class Test
{
    private $api;

    public function __construct(API $api)
    {
        $this->api = $api;
    }

    public function test()
    {
        return $this->api->getRequest("ITest", "Test");
    }

    public function testAuthed()
    {
        return $this->api->getRequest("ITest", "TestAuthed");
    }
}