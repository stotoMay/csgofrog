<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamMarket\OPSkins;

use App\HTTP\Exception\UrlException;
use App\Tools\Request;
use Component\SteamMarket\Exception\OPSkinsAPIException;

class API
{
    private $apikey;

    private $cashout;
    private $inventory;
    private $pricing;
    private $sales;
    private $trade;

    private $balance;

    public function __construct($apikey = NULL)
    {
        $this->apikey = $apikey;

        $this->cashout = new Cashout($this);
        $this->inventory = new Inventory($this);
        $this->pricing = new Pricing($this);
        $this->sales = new Sales($this);
        $this->trade = new Trade($this);
    }

    public function cashout()
    {
        return $this->cashout;
    }

    public function inventory()
    {
        return $this->inventory;
    }

    public function pricing()
    {
        return $this->pricing;
    }

    public function sales()
    {
        return $this->sales;
    }

    public function trade()
    {
        return $this->trade;
    }

    public function getLastBalance()
    {
        return $this->balance;
    }

    public function getRequest($interface, $method, $args = [], $version = 1, $responseonly = true, $suffixUrl = "")
    {
        $url = "https://api" . $suffixUrl . ".opskins.com/".$interface."/".$method."/v".$version."/";

        if(isset($this->apikey))
        {
            $args["key"] = $this->apikey;
        }
        $args["format"] = "json";

        $params = [];

        foreach($args as $key => $value)
        {
            $params[] = $key."=".$value;
        }

        $url .= "?".implode("&", $params);

        $request = new Request($url);

        try
        {
            return $this->handleResponse($request->execute(), $responseonly);
        }
        catch(UrlException $e)
        {
            throw new OPSkinsAPIException($e->getMessage(), $e->getCode());
        }
    }

    public function postRequest($interface, $method, $args = [], $version = 1, $responseonly = true)
    {
         $url = "https://api.opskins.com/".$interface."/".$method."/v".$version."/";

         if(isset($this->apikey))
         {
             $args["key"] = $this->apikey;
         }
         $args["format"] = "json";

         $request = new Request($url);

         $request->setPostData($args);

         try
         {
             return $this->handleResponse($request->execute(), $responseonly);
         }
         catch(UrlException $e)
         {
             throw new OPSkinsAPIException($e->getMessage(), $e->getCode());
         }
    }

    private function handleResponse($json, $responseonly)
    {
        $data = @json_decode($json, true);

        if(!is_array($data))
        {
            throw new OPSkinsAPIException();
        }

        if($data["status"] != 1)
        {
            throw new OPSkinsAPIException($data["message"], $data["status"], $data["response"]);
        }

        if(isset($data["balance"]))
        {
            $this->balance = $data["balance"];
        }

        if($responseonly)
        {
            return $data["response"];
        }
        else
        {
            return $data;
        }
    }
}