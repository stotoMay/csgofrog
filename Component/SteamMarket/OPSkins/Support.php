<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamMarket\OPSkins;

class Support
{
    private $api;

    public function __construct(API $api)
    {
        $this->api = $api;
    }

    public function RepairItem($saleID)
    {
        return $this->api->postRequest("ISupport", "RepairItem", ["saleid" => $saleID]);
    }
}