<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamMarket\OPSkins;

class Inventory
{
    private $api;

    public function __construct(API $api)
    {
        $this->api = $api;
    }

    public function getInventory()
    {
        return $this->api->getRequest("IInventory", "GetInventory");
    }

    public function withdraw($items)
    {
        return $this->api->postRequest("IInventory", "Deposit", ["items" => $items]);
    }

    public function deposit($items)
    {
        return $this->api->postRequest("IInventory", "Withdraw", ["items" => $items]);
    }
}