<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamMarket\OPSkins;

class Pricing
{
    private $api;

    public function __construct(API $api)
    {
        $this->api = $api;
    }

    public function getPriceList($appID)
    {
        return $this->api->getRequest("IPricing", "GetPriceList", ["appid" => $appID], 2);
    }

    public function getAllLowestListPrices($appID)
    {
        return $this->api->getRequest("IPricing", "GetAllLowestListPrices", ["appid" => $appID]);
    }

    public function getSuggestedPrices($appID)
    {
        return $this->api->getRequest("IPricing", "GetSuggestedPrices", ["appid" => $appID], 2);
    }
}