<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamMarket\OPSkins;

class Sales
{
    private $api;

    public function __construct(API $api)
    {
        $this->api = $api;
    }

    public function getSales($type = NULL, $appID = NULL, $after_saleid = NULL, $page = 1, $per_page = 10000, $sort = NULL)
    {
        $args = ["page" => $page, "per_page" => $per_page];

        if(isset($type))
        {
            $args["type"] = $type;
        }

        if(isset($appID))
        {
            $args["appid"] = $appID;
        }

        if(isset($after_saleid))
        {
            $args["after_saleid"] = $after_saleid;
        }

        if(isset($sort))
        {
            $args["sort"] = $sort;
        }

        return $this->api->getRequest("ISales", "GetSales", $args);
    }

    public function getListingLimit()
    {
        return $this->api->getRequest("ISales", "GetListingLimit");
    }

    public function listItems($items)
    {
        return $this->api->postRequest("ISales", "ListItems", ["items" => json_encode($items)]);
    }

    public function editPrice($saleID, $price)
    {
        return $this->api->postRequest("ISales", "ListItems", ["saleid" => $saleID, "price" => $price]);
    }

    public function editPriceMulti($prices)
    {
        return $this->api->postRequest("ISales", "EditPriceMulti", ["items" => $prices]);
    }

    public function bumpItems($saleIDs)
    {
        return $this->api->postRequest("ISales", "BumpItems", ["items" => implode(",", $saleIDs)]);
    }

    public function returnItems($saleIDs)
    {
        return $this->api->postRequest("ISales", "ReturnItems", ["items" => implode(",", $saleIDs)]);
    }

    public function getActiveTradeOffers()
    {
        return $this->api->getRequest("ISales", "GetActiveTradeOffers");
    }

    public function search($appID, $contextID, $search_item = NULL, $min = NULL, $max = NULL)
    {
        $args = ["app" => $appID."_".$contextID];

        if(isset($search_item))
        {
            $args["search_item"] = $search_item;
        }

        if(isset($min))
        {
            $args["min"] = $min;
        }

        if(isset($max))
        {
            $args["max"] = $max;
        }

        return $this->api->getRequest("ISales", "Search", $args);
    }

    public function buyItems($saleIDs, $total)
    {
        return $this->api->postRequest("ISales", "BuyItems", ["saleids" => implode(",", $saleIDs), "total" => $total]);
    }
}