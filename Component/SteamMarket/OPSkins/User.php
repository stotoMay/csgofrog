<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamMarket\OPSkins;

class User
{
    private $api;

    public function __construct(API $api)
    {
        $this->api = $api;
    }

    public function getBalance()
    {
        $this->api->getRequest("IUser", "GetBalance");

        return $this->api->getLastBalance();
    }

    public function saveTradeURL($trade_url)
    {
        return $this->api->postRequest("IUser", "SaveTradeURL", ["trade_url" => $trade_url]);
    }
}