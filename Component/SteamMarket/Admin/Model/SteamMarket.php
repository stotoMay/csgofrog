<?php
namespace Component\SteamMarket\Admin\Model;

use App\Router\Model;
use Component\Steam\Core;

class SteamMarket extends Model
{
    public function getMarketItems(Core $steam, $match = NULL, $page = 1, $size = 50)
    {
        $count = $this->database->select("market");
        $query = $this->database->select("market")->order("price", "desc")->limit(($page-1)*$size, $size);

        if(isset($match))
        {
            $count->where(["market_name[~~]" => "%".$match."%"]);
            $query->where(["market_name[~~]" => "%".$match."%"]);
        }

        $items = $query->fetchAssoc();
        foreach($items as $key => $value)
        {
            $items[$key]["appname"] = $steam->api()->steam()->getAppName($value["appID"]);
            $items[$key]["image"] = $value["image"];
            $items[$key]["price"] /= 100;
        }

        return ["current" => $page, "total" => ceil($count->numRows() / $size), "results" => $items];
    }

    public function getItemInfo(Core $steam, $itemID)
    {
        $data = $this->database->select("market")->where(["id" => $itemID])->fetchRow();

        if(!is_array($data))
        {
            return [];
        }

        $data["appname"] = $steam->api()->steam()->getAppName($data["appID"]);
        $data["price"] /= 100;
        $data["dumpeddata"] = var_export($data["data"], true);

        return $data;
    }
}