<?php
namespace Component\SteamMarket\Admin\Model;

use App\Router\Model;
use Component\Steam\Core;

class OPSkins extends Model
{
    public function saveOpskinsKey($userID, $key, $steamID)
    {
        $data = $steamID.":".$key;

        if($this->database->select("setting")->where(["type" => "controller", "typeID" => "opskins", "name" => $userID])->exists())
        {
            $this->database->update("setting")->set(["value" => $data])->where(["type" => "controller", "typeID" => "opskins", "name" => $userID]);
        }
        else
        {
            $this->database->insert("setting")->set([
                "name" => $userID, "value" => $data, "type" => "controller",
                "typeID" => "opskins", "updated" => time()
            ]);
        }
    }

    public function getOpskinsKey($userID)
    {
        $data = array_clear(explode(":", $this->setting->get($userID, "controller", "opskins")));

        return ["steamID" => $data[0], "apikey" => $data[1]];
    }
}