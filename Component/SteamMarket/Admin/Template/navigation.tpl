<ul class="nav nav-tabs">
    {if $is_market_enabled}
        <li class="{if $path.1 == "steammarket" && $path.2 == ""}active{/if}"><a href="/admin/steammarket">Market</a></li>
    {/if}
    <li class="{if $path.1 == "steammarket" && $path.2 == "update"}active{/if}"><a href="/admin/steammarket/update">Update Prices</a></li>
    {if permission("sell", "controller", "opskins") == 1}
        <li class="{if $path.1 == "opskins" && $path.2 == "sell"}active{/if}"><a href="/admin/opskins/sell">Sell Items on OPskins</a></li>
    {/if}
</ul>