<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <script src="/Component/SteamMarket/Admin/Template/JScript/update_price.js" type="text/javascript"></script>

    <title>Update Item Prices {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-tabs panel-default">
    <div class="panel-heading">
        {includetemplate="Admin/SteamMarket/navigation.tpl"}
    </div>
    <div class="panel-body">
        <table class="table table-bordered">
            <tr>
                <td>{language("steammarket", "statsupdated")}</td>
                <td id="stats-updated">--</td>
            </tr>
            <tr>
                <td>{language("steammarket", "statscreated")}</td>
                <td id="stats-created">--</td>
            </tr>
        </table>
        <div class="center-block">
            <a href="javascript:" onclick="startPriceUpdate('opskins', this)" class="btn btn-primary">Update with OPSkins Database</a>
            {if array_search("730", $appIDs)}
                <a href="javascript:" onclick="startPriceUpdate('csgofast', this)" class="btn btn-primary">Update with CSGOFAST Database</a>
                {if $is_steamanalyst_enabled}
                    <a href="javascript:" onclick="startPriceUpdate('steamanalyst', this)" class="btn btn-primary">Update with SteamAnalyst Database</a>
                {/if}
                {if $is_steamlytics_enabled}
                    <a href="javascript:" onclick="startPriceUpdate('steamlytics', this)" class="btn btn-primary">Update with SteamLytics.XYZ Database</a>
                {/if}
            {/if}
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>