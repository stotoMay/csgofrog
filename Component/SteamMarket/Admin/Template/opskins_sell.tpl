<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css" />

    <script type="text/javascript">
        var g_sell_error = "There was an error while selling the items!";
        var g_sell_success = "The OPSkins Bot has sent you a tradeoffer with these items now!";
    </script>

    <script src="/Component/Steam/Admin/Template/Static/JScript/steam.js" type="text/javascript"></script>
    <script src="/Component/SteamMarket/Admin/Template/JScript/opskins_sell.js?t={time()}" type="text/javascript"></script>

    <title>Sell items on OPSkins {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-tabs panel-default">
    <div class="panel-heading">
        {includetemplate="Admin/SteamMarket/navigation.tpl"}
    </div>
    <div class="panel-body">
        <form method="post" action="/admin/opskins/sell">
            <input type="text" name="steamID" class="form-control" placeholder="Steam ID" value="{$steamID}" id="sellitem-steamID">
            <br>
            <input type="text" name="apikey" class="form-control" placeholder="OPSkins API Key" value="{$apikey}" id="sellitem-apikey">
            <br>
            <input type="submit" class="btn btn-primary" value="Save Data">
            <input type="hidden" name="token" value="{token()}">
        </form>

        <hr>

        <select id="sellitem-inventory" class="form-control">
            {foreach $inventories as $inventory}
                <option value="{$inventory.appID}:{$inventory.contextID}">{$inventory.name}</option>
            {/foreach}
        </select>

        <br>

        <div class="panel panel-default">
            <div class="panel-body" id="sellitem-items" style="height:200px;overflow-y: auto;"></div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body" id="sellitem-selected" style="height:200px;overflow-y: auto;"></div>
        </div>

        <a href="javascript:" class="btn btn-primary" id="button-sell">Sell Items ($<span class="sell-value">0.00</span>)</a>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>