<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css" />

    <title>Item #{$item.id} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/steammarket">Market</a></li>
    <li class="breadcrumb-item active">Item #{$item.id}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-heading">
        {$item.market_name}
    </div>
    <div class="panel-body">
        <div class="col-md-4">
            <div class="center-block">{renderItem($item)}</div>
        </div>
        <div class="col-md-8">
            <table class="table table-bordered">
                <tr>
                    <td>App ID</td>
                    <td>{$item.appname} ({$item.appID})</td>
                </tr>
                <tr>
                    <td>Context ID</td>
                    <td>{$item.contextID}</td>
                </tr>
                <tr>
                    <td>Market Name</td>
                    <td>{$item.market_name}</td>
                </tr>
                <tr>
                    <td>Asset ID</td>
                    <td>{$item.assetID}</td>
                </tr>
                <tr>
                    <td>Amount</td>
                    <td>{$item.amount}</td>
                </tr>
                <tr>
                    <td>DATA</td>
                    <td><pre>{$item.dumpeddata}</pre></td>
                </tr>
            </table>
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>