function startPriceUpdate(mode, btn) {
    var $btn = $(btn);
    var btntext = $btn.html();

    if($btn.hasClass("disabled"))
    {
        return;
    }

    $btn.css("width", $btn.innerWidth()).html('<i class="fa fa-spin fa-circle-o-notch"></i>').addClass("disabled");

    $.ajax("/admin/steammarket/doupdate/"+mode).always(onUpdateFinish);

    function onUpdateFinish(json) {
        if (json["success"]) {
            $("#stats-updated").html(json["response"]["updated"]);
            $("#stats-created").html(json["response"]["created"]);
        }

        $btn.css("width", "").html(btntext).removeClass("disabled");
    }
}