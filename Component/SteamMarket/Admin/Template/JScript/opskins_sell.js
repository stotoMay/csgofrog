$(document).ready(function () {
    var itemdatabase = {};

    var $inventories = $("#sellitem-inventory");
    var $apikey = $("#sellitem-apikey");

    var $useritems = $("#sellitem-items");
    var $selecteditems = $("#sellitem-selected");

    var $button_sell = $("#button-sell");

    function setInventoryLoading() {
        $useritems.html('<div style="display:block;text-align: center;margin-top:80px;"><i class="fa fa-spin fa-circle-o-notch"></i></div>')
    }

    function updateInventory() {
        var inventoryinfo = $inventories.val().split(":");

        var appID = inventoryinfo[0];
        var contextID = inventoryinfo[1];

        $useritems.html("");
        $selecteditems.html("");

        setInventoryLoading();

        $.ajax({
            url: "/admin/steamajax/inventory",
            dataType: "json",

            method: "post",
            data: {
                "appID": appID,
                "contextID": contextID,
                "steamID": $("#sellitem-steamID").val(),
                "token": g_sessionid
            },

            success: function (data) {
                var i;

                if(data["success"] !== true)
                {
                    return;
                }

                $useritems.html("");

                var itemlist = [];
                for(i = 0; i < data["response"].length; i++)
                {
                    if(typeof itemdatabase[data["response"][i]["market_name"]] === "undefined")
                    {
                        continue;
                    }

                    var price = Math.round(((100/105)*itemdatabase[data["response"][i]["market_name"]])) / 100;

                    if(price <= 0)
                    {
                        continue;
                    }

                    data["response"][i]["price"] = price;

                    itemlist.push(data["response"][i])
                }

                itemlist.sort(function (a, b) {
                    return (b["price"] - a["price"])*100
                });

                for(i = 0; i < itemlist.length; i++)
                {
                    if(itemlist[i]["market_name"].search("Music Kit ") >= 0 || (itemlist[i]["market_name"].search(" Case") >= 0 && itemlist[i]["price"] < 1) || itemlist[i]["market_name"].search(" Graffiti ") >= 0 )
                    {
                        continue;
                    }

                    var $container = $(document.createElement("div")).html(renderItem(itemlist[i])).attr({
                        "data-price": Math.round(itemlist[i]["price"]*100), "data-assetID": itemlist[i]["assetID"],
                        "data-appID": itemlist[i]["appID"], "data-contextID": itemlist[i]["contextID"], "id": "item-"+itemlist[i]["assetID"]
                    }).addClass("sell-item").click(onItemClick);
                    var $spacer = $(document.createElement("div")).css({"width": 76, "height": 72, "display": "inline-block"}).attr("id", "item-spacer-"+itemlist[i]["assetID"]).html($container);

                    $useritems.append($spacer);
                }
            }
        });
    }
    
    function updatePriceDatabase() {
        setInventoryLoading();

        var appID = $inventories.val().split(":")[0];

        $.ajax("/admin/opskins/fetchprice/"+appID).always(function (data) {
            if(data["success"] !== true)
            {
                return;
            }

            itemdatabase = data["items"];

            updateInventory();
        });
    }

    var sellitems = [];
    var totalvalue = 0;

    var shifted;
    $(document).on('keyup keydown', function(e){shifted = e.shiftKey;});

    function onItemClick() {
        var $this = $(this);

        var appID = parseInt($this.attr("data-appID"));
        var contextID = parseInt($this.attr("data-contextID"));
        var assetID = $this.attr("data-assetID");
        var price = parseInt($this.attr("data-price"));

        var is_added = false;
        for(var i = 0; i < sellitems.length; i++)
        {
            if(sellitems[i]["appid"] == appID && sellitems[i]["contextid"] == contextID && sellitems[i]["assetid"] == assetID)
            {
                sellitems.splice(i, 1);
                totalvalue -= price;
                $("#item-spacer-"+assetID).html($this);
                is_added = true;
            }
        }

        if(is_added === false)
        {
            var moving = [$this];

            if(shifted === true)
            {
                var $sellitem = $useritems.find(".sell-item");
                for(i = 0; i < $sellitem.length; i++)
                {
                    var $item = $($sellitem[i]);

                    if($item.attr("data-assetID") == assetID)
                    {
                        break;
                    }

                    moving.push($item);
                }
            }

            for(i = 0; i < moving.length; i++)
            {
                appID = parseInt(moving[i].attr("data-appID"));
                contextID = parseInt(moving[i].attr("data-contextID"));
                assetID = moving[i].attr("data-assetID");
                console.log(moving[i].attr("data-assetID"));
                price = parseInt(moving[i].attr("data-price"));

                $selecteditems.append(moving[i]);
                totalvalue += price;
                sellitems.push({"appid": appID, "contextid": contextID, "assetid": assetID, "price": Math.round(1.05*price), "addons": []})
            }
        }

        $(".sell-value").html((totalvalue/100).toFixed(2))
    }

    $inventories.change(function () {
        updatePriceDatabase();
    });

    var btntext = $button_sell.html();
    $button_sell.click(function () {
        if($apikey.val() == "")
        {
            return;
        }

        if($button_sell.hasClass("disabled"))
        {
            return;
        }

        $button_sell.addClass("disabled").css("width", $button_sell.innerWidth()).html('<i class="fa fa-spin fa-circle-o-notch"></i>');

        $.ajax({
            url: "/admin/opskins/sellitems",
            dataType: "json",

            method: "post",
            data: {
                "items": JSON.stringify(sellitems),
                "apikey": $apikey.val(),
                "token": g_sessionid
            },

            success: function (data) {
                for(var i = 0; i < data["items"].length; i++)
                {
                    var $item = $("#item-"+data["items"][i]["assetid"]);

                    totalvalue -= parseInt($item.attr("data-price"));

                    $("#item-spacer-"+data["items"][i]["assetid"]).remove();
                    $item.remove();
                }

                if(!data["success"])
                {
                    print_error(g_sell_error);
                }
                else
                {
                    print_info(g_sell_success);
                }
            },

            complete: function () {
                $button_sell.html(btntext).removeClass("disabled");
                $(".sell-value").html((totalvalue/100).toFixed(2));
            }
        });
    });

    updatePriceDatabase();
});