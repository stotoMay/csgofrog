<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css" />

    <title>Market {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-tabs panel-default">
    <div class="panel-heading">
        {includetemplate="Admin/SteamMarket/navigation.tpl"}
    </div>
    <div class="panel-body">
        <form method="get" action="/admin/steammarket">
            <div class="input-group">
                <input type="text" class="form-control" placeholder='{language("steam", "items", "itemname")}' value="{htmlspecialchars($match)}" name="match">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
                <input type="hidden" name="page" value="1">
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th></th>
                    <th>Item ID</th>
                    <th>Bot ID</th>
                    <th>App</th>
                    <th>Market Name</th>
                    <th>Price</th>
                    <th></th>
                </tr>
                </thead>
                {foreach $items.results as $item}
                    <tbody>
                    <tr>
                        <td>{renderItem($item)}</td>
                        <td>#{$item.id}</td>
                        <td><a href="/admin/steambotconfig/account/{$item.botID}">#{$item.botID}</a></td>
                        <td>{$item.appname}</td>
                        <td>{$item.market_name}</td>
                        <td>{number_format($item.price)}</td>
                        <td><a href="/admin/steammarket/item/{$item.id}" class="btn btn-default">View Details</a></td>
                    </tr>
                    </tbody>
                {/foreach}
            </table>
        </div>
        <div class="center-block">
            {pagination($items.total, $items.current, $pageurl)}
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>