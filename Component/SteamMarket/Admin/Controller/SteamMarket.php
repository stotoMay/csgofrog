<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Router\Controller;
use Component\Steam\SteamFactory;
use Component\SteamMarket\Exception\UpdateException;
use Component\SteamMarket\Update\CSGOFAST;
use Component\SteamMarket\Update\SteamAnalyst;
use Component\SteamMarket\Update\SteamLytics;

class SteamMarket extends Controller
{
    private $admin;
    /**
     * @var \Component\Steam\Core
     */
    private $steam;
    /**
     * @var \Component\SteamMarket\Admin\Model\SteamMarket
     */
    private $model;

    public static function __info__()
    {
        return [
            "uniqname" => "steammarket", "permission" => ["market" => "bool", "update" => "bool"], "icon" => "line-chart",
            "requirement" => [["application", "general", "admin_control_panel"], ["controller", "steammarket", "market"]],
            "setting" => ["steamanalyst" => "text", "steamlytics" => "text"], "tracking" => true, "priority" => 10
        ];
    }

    protected function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("Component/SteamMarket/Admin/Model/SteamMarket");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));

        $this->theme->registerFolder("Component/SteamMarket/Admin/Template", "Admin/SteamMarket/");
        $this->steam = SteamFactory::getSteamCore();
    }

    public function main()
    {
        if(!\Component\SteamMarket\SteamMarket::MARKET_ENABLED)
        {
            redirect("/admin/steammarket/update", $this->request);
        }

        $template = $this->theme->draw("Admin/SteamMarket/market.tpl");

        $match = $this->request->input("match", NULL, false, v()->textstring()->min(1));
        $page = $this->request->input("page", 1, false, v()->integer()->min(1));

        $template->assignVar("items", $this->model->getMarketItems($this->steam, $match, $page));
        $template->assignVar("match", $match);
        $template->assignVar("pageurl", "/admin/steammarket?page=%p&match=".$match);

        return response($template, $this->request);
    }

    public function item($itemID)
    {
        if(!\Component\SteamMarket\SteamMarket::MARKET_ENABLED)
        {
            redirect("/admin/steammarket/update", $this->request);
        }

        $template = $this->theme->draw("Admin/SteamMarket/item.tpl");

        $template->assignVar("item", $this->model->getItemInfo($this->steam, $itemID));

        return response($template, $this->request);
    }

    public function update()
    {
        if($this->getPermission("update") != 1)
        {
            redirect("/admin/steammarket/update", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("Admin/SteamMarket/update_price.tpl");

        $template->assignVar("appIDs", $this->steam->getAppIDs());
        $template->assignVar("is_steamanalyst_enabled", !empty($this->getSetting("steamanalyst")));
        $template->assignVar("is_steamlytics_enabled", !empty($this->getSetting("steamlytics")));

        return response($template, $this->request);
    }

    public function doupdate($mode)
    {
        $this->request->session()->closeSessionWrite();

        try
        {
            if($this->getPermission("update") != 1)
            {
                throw new UpdateException();
            }

            if($mode == "opskins")
            {
                $update = new \Component\SteamMarket\Update\OPSkins($this->app, $this->steam);

                $data = $update->updateSuggestedPrices();
            }
            elseif($mode == "csgofast")
            {
                $update = new CSGOFAST($this->steam);

                $data = $update->update();
            }
            elseif($mode == "steamanalyst")
            {
                $update = new SteamAnalyst($this->steam, $this->getSetting("steamanalyst"));

                $data = $update->update();
            }
            elseif($mode == "steamlytics")
            {
                $update = new SteamLytics($this->app, $this->steam, $this->getSetting("steamlytics"));

                $data = $update->updateSafePrices();
            }
            else
            {
                throw new UpdateException();
            }

            return response(json_encode(["success" => true, "response" => $data]), $this->request)->contentType("text/json");
        }
        catch(UpdateException $e)
        {
            return response(json_encode(["success" => false]), $this->request)->contentType("text/json");
        }
    }
}