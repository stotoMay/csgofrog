<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Router\Controller;
use Component\Steam\Exception\APIException;
use Component\Steam\SteamFactory;
use Component\SteamMarket\Exception\OPSkinsAPIException;
use Component\SteamMarket\OPSkins\API;

class OPSkins extends Controller
{
    private $admin;
    /**
     * @var \Component\Steam\Core
     */
    private $steam;
    /**
     * @var \Component\SteamMarket\Admin\Model\OPSkins
     */
    private $model;

    public static function __info__()
    {
        return [
            "uniqname" => "opskins", "permission" => ["sell" => "bool"], "visible" => false, "tracking" => true,
            "requirement" => [["application", "general", "admin_control_panel"], ["controller", "opskins", "sell"]]
        ];
    }

    protected function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("Component/SteamMarket/Admin/Model/OPSkins");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));
        $this->theme->registerFolder("Component/SteamMarket/Admin/Template", "Admin/SteamMarket/");

        $this->steam = SteamFactory::getSteamCore();
    }

    public function sell()
    {
        $data = $this->model->getOpskinsKey($this->browser->getUserID());

        $steamID = $data["steamID"];
        $apikey = $data["apikey"];

        if(empty($steamID))
        {
            $steamID = $this->browser->user()->getColumn("steamID");
        }

        if($this->request->has("apikey", true, v()->textstring()) && $this->request->has("steamID", true, v()->integer()))
        {
            try
            {
                $steamID = $this->steam->api()->user()->getSteamID64($this->request->input("steamID"));
                $apikey = $this->request->input("apikey");

                if(strpos($apikey, ":") !== false)
                {
                    throw new APIException();
                }

                $this->model->saveOpskinsKey($this->browser->getUserID(), $apikey, $steamID);
            }
            catch(APIException $e)
            {
                $this->printError($this->language->getMessage("standard_error"));
            }
        }

        $template = $this->theme->draw("Admin/SteamMarket/opskins_sell.tpl");

        $inventories = $this->steam->getInventories();
        foreach($inventories as $key => $inventory)
        {
            $inventories[$key]["name"] = $this->steam->api()->steam()->getAppName($inventory["appID"]);
        }

        $template->assignVar("inventories", $inventories);

        $template->assignVar("steamID", $steamID);
        $template->assignVar("apikey", $apikey);

        $template->assignVar("appIDs", $this->steam->getAppIDs());

        return response($template, $this->request);
    }

    public function fetchprice($appID)
    {
        $this->request->session()->closeSessionWrite();
        $response = ["success" => false, "items" => []];
        if(is_numeric($appID))
        {
            $opskins = new API();
            try
            {
                $data = $opskins->pricing()->getSuggestedPrices($appID);
                foreach($data as $market_name => $info)
                {
                    if(empty($info["op_7_day"]))
                    {
                        continue;
                    }
                    $response["items"][$market_name] = $info["op_7_day"];
                }
                $response["success"] = true;
            }
            catch(OPSkinsAPIException $e)
            {
                $response["success"] = false;
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function sellitems()
    {
        $items = $this->request->input("items");
        $apikey = $this->request->input("apikey");
        $response = ["success" => false, "items" => []];
        $decodeditems = @json_decode($items, true);
        if(is_array($decodeditems) && count($decodeditems) > 0 && !empty($apikey))
        {
            $opskins = new API($apikey);
            $chunks = array_chunk($decodeditems, 50);
            $response["success"] = true;
            foreach($chunks as $chunk)
            {
                while(true)
                {
                    try
                    {
                        $data = $opskins->sales()->listItems($chunk);

                        foreach($data["sales"] as $item)
                        {
                            $response["items"][] = $item;
                        }
                        break;
                    }
                    catch(OPSkinsAPIException $e)
                    {
                        if($e->getCode() == 3003)
                        {
                            $data = $e->getResponse();
                            foreach($chunk as $key => $item)
                            {
                                if($item["assetid"] != $data["bad_item"]["assetid"])
                                {
                                    continue;
                                }
                                unset($chunk[$key]);
                                if(count($chunk) == 0)
                                {
                                    $response["success"] = false;
                                    continue 3;
                                }
                                $chunk = array_values($chunk);
                                continue 2;
                            }
                            $response["success"] = false;
                            continue;
                        }
                        else
                        {
                            $response["success"] = false;
                            break;
                        }
                    }
                }
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }
}