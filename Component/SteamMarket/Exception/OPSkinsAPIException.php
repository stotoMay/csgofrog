<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamMarket\Exception;

class OPSkinsAPIException extends \Exception
{
    private $response;

    public function __construct($message = "", $code = 0, $response = [])
    {
        parent::__construct($message, $code, NULL);

        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }
}