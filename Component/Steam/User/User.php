<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\User;

use Component\Steam\App\AppList;
use Component\Steam\Exception\UserNotFoundException;
use Component\Steam\Group\GroupList;
use Component\Steam\SteamFactory;

class User
{
    private $steamID;
    private $usecache;
    private $cache;

    private $steam;

    /**
     * @param string $id
     * @param bool $cache
     *
     * @throws UserNotFoundException
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function __construct($id, $cache = true)
    {
        $this->steam = SteamFactory::getSteamCore();

        $this->steamID = $this->steam->api()->user()->getSteamID64($id, $cache);
        if(empty($this->steamID))
        {
            throw new UserNotFoundException();
        }

        $this->usecache = $cache;
    }

    /**
     * @return string
     */
    public function getSteamID64()
    {
        return $this->steamID;
    }

    /**
     * @return string
     */
    public function getSteamID32()
    {
        return bcsub($this->steamID, "76561197960265728");
    }

    /**
     * @return string
     */
    public function getSteamID()
    {
        $x = "STEAM_0";
        $y = ($this->getSteamID32() % 2) != 0 ? "1" : "0";
        $z = bcdiv(bcsub($this->steamID, bcadd("76561197960265728", $y)), "2");

        return $x.":".$y.":".$z;
    }

    /**
     * @return string
     */
    public function getSteamID3()
    {
        return "U:1:".$this->getSteamID32();
    }


    /**
     * @return array
     */
    public function getPlayerSummaries()
    {
        return $this->cache;
    }

    /**
     * @return string
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getName()
    {
        return $this->getCached("personaname");
    }

    /**
     * @return string
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getProfileURL()
    {
        return $this->getCached("profileurl");
    }

    /**
     * @param string $size
     *
     * @return string
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getAvatar($size = NULL)
    {
        if($size == "full")
        {
            return $this->getCached("avatarfull");
        }
        elseif($size == "medium")
        {
            return $this->getCached("avatarmedium");
        }
        else
        {
            return $this->getCached("avatar");
        }
    }

    /**
     * @return string
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getAge()
    {
        return $this->getCached("timecreated");
    }

    /**
     * @return string
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getCountry()
    {
        return $this->getCached("loccountrycode");
    }

    /**
     * @return string
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getLastAction()
    {
        return $this->getCached("lastlogoff", false);
    }

    /**
     * @return string
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getOnlineStatus()
    {
        return $this->getCached("personastate", false);
    }

    /**
     * @return string
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getPrimaryClanID()
    {
        return $this->getCached("primaryclanid");
    }

    /**
     * @return int
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getSteamLevel()
    {
        return $this->steam->api()->user()->getPlayerLevel($this->steamID, $this->usecache);
    }

    /**
     * @return AppList
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getGames()
    {
        return $this->steam->api()->user()->getPlayerGames($this->steamID, $this->usecache);
    }

    /**
     * @return UserList
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getFriends()
    {
        return $this->steam->api()->user()->getPlayerFriends($this->steamID, $this->usecache);
    }

    /**
     * @return GroupList
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getGroups()
    {
        return $this->steam->api()->user()->getUserGroups($this->steamID, $this->usecache);
    }

    /**
     * @param int $groupID
     *
     * @return bool
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function isGroupMember($groupID)
    {
        $groups = $this->getGroups();

        return $groups->has($groupID);
    }

    /**
     * @param string $appID
     * @param string $contextID
     *
     * @return \Component\Steam\Item\ItemList
     * @throws \App\HTTP\Exception\UrlException
     */
    public function getInventory($appID, $contextID)
    {
        return $this->steam->api()->user()->getPlayerInventory($this->getSteamID64(), $appID, $contextID);
    }

    /**
     * @param string $key
     * @param bool $cache
     *
     * @return string
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getCached($key, $cache = true)
    {
        if(!isset($this->cache) || !$cache)
        {
            $cache = $cache ? true : $this->usecache;
            $this->cache = $this->steam->api()->user()->getPlayerSummaries($this->getSteamID64(), $cache)[0];
        }

        return $this->cache[$key];
    }

    public function setCache($cache)
    {
        $this->cache = $cache;
    }
}