<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\User;

use App\Tools\ItemList;
use \Component\Steam\Exception\UserNotFoundException;
use Component\Steam\SteamFactory;

class UserList extends ItemList
{
    private $steam;

    public function __construct()
    {
        parent::__construct();
        $this->steam = SteamFactory::getSteamCore();
    }

    public function loadPlayerProfiles()
    {
        $steamIDs = $this->getSteamIDList();

        if(count($steamIDs) == 0)
        {
            return;
        }

        $profiles = $this->steam->api()->user()->getPlayerSummaries($steamIDs);

        $i = 0;
        foreach($this as $user)
        {
            /**
             * @var User $user
             */
            $user->setCache($profiles[$i]);
            $i++;
        }
    }

    /**
     * @return array
     */
    public function getSteamIDList()
    {
        $steamIDs = [];
        foreach($this as $user)
        {
            $steamIDs[] = $user->getSteamID64();
        }

        return $steamIDs;
    }

    /**
     * @param $steamID
     *
     * @return User
     *
     * @throws UserNotFoundException
     */
    public function find($steamID)
    {
        foreach($this as $profile)
        {
            if($profile->getSteamID64() == $steamID)
            {
                return $profile;
            }
        }

        throw new UserNotFoundException();
    }

    /**
     * @param string|User $steamID
     *
     * @return bool
     */
    public function has($steamID)
    {
        if(is_object($steamID))
        {
            $steamID = $steamID->getSteamID64();
        }

        try
        {
            $this->find($steamID);

            return true;
        }
        catch(UserNotFoundException $e)
        {
            return false;
        }
    }
}