<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Item;

class MarketItem extends SuperItem
{
    public function __construct($appID, $market_name)
    {
        parent::__construct();
        $this->appID = $appID;
        $this->market_name = $market_name;
    }

    public function getData($name = NULL, $force = true)
    {
        $data = [
            "appID" => $this->getAppID(), "market_name" => $this->getMarketName(), "image" => $this->getImage($force),
            "background" => $this->getBackgroundColor(), "data" => $this->getItemInfo(),
            "color" => $this->getColor(), "price" => $this->getMarketPrice($force),
            "market_volume" => $this->getMarketVolume($force), "raw_image" => $this->getRawImageCode($force)
        ];

        if(isset($name))
        {
            return $data[$name];
        }
        else
        {
            return $data;
        }
    }
}