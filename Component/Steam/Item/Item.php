<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Item;

class Item extends SuperItem
{
    public function __construct($appID, $contextID, $market_name, $assetID, $amount = 1)
    {
        parent::__construct();
        $this->appID = $appID;
        $this->contextID = $contextID;
        $this->market_name = $market_name;
        $this->assetID = $assetID;
        $this->amount = $amount;
    }

    public function getData($name = NULL, $force = true)
    {
        $data = [
            "appID" => $this->getAppID(), "contextID" => $this->getContextID(),
            "market_name" => $this->getMarketName(), "assetID" => $this->getAssetID(),
            "image" => $this->getImage($force), "background" => $this->getBackgroundColor(),
            "color" => $this->getColor(), "price" => $this->getMarketPrice($force),
            "market_volume" => $this->getMarketVolume($force), "raw_image" => $this->getRawImageCode($force),
            "data" => $this->getItemInfo(), "amount" => $this->getAmount()
        ];

        if(isset($this->classID) && isset($this->instanceID))
        {
            $data["classID"] = $this->classID;
            $data["instanceID"] = $this->instanceID;
        }

        if(isset($name))
        {
            return $data[$name];
        }
        else
        {
            return $data;
        }
    }
}