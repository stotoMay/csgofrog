<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Item;

use Component\Steam\SteamFactory;

class SuperItem
{
    protected $steam;

    protected $appID;
    protected $contextID;
    protected $assetID;
    protected $market_name;
    protected $amount;

    protected $classID;
    protected $instanceID;

    protected $color;
    protected $background;
    protected $image;

    protected $market_volume;
    protected $market_price;

    private $data;

    public function __construct()
    {
        $this->steam = SteamFactory::getSteamCore();
        $this->color = "#D2D2D2";
        $this->background = "#292929";

        $this->data = [];
    }

    /**
     * @return string
     */
    public function getMarketName()
    {
        return $this->market_name;
    }

    /**
     * @param bool $force
     *
     * @return string
     */
    public function getImage($force = true)
    {
        if(!empty($this->image))
        {
            return $this->image;
        }
        elseif(isset($this->classID) && isset($this->instanceID))
        {
            return "https://steamcommunity-a.akamaihd.net/economy/image/class/".$this->appID."/".$this->classID."/";
        }
        elseif($force)
        {
            $this->loadInfo();

            return $this->getImage(false);
        }
        else
        {
            return "";
        }
    }

    /**
     * @param bool $force
     *
     * @return string
     */
    public function getRawImageCode($force = true)
    {
        if(!empty($this->image))
        {
            return $this->image;
        }
        elseif($force)
        {
            $this->loadInfo();

            return $this->getRawImageCode(false);
        }
        else
        {
            return $this->image;
        }
    }


    /**
     * @return string
     */
    public function getAppID()
    {
        return $this->appID;
    }

    /**
     * @return string
     */
    public function getContextID()
    {
        return $this->contextID;
    }

    /**
     * @return string
     */
    public function getAssetID()
    {
        return $this->assetID;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getClassID()
    {
        return $this->classID;
    }

    /**
     * @return string
     */
    public function getInstanceID()
    {
        return $this->instanceID;
    }


    /**
     * @return string
     */
    public function getBackgroundColor()
    {
        return $this->background;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }


    /**
     * @param bool $force
     *
     * @return float
     */
    public function getMarketPrice($force = true)
    {
        if(!isset($this->market_price) && $force)
        {
            $this->loadInfo();
        }

        return $this->market_price;
    }

    /**
     * @param bool $force
     *
     * @return int
     */
    public function getMarketVolume($force = true)
    {
        if(!isset($this->market_volume) && $force)
        {
            $this->loadInfo();
        }

        return $this->market_volume;
    }

    /**
     * @return array
     */
    public function getItemInfo()
    {
        return $this->data;
    }


    /**
     * @param string $classID
     * @param string $instanceID
     */
    public function setClassItem($classID, $instanceID)
    {
        $this->classID = $classID;
        $this->instanceID = $instanceID;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        if(isset($data["market_name"]))
        {
            $this->market_name = $data["market_name"];
        }
        if(isset($data["market_volume"]))
        {
            $this->market_volume = $data["market_volume"];
        }
        if(isset($data["price"]))
        {
            $this->market_price = $data["price"];
        }
        if(isset($data["raw_image"]) && !isset($this->image))
        {
            $this->image = $data["raw_image"];
        }
        if(isset($data["color"]) && !isset($this->color))
        {
            $this->color = $data["color"];
        }
        if(isset($data["background"]) && !isset($this->background))
        {
            $this->background = $data["background"];
        }
        if(isset($data["data"]))
        {
            $this->data = $data["data"];
        }
    }

    public function loadInfo()
    {
        $this->setData($this->steam->item()->getMarket($this->getAppID(), $this->getMarketName()));

        if(!isset($this->market_price))
        {
            $this->market_price = 0;
            $this->market_volume = 0;
        }
        if(!isset($this->image))
        {
            $this->image = "";
        }
    }
}