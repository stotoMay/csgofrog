<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Item;

class ItemAsset extends SuperItem
{
    public function __construct($appID, $contextID, $assetID, $amount = 1)
    {
        parent::__construct();

        $this->appID = $appID;
        $this->contextID = $contextID;
        $this->assetID = $assetID;
        $this->amount = $amount;
    }

    public function getData($name = NULL)
    {
        $data = [
            "appID" => $this->getAppID(), "contextID" => $this->getContextID(), "assetID" => $this->getAssetID(), "amount" => $this->getAmount()
        ];

        if(isset($this->classID) && isset($this->instanceID))
        {
            $data["classID"] = $this->classID;
            $data["instanceID"] = $this->instanceID;
        }

        $data["data"] = $this->getItemInfo();

        if(isset($name))
        {
            return $data[$name];
        }
        else
        {
            return $data;
        }
    }
}