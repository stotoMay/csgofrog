<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Item;

use Component\Steam\SteamFactory;

class ItemList extends \App\Tools\ItemList
{
    private $steam;

    public function __construct()
    {
        parent::__construct();
        $this->steam = SteamFactory::getSteamCore();
    }

    public function getTotalPrices()
    {
        return $this->steam->item()->filling($this);
    }

    public function queueItemPrices()
    {
        $this->steam->item()->queue($this);
    }

    public function removeAsset($appID, $contextID, $assetID)
    {
        foreach($this as $key => $item)
        {
            if($item->getAssetID() == $assetID && $appID == $item->getAppID() && $contextID == $item->getContextID())
            {
                $this->remove($key);
            }
        }
    }

    public function removeMarketItem($appID, $market_name)
    {
        foreach($this as $key => $item)
        {
            if($item->getAppID() == $appID && $market_name == $item->getMarketName())
            {
                $this->remove($key);
            }
        }
    }

    public function toArray($force = true)
    {
        if($force)
        {
            $this->steam->item()->filling($this);
        }

        $list = [];
        foreach($this as $item)
        {
            $list[] = $item->getData(NULL, $force);
        }

        return $list;
    }
}