<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Item;

use App\Core\App;
use Component\Steam\Core;

class PriceHandler
{
    private $steam;
    private $app;

    private $queue;

    private $pricecache;
    private $idcache;

    public function __construct(Core $steam, App $app)
    {
        $this->steam = $steam;
        $this->app = $app;

        $this->queue = [];

        $this->pricecache = [];
        $this->idcache = [];
    }

    /**
     * @param int $appID
     * @param string $market_name
     *
     * @return bool
     */
    public function updateItemPrice($appID, $market_name)
    {
        $itemID = $this->getItemID($appID, $market_name);

        $this->app->database()->query("SET time_zone = '+00:00'");

        $history = $this->app->database()->select("steam_price_history")
            ->column([
                ["str" => "DATE_FORMAT(FROM_UNIXTIME(dateline), '%Y-%m-%d')", "alias" => "day"],
                ["str" => "SUM(price) / COUNT(*)", "alias" => "price"], "volume"
            ])
            ->where(["dateline[>=]" => time() - 3600 * 24 * 30, "itemID" => $itemID])
            ->group("day")->fetchAssoc();

        $prices = [];
        foreach($history as $row)
        {
            $prices[] = round($row["price"]);
        }

        for($i = 0; $i < 3; $i++)
        {
            if(count($prices) == 0)
            {
                return false;
            }

            $average = array_sum($prices)/count($prices);

            foreach($prices as $key => $value)
            {
                if($value/$average < 2)
                {
                    continue;
                }

                unset($prices[$key]);
            }

            $prices = array_values($prices);
        }

        $volsum = [];
        foreach($history as $row)
        {
            if($row["volume"] !== NULL)
            {
                $volsum[] = $row["volume"];
            }
        }

        $lastprice = NULL;
        $trendsum = [];
        foreach($prices as $value)
        {
            if(isset($lastprice))
            {
                $trendsum[] = ($value - $lastprice)/100;
            }

            $lastprice = $value;
        }

        if(count($prices) == 0)
        {
            return false;
        }

        $price = min(round(array_sum($prices)/count($prices)), round($history[count($history)-1]["price"]));

        $volume = 0;
        if(count($volsum) > 0)
        {
            $volume = round(array_sum($volsum)/ count($volsum));
        }

        $trend = 0;
        if(count($trendsum) > 0)
        {
            $trend = round(array_sum($trendsum) / count($trendsum), 2);
        }

        if($price <= 0)
        {
            return false;
        }

        $this->app->database()->update("steam_market")
            ->set(["market_price" => $price, "market_volume" => $volume, "trend" => $trend])
            ->where(["id" => $itemID]);

        return true;
    }

    /**
     * @param int $appID
     * @param string $market_name
     * @param float $price
     * @param int $volume
     */
    public function addItemPrice($appID, $market_name, $price, $volume = NULL)
    {
        if($price <= 0)
        {
            return;
        }

        $itemID = $this->getItemID($appID, $market_name);

        $this->app->database()->insert("steam_price_history")->set([
            "itemID" => $itemID, "price" => $price * 100, "dateline" => time(), "volume" => $volume
        ]);

        $this->updateItemPrice($appID, $market_name);
    }

    /**
     * @param int $appID
     * @param string $market_name
     *
     * @return int
     */
    public function getItemID($appID, $market_name)
    {
        if(isset($this->idcache[$appID][$market_name]))
        {
            return $this->idcache[$appID][$market_name];
        }

        $item = $this->app->database()->select("steam_market")
            ->column(["id"])->where(["appID" => $appID, "market_name" => $market_name])
            ->fetchRow();

        if(empty($item))
        {
            $itemID = $this->createMarketItem($appID, $market_name);
        }
        else
        {
            $itemID = $item["id"];
        }

        $this->cacheItemID($appID, $market_name, $itemID);

        return $itemID;
    }

    public function setMarketItem($appID, $market_name, $market_volume, $market_price, $name, $color, $background, $image)
    {
        $data = $this->app->database()->select("steam_market")->where(["appID" => $appID, "market_name" => $market_name])->fetchRow();

        if(empty($data))
        {
            $itemID = $this->createMarketItem($appID, $market_name, $market_price, $image, $color, $background, $name, $market_volume);
        }
        else
        {
            $itemID = $data["itemID"];
        }

        $this->cacheItemID($appID, $market_name, $itemID);
        $this->addItemPrice($appID, $market_name, $market_price, $market_volume);
    }

    /**
     * @param int $appID
     * @param string $market_name
     * @param int $itemID
     */
    public function cacheItemID($appID, $market_name, $itemID)
    {
        $this->idcache[$appID][$market_name] = $itemID;
    }

    /**
     * @param int $appID
     * @param string $market_name
     * @param int $price
     * @param string $image
     * @param string $color
     * @param string $background
     * @param string $name
     * @param int $market_volume
     *
     * @return int
     */
    public function createMarketItem($appID, $market_name, $price = 0, $image = "", $color = "#D2D2D2", $background = "#292929", $name = NULL, $market_volume = 0)
    {
        if(empty($name))
        {
            $name = $market_name;
        }

        if(empty($color))
        {
            $color = "#D2D2D2";
        }

        if(empty($background))
        {
            $background = "#292929";
        }

        return $this->app->database()->insert("steam_market")->set([
            "appID" => $appID, "market_name" => $market_name, "market_volume" => $market_volume, "market_price" => $price * 100, "name" => $name,
            "color" => $color, "background" => $background, "image" => $image, "updated" => time()
        ])->getID();
    }

    /**
     * @param ItemList|array $items
     * @param bool $overwrite
     *
     * @return float
     */
    public function filling(&$items, $overwrite = false)
    {
        $value = 0.0;
        foreach($items as $item)
        {
            if(is_object($item))
            {
                $this->queueMarket($item->getAppID(), $item->getMarketName());
            }
            elseif(is_array($item))
            {
                $this->queueMarket($item["appID"], $item["market_name"]);
            }
        }

        foreach($items as $key => $item)
        {
            if(is_object($item))
            {
                $i = $this->getMarket($item->getAppID(), $item->getMarketName());
                $item->setData($i);
            }
            elseif(is_array($item))
            {
                $i = $this->getMarket($item["appID"], $item["market_name"]);

                if($overwrite)
                {
                    $items[$key] = array_merge($items[$key], $i);
                }
                else
                {
                    $items[$key] = array_merge($i, $items[$key]);
                }
            }
            else
            {
                $i = ["price" => 0];
            }

            $value += $i["price"];
        }

        return $value;
    }

    /**
     * @param array|ItemList $items
     */
    public function queue($items)
    {
        foreach($items as $item)
        {
            if(is_object($item))
            {
                /**
                 * @var SuperItem $item
                 */
                $this->queueMarket($item->getAppID(), $item->getMarketName());
            }
            elseif(is_array($item))
            {
                $this->queueMarket($item["appID"], $item["market_name"]);
            }
        }
    }

    /**
     * @param int $appID
     * @param string $market_name
     *
     * @return array
     */
    public function getMarket($appID, $market_name)
    {
        if(empty($appID) || empty($market_name))
        {
            return [];
        }

        if(!isset($this->pricecache["market"][$appID][$market_name]))
        {
            $this->queueMarket($appID, $market_name);
            $this->dequeueMarket();
        }

        $item = $this->pricecache["market"][$appID][$market_name];

        if(!isset($item))
        {
            return ["appID" => $appID, "market_name" => $market_name];
        }
        else
        {
            return $item;
        }
    }

    /**
     * @param int $appID
     * @param string $market_name
     */
    public function queueMarket($appID, $market_name)
    {
        if(empty($appID) || empty($market_name))
        {
            return;
        }

        if(is_array($this->queue["market"][$appID][$market_name]))
        {
            return;
        }

        if(!isset($this->pricecache["market"][$appID][$market_name]))
        {
            $this->queue["market"][$appID][$market_name] = [];
        }
    }

    private function dequeueMarket()
    {
        if(!is_array($this->queue["market"]) || count($this->queue["market"]) == 0)
        {
            return;
        }

        $condition = ["OR" => []];
        $i = 0;
        $queuelist = [];
        foreach($this->queue["market"] as $appID => $a)
        {
            if(!is_array($a) || count($a) == 0)
            {
                continue;
            }

            $condition["OR"][$i] = ["appID" => $appID, "market_name" => []];

            foreach($a as $market_name => $b)
            {
                $condition["OR"][$i]["market_name"][] = $market_name;
                $queuelist[$appID.":".$market_name] = ["appID" => $appID, "market_name" => $market_name];
            }

            $i++;
        }

        $items = $this->app->database()->select("steam_market")->column([
            "steam_market.id" => "marketID", "steam_market.appID", "steam_market.market_name",
            "steam_market.market_volume", "steam_market.market_price" => "market_price",
            "steam_market.color", "steam_market.background", "steam_market.image", "steam_price.price" => "user_price",
        ])->leftJoin("steam_price", ["steam_market.id[=]steam_price.itemID"])->where($condition)
            ->fetchAssoc();

        foreach($items as $item)
        {
            if(is_numeric($item["market_price"]))
            {
                $item["market_price"] = $item["market_price"] / 100;
                $item["price"] = $item["market_price"];
            }

            if(is_numeric($item["user_price"]))
            {
                $item["user_price"] = $item["user_price"] / 100;
                $item["price"] = $item["user_price"];
            }

            $item["raw_image"] = $item["image"];
            $this->pricecache["market"][$item["appID"]][$item["market_name"]] = $item;
            unset($this->queue["market"][$item["appID"]][$item["market_name"]]);
            unset($queuelist[$item["appID"].":".$item["market_name"]]);
        }

        if(!is_array($items))
        {
            return;
        }

        foreach($queuelist as $item)
        {
            unset($this->queue["market"][$item["appID"]][$item["market_name"]]);
            $this->pricecache["market"][$item["appID"]][$item["market_name"]] = [];
        }
    }
}