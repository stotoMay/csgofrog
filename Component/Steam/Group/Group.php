<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Group;

use Component\Steam\Exception\GroupNotFoundException;
use Component\Steam\SteamFactory;

class Group
{
    private $steam;

    private $cache;
    private $usecache;

    private $steamID;

    /**
     * @param string $id
     * @param bool $usecache
     *
     * @throws GroupNotFoundException
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function __construct($id, $usecache = true)
    {
        $this->steam = SteamFactory::getSteamCore();

        $this->steamID = $this->steam->api()->group()->getSteamID64($id, $usecache);
        if(empty($this->steamID))
        {
            throw new GroupNotFoundException();
        }

        $this->usecache = $usecache;
    }

    /**
     * @return string
     */
    public function getSteamID64()
    {
        return $this->steamID;
    }

    /**
     * @return string
     */
    public function getSteamID32()
    {
        return bcsub($this->steamID, "103582791429521408");
    }

    /**
     * @return string
     */
    public function getSteamID()
    {
        $x = "STEAM_7";
        $y = ($this->getSteamID32() % 2) != 0 ? "1" : "0";
        $z = bcdiv(bcsub($this->steamID, bcadd("103582791429521408", $y)), "2");

        return $x.":".$y.":".$z;
    }

    /**
     * @return string
     */
    public function getSteamID3()
    {
        return "g:1:".$this->getSteamID32();
    }


    public function getName()
    {
        return $this->getCached("name");
    }

    public function getDescription()
    {
        return $this->getCached("summary");
    }

    public function getTotalMembers()
    {
        return $this->getCached("membercount");
    }

    public function getMemberList($page = NULL)
    {
        if(!isset($page))
        {
            $result = [];
            $x = 1;
            while($x <= $this->cache["pages"])
            {
                $result = array_merge($result, $this->getMemberList($x));
                $x++;
            }

            return $result;
        }

        return $this->steam->api()->group()->getGroupMembers($this->steamID, $page);
    }

    public function getTotalPages()
    {
        return $this->getCached("pages");
    }

    public function getCached($key, $usecache = true)
    {
        if(!isset($this->cache) || !$usecache)
        {
            $usecache = $usecache ? true : $this->usecache;
            $this->cache = $this->steam->api()->group()->getGroupSummaries($this->getSteamID64(), $usecache);
        }

        return $this->cache[$key];
    }

    public function setCache($cache)
    {
        $this->cache = $cache;
    }
}