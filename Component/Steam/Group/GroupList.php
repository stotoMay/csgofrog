<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Group;

use App\Tools\ItemList;
use Component\Steam\Exception\GroupNotFoundException;
use Component\Steam\SteamFactory;

class GroupList extends ItemList
{
    private $steam;

    public function __construct()
    {
        parent::__construct();
        $this->steam = SteamFactory::getSteamCore();
    }

    /**
     * @return array
     */
    public function getGroupIDList()
    {
        $groupIDs = [];
        foreach($this as $group)
        {
            $groupIDs[] = $group->getSteamID64();
        }

        return $groupIDs;
    }

    /**
     * @param $groupID
     *
     * @return Group
     *
     * @throws GroupNotFoundException
     */
    public function find($groupID)
    {
        foreach($this as $group)
        {
            if($group->getSteamID64() == $groupID)
            {
                return $group;
            }
        }

        throw new GroupNotFoundException();
    }

    /**
     * @param string|Group $groupID
     *
     * @return bool
     */
    public function has($groupID)
    {
        if(is_object($groupID))
        {
            $groupID = $groupID->getSteamID64();
        }

        try
        {
            $this->find($groupID);

            return true;
        }
        catch(GroupNotFoundException $e)
        {
            return false;
        }
    }
}