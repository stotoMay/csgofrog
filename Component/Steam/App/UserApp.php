<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\App;

class UserApp extends App
{
    private $user;
    private $playtime;

    /**
     * @param string $appID
     * @param \Component\Steam\User\User $user
     * @param int $playtime
     */
    public function __construct($appID, $user, $playtime)
    {
        parent::__construct($appID);
        $this->user = $user;
        $this->playtime = $playtime;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPlayTime()
    {
        return $this->playtime;
    }
}