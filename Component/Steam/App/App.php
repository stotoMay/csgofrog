<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\App;

use Component\Steam\SteamFactory;

class App
{
    protected $appID;

    /**
     * @param string $appID
     */
    public function __construct($appID)
    {
        $this->appID = $appID;
        $this->steam = SteamFactory::getSteamCore();
    }

    /**
     * @return string
     */
    public function getAppID()
    {
        return $this->appID;
    }

    /**
     * @return string
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getAppName()
    {
        return $this->steam->api()->steam()->getAppName($this->getAppID());
    }
}