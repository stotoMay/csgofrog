<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\App;

use App\Tools\ItemList;
use Component\Steam\Exception\AppNotFoundException;
use Component\Steam\SteamFactory;

class AppList extends ItemList
{
    private $steam;

    public function __construct()
    {
        parent::__construct();
        $this->steam = SteamFactory::getSteamCore();
    }

    /**
     * @return array
     */
    public function getAppIDList()
    {
        $appIDs = [];
        foreach($this as $app)
        {
            $appIDs[] = $app->getAppID();
        }

        return $appIDs;
    }

    /**
     * @param $appID
     *
     * @return App|UserApp
     *
     * @throws AppNotFoundException
     */
    public function find($appID)
    {
        foreach($this as $app)
        {
            if($app->getAppID() == $appID)
            {
                return $app;
            }
        }

        throw new AppNotFoundException();
    }

    /**
     * @param string $appID
     *
     * @return bool
     */
    public function has($appID)
    {
        try
        {
            $this->find($appID);

            return true;
        }
        catch(AppNotFoundException $e)
        {
            return false;
        }
    }
}