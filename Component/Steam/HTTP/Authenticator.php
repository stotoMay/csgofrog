<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\HTTP;

use Component\Steam\Exception\AddNumberException;
use Component\Steam\Exception\AlreadyLinkedException;
use Component\Steam\Exception\AuthenticatorRemoveException;
use Component\Steam\Exception\ConfirmationException;
use Component\Steam\Exception\InvalidSMSCodeException;
use Component\Steam\Exception\LinkAuthenticatorException;
use Component\Steam\Exception\MaxRetryException;

class Authenticator
{
    private $client;
    private $data;
    private $deviceID;
    private $confirmed;

    /**
     * @param Client $client
     * @param array $data
     * @param string $deviceID
     * @param bool $confirmed
     */
    public function __construct(Client $client, $data = NULL, $deviceID = NULL, $confirmed = false)
    {
        $this->client = $client;
        $this->data = $data;
        $this->deviceID = $deviceID;
        $this->confirmed = $confirmed;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return bool
     */
    public function hasAuthenticatorAttatched()
    {
        $post = [
            "op" => "has_phone", "arg" => "null", "sessionid" => $this->client->getSessionID()
        ];

        try
        {
            $response = $this->client->post("https://steamcommunity.com/steamguard/phoneajax", $post);

            $json = @json_decode($response->getResponseContent(), true);
        }
        catch(MaxRetryException $e)
        {
            return false;
        }

        if(isset($json) && $json["has_phone"])
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @param $number
     *
     * @return bool
     *
     * @throws AddNumberException
     * @throws AlreadyLinkedException
     */
    public function addTelephoneNumber($number)
    {
        if($this->confirmed && isset($this->deviceID))
        {
            throw new AlreadyLinkedException();
        }

        $post = [
            "op" => "add_phone_number", "arg" => $number, "sessionid" => $this->client->getSessionID()
        ];

        try
        {
            $response = $this->client->post("https://steamcommunity.com/steamguard/phoneajax", $post);
        }
        catch(MaxRetryException $e)
        {
            throw new AddNumberException();
        }

        $json = @json_decode($response->getResponseContent(), true);

        if(isset($json) && $json["success"])
        {
            return true;
        }
        else
        {
            throw new AddNumberException($json["error_text"]);
        }
    }

    /**
     * @return array
     *
     * @throws AlreadyLinkedException
     * @throws LinkAuthenticatorException
     */
    public function linkMobileAuthenticator()
    {
        if($this->confirmed && isset($this->deviceID))
        {
            throw new AlreadyLinkedException();
        }

        $deviceID = $this->generateDeviceID();

        $post = [
            "access_token" => $this->client->getOAuthToken(), "steamid" => $this->client->getSteamID(),
            "authenticator_type" => "1", "device_identifier" => $deviceID, "sms_phone_id" => "1"
        ];

        try
        {
            $response = $this->client->oAuthRequest("ITwoFactorService", "AddAuthenticator", [], $post);
        }
        catch(MaxRetryException $e)
        {
            throw new LinkAuthenticatorException();
        }

        if($response->getStatusCode() != 200)
        {
            throw new LinkAuthenticatorException();
        }

        $json = @json_decode($response->getResponseContent(), true);

        if(is_array($json) && $json["response"]["status"] == 1)
        {
            $this->deviceID = $deviceID;
            $this->data = $json["response"];

            return $this->data;
        }
        else
        {
            throw new LinkAuthenticatorException("account already linked");
        }
    }

    /**
     * @param $code
     *
     * @return bool
     *
     * @throws AlreadyLinkedException
     * @throws InvalidSMSCodeException
     * @throws LinkAuthenticatorException
     */
    public function finalizeAuthenticatorLink($code)
    {
        if($this->confirmed && isset($this->deviceID))
        {
            throw new AlreadyLinkedException();
        }

        if(!isset($this->deviceID))
        {
            throw new LinkAuthenticatorException();
        }

        $post = [
            "steamid" => $this->client->getSteamID(), "access_token" => $this->client->getOAuthToken(),
            "activation_code" => $code, "authenticator_code" => ""
        ];

        $tries = 0;
        while($tries <= 30)
        {
            $post["authenticator_time"] = time();

            try
            {
                $finalize = $this->client->oAuthRequest("ITwoFactorService", "FinalizeAddAuthenticator", [], $post);
            }
            catch(MaxRetryException $e)
            {
                throw new LinkAuthenticatorException();
            }

            if($finalize->getStatusCode() != 200)
            {
                continue;
            }

            $response = @json_decode($finalize->getResponseContent(), true);

            if(!is_array($response))
            {
                continue;
            }

            if($response["response"]["status"] == 89)
            {
                throw new InvalidSMSCodeException();
            }

            if(!$response["response"]["success"])
            {
                throw new LinkAuthenticatorException();
            }

            if($response["response"]["want_more"])
            {
                $post["activation_code"] = "";
                $post["authenticator_code"] = $this->generateSteamGuardCode($post["authenticator_time"]);
                $tries++;
                continue;
            }

            return true;
        }

        throw new LinkAuthenticatorException();
    }

    /**
     * @return bool
     *
     * @throws AuthenticatorRemoveException
     */
    public function removeMobileAuthenticator()
    {
        if(!isset($this->deviceID) || !is_array($this->data))
        {
            throw new AuthenticatorRemoveException();
        }

        $post = [
            "steamid" => $this->client->getSteamID(), "access_token" => $this->client->getOAuthToken(),
            "steamguard_scheme" => "2", "revocation_code" => $this->data["revocation_code"]
        ];

        try
        {
            $response = $this->client->oAuthRequest("ITwoFactorService", "RemoveAuthenticator", [], $post);
        }
        catch(MaxRetryException $e)
        {
            throw new AuthenticatorRemoveException();
        }

        if($response->getStatusCode() != 200)
        {
            throw new AuthenticatorRemoveException();
        }

        $json = @json_decode($response->getResponseContent(), true);

        if(is_array($json) && $json["response"]["success"] == true)
        {
            return true;
        }
        else
        {
            throw new AuthenticatorRemoveException();
        }
    }

    /**
     * @return bool
     */
    public function is_connected()
    {
        return isset($this->deviceID);
    }

    /**
     * @return bool
     */
    public function is_attatched()
    {
        return $this->confirmed;
    }

    /**
     * @return string
     */
    public function getDeviceID()
    {
        return $this->deviceID;
    }


    /**
     * @param int $time
     *
     * @return string
     */
    public function generateSteamGuardCode($time = NULL)
    {
        if(!isset($this->data["shared_secret"]))
        {
            return NULL;
        }

        if(!isset($time))
        {
            $time = time();
        }

        $time = intval($time / 30);
        $steamGuardCodeTranslations = [
            50, 51, 52, 53, 54, 55, 56, 57, 66, 67, 68, 70, 71, 72, 74, 75, 77, 78, 80, 81, 82, 84, 86, 87, 88, 89
        ];

        $bts = pack('N*', 0).pack('N*', $time);
        $sharedSecret = base64_decode($this->data["shared_secret"]);

        $hash = hash_hmac("sha1", $bts, $sharedSecret);
        $hmac = [];
        foreach(str_split($hash, 2) as $hex)
        {
            $hmac[] = hexdec($hex);
        }

        $offset = $hmac[19] & 0xf;
        $code = ($hmac[$offset] & 0x7F) << 24 | ($hmac[$offset + 1] & 0xFF) << 16 | ($hmac[$offset + 2] & 0xFF) << 8 | ($hmac[$offset + 3] & 0xFF);

        $steamCode = "";
        for($i = 0; $i < 5; ++$i)
        {
            $steamCode .= chr($steamGuardCodeTranslations[$code % count($steamGuardCodeTranslations)]);
            $code = floor($code / count($steamGuardCodeTranslations));
        }

        return $steamCode;
    }

    /**
     * @param int $time
     *
     * @return array
     *
     * @throws ConfirmationException
     */
    public function getConfirmations($time = NULL)
    {
        if(!isset($this->data["identity_secret"]))
        {
            throw new ConfirmationException();
        }

        try
        {
            $response = $this->client->get($this->generateConfirmationURL("conf", NULL, $time));
        }
        catch(MaxRetryException $e)
        {
            throw new ConfirmationException();
        }

        if($response->getStatusCode() != 200)
        {
            throw new ConfirmationException();
        }

        $html = $response->getResponseContent();

        preg_match_all('#data-confid="(\\d+)"#', $html, $confIDs);
        preg_match_all('#data-key="(\\d+)"#', $html, $confKeys);
        preg_match_all('#<div>((Confirm|Trade with|Sell -) .+)</div>#', $html, $confDescrs);

        $actionTranslator = ["Trade with" => "trade", "Sell -" => "sell", "Confirm" => "confirm"];
        $result = [];
        foreach($confKeys[1] as $i => $confkey)
        {
            $result[] = [
                "id" => $confIDs[1][$i], "key" => $confkey, "action" => $actionTranslator[$confDescrs[2][$i]],
                "subject" => $confDescrs[1][$i]
            ];
        }

        return $result;
    }

    /**
     * @param string $action accpeted | sent | sold
     * @param int $interval
     * @param int $time
     *
     * @throws ConfirmationException
     */
    public function confirmAction($action, $interval = 30, $time = NULL)
    {
        if(!isset($this->data["identity_secret"]))
        {
            throw new ConfirmationException();
        }

        $confirmations = $this->getConfirmations($time);
        foreach($confirmations as $conf)
        {
            if($conf["action"] == $action)
            {
                $this->sendConfirmationAjax($conf["id"], $conf["key"], "allow");
            }
        }

        unset($interval);
    }

    /**
     * @param int $confirmationID
     * @param int $key
     * @param string $operation allow | cancel
     *
     * @throws ConfirmationException
     */
    public function sendConfirmationAjax($confirmationID, $key, $operation)
    {
        if(!isset($this->data["identity_secret"]))
        {
            throw new ConfirmationException();
        }

        $url = $this->generateConfirmationURL($operation, "mobileconf/ajaxop");
        $url .= "&op=".$operation."&cid=".$confirmationID."&ck=".$key;

        try
        {
            $response = $this->client->credentials($url, $this->generateConfirmationURL("conf"));
        }
        catch(MaxRetryException $e)
        {
            throw new ConfirmationException();
        }

        if($response->getStatusCode() != 200)
        {
            throw new ConfirmationException();
        }

        $json = @json_decode($response->getResponseContent(), true);

        if($json["success"] != true)
        {
            throw new ConfirmationException();
        }
    }

    /**
     * @param string $tag
     * @param string $url
     * @param int $time
     *
     * @return string
     */
    private function generateConfirmationURL($tag, $url = NULL, $time = NULL)
    {
        if(!isset($this->data["identity_secret"]))
        {
            return NULL;
        }

        if(!isset($url))
        {
            $url = "https://steamcommunity.com/mobileconf/conf";
        }
        else
        {
            $url = "https://steamcommunity.com/".$url;
        }

        if(!isset($time))
        {
            $time = time();
        }

        $bts = pack('N*', 0).pack('N*', $time).$tag;
        $confirmationHash = urlencode(base64_encode(hash_hmac("sha1", $bts, base64_decode($this->data["identity_secret"]), true)));

        $url .= "?p=".$this->deviceID."&a=".$this->client->getSteamID()."&k=".$confirmationHash."&t=".$time."&m=android&tag=".$tag;

        return $url;
    }

    /**
     * @return string
     */
    private function generateDeviceID()
    {
        return "android:".sha1(str_random(20));
    }
}