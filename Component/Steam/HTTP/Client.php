<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\HTTP;

use Component\Steam\Exception\ClientSessionException;
use Component\Steam\Exception\MaxRetryException;
use App\HTTP\Exception\UrlException;

class Client
{
    private $logincookies;
    private $tmpcookies;

    private $oAuthToken;
    private $steamID;

    private $login;
    private $secure;
    private $refreshed;

    /**
     * @param string $steamID
     * @param string $login
     * @param string $secure
     * @param string $oAuthToken
     * @param array $cookies
     */
    public function __construct($steamID, $login, $secure, $oAuthToken, $cookies = [])
    {
        $this->oAuthToken = $oAuthToken;
        $this->steamID = $steamID;

        if(!isset($cookies["sessionid"]))
        {
            $cookies["sessionid"] = str_random(24, "abcdef0123456789");
        }

        $this->refreshed = false;
        $this->login = $login;
        $this->secure = $secure;

        $this->logincookies = [
            "steamLogin" => $steamID."||".$login, "steamLoginSecure" => $steamID."||".$secure, "Steam_Language" => "english",
            "timezoneOffset" => "7200,0", "mobileClientVersion" => "0 (2.1.3)", "mobileClient" => "android",
            "webTradeEligibility" => '{"allowed":1,"allowed_at_time":0,"steamguard_required_days":15,"sales_this_year":0,"max_sales_per_year":-1,"forms_requested":0,"new_device_cooldown_days":7}'
        ];
        $this->tmpcookies = $cookies;
    }

    /**
     * @param string $url
     *
     * @param int $retry
     * @param bool $refresh
     *
     * @return SteamResponse
     *
     * @throws MaxRetryException
     */
    public function get($url, $retry = 3, $refresh = true)
    {
        $curl = $this->buildRequest($url);

        return $this->executeRequest($curl, $retry, $refresh);
    }

    /**
     * @param string $url
     * @param array $post
     *
     * @param int $retry
     * @param bool $refresh
     *
     * @return SteamResponse
     * @throws MaxRetryException
     */
    public function post($url, $post, $retry = 3, $refresh = true)
    {
        $curl = $this->buildRequest($url)->setPostData($post);

        return $this->executeRequest($curl, $retry, $refresh);
    }

    /**
     * @param string $url
     * @param string $referer
     * @param array $post
     *
     * @param int $retry
     * @param bool $refresh
     *
     * @return SteamResponse
     * @throws MaxRetryException
     */
    public function credentials($url, $referer, $post = NULL, $retry = 3, $refresh = true)
    {
        $curl = $this->buildRequest($url)->setReferer($referer);

        if(is_array($post))
        {
            $curl->setPostData($post);
        }

        return $this->executeRequest($curl, $retry, $refresh);
    }

    /**
     * @param string $class
     * @param string $method
     * @param array $args
     * @param array $post
     * @param int $version
     *
     * @param int $retry
     * @param bool $refresh
     *
     * @return SteamResponse
     * @throws MaxRetryException
     */
    public function oAuthRequest($class, $method, $args = [], $post = NULL, $version = 1, $retry = 3, $refresh = true)
    {
        if(!isset($post))
        {
            $post = ["access_token" => $this->getOAuthToken()];
        }

        $url = "https://api.steampowered.com/".$class."/".$method."/v".$version."/";

        if(count($args) > 0)
        {
            $params = [];
            foreach($args as $key => $value)
            {
                $params[] = $key."=".$value;
            }
            $url .= "?".implode("&", $params);
        }

        return $this->post($url, $post, $retry, $refresh);
    }


    /**
     * @return string
     */
    public function getSessionID()
    {
        return $this->tmpcookies["sessionid"];
    }

    /**
     * @return string
     */
    public function getOAuthToken()
    {
        return $this->oAuthToken;
    }

    /**
     * @return string
     */
    public function getSteamID()
    {
        return $this->steamID;
    }


    /**
     * @return array
     */
    public function getBrowserCookies()
    {
        return $this->tmpcookies;
    }

    /**
     * @return string
     */
    public function getSteamLoginCookie()
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getSteamLoginSecureCookie()
    {
        return $this->secure;
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function getCookie($name)
    {
        return $this->tmpcookies[$name];
    }

    /**
     * @return bool
     */
    public function is_refreshed()
    {
        return $this->refreshed;
    }

    public function refreshSession()
    {
        $response = $this->oAuthRequest("IMobileAuthService", "GetWGToken", [], ["access_token" => $this->getOAuthToken()], 1, 1, false);

        if($response->getStatusCode() != 200)
        {
            throw new ClientSessionException();
        }

        $json = @json_decode($response->getResponseContent(), true);

        if(!isset($json["response"]["token"]))
        {
            throw new ClientSessionException();
        }

        $login = $json["response"]["token"];
        $secure = $json["response"]["token_secure"];

        if($login != $this->login || $secure != $this->secure)
        {
            $this->refreshed = true;
            $this->login = $login;
            $this->secure = $secure;
        }

        $this->logincookies["steamLogin"] = $this->steamID."||".$login;
        $this->logincookies["steamLoginSecure"] = $this->steamID."||".$secure;
    }

    /**
     * @param array $cookies
     */
    public function setCookie($cookies)
    {
        $blacklist = [
            "steamLogin", "steamLoginSecure", "steamMachineAuth".$this->steamID, "Steam_Language", "timezoneOffset"
        ];
        foreach($blacklist as $key)
        {
            unset($cookies[$key]);
        }
        $this->tmpcookies = array_merge($this->tmpcookies, $cookies);
    }


    /**
     * @param string $url
     *
     * @return \App\Tools\Request
     */
    private function buildRequest($url)
    {
        $curl = RequestBuilder::buildStandardSteamRequest($url);
        $curl->setCookies(array_merge($this->tmpcookies, $this->logincookies));

        return $curl;
    }

    /**
     * @param \App\Tools\Request $curl
     *
     * @param int $retry
     * @param bool $refresh
     *
     * @return SteamResponse
     *
     * @throws MaxRetryException
     */
    private function executeRequest($curl, $retry = 3, $refresh = true)
    {
        if($retry <= 0)
        {
            throw new MaxRetryException();
        }

        try
        {
            $curl->execute();
        }
        catch(UrlException $e)
        {
            if($e->getCode() == 1 && $refresh)
            {
                try
                {
                    $this->refreshSession();
                }
                catch(ClientSessionException $e)
                {
                }

                $curl->setCookies(array_merge($this->tmpcookies, $this->logincookies));

                $curl->clear();
                $refresh = false;
            }

            if($retry - 1 <= 0)
            {
                throw new MaxRetryException($e->getMessage(), $e->getCode());
            }

            return $this->executeRequest($curl, $retry - 1, $refresh);
        }

        $this->setCookie($curl->getResponseCookies());

        if(($curl->getStatusCode() != 200 && $curl->getStatusCode() <= 500) || is_int(strpos($curl->getDestination(), "/login")))
        {
            if($refresh)
            {
                try
                {
                    $this->refreshSession();
                }
                catch(ClientSessionException $e)
                {
                }

                $refresh = false;
            }

            if($retry == 1)
            {
                return new SteamResponse($curl->getResponseContent(), $curl->getStatusCode(), $curl->getDestination(), $curl->getResponseHeader());
            }

            $curl->setCookies(array_merge($this->tmpcookies, $this->logincookies));
            $curl->clear();

            return $this->executeRequest($curl, $retry - 1, $refresh);
        }

        if($curl->getStatusCode() > 500)
        {
            $curl->clear();

            return $this->executeRequest($curl, $retry - 1, $refresh);
        }

        return new SteamResponse($curl->getResponseContent(), $curl->getStatusCode(), $curl->getDestination(), $curl->getResponseHeader());
    }
}