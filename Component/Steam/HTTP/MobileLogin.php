<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\HTTP;

use Component\Steam\Exception\CaptchaException;
use Component\Steam\Exception\LoginException;
use Component\Steam\Exception\SteamGuardEmailException;
use Component\Steam\Exception\TwoFactorException;
use Component\Steam\SteamFactory;

class MobileLogin extends Login
{
    /**
     * @return array
     * @throws CaptchaException
     * @throws LoginException
     * @throws SteamGuardEmailException
     * @throws TwoFactorException
     * @throws \Component\Steam\Exception\MaxRetryException
     */
    public function auth()
    {
        $steam = SteamFactory::getSteamCore();

        $post = [
            "username" => $this->username, "password" => $this->encrypt($this->password),

            "twofactorcode" => $this->twofactor ? $this->twofactor : "",

            "captchagid" => is_array($this->captcha) ? $this->captcha["token"] : "-1",
            "captcha_text" => is_array($this->captcha) ? $this->captcha["string"] : "",

            "emailsteamid" => is_array($this->email) ? $this->email["steamID"] : "",
            "emailauth" => is_array($this->email) ? $this->email["string"] : "",

            "oauth_client_id" => "DE45CD61", "oauth_scope" => "read_profile write_profile read_client write_client",

            "rsatimestamp" => $this->time, "loginfriendlyname" => "#login_emailauth_friendlyname_mobile",
            "remember_login" => "true", "donotcache" => time()
        ];

        $loginFile = RequestBuilder::buildLoginSteamRequest("https://steamcommunity.com/login/dologin/");
        $loginFile->setPostData($post);
        $loginFile->setCookies($this->getLoginCookies());

        $login = @json_decode($loginFile->getResponseContent(), true);

        if(isset($login) && $login["success"] == true && $login["login_complete"] == true)
        {
            $oAuth = json_decode($login["oauth"], true);

            return [
                "steamID" => $oAuth["steamid"], "oAuth" => $oAuth["oauth_token"],
                "loginSecure" => $oAuth["wgtoken_secure"], "login" => $oAuth["wgtoken"]
            ];
        }
        else
        {
            if($login["emailauth_needed"] == true)
            {
                throw new SteamGuardEmailException($login["message"], $login["emailsteamid"]);
            }
            elseif($login["captcha_needed"] == true)
            {
                $image = base64_encode($steam->api()->url("https://steamcommunity.com/public/captcha.php?gid=".$login["captcha_gid"]));

                throw new CaptchaException($login["message"], $login["captcha_gid"], $image);
            }
            elseif($login["requires_twofactor"] == true)
            {
                throw new TwoFactorException($login["message"]);
            }
            else
            {
                throw new LoginException($login["message"]);
            }
        }
    }

    private function getLoginCookies()
    {
        $data = $this->getMachineAuth();

        $data["mobileClientVersion"] = "0 (2.1.3)";
        $data["mobileClient"] = "android";
        $data["Steam_Language"] = "english";

        return $data;
    }
}