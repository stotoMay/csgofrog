<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\HTTP;

use App\Bootstrap\Factory;
use App\Tools\Request;
use Component\Steam\Exception\CaptchaException;
use Component\Steam\Exception\LoginException;
use Component\Steam\Exception\SteamGuardEmailException;
use Component\Steam\Exception\TwoFactorException;
use Component\Steam\SteamFactory;

class DesktopLogin extends Login
{
    /**
     * @return array
     * @throws CaptchaException
     * @throws LoginException
     * @throws SteamGuardEmailException
     * @throws TwoFactorException
     * @throws \App\Database\Exception\ParseException
     * @throws \Component\Steam\Exception\MaxRetryException
     */
    public function auth()
    {
        $steam = SteamFactory::getSteamCore();
        $app = Factory::getApplication();

        $post = [
            'username' => $this->username, 'twofactorcode' => "", 'password' => $this->encrypt($this->password),
            'rsatimestamp' => $this->time, 'loginfriendlyname' => $this->name, 'remember_login' => "true",
            'donotcache' => time(), 'captchagid' => -1, 'captcha_text' => "", 'emailsteamid' => ""
        ];

        if(is_array($this->captcha))
        {
            $post['captchagid'] = $this->captcha["token"];
            $post["captcha_text"] = $this->captcha["string"];
        }
        if(is_array($this->email))
        {
            $post['emailauth'] = $this->email["string"];
            $post["emailsteamid"] = $this->email["steamID"];
        }

        $loginFile = new Request("https://steamcommunity.com/login/dologin/");
        $loginFile->setPostData($post);
        $loginFile->setCookies($this->getMachineAuth());

        $login = @json_decode($loginFile->getResponseContent(), true);

        if($login["success"] == true && $login["login_complete"] == true)
        {
            $exists = $app->database()->select("steam_auth")->where([
                "steamID" => $login["transfer_parameters"]["steamid"]
            ])->exists();

            if(!$exists && !empty($login["transfer_parameters"]["webcookie"]))
            {
                $app->database()->insert("steam_auth")->set([
                    "steamID" => $login["transfer_parameters"]["steamid"],
                    "auth" => $steam->crypt()->encrypt($login["transfer_parameters"]["webcookie"]),
                    "dateline" => time()
                ]);
            }

            $data = [];
            $cookies = array_map("urldecode", $loginFile->getResponseCookies());
            foreach($cookies as $name => $cookie)
            {
                $split = explode("||", $cookie);
                if(!empty($split[1]))
                {
                    $data[$name] = $split[1];
                }
            }

            return [
                "steamID" => $login["transfer_parameters"]["steamid"], "login" => $data["steamLogin"],
                "loginSecure" => $data["steamLoginSecure"], "rememberLogin" => $data["steamRememberLogin"]
            ];
        }
        else
        {
            if($login["emailauth_needed"] == true)
            {
                throw new SteamGuardEmailException($login["message"], $login["emailsteamid"]);
            }
            elseif($login["captcha_needed"] == true)
            {
                $image = base64_encode($steam->api()
                    ->url("https://steamcommunity.com/public/captcha.php?gid=".$login["captcha_gid"]));
                throw new CaptchaException($login["message"], $login["captcha_gid"], $image);
            }
            elseif($login["requires_twofactor"] == true)
            {
                throw new TwoFactorException($login["message"]);
            }
            else
            {
                throw new LoginException($login["message"]);
            }
        }
    }
}