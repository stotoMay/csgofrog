<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\HTTP;

class SteamResponse
{
    private $content;
    private $statuscode;
    private $destination;
    private $header;

    /**
     * @param string $content
     * @param int $statuscode
     * @param string $destination
     * @param array $header
     */
    public function __construct($content, $statuscode, $destination, $header)
    {
        $this->content = $content;
        $this->statuscode = $statuscode;
        $this->destination = $destination;
        $this->header = $header;
    }

    /**
     * @return string
     */
    public function getResponseContent()
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statuscode;
    }

    /**
     * @return string
     */
    public function getDestinationURL()
    {
        return $this->destination;
    }

    /**
     * @return array
     */
    public function getHeader()
    {
        return $this->header;
    }
}