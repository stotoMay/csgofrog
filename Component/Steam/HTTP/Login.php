<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\HTTP;

use App\Bootstrap\Factory;
use App\Tools\Request;
use Component\Steam\Exception\LoginException;
use Component\Steam\SteamFactory;
use phpseclib\Crypt\RSA;
use phpseclib\Math\BigInteger;

class Login
{
    protected $time;
    protected $name;

    protected $username;
    protected $password;

    protected $captcha;
    protected $email;
    protected $twofactor;

    /**
     * @param string $username
     * @param string $password
     * @param string $name
     */
    public function __construct($username, $password, $name = "PC-34657")
    {
        $this->username = $username;
        $this->password = $password;
        $this->name = $name;
    }

    /**
     * @param string $token
     * @param string $str
     */
    public function setCaptchaCode($token, $str)
    {
        $this->captcha = ["token" => $token, "string" => $str];
    }

    /**
     * @param string $steamID
     * @param string $str
     */
    public function setEmailCode($steamID, $str)
    {
        $this->email = ["steamID" => $steamID, "string" => $str];
    }

    public function setTwoFactorCode($code)
    {
        $this->twofactor = $code;
    }

    protected function encrypt($str)
    {
        $keys = $this->getRSAKeys();

        $n = new BigInteger($keys["publickey_mod"], 16);
        $e = new BigInteger($keys["publickey_exp"], 16);

        $key = ["modulus" => $n, "publicExponent" => $e];

        $rsa = new RSA();
        $rsa->loadKey($key, RSA::PUBLIC_FORMAT_RAW);
        $rsa->setPublicKey($key);
        $rsa->setEncryptionMode(RSA::ENCRYPTION_PKCS1);

        return base64_encode($rsa->encrypt($str));
    }

    /**
     * @return array
     * @throws LoginException
     * @throws \App\HTTP\Exception\UrlException
     */
    protected function getRSAKeys()
    {
        $data = new Request("https://steamcommunity.com/login/getrsakey/");
        $data->setPostData(["username" => $this->username, "donotcache" => time()]);
        $keys = @json_decode($data->getResponseContent(), true);

        if($keys["success"] != true)
        {
            throw new LoginException("invalid rsa keys");
        }
        else
        {
            $this->time = $keys["timestamp"];

            return $keys;
        }
    }

    /**
     * @return array
     * @throws \App\Database\Exception\ParseException
     */
    protected function getMachineAuth()
    {
        $app = Factory::getApplication();
        $steam = SteamFactory::getSteamCore();

        $data = $app->tcache()->get("steam_machine_auth");
        if(!is_array($data))
        {
            $data = [];

            $query = $app->database()->select("steam_auth")->fetchAssoc();
            foreach($query as $row)
            {
                $data["steamMachineAuth".$row["steamID"]] = $steam->crypt()->decrypt($row["auth"]);
            }

            $app->tcache()->set("steam_machine_auth", $data);
        }

        return $data;
    }
}