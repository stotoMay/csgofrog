<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\HTTP;

use App\Tools\Request;

class RequestBuilder
{
    public static function buildStandardSteamRequest($url)
    {
        $curl = new Request($url);
        $curl->setUserAgent("Mozilla/5.0 (Linux; U; Android 4.1.1; en-us; Google Nexus 4 - 4.1.1 - API 16 - 768x1280 Build/JRO03S) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
        $curl->setHeader("X-Requested-With", "com.valvesoftware.android.steam.community");
        $curl->setReferer("https://steamcommunity.com");

        return $curl;
    }

    public static function buildLoginSteamRequest($url)
    {
        $curl = self::buildStandardSteamRequest($url);
        $curl->setReferer("https://steamcommunity.com/mobilelogin?oauth_client_id=DE45CD61&oauth_scope=read_profile%20write_profile%20read_client%20write_client");

        return $curl;
    }
}