<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css" />

    <script type="text/javascript">
        var g_botsteamid = "{$botSteamID}";
        var g_botid = "{$botID}";
        var g_inventorytypes = {json_encode(getSteamApps())}
    </script>

    <script src="/Component/Steam/Admin/Template/Static/JScript/steam.js" type="text/javascript"></script>
    <script src="/Component/Steam/Admin/Template/Static/JScript/tradeoffer.js" type="text/javascript"></script>

    <title>{language("steam", "bots", "title_send")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{language("steam", "bots", "breadcrumb_bots")}</a></li>
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{$botname}</a></li>
    <li class="breadcrumb-item active">{language("steam", "bots", "breadcrumb_newoffer")}</li>
</ol>

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        <ul class="nav nav-tabs">
            <li>
                <a href="/admin/steambotconfig/offers/received/{$botID}/filter/{$match}">{language("steam", "bots", "offers_receive")}</a>
            </li>
            <li>
                <a href="/admin/steambotconfig/offers/sent/{$botID}/fillter/{$match}">{language("steam", "bots", "offers_send")}</a>
            </li>
            <li class="active"><a href="/admin/steambotconfig/send/{$botID}">{language("steam", "bots", "offers_new")}</a></li>
        </ul>
    </div>

    <div class="panel-body">
        <div id="inventorytypes" style="margin-bottom: 20px;"></div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body" style="height:180px;overflow-y:auto;" id="myitems-inventory">

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body" style="height:180px;overflow-y:auto;" id="myitems-trade">

                </div>
            </div>
        </div>

        <div class="input-group" style="margin-bottom: 20px;">
            <input type="text" class="form-control" placeholder='{language("steam", "bots", "tradeurl")}' value="{$tradeurl}" id="tradeurl">
            <div class="input-group-btn">
                <button class="btn btn-primary" id="tradeurl-update" type="submit">{language("steam", "bots", "update_tradeurl")}</button>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body" style="height:180px;overflow-y:auto;" id="hisitems-inventory">

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body" style="height:180px;overflow-y:auto;" id="hisitems-trade">

                </div>
            </div>
        </div>

        <a href="javascript:" id="offer-send-button" class="btn btn-primary">{language("steam", "bots", "button_send")}</a>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-borderless table-marginless">
                {foreach $bots as $bot}
                    {if $bot.id != $botID}
                        <tr class="boturl">
                            <td>{$bot.steam.personaname}</td>
                            <td>{$bot.tradeurl}</td>
                            <td>
                                <a href="javascript:" data-tradeurl="{$bot.tradeurl}" class="btn btn-primary">{language("steam", "bots", "button_copy")}</a>
                            </td>
                        </tr>
                    {/if}
                {/foreach}
            </table>
        </div>
    </div>
</div>
{includetemplate="footer.tpl"}
</body>
</html>