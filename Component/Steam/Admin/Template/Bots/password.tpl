<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("steam", "bots", "title_password")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{language("steam", "bots", "breadcrumb_bots")}</a></li>
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{$botname}</a></li>
    <li class="breadcrumb-item"><a href="/admin/steambotconfig/account/{$botID}">{language("steam", "bots", "breadcrumb_account")}</a></li>
    <li class="breadcrumb-item active">{language("steam", "bots", "breadcrumb_changepassword")}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-heading">{language("steam", "bots", "header_password")}</div>
    <div class="panel-body">
        <form method="post" action="/admin/steambotconfig/changepassword/{$botID}">
            <table class="table table-borderless">
                <tr>
                    <td><label for="username">{language("steam", "bots", "botusername")}</label></td>
                    <td>{$username}</td>
                </tr>
                <tr>
                    <td><label for="password">{language("steam", "bots", "botpassword")}</label></td>
                    <td><input type="password" id="password" class="form-control" name="password" /></td>
                </tr>
            </table>
            <input type="hidden" name="token" value="{token()}">
            <input type="submit" class="btn btn-primary" value='{language("submit")}'>
        </form>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>