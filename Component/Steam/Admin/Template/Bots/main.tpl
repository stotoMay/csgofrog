<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("steam", "bots", "title_main")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}
<div class="panel panel-default">
    <div class="panel-heading">{language("steam", "bots", "header_main")}</div>
    <div class="panel-body">
        <form method="post" action="/admin/steambotconfig/delete">
            <div class="table-responsive">
                <table class="table table-borderless">
                    {foreach $bots as $bot}
                        <tr>
                            <td><img src="{$bot.steam.avatar}"></td>
                            <td><a href="{$bot.steam.profileurl}">{$bot.steam.personaname}</a></td>
                            {if permission("trade", "controller", "steambotconfig") == 1}
                                {if $bot.tradeable == 1}
                                    <td>
                                        <a href="/admin/steambotconfig/offers/received/{$bot.id}" class="btn btn-default">{language("steam", "bots", "tradeoffers")}</a>
                                    </td>
                                {else}
                                    <td>{language("steam", "bots", "tradeable")} {dateformat($bot.tradetime)}</td>
                                {/if}
                            {/if}
                            {if permission("view", "controller", "steamauthenticator") == 1}
                                <td>
                                    <a href="/admin/steamauthenticator/bot/{$bot.id}" class="btn btn-default">{language("steam", "bots", "authenticator")}</a>
                                </td>
                            {/if}
                            {if permission("account", "controller", "steambotconfig") == 1}
                                <td>
                                    <a href="/admin/steambotconfig/account/{$bot.id}" class="btn btn-default">{language("steam", "bots", "account")}</a>
                                </td>
                            {/if}
                            {if permission("delete", "controller", "steambotconfig") == 1}
                                <td>
                                    <button name="botID" value="{$bot.id}" class="btn btn-primary" onclick="return confirm('{language("steam", "bots", "message", "bot_delete_confirmation")}')">{language("steam", "bots", "delete")}</button>
                                </td>
                            {/if}
                        </tr>
                    {/foreach}
                </table>
            </div>
            <input type="hidden" name="token" value="{token()}">
        </form>

        {if permission("add", "controller", "steambotconfig") == 1}
            <a href="/admin/steambotconfig/add" class="btn btn-default">{language("steam", "bots", "addbot")}</a>
        {/if}
    </div>
</div>
{includetemplate="footer.tpl"}
</body>
</html>