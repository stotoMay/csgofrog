<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("steam", "bots", "title_add")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{language("steam", "bots", "breadcrumb_bots")}</a></li>
    <li class="breadcrumb-item active">{language("steam", "bots", "breadcrumb_add")}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-heading">{language("steam", "bots", "header_addbot")}</div>
    <div class="panel-body">
        <form method="post" action="/admin/steambotconfig/add">
            <table class="table table-borderless">
                {if $email_needed}
                    <tr>
                        <td><label for="steamguardstring">{language("steam", "bots", "steamguard")}</label></td>
                        <td><input type="text" id="steamguardstring" class="form-control" name="steamguardstring" />
                        </td>
                    </tr>
                    <tr>
                        <td><input type="hidden" name="steamID" value="{$steamID}"></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="hidden" name="username" value="{$username}"><input type="hidden" name="password" value="{$password}">
                        </td>
                    </tr>
                {else}
                    <tr>
                        <td><label for="username">{language("steam", "bots", "botusername")}</label></td>
                        <td><input value="{$username}" class="form-control" type="text" id="username" name="username" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="password">{language("steam", "bots", "botpassword")}</label></td>
                        <td><input type="password" id="password" class="form-control" name="password" /></td>
                    </tr>
                    {if $captcha_needed}
                        <tr>
                            <td><label for="captcha"><img src="data:image/png;base64,{$captchaImage}" /></label></td>
                            <td><input id="captcha" class="form-control" type="text" name="captchastring" /></td>
                        </tr>
                        <tr>
                            <td><input type="hidden" name="captchaID" value="{$captchaID}"></td>
                        </tr>
                    {/if}
                    {if $twofactor_needed}
                        <tr>
                            <td><label for="twofactorcode">{language("steam", "bots", "twofactorcode")}</label></td>
                            <td><input type="text" id="twofactorcode" class="form-control" name="twofactorcode" /></td>
                        </tr>
                    {/if}
                {/if}
            </table>
            <input type="hidden" name="token" value="{token()}">
            <input type="submit" class="btn btn-primary" value='{language("submit")}'>
        </form>
    </div>
</div>
{includetemplate="footer.tpl"}
</body>
</html>