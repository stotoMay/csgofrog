<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("steam", "bots", "title_account")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{language("steam", "bots", "breadcrumb_bots")}</a></li>
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{$botname}</a></li>
    <li class="breadcrumb-item active">{language("steam", "bots", "breadcrumb_account")}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="col-md-3 center-block">
            {if permission("refresh", "controller", "steambotconfig") == 1}
                <a href="/admin/steambotconfig/updatesteamdata/{$botID}" class="btn btn-primary btn-margin">{language("steam", "bots", "updatesteamdata")}</a>
            {/if}
        </div>
        <div class="col-md-3 center-block">
            {if permission("refresh", "controller", "steambotconfig") == 1}
                <a href="/admin/steambotconfig/refresh/{$botID}" class="btn btn-primary btn-margin">{language("steam", "bots", "refreshbotsession")}</a>
            {/if}
        </div>
        <div class="col-md-3 center-block">
            {if permission("refresh", "controller", "steambotconfig") == 1}
                <a href="/admin/steambotconfig/relog/{$botID}" class="btn btn-primary btn-margin">{language("steam", "bots", "relogbot")}</a>
            {/if}
        </div>
        <div class="col-md-3 center-block">
            {if permission("add", "controller", "steambotconfig") == 1}
                <a href="/admin/steambotconfig/changepassword/{$botID}" class="btn btn-primary btn-margin">{language("steam", "bots", "changepassword")}</a>
            {/if}
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <form method="post" action="/admin/steambotconfig/account/{$botID}">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tr>
                        <td><label for="tradeurl">{language("steam", "bots", "tradeurl")}</label></td>
                        <td><input type="text" value="{$tradeurl}" id="tradeurl" name="tradeurl" class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td><label for="apikey">{language("steam", "bots", "apikey")}</label></td>
                        <td><input type="text" value="{$apikey}" id="apikey" name="apikey" class="form-control"></td>
                    </tr>
                    <tr>
                        <td><label for="inventories">{language("steam", "bots", "inventories")}</label></td>
                        <td>
                            <select name="inventories[]" class="form-control" id="inventories" multiple>
                                {foreach getSteamApps() as $a}
                                    {if array_search($a.id, $inventories)}
                                        <option value="{$a.appID}:{$a.contextID}" selected>{$a.name}</option>
                                    {else}
                                        <option value="{$a.appID}:{$a.contextID}">{$a.name}</option>
                                    {/if}
                                {/foreach}
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            <input type="hidden" name="token" value="{token()}">
            <input type="submit" class="btn btn-primary" value='{language("submit")}'>
        </form>
    </div>
</div>

{sethook("pageBotAccount", $botID)}

{includetemplate="footer.tpl"}
</body>
</html>