<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css" />
    <script src="/Component/Steam/Admin/Template/Static/JScript/steam.js" type="text/javascript"></script>

    <title>{language("steam", "bots", "title_offers")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{language("steam", "bots", "breadcrumb_bots")}</a></li>
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{$botname}</a></li>
    <li class="breadcrumb-item active">
        {if $mode == "sent"}
            {language("steam", "bots", "breadcrumb_senttrades")}
        {else}
            {language("steam", "bots", "breadcrumb_receivedtrades")}
        {/if}
    </li>
</ol>

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        <ul class="nav nav-tabs">
            <li class='{if $mode == "received"}active{/if}'>
                <a href="/admin/steambotconfig/offers/received/{$botID}/filter/{$match}">{language("steam", "bots", "offers_receive")}</a>
            </li>
            <li class='{if $mode == "sent"}active{/if}'>
                <a href="/admin/steambotconfig/offers/sent/{$botID}/fillter/{$match}">{language("steam", "bots", "offers_send")}</a>
            </li>
            <li><a href="/admin/steambotconfig/send/{$botID}">{language("steam", "bots", "offers_new")}</a></li>
        </ul>
    </div>

    <div class="panel-body" style="min-height: 150px;">
        {if $offers->length() == 0}
            <div class="center-block" style="margin-top:40px;">{language("steam", "bots", "no_offers")}</div>
        {/if}

        {foreach $offers as $offer}
            <div class="panel panel-default tradeoffer-{$offer->getOfferID()}">
                <div class="panel-body">
                    <div class="col-md-6">
                        <img src="{$offer->getBot()->user()->getAvatar()}">
                        <a href="{$offer->getBot()->user()->getProfileURL()}">{$offer->getBot()->user()->getName()}</a>

                        <div style="height:180px;overflow-y:auto;padding:10px;">
                            {foreach $offer->getBotItems() as $item}
                                {renderItem($item)}
                            {/foreach}
                        </div>

                        <div style="margin-bottom:10px;">
                            {number_format($offer->getBotItems()->getTotalPrices())}$
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img src="{$offer->getPartner()->getAvatar()}">
                        <a href="{$offer->getPartner()->getProfileURL()}">{$offer->getPartner()->getName()}</a>

                        <div style="height:180px;overflow-y:auto;padding:10px 0;">
                            {foreach $offer->getPartnerItems() as $item}
                                {renderItem($item)}
                            {/foreach}
                        </div>

                        <div style="margin-bottom:10px;">
                            {number_format($offer->getPartnerItems()->getTotalPrices())}$
                        </div>
                    </div>
                    {if $offer->is_pending()}
                        {if $mode == "sent"}
                            <a href="javascript:;" onclick="sendTradeAjax('cancel', this, '{$botID}', '{$offer->getOfferID()}');" class="btn btn-primary">{language("steam", "bots", "button_cancel")}</a>
                        {else}
                            <div class="col-xs-6">
                                <a href="javascript:;" onclick="sendTradeAjax('accept', this, '{$botID}', '{$offer->getOfferID()}', '{$offer->getPartner()->getSteamID64()}');" class="btn btn-primary">{language("steam", "bots", "button_accept")}</a>
                            </div>
                            <div class="col-xs-6">
                                <a href="javascript:;" onclick="sendTradeAjax('decline', this, '{$botID}', '{$offer->getOfferID()}');" class="btn btn-primary">{language("steam", "bots", "button_decline")}</a>
                            </div>
                        {/if}
                    {/if}
                </div>
            </div>
        {/foreach}
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>