function Trade(a_steamID, a_botID) {
    var steamID = a_steamID;
    var botID = a_botID;

    var myitems = [];
    var hisitems = [];

    var appID = g_inventorytypes[0]["appID"];
    var contextID = g_inventorytypes[0]["contextID"];

    var partnerurl = null;

    this.setTradeURL = function (str) {
        if (partnerurl != str) {
            hisitems = [];
        }
        partnerurl = str;
    };

    this.setInventory = function (a_appID, a_contextID) {
        appID = a_appID;
        contextID = a_contextID;
    };

    this.updateMyInventory = function () {
        $.ajax({
            url: "/admin/steamajax/botinventory",
            dataType: "json",

            method: "post",
            data: {
                "appID": appID,
                "contextID": contextID,
                "botID": botID, "token": g_sessionid
            },

            success: onMyInventoryUpdate,
            error: function () {
                onMyInventoryUpdate({"success": false});
            }
        });
    };

    this.updateHisInventory = function () {
        $.ajax({
            url: "/admin/steamajax/inventory",
            dataType: "json",

            method: "post",
            data: {
                "appID": appID,
                "contextID": contextID,
                "tradeurl": partnerurl,
                "token": g_sessionid
            },

            success: onHisInventoryUpdate,
            error: function () {
                onHisInventoryUpdate({"success": false});
            }
        });
    };

    this.clearTrade = function () {
        if (myitems.length > 0) {
            this.updateMyInventory();
        }

        if (hisitems.length > 0) {
            this.updateHisInventory();
        }

        myitems = [];
        hisitems = [];
    };

    this.addMyItem = function (assetID, amount) {
        myitems.push({
            "appID": appID,
            "contextID": contextID,
            "assetID": assetID,
            "amount": amount
        });
    };

    this.removeMyItem = function (a_appID, a_contextID, a_assetID, a_amount) {
        for (var i = 0; i < myitems.length; i++) {
            if (myitems[i]["appID"] == a_appID && myitems[i]["contextID"] == a_contextID && myitems[i]["assetID"] == a_assetID) {
                var a = myitems[i]["amount"] - a_amount;

                if (a <= 0) {
                    myitems.splice(i, 1);
                    break;
                }
                else {
                    myitems[i]["amount"] = a;
                }
            }
        }
    };

    this.addHisItem = function (assetID, amount) {
        hisitems.push({
            "appID": appID,
            "contextID": contextID,
            "assetID": assetID,
            "amount": amount
        });
    };

    this.removeHisItem = function (a_appID, a_contextID, a_assetID, a_amount) {
        for (var i = 0; i < hisitems.length; i++) {
            if (hisitems[i]["appID"] == a_appID && hisitems[i]["contextID"] == a_contextID && hisitems[i]["assetID"] == a_assetID) {
                var a = hisitems[i]["amount"] - a_amount;

                if (a <= 0) {
                    hisitems.splice(i, 1);
                    break;
                }
                else {
                    hisitems[i]["amount"] = a;
                }
            }
        }
    };

    this.isAdded = function (a_appID, a_contextID, a_assetID) {
        var i;

        for(i = 0; i < myitems.length; i++)
        {
            if(myitems[i].appID == a_appID && myitems[i].contextID == a_contextID && myitems[i].assetID == a_assetID) {
                return true;
            }
        }

        for(i = 0; i < hisitems.length; i++)
        {
            if(hisitems[i].appID == a_appID && hisitems[i].contextID == a_contextID && hisitems[i].assetID == a_assetID) {
                return true;
            }
        }

        return false;
    };

    this.execute = function () {
        $.ajax({
            url: "/admin/steamajax/sendoffer",
            dataType: "json",

            method: "post",
            data: {
                "botID": botID,
                "tradeurl": partnerurl,
                "myitems": JSON.stringify(myitems),
                "hisitems": JSON.stringify(hisitems),
                "token": g_sessionid
            },

            success: onTradeOfferComplete,
            error: function () {
                onTradeOfferComplete({"success": false, "error": "connection", "message": "Connection Problems"});
            }
        });
    };
}

function renderInventoryTypes() {
    var types = g_inventorytypes;
    var $select = $(document.createElement("select")).addClass("form-control");

    $.each(types, function (index, value) {
        var $option = $(document.createElement("option"));

        $option.html(value.name);
        $option.attr({
            "data-appid": value.appID,
            "data-contextid": value.contextID
        });

        $select.append($option);
    });

    return $select;
}

function renderLoadingGIF() {
    var $container = $(document.createElement("div"));
    $container.css({
        "width": 24,
        "height": 24,
        "position": "absolute",
        "top": "calc(50% - 12px)",
        "left": "calc(50% - 12px)",
        "font-size": 18
    }).html('<i class="fa fa-spin fa-circle-o-notch"></i>');

    return $container;
}

function appendMyItem($i) {
    $i.click(function () {
        if(trade.isAdded($i.attr("data-appid"), $i.attr("data-contextid"), $i.attr("data-assetid")))
        {
            $("#myitems-inventory").append($i);
            trade.removeMyItem($i.attr("data-appid"), $i.attr("data-contextid"), $i.attr("data-assetid"), 1);
        }
        else
        {
            $("#myitems-trade").append($i);
            trade.addMyItem($i.attr("data-assetid"), 1);
        }
    });
    $("#myitems-inventory").append($i);
}

function appendHisItem($i) {
    $i.click(function () {
        if(trade.isAdded($i.attr("data-appid"), $i.attr("data-contextid"), $i.attr("data-assetid")))
        {
            $("#hisitems-inventory").append($i);
            trade.removeHisItem($i.attr("data-appid"), $i.attr("data-contextid"), $i.attr("data-assetid"), 1);
        }
        else
        {
            $("#hisitems-trade").append($i);
            trade.addHisItem($i.attr("data-assetid"), 1);
        }
    });
    $("#hisitems-inventory").append($i);
}

function onMyInventoryUpdate(data) {
    var $myinventory = $("#myitems-inventory");

    if (data.success != true) {
        $myinventory.html("");

        return;
    }

    $myinventory.html("");
    $.each(data.response, function (key, item) {
        var $i = renderItem(item);

        if (trade.isAdded(item.appID, item.contextID, item.assetID) == false) {
            appendMyItem($i);
        }
    });
}

function onHisInventoryUpdate(data) {
    var $hisinventory = $("#hisitems-inventory");

    if (data.success != true) {
        $hisinventory.html("");

        return;
    }

    $hisinventory.html("");
    $.each(data.response, function (key, item) {
        var $i = renderItem(item);

        if (trade.isAdded(item.appID, item.contextID, item.assetID) == false) {
            appendHisItem($i);
        }
    });
}

function onTradeOfferComplete(data) {
    $sendbutton.removeClass("disabled").css("width", "").html(buttontext);

    if (!data.success) {
        print_error(data.message);
    }
    else {
        print_info(data.message);

        $("#myitems-trade").html("");
        $("#hisitems-trade").html("");

        trade.clearTrade();
    }
}

var trade = new Trade(g_botsteamid, g_botid);
var $sendbutton, buttontext;

$(document).ready(function () {
    var $tradeurl = $("#tradeurl");

    var $hisinventory = $("#hisitems-inventory");
    var $histrade = $("#hisitems-trade");
    var $myinventory = $("#myitems-inventory");

    $sendbutton = $("#offer-send-button");
    buttontext = $sendbutton.html();

    $myinventory.html(renderLoadingGIF());
    trade.updateMyInventory();
    if ($tradeurl.val()) {
        $hisinventory.html(renderLoadingGIF());
        trade.setTradeURL($tradeurl.val());
        trade.updateHisInventory();
    }

    $("#tradeurl-update").click(function () {
        $hisinventory.html(renderLoadingGIF());
        trade.setTradeURL($tradeurl.val());
        trade.updateHisInventory();
    });

    $sendbutton.click(function () {
        if ($sendbutton.hasClass("disabled")) {
            return;
        }

        $sendbutton.css("width", $sendbutton.innerWidth());
        $sendbutton.html('<i class="fa fa-spin fa-circle-o-notch"></i>');
        $sendbutton.addClass("disabled");

        trade.execute(true);
    });

    $(".boturl").find("a").click(function () {
        var $e = $(this);

        var tradeurl = $e.attr("data-tradeurl");

        trade.setTradeURL(tradeurl);
        $tradeurl.val(tradeurl);
        $hisinventory.html(renderLoadingGIF());
        trade.updateHisInventory();
        $histrade.html("");
    });

    var $select = renderInventoryTypes();
    $select.change(function () {
        var $selected = $(this).find("option:selected");
        trade.setInventory($selected.attr("data-appid"), $selected.attr("data-contextid"));

        $myinventory.html(renderLoadingGIF());
        if ($tradeurl.val()) {
            $hisinventory.html(renderLoadingGIF());
        }

        trade.updateMyInventory();
        trade.updateHisInventory();
    });

    $("#inventorytypes").html($select);
});