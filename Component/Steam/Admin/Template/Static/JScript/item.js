$(document).ready(function () {
    var $button_start = $("#itemupdate-start");
    var $button_stop = $("#itemupdate-stop");

    var $progress = $("#itemupdate-progress");

    var buttontext = $button_start.html();

    function onUpdateStart() {
        $button_start.addClass("disabled").css("width", $button_start.innerWidth());
        $button_start.html('<i class="fa fa-spin fa-circle-o-notch"></i>');

        $button_stop.removeClass("disabled");
        $progress.css("width", 0).html("");
    }

    function onUpdateStop() {
        $button_start.removeClass("disabled").css("width", "").html(buttontext);

        $button_stop.addClass("disabled");
    }

    var ajaxQueue = $({});
    var running = false;

    $button_start.click(function () {
        if ($button_start.hasClass("disabled")) {
            return;
        }

        var ajax_requests = [];

        running = true;
        onUpdateStart();

        function updatePage(iterator) {
            if (!(iterator in ajax_requests)) {
                running = false;
                onUpdateStop();

                return;
            }

            $.ajax({
                url: "/admin/steamitemconfig/updatemarketpage",
                dataType: "json", method: "post",
                data: {
                    "appID": ajax_requests[iterator]["appID"],
                    "page": ajax_requests[iterator]["page"],
                    "token": g_sessionid
                },

                success: function (data) {
                    if (!running) {
                        return;
                    }

                    if(data.success)
                    {
                        var percentage = (Math.round(((iterator + 1) / ajax_requests.length) * 10000) / 100).toFixed(2) + "%";

                        $progress.css("width", percentage).html(percentage);
                        updatePage(iterator + 1);
                    }
                    else
                    {
                        setTimeout(function () {
                            updatePage(iterator)
                        }, 30000);
                    }
                },

                error: function () {
                    setTimeout(function () {
                        updatePage(iterator)
                    }, 30000);
                }
            });
        }

        var icount = 0;
        for (var i = 0; i < g_inventorytypes.length; i++) {
            ajaxQueue.queue("itemupdate", function (next) {
                $.ajax({
                    url: "/admin/steamitemconfig/getmarketinfo",
                    dataType: "json", method: "post",
                    data: {"token": g_sessionid, "appID": g_inventorytypes[icount]["appID"]},

                    success: function (data) {
                        if (!data.success) {
                            onUpdateStop();
                            return;
                        }

                        for (var pageCount = 1; pageCount <= data["response"]["totalPages"]; pageCount++) {
                            ajax_requests.push({"appID": g_inventorytypes[icount]["appID"], "page": pageCount});
                        }
                    },

                    complete: function () {
                        icount++;
                        next();
                    }
                });
            });
        }

        ajaxQueue.queue("itemupdate", function () {
            updatePage(0);
        });

        ajaxQueue.dequeue("itemupdate");
    });

    $button_stop.click(function () {
        if ($button_stop.hasClass("disabled")) {
            return;
        }

        running = false;
        ajaxQueue.finish("itemupdate");
        onUpdateStop();
    });
});