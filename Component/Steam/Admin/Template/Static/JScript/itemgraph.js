$(document).ready(function () {
    var $container = $("#item-graph");

    var $canvas = $(document.createElement("canvas")).css({"width": "100%"});
    var $ctx = $canvas[0].getContext("2d");

    $container.append($canvas);

    var labels = [];
    var values = [];

    var firstvalue = null;
    var lastvalue = 0;

    for (var i = 0; i < g_itemgraph.length; i++) {
        if(firstvalue === null && g_itemgraph[i]["price"] > 0)
        {
            firstvalue = g_itemgraph[i]["price"];
        }

        labels.push(g_itemgraph[i]["day"]);
        values.push(g_itemgraph[i]["price"]);
        lastvalue = g_itemgraph[i]["price"];
    }

    var trendvar = (firstvalue+lastvalue)/2 - g_trend * (g_itemgraph.length/2);
    var trend = [];
    for (i = 0; i < g_itemgraph.length; i++) {
        trend.push(trendvar);
        trendvar += g_trend;
    }

    new Chart($ctx, {
        type: 'line',
        data: {
            "labels": labels, "datasets": [
                {"label": "Price", "data": values, "backgroundColor": "rgba(0,0,0,0)", "borderColor": "rgba(41,128,185,0.8)"},
                {"label": "Trend", "data": trend, "backgroundColor": "rgba(0,0,0,0)", "borderColor": "rgba(211,84,0,0.8)"}
            ]
        },
        options: {"responsive": true}
    });
});