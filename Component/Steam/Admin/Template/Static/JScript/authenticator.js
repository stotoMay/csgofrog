$(document).ready(function () {
    var $code = $("#twofactorcode");
    var $timer = $("#twofactortimer");

    var timer = -1;
    var counter = -1;

    function refreshToken() {
        $.ajax("/admin/steamauthenticator/twofactortoken/" + g_botID, {dataType: "json"}).done(function (data) {
            if (data.success != true) {
                return;
            }

            $code.html(data["token"]);
            counter = parseInt(data["update_in"]) + 1;

            clearTimeout(timer);
            refreshTimer();
        });
    }

    function refreshTimer() {
        if (counter <= 0) {
            refreshToken();

            return;
        }

        var tstr = counter;
        if (counter < 10) {
            tstr = "0" + tstr;
        }

        $timer.html(tstr + "s");
        counter--;

        if (counter >= 0) {
            timer = setTimeout(refreshTimer, 1000);
        }
    }

    refreshTimer();
});