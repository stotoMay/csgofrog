function renderItem(item, width, height, ahref) {
    if (!width) {
        width = 72;
    }
    if (!height) {
        height = 72;
    }

    ahref = !!ahref;

    var $span = $(document.createElement("div"));
    var $img = $(document.createElement("img"));
    var $price = $(document.createElement("div"));

    $span.css({
        "background-color": item["background"],
        "border-color": item["color"],
        "width": width,
        "height": height
    });
    $span.attr({
        "data-appid": item.appID,
        "data-contextid": item.contextID,
        "data-assetid": item.assetID,
        "title": item.market_name,
        "class": "steam-item",
        "data-toggle": "tooltip"
    });

    $img.attr("src", item.image);
    $span.html($img);

    if (item.price) {
        $price.html("$" + parseFloat(item.price).toFixed(2));
        $price.css({
            "background-color": item["background"],
            "color": item["color"]
        }).addClass("steam-item-price");
        $span.append($price);
    }

    $span.tooltip();

    if(ahref)
    {
        var $a = $(document.createElement("a")).attr("href", "/admin/steamitemconfig/item/"+item["appID"]+"/"+encodeURIComponent(item["market_name"]));

        return $a.html($span);
    }
    else
    {
        return $span;
    }

}

function sendTradeAjax(operation, btn, botID, offerID, partnerID) {
    if (operation != "accept" && operation != "decline" && operation != "cancel") {
        return;
    }

    var $btn = $(btn);

    if ($btn.hasClass("disabled")) {
        return;
    }

    var buttontext = $btn.html();
    var buttonwidth = $btn.innerWidth();

    $btn.addClass("disabled").css("width", buttonwidth);

    $btn.html('<i class="fa fa-spin fa-circle-o-notch"></i>');

    $.ajax({
        "url": "/admin/steambotconfig/" + operation,
        "method": "post", "dataType": "json",
        "data": {
            "token": g_sessionid, "botID": botID,
            "offerID": offerID, "partnerID": partnerID
        }, "success": onRequestSuccess, "complete": onRequestComplete
    });

    function onRequestSuccess(data) {
        var $msg = $(document.createElement("div")).html(data["message"]);
        var $offer = $(".tradeoffer-" + offerID);

        if (data["success"]) {
            $offer.before($msg.addClass("alert").addClass("alert-success"));
            $offer.slideUp(500, function () {
                $offer.remove();
            });
        }
        else {
            $offer.before($msg.addClass("alert").addClass("alert-danger"));
        }

        setTimeout(function () {
            $msg.fadeOut(500);
        }, 8000)
    }

    function onRequestComplete() {
        $btn.html(buttontext);
        $btn.removeClass("disabled");
        $btn.css("width", "");
    }

    return true;
}

function sendConfirmationAjax(operation, btn, botID, confID, key) {
    if (operation == "accept") {
        operation = "accept";
    }
    else {
        operation = "decline";
    }

    var $btn = $(btn);

    if ($btn.hasClass("disabled")) {
        return;
    }

    var buttontext = $btn.html();
    var buttonwidth = $btn.innerWidth();

    $btn.addClass("disabled").css("width", buttonwidth);

    $btn.html('<i class="fa fa-spin fa-circle-o-notch"></i>');

    $.ajax({
        "url": "/admin/steamauthenticator/" + operation,
        "method": "post", "dataType": "json",
        "data": {
            "token": g_sessionid, "botID": botID,
            "confID": confID, "key": key
        }, "success": onRequestSuccess, "complete": onRequestComplete
    });

    function onRequestSuccess(data) {
        var $msg = $(document.createElement("div")).html(data["message"]);
        var $confirmation = $(".confirmation-" + confID);

        if (data["success"]) {
            $confirmation.before($msg.addClass("alert").addClass("alert-success"));
            $confirmation.slideUp(500, function () {
                $confirmation.remove();
            });
        }
        else {
            $confirmation.before($msg.addClass("alert").addClass("alert-danger"));
        }

        setTimeout(function () {
            $msg.fadeOut(500);
        }, 8000)
    }

    function onRequestComplete() {
        $btn.html(buttontext);
        $btn.removeClass("disabled");
        $btn.css("width", "");
    }

    return true;
}

function updateItem(btn, appID, market_name) {
    var $btn = $(btn);

    if ($btn.hasClass("disabled")) {
        return;
    }

    var buttontext = $btn.html();
    var buttonwidth = $btn.innerWidth();

    $btn.addClass("disabled").css("width", buttonwidth);

    $btn.html('<i class="fa fa-spin fa-circle-o-notch"></i>');

    $.ajax({
        "url": "/admin/steamitemconfig/updateitem",
        "method": "post", "dataType": "json",
        "data": {
            "token": g_sessionid, "appID": appID,
            "market_name": market_name
        }, "success": onRequestSuccess, "complete": onRequestComplete
    });

    function onRequestSuccess(data) {
        if (data["success"]) {
            location.reload();
        }
        else {
            print_error(data["message"]);
        }
    }

    function onRequestComplete() {
        $btn.html(buttontext);
        $btn.removeClass("disabled");
        $btn.css("width", "");
    }
}