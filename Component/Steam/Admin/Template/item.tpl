{if $ahref == true}
    <a href="/admin/steamitemconfig/item/{$item.appID}/{rawurlencode($item.market_name)}">
{/if}

<div title="{$item.market_name}" data-toggle="tooltip" class="steam-item" style="width:{$itemsizex}px;height:{$itemsizey}px;border:1px solid {$item.color};background-color:{$item.background};">
    <img src="{$item.image}">
    <div class="steam-item-price" style="color:{$item.color};background-color:{$item.background};">
        {if $item.price}
            ${number_format($item.price)}
        {else}
            NaN
        {/if}
    </div>
</div>

{if $ahref == true}
    </a>
{/if}