<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css" />

    <title>{language("steam", "items", "title_items")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        <ul class="nav nav-tabs">
            <li class='{if $mode != "priceless" && $mode != "banned"}active{/if}'>
                <a href="/admin/steamitemconfig">{language("steam", "items", "itemdatabase")}</a></li>
            <li class='{if $mode == "priceless"}active{/if}'>
                <a href="/admin/steamitemconfig?mode=priceless"> {language("steam", "items", "pricelessitems")}
                    {if $pricless_item_count > 0}
                        <span class="badge">{$pricless_item_count}</span>
                    {/if}
                </a>
            </li>
            <li class='{if $mode == "banned"}active{/if}'><a href="/admin/steamitemconfig?mode=banned">{language("steam", "items", "banneditems")}</a>
            </li>
            {if permission("refresh", "controller", "steamitemconfig") == 1}
                <li><a href="/admin/steamitemconfig/update">{language("steam", "items", "steammarket")}</a></li>
            {/if}
        </ul>
    </div>

    <div class="panel-body">
        <form action="/admin/steamitemconfig" method="get">
            <div class="input-group">
                <input type="text" class="form-control" placeholder='{language("steam", "items", "itemname")}' value="{htmlspecialchars($match)}" name="search" id="srch-term">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
            <input type="hidden" name="page" value="1">
            <input type="hidden" name="mode" value="{$mode}">
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-borderless">
                {foreach $items.results as $item}
                    <tr>
                        <td>{renderItem($item)}</td>
                        <td>{$item.appname}</td>
                        <td>{$item.name}</td>
                        {if $item.price == ""}
                            <td>NaN</td>
                        {else}
                            <td>${number_format($item.price)}</td>
                        {/if}
                        <td>{$item.market_volume}</td>
                        <td>
                            <a href="/admin/steamitemconfig/item/{$item.appID}/{rawurlencode($item.market_name)}" class="btn btn-default">{language("steam", "items", "details")}</a>
                        </td>
                    </tr>
                {/foreach}
            </table>
        </div>

        <div class="center-block">{pagination($items.pages, $items.current, $pageurl)}</div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>