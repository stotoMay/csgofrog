<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <script type="text/javascript">
        var g_inventorytypes = {json_encode(getSteamApps())}
    </script>
    <script type="text/javascript" src="/Component/Steam/Admin/Template/Static/JScript/item.js"></script>

    <title>{language("steam", "items", "title_update")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        <ul class="nav nav-tabs">
            <li><a href="/admin/steamitemconfig">{language("steam", "items", "itemdatabase")}</a></li>
            <li>
                <a href="/admin/steamitemconfig?mode=priceless">{language("steam", "items", "pricelessitems")}
                    {if $pricless_item_count > 0}
                        <span class="badge">{$pricless_item_count}</span>
                    {/if}
                </a>
            </li>
            <li><a href="/admin/steamitemconfig?mode=banned">{language("steam", "items", "banneditems")}</a></li>
            <li class="active"><a href="/admin/steamitemconfig/update">{language("steam", "items", "steammarket")}</a></li>
        </ul>
    </div>

    <div class="panel-body">
        <div class="progress">
            <div class="progress-bar" id="itemupdate-progress" style="width:0;"></div>
        </div>

        <a href="javascript:;" id="itemupdate-start" class="btn btn-primary">{language("steam", "items", "button_start")}</a>
        <a href="javascript:;" id="itemupdate-stop" class="btn btn-primary disabled">{language("steam", "items", "button_stop")}</a>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>