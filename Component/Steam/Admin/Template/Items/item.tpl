<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css" />
    <script type="text/javascript" src="/Component/Steam/Admin/Template/Static/JScript/steam.js"></script>

    <script type="text/javascript">
        g_itemgraph = {json_encode($graph)};
        g_trend = parseFloat('{$item.trend}');
    </script>
    <script type="text/javascript" src="/Component/Steam/Admin/Template/Static/JScript/itemgraph.js"></script>

    <title>{$item.name} - {language("steam", "items", "title_item")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/steamitemconfig">{language("steam", "items", "breadcrumb_items")}</a></li>
    <li class="breadcrumb-item active">{$item.name}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-heading">{$item.name}</div>
    <div class="panel-body">
        <div class="col-md-4">{renderItem($item, 128, 128)}</div>
        <div class="col-md-8">
            {$item.appname}<br><br>
            {$item.name}<br><br>
            {if permission("refresh", "controller", "steamitemconfig") == 1}
                <a href="#" onclick="updateItem(this, '{$item.appID}','{$item.market_name}');" class="btn btn-primary">{language("steam", "items", "button_updateprice")}</a>
            {/if}
        </div>

        <div style="display:block;clear:both;margin-bottom: 20px;"></div>

        <table class="table table-bordered" style="margin-top: 20px;">
            <tr>
                <td>{language("steam", "items", "market_name")}</td>
                <td>
                    <a href="https://steamcommunity.com/market/listings/{$item.appID}/{rawurlencode($item.market_name)}">{$item.market_name}</a>
                </td>
            </tr>
            <tr>
                <td>{language("steam", "items", "user_price")}</td>
                <td>
                    {if $item.user_price == ""}
                        NaN
                    {else}
                        ${number_format($item.user_price)}
                    {/if}
                </td>
            </tr>
            <tr>
                <td>{language("steam", "items", "market_price")}</td>
                <td>
                    {if $item.market_price == ""}
                        NaN
                    {else}
                        ${number_format($item.market_price)}
                    {/if}
                </td>
            </tr>
            <tr>
                <td>{language("steam", "items", "market_volume")}</td>
                <td>{$item.market_volume}</td>
            </tr>
        </table>

        <div id="item-graph"></div>
        <div style="display:block;clear:both;margin-bottom: 20px;"></div>

        {if permission("change", "controller", "steamitemconfig") == 1}
            {if $is_banned == false}
                <form method="post" action="/admin/steamitemconfig/item/{$item.appID}/{rawurlencode($item.market_name)}">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder='Item Price' name="price" value="{$item.user_price}">
                        <div class="input-group-btn">
                            <button class="btn btn-primary" type="submit">{language("steam", "items", "button_setprice")}</button>
                        </div>
                    </div>

                    <input type="hidden" name="token" value="{token()}">
                </form>
                <hr>
            {/if}
            <form method="post" action="/admin/steamitemconfig/item/{$item.appID}/{rawurlencode($item.market_name)}">
                <input type="hidden" name="token" value="{token()}" />

                {if $is_banned}
                    <button type="submit" name="price" value="" class="btn btn-primary">{language("steam", "items", "item_unban")}</button>
                {else}
                    <button type="submit" name="price" value="ban" class="btn btn-primary">{language("steam", "items", "item_ban")}</button>
                {/if}
            </form>
        {/if}
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>