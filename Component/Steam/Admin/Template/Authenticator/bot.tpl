<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <script type="text/javascript">
        var g_botID = "{$botID}";
    </script>

    <script src="/Component/Steam/Admin/Template/Static/JScript/steam.js" type="text/javascript"></script>
    <script src="/Component/Steam/Admin/Template/Static/JScript/authenticator.js" type="text/javascript"></script>

    <title>{$botname} - {language("steam", "authenticator", "title_bot")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{language("steam", "authenticator", "breadcrumb_bots")}</a></li>
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{$botname}</a></li>
    <li class="breadcrumb-item active">{language("steam", "authenticator", "breadcrumb_authenticator")}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="col-xs-4">{language("steam", "authenticator", "twofactorcode")}</div>
        <div class="col-xs-4" id="twofactorcode">{$twofactorcode}</div>
        <div class="col-xs-4" id="twofactortimer">00s</div>
    </div>
</div>

{if permission("confirm", "controller", "steamauthenticator") == 1}
    <div class="panel panel-default">
        <div class="panel-heading">{language("steam", "authenticator", "confirmations")}</div>
        <div class="panel-body">
            {if count($confirmations) > 0}
                {foreach $confirmations as $conf}
                    <div class="confirmation-{$conf.id}">
                        <div class="col-md-6">
                            {$conf.subject}
                        </div>
                        <div class="col-md-3">
                            <a href="javascript:;" onclick="sendConfirmationAjax('accept', this, '{$botID}', '{$conf.id}', '{$conf.key}')" class="btn btn-primary">{language("steam", "authenticator", "button_accept")}</a>
                        </div>
                        <div class="col-md-3">
                            <a href="javascript:;" onclick="sendConfirmationAjax('decline', this, '{$botID}', '{$conf.id}', '{$conf.key}')" class="btn btn-primary">{language("steam", "authenticator", "button_decline")}</a>
                        </div>
                    </div>
                {/foreach}
            {else}
                <div class="center-block">{language("steam", "authenticator", "no_confirmations")}</div>
            {/if}
        </div>
    </div>
{/if}

<div class="col-md-6 center-block">
    <a href="/admin/steamauthenticator/export/{$botID}" class="btn btn-default btn-margin">{language("steam", "authenticator", "export")}</a>
</div>
<div class="col-md-6 center-block">
    <a href="/admin/steamauthenticator/remove/{$botID}" class="btn btn-default btn-margin">{language("steam", "authenticator", "remove")}</a>
</div>

{includetemplate="footer.tpl"}
</body>
</html>