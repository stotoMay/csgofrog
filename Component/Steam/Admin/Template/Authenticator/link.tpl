<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("steam", "authenticator", "title_link")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{language("steam", "authenticator", "breadcrumb_bots")}</a></li>
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{$botname}</a></li>
    <li class="breadcrumb-item"><a href="/admin/steamauthenticator/add/{$botID}">{language("steam", "authenticator", "breadcrumb_add")}</a></li>
    <li class="breadcrumb-item active">{language("steam", "authenticator", "breadcrumb_link")}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-body">
        <form method="post" action="/admin/steamauthenticator/link/{$botID}">
            <table class="table table-borderless">
                {if $confirmation_needed}
                    <tr>
                        <td><label for="smscode">{language("steam", "authenticator", "smscode")}</label></td>
                        <td><input class="form-control" type="text" id="smscode" name="smscode" /></td>
                    </tr>
                {else}
                    <tr>
                        <td><label for="phonenumber">{language("steam", "authenticator", "phonenumber")}</label></td>
                        <td>
                            <input class="form-control" value="+0 123 456789" type="text" id="phonenumber" name="phonenumber" />
                        </td>
                    </tr>
                {/if}
            </table>
            <input type="hidden" name="token" value="{token()}">
            <input type="submit" class="btn btn-primary" value='{language("submit")}'>
        </form>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>