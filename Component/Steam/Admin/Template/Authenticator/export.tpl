<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("steam", "authenticator", "title_export")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{language("steam", "authenticator", "breadcrumb_bots")}</a></li>
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{$botname}</a></li>
    <li class="breadcrumb-item"><a href="/admin/steamauthenticator/bot/{$botID}">{language("steam", "authenticator", "breadcrumb_authenticator")}</a></li>
    <li class="breadcrumb-item active">{language("steam", "authenticator", "breadcrumb_export")}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-heading">{$user.personaname}</div>
    <div class="panel-body">
        {if $logged_in}
            <textarea class="form-control" style="min-height:300px;" disabled>
                {$data}
            </textarea>
        {else}
            <form method="post" action="/admin/steamauthenticator/export/{$botID}">
                <table class="table table-borderless">
                    <tr>
                        <td>{language("steam", "authenticator", "botusername")}</td>
                        <td>{$username}</td>
                    </tr>
                    <tr>
                        <td>{language("steam", "authenticator", "botpassword")}</td>
                        <td><input type="password" class="form-control" name="password"></td>
                    </tr>
                </table>
                <input type="hidden" name="token" value="{token()}">
                <input type="submit" class="btn btn-primary" value="{language("submit")}">
            </form>
        {/if}
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>