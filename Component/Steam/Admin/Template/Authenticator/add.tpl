<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("steam", "authenticator", "title_authenticator")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{language("steam", "authenticator", "breadcrumb_bots")}</a></li>
    <li class="breadcrumb-item active">{language("steam", "authenticator", "breadcrumb_add")}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="col-md-6 center-block">
            <a href="/admin/steamauthenticator/link/{$botID}" class="btn btn-default">{language("steam", "authenticator", "add")}</a>
        </div>
        <div class="col-md-6 center-block">
            <a href="/admin/steamauthenticator/import/{$botID}" class="btn btn-default">{language("steam", "authenticator", "import")}</a>
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>