<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("steam", "authenticator", "title_remove")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{language("steam", "authenticator", "breadcrumb_bots")}</a></li>
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{$botname}</a></li>
    <li class="breadcrumb-item"><a href="/admin/steamauthenticator/bot/{$botID}">{language("steam", "authenticator", "breadcrumb_authenticator")}</a></li>
    <li class="breadcrumb-item active">{language("steam", "authenticator", "breadcrumb_remove")}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-body">
        <form method="post" action="/admin/steamauthenticator/remove/{$botID}">
            <div class="col-md-6 center-block">
                <button name="mode" value="website" class="btn btn-primary">{language("steam", "authenticator", "remove_website")}</button>
            </div>
            <div class="col-md-6 center-block">
                <button name="mode" value="steam" class="btn btn-primary">{language("steam", "authenticator", "remove_completely")}</button>
            </div>
            <input type="hidden" name="token" value="{token()}" />
        </form>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>