<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("steam", "authenticator", "title_import")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{language("steam", "authenticator", "breadcrumb_bots")}</a></li>
    <li class="breadcrumb-item"><a href="/admin/steambotconfig">{$botname}</a></li>
    <li class="breadcrumb-item"><a href="/admin/steamauthenticator/add/{$botID}">{language("steam", "authenticator", "breadcrumb_authenticator")}</a></li>
    <li class="breadcrumb-item active">{language("steam", "authenticator", "breadcrumb_import")}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-body">
        <form method="post" action="/admin/steamauthenticator/import/{$botID}">
            <table class="table table-borderless">
                <tr>
                    <td>shared_secret</td>
                    <td><input type="text" class="form-control" name="shared_secret" /></td>
                </tr>
                <tr>
                    <td>serial_number</td>
                    <td><input type="text" class="form-control" name="serial_number" /></td>
                </tr>
                <tr>
                    <td>revocation_code</td>
                    <td><input type="text" class="form-control" name="revocation_code" /></td>
                </tr>
                <tr>
                    <td>token_gid</td>
                    <td><input type="text" class="form-control" name="token_gid" /></td>
                </tr>
                <tr>
                    <td>identity_secret</td>
                    <td><input type="text" class="form-control" name="identity_secret" /></td>
                </tr>
                <tr>
                    <td>secret_1</td>
                    <td><input type="text" class="form-control" name="secret_1" /></td>
                </tr>
                <tr>
                    <td>device_id</td>
                    <td><input type="text" class="form-control" name="device_id" /></td>
                </tr>
            </table>
            <input type="hidden" name="token" value="{token()}">
            <input type="submit" class="btn btn-primary" value='{language("submit")}'>
        </form>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>