<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Router\Controller;
use Component\Steam\Admin\Model\Authenticator;

use Component\Steam\Exception\AddNumberException;
use Component\Steam\Exception\AuthenticatorRemoveException;
use Component\Steam\Exception\BotNotFoundException;
use Component\Steam\Exception\ConfirmationException;
use Component\Steam\Exception\InvalidSMSCodeException;
use Component\Steam\Exception\LinkAuthenticatorException;
use Component\Steam\Exception\TradeException;
use Component\Steam\SteamFactory;
use Component\Steam\Core;

use Component\Steam\Bot\Bot;

class SteamAuthenticator extends Controller
{
    /**
     * @var Authenticator
     */
    private $model;
    private $admin;
    /**
     * @var Core
     */
    private $steam;

    public static function __info__()
    {
        return [
            "uniqname" => "steamauthenticator", "permission" => [
                "add" => "bool", "remove" => "bool", "export" => "bool", "view" => "bool", "confirm" => "bool"
            ], "visible" => false, "tracking" => true,
            "requirement" => [["application", "general", "admin_control_panel"],
                ["controller", "steamauthenticator", "view"]]
        ];
    }

    public function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("Component/Steam/Admin/Model/Authenticator");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));

        $this->steam = SteamFactory::getSteamCore();
    }

    public function bot($botID)
    {
        try
        {
            $bot = Bot::getInstance($botID);
        }
        catch(BotNotFoundException $e)
        {
            redirect("/admin/steambotconfig/", $this->request)->error($this->language->getMessage("standard_error"));
            exit();
        }

        if(!$bot->http()->hasAuthenticator())
        {
            redirect("/admin/steamauthenticator/add/".$botID, $this->request);
        }

        $template = $this->theme->draw("Steam/Authenticator/bot.tpl");

        $template->assignVar("botID", $botID);
        $template->assignVar("twofactorcode", $bot->http()->authenticator()->generateSteamGuardCode());

        try
        {
            $template->assignVar("confirmations", $bot->http()->authenticator()->getConfirmations());
        }
        catch(ConfirmationException $e)
        {
            $this->printError($this->language->get(["steam", "authenticator", "message", "failed_to_load_confirmations"]));
            $template->assignVar("confirmations", []);
        }

        $template->assignVar("botname", $bot->user()->getName());

        return response($template, $this->request);
    }

    public function twofactortoken($botID)
    {
        try
        {
            $bot = Bot::getInstance($botID);

            $response = [
                "success" => true, "token" => $bot->http()->authenticator()->generateSteamGuardCode(),
                "update_in" => 30 - time() % 30
            ];
        }
        catch(BotNotFoundException $e)
        {
            $response = ["success" => false];
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function add($botID)
    {
        if($this->getPermission("add") != 1)
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->getMessage("permission"));
        }

        try
        {
            $bot = Bot::getInstance($botID);
        }
        catch(BotNotFoundException $e)
        {
            redirect("/admin/steambotconfig/", $this->request)->error($this->language->getMessage("standard_error"));
            exit();
        }

        if($bot->http()->hasAuthenticator())
        {
            redirect("/admin/steamauthenticator/bot/".$botID, $this->request)->error($this->language->get([
                "steam", "message", "authenticator_attatched"
            ]));
        }

        $template = $this->theme->draw("Steam/Authenticator/add.tpl")->assignVar("botID", $bot->getBotID());

        $template->assignVar("botname", $bot->user()->getName());

        return response($template, $this->request);
    }

    public function link($botID)
    {
        if($this->getPermission("add") != 1)
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->getMessage("permission"));
        }

        try
        {
            $bot = Bot::getInstance($botID);
        }
        catch(BotNotFoundException $e)
        {
            redirect("/admin/steambotconfig/", $this->request)->error($this->language->getMessage("standard_error"));
            exit();
        }

        if($bot->http()->hasAuthenticator() || $bot->http()->authenticator()->hasAuthenticatorAttatched())
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->get([
                "steam", "authenticator", "message", "authenticator_attatched"
            ]));
        }

        $template = $this->theme->draw("Steam/Authenticator/link.tpl")->assignVar("botID", $bot->getBotID());

        $template->assignVar("botname", $bot->user()->getName());

        if($this->request->has("phonenumber"))
        {
            $number = $this->request->input("phonenumber");

            try
            {
                $bot->http()->authenticator()->addTelephoneNumber($number);

                $data = $bot->http()->authenticator()->linkMobileAuthenticator();

                $this->model->setAuthenticatorData($this->steam, $bot->getBotID(), $bot->http()->authenticator()
                    ->getDeviceID(), $data["shared_secret"], $data["serial_number"], $data["revocation_code"], $data["token_gid"], $data["identity_secret"], $data["secret_1"]);

                $template->assignVar("confirmation_needed", true);
            }
            catch(AddNumberException $e)
            {
                if($e->getMessage() != "")
                {
                    $this->printError($e->getMessage());
                }
            }
            catch(LinkAuthenticatorException $e)
            {
                if($e->getMessage() != "")
                {
                    $this->printError($e->getMessage());
                }
            }
        }

        if($this->request->has("smscode"))
        {
            $template->assignVar("confirmation_needed", true);
            $code = $this->request->input("smscode");

            try
            {
                $bot->http()->authenticator()->finalizeAuthenticatorLink($code);

                $this->model->confirmAuthenticator($botID);

                if($this->model->getTradeableTime($botID) - 3600 * 24 * 7 < time())
                {
                    $this->model->setTradeBan($botID, time() + 3600 * 24 * 7);
                }

                redirect("/admin/steamauthenticator/bot/".$botID, $this->request)->info($this->language->get([
                    "steam", "authenticator", "message", "authenticator_linked"
                ]));
            }
            catch(InvalidSMSCodeException $e)
            {
                $this->printError($this->language->get(["steam", "authenticator", "message", "invalid_sms_code"]));
            }
            catch(LinkAuthenticatorException $e)
            {
                $this->printError($this->language->getMessage("standard_error"));
            }
        }

        return response($template, $this->request);
    }

    public function import($botID)
    {
        if($this->getPermission("add") != 1)
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->getMessage("permission"));
        }

        try
        {
            $bot = Bot::getInstance($botID);
        }
        catch(BotNotFoundException $e)
        {
            redirect("/admin/steambotconfig/", $this->request)->error($this->language->getMessage("standard_error"));
            exit();
        }

        if($bot->http()->hasAuthenticator())
        {
            redirect("/admin/steamauthenticator/bot/".$botID, $this->request)->error($this->language->get([
                "steam", "authenticator", "message", "authenticator_attatched"
            ]));
        }

        $template = $this->theme->draw("Steam/Authenticator/import.tpl")->assignVar("botID", $bot->getBotID());

        $template->assignVar("botname", $bot->user()->getName());

        if($this->request->has("shared_secret") && $this->request->has("identity_secret") && $this->request->has("revocation_code"))
        {
            $this->model->setAuthenticatorData(
                $this->steam, $botID,
                $this->request->input("device_id"), $this->request->input("shared_secret"),
                $this->request->input("serial_number"), $this->request->input("revocation_code"),
                $this->request->input("token_gid"), $this->request->input("identity_secret"),
                $this->request->input("secret_1")
            );
            $this->model->confirmAuthenticator($botID);
            $this->model->removeAuthenticatorRestriction($botID);

            redirect("/admin/steamauthenticator/bot/".$botID, $this->request)->info($this->language->get([
                "steam", "authenticator", "message", "authenticator_linked"
            ]));
        }

        return response($template, $this->request);
    }

    public function remove($botID)
    {
        if($this->getPermission("remove") != 1)
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("Steam/Authenticator/remove.tpl");

        $template->assignVar("botID", $botID);

        try
        {
            $bot = Bot::getInstance($botID);
        }
        catch(BotNotFoundException $e)
        {
            redirect("/admin/steambotconfig/", $this->request)->error($this->language->getMessage("standard_error"));
            exit();
        }

        $template->assignVar("botname", $bot->user()->getName());

        if(!$bot->http()->hasAuthenticator())
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->getMessage("standard_error"));
        }

        if($this->request->has("mode"))
        {
            try
            {
                if($this->request->input("mode") == "steam")
                {
                    $bot->http()->authenticator()->removeMobileAuthenticator();
                }

                $this->model->removeAuthenticator($botID);

                redirect("/admin/steambotconfig", $this->request)->info($this->language->get([
                    "steam", "authenticator", "message", "authenticator_removed"
                ]));
            }
            catch(AuthenticatorRemoveException $e)
            {
                redirect("/admin/steambotconfig/", $this->request)->error($this->language->get([
                    "steam", "authenticator", "message", "failed_to_remove_authenticator"
                ]));
            }
        }

        return response($template, $this->request);
    }

    public function export($botID)
    {
        if($this->getPermission("export") != 1)
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->getMessage("permission"));
        }

        try
        {
            $bot = Bot::getInstance($botID);
        }
        catch(BotNotFoundException $e)
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->getMessage("standard_error"));
            exit();
        }

        if(!$bot->http()->hasAuthenticator())
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->getMessage("standard_error"));
        }

        $template = $this->theme->draw("Steam/Authenticator/export.tpl");

        $template->assignVar("botname", $bot->user()->getName());

        $login = $this->model->getLoginData($botID);

        $template->assignVar("user", $this->steam->api()->user()->getPlayerSummaries($bot->user()->getSteamID64())[0]);
        $template->assignVar("botID", $botID);

        if($this->request->has("password") && $this->request->input("password") == $login["password"])
        {
            $desktopdata = $this->model->getSteamDesktopAuthenticatorData($botID);

            $template->assignVar("data", json_encode($desktopdata));
            $template->assignVar("logged_in", true);
        }
        else
        {
            $template->assignVar("username", $login["username"]);
            $template->assignVar("logged_in", false);
        }

        return response($template, $this->request);
    }

    public function accept()
    {
        $botID = $this->request->input("botID", NULL, true, v()->integer());
        $confID = $this->request->input("confID");
        $key = $this->request->input("key");

        if($this->getPermission("confirm") != 1 || !is_numeric($botID))
        {
            $response = ["success" => false, "message" => $this->language->getMessage("permission")];
        }
        else
        {
            try
            {
                $bot = Bot::getInstance($botID);

                $this->request->ignoreUserAbort();
                $this->request->session()->closeSessionWrite();

                $bot->http()->authenticator()->sendConfirmationAjax($confID, $key, "allow");

                $response = ["success" => true, "message" => $this->language->get([
                    "steam", "authenticator", "message", "conf_allow"
                ])];
            }
            catch(ConfirmationException $e)
            {
                $message = empty($e->getMessage()) ? $this->language->get(["steam", "authenticator", "message", "conf_error"]) : $e->getMessage();
                $response = ["success" => false, "message" => $message];
            }
            catch(BotNotFoundException $e)
            {
                $response = ["success" => false, "message" => $this->language->getMessage("standard_error")];
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function decline()
    {
        $botID = $this->request->input("botID", NULL, true, v()->integer());
        $confID = $this->request->input("confID");
        $key = $this->request->input("key");

        if($this->getPermission("confirm") != 1 || !is_numeric($botID))
        {
            $response = ["success" => false, "message" => $this->language->getMessage("permission")];
        }
        else
        {
            try
            {
                $this->request->ignoreUserAbort();
                $this->request->session()->closeSessionWrite();

                $bot = Bot::getInstance($botID);

                $bot->http()->authenticator()->sendConfirmationAjax($confID, $key, "cancel");

                $response = ["success" => true, "message" => $this->language->get([
                    "steam", "authenticator", "message", "conf_cancel"
                ])];
            }
            catch(ConfirmationException $e)
            {
                $message = empty($e->getMessage()) ? $this->language->get(["steam", "authenticator", "message", "conf_error"]) : $e->getMessage();
                $response = ["success" => false, "message" => $message];
            }
            catch(BotNotFoundException $e)
            {
                $response = ["success" => false, "message" => $this->language->getMessage("standard_error")];
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }
}