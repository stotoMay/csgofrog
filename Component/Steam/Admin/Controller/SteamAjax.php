<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Router\Controller;
use Component\Steam\Admin\Model\Ajax;

use Component\Steam\Exception\ConfirmationException;
use Component\Steam\SteamFactory;
use Component\Steam\Core;

use Component\Steam\Bot\Bot;
use Component\Steam\Offer\Trade;

use Component\Steam\Exception\APIException;
use Component\Steam\Exception\BotNotFoundException;
use Component\Steam\Exception\UserNotFoundException;
use Component\Steam\Exception\TradeException;

class SteamAjax extends Controller
{
    /**
     * @var Ajax
     */
    private $model;
    private $admin;
    /**
     * @var Core
     */
    private $steam;

    public static function __info__()
    {
        return [
            "uniqname" => "steamajax", "visible" => false, "tracking" => true,
            "requirement" => [["application", "general", "admin_control_panel"]]
        ];
    }

    public function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("Component/Steam/Admin/Model/Ajax");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));

        $this->steam = SteamFactory::getSteamCore();
    }

    public function sendoffer()
    {
        if($this->browser->user()->permission()
                ->getController("steambotconfig", "trade") != 1 || !$this->request->has("botID")
        )
        {
            $response = ["success" => false, "message" => $this->language->getMessage("permission")];
        }
        else
        {
            $this->request->ignoreUserAbort();
            $this->request->session()->closeSessionWrite();

            try
            {
                $tradeurl = $this->steam->api()->user()
                    ->splitTradeURL(htmlspecialchars_decode($this->request->input("tradeurl")));

                if(!is_array($tradeurl))
                {
                    throw new UserNotFoundException();
                }

                $trade = new Trade(Bot::getInstance($this->request->input("botID")), new \Component\Steam\User\User($tradeurl["steamID"]), $tradeurl["token"]);

                $myitems = @json_decode($this->request->input("myitems"), true);
                if(!is_array($myitems))
                {
                    $myitems = [];
                }

                $hisitems = @json_decode($this->request->input("hisitems"), true);
                if(!is_array($hisitems))
                {
                    $hisitems = [];
                }

                foreach($myitems as $item)
                {
                    if(!isset($item["appID"]) || !isset($item["contextID"]) || !isset($item["assetID"]) || !isset($item["amount"]))
                    {
                        continue;
                    }

                    $trade->addMyAsset($item["appID"], $item["contextID"], $item["assetID"], $item["amount"]);
                }

                foreach($hisitems as $item)
                {
                    if(!isset($item["appID"]) || !isset($item["contextID"]) || !isset($item["assetID"]) || !isset($item["amount"]))
                    {
                        continue;
                    }

                    $trade->addHisAsset($item["appID"], $item["contextID"], $item["assetID"], $item["amount"]);
                }

                $trade->execute();

                $response = [
                    "success" => true, "message" => $this->language->get(["steam", "bots", "message", "tradesend_success"])
                ];
            }
            catch(BotNotFoundException $e)
            {
                $response = [
                    "success" => false, "error" => "invalid_bot",
                    "message" => $this->language->getMessage("standard_error")
                ];
            }
            catch(UserNotFoundException $e)
            {
                $response = [
                    "success" => false, "error" => "invalid_partner",
                    "message" => $this->language->get(["steam", "bots", "message", "invalid_trade_partner"])
                ];
            }
            catch(TradeException $e)
            {
                $message = empty($e->getMessage()) ? $this->language->get(["steam", "bots", "message", "tradesend_error"]) : $e->getMessage();
                $response = ["success" => false, "error" => "invalid_trade", "message" => $message];
            }
            catch(ConfirmationException $e)
            {
                $message = empty($e->getMessage()) ? $this->language->get(["steam", "bots", "message", "trade_confirmation"]) : $e->getMessage();
                $response = ["success" => false, "error" => "invalid_trade", "message" => $message];
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function botinventory()
    {
        $appID = $this->request->input("appID");
        $contextID = $this->request->input("contextID");
        $botID = $this->request->input("botID");

        try
        {
            $this->request->ignoreUserAbort();
            $this->request->session()->closeSessionWrite();

            if(!is_numeric($appID) || !is_numeric($contextID) || !is_numeric($botID))
            {
                throw new APIException();
            }

            $bot = Bot::getInstance($botID);

            $inventory = $bot->action()->getOwnInventory($appID, $contextID);

            $args = [$botID, &$inventory];
            $this->app->component()->setHook("onBotInventoryLoad", $args);


            $response = ["success" => true, "response" => $inventory->toArray()];
        }
        catch(BotNotFoundException $e)
        {
            $response = ["success" => false, "response" => []];
        }
        catch(APIException $e)
        {
            $response = ["success" => false, "response" => []];
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function inventory()
    {
        $appID = $this->request->input("appID");
        $contextID = $this->request->input("contextID");
        $steamID = $this->request->input("steamID");

        if($this->request->has("tradeurl"))
        {
            $data = $this->steam->api()->user()->splitTradeURL(htmlspecialchars_decode($this->request->input("tradeurl")));
            //$steamID = empty($steamID) ? $data["steamID"] : $steamID;
        }

        try
        {
            $this->request->ignoreUserAbort();
            $this->request->session()->closeSessionWrite();

            if(!is_numeric($appID) || !is_numeric($contextID) || !is_numeric($steamID))
            {
                throw new APIException();
            }

            $botID = $this->model->getBotID($steamID);
            if($botID !== false)
            {
                $bot = Bot::getInstance($botID);
                $inventory = $bot->action()->getOwnInventory($appID, $contextID);
                $args = [$botID, &$inventory];
                $this->app->component()->setHook("onBotInventoryLoad", $args);
            }
            else
            {
                $bot = Bot::getInstance($this->steam->getRandomBotID());
                $inventory = $bot->action()->getPartnerInventory($steamID, $appID, $contextID);
            }

            $response = ["success" => true, "response" => $inventory->toArray()];
        }
        catch(APIException $e)
        {
            $response = ["success" => false, "response" => []];
        }
        catch(BotNotFoundException $e)
        {
            $response = ["success" => false, "response" => []];
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }
}