<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Router\Controller;
use Component\Steam\Admin\Model\BotConfig;

use Component\Steam\Exception\BotNotFoundException;
use Component\Steam\Exception\CaptchaException;
use Component\Steam\Exception\ClientSessionException;
use Component\Steam\Exception\ConfirmationException;
use Component\Steam\Exception\LoginException;
use Component\Steam\Exception\SteamGuardEmailException;
use Component\Steam\Exception\TradeException;
use Component\Steam\Exception\TwoFactorException;
use Component\Steam\SteamFactory;
use Component\Steam\Core;

use Component\Steam\Bot\Bot;
use Component\Steam\HTTP\MobileLogin;

class SteamBotConfig extends Controller
{
    /**
     * @var BotConfig
     */
    private $model;
    private $admin;
    /**
     * @var Core
     */
    private $steam;

    public static function __info__()
    {
        return [
            "uniqname" => "steambotconfig", "permission" => [
                "view" => "bool", "add" => "bool", "delete" => "bool",
                "account" => "bool", "trade" => "bool", "refresh" => "bool",
            ], "icon" => "server", "priority" => 50,
            "requirement" => [["application", "general", "admin_control_panel"],
                ["controller", "steambotconfig", "view"]], "tracking" => true
        ];
    }

    public function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("Component/Steam/Admin/Model/BotConfig");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));

        $this->steam = SteamFactory::getSteamCore();
    }

    public function main()
    {
        return response($this->theme->draw("Steam/Bots/main.tpl")
            ->assignVar("bots", $this->model->getBotList($this->steam)), $this->request);
    }

    public function account($botID)
    {
        if($this->getPermission("account") != 1)
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->getMessage("permission"));
        }

        try
        {
            $bot = Bot::getInstance($botID);
        }
        catch(BotNotFoundException $e)
        {
            redirect("/admin/steambotconfig/", $this->request)->error($this->language->getMessage("standard_error"));
            exit();
        }

        $botData = $this->model->getBot($botID);

        $template = $this->theme->draw("Steam/Bots/account.tpl")->assignVar("botID", $botID)
            ->assignVar("apikey", $bot->getAPIKey())->assignVar("hasAuthenticator", $bot->http()->hasAuthenticator());

        $template->assignVar("botname", $bot->user()->getName());

        if(!empty($bot->getToken()))
        {
            $template->assignVar("tradeurl", $bot->getTradeURL());
        }

        $list = [];
        $apps = array_clear(explode(";", $botData["inventories"]));
        foreach($apps as $app)
        {
            $list[] = $app;
        }

        $template->assignVar("inventories", $list);

        if($this->request->has("tradeurl"))
        {
            $tradeurl = $this->steam->api()->user()->splitTradeURL($this->request->input("tradeurl"));

            if(is_array($tradeurl) && $tradeurl["steamID"] == $bot->user()->getSteamID64())
            {
                $invs = implode(";", $this->request->input("inventories", []));
                $apikey = $this->request->input("apikey");

                $this->model->setInventories($botID, $invs);
                $this->model->setTradeURL($botID, $tradeurl["token"]);
                $this->model->setAPIKey($botID, $apikey);

                redirect("/admin/steambotconfig/account/".$botID, $this->request)->info($this->language->get([
                    "steam", "bots", "message", "bot_edited"
                ]));
            }
            else
            {
                $this->printError($this->language->get(["steam", "message", "invalid_tradeurl"]));
            }
        }

        return response($template, $this->request);
    }

    public function send($botID, $partner = NULL, $token = NULL)
    {
        if($this->getPermission("trade") != 1 || !is_numeric($botID))
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->getMessage("permission"));
        }

        try
        {
            $bot = Bot::getInstance($botID);
        }
        catch(BotNotFoundException $e)
        {
            redirect("/admin/steambotconfig/", $this->request)->error($this->language->getMessage("standard_error"));
            exit();
        }

        $template = $this->theme->draw("Steam/Bots/send.tpl")->assignVar("botID", $botID)
            ->assignVar("botSteamID", $bot->user()->getSteamID64())
            ->assignVar("bots", $this->model->getBotList($this->steam));

        $template->assignVar("botname", $bot->user()->getName());

        if(!empty($partner))
        {
            $user = new \Component\Steam\User\User($partner);
            $url = $this->steam->api()->user()->buildTradeURL($user, $token);

            $template->assignVar("tradeurl", $url);
        }

        return response($template, $this->request);
    }

    public function offers($mode, $botID)
    {
        if($this->getPermission("trade") != 1 || !is_numeric($botID))
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->getMessage("permission"));
        }

        try
        {
            $bot = Bot::getInstance($botID);
        }
        catch(BotNotFoundException $e)
        {
            redirect("/admin/steambotconfig/", $this->request)->error($this->language->getMessage("standard_error"));
            exit();
        }

        if($mode == "sent")
        {
            $offers = $bot->trading()->getSentTrades();
        }
        else
        {
            $offers = $bot->trading()->getReceivedTrades();
        }

        $offers->queueItemPrices();

        $template = $this->theme->draw("Steam/Bots/offers.tpl")->assignVar("offers", $offers)
            ->assignVar("mode", $mode)->assignVar("botID", $bot->getBotID());

        $template->assignVar("botname", $bot->user()->getName());

        return response($template, $this->request);
    }

    public function checkoffer($botID, $offerID)
    {
        if($this->getPermission("account") != 1 || !is_numeric($botID) || !is_numeric($offerID))
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->getMessage("permission"));
        }

        try
        {
            $bot = Bot::getInstance($botID);
        }
        catch(BotNotFoundException $e)
        {
            redirect("/admin/steambotconfig/", $this->request)->error($this->language->getMessage("standard_error"));
            exit();
        }

        $template = file_get_contents("https://api.steampowered.com/IEconService/GetTradeOffer/v1/?key=".$bot->getAPIKey()."&tradeofferid=".$offerID."&get_descriptions=1");

        return response($template, $this->request);
    }

    public function changepassword($botID)
    {
        if($this->getPermission("add") != 1)
        {
            redirect("/admin/steambotconfig/account/".$botID, $this->request)->error($this->language->getMessage("permission"));
        }

        try
        {
            $bot = Bot::getInstance($botID);
        }
        catch(BotNotFoundException $e)
        {
            redirect("/admin/steambotconfig/", $this->request)->error($this->language->getMessage("standard_error"));
            exit();
        }

        $template = $this->theme->draw("Steam/Bots/password.tpl");

        $username = $this->model->getBotUsername($botID);

        $template->assignVar("username", $username);
        $template->assignVar("botID", $botID);
        $template->assignVar("botname", $bot->user()->getName());

        if($this->request->has("password", true, v()->textstring()))
        {
            $password = $this->request->input("password");

            try
            {
                $auth = new MobileLogin($username, $password);
                $auth->auth();
            }
            catch(SteamGuardEmailException $e)
            {
            }
            catch(TwoFactorException $e)
            {
            }
            catch(CaptchaException $e)
            {
            }
            catch(LoginException $e)
            {
                redirect("/admin/steambotconfig/changepassword/".$botID, $this->request)->error($e->getMessage());
            }

            $this->model->setBotPassword($this->steam, $botID, $password);

            redirect("/admin/steambotconfig/account/".$botID, $this->request)->info($this->language->get(["steam", "bots", "message", "bot_password_changed"]));
        }

        return response($template, $this->request);
    }

    public function add()
    {
        if($this->getPermission("add") != 1)
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("Steam/Bots/add.tpl");

        if($this->request->has("username") && $this->request->has("password"))
        {
            $auth = new MobileLogin($this->request->input("username"), $this->request->input("password"));

            if($this->request->has("captchaID") && $this->request->has("captchastring"))
            {
                $auth->setCaptchaCode($this->request->input("captchaID"), $this->request->input("captchastring"));
            }
            if($this->request->has("steamguardstring") && $this->request->has("steamID"))
            {
                $auth->setEmailCode($this->request->input("steamID"), $this->request->input("steamguardstring"));
            }
            if($this->request->has("twofactorcode"))
            {
                $auth->setTwoFactorCode($this->request->input("twofactorcode"));
            }

            try
            {
                $session = $auth->auth();

                $botID = $this->model->addBot($this->steam, $session["steamID"], $this->request->input("username"), $this->request->input("password"), $session["oAuth"]);
                $this->model->setBotSession($this->steam, $botID, $session["login"], $session["loginSecure"]);

                redirect("/admin/steambotconfig/updatesteamdata/".$botID."/add", $this->request);
            }
            catch(SteamGuardEmailException $e)
            {
                $template->assignVar([
                    "email_needed" => true, "steamID" => $e->getSteamID(),
                    "username" => $this->request->input("username"),
                    "password" => $this->request->input("password")
                ]);

                $this->printError($this->language->get(["steam", "bots", "message", "email_needed"]));
            }
            catch(TwoFactorException $e)
            {
                $template->assignVar([
                    "twofactor_needed" => true, "username" => $this->request->input("username"),
                    "password" => $this->request->input("password")
                ]);

                $this->printError($this->language->get(["steam", "bots", "message", "twofactor_needed"]));
            }
            catch(CaptchaException $e)
            {
                $template->assignVar([
                    "captcha_needed" => true, "captchaID" => $e->getCaptchaID(),
                    "captchaImage" => $e->getCaptchaImage(), "username" => $this->request->input("username"),
                    "password" => $this->request->input("password")
                ]);

                $this->printError($this->language->get(["steam", "bots", "message", "captcha_needed"]));
            }
            catch(LoginException $e)
            {
                $this->printError($this->language->get(["steam", "bots", "message", "invalid_bot_password"]));
            }
        }

        return response($template, $this->request);
    }

    public function delete()
    {
        if($this->getPermission("delete") != 1)
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->getMessage("permission"));
        }

        $botID = $this->request->input("botID", NULL, true, v()->integer());

        try
        {
            Bot::getInstance($botID);
        }
        catch(BotNotFoundException $e)
        {
            redirect("/admin/steambotconfig", $this->request)->error($this->language->getMessage("standard_error"));
            exit();
        }

        $args = [$botID];
        $this->app->component()->setHook("onSteamBotDelete", $args);

        $this->model->deleteBot($botID);

        redirect("/admin/steambotconfig", $this->request)->info($this->language->get([
            "steam", "bots", "message", "bot_deleted"
        ]));
    }

    public function refresh($botID)
    {
        if($this->getPermission("refresh") != 1)
        {
            redirect("/admin/steambotconfig/account", $this->request)->error($this->language->getMessage("permission"));
        }

        try
        {
            $bot = Bot::getInstance($botID);
        }
        catch(BotNotFoundException $e)
        {
            redirect("/admin/steambotconfig/", $this->request)->error($this->language->getMessage("standard_error"));
            exit();
        }

        try
        {
            $bot->http()->client()->refreshSession();
            Bot::dispatch();

            redirect("/admin/steambotconfig/account/".$botID, $this->request)->info($this->language->get([
                "steam", "bots", "message", "bot_refreshed"
            ]));
        }
        catch(ClientSessionException $e)
        {
            redirect("/admin/steambotconfig/account/".$botID, $this->request)->error($this->language->getMessage("standard_error"));
        }
    }

    public function relog($botID)
    {
        if($this->getPermission("refresh") != 1)
        {
            redirect("/admin/steambotconfig/account/", $this->request)->error($this->language->getMessage("permission"));
        }

        try
        {
            $bot = Bot::getInstance($botID);
        }
        catch(BotNotFoundException $e)
        {
            redirect("/admin/steambotconfig/", $this->request)->error($this->language->getMessage("standard_error"));
            exit();
        }

        if(!$bot->http()->hasAuthenticator())
        {
            redirect("/admin/steambotconfig/account/".$botID, $this->request)->error($this->language->get([
                "steam", "bots", "message", "authenticator_needed"
            ]));
        }

        $logindata = $this->model->getLoginData($botID);
        $login = New MobileLogin($logindata["username"], $logindata["password"]);

        $login->setTwoFactorCode($bot->http()->authenticator()->generateSteamGuardCode());

        try
        {
            $session = $login->auth();

            $this->model->updateBotSession($botID, $session["oAuth"], $session["login"], $session["loginSecure"]);

            redirect("/admin/steambotconfig/account/".$botID, $this->request)->info($this->language->get([
                "steam", "bots", "message", "bot_relogged"
            ]));
        }
        catch(SteamGuardEmailException $e)
        {
            redirect("/admin/steambotconfig/account/".$botID, $this->request)->error($this->language->getMessage("standard_error"));
        }
        catch(TwoFactorException $e)
        {
            redirect("/admin/steambotconfig/account/".$botID, $this->request)->error($this->language->getMessage("standard_error"));
        }
        catch(CaptchaException $e)
        {
            redirect("/admin/steambotconfig/account/".$botID, $this->request)->error($this->language->getMessage("standard_error"));
        }
        catch(LoginException $e)
        {
            redirect("/admin/steambotconfig/account/".$botID, $this->request)->error($this->language->getMessage("standard_error"));
        }
    }

    public function updatesteamdata($botID, $mode = NULL)
    {
        if($this->getPermission("refresh") != 1)
        {
            redirect("/admin/steambotconfig/account/", $this->request)->error($this->language->getMessage("permission"));
        }

        try
        {
            $bot = Bot::getInstance($botID);
        }
        catch(BotNotFoundException $e)
        {
            redirect("/admin/steambotconfig/", $this->request)->error($this->language->getMessage("standard_error"));
            exit();
        }

        $this->steam->api()->user()->getPlayerSummaries($bot->user()->getSteamID64(), false);

        $bot->http()->client()->get("https://steamcommunity.com/market/eligibilitycheck/");
        $webTradeEligibility = json_decode(rawurldecode($bot->http()->client()->getCookie("webTradeEligibility")), true);

        if($webTradeEligibility["allowed"] != 1)
        {
            $this->model->setTradeBan($botID, $webTradeEligibility["allowed_at_time"]);
        }
        else
        {
            $this->model->removeTradeBan($botID);
        }

        if($mode == "add")
        {
            redirect("/admin/steambotconfig/account/".$botID, $this->request)->info($this->language->get([
                "steam", "bots", "message", "bot_added"
            ]));
        }
        else
        {
            redirect("/admin/steambotconfig/account/".$botID, $this->request)->info($this->language->get([
                "steam", "bots", "message", "bot_updated"
            ]));
        }
    }

    public function accept()
    {
        $botID = $this->request->input("botID");
        $offerID = $this->request->input("offerID");
        $partnerID = $this->request->input("partnerID");

        if($this->getPermission("trade") != 1 || !is_numeric($botID))
        {
            $response = ["success" => false, "message" => $this->language->getMessage("permission")];
        }
        else
        {
            try
            {
                $this->request->ignoreUserAbort();
                $this->request->session()->closeSessionWrite();

                $bot = Bot::getInstance($botID);

                $bot->trading()->acceptTrade($offerID, $partnerID);

                $response = ["success" => true, "message" => $this->language->get([
                    "steam", "bots", "message", "trade_accepted"
                ])];
            }
            catch(TradeException $e)
            {
                $message = empty($e->getMessage()) ? $this->language->get(["steam", "bots", "message", "trade_error"]) : $e->getMessage();
                $response = ["success" => false, "message" => $message];
            }
            catch(ConfirmationException $e)
            {
                $message = empty($e->getMessage()) ? $this->language->get(["steam", "bots", "message", "trade_confirmation"]) : $e->getMessage();
                $response = ["success" => false, "message" => $message];
            }
            catch(BotNotFoundException $e)
            {
                $response = ["success" => false, "message" => $this->language->getMessage("standard_error")];
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function decline()
    {
        $botID = $this->request->input("botID");
        $offerID = $this->request->input("offerID");

        if($this->getPermission("trade") != 1 || !is_numeric($botID))
        {
            $response = ["success" => false, "message" => $this->language->getMessage("permission")];
        }
        else
        {
            try
            {
                $this->request->ignoreUserAbort();
                $this->request->session()->closeSessionWrite();

                $bot = Bot::getInstance($botID);

                $bot->trading()->declineTrade($offerID);

                $response = ["success" => true, "message" => $this->language->get([
                    "steam", "bots", "message", "trade_declined"
                ])];
            }
            catch(TradeException $e)
            {
                $message = empty($e->getMessage()) ? $this->language->get(["steam", "bots", "message", "trade_error"]) : $e->getMessage();
                $response = ["success" => false, "message" => $message];
            }
            catch(BotNotFoundException $e)
            {
                $response = ["success" => false, "message" => $this->language->getMessage("standard_error")];
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function cancel()
    {
        $botID = $this->request->input("botID");
        $offerID = $this->request->input("offerID");

        if($this->getPermission("trade") != 1 || !is_numeric($botID))
        {
            $response = ["success" => false, "message" => $this->language->getMessage("permission")];
        }
        else
        {
            try
            {
                $this->request->ignoreUserAbort();
                $this->request->session()->closeSessionWrite();

                $bot = Bot::getInstance($botID);

                $bot->trading()->cancelTrade($offerID);

                $response = ["success" => true, "message" => $this->language->get([
                    "steam", "bots", "message", "trade_cancled"
                ])];
            }
            catch(TradeException $e)
            {
                $message = empty($e->getMessage()) ? $this->language->get(["steam", "bots", "message", "trade_error"]) : $e->getMessage();
                $response = ["success" => false, "message" => $message];
            }
            catch(BotNotFoundException $e)
            {
                $response = ["success" => false, "message" => $this->language->getMessage("standard_error")];
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }
}