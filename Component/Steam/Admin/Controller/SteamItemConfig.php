<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Router\Controller;
use Component\Steam\Admin\Model\ItemConfig;
use Component\Steam\Core;
use Component\Steam\SteamFactory;
use Component\Steam\Exception\APIException;

class SteamItemConfig extends Controller
{
    /**
     * @var ItemConfig
     */
    private $model;
    private $admin;

    /**
     * @var Core
     */
    private $steam;

    public static function __info__()
    {
        return [
            "uniqname" => "steamitemconfig", "priority" => 40,
            "permission" => ["view" => "bool", "change" => "bool", "refresh" => "bool"], "icon" => "database",
            "requirement" => [["application", "general", "admin_control_panel"],
                ["controller", "steamitemconfig", "view"]], "tracking" => true
        ];
    }

    public function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("Component/Steam/Admin/Model/ItemConfig");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));

        $this->steam = SteamFactory::getSteamCore();
    }

    public function main()
    {
        $template = $this->theme->draw("Steam/Items/search.tpl");

        $match = $this->request->input("search", NULL, false);
        $page = $this->request->input("page", 1, false, v()->integer());
        $mode = $this->request->input("mode", "", false);

        $availableModes = ["priceless", "banned"];

        if(!empty($mode) && !is_int(array_search($mode, $availableModes)))
        {
            $mode = "";
        }

        if(isset($match))
        {
            $match = rawurldecode($match);
        }

        $items = $this->model->getSearchResults($this->steam, $match, $page, 50, $mode);

        if(count($items["results"]) == 0 && !empty($match))
        {
            $this->printError($this->language->get(["steam", "items", "message", "no_items_found"]));
        }

        $template->assignVar("pricless_item_count", $this->model->getUndefinedItemCount());
        $template->assignVar("match", $match);
        $template->assignVar("pageurl", "/admin/steamitemconfig?search=".rawurlencode($match)."&mode=".$mode."&page=%p");
        $template->assignVar("items", $items);
        $template->assignVar("mode", $mode);

        return response($template, $this->request);
    }

    public function item($appID, $marketname)
    {
        $marketname = rawurldecode($marketname);
        $item = $this->model->getItem($appID, $marketname);
        $itemlist = [&$item];
        $this->steam->item()->filling($itemlist, true);

        if(!is_array($item))
        {
            redirect("/admin/steamitemconfig/", $this->request)->error($this->language->getMessage("standard_error"));
        }

        $item["appname"] = $this->steam->api()->steam()->getAppName($item["appID"]);

        $template = $this->theme->draw("Steam/Items/item.tpl");

        $template->assignVar("item", $item);
        $template->assignVar("graph", $this->model->getItemGraph($appID, $marketname));
        $template->assignVar("is_banned", $item["user_price"] === 0);

        if($this->request->has("price") && $this->getPermission("change") == 1)
        {
            $price = $this->request->input("price");

            if(is_numeric($price) || empty($price) || $price == "ban")
            {
                if(empty($price))
                {
                    $this->model->removeUserPrice($appID, $marketname);
                }
                else
                {
                    if($price == "ban")
                    {
                        $price = 0;
                    }

                    $this->model->setUserPrice($appID, $marketname, $price, $this->browser->getUserID());
                }

                redirect("/admin/steamitemconfig/item/".$appID."/".rawurlencode($marketname), $this->request)->info($this->language->get([
                    "steam", "items", "message", "price_changed"
                ]));
            }
            else
            {
                $this->printError($this->language->getMessage("standard_error"));
            }
        }

        return response($template, $this->request);
    }

    public function update()
    {
        if($this->getPermission("refresh") != 1)
        {
            redirect("/admin/steamitemconfig", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("Steam/Items/update.tpl");

        $template->assignVar("pricless_item_count", $this->model->getUndefinedItemCount());

        return response($template, $this->request);
    }


    public function getmarketinfo()
    {
        if($this->browser->user()->permission()->getController("steamitemconfig", "refresh") != 1)
        {
            $response = ["success" => false];
        }
        else
        {
            $this->request->ignoreUserAbort();
            $this->request->session()->closeSessionWrite();

            $appID = $this->request->input("appID", NULL, true, v()->integer());

            try
            {
                if(!is_numeric($appID))
                {
                    throw new APIException();
                }

                $response = ["success" => true, "response" => $this->steam->api()->market()->getMarketInfo($appID)];
            }
            catch(APIException $e)
            {
                $response = ["success" => false];
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function updatemarketpage()
    {
        if($this->browser->user()->permission()->getController("steamitemconfig", "refresh") != 1)
        {
            $response = ["success" => false];
        }
        else
        {
            $this->request->ignoreUserAbort();
            $this->request->session()->closeSessionWrite();

            $page = $this->request->input("page", NULL, true, v()->integer());
            $appID = $this->request->input("appID", NULL, true, v()->integer());

            try
            {
                if(!is_numeric($page) || !is_numeric($appID))
                {
                    throw new APIException();
                }

                $items = $this->steam->api()->market()->getMarketPage($page, $appID, "name", "asc")->toArray(false);

                foreach($items as $item)
                {
                    $this->steam->item()->setMarketItem($item["appID"], $item["market_name"], $item["market_volume"], $item["price"], $item["name"], $item["color"], $item["background"], $item["raw_image"]);
                }

                $response = ["success" => true];
            }
            catch(APIException $e)
            {
                $response = ["success" => false];
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function updateitem()
    {
        if($this->browser->user()->permission()->getController("steamitemconfig", "refresh") != 1)
        {
            $response = ["success" => false];
        }
        else
        {
            $this->request->ignoreUserAbort();
            $this->request->session()->closeSessionWrite();

            $market_name = $this->request->input("market_name", NULL, true, v()->textstring());
            $appID = $this->request->input("appID", NULL, true, v()->integer());

            try
            {
                if(!is_numeric($appID) || !is_string($market_name))
                {
                    throw new APIException();
                }

                $price = $this->steam->api()->market()->getMarketPrice($appID, $market_name);
                $this->steam->item()->addItemPrice($appID, $market_name, $price);

                $response = ["success" => true, "price" => $price];
            }
            catch(APIException $e)
            {
                $response = ["success" => false];
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }
}