<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Admin\Model;

use App\Router\Model;
use Component\Steam\Core;
use Component\Steam\User\User;

class BotConfig extends Model
{
    //getter
    public function getBotList(Core $steam)
    {
        $botList = [];
        $bots = $this->database->select("steam_bot")->column([
            "steam_bot.dateline" => "steamguard", "steam_bot.username" => "username", "steam_bot.id" => "id",
            "steam_bot.steamID" => "steamID", "steam_bot.tradetoken" => "tradetoken",
            "steam_bot.tradeable" => "tradeable"
        ])->leftJoin("steam_auth", ["steam_auth.steamID[=]steam_bot.steamID"])->fetchAssoc();

        foreach($bots as $bot)
        {
            $user = new User($bot["steamID"]);

            $botList[] = [
                "id" => $bot["id"], "username" => $bot["username"], "steamID" => $bot["steamID"],
                "steam" => $steam->api()->user()->getPlayerSummaries($bot["steamID"])[0],
                "tradeable" => $bot["tradeable"] ? $bot["tradeable"] < time() : true,
                "tradetime" => $bot["tradeable"],
                "tradeurl" => $steam->api()->user()->buildTradeURL($bot["tradetoken"]),
                "tradetoken" => $bot["tradetoken"]
            ];
        }

        return $botList;
    }

    public function getBot($botID)
    {
        return $this->database->select("steam_bot")->where(["id" => $botID])->fetchRow();
    }

    //add
    public function addBot(Core $steam, $steamID, $username, $password, $oAuth)
    {
        return $this->database->insert("steam_bot")->set([
            "steamID" => $steamID, "username" => $username, "password" => $steam->crypt()->encrypt($password),
            "oAuth" => $steam->crypt()->encrypt($oAuth), "dateline" => time()
        ])->getID();
    }

    public function setBotPassword(Core $steam, $botID, $password)
    {
        $this->database->update("steam_bot")
            ->set(["password" => $steam->crypt()->encrypt($password)])
            ->where(["id" => $botID]);
    }

    public function setBotSession(Core $steam, $botID, $login, $loginSecure)
    {
        $this->database->delete("steam_bot_session")->where(["botID" => $botID]);

        $this->database->insert("steam_bot_session")->set([
            "botID" => $botID, "secure" => $steam->crypt()->encrypt($loginSecure),
            "login" => $steam->crypt()->encrypt($login)
        ]);
    }

    //delete
    public function deleteBot($botID)
    {
        $this->database->delete("steam_bot")->where(["id" => $botID]);

        $this->database->delete("steam_bot_session")->where(["botID" => $botID]);
    }

    //edit
    public function setTradeURL($botID, $token)
    {
        $this->database->update("steam_bot")->set(["tradetoken" => $token])->where(["id" => $botID]);
    }

    public function setInventories($botID, $inventories)
    {
        $this->database->update("steam_bot")->set(["inventories" => $inventories])->where(["id" => $botID]);
    }

    public function setAPIKey($botID, $key)
    {
        $this->database->update("steam_bot")->set(["apikey" => $key])->where(["id" => $botID]);
    }

    public function removeTradeBan($botID)
    {
        $this->database->update("steam_bot")->set(["tradeable" => NULL])->where(["id" => $botID]);
    }

    public function setTradeBan($botID, $time)
    {
        $this->database->update("steam_bot")->set(["tradeable" => $time])->where(["id" => $botID]);
    }

    public function getTradeableTime($botID)
    {
        $data = $this->database->select("steam_bot")
            ->column(["tradeable"])
            ->where(["id" => $botID])->fetchAssoc();

        if($data["tradeable"])
        {
            return $data["tradeable"];
        }
        else
        {
            return time() - 1;
        }
    }

    // refresh
    public function getLoginData($botID)
    {
        $data = $this->database->select("steam_bot")
            ->column(["username", "password"])
            ->where(["id" => $botID])->fetchRow();

        return ["username" => $data["username"], "password" => $this->decrypt($data["password"])];
    }

    public function getBotUsername($botID)
    {
        $bot = $this->database->select("steam_bot")
            ->column(["username"])->where(["id" => $botID])->fetchRow();

        return $bot["username"];
    }

    public function updateBotSession($botID, $oAuthToken, $login, $secure)
    {
        $this->database->update("steam_bot")->set(["oAuth" => $this->encrypt($oAuthToken)])->where(["id" => $botID]);
        $this->database->update("steam_bot_session")->set(["login" => $this->encrypt($login),
            "secure" => $this->encrypt($secure)])->where(["botID" => $botID]);
    }
}