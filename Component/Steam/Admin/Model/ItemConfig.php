<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Admin\Model;

use App\Router\Model;
use Component\Steam\Core;

class ItemConfig extends Model
{
    public function getSearchResults(Core $steam, $match = NULL, $page = 1, $size = 50, $mode = "")
    {
        $query = $this->database->select("steam_market")->column([
            "appID", "market_name", "market_volume", "market_price",
            "name", "color", "background", "image", "steam_price.price" => "user_price"
        ])->leftJoin("steam_price", ["steam_market.id[=]steam_price.itemID"]);

        if($mode == "priceless")
        {
            $condition = ["steam_market.market_price" => [0, NULL], "steam_price.price" => NULL];
        }
        elseif($mode == "banned")
        {
            $condition = ["steam_price.price" => 0];
        }
        else
        {
            $condition = [];
        }

        if(isset($match))
        {
            $condition["steam_market.name[~~]"] = "%".$match."%";
        }

        if(count($condition) > 0)
        {
            $query->where($condition);
        }

        $query->order("name", "asc");

        $pagecount = ceil($query->numRows() / $size);
        $results = $query->limit(($page - 1) * $size, $size)->fetchAssoc();

        foreach($results as $key => $item)
        {
            $results[$key]["image"] = $item["image"];
            $results[$key]["price"] = !isset($item["user_price"]) ? $item["market_price"] / 100 : $item["user_price"] / 100;
            $results[$key]["appname"] = $steam->api()->steam()->getAppName($item["appID"]);

        }

        return ["results" => $results, "pages" => $pagecount, "current" => $page];
    }

    public function getUndefinedItemCount()
    {
        $condition = ["steam_market.market_price" => [0, NULL], "steam_price.price" => NULL,];

        if(!empty($match))
        {
            $condition["steam_market.name[~~]"] = "%".$match."%";
        }

        $items = $this->database->select("steam_market")
            ->leftJoin("steam_price", ["steam_market.id[=]steam_price.itemID"])->where($condition)
            ->numRows();

        return $items;
    }

    public function setUserPrice($appID, $market_name, $price, $userID)
    {
        $item = $this->getItem($appID, $market_name);

        $this->database->delete("steam_price")->where(["itemID" => $item["id"]]);
        $this->database->insert("steam_price")->set([
            "itemID" => $item["id"], "price" => $price * 100, "userID" => $userID, "created" => time()
        ]);
    }

    public function removeUserPrice($appID, $market_name)
    {
        $item = $this->getItem($appID, $market_name);

        $this->database->delete("steam_price")->where(["itemID" => $item["id"]]);
    }

    public function getItem($appID, $market_name)
    {
        return $this->database->select("steam_market")->column([
            "steam_market.id", "appID", "market_name", "market_volume",
            "market_price", "steam_price.price" => "user_price",
            "name", "color", "background", "image", "trend"
        ])->leftJoin("steam_price", ["steam_market.id[=]steam_price.itemID"])->where([
            "steam_market.appID" => $appID, "steam_market.market_name" => $market_name
        ])->fetchRow();
    }

    public function getItemGraph($appID, $market_name)
    {
        $this->database->query("SET time_zone = '+00:00'");

        $prices = $this->database->select("steam_price_history")
            ->column([
                ["str" => "DATE_FORMAT(FROM_UNIXTIME(dateline), '%Y-%m-%d')", "alias" => "day"],
                ["str" => "SUM(price) / COUNT(*)", "alias" => "price"], "volume"
            ])
            ->leftJoin("steam_market", ["steam_market.id[=]steam_price_history.itemID"])
            ->where(["appID" => $appID, "market_name" => $market_name, "steam_price_history.dateline[>=]" => time() - 3600 * 24 * 31])
            ->group("day")->fetchAssoc();

        $days = [];
        $timestamp = time();
        for($i = 0; $i < 30; $i++)
        {
            $days[] = date("Y-m-d", $timestamp);
            $timestamp -= 24 * 3600;
        }

        $days = array_reverse($days);
        $data = [];
        $lastprice = 0;

        foreach($days as $day)
        {
            $data[$day] = $lastprice;

            foreach($prices as $price)
            {
                if($price["day"] != $day)
                {
                    continue;
                }

                $data[$day] = round($price["price"] / 100, 2);
                $lastprice = $data[$day];
            }
        }

        $result = [];
        foreach($data as $day => $price)
        {
            $result[] = ["day" => $day, "price" => $price];
        }

        return $result;
    }
}