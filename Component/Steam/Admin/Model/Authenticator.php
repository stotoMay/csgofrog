<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Admin\Model;

use App\Router\Model;
use Component\Steam\Core;

class Authenticator extends Model
{
    public function setAuthenticatorData(Core $steam, $botID, $deviceID, $shared_secret, $serial_number, $revocation_code, $token_gid, $identity_secret, $secret_1)
    {
        $username = $this->getBotUsername($botID);

        $this->database->replace("steam_bot_mobile")->set([
            "username" => $username, "shared_secret" => $steam->crypt()->encrypt($shared_secret),
            "serial_number" => $serial_number, "revocation_code" => $steam->crypt()->encrypt($revocation_code),
            "token_gid" => $steam->crypt()->encrypt($token_gid),
            "identity_secret" => $steam->crypt()->encrypt($identity_secret),
            "secret_1" => $steam->crypt()->encrypt($secret_1),
            "device_id" => $steam->crypt()->encrypt($deviceID), "confirmed" => 0, "dateline" => time()
        ])->where(["username" => $username]);
    }

    public function confirmAuthenticator($botID)
    {
        $this->database->update("steam_bot_mobile")->set([
            "confirmed" => 1
        ])->where([
            "username" => $this->getBotUsername($botID)
        ]);
    }

    public function removeAuthenticatorRestriction($botID)
    {
        $this->database->update("steam_bot_mobile")->set([
            "dateline" => time() - 3600 * 24 * 7
        ])->where([
            "username" => $this->getBotUsername($botID)
        ]);
    }

    public function removeAuthenticator($botID)
    {
        $this->database->delete("steam_bot_mobile")
            ->where(["username" => $this->getBotUsername($botID)]);
    }

    public function setTradeBan($botID, $time)
    {
        $this->database->update("steam_bot")->set(["tradeable" => $time])->where(["id" => $botID]);
    }

    public function getTradeableTime($botID)
    {
        $data = $this->database->select("steam_bot")
            ->column(["tradeable"])
            ->where(["id" => $botID])->fetchAssoc();

        if($data["tradeable"])
        {
            return $data["tradeable"];
        }
        else
        {
            return time() - 1;
        }
    }

    public function getSteamDesktopAuthenticatorData($botID)
    {
        $data = [];

        $bot = $this->database->select("steam_bot")->where(["id" => $botID])->fetchRow();
        $authenticator = $this->database->select("steam_bot_mobile")->where(["username" => $bot["username"]])
            ->fetchRow();

        $data["shared_secret"] = $this->decrypt($authenticator["shared_secret"]);
        $data["serial_number"] = $authenticator["serial_number"];
        $data["revocation_code"] = $this->decrypt($authenticator["revocation_code"]);
        $data["uri"] = "otpauth://totp/Steam:".$bot["username"]."?secret=".strtoupper(base32_encode(base64_decode($authenticator["shared_secret"])))."&issuer=Steam";
        $data["servertime"] = $authenticator["dateline"];
        $data["account_name"] = $bot["username"];
        $data["token_gid"] = $this->decrypt($authenticator["token_gid"]);
        $data["identity_secret"] = $this->decrypt($authenticator["identity_secret"]);
        $data["secret_1"] = $this->decrypt($authenticator["secret_1"]);
        $data["status"] = 1;
        $data["device_id"] = $this->decrypt($authenticator["device_id"]);
        $data["fully_enrolled"] = true;

        $session = $this->database->select("steam_bot_session")->where(["botID" => $botID])->fetchRow();
        $auth = $this->database->select("steam_auth")->where(["steamID" => $bot["steamID"]])->fetchRow();

        $data["Session"] = [
            "SessionID" => "66fb20067254b6c7b55941db",
            "SteamLogin" => $bot["steamID"]."%7C%7C".$this->decrypt($session["login"]),
            "SteamLoginSecure" => $bot["steamID"]."%7C%7C".$this->decrypt($session["secure"]),
            "WebCookie" => $this->decrypt($auth["auth"]),
            "oAuthToken" => $this->decrypt($bot["oAuth"]),
            "SteamID" => $bot["steamID"],
        ];

        return $data;
    }

    public function getLoginData($botID)
    {
        $data = $this->database->select("steam_bot")
            ->column(["username", "password"])
            ->where(["id" => $botID])->fetchRow();

        return ["username" => $data["username"], "password" => $this->decrypt($data["password"])];
    }

    public function getBotUsername($botID)
    {
        $data = $this->database->select("steam_bot")
            ->column(["username"])
            ->where(["id" => $botID])->fetchRow();

        return $data["username"];
    }
}