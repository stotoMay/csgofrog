<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Admin\Model;

use App\Router\Model;

class Ajax extends Model
{
    public function getBotID($steamID)
    {
        $bot = $this->database->select("steam_bot")->where(["steamID" => $steamID])->fetchRow();

        if(isset($bot))
        {
            return $bot["id"];
        }
        else
        {
            return false;
        }
    }
}