<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\API;

use Component\Steam\App\AppList;
use Component\Steam\App\UserApp;
use Component\Steam\Exception\APIException;
use Component\Steam\Exception\MaxRetryException;
use Component\Steam\Group\Group as SteamGroup;
use Component\Steam\Group\GroupList;
use Component\Steam\User\User as SteamUser;
use Component\Steam\Item\Item;
use Component\Steam\Item\ItemList;
use Component\Steam\User\UserList;

class User
{
    private $api;

    /**
     * @param API $api
     */
    public function __construct(API $api)
    {
        $this->api = $api;
    }

    /**
     * @param array|string $steamIDs
     * @param bool $cache
     *
     * @return array
     * @throws APIException
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     */
    public function getPlayerSummaries($steamIDs, $cache = true, $save = true)
    {
        if(!is_array($steamIDs))
        {
            $steamIDs = [$steamIDs];
        }

        $result = [];
        foreach($steamIDs as $steamID)
        {
            if($this->api->tcache()->exists($steamID, "steam_player_summaries"))
            {
                $result[] = $this->api->tcache()->get($steamID, "steam_player_summaries");
            }
            elseif($cache && $this->api->mcache()->is_cached($steamID, "steam_player_summaries"))
            {
                $result[] = $this->api->mcache()->retrieve($steamID, "steam_player_summaries");
            }
        }

        if(count($result) != count($steamIDs))
        {
            $summaries = $this->api->api("ISteamUser", "GetPlayerSummaries", ["steamids" => implode(",", $steamIDs)], 2);

            if(is_array($summaries["response"]["players"]) && count($summaries["response"]["players"]) == count($steamIDs))
            {
                $result = [];
                foreach($summaries["response"]["players"] as $player)
                {
                    if($save)
                    {
                        $this->api->tcache()->set($player["steamid"], $player, "steam_player_summaries");
                        $this->api->mcache()->store($player["steamid"], $player, NULL, "steam_player_summaries");
                    }

                    $result[] = $player;
                }
            }
        }

        if(count($result) == count($steamIDs))
        {
            $data = [];
            foreach($result as $player)
            {
                $data[] = [
                    "steamid" => $player["steamid"],
                    "communityvisibilitystate" => $player["communityvisibilitystate"],
                    "personaname" => htmlspecialchars($player["personaname"], ENT_QUOTES),
                    "profileurl" => $player["profileurl"],
                    "avatar" => $player["avatar"], "avatarmedium" => $player["avatarmedium"],
                    "avatarfull" => $player["avatarfull"], "loccountrycode" => $player["loccountrycode"],
                    "locstatecode" => $player["locstatecode"], "loccityid" => $player["loccityid"],
                    "primaryclanid" => $player["primaryclanid"]
                ];
            }

            return $data;
        }
        else
        {
            throw new APIException(implode(",", $steamIDs));
        }
    }

    /**
     * @param string $steamID
     * @param int $appID
     * @param int $contextID
     * @param bool $raw
     * @param bool $parseItemData
     *
     * @return array|ItemList
     * @throws \App\HTTP\Exception\UrlException
     */
    public function getPlayerInventory($steamID, $appID, $contextID, $raw = false, $parseItemData = true)
    {
        $count = 5000;
        $lastID = NULL;

        $assets = [];
        $descriptions = [];

        $url = "https://steamcommunity.com/inventory/".$steamID."/".$appID."/".$contextID."/?l=english&count=".$count;

        while(true)
        {
            try
            {
                if(isset($lastID))
                {
                    $items = $this->api->json($url."&start_assetid=".$lastID);
                }
                else
                {
                    $items = $this->api->json($url);
                }
            }
            catch(MaxRetryException $e)
            {
                throw new APIException();
            }

            $assets = array_merge($assets, $items["assets"]);
            foreach($items["descriptions"] as $d)
            {
                $descriptions[$d["classid"]][$d["instanceid"]] = $d;
            }

            if($items["more_items"] != 1)
            {
                break;
            }

            $lastID = $items["last_assetid"];
        }

        if($raw === true)
        {
            return ["assets" => $assets, "descriptions" => $descriptions];
        }

        return $this->api->core()->parser()->item()->parseItemData($assets, $descriptions, $parseItemData);
    }

    /**
     * @param string $steamID
     * @param bool $cache
     *
     * @return int
     * @throws APIException
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     */
    public function getPlayerLevel($steamID, $cache = true)
    {
        if($cache && $this->api->mcache()->is_cached($steamID, "steam_player_level"))
        {
            return $this->api->mcache()->retrieve($steamID, "steam_player_level");
        }

        $level = $this->api->api("IPlayerService", "GetSteamLevel", ["steamid" => $steamID]);

        if(!is_numeric($level["response"]["player_level"]))
        {
            throw new APIException();
        }

        $this->api->mcache()->store($steamID, $level["response"]["player_level"], NULL, "steam_player_level");

        return $level["response"]["player_level"];
    }

    /**
     * @param string $steamID
     * @param bool $cache
     *
     * @return AppList
     * @throws APIException
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     */
    public function getPlayerGames($steamID, $cache = true)
    {
        if($cache && $this->api->mcache()->is_cached($steamID, "steam_player_games"))
        {
            $result = $this->api->mcache()->retrieve($steamID, "steam_player_games");
        }
        else
        {
            $result = [];
            $games = $this->api->api("IPlayerService", "GetOwnedGames", [
                "steamid" => $steamID, "include_appinfo" => 0, "include_played_free_games" => 1
            ]);

            if(!is_array($games["response"]["games"]))
            {
                throw new APIException();
            }

            foreach($games["response"]["games"] as $game)
            {
                $result[] = ["appID" => $game["appid"], "playtime" => $game["playtime_forever"]];
            }

            $this->api->mcache()->store($steamID, $result, NULL, "steam_player_games");
        }

        $user = new SteamUser($steamID);
        $appList = new AppList();
        foreach($result as $userapp)
        {
            $appList->append(new UserApp($userapp["appID"], $user, $userapp["playtime"] * 60));
        }

        return $appList;
    }

    /**
     * @param string $steamID
     * @param bool $cache
     *
     * @return GroupList
     * @throws APIException
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\HTTP\Exception\UrlException
     */
    public function getUserGroups($steamID, $cache = true)
    {
        if($cache && $this->api->mcache()->is_cached($steamID, "steam_player_groups"))
        {
            $result = $this->api->mcache()->retrieve($steamID, "steam_player_groups");
        }
        else
        {
            $result = [];
            $groups = $this->api->api("ISteamUser", "GetUserGroupList", ["steamid" => $steamID]);

            if($groups["response"]["success"] != true)
            {
                throw new APIException();
            }

            foreach($groups["response"]["groups"] as $group)
            {
                $result[] = $group["gid"];
            }

            $this->api->mcache()->store($steamID, $result, NULL, "steam_player_groups");
        }

        $groupList = new GroupList();
        foreach($result as $group)
        {
            $groupList->append(new SteamGroup($group));
        }

        return $groupList;
    }

    /**
     * @param string $steamID
     * @param bool $cache
     *
     * @return UserList
     *
     * @throws APIException
     */
    public function getPlayerFriends($steamID, $cache = true)
    {
        if($cache && $this->api->mcache()->is_cached($steamID, "steam_player_friends"))
        {
            $result = $this->api->mcache()->retrieve($steamID, "steam_player_friends");
        }
        else
        {
            $result = [];

            try
            {
                $friends = $this->api->api("ISteamUser", "GetFriendList", ["steamid" => $steamID]);
            }
            catch(MaxRetryException $e)
            {
                throw new APIException();
            }

            if(!is_array($friends["friendslist"]["friends"]))
            {
                throw new APIException();
            }

            foreach($friends["friendslist"]["friends"] as $user)
            {
                if($user["relationship"] != "friend")
                {
                    continue;
                }

                $result[] = ["steamID" => $user["steamid"], "dateline" => $user["friend_since"]];
            }

            $this->api->mcache()->store($steamID, $result, NULL, "steam_player_friends");
        }

        $userList = new UserList();
        foreach($result as $user)
        {
            $userList->append(new SteamUser($user["steamID"]));
        }

        return $userList;
    }

    /**
     * @param string $url
     *
     * @return array
     *
     * @throws APIException
     */
    public function splitTradeURL($url)
    {
        if(!is_string($url))
        {
            return NULL;
        }

        preg_match("#^https:\/\/trade\.opskins\.com\/t\/([0-9]+)\/([a-zA-Z0-9]+)#", $url, $match);

        if(!empty($match[2]))
        {
            $opskinId = $match[1];
            $token = $match[2];

            return ["token" => $token];
        }

        return NULL;
    }

    /**
     * @param \Component\Steam\User\User $user
     * @param string $token
     *
     * @return string
     */
    public function buildTradeURL($token = NULL)
    {
        $url = "https://trade.opskins.com/t/0000/";

        if(isset($token))
        {
            $url .= $token;
        }

        return $url;
    }

    /**
     * @param string $str
     * @param bool $cache
     *
     * @return string
     *
     * @throws APIException
     */
    public function getSteamID64($str, $cache = true)
    {
        if((is_int(strpos($str, "http://")) || is_int(strpos($str, "https://"))) && substr($str, -1) != "/")
        {
            $str .= "/";
        }

        if(strpos($str, "STEAM_") !== false)
        {
            $split = explode(":", str_replace("STEAM_", "", $str));

            return bcadd(bcadd(strval($split[2] * 2), "76561197960265728"), strval($split[1]));
        }
        elseif(strpos($str, "U:1:"))
        {
            return bcadd("76561197960265728", str_replace("U:1:", "", $str));
        }
        elseif(preg_match("#^https?\://steamcommunity\.com/profiles/(.*)/$#", $str, $match))
        {
            return $match[1];
        }
        elseif(preg_match("#^https?\://steamcommunity\.com/id/(.*)/$#", $str, $match))
        {
            return $this->api->steam()->resolveVanityURL($match[1], 1, $cache);
        }
        elseif(!is_numeric($str))
        {
            return $this->api->steam()->resolveVanityURL($str, 1, $cache);
        }
        elseif(is_numeric($str) && strlen($str) < 15)
        {
            return bcadd("76561197960265728", $str);
        }
        else
        {
            return $str;
        }
    }
}