<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\API;

use App\DOM\Element;
use App\DOM\Exception\NodeNotFoundException;
use App\DOM\XMLDocument;
use Component\Steam\Exception\APIException;
use Component\Steam\Exception\MaxRetryException;
use Component\Steam\User\User as SteamUser;
use Component\Steam\User\UserList;

class Group
{
    private $api;

    /**
     * @param API $api
     */
    public function __construct(API $api)
    {
        $this->api = $api;
    }

    /**
     * @param string $groupID
     * @param bool $cache
     *
     * @return array
     *
     * @throws \Component\Steam\Exception\APIException
     */
    public function getGroupSummaries($groupID, $cache = true)
    {
        if($this->api->tcache()->exists($groupID, "steam_group_summaries"))
        {
            return $this->api->tcache()->get($groupID, "steam_group_summaries");
        }

        if($cache && $this->api->mcache()->is_cached($groupID, "steam_group_summaries"))
        {
            return $this->api->mcache()->retrieve($groupID, "steam_group_summaries");
        }

        try
        {
            $xml = new XMLDocument($this->api->url("http://steamcommunity.com/gid/".$groupID."/memberslistxml/?xml=1"));
        }
        catch(MaxRetryException $e)
        {
            throw new APIException();
        }

        $group = [
            "groupID" => $xml->find("groupID64")->text(), "name" => $xml->find("groupName")->text(),
            "headline" => $xml->find("headline")->text(), "summary" => $xml->find("summary")->text(),
            "avatar" => $xml->find("avatarIcon")->text(), "membercount" => $xml->find("memberCount")->text(),
            "pages" => $xml->find("totalPages")->text()
        ];

        $this->api->mcache()->store($groupID, $group, NULL, "steam_group_summaries");
        $this->api->tcache()->set($groupID, $group, "steam_group_summaries");

        return $group;
    }

    /**
     * @param int $groupID
     * @param int $page
     *
     * @return UserList
     *
     * @throws APIException
     */
    public function getGroupMembers($groupID, $page = 1)
    {
        $userList = new UserList();

        try
        {
            $xml = new XMLDocument($this->api->url("http://steamcommunity.com/gid/".$groupID."/memberslistxml/?xml=1&p=".$page));
        }
        catch(MaxRetryException $e)
        {
            throw new APIException();
        }

        try
        {
            foreach($xml->find("steamID64")->item() as $steamID)
            {
                /**
                 * @var Element $steamID
                 */
                $userList->append(new SteamUser($steamID->text()));
            }
        }
        catch(NodeNotFoundException $e)
        {
            throw new APIException();
        }

        return $userList;
    }

    /**
     * @param string $str
     * @param bool $cache
     *
     * @return string
     *
     * @throws APIException
     */
    public function getSteamID64($str, $cache = true)
    {
        if(is_int(strpos($str, "http://")) && substr($str, -1) != "/")
        {
            $str .= "/";
        }

        if(strpos($str, "STEAM_") !== false)
        {
            $split = explode(":", str_replace("STEAM_", "", $str));

            return bcadd(bcadd(strval($split[2] * 2), "103582791429521408"), strval($split[1]));
        }
        elseif(strpos($str, "g:1:"))
        {
            return bcadd("103582791429521408", str_replace("g:1:", "", $str));
        }
        elseif(preg_match("#^https?\://steamcommunity\.com/gid/(.*)/$#", $str, $match))
        {
            return $match[1];
        }
        elseif(preg_match("#^https?\://steamcommunity\.com/groups/(.*)/$#", $str, $match))
        {
            return $this->api->steam()->resolveVanityURL($match[1], 2, $cache);
        }
        elseif(!is_numeric($str))
        {
            return $this->api->steam()->resolveVanityURL($str, 2, $cache);
        }
        elseif(is_numeric($str) && strlen($str) < 15)
        {
            return bcadd("103582791429521408", $str);
        }
        else
        {
            return $str;
        }
    }
}