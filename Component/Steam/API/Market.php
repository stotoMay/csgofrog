<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\API;

use App\DOM\Element;
use App\DOM\Exception\NodeNotFoundException;
use App\DOM\HTMLDocument;
use Component\Steam\Exception\APIException;
use Component\Steam\Exception\MaxRetryException;
use Component\Steam\Item\ItemList;
use Component\Steam\Item\MarketItem;

class Market
{
    private $api;

    /**
     * @param API $api
     */
    public function __construct(API $api)
    {
        $this->api = $api;
    }

    /**
     * @param int $appID
     * @param string $market_name
     *
     * @return float
     *
     * @throws APIException
     */
    public function getMarketPrice($appID, $market_name)
    {
        try
        {
            $item = $this->api->json("https://steamcommunity.com/market/priceoverview/?country=us&appid=".$appID."&market_hash_name=".rawurlencode($market_name));
        }
        catch(MaxRetryException $e)
        {
            throw new APIException();
        }

        if($item["success"])
        {
            if(!isset($item["lowest_price"]))
            {
                throw new APIException();
            }

            return preg_replace('/\$(.*)/', "$1", $item["lowest_price"]);
        }
        else
        {
            throw new APIException();
        }
    }

    /**
     * @param int $appID
     * @param string $market_name
     *
     * @return MarketItem
     *
     * @throws APIException
     */
    public function getItemByHash($appID, $market_name)
    {
        try
        {
            $market_page = $this->api->url("https://steamcommunity.com/market/listings/".$appID."/".rawurlencode($market_name));
        }
        catch(MaxRetryException $e)
        {
            throw new APIException();
        }

        $dom = new HTMLDocument($market_page);

        try
        {
            foreach($dom->find("script")->item() as $script)
            {
                /**
                 * @var Element $script
                 */
                if(preg_match("#var g_rgAssets = (.*);#", $script->text(), $asset))
                {
                    $asset = array_first(array_first(array_first(@json_decode($asset[1], true))));

                    if(!is_array($asset))
                    {
                        continue;
                    }

                    $instance = new MarketItem($appID, $market_name);
                    $instance->setData([
                        "color" => empty($asset["name_color"]) ? "#3a3a3a" : $asset["name_color"],
                        "background" => empty($asset["background_color"]) ? "#292929" : $asset["background_color"],
                        "name" => $asset["name"], "raw_image" => $asset["icon_url"]
                    ]);

                    return $instance;
                }
            }

            throw new APIException();
        }
        catch(NodeNotFoundException $e)
        {
            throw new APIException();
        }
    }

    /**
     * @param int $page
     * @param string $appID
     * @param string $sort_column
     * @param string $sort_dir
     *
     * @return ItemList
     *
     * @throws APIException
     */
    public function getMarketPage($page = 1, $appID = "", $sort_column = "popular", $sort_dir = "desc")
    {
        $start = ($page - 1) * 100;

        try
        {
            $market = $this->api->json("https://steamcommunity.com/market/search/render/?query=&start=".$start."&count=100&search_descriptions=0&sort_column=".$sort_column."&sort_dir=".$sort_dir."&appid=".$appID);
        }
        catch(MaxRetryException $e)
        {
            throw new APIException();
        }


        try
        {
            if($market["success"])
            {
                $dom = new HTMLDocument($market["results_html"]);

                $itemList = new ItemList();
                foreach($dom->find("a")->item() as $item)
                {
                    /**
                     * @var Element $item
                     */
                    $img = $item->find("img");

                    preg_match("#^https://steamcommunity\.com/market/listings/(.*)/(.*)$#", $item->attr("href"), $market_name);
                    preg_match("#^https://steamcommunity-a.akamaihd.net/economy/image/(.*)/62fx62f$#", $img->attr("src"), $image);
                    preg_match('#^\$(.*) USD$#', $item->find(".normal_price")->item(1)->html(), $price);

                    $instance = new MarketItem($market_name[1], rawurldecode($market_name[2]));
                    $instance->setData([
                        "raw_image" => $image[1], "name" => rawurldecode($market_name[2]),
                        "color" => $img->css("border-color", "#3a3a3a"),
                        "background" => $img->css("background-color", "#292929"),
                        "market_volume" => (int)str_replace([",", " "], "", $item->find(".market_listing_num_listings_qty")->text()),
                        "price" => (float)str_replace(" ", "", $price[1])
                    ]);
                    $itemList->append($instance);
                }

                return $itemList;
            }

            throw new APIException();
        }
        catch(NodeNotFoundException $e)
        {
            throw new APIException();
        }
    }

    /**
     * @param string $appID
     *
     * @return array
     *
     * @throws APIException
     */
    public function getMarketInfo($appID = "")
    {
        try
        {
            $market = $this->api->json("https://steamcommunity.com/market/search/render/?query=&start=0&count=1&search_descriptions=0&sort_column=popular&sort_dir=desc&appid=".$appID);
        }
        catch(MaxRetryException $e)
        {
            throw new APIException();
        }

        if($market["success"])
        {
            return [
                "totalItems" => $market["total_count"], "totalPages" => intval(($market["total_count"] / 100)) + 1
            ];
        }

        throw new APIException();
    }
}