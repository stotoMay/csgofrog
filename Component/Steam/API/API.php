<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\API;

use App\HTTP\Exception\UrlException;
use App\Tools\Request;
use App\Storage\MCache;
use App\Storage\TCache;

use Component\Steam\Core;
use Component\Steam\Exception\APIException;
use Component\Steam\Exception\MaxRetryException;

class API
{
    private $key;

    private $steam;
    private $user;
    private $group;
    private $market;

    private $mcache;
    private $tcache;
    private $core;

    /**
     * @param string $apikey
     * @param Core $core
     * @param MCache $mcache
     * @param TCache $tcache
     */
    public function __construct($apikey, Core $core, MCache $mcache, TCache $tcache)
    {
        $this->key = $apikey;

        $this->tcache = $tcache;
        $this->mcache = $mcache;

        $this->core = $core;

        $this->steam = new Steam($this);
        $this->user = new User($this);
        $this->group = new Group($this);
        $this->market = new Market($this);
    }

    /**
     * @return Steam
     */
    public function steam()
    {
        return $this->steam;
    }

    /**
     * @return User
     */
    public function user()
    {
        return $this->user;
    }

    /**
     * @return Group
     */
    public function group()
    {
        return $this->group;
    }

    /**
     * @return Market
     */
    public function market()
    {
        return $this->market;
    }


    /**
     * @return TCache
     */
    public function tcache()
    {
        return $this->tcache;
    }

    /**
     * @return MCache
     */
    public function mcache()
    {
        return $this->mcache;
    }
    
    /**
     * @return Core
     */
    public function core()
    {
        return $this->core;
    }


    /**
     * @param string $url
     *
     * @param int $retry
     *
     * @return string
     * @throws MaxRetryException
     */
    public function url($url, $retry = 3)
    {
        if($retry <= 0)
        {
            throw new MaxRetryException();
        }

        $curl = new Request($url);

        try
        {
            $response = $curl->getResponseContent();
        }
        catch(UrlException $e)
        {
            return $this->url($url, $retry - 1);
        }

        if($response == "null" || empty($response))
        {
            return $this->url($url, $retry - 1);
        }
        else
        {
            return $response;
        }
    }

    /**
     * @param string $url
     *
     * @param int $retry
     *
     * @return array
     *
     * @throws MaxRetryException
     */
    public function json($url, $retry = 3)
    {
        $response = @json_decode($this->url($url, $retry), true);
        if(!is_array($response))
        {
            return $this->json($url, $retry - 1);
        }
        else
        {
            return $response;
        }
    }

    /**
     * @param string $class
     * @param string $method
     * @param array $args
     * @param int $version
     * @param string $key
     * @param int $retry
     *
     * @return array
     *
     * @throws APIException
     */
    public function api($class, $method, $args = [], $version = 1, $key = NULL, $retry = 3)
    {
        if(!isset($args["format"]))
        {
            $args["format"] = "json";
        }
        if(!isset($key))
        {
            $key = $this->key;
        }

        $url = "https://api.steampowered.com/".$class."/".$method."/v".$version."/?key=".$key;
        $args = is_array($args) ? $args : [];

        foreach($args as $k => $v)
        {
            $url .= "&".$k."=".$v;
        }

        try
        {
            $response = $this->json($url, $retry);
        }
        catch(MaxRetryException $e)
        {
            throw new APIException();
        }

        if(!is_array($response))
        {
            throw new APIException();
        }
        else
        {
            return $response;
        }
    }
}