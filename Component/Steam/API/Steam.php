<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\API;

use Component\Steam\Exception\APIException;

class Steam
{
    private $api;

    /**
     * @param API $api
     */
    public function __construct(API $api)
    {
        $this->api = $api;
    }

    /**
     * @param int $appID
     * @param bool $cache
     *
     * @return string
     *
     * @throws APIException
     */
    public function getAppName($appID, $cache = true)
    {
        $appList = NULL;
        if($this->api->tcache()->exists("steam_apps"))
        {
            $appList = $this->api->tcache()->get("steam_apps");
        }

        if(!is_array($appList) && $cache && $this->api->mcache()->is_cached("steam_apps"))
        {
            $appList = $this->api->mcache()->retrieve("steam_apps");
            $this->api->tcache()->set("steam_apps", $appList);
        }

        if(!is_array($appList))
        {
            $appList = [];
            $apps = $this->api->api("ISteamApps", "GetAppList", [], 2);

            if(!is_array($apps["applist"]["apps"]))
            {
                throw new APIException();
            }

            foreach($apps["applist"]["apps"] as $app)
            {
                $appList[$app["appid"]] = $app["name"];
            }

            $this->api->tcache()->set("steam_apps", $appList);
            $this->api->mcache()->store("steam_apps", $appList);
        }

        return $appList[$appID];
    }

    /**
     * @param string $name
     * @param int $type
     * @param bool $cache
     *
     * @return string
     *
     * @throws APIException
     */
    public function resolveVanityURL($name, $type = 1, $cache = true)
    {
        if($type == 1)
        {
            $class = "steam_vanity_user";
        }
        elseif($type == 2)
        {
            $class = "steam_vanity_group";
        }
        elseif($type == 3)
        {
            $class = "steam_vanity_official";
        }
        else
        {
            throw new APIException();
        }

        if($this->api->tcache()->exists($name, $class))
        {
            return $this->api->tcache()->get($name, $class);
        }

        if($cache && $this->api->mcache()->is_cached($name, $class))
        {
            $steamID = $this->api->mcache()->retrieve($name, $class);

            if(empty($steamID))
            {
                return $this->resolveVanityURL($name, $type, false);
            }

            return $steamID;
        }

        $response = $this->api->api("ISteamUser", "ResolveVanityURL", ["vanityurl" => $name, "url_type" => $type]);
        if($response["response"]["success"] == 1)
        {
            $this->api->mcache()->store($name, $response["response"]["steamid"], time() + 3600 * 24 * 4, $class);
            $this->api->tcache()->set($name, $response["response"]["steamid"], $class);

            return $response["response"]["steamid"];
        }
        else
        {
            throw new APIException();
        }
    }
}