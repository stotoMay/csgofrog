<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Bot;

use App\Core\App;
use Component\Steam\Core;
use Component\Steam\Exception\APIException;
use Component\Steam\HTTP\Authenticator;
use Component\Steam\HTTP\Client;

class HTTP
{
    private $app;
    private $steam;
    private $bot;

    private $client;
    private $authenticator;
    private $cache;

    private $apikey;
    private $oAuth;

    public function __construct(Bot $bot, App $app, Core $steam)
    {
        $this->app = $app;
        $this->steam = $steam;
        $this->bot = $bot;

        $this->cache = $bot->getDatabaseRow();
        $this->apikey = $this->cache["apikey"];
        $this->oAuth = empty($this->cache["oAuth"]) ? "" : $this->steam->crypt()->decrypt($this->cache["oAuth"]);
    }

    public function saveCookies()
    {
        if(!is_object($this->client))
        {
            return;
        }

        $c = json_encode($this->client()->getBrowserCookies());

        if($this->cache["cookie"] != $c)
        {
            $this->app->database()->update("steam_bot_session")->set(["cookie" => $c])
                ->where(["id" => $this->bot->getBotID()]);
        }

        if($this->client()->is_refreshed())
        {
            $this->app->database()->update("steam_bot_session")->set([
                "login" => $this->steam->crypt()->encrypt($this->client()->getSteamLoginCookie()),
                "secure" => $this->steam->crypt()->encrypt($this->client()->getSteamLoginSecureCookie()),
            ])->where([
                "botID" => $this->bot->getBotID()
            ])->execute();
        }
    }

    /**
     * @return Client
     */
    public function client()
    {
        if(!is_object($this->client))
        {
            $this->client = new Client($this->cache["steamID"], $this->steam->crypt()
                ->decrypt($this->cache["login"]), $this->steam->crypt()
                ->decrypt($this->cache["secure"]), $this->oAuth, @json_decode($this->cache["cookie"], true));
        }

        return $this->client;
    }


    /**
     * @return Authenticator
     */
    public function authenticator()
    {
        if(!is_object($this->authenticator))
        {
            $mobile = $this->app->database()->select("steam_bot_mobile")
                ->where(["username" => $this->cache["username"]])
                ->fetchAssoc();

            if(count($mobile) == 0)
            {
                $this->authenticator = new Authenticator($this->client());
            }
            else
            {
                $row = $mobile[0];

                $data = [
                    "shared_secret" => $this->steam->crypt()->decrypt($row["shared_secret"]),
                    "revocation_code" => $this->steam->crypt()->decrypt($row["revocation_code"]),
                    "token_gid" => $this->steam->crypt()->decrypt($row["token_gid"]),
                    "identity_secret" => $this->steam->crypt()->decrypt($row["identity_secret"]),
                    "secret_1" => $this->steam->crypt()->decrypt($row["secret_1"]),
                ];

                $this->authenticator = new Authenticator($this->client(), $data, $this->steam->crypt()
                    ->decrypt($row["device_id"]), $row["confirmed"] ? true : false);
            }
        }

        return $this->authenticator;
    }

    /**
     * @return bool
     */
    public function hasAuthenticator()
    {
        return $this->authenticator()->is_attatched();
    }

    /**
     * @param string $class
     * @param string $method
     * @param array $args
     * @param int $version
     *
     * @return array
     * @throws APIException
     */
    public function api($class, $method, $args, $version = 1)
    {
        if(empty($this->apikey))
        {
            throw new APIException();
        }

        return $this->steam->api()->api($class, $method, $args, $version, $this->apikey);
    }
}