<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Bot;

use App\Core\App;
use Component\Steam\Core;
use Component\Steam\Exception\APIException;
use Component\Steam\Exception\TradeException;
use Component\Steam\Exception\MaxRetryException;
use Component\Steam\Item\ItemList;
use Component\Steam\Offer\Offer;
use Component\Steam\Offer\OfferList;
use Component\Steam\User\User;

class Trading
{
    private $app;
    private $steam;
    private $bot;

    private $cache;
    private $apikey;

    private $receivedItems;

    public function __construct(Bot $bot, App $app, Core $steam)
    {
        $this->app = $app;
        $this->steam = $steam;
        $this->bot = $bot;

        $this->cache = $bot->getDatabaseRow();
        $this->apikey = $this->cache["apikey"];
    }

    /**
     * @param $tradeID
     * @param bool $descriptions
     * @param bool $parseItemInfo
     *
     * @return ItemList
     * @throws MaxRetryException
     * @throws TradeException
     */
    public function getReceivedItems($tradeID, $descriptions = true, $parseItemInfo = true)
    {
        if(is_array($this->receivedItems[$tradeID]))
        {
            return $this->receivedItems[$tradeID];
        }

        try
        {
            $response = $this->bot->http()->api("IEconService", "GetTradeStatus", ["tradeid" => $tradeID, "language" => "en", "get_descriptions" => $descriptions ? 1 : 0]);
        }
        catch(APIException $e)
        {
            throw new TradeException();
        }

        if(count($response["response"]["trades"]) == 0)
        {
            throw new TradeException();
        }

        if($response["response"]["trades"][0]["status"] != 3)
        {
            throw new TradeException();
        }

        $descriptions = [];
        $assets = [];

        if(is_array($response["response"]["descriptions"]))
        {
            foreach($response["response"]["descriptions"] as $d)
            {
                $descriptions[$d["classid"]][$d["instanceid"]] = $d;
            }
        }

        foreach($response["response"]["trades"][0]["assets_received"] as $item)
        {
            $assets[] = [
                "appid" => $item["appid"], "contextid" => $item["new_contextid"], "assetid" => $item["new_assetid"],
                "classid" => $item["classid"], "instanceid" => $item["instanceid"], "amount" => $item["amount"]
            ];
        }

        $this->receivedItems[$tradeID] = $this->steam->parser()->item()->parseItemData($assets, $descriptions, $parseItemInfo);

        return $this->receivedItems[$tradeID];
    }

    /**
     * @param string $id
     * @param string $partnerID
     *
     * @return string
     *
     * @throws TradeException
     * @throws \Component\Steam\Exception\ConfirmationException
     */
    public function acceptTrade($id, $partnerID)
    {
        $post = [
            "sessionid" => $this->bot->http()->client()->getSessionID(), "serverid" => "1", "tradeofferid" => $id,
            "partner" => $partnerID, "captcha" => ""
        ];

        try
        {
            $response = $this->bot->http()->client()->credentials("https://steamcommunity.com/tradeoffer/".$id."/accept", "https://steamcommunity.com/tradeoffer/".$id."/", $post);
        }
        catch(MaxRetryException $e)
        {
            throw new TradeException();
        }

        $json = @json_decode($response->getResponseContent(), true);

        if($json["needs_mobile_confirmation"])
        {
            if($this->bot->http()->hasAuthenticator())
            {
                $this->bot->http()->authenticator()->confirmAction("trade");
            }
            else
            {
                throw new TradeException();
            }
        }
        elseif($json["needs_email_confirmation"])
        {
            throw new TradeException();
        }

        if($response->getStatusCode() != 200)
        {
            throw new TradeException($json["strError"]);
        }

        return $json["tradeid"];
    }

    /**
     * @param string $id
     *
     * @return bool
     *
     * @throws TradeException
     */
    public function cancelTrade($id)
    {
        try
        {
            $response = $this->bot->http()->client()->post("https://steamcommunity.com/tradeoffer/".$id."/cancel", [
                "sessionid" => $this->bot->http()->client()->getSessionID()
            ]);
        }
        catch(MaxRetryException $e)
        {
            throw new TradeException("steam request failed");
        }

        $json = @json_decode($response->getResponseContent(), true);

        if($response->getStatusCode() != 200)
        {
            if(is_int(strpos($json["strError"], "(16)")) || is_int(strpos($json["strError"], "(20)")))
            {
                return $this->cancelTrade($id);
            }

            throw new TradeException($json["strError"]);
        }

        return true;
    }

    /**
     * @param string $id
     *
     * @return bool
     *
     * @throws TradeException
     */
    public function declineTrade($id)
    {
        try
        {
            $response = $this->bot->http()->client()->post("https://steamcommunity.com/tradeoffer/".$id."/decline", [
                "sessionid" => $this->bot->http()->client()->getSessionID()
            ]);
        }
        catch(MaxRetryException $e)
        {
            throw new TradeException("steam request failed");
        }
        $json = @json_decode($response->getResponseContent(), true);

        if($response->getStatusCode() != 200)
        {
            if(is_int(strpos($json["strError"], "(16)")) || is_int(strpos($json["strError"], "(20)")))
            {
                return $this->declineTrade($id);
            }

            throw new TradeException($json["strError"]);
        }

        return true;
    }

    /**
     * @param string $offerID
     *
     * @return bool
     */
    public function is_holded($offerID)
    {
        try
        {
            $content = $this->bot->http()->client()->get("https://steamcommunity.com/tradeoffer/".$offerID."/")->getResponseContent();
        }
        catch(MaxRetryException $e)
        {
            return true;
        }

        preg_match('#g_daysMyEscrow = (.*);#', $content, $mytradehold);
        preg_match('#g_daysTheirEscrow = (.*);#', $content, $histradehold);

        if($mytradehold[1] == 0 && $histradehold[1] == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * @param int $time
     * @param bool $descriptions
     * @param bool $parseItemInfo
     *
     * @return OfferList
     * @throws TradeException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getReceivedTrades($time = NULL, $descriptions = true, $parseItemInfo = true)
    {
        if(isset($this->apikey))
        {
            $options = [
                "get_sent_offers" => 0, "get_received_offers" => 1, "get_descriptions" => $descriptions ? 1 : 0,
                "time_historical_cutoff" => time()
            ];
            if(isset($time))
            {
                $options["time_historical_cutoff"] = $time;
                $options["active_only"] = 0;
                $options["historical_only"] = 0;
            }
            else
            {
                $options["active_only"] = 1;
            }

            return $this->parseTrades($this->bot->http()->api("IEconService", "GetTradeOffers", $options), $parseItemInfo);
        }
        else
        {
            throw new TradeException();
        }
    }

    /**
     * @param int $time
     * @param bool $descriptions
     * @param bool $parseItemInfo
     *
     * @return OfferList
     * @throws TradeException
     * @throws \Component\Steam\Exception\APIException
     */
    public function getSentTrades($time = NULL, $descriptions = true, $parseItemInfo = true)
    {
        if(isset($this->apikey))
        {
            $options = [
                "get_sent_offers" => 1, "get_received_offers" => 0, "get_descriptions" => $descriptions ? 1 : 0,
                "time_historical_cutoff" => time()
            ];
            if(isset($time))
            {
                $options["time_historical_cutoff"] = $time;
                $options["active_only"] = 0;
                $options["historical_only"] = 0;
            }
            else
            {
                $options["active_only"] = 1;
            }

            return $this->parseTrades($this->bot->http()->api("IEconService", "GetTradeOffers", $options), $parseItemInfo);
        }
        else
        {
            throw new TradeException();
        }
    }

    /**
     * @param array $json
     * @param bool $parseItemInfo
     *
     * @return OfferList
     *
     * @throws TradeException
     */
    private function parseTrades($json, $parseItemInfo = true)
    {
        $offerList = new OfferList();

        $trades = [];
        foreach(array_clear((array)$json["response"]["trade_offers_received"]) as $trade)
        {
            if(!is_array($trade))
            {
                throw new TradeException();
            }

            $trade["mode"] = "received";
            $trades[$trade["tradeofferid"]] = $trade;
        }
        foreach(array_clear((array)$json["response"]["trade_offers_sent"]) as $trade)
        {
            if(!is_array($trade))
            {
                throw new TradeException();
            }

            $trade["mode"] = "sent";
            $trades[$trade["tradeofferid"]] = $trade;
        }

        $descr = [];
        foreach((array)$json["response"]["descriptions"] as $d)
        {
            $descr[$d["classid"]][$d["instanceid"]] = $d;
        }

        foreach($trades as $trade)
        {
            $myItemList = $this->steam->parser()->item()->parseItemData($trade["items_to_give"], $descr, $parseItemInfo);
            $hisItemList = $this->steam->parser()->item()->parseItemData($trade["items_to_receive"], $descr, $parseItemInfo);

            $offer = new Offer($trade["tradeofferid"], $this->bot, new User($trade["accountid_other"]), $myItemList, $hisItemList, $trade["trade_offer_state"], $trade["mode"]);

            if(isset($trade["tradeid"]))
            {
                $offer->setTradeID($trade["tradeid"]);
            }

            $offerList->append($offer);
        }

        return $offerList;
    }
}