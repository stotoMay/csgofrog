<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Bot;

use App\Bootstrap\Factory;
use Component\Steam\Exception\BotNotFoundException;
use Component\Steam\Offer\Trade;
use Component\Steam\SteamFactory;
use Component\Steam\User\User;

class Bot
{
    private static $instance = [];

    private $app;
    private $steam;

    private $user;
    private $trading;
    private $http;
    private $action;

    private $id;
    private $token;
    private $apikey;

    private $cache;

    public function __construct($botID)
    {
        $this->app = Factory::getApplication();
        $this->steam = SteamFactory::getSteamCore();

        $bots = NULL;
        if($this->app->tcache()->exists("steam_bots"))
        {
            $bots = $this->app->tcache()->get("steam_bots");
        }

        if(!is_array($bots))
        {
            $bots = [];
            $botrow = $this->app->database()->select("steam_bot")
                ->leftJoin("steam_bot_session", ["steam_bot.id[=]steam_bot_session.botID"])
                ->fetchAssoc();

            foreach($botrow as $row)
            {
                $bots[$row["botID"]] = $row;
            }

            $this->app->tcache()->set("steam_bots", $bots);
        }

        if(!is_array($bots[$botID]))
        {
            throw new BotNotFoundException();
        }

        $this->user = new User($bots[$botID]["steamID"]);
        $this->token = $bots[$botID]["tradetoken"];
        $this->apikey = $bots[$botID]["apikey"];

        $this->id = $botID;
        $this->cache = $bots[$botID];

        $this->trading = new Trading($this, $this->app, $this->steam);
        $this->http = new HTTP($this, $this->app, $this->steam);
        $this->action = new Action($this, $this->app, $this->steam);
    }

    /**
     * @param User|Bot $partner
     * @param string $token
     *
     * @return Trade
     */
    public function trade($partner, $token = NULL)
    {
        return new Trade($this, $partner, $token);
    }

    /**
     * @return Trading
     */
    public function trading()
    {
        return $this->trading;
    }

    /**
     * @return HTTP
     */
    public function http()
    {
        return $this->http;
    }

    /**
     * @return Action
     */
    public function action()
    {
        return $this->action;
    }

    /**
     * @return User
     */
    public function user()
    {
        return $this->user;
    }

    /**
     * @return array
     */
    public function getDatabaseRow()
    {
        return $this->cache;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getTradeURL()
    {
        return $this->steam->api()->user()->buildTradeURL($this->getToken());
    }

    /**
     * @return string
     */
    public function getAPIKey()
    {
        return $this->apikey;
    }

    /**
     * @return int
     */
    public function getBotID()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function is_tradeable()
    {
        if(!isset($this->token) || !isset($this->apikey))
        {
            return false;
        }

        return $this->cache["tradeable"] ? $this->cache["tradeable"] < time() : true;
    }

    /**
     * @param int $botID
     *
     * @return Bot
     */
    public static function getInstance($botID)
    {
        if(!is_object(self::$instance[$botID]))
        {
            self::$instance[$botID] = new Bot($botID);
        }

        return self::$instance[$botID];
    }

    public static function dispatch()
    {
        foreach(self::$instance as $bot)
        {
            /**
             * @var Bot $bot
             */
            $bot->http()->saveCookies();
        }
    }
}