<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Bot;

use App\Core\App;
use Component\Steam\Core;
use Component\Steam\Exception\APIException;
use Component\Steam\Exception\AddFriendException;
use Component\Steam\Exception\RemoveFriendException;
use Component\Steam\Exception\SellItemException;
use Component\Steam\Exception\MaxRetryException;
use Component\Steam\Exception\TradeException;
use Component\Steam\User\User;

class Action
{
    private $app;
    private $steam;
    private $bot;

    /**
     * Action constructor.
     *
     * @param Bot $bot
     * @param App $app
     * @param Core $steam
     */
    public function __construct(Bot $bot, App $app, Core $steam)
    {
        $this->app = $app;
        $this->steam = $steam;
        $this->bot = $bot;
    }

    /**
     * @param string $steamID
     * @param string $appID
     * @param string $contextID
     * @param bool $raw
     * @param bool $parseItemData
     *
     * @return array|\Component\Steam\Item\ItemList
     *
     * @throws APIException
     */
    public function getPartnerInventory($steamID, $appID, $contextID, $raw = false, $parseItemData = true)
    {
        $user = new User($steamID);

        $referer = "https://steamcommunity.com/tradeoffer/new/?partner=".$user->getSteamID32();

        $params = [
            "sessionid=".$this->bot->http()->client()->getSessionID(),
            "partner=".$user->getSteamID64(), "appid=".$appID, "contextid=".$contextID
        ];

        $url = "https://steamcommunity.com/tradeoffer/new/partnerinventory/?".implode("&", $params);
        $start = 0;

        $assets = [];
        $descriptions = [];

        while(true)
        {
            try
            {
                $data = $this->bot->http()->client()->credentials($url."&start=".$start, $referer);
            }
            catch(MaxRetryException $e)
            {
                throw new APIException();
            }

            $items = @json_decode($data->getResponseContent(), true);

            if(!is_array($items))
            {
                throw new APIException();
            }

            foreach($items["rgInventory"] as $item)
            {
                $item["assetid"] = $item["id"];
                $item["appid"] = $appID;
                $item["contextid"] = $contextID;

                unset($item["id"], $item["pos"]);

                $assets[] = $item;
            }

            foreach($items["rgDescriptions"] as $d)
            {
                $descriptions[$d["classid"]][$d["instanceid"]] = $d;
            }

            if($items["more"] != 1)
            {
                break;
            }

            $start = $items["more_start"];
        }

        if($raw)
        {
            return ["assets" => $assets, "descriptions" => $descriptions];
        }

        return $this->steam->parser()->item()->parseItemData($assets, $descriptions, $parseItemData);
    }

    /**
     * @param string $appID
     * @param string $contextID
     * @param bool $raw
     * @param bool $parseItemData
     *
     * @return array|\Component\Steam\Item\ItemList
     * @throws APIException
     */
    public function getOwnInventory($appID, $contextID, $raw = false, $parseItemData = true)
    {
        $count = 5000;
        $lastID = NULL;

        $assets = [];
        $descriptions = [];

        $url = "https://steamcommunity.com/inventory/".$this->bot->user()->getSteamID64()."/".$appID."/".$contextID."/?l=english&count=".$count;

        while(true)
        {
            try
            {
                if(isset($lastID))
                {
                    $data = $this->bot->http()->client()->get($url."&start_assetid=".$lastID);
                }
                else
                {
                    $data = $this->bot->http()->client()->get($url);
                }
            }
            catch(MaxRetryException $e)
            {
                throw new APIException();
            }

            $items = @json_decode($data->getResponseContent(), true);

            if(!is_array($items))
            {
                throw new APIException();
            }

            $assets = array_merge($assets, $items["assets"]);
            foreach($items["descriptions"] as $d)
            {
                $descriptions[$d["classid"]][$d["instanceid"]] = $d;
            }

            if($items["more_items"] != 1)
            {
                break;
            }

            $lastID = $items["last_assetid"];
        }

        if($raw === true)
        {
            return ["assets" => $assets, "descriptions" => $descriptions];
        }

        return $this->steam->parser()->item()->parseItemData($assets, $descriptions, $parseItemData);
    }

    /**
     * @param $steamID
     *
     * @return bool
     *
     * @throws APIException
     * @throws AddFriendException
     */
    public function addFriend($steamID)
    {
        $steamID = $this->steam->api()->user()->getSteamID64($steamID);

        try
        {
            $response = $this->bot->http()->client()->post("http://steamcommunity.com/actions/AddFriendAjax", [
                "sessionID" => $this->bot->http()->client()->getSessionID(), "steamid" => $steamID, "accept_invite" => 0
            ]);
        }
        catch(MaxRetryException $e)
        {
            throw new AddFriendException("steam request failed");
        }

        if($response->getStatusCode() == 200)
        {
            return true;
        }

        throw new AddFriendException();
    }

    /**
     * @param $steamID
     *
     * @return bool
     *
     * @throws RemoveFriendException
     */
    public function removeFriend($steamID)
    {
        try
        {
            $steamID = $this->steam->api()->user()->getSteamID64($steamID);
        }
        catch(APIException $e)
        {
            throw new RemoveFriendException();
        }

        try
        {
            $response = $this->bot->http()->client()->post("http://steamcommunity.com/actions/RemoveFriendAjax", [
                "sessionID" => $this->bot->http()->client()->getSessionID(), "steamid" => $steamID
            ]);
        }
        catch(MaxRetryException $e)
        {
            throw new RemoveFriendException("steam request failed");
        }

        if($response->getStatusCode() == 200)
        {
            return true;
        }

        throw new RemoveFriendException();
    }

    /**
     * @param $price
     * @param $appID
     * @param $contextID
     * @param $assetID
     * @param int $amount
     *
     * @return bool
     * @throws SellItemException
     * @throws \Component\Steam\Exception\ConfirmationException
     */
    public function sellMarketItem($price, $appID, $contextID, $assetID, $amount = 1)
    {
        $url = "https://steamcommunity.com/market/sellitem/";
        $referer = "http://steamcommunity.com/profiles/".$this->bot->user()->getSteamID64()."/inventory/";
        $post = [
            "sessionid" => $this->bot->http()->client()->getSessionID(), "appid" => $appID, "contextid" => $contextID,
            "assetid" => $assetID, "amount" => $amount, "price" => $price * 100
        ];

        try
        {
            $response = $this->bot->http()->client()->credentials($url, $referer, $post);
        }
        catch(MaxRetryException $e)
        {
            throw new SellItemException();
        }

        if($response->getStatusCode() == 200)
        {
            $data = json_decode($response->getResponseContent(), true);

            if($data["success"] && $data["requires_confirmation"] == 0)
            {
                return true;
            }

            if($data["success"] && $data["needs_mobile_confirmation"])
            {
                $this->bot->http()->authenticator()->confirmAction("sell");

                return true;
            }
        }

        throw new SellItemException();
    }
}