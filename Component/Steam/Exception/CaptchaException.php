<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Exception;

class CaptchaException extends \Exception
{
    private $captchaID;
    private $image;

    /**
     * @param string $message
     * @param int $captchaID
     * @param string $image
     */
    public function __construct($message, $captchaID, $image)
    {
        $this->captchaID = $captchaID;
        $this->image = $image;
        parent::__construct($message);
    }

    public function getCaptchaID()
    {
        return $this->captchaID;
    }

    public function getCaptchaImage()
    {
        return $this->image;
    }
}