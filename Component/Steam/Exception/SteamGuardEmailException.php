<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Exception;

class SteamGuardEmailException extends \Exception
{
    private $steamID;

    public function __construct($message, $steamID)
    {
        parent::__construct($message);
        $this->steamID = $steamID;
    }

    public function getSteamID()
    {
        return $this->steamID;
    }
}