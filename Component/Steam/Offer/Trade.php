<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Offer;

use App\Bootstrap\Factory;
use Component\Steam\Bot\Bot;
use Component\Steam\Item\ItemList;
use Component\Steam\SteamFactory;
use Component\Steam\User\User;
use Component\Steam\Exception\TradeException;

class Trade
{
    private $app;
    private $steam;

    private $myassets;
    private $hisassets;

    private $primary;
    private $secondary;

    private $trader;
    private $token;

    private $offerID;
    private $tradeID;

    private $message;

    /**
     * @param Bot $a
     * @param Bot|User $b
     * @param string $token
     *
     * @throws TradeException
     */
    public function __construct($a, $b, $token = NULL)
    {
        $this->app = Factory::getApplication();
        $this->steam = SteamFactory::getSteamCore();

        $this->message = "";
        $this->token = $token;

        if(!is_object($a) || !is_object($b))
        {
            throw new TradeException();
        }

        $this->primary = $a;

        if(get_class($b) == 'Component\\Steam\\Bot\\Bot')
        {
            $this->secondary = $b;
            $this->token = $b->getToken();
            $this->trader = $b->user();
        }
        else
        {
            $this->trader = $b;
        }

        $this->myassets = [];
        $this->hisassets = [];
    }

    /**
     * @param string $msg
     */
    public function setMessage($msg)
    {
        if(!is_string($msg))
        {
            return;
        }

        $this->message = $msg;
    }

    /**
     * @return int
     * @throws TradeException
     * @throws \App\Database\Exception\ParseException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\ClientSessionException
     */
    public function execute()
    {
        if(is_string($this->offerID))
        {
            return $this->offerID;
        }

        if(count($this->myassets) == 0 && count($this->hisassets) == 0)
        {
            throw new TradeException();
        }

        $post = [
            "sessionid" => $this->primary->http()->client()->getSessionID(), "serverid" => "1",
            "partner" => $this->trader->getSteamID64(),
            "trade_offer_create_params" => empty($this->token) ? "{}" : json_encode(["trade_offer_access_token" => $this->token]),
            "captcha" => "", "tradeoffermessage" => $this->message, "json_tradeoffer" => json_encode([
                "newversion" => true, "version" => 2,
                "me" => ["assets" => array_values($this->myassets), "currency" => [], "ready" => false],
                "them" => ["assets" => array_values($this->hisassets), "currency" => [], "ready" => false]
            ])
        ];

        $response = $this->primary->http()->client()
            ->credentials("https://steamcommunity.com/tradeoffer/new/send", $this->steam->api()->user()
                ->buildTradeURL($this->token), $post);

        $json = @json_decode($response->getResponseContent(), true);

        if(!is_array($json))
        {
            throw new TradeException();
        }

        if(!is_numeric($json["tradeofferid"]))
        {
            throw new TradeException($json["strError"]);
        }

        if($json["needs_email_confirmation"])
        {
            throw new TradeException("email confirmation needed");
        }

        if($json["needs_mobile_confirmation"])
        {
            if($this->primary->http()->hasAuthenticator())
            {
                $this->primary->http()->authenticator()->confirmAction("trade");
            }
            else
            {
                throw new TradeException("two factor confirmation needed");
            }
        }

        if($response->getStatusCode() != 200)
        {
            throw new TradeException();
        }

        $this->offerID = $json["tradeofferid"];

        if(is_object($this->secondary))
        {
            $this->tradeID = $this->secondary->trading()
                ->acceptTrade($json["tradeofferid"], $this->secondary->user()->getSteamID64());
        }

        return $json["tradeofferid"];
    }

    /**
     * @param int $appID
     * @param int $contextID
     * @param int $assetID
     * @param int $amount
     */
    public function addMyAsset($appID, $contextID, $assetID, $amount = 1)
    {
        $this->myassets[$appID.":".$contextID.":".$assetID] = [
            "appid" => $appID, "contextid" => $contextID, "amount" => $amount, "assetid" => $assetID
        ];
    }

    /**
     * @param int $appID
     * @param int $contextID
     * @param int $assetID
     * @param int $amount
     */
    public function addHisAsset($appID, $contextID, $assetID, $amount = 1)
    {
        $this->hisassets[$appID.":".$contextID.":".$assetID] = [
            "appid" => $appID, "contextid" => $contextID, "amount" => $amount, "assetid" => $assetID
        ];
    }


    /**
     * @return ItemList
     * @throws TradeException
     * @throws \App\Database\Exception\ParseException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\ClientSessionException
     */
    public function getSecondaryReceivedItems()
    {
        $this->execute();
        if(is_string($this->tradeID))
        {
            return $this->secondary->trading()->getReceivedItems($this->tradeID);
        }
        else
        {
            return new ItemList();
        }
    }

    /**
     * @return ItemList
     * @throws TradeException
     * @throws \App\Database\Exception\ParseException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\ClientSessionException
     */
    public function getPrimaryReceivedItems()
    {
        $this->execute();
        if(is_string($this->tradeID))
        {
            return $this->primary->trading()->getReceivedItems($this->tradeID);
        }
        else
        {
            return new ItemList();
        }
    }

}