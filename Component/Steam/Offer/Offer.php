<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Offer;

use Component\Steam\Exception\TradeException;

class Offer
{
    private $offerID;

    private $bot;
    private $user;

    private $myitems;
    private $hisitems;

    private $state;
    private $holded;
    private $mode;

    private $tradeID;

    /**
     * @param string $offerID
     * @param \Component\Steam\Bot\Bot $bot
     * @param \Component\Steam\User\User $user
     * @param \Component\Steam\Item\ItemList $botitemlist
     * @param \Component\Steam\Item\ItemList $useritemlist
     * @param int $state
     * @param string $mode
     * @param bool $hold
     */
    public function __construct($offerID, $bot, $user, $botitemlist, $useritemlist, $state, $mode = "received", $hold = NULL)
    {
        $this->offerID = (string)$offerID;
        $this->bot = $bot;
        $this->user = $user;
        $this->myitems = $botitemlist;
        $this->hisitems = $useritemlist;
        $this->state = (int)$state;
        $this->mode = $mode;
        $this->holded = $hold;
        $this->tradeID = NULL;
    }

    /**
     * @return string
     * @throws TradeException
     * @throws \App\Database\Exception\ParseException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\ClientSessionException
     */
    public function accept()
    {
        if($this->mode != "received")
        {
            throw new TradeException();
        }

        $this->tradeID = $this->bot->trading()->acceptTrade($this->offerID, $this->user->getSteamID64());

        return $this->tradeID;
    }

    /**
     * @return bool
     * @throws TradeException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\ClientSessionException
     */
    public function decline()
    {
        if($this->mode != "received")
        {
            throw new TradeException();
        }

        return $this->bot->trading()->declineTrade($this->offerID);
    }

    /**
     * @return bool
     * @throws TradeException
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\ClientSessionException
     */
    public function cancel()
    {
        if($this->mode != "sent")
        {
            throw new TradeException();
        }

        return $this->bot->trading()->cancelTrade($this->offerID);
    }

    /**
     * @return \Component\Steam\Item\ItemList
     */
    public function getBotItems()
    {
        return $this->myitems;
    }

    /**
     * @return \Component\Steam\Item\ItemList
     */
    public function getPartnerItems()
    {
        return $this->hisitems;
    }

    /**
     * @return \Component\Steam\User\User
     */
    public function getPartner()
    {
        return $this->user;
    }

    /**
     * @return \Component\Steam\Bot\Bot
     */
    public function getBot()
    {
        return $this->bot;
    }

    /**
     * @return string
     */
    public function getOfferID()
    {
        return $this->offerID;
    }

    /**
     * @return string
     */
    public function getTradeID()
    {
        return $this->tradeID;
    }

    /**
     * @param string $tradeID
     */
    public function setTradeID($tradeID)
    {
        $this->tradeID = $tradeID;
    }

    /**
     * @return bool
     */
    public function is_accepted()
    {
        return $this->state == 3;
    }

    /**
     * @return bool
     */
    public function is_canceld()
    {
        return !$this->is_accepted() && !$this->is_pending();
    }

    /**
     * @return bool
     */
    public function is_pending()
    {
        return $this->state == 2 || $this->state == 11;
    }

    /**
     * @return bool
     * @throws \App\HTTP\Exception\UrlException
     * @throws \Component\Steam\Exception\ClientSessionException
     */
    public function is_holded()
    {
        if(!isset($this->holded))
        {
            $this->holded = $this->bot->trading()->is_holded($this->getOfferID());
        }

        return $this->holded;
    }
}