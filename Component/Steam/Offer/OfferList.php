<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam\Offer;

use App\Tools\ItemList;
use Component\Steam\Exception\OfferNotFoundException;
use Component\Steam\SteamFactory;
use Component\Steam\User\User;

class OfferList extends ItemList
{
    private $steam;

    public function __construct()
    {
        parent::__construct();
        $this->steam = SteamFactory::getSteamCore();
    }

    /**
     * @return array
     */
    public function getOfferIDList()
    {
        $offerIDs = [];
        foreach($this as $user)
        {
            $offerIDs[] = $user->getSteamID64();
        }

        return $offerIDs;
    }

    /**
     * @param $offerID
     *
     * @return Offer
     *
     * @throws OfferNotFoundException
     */
    public function find($offerID)
    {
        foreach($this as $profile)
        {
            if($profile->getOfferID() == $offerID)
            {
                return $profile;
            }
        }

        throw new OfferNotFoundException();
    }

    /**
     * @param string|Offer $offerID
     *
     * @return bool
     */
    public function has($offerID)
    {
        if(is_object($offerID))
        {
            $offerID = $offerID->getOfferID();
        }

        try
        {
            $this->find($offerID);

            return true;
        }
        catch(OfferNotFoundException $e)
        {
            return false;
        }
    }

    /**
     * @param string|User $steamID
     *
     * @return OfferList
     */
    public function filter($steamID)
    {
        if(is_object($steamID))
        {
            $steamID = $steamID->getSteamID64();
        }

        $list = new OfferList();

        foreach($this as $offer)
        {
            if($offer->getPartner()->getSteamID64() == $steamID)
            {
                $list->append($offer);
            }
        }

        return $list;
    }

    public function queueItemPrices()
    {
        foreach($this as $offer)
        {
            $this->steam->item()->queue($offer->getPartnerItems());
            $this->steam->item()->queue($offer->getBotItems());
        }
    }
}