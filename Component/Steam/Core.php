<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam;

use App\Core\App;
use App\Tools\Crypt;
use App\User\User;
use Component\Steam\API\API;
use Component\Steam\Item\PriceHandler;
use Component\Steam\Parser\Parser;

class Core
{
    private $api;
    private $crypt;
    private $item;
    private $parser;

    private $app;
    private $cache;

    private $botIDs;

    public function __construct(App $app)
    {
        $this->app = $app;

        $this->api = new API($this->app->setting()->getComponent("Steam", "apikey"), $this, $this->app->mcache(), $this->app->tcache());
        $this->crypt = $this->app->crypt();
        $this->item = new PriceHandler($this, $this->app);
        $this->parser = new Parser($this);

        $this->botIDs = NULL;
    }

    /**
     * @return API
     */
    public function api()
    {
        return $this->api;
    }

    /**
     * @return Crypt
     */
    public function crypt()
    {
        return $this->crypt;
    }

    /**
     * @return PriceHandler
     */
    public function item()
    {
        return $this->item;
    }

    /**
     * @return Parser
     */
    public function parser()
    {
        return $this->parser;
    }

    /**
     * @return array
     */
    public function getAppIDs()
    {
        $applist = explode(";", $this->app->setting()->get("applist", "component", "Steam"));
        $appIDs = [];

        foreach($applist as $data)
        {
            $split = explode(":", $data);

            if(empty($split[0]))
            {
                continue;
            }

            $appIDs[] = $split[0];
        }

        return $appIDs;
    }

    /**
     * @return array
     */
    public function getInventories()
    {
        $applist = explode(";", $this->app->setting()->get("applist", "component", "Steam"));
        $appIDs = [];

        foreach($applist as $data)
        {
            $split = explode(":", $data);

            if(empty($split[0]))
            {
                continue;
            }

            $appIDs[] = ["appID" => $split[0], "contextID" => $split[1]];
        }

        return $appIDs;
    }

    /**
     * @param string $steamID
     *
     * @return int
     * @throws \App\Database\Exception\ParseException
     */
    public function getUserID($steamID)
    {
        if(!is_numeric($this->cache[$steamID]))
        {
            $this->cache[$steamID] = $this->app->database()->select("user")->where(["steamID" => $steamID])->fetchRow();

            $this->app->tcache()->set($this->cache[$steamID]["id"], $this->cache[$steamID], "user");
        }

        return $this->cache[$steamID]["id"];
    }

    /**
     * @param int $userID
     *
     * @return string
     */
    public function getSteamID($userID)
    {
        $user = new User($userID);

        return $user->getColumn("steamID");
    }

    /**
     * @return int
     */
    public function getRandomBotID()
    {
        if(!is_array($this->botIDs))
        {
            $rows = $this->app->database()->select("steam_bot")
                ->column(["id"])->fetchAssoc();

            $this->botIDs = [];
            foreach($rows as $row)
            {
                $this->botIDs[] = $row["id"];
            }
        }

        return $this->botIDs[rand(0, count($this->botIDs) - 1)];
    }
}