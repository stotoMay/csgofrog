<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam;

use App\Bootstrap\Factory;
use App\Component\Parent\Component;
use App\HTTP\Browser;
use App\Theme\Theme;
use App\Core\App;
use Component\Steam\Bot\Bot;
use Component\Steam\Item\Item;

class Steam extends Component
{
    public static function info()
    {
        return [
            "setting" => ["apikey" => "^[0-9A-F]{32}$", "applist" => "textarea"],
            "language" => ["english" => "Language/English.lang"]
        ];
    }

    public function onEnable()
    {
        $controllers = ["SteamBotConfig", "SteamItemConfig", "SteamAjax", "SteamAuthenticator"];

        foreach($controllers as $controller)
        {
            if(!class_exists('App\\Admin\\Controller\\'.$controller, false))
            {
                include(base_dir("Component/Steam/Admin/Controller/".$controller.".php"));
                $this->app->router()->initController('\\App\\Admin\\Controller\\'.$controller);
            }
        }

        $autoloader = Factory::getAutoloader();
        $autoloader->addClass("LightOpenID", "Component/Steam/Library/LightOpenID.php");
    }

    public function onApplicationBuild()
    {
        $args = [SteamFactory::buildCore($this->app)];
        $this->app->component()->setHook("onSteamLoad", $args);
    }

    public function onApplicationFinish()
    {
        Bot::dispatch();
    }

    /**
     * @param Theme $theme
     *
     * @throws \App\Core\Exception\FolderNotFoundException
     * @throws \App\Core\Exception\InvalidArgumentException
     * @throws \App\Theme\Exception\FolderNotFoundException
     */
    public function onPageload($theme)
    {
        $theme->assignFunction("getSteamSessionUser", function($args, $key = NULL)
        {
            $steam = SteamFactory::getSteamCore();
            /**
             * @var Browser $browser
             */
            $browser = $args[3];
            /**
             * @var App $app
             */
            $app = $args[1];

            if($browser->isLogin() && is_numeric($browser->user()
                    ->getColumn("steamID"))
            )
            {
                if(!$app->tcache()->exists("steam_theme_user"))
                {
                    $u = $steam->api()->user()->getPlayerSummaries($browser->user()
                        ->getColumn("steamID"))[0];
                    $app->tcache()->set("steam_theme_user", $u);
                }
                else
                {
                    $u = $app->tcache()->get("steam_theme_user");
                }

                return is_string($key) ? $u[$key] : $u;
            }

            return [];
        });

        $theme->assignFunction("getSteamApps", function($args)
        {
            $steam = SteamFactory::getSteamCore();
            /**
             * @var App $app
             */
            $app = $args[1];
            if($app->tcache()->exists("steam_theme_app"))
            {
                return $app->tcache()->get("steam_theme_app");
            }

            $result = [];
            $apps = array_clear(explode(";", $this->getSetting("applist")));
            foreach($apps as $a)
            {
                $data = array_clear(explode(":", $a));

                if(count($data) != 2)
                {
                    continue;
                }

                $result[] = [
                    "appID" => $data[0], "contextID" => $data[1],
                    "name" => $steam->api()->steam()->getAppName($data[0]), "id" => $a
                ];

                $app->tcache()->set("steam_theme_apps", $result);
            }

            return $result;
        });

        $theme->assignFunction("renderItem", function($args, $item, $sizex = 73, $sizey = 73, $ahref = true)
        {
            /**
             * @var Theme $theme
             */
            $theme = $args[0];

            if(is_object($item))
            {
                /**
                 * @var Item $item
                 */
                $data = $item->getData();
            }
            else
            {
                $data = $item;
            }

            return $theme->draw("Steam/item.tpl")->assignVar("item", $data)
                ->assignVar("itemsizey", $sizey)
                ->assignVar("itemsizex", $sizex)
                ->assignVar("ahref", $ahref)
                ->parse();
        });

        $theme->registerFolder("Component/Steam/Admin/Template", "Steam/");
    }
}