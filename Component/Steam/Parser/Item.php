<?php
namespace Component\Steam\Parser;

use Component\Steam\Core;
use Component\Steam\Item\ItemAsset;
use Component\Steam\Item\ItemList;

class Item
{
    private $steam;
    private $apps;

    public function __construct(Core $steam)
    {
        $this->steam = $steam;
        $this->apps = [];
        $apps = get_dir_files(base_dir("Component/Steam/Parser/Apps"));
        foreach($apps as $app)
        {
            $classname = 'Component\\Steam\\Parser\\Apps\\'.str_replace(".php", "", $app);

            if(!class_exists($classname))
            {
                continue;
            }

            $class = new $classname($this->steam);

            if(method_exists($class, "getAppID") && is_callable([$class, "getAppID"]))
            {
                $this->apps[$class->getAppID()] = $class;
            }
        }
    }

    public function parseItemData($items, $descr = NULL, $parseItemInfo = true)
    {
        $itemlist = new ItemList();

        if(!is_array($items))
        {
            return $itemlist;
        }

        foreach($items as $item)
        {
            if(!isset($item["classid"]) || !isset($item["instanceid"]))
            {
                continue;
            }

            $assetID = empty($item["assetid"]) ? $item["id"] : $item["assetid"];
            $amount = empty($item["amount"]) ? 1 : $item["amount"];

            if(!is_array($descr) || !is_array($descr[$item["classid"]][$item["instanceid"]]))
            {
                $i = new ItemAsset($item["appid"], $item["contextid"], $assetID, $amount);
                $i->setClassItem($item["classid"], $item["instanceid"]);

                $itemlist->append($i);

                continue;
            }

            $d = $descr[$item["classid"]][$item["instanceid"]];

            if(empty($d["market_hash_name"]))
            {
                $market_name = $d["name"];
            }
            else
            {
                $market_name = $d["market_hash_name"];
            }

            $i = new \Component\Steam\Item\Item($item["appid"], $item["contextid"], $market_name, $assetID, $amount);
            $i->setClassItem($item["classid"], $item["instanceid"]);

            $color = empty($d["name_color"]) ? "#3a3a3a" : "#".$d["name_color"];
            $background = empty($d["background_color"]) ? "#292929" : "#".$d["background_color"];

            $info = [];
            if($parseItemInfo === true)
            {
                $info = $this->parseItemDescription($item["appid"], $d);
            }

            $i->setData([
                "raw_image" => $d["icon_url"], "color" => $color,
                "background" => $background, "data" => $info
            ]);

            $itemlist->append($i);
        }

        return $itemlist;
    }

    public function parseItemDescription($appID, $description)
    {
        if(!is_object($this->apps[$appID]) || !method_exists($this->apps[$appID], "parseItemDescription"))
        {
            return [];
        }

        $data = $this->apps[$appID]->parseItemDescription($description);

        if(!is_array($data))
        {
            return [];
        }

        return $data;
    }
}