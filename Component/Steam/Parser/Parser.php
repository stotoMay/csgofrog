<?php
namespace Component\Steam\Parser;

use Component\Steam\Core;

class Parser
{
    private $steam;
    private $item;

    public function __construct(Core $steam)
    {
        $this->steam = $steam;
        $this->item = new Item($steam);
    }

    public function item()
    {
        return $this->item;
    }
}