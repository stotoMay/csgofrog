<?php
namespace Component\Steam\Parser\Apps;

class CSGO
{
    public function parseItemDescription($description)
    {
        $info = [];

        if(is_array($description["actions"]))
        {
            foreach($description["actions"] as $action)
            {
                if(!isset($action["link"]) || !is_int(strpos($action["link"], "+csgo_econ_action_preview")))
                {
                    continue;
                }

                $split = explode("%assetid%", $action["link"]);

                if(count($split) != 2 || !starts_with($split[1], "D"))
                {
                    continue;
                }

                $info["inspect"] = str_replace("D", "", $split[1]);
            }
        }

        if(is_array($description["descriptions"]))
        {
            foreach($description["descriptions"] as $data)
            {
                if($data["type"] != "html" || !is_int(strpos($data["value"], "sticker_info")))
                {
                    continue;
                }

                $stickerimages = [];

                $rows = explode("<br>", $data["value"]);
                $rawstickerimages = explode("img", $rows[1]);
                preg_match('/Sticker: (.*)<\/center>/', $rows[2], $rawstickernames);

                foreach($rawstickerimages as $image)
                {
                    preg_match('/ src="(.*)">/', $image, $match);

                    if(count($match) == 0)
                    {
                        continue;
                    }

                    $stickerimages[] = $match[1];
                }

                $stickernames = explode(",", $rawstickernames[1]);

                if(count($stickernames) != count($stickerimages))
                {
                    break;
                }

                $sticker = [];
                foreach($stickernames as $key => $name)
                {
                    $sticker[] = ["name" => $name, "image" => $stickerimages[$key]];
                }
                $info["sticker"] = $sticker;
            }
        }

        if(is_array($description["tags"]))
        {
            foreach($description["tags"] as $tag)
            {
                if($tag["category"] == "Rarity")
                {
                    $info["rarity"] = ["name" => $tag["localized_tag_name"], "color" => "#".$tag["color"]];
                }
                elseif($tag["category"] == "Type")
                {
                    $info["type"] = ["internal" => $tag["internal_name"], "name" => $tag["localized_tag_name"]];
                }
                elseif($tag["category"] == "Weapon")
                {
                    $info["waepon"] = ["internal" => $tag["internal_name"], "name" => $tag["localized_tag_name"]];
                }
                elseif($tag["category"] == "ItemSet")
                {
                    $info["collection"] = ["internal" => $tag["internal_name"], "name" => $tag["localized_tag_name"]];
                }
                elseif($tag["category"] == "Quality")
                {
                    $info["quality"] = $tag["internal_name"];
                }
                elseif($tag["category"] == "Exterior")
                {
                    $info["item"] = ["name" => $description["name"], "wear" => $tag["localized_tag_name"]];
                }
            }
        }

        return $info;
    }

    public function getAppID()
    {
        return 730;
    }
}