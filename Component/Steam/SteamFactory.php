<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Steam;

use App\Core\App;

class SteamFactory
{
    private static $core;

    public static function buildCore(App $app)
    {
        self::$core = new Core($app);

        return self::$core;
    }

    /**
     * @return Core
     */
    public static function getSteamCore()
    {
        return self::$core;
    }
}