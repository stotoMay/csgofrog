<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Tracking;

use App\Admin\Controller\Referral;
use App\Component\Parent\Component;
use App\HTTP\Browser;
use App\HTTP\Request;
use App\Theme\Theme;

class Tracking extends Component
{
    const REFERRAL_COOKIE = "ref";
    const REFERRAL_REQUEST = "r";

    public static function info()
    {
        return [
            "language" => ["english" => "Language/English.lang"]
        ];
    }

    public function onEnable()
    {
        $controllers = ["Referral", "DDoS"];
        foreach($controllers as $controller)
        {
            if(!class_exists('App\\Admin\\Controller\\'.$controller, false))
            {
                include(base_dir("Component/Tracking/Admin/Controller/".$controller.".php"));
                $this->app->router()->initController('\\App\\Admin\\Controller\\'.$controller);
            }
        }
    }

    public function onPageload(Theme $theme, Request $request, Browser $browser)
    {
        if($request->has(self::REFERRAL_REQUEST, false, v()->textstring()) && !$request->cookie()->has(self::REFERRAL_COOKIE))
        {
            $request->cookie()->put(self::REFERRAL_COOKIE, $request->input(self::REFERRAL_REQUEST, NULL, false), time() + 3600 * 24 * 7);
        }
    }

    public function onAccountCreation(Request $request, $userID)
    {
        if($request->cookie()->has(self::REFERRAL_COOKIE))
        {
            $tag = $request->cookie()->get(self::REFERRAL_COOKIE);

            if(!is_string($tag))
            {
                return;
            }

            $row = $this->app->database()->select("referral")->where(["tag" => $tag])->fetchRow();

            if(!is_array($row))
            {
                return;
            }

            $this->app->database()->update("user")->set(["referralID" => $row["id"]])->where(["id" => $userID]);
        }
    }

    public function pageUserProfile($args, $user)
    {
        $controller = new Referral($args[2], $args[3], $args[0], $args[4], $args[1]);

        $template = "";
        if(method_exists($controller, "user") && is_callable([$controller, "user"]))
        {
            $template = (string)$controller->user($user["id"]);
        }

        return $template;
    }
}