<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th>User ID</th>
                <th>Group</th>
                <th>Username</th>
                <th>Member since</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            {foreach $users as $user}
                <tr>
                    <td>#{$user.id}</td>
                    <td>{$user.group}</td>
                    <td>{$user.username}</td>
                    <td>{datetimeformat($user.created)}</td>
                    <td><a href="/admin/user/view/{$user.id}" class="btn btn-default">View</a></td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</div>