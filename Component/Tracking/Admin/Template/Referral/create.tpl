<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>Tracking {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default">
    <div class="panel-heading">
        Tracking
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <form method="post" action="/admin/referral/create">
                <table class="table table-borderless">
                    <tr>
                        <td>Name (Just visible in the admin panel)</td>
                        <td><input type="text" name="name" class="form-control"></td>
                    </tr>

                    <tr>
                        <td>Url</td>
                        <td>
                            <div class="input-group">
                                <span class="input-group-addon">{$refurl}</span>
                                <input type="text" name="tag" class="form-control" value="{$reftag}">
                            </div>
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="token" value="{token()}">
                <input type="submit" value="Create" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>