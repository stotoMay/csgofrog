<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>Tracking {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default">
    <div class="panel-heading">
        Referral
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <form method="post" action="/admin/referral/delete">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Url</th>
                        <th>Owner</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach $referrals.results as $referral}
                        <tr>
                            <td>{$referral.name}</td>
                            <td>{$refurl}{$referral.tag}</td>
                            <td>
                                {if $referral.userID}
                                    <a href="/admin/user/view/{$referral.userID}">{$referral.username}</a>
                                {/if}
                            </td>
                            <td><a href="/admin/referral/stats/{$referral.id}" class="btn btn-default">Statistics</a></td>
                            <td><button name="referralID" value="{$referral.id}" class="btn btn-primary">Delete</button></td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>

                <input type="hidden" name="token" value="{token()}">
            </form>

            <div class="center-block">
                {pagination($referrals.total, $referrals.current, $pageurl)}
            </div>

        </div>

        <a href="/admin/referral/create" class="btn btn-default">Create Referral Link</a>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>