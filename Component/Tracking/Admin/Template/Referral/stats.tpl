<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>Tracking {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ul class="breadcrumb">
    <li><a href="/admin/referral">Referrals</a></li>
    <li class="active">{$refurl}</li>
</ul>

{if is_array($referral_user)}
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="center-block">
                This referral link is owned by <a href="/admin/user/view/{$referral_user.id}">{$referral_user.username}</a>
            </div>
        </div>
    </div>
{/if}

<div class="panel panel-default">
    <div class="panel-heading">Tracked Users</div>
    <div class="panel-body">
        {pagination_ajax(string("/admin/referral/ajaxusers/", $referralID))}
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">New Users</div>
    <div class="panel-body">
        {statistics(string("/admin/referral/newuserstats/", $referralID))}
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Activity</div>
    <div class="panel-body">
        {statistics(string("/admin/referral/activitystats/", $referralID))}
    </div>
</div>

{sethook("pageReferralStats", $referralID)}

{includetemplate="footer.tpl"}
</body>
</html>