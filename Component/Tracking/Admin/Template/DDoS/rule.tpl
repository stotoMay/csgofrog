<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>DDoS Rule {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ul class="breadcrumb">
    <li><a href="/admin/ddos">DDoS</a></li>
    <li class="active">DDoS Rule</li>
</ul>

<div class="panel panel-default">
    <div class="panel-heading">
        DDoS Rule
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <form method="post" action="{if $ddosID}/admin/ddos/edit/{$ddosID}{else}/admin/ddos/create{/if}">
                <table class="table table-borderless">
                    <tr>
                        <td>Path</td>
                        <td><input type="text" name="path" value="{$protection.path}" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Priority</td>
                        <td><input type="number" name="priority" value="{$protection.priority}" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Group</td>
                        <td>
                            <select name="session" class="form-control">
                                <option value="0" {if $protection.session == 0}selected{/if}>Every User</option>
                                <option value="1" {if $protection.session == 1}selected{/if}>Logged In Users</option>
                                <option value="2" {if $protection.session == 2}selected{/if}>Users without a session</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Request Limit (0 = No Limit)</td>
                        <td><input type="number" name="limit" value="{$protection.limit}" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Request Interval (in seconds)</td>
                        <td><input type="number" name="interval" value="{$protection.interval}" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Ban time when the limit is exceeded in the interval (0 = forever)</td>
                        <td><input type="number" name="bantime" value="{$protection.bantime}" class="form-control"></td>
                    </tr>
                </table>
                <input type="hidden" name="token" value="{token()}">
                <input type="submit" value="Submit" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>