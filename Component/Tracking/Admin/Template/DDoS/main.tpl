<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>DDoS Protection {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default">
    <div class="panel-heading">
        DDoS Protection
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <form method="post" action="/admin/ddos/delete">
                <table class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Path</th>
                        <th>Session Needed</th>
                        <th>Priority</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach $protections as $protection}
                        <tr>
                            <td>#{$protection.id}</td>
                            <td>{$protection.path}</td>
                            <td>
                                {if $protection.session == "2"}
                                    Users without a session
                                {elseif $protection.session == "1"}
                                    Logged In User
                                {else}
                                    Every User
                                {/if}
                            </td>
                            <td>{$protection.priority}</td>
                            <td><a href="/admin/ddos/edit/{$protection.id}" class="btn btn-default">Edit</a></td>
                            <td><button name="ddosID" value="{$protection.id}" class="btn btn-primary">Delete</button></td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                <input type="hidden" name="token" value="{token()}">
            </form>
        </div>

        <a href="/admin/ddos/create" class="btn btn-default">Create Protection Rule</a>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>