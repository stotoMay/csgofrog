<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Admin\Tools\AjaxStatistics;
use App\Router\Controller;
use App\Validator\Exception\ValidateException;
use App\Validator\Validator;
use Component\Tracking\Tracking;

class Referral extends Controller
{
    /**
     * @var \Component\Tracking\Admin\Model\Referral
     */
    private $model;
    private $admin;

    public static function __info__()
    {
        return [
            "uniqname" => "admin_referral", "tracking" => true, "permission" => ["view" => "bool", "create" => "bool"], "priority" => 4,
            "icon" => "share-alt", "requirement" => [["application", "general", "admin_control_panel"], ["controller", "admin_referral", "view"]]
        ];
    }

    public function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("Component/Tracking/Admin/Model/Referral");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));

        $this->theme->registerFolder("Component/Tracking/Admin/Template", "Admin/Tracking/");
    }

    public function main()
    {
        $template = $this->theme->draw("Admin/Tracking/Referral/main.tpl");

        $page = $this->request->input("page", 1, false, v()->integer()->min(1));

        $template->assignVar("referrals", $this->model->getReferrals($page, 50));
        $template->assignVar("refurl", $this->request->domain()."?".Tracking::REFERRAL_REQUEST."=");
        $template->assignVar("pageurl", "/admin/referral/?page=%p");

        return response($template, $this->request);
    }

    public function create()
    {
        if($this->getPermission("create") != 1)
        {
            redirect("/admin/referral", $this->request)->error($this->language->getMessage("permission"));
        }

        if($this->request->has("name", true, v()->textstring()) && $this->request->has("tag", true, v()->textstring()))
        {
            $validator = new Validator();

            $validator->textstring()->alnum();

            try
            {
                $validator->validate($this->request->input("name"));
                $validator->validate($this->request->input("tag"));

                if($this->model->tagExists($this->request->input("tag")))
                {
                    redirect("/admin/referral/create", $this->request)->error($this->language->get(["admin_referral", "message", "tag_exists"]));
                }

                $refID = $this->model->createReferral($this->request->input("name"), $this->request->input("tag"));

                redirect("/admin/referral/stats/".$refID, $this->request)->info($this->language->get(["admin_referral", "message", "referral_created"]));
            }
            catch(ValidateException $e)
            {
                $this->printError($e->getFullMessage($this->theme->draw("validator.tpl"), $this->language));
            }
        }

        $template = $this->theme->draw("Admin/Tracking/Referral/create.tpl");

        $template->assignVar("reftag", str_random(5));
        $template->assignVar("refurl", $this->request->domain()."?".Tracking::REFERRAL_REQUEST."=");

        return response($template, $this->request);
    }

    public function stats($referralID)
    {
        $referral = $this->model->getReferral($referralID);

        $template = $this->theme->draw("Admin/Tracking/Referral/stats.tpl");

        $template->assignVar("referralID", $referralID);
        $template->assignVar("referral", $referral);
        $template->assignVar("refurl", $this->request->domain()."?".Tracking::REFERRAL_REQUEST."=".$referral["tag"]);

        $template->assignVar("referral_user", $this->model->getReferralUser($referralID));

        return response($template, $this->request);
    }

    public function delete()
    {
        if($this->getPermission("create") != 1)
        {
            redirect("/admin/referral", $this->request)->error($this->language->getMessage("permission"));
        }

        if(!$this->request->has("referralID", true, v()->integer()))
        {
            redirect("/admin/referral", $this->request)->error($this->language->getMessage("standard_error"));
        }

        $this->model->deleteReferral($this->request->input("referralID"));

        redirect("/admin/referral/", $this->request)->info($this->language->get(["admin_referral", "message", "referral_deleted"]));
    }

    public function user($userID)
    {
        if($this->getPermission("view") != 1)
        {
            return "";
        }

        $referral = $this->model->getReferralByUser($userID);

        if(empty($referral))
        {
            return "";
        }

        $template = $this->theme->draw("Admin/Tracking/Referral/user.tpl");

        $template->assignVar("referral", $referral);

        return $template->parse();
    }

    public function ajaxusers($referralID)
    {
        if($this->browser->user()->permission()->get("trades", "controller", "steamgamble") == 1)
        {
            $this->request->session()->closeSessionWrite();

            $page = $this->request->input("page", 1, false, v()->integer());
            $users = $this->model->getReferralUsers($referralID, $page, 10);

            $template = $this->theme->draw("Admin/Tracking/Referral/users.tpl");

            $template->assignVar("referralID", $referralID);
            $template->assignVar("users", $users["results"]);

            $json = ["success" => true, "content" => $template->parse(), "current" => $page, "total" => $users["total"]];
        }
        else
        {
            $json = ["success" => false];
        }

        return response(json_encode($json, JSON_HEX_QUOT | JSON_HEX_TAG), $this->request)->contentType("text/json");
    }

    public function newuserstats($referralID)
    {
        $statistic = new AjaxStatistics($this->request, 0);

        $this->request->session()->closeSessionWrite();
        $this->model->getNewUserStats($referralID, $statistic);

        return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
    }

    public function activitystats($referralID)
    {
        $statistic = new AjaxStatistics($this->request, 0);

        $this->request->session()->closeSessionWrite();
        $this->model->getActivityStats($referralID, $statistic);

        return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
    }
}