<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Router\Controller;
use App\Validator\Exception\MultipleValidateException;
use App\Validator\MultipleValidation;

class DDoS extends Controller
{
    /**
     * @var \Component\Tracking\Admin\Model\DDoS
     */
    private $model;
    private $admin;

    public static function __info__()
    {
        return [
            "uniqname" => "admin_ddos", "tracking" => true,
            "permission" => ["view" => "bool", "change" => "bool"] , "priority" => 4,
            "icon" => "shield", "requirement" => [["application", "general", "admin_control_panel"], ["controller", "admin_ddos", "view"]]
        ];
    }

    public function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("Component/Tracking/Admin/Model/DDoS");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));

        $this->theme->registerFolder("Component/Tracking/Admin/Template", "Admin/Tracking/");
    }

    public function main()
    {
        $template = $this->theme->draw("Admin/Tracking/DDoS/main.tpl");

        $template->assignVar("protections", $this->model->getDDosRules());

        return response($template, $this->request);
    }

    public function create()
    {
        if($this->getPermission("change") != 1)
        {
            redirect("/admin/ddos", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("Admin/Tracking/DDoS/rule.tpl");

        if(count($this->request->all()) > 2)
        {
            $path = $this->request->input("path", "/");
            $priority = $this->request->input("priority", 0);
            $session = $this->request->input("session", 0);
            $limit = $this->request->input("limit", 0);
            $interval = $this->request->input("interval", 0);
            $bantime = $this->request->input("bantime", 0);

            $validator = new MultipleValidation();

            $validator->append(v()->textstring()->notEmpty(), $path)
                ->append(v()->integer()->min(1)->max(100), $priority)
                ->append(v()->integer()->min(0)->max(2), $session)
                ->append(v()->integer()->min(0), $limit)
                ->append(v()->integer()->min(0), $interval)
                ->append(v()->integer()->min(0), $bantime);

            try
            {
                $validator->assert();

                $this->model->createDDosRule($path, $priority, $session, $limit, $interval, $bantime);

                redirect("/admin/ddos", $this->request)->info($this->language->get(["admin_ddos", "message", "rule_created"]));
            }
            catch(MultipleValidateException $e)
            {
                $this->printError($e->getFullMessages($this->theme->draw("validator.tpl"), $this->language));
            }
        }

        return response($template, $this->request);
    }

    public function edit($ddosID)
    {
        if($this->getPermission("change") != 1)
        {
            redirect("/admin/ddos", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("Admin/Tracking/DDoS/rule.tpl");

        if(count($this->request->all()) > 2)
        {
            $path = $this->request->input("path", "/");
            $priority = $this->request->input("priority", 0);
            $session = $this->request->input("session", 0);
            $limit = $this->request->input("limit", 0);
            $interval = $this->request->input("interval", 0);
            $bantime = $this->request->input("bantime", 0);

            $validator = new MultipleValidation();

            $validator->append(v()->textstring()->notEmpty(), $path)
                ->append(v()->integer()->min(1)->max(100), $priority)
                ->append(v()->integer()->min(0)->max(2), $session)
                ->append(v()->integer()->min(0), $limit)
                ->append(v()->integer()->min(0), $interval)
                ->append(v()->integer()->min(0), $bantime);

            try
            {
                $validator->assert();

                $this->model->updateDDosRule($ddosID, $path, $priority, $session, $limit, $interval, $bantime);

                redirect("/admin/ddos", $this->request)->info($this->language->get(["admin_ddos", "message", "rule_updated"]));
            }
            catch(MultipleValidateException $e)
            {
                $this->printError($e->getFullMessages($this->theme->draw("validator.tpl"), $this->language));
            }
        }

        $template->assignVar("protection", $this->model->getDDosRule($ddosID));
        $template->assignVar("ddosID", $ddosID);

        return response($template, $this->request);
    }

    public function delete()
    {
        if($this->getPermission("change") != 1)
        {
            redirect("/admin/ddos", $this->request)->error($this->language->getMessage("permission"));
        }

        if($this->request->has("ddosID", true, v()->integer()) && $this->model->ruleExists($this->request->input("ddosID")))
        {
            $this->model->deleteDDosRule($this->request->input("ddosID"));

            redirect("/admin/ddos", $this->request)->info($this->language->get(["admin_ddos", "message", "rule_deleted"]));
        }

        redirect("/admin/ddos", $this->request)->error($this->language->getMessage("standard_error"));
    }
}