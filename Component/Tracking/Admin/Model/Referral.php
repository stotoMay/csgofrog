<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Tracking\Admin\Model;

use App\Admin\Tools\AjaxStatistics;
use App\Router\Model;

class Referral extends Model
{
    public function getReferral($referralID)
    {
        return $this->database->select("referral")->where(["id" => $referralID])->fetchRow();
    }

    public function getReferrals($page = 1, $size = 50)
    {
        $total = $this->database->select("referral")->numRows();

        $result = $this->database->select("referral")
            ->column([
                "referral.id", "referral.name", "referral.tag", "referral.userID", "user.username", "referral.dateline"
            ])
            ->leftJoin("user", ["referral.userID[=]user.id"])
            ->limit(($page - 1)*$size, $size)->fetchAssoc();

        return ["results" => $result, "current" => $page, "total" => ceil($total / $size)];
    }

    public function createReferral($name, $urltag)
    {
        $referralID = $this->database->insert("referral")->set([
            "name" => $name, "tag" => $urltag, "dateline" => time()
        ])->getID();

        return $referralID;
    }

    public function tagExists($tag)
    {
        return $this->database->select("referral")->where(["tag" => $tag])->exists();
    }

    public function deleteReferral($referralID)
    {
        $this->database->delete("referral")->where(["id" => $referralID]);
    }

    public function getReferralUser($referralID)
    {
        $referral = $this->database->select("referral")->where(["id" => $referralID])->fetchRow();

        if(is_array($referral))
        {
            $user = $this->database->select("user")->where(["id" => $referral["userID"]])->fetchRow();

            return $user;
        }

        return NULL;
    }

    public function getReferralByUser($userID)
    {
        return $this->database->select("referral")->where(["userID" => $userID])->fetchRow();
    }

    public function getActivityStats($referralID, AjaxStatistics $statistic)
    {
        $this->database->query("SET time_zone = '".$statistic->getTimezone()."'");

        $userIDs = [];
        $users = $this->database->select("user")->column(["id"])->where(["referralID" => $referralID])->fetchAssoc();
        foreach($users as $user)
        {
            $userIDs[] = $user["id"];
        }

        if(count($userIDs) == 0)
        {
            $statistic->addGraphData([], $this->language->get(["admin_referral", "label_activity"]));

            return;
        }

        $innerquery = $this->database->select("activity")
            ->column([["str" => "DATE_FORMAT(FROM_UNIXTIME(dateline), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]])
            ->where(["userID" => $userIDs])->group(["userID", "date"]);

        $data = $this->database->select($innerquery, "tb")
            ->column(["date", "*" => ["function" => "count", "alias" => "value"]])
            ->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->language->get(["admin_referral", "label_activity"]));
    }

    public function getNewUserStats($referralID, AjaxStatistics $statistic)
    {
        $this->database->query("SET time_zone = '".$statistic->getTimezone()."'");

        $data = $this->database->select("user")->column([
            ["str" => "COUNT(*)", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(created), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where(["created[>=]" => $statistic->getStartTime(), "created[<=]" => $statistic->getStopTime(), "referralID" => $referralID])
            ->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->language->get(["admin_referral", "label_newusers"]));
    }

    public function getReferralUsers($referralID, $page = 1, $size = 10)
    {
        $total = $this->database->select("user")->where(["referralID" => $referralID])->numRows();

        $users = $this->database->select("user")
            ->column(["user.id", "username", "group.name" => "group", "user.created"])
            ->leftJoin("group", ["group.id[=]user.groupID"])
            ->where(["referralID" => $referralID])
            ->order("id", "desc")
            ->limit(($page - 1) * $size, $size)->fetchAssoc();

        return ["results" => $users, "total" => ceil($total / $size), "current" => $page];
    }
}