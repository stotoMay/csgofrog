<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\Tracking\Admin\Model;

use App\Router\Model;

class DDoS extends Model
{
    public function getDDosRules()
    {
        return $this->database->select("ddos")->fetchAssoc();
    }

    public function getDDosRule($ddosID)
    {
        return $this->database->select("ddos")->where(["id" => $ddosID])->fetchRow();
    }

    public function createDDosRule($path, $priority, $session, $limit, $interval, $bantime)
    {
        return $this->database->insert("ddos")->set([
            "path" => $path, "priority" => $priority, "session" => $session,
            "limit" => $limit, "interval" => $interval, "bantime" => $bantime
        ])->getID();
    }

    public function updateDDosRule($ddosID, $path, $priority, $session, $limit, $interval, $bantime)
    {
        $this->database->update("ddos")->set([
            "path" => $path, "priority" => $priority, "session" => $session,
            "limit" => $limit, "interval" => $interval, "bantime" => $bantime
        ])->where(["id" => $ddosID]);
    }

    public function deleteDDosRule($ddosID)
    {
        $this->database->delete("ddos")->where(["id" => $ddosID]);
    }

    public function ruleExists($ddosID)
    {
        return $this->database->select("ddos")->where(["id" => $ddosID])->exists();
    }
}