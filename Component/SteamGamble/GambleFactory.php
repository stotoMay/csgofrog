<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble;

use Component\SteamGamble\Classes\Core;

class GambleFactory
{
    private static $core;

    public static function buildCore($app, $steam, $web)
    {
        self::$core = new Core($app, $steam, $web);
    }

    /**
     * @return Core
     */
    public static function getGambleCore()
    {
        return self::$core;
    }
}