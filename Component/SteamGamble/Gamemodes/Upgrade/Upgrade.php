<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Gamemodes\Upgrade;

use App\Admin\Tools\AjaxStatistics;
use Component\Steam\Item\ItemList;
use Component\SteamGamble\Parent\Gamemode;

class Upgrade extends Gamemode
{
    public function init()
    {
        if(!class_exists('App\\Admin\\Controller\\Upgrade', false))
        {
            include(base_dir("Component/SteamGamble/Gamemodes/Upgrade/Admin/Upgrade.php"));
            $this->app->router()->initController('\\App\\Admin\\Controller\\Upgrade');
        }

        $this->app->language()->mergeFile(__DIR__."/Language/English.lang");
    }

    /**
     * @param ItemList $items
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function onBotInventoryLoad($items)
    {
        $assetIDs = [];
        foreach($items as $item)
        {
            $assetIDs[] = $item->getAssetID();
        }

        $upgradeitems = $this->gamble->app()->database()->select("upgrade_storage")
            ->column(["appID", "contextID", "assetID"])
            ->where(["assetID" => $assetIDs, "status" => 0])->fetchAssoc();

        foreach($upgradeitems as $item)
        {
            $items->removeAsset($item["appID"], $item["contextID"], $item["assetID"]);
        }
    }

    /**
     * @param int $botID
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function onSteamBotDelete($botID)
    {
        // TODO ?
    }

    public function totalprofit()
    {
        $payout = $this->gamble->app()->database()->select("upgrade")
            ->column(["tickets" => ["function" => "sum", "alias" => "value"]])
            ->where(["won" => 1])->fetchRow();

        $deposited = $this->gamble->app()->database()->select("upgrade")
            ->column(["tickets_deposited" => ["function" => "sum", "alias" => "value"]])
            ->fetchRow();

        return (intval($deposited["value"]) - intval($payout["value"])) / 100;
    }

    public function todayprofit()
    {
        $payout = $this->gamble->app()->database()->select("upgrade")
            ->column(["tickets" => ["function" => "sum", "alias" => "value"]])
            ->where(["won" => 1, "dateline[>=]" => time() - 3600 * 24])->fetchRow();

        $deposited = $this->gamble->app()->database()->select("upgrade")
            ->column(["tickets_deposited" => ["function" => "sum", "alias" => "value"]])
            ->where(["dateline[>=]" => time() - 3600 * 24])
            ->fetchRow();

        return (intval($deposited["value"]) - intval($payout["value"])) / 100;
    }

    public function userprofitgraph($userID)
    {
        /**
         * @var Statistic $statistic
         */
        $statistic = $this->gamble->statistic()->gamemode("upgrade");

        return $statistic->getUserProfitGraph($userID);
    }

    public function userluckygraph($userID)
    {
        /**
         * @var Statistic $statistic
         */
        $statistic = $this->gamble->statistic()->gamemode("upgrade");

        return $statistic->getUserLuckyGraph($userID);
    }

    public function profitgraph(AjaxStatistics $statistic)
    {
        $data = [];

        $deposited = $this->app->database()->select("upgrade")->column([
            ["str" => "SUM(tickets_deposited) / 100", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(dateline), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where(["dateline[>=]" => $statistic->getStartTime(), "dateline[<=]" => $statistic->getStopTime()])
            ->group("date")->order("date")->fetchAssoc();

        foreach($deposited as $deposit) {
            $data[$deposit["date"]] = $deposit["value"];
        }

        $payouts = $this->app->database()->select("upgrade")->column([
            ["str" => "SUM(tickets) / 100", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(dateline), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where(["dateline[>=]" => $statistic->getStartTime(), "dateline[<=]" => $statistic->getStopTime(), "won" => 1])
            ->group("date")->order("date")->fetchAssoc();

        foreach($payouts as $payout) {
            if(empty($data[$payout["date"]])) {
                $data[$payout["date"]] = 0;
            }

            $data[$payout["date"]] -= $payout["value"];
        }

        $graph = [];
        foreach($data as $date => $value) {
            $graph[] = ["date" => $date, "value" => $value];
        }

        $statistic->addGraphData($graph, $this->app->language()->get(["steamgamble", "upgrade", "label_profit"]));

        return $graph;
    }

    public function profitofusergraph(AjaxStatistics $statistic, $userIDs)
    {
        $data = [];

        $deposited = $this->app->database()->select("upgrade")->column([
            ["str" => "SUM(tickets_deposited) / 100", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(dateline), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where(["dateline[>=]" => $statistic->getStartTime(), "dateline[<=]" => $statistic->getStopTime(), "userID" => $userIDs])
            ->group("date")->order("date")->fetchAssoc();

        foreach($deposited as $deposit) {
            $data[$deposit["date"]] = $deposit["value"];
        }

        $payouts = $this->app->database()->select("upgrade")->column([
            ["str" => "SUM(tickets) / 100", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(dateline), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where(["dateline[>=]" => $statistic->getStartTime(), "dateline[<=]" => $statistic->getStopTime(), "won" => 1, "userID" => $userIDs])
            ->group("date")->order("date")->fetchAssoc();

        foreach($payouts as $payout) {
            if(empty($data[$payout["date"]])) {
                $data[$payout["date"]] = 0;
            }

            $data[$payout["date"]] -= $payout["value"];
        }

        $graph = [];
        foreach($data as $date => $value) {
            $graph[] = ["date" => $date, "value" => $value];
        }

        $statistic->addGraphData($graph, $this->app->language()->get(["steamgamble", "upgrade", "label_profit"]));

        return $data;
    }

    public function pageUserProfile($args, $userID)
    {
        $controller = new \App\Admin\Controller\Upgrade($args[2], $args[3], $args[0], $args[4], $args[1]);

        $template = "";
        if(method_exists($controller, "user") && is_callable([$controller, "user"]))
        {
            $template = (string)$controller->user($userID);
        }

        return $template;
    }
}