<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Gamemodes\Upgrade\Admin;

use App\Admin\Tools\AjaxStatistics;
use Component\Steam\Core;
use Component\SteamGamble\Gamemodes\Upgrade\Statistic;

class Model extends \App\Router\Model
{
    public function markItemAsRefunded($tradeID) {
        return $this->database->update("upgrade_deposit")
                ->set(["status" => 6])->where(["status" => 5, "id" => $tradeID])
                ->getAffectedRows() > 0;
    }

    public function getSearchResults(Core $steam, $match = NULL, $page = 1, $size = 50)
    {
        $query = $this->database->select("steam_market")->column([
            "steam_market.id" => "id", "appID", "market_name", "market_volume", "market_price",
            "name", "color", "background", "image", "steam_price.price" => "user_price"
        ])->leftJoin("steam_price", ["steam_market.id[=]steam_price.itemID"]);

        $condition = [];
        if(isset($match))
        {
            $condition["steam_market.name[~~]"] = "%".$match."%";
        }

        if(count($condition) > 0)
        {
            $query->where($condition);
        }

        $query->order("name", "asc");

        $pagecount = ceil($query->numRows() / $size);
        $results = $query->limit(($page - 1) * $size, $size)->fetchAssoc();

        foreach($results as $key => $item)
        {
            $results[$key]["image"] = $item["image"];
            $results[$key]["price"] = !isset($item["user_price"]) ? $item["market_price"] / 100 : $item["user_price"] / 100;
            $results[$key]["appname"] = $steam->api()->steam()->getAppName($item["appID"]);

        }

        return ["results" => $results, "pages" => $pagecount, "current" => $page];
    }

    public function getUpgradeMarket(Core $steam) {
        $items = $this->database->select("upgrade_market")->fetchAssoc();
        $steam->item()->filling($items);

        return $items;
    }

    public function deleteUpgradeMarketItem($marketID) {
        $this->database->delete("upgrade_market")->where(["id" => $marketID]);
    }

    public function enableUpgradeMarketItem($marketID) {
        $this->database->update("upgrade_market")->set(["disabled" => 0])->where(["id" => $marketID]);
    }

    public function upgradeMarketItemExists($appID, $contextID, $market_name) {
        return $this->database->select("upgrade_market")->where(["appID" => $appID, "contextID" => $contextID, "market_name" => $market_name])->exists();
    }

    public function createUpgradeMarketItem($marketID) {
        $item = $this->database->select("steam_market")->where(["id" => $marketID])->fetchRow();

        if(!is_array($item)) {
            return false;
        }

        $appID = $item["appID"];
        $contextID = 1;
        $market_name = $item["market_name"];

        if($this->upgradeMarketItemExists($appID, $contextID, $market_name)) {
            return false;
        }

        $this->database->insert("upgrade_market")->set([
            "appID" => $appID, "contextID" => $contextID, "market_name" => $market_name, "disabled" => 0
        ]);

        return true;
    }

    public function upgradeExists($upgradeID)
    {
        return $this->database->select("upgrade")->where(["id" => $upgradeID])->exists();
    }

    public function activeUpgradeExists($upgradeID)
    {
        return $this->database->select("upgrade")->where(["id" => $upgradeID])->exists();
    }

    public function getUpgradeHistory(Statistic $statistic, $page, $size = 50, $userID = NULL)
    {
        $query = $this->database->select("upgrade");

        if(is_numeric($userID))
        {
            $query->where(["userID" => $userID]);
        }

        $count = $query->numRows();
        $result = $statistic->getUpgradeHistory($page, $size, $userID);

        return ["results" => $result, "total" => ceil($count / $size), "current" => $page];
    }

    public function getUpgradeWithdrawHistory(Statistic $statistic, $page, $size = 50, $userID = NULL)
    {
        $query = $this->database->select("upgrade_deposit");

        if(is_numeric($userID))
        {
            $query->where(["userID" => $userID, "status" => [1,2,3,5,6]]);
        }

        $count = $query->numRows();
        $result = $statistic->getWithdrawHistory($userID, $page, $size);

        return ["results" => $result, "total" => ceil($count / $size), "current" => $page];
    }

    public function getGameCountStats(AjaxStatistics $statistic)
    {
        $this->database->query("SET time_zone = '".$statistic->getTimezone()."'");

        $data = $this->database->select("upgrade")->column([
            "*" => ["function" => "count", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(dateline), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where(["dateline[>=]" => $statistic->getStartTime(), "dateline[<=]" => $statistic->getStopTime()])
            ->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->language->get(["steamgamble", "upgrade", "label_gamecount"]));
    }

    public function getGameSizeStats(AjaxStatistics $statistic)
    {
        $this->database->query("SET time_zone = '".$statistic->getTimezone()."'");

        $data = $this->database->select("upgrade")->column([
            ["str" => "(SUM(tickets_deposited) / COUNT(*)) / 100", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(dateline), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where(["dateline[>=]" => $statistic->getStartTime(), "dateline[<=]" => $statistic->getStopTime()])
            ->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->language->get(["steamgamble", "upgrade", "label_gamesize"]));
    }
}