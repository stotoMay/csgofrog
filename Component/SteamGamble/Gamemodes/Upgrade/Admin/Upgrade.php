<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Admin\Tools\AjaxStatistics;
use App\Router\Controller;
use Component\Steam\SteamFactory;
use Component\SteamGamble\Classes\Core;
use Component\SteamGamble\GambleFactory;
use Component\SteamGamble\Gamemodes\Upgrade\Admin\Model;
use Component\SteamGamble\Gamemodes\Upgrade\Statistic;

class Upgrade extends Controller
{
    private $admin;
    /**
     * @var Model
     */
    private $model;
    /**
     * @var Statistic
     */
    private $statistic;
    /**
     * @var \Component\Steam\Core $steam
     */
    private $steam;
    /**
     * @var Core $gamble
     */
    private $gamble;

    public static function __info__()
    {
        return [
            "uniqname" => "upgrade", "setting" => [
                "min_deposit_value" => "^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$", "max_deposit_items" => "int",
                "min_item_value" => "^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$", "fees" => "^(?:100|[1-9]?[0-9])$",
            ], "permission" => ["view" => "bool", "stats" => "bool", "percentage" => "bool"], "icon" => "arrow-up", "tracking" => true,
            "requirement" => [["application", "general", "admin_control_panel"], ["controller", "upgrade", "view"]], "priority" => 5,
        ];
    }

    public function __init__()
    {
        $this->model = $this->getModel("Component/SteamGamble/Gamemodes/Upgrade/Admin/Model");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);

        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));
        $this->theme->registerFolder("Component/SteamGamble/Gamemodes/Upgrade/Admin/Template", "Admin/Upgrade/");

        $this->authAdmin();

        $this->steam = SteamFactory::getSteamCore();
        $this->gamble = GambleFactory::getGambleCore();

        $this->statistic = $this->gamble->statistic()->gamemode("upgrade");
    }

    public function main() {
        $template = $this->theme->draw("Admin/Upgrade/main.tpl");

        if($this->request->has("marketID", true, v()->integer())) {
            if($this->model->createUpgradeMarketItem($this->request->input("marketID")))
            {
                $this->printInfo("You successfully added the item to the upgrade market");
            }
            else
            {
                $this->printError("There was an error while adding the item to the upgrade market");
            }
        }

        $match = $this->request->input("search", NULL, false);
        $page = $this->request->input("page", 1, false, v()->integer());

        if(isset($match))
        {
            $match = rawurldecode($match);
        }

        $items = $this->model->getSearchResults($this->steam, $match, $page, 50);

        if(count($items["results"]) == 0 && !empty($match))
        {
            $this->printError($this->language->get(["steam", "items", "message", "no_items_found"]));
        }

        $template->assignVar("match", $match);
        $template->assignVar("pageurl", "/admin/upgrade/?search=".rawurlencode($match)."&page=%p");
        $template->assignVar("items", $items);

        return response($template, $this->request);
    }

    public function manualrefunded() {
        if($this->request->has("tradeID", true, v()->integer())) {
            $tradeID = $this->request->input("tradeID");
            $userID = $this->request->input("userID");

            $this->model->markItemAsRefunded($tradeID);

            redirect("/admin/user/view/".$userID, $this->request)->info("You successfully marked the item as refunded");
        }
        else {
            redirect("/admin/upgrade", $this->request)->error($this->language->getMessage("standard_error"));
        }
    }

    public function market() {
        $template = $this->theme->draw("Admin/Upgrade/market.tpl");

        if($this->request->has("action", true, v()->textstring()) && $this->request->has("marketID", true, v()->integer())) {
            $action = $this->request->input("action");
            $upgradeMarketID = $this->request->input("marketID");

            if($action == "delete") {
                $this->model->deleteUpgradeMarketItem($upgradeMarketID);

                $this->printInfo("You successfully deleted the item from the market");
            }
            elseif($action == "enable") {
                $this->model->enableUpgradeMarketItem($upgradeMarketID);

                $this->printInfo("You enabled the item again");
            }
        }

        $template->assignVar("marketitems", $this->model->getUpgradeMarket($this->steam));

        return response($template, $this->request);
    }

    public function history()
    {
        $template = $this->theme->draw("Admin/Upgrade/history.tpl");

        if($this->request->has("upgradeID", true, v()->integer()))
        {
            $upgradeID = $this->request->input("upgradeID");

            if($this->model->upgradeExists($upgradeID))
            {
                redirect("/admin/upgrade/game/".$upgradeID, $this->request);
            }
            else
            {
                $this->printError($this->language->get(["upgrade", "upgrade_not_found"]));
            }
        }

        $page = $this->request->input("page", 1, false, v()->integer()->min(1));

        $upgrades = $this->model->getUpgradeHistory($this->statistic, $page);

        $template->assignVar("upgrades", $upgrades);
        $template->assignVar("pageurl", "/admin/upgrade/history?page=%p");

        return response($template, $this->request);
    }

    public function stats()
    {
        if($this->getPermission("stats") != 1)
        {
            redirect("/admin/upgrade", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("Admin/Upgrade/stats.tpl");

        return response($template, $this->request);
    }

    public function game($upgradeID)
    {
        $template = $this->theme->draw("Admin/Upgrade/upgrade.tpl");

        if(!$this->model->upgradeExists($upgradeID))
        {
            redirect("/admin/upgrade/history", $this->request)->error($this->language->get(["upgrade",
                "upgrade_not_found"]));
        }

        $upgrade = $this->statistic->getUpgrade($upgradeID);

        $template->assignVar("upgrade", $upgrade);

        return response($template, $this->request);
    }

    public function user($userID)
    {
        if($this->getPermission("view") != 1)
        {
            return "";
        }

        $template = $this->theme->draw("Admin/Upgrade/user.tpl");

        $template->assignVar("userID", $userID);

        return response($template, $this->request);
    }


    public function ajaxhistory($userID)
    {
        $this->request->session()->closeSessionWrite();

        $page = $this->request->input("page", 1, false, v()->integer()->min(1));
        $upgrades = $this->model->getUpgradeHistory($this->statistic, $page, 10, $userID);

        $template = $this->theme->draw("Admin/Upgrade/history_user.tpl");

        $template->assignVar("userID", $userID);
        $template->assignVar("upgrades", $upgrades);

        $json = ["success" => true, "content" => $template->parse(), "current" => $page, "total" => $upgrades["total"]];

        return response(json_encode($json, JSON_HEX_QUOT | JSON_HEX_TAG), $this->request)->contentType("text/json");
    }

    public function ajaxwithdrawhistory($userID)
    {
        $this->request->session()->closeSessionWrite();

        $page = $this->request->input("page", 1, false, v()->integer()->min(1));
        $withdraws = $this->model->getUpgradeWithdrawHistory($this->statistic, $page, 10, $userID);

        $template = $this->theme->draw("Admin/Upgrade/withdraw_history_user.tpl");

        $template->assignVar("userID", $userID);
        $template->assignVar("withdraws", $withdraws);

        $json = ["success" => true, "content" => $template->parse(), "current" => $page, "total" => $withdraws["total"]];

        return response(json_encode($json, JSON_HEX_QUOT | JSON_HEX_TAG), $this->request)->contentType("text/json");
    }

    public function gamecountstats()
    {
        if($this->getPermission("stats") == 1)
        {
            $statistic = new AjaxStatistics($this->request, 0);

            $this->request->session()->closeSessionWrite();
            $this->model->getGameCountStats($statistic);

            return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
        }

        return response(json_encode(["success" => false]), $this->request)->contentType("text/json");
    }

    public function gamesizestats()
    {
        if($this->getPermission("stats") == 1)
        {
            $statistic = new AjaxStatistics($this->request, 0);

            $this->request->session()->closeSessionWrite();
            $this->model->getGameSizeStats($statistic);

            return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
        }

        return response(json_encode(["success" => false]), $this->request)->contentType("text/json");
    }

    public function updateitemimages() {
        $data = json_decode(file_get_contents("https://api-trade.opskins.com/IItem/GetItems/v1/?key=".$this->request->input("key", "", false)), true);
        $items = $data["response"]["items"];

        foreach($items as $item) {
            foreach($item as $detaileditem) {
                $this->app->database()->update("steam_market")->set(["image" => $detaileditem["image"]["300px"]])->where(["market_name" => $detaileditem["name"]]);
            }
        }
    }
}