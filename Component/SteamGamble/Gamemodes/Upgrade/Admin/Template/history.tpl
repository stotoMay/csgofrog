<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css" />

    <title>Upgrade History {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="Admin/Upgrade/navigation.tpl"}
    </div>
    <div class="panel-body">
        <table class="table table-bordered">
            <tr>
                <td></td>
                <td>Today</td>
                <td>All Time</td>
            </tr>
            <tr>
                <td>Game count</td>
                <td>{$gamblestats->gamemode("upgrade")->getGameCount(midnight())}</td>
                <td>{$gamblestats->gamemode("upgrade")->getGameCount(0)}</td>
            </tr>
        </table>

        <form method="post" action="/admin/upgrade/history">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="{language("upgrade", "upgradeID")}" value="" name="upgradeID">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
            <input type="hidden" name="token" value="{token()}">
        </form>

        <hr>

        {includetemplate="Admin/Upgrade/upgrade_rows.tpl"}

        <div class="center-block">
            {pagination($upgrades.total, $upgrades.current, $pageurl)}
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>