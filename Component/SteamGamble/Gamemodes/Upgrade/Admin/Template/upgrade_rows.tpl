<div class="table-responsive">
<table class="table">
    <thead>
        <tr>
            <th>Upgrade ID</th>
            <th>User</th>
            <th>Chance</th>
            <th>Deposited</th>
            <th>Upgraded to</th>
            <th>Created</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        {foreach $upgrades.results as $upgrade}
            <tr class="{if $upgrade.won}success{else}danger{/if}">
                <td>#{$upgrade.upgradeID}</td>
                <td>
                    <img src="{$upgrade.user.avatar}" title="{$upgrade.user.personaname}" style="width:32px;height:32px;" data-toggle="tooltip"> <a href="/admin/user/view/{$upgrade.user.userID}">{$upgrade.user.personaname}</a>
                </td>
                <td>{number_format(multiply($upgrade.chance, 100))}%</td>
                <td>
                    {foreach $upgrade.deposited as $item}
                        {renderItem($item)}
                    {/foreach}
                </td>
                <td>{renderItem($upgrade.item)}</td>
                <td>{datetimeformat($upgrade.dateline)}</td>
                <td><a href="/admin/upgrade/game/{$upgrade.upgradeID}" class="btn btn-default">View</a></td>
            </tr>
        {/foreach}
    </tbody>
</table>
</div>