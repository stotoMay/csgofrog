<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css" />

    <title>Upgrade #{$upgrade.upgradeID} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ul class="breadcrumb">
    <li><a href="/admin/upgrade">Upgrade</a></li>
    <li><a href="/admin/upgrade/history">History</a></li>
    <li class="active">#{$upgrade.upgradeID}</li>
</ul>

<div class="panel panel-default">
    <div class="panel-body">
        <table class="table table-bordered">
            <tr>
                <td>Upgrade ID</td>
                <td>{$upgrade.upgradeID}</td>
            </tr>
            <tr>
                <td>Won</td>
                <td>
                    {if $upgrade.won}
                        YES
                    {else}
                        NO
                    {/if}
                </td>
            </tr>
            <tr>
                <td>Chance</td>
                <td>{number_format(multiply($upgrade.chance, 100))}%</td>
            </tr>
            <tr>
                <td>Roll Result</td>
                <td>{$upgrade.roll_result}</td>
            </tr>
            <tr>
                <td>Roll Type</td>
                <td>{$upgrade.roll_type}</td>
            </tr>
            <tr>
                <td>Roll Fee</td>
                <td>{$upgrade.roll_fee}</td>
            </tr>
            <tr>
                <td>Created</td>
                <td>{datetimeformat($upgrade.dateline)}</td>
            </tr>
        </table>

        <div class="col-md-6">
            <table class="table table-bordered">
                <tr>
                    <td>Tickets</td>
                    <td>${number_format(divide($upgrade.tickets_deposited, 100))}</td>
                </tr>
            </table>

            <div class="panel panel-default">
                <div class="panel-body">
                    {foreach $upgrade.deposited as $item}
                        {renderItem($item)}
                    {/foreach}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <table class="table table-bordered">
                <tr>
                    <td>Tickets</td>
                    <td>${number_format(divide($upgrade.item.tickets, 100))}</td>
                </tr>
            </table>

            <div class="panel panel-default">
                <div class="panel-body">
                    {renderItem($upgrade.item)}
                </div>
            </div>
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>