<ul class="nav nav-tabs">
    <li class="{if $path.2 == ""}active{/if}"><a href="/admin/upgrade/">Search Item</a></li>
    <li class="{if $path.2 == "market"}active{/if}"><a href="/admin/upgrade/market">Market</a></li>
    <li class="{if $path.2 == "history"}active{/if}"><a href="/admin/upgrade/history">History</a></li>
    {if permission("stats", "controller", "upgrade") == 1}
        <li class="{if $path.2 == "stats"}active{/if}"><a href="/admin/upgrade/stats">Statistics</a></li>
    {/if}
</ul>