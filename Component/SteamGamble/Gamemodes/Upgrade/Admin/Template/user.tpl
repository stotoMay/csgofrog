<div class="panel panel-default">
    <div class="panel-heading">Upgrade History</div>
    <div class="panel-body">
        {pagination_ajax(string("/admin/upgrade/ajaxhistory/", $userID))}
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Upgrade Withdraw History</div>
    <div class="panel-body">
        {pagination_ajax(string("/admin/upgrade/ajaxwithdrawhistory/", $userID))}
    </div>
</div>