<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css" />

    <title>Upgrade Search {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="Admin/Upgrade/navigation.tpl"}
    </div>
    <div class="panel-body">
        <form action="/admin/upgrade" method="get">
            <div class="input-group">
                <input type="text" class="form-control" placeholder='{language("steam", "items", "itemname")}' value="{htmlspecialchars($match)}" name="search" id="srch-term">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
            <input type="hidden" name="page" value="1">
        </form>

        <hr>

        <div class="table-responsive">
            <form method="post" action="/admin/upgrade">
                <table class="table table-borderless">
                    {foreach $items.results as $item}
                        <tr>
                            <td>{renderItem($item)}</td>
                            <td>{$item.appname}</td>
                            <td>{$item.name}</td>
                            {if $item.price == ""}
                                <td>NaN</td>
                            {else}
                                <td>${number_format($item.price)}</td>
                            {/if}
                            <td>
                                <button name="marketID" value="{$item.id}" class="btn btn-primary">Add to Market</button>
                            </td>
                        </tr>
                    {/foreach}
                </table>
                <input type="hidden" name="token" value="{token()}">
                <input type="hidden" name="page" value="{$items.current}">
                <input type="hidden" name="search" value="{$match}">
            </form>
        </div>

        <div class="center-block">{pagination($items.pages, $items.current, $pageurl)}</div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>