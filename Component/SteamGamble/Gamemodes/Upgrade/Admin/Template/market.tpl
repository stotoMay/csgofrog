<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css" />

    <title>Upgrade Market {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="Admin/Upgrade/navigation.tpl"}
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-borderless">
                {foreach $marketitems as $item}
                    <tr>
                        <td>{renderItem($item)}</td>
                        <td>{$item.market_name}</td>
                        {if $item.price == ""}
                            <td>NaN</td>
                        {else}
                            <td>${number_format($item.price)}</td>
                        {/if}
                        <td>
                            <form method="post" action="/admin/upgrade/market">
                                {if $item.disabled}
                                    <button name="action" value="enable" class="btn btn-primary">Enable again</button>
                                {/if}
                                <button name="action" value="delete" class="btn btn-primary">Delete from Market</button>
                                <input type="hidden" name="marketID" value="{$item.id}">
                                <input type="hidden" name="token" value="{token()}">
                            </form>
                        </td>
                    </tr>
                {/foreach}
            </table>
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>