<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th>Withdraw ID</th>
            <th>Item</th>
            <th>Status</th>
            <th>Date</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            {foreach $withdraws.results as $trade}
                <tr class="{if $trade.status == 5}danger{/if}">
                    <td>#{$trade.id}</td>
                    <td>{renderItem($trade.item)}</td>
                    <td>
                        {if $trade.status == 1}
                            Withdraw Requested
                        {elseif $trade.status == 2}
                            Item needs to be bought
                        {elseif $trade.status == 3}
                            Withdraw was sent successfully
                        {elseif $trade.status == 5}
                            Failed to buy item from Opskins. Refund necessary
                        {elseif $trade.status == 6}
                            Item refunded
                        {/if}
                    </td>
                    <td>{datetimeformat($trade.dateline)}</td>
                    <td>
                        {if $trade.status == 5}
                            <form method="post" action="/admin/upgrade/manualrefunded">
                                <input type="submit" class="btn btn-primary" value="Mark as manually refunded">
                                <input type="hidden" name="token" value="{token()}">
                                <input type="hidden" name="tradeID" value="{$trade.id}">
                                <input type="hidden" name="userID" value="{$trade.userID}">
                            </form>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</div>