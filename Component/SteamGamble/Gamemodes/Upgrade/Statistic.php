<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Gamemodes\Upgrade;

class Statistic extends \Component\SteamGamble\Parent\Statistic
{
    public function getGameCount($time = 0)
    {
        $count = $this->gamble->app()->database()->select("upgrade")->where(["dateline[>=]" => $time])->numRows();

        return $count;
    }

    public function getBiggestValue($time = 0)
    {
        $coinflip = $this->gamble->app()->database()->select("upgrade")
            ->column(["tickets"])
            ->where(["dateline[>=]" => $time])
            ->order("tickets", "desc")
            ->limit(0, 1)->fetchRow();

        return $coinflip["tickets"] / 100;
    }

    public function getWithdrawHistory($userID, $page, $size = 15) {
        $withdraws = $this->gamble->app()->database()->select("upgrade_deposit")
            ->where(["userID" => $userID, "status" => [1,2,3,5,6]])
            ->order("dateline", "desc")
            ->limit(($page-1)*$size, $size)
            ->fetchAssoc();

        foreach($withdraws as $withdraw) {
            $this->steam->item()->queueMarket($withdraw["appID"], $withdraw["market_name"]);
        }

        $result = [];
        foreach($withdraws as $withdraw) {
            $result[] = [
                "item" => $this->steam->item()->getMarket($withdraw["appID"], $withdraw["market_name"]),
                "id" => $withdraw["id"], "tickets" => $withdraw["tickets"], "status" => $withdraw["status"],
                "dateline" => $withdraw["dateline"], "userID" => $userID
            ];
        }

        foreach($result as $key => $row) {
            $result[$key]["item"]["price"] = $row["tickets"] / 100;
        }

        return $result;
    }

    public function getUpgradeHistory($page, $size = 50, $userID = NULL, $wononly = false)
    {
        $query = $this->app->database()->select("upgrade")->order("dateline", "desc")->limit(($page-1)*$size, $size);

        $condition = [];
        if(is_numeric($userID))
        {
            $condition["userID"] = $userID;
        }

        if($wononly) {
            $condition["won"] = 1;
        }

        if(count($condition) > 0) {
            $query->where($condition);
        }

        $upgrades = $query->fetchAssoc();

        $upgradeIDs = [];
        $userIDs = [];

        foreach($upgrades as $upgrade)
        {
            $userIDs[] = $upgrade["userID"];
            $upgradeIDs[] = $upgrade["id"];

            $this->steam->item()->queueMarket($upgrade["appID"], $upgrade["market_name"]);
        }

        if(count($upgradeIDs) == 0)
        {
            return [];
        }

        $userIDs = array_unique($userIDs);

        $userlist = $this->app->database()->select("user")->column(["id", "steamID"])->where(["id" => $userIDs])->fetchAssoc();
        $users = [];
        foreach($userlist as $user)
        {
            $users[$user["id"]] = array_merge(["userID" => $user["id"]], $this->steam->api()->user()->getPlayerSummaries($user["steamID"])[0]);
        }

        $depositItems = [];

        foreach($upgradeIDs as $upgradeID)
        {
            $depositItems[$upgradeID] = [];
        }

        $itemlist = $this->app->database()->select("upgrade_item")->where(["upgradeID" => $upgradeIDs])->fetchAssoc();
        $this->steam->item()->filling($itemlist);

        foreach($itemlist as $item)
        {
            $item["price"] = $item["tickets"] / 100;
            $depositItems[$item["upgradeID"]][] = $item;
        }

        foreach($depositItems as $upgradeID => $itemlist)
        {
            usort($depositItems[$upgradeID], function($a, $b) {
                return $b["tickets"] - $a["tickets"];
            });
        }

        $result = [];
        foreach($upgrades as $upgrade)
        {
            $data = ["upgradeID" => $upgrade["id"], "user" => $users[$upgrade["userID"]]];

            $data["user"] = $users[$upgrade["userID"]];

            $data["deposited"] = $depositItems[$upgrade["id"]];

            $data["item"] = $this->steam->item()->getMarket($upgrade["appID"], $upgrade["market_name"]);
            $data["item"]["tickets"] = $upgrade["tickets"];
            $data["item"]["price"] = $upgrade["tickets"] / 100;
            $data["item"]["contextID"] = $upgrade["contextID"];

            $data["chance"] = ((1 - $upgrade["roll_fee"] / 100)*$upgrade["tickets_deposited"]) / $upgrade["tickets"];

            $data = array_merge($data, [
                "roll_result" => $upgrade["roll_result"], "roll_type" => $upgrade["roll_type"], "roll_fee" => $upgrade["roll_fee"],
                "won" => $upgrade["won"], "tickets_deposited" => $upgrade["tickets_deposited"], "dateline" => $upgrade["dateline"]
            ]);

            $result[] = $data;
        }

        return $result;
    }

    public function getUpgrade($upgradeID)
    {
        $upgrade = $this->app->database()->select("upgrade")->where(["id" => $upgradeID])->fetchRow();

        if(!is_array($upgrade))
        {
            return false;
        }

        $data = ["upgradeID" => $upgrade["id"]];

        $items = $this->app->database()->select("upgrade_item")->where(["upgradeID" => $upgradeID])->fetchAssoc();
        $items[] = ["appID" => $upgrade["appID"], "contextID" => $upgrade["contextID"], "market_name" => $upgrade["market_name"], "tickets" => $upgrade["tickets"]];

        foreach($items as $key => $value) {
            $items[$key]["price"] = $value["tickets"] / 100;
        }

        $this->steam->item()->filling($items);

        $user = $this->app->database()->select("user")->column(["id", "steamID"])->where(["id" => $upgrade["userID"]])->fetchRow();
        $data["user"] = array_merge(["userID" => $user["id"]], $this->steam->api()->user()->getPlayerSummaries($user["steamID"])[0]);

        $data["item"] = $items[count($items) - 1];
        unset($items[count($items) - 1]);
        $data["deposited"] = $items;

        $data["chance"] = ((1 - $upgrade["roll_fee"] / 100)*$upgrade["tickets_deposited"]) / $upgrade["tickets"];

        $data = array_merge($data, [
            "roll_result" => $upgrade["roll_result"], "roll_type" => $upgrade["roll_type"], "roll_fee" => $upgrade["roll_fee"],
            "won" => $upgrade["won"], "tickets_deposited" => $upgrade["tickets_deposited"], "dateline" => $upgrade["dateline"]
        ]);

        return $data;
    }

    public function getUserProfitGraph($userID)
    {
        $games = $this->gamble->app()->database()->select("upgrade")
            ->column(["dateline", "userID", "won", "tickets", "tickets_deposited"])
            ->where(["userID" => $userID])
            ->order("dateline", "asc")->fetchAssoc();

        $result = [];

        foreach($games as $game)
        {
            if($game["won"])
            {
                $result[] = [
                    "value" => ($game["tickets"] - $game["tickets_deposited"]) / 100,
                    "dateline" => $game["dateline"]
                ];
            }
            else
            {
                $result[] = [
                    "value" => (-1 * $game["tickets_deposited"]) / 100,
                    "dateline" => $game["dateline"]
                ];
            }
        }

        return $result;
    }

    public function getUserLuckyGraph($userID)
    {
        $games = $this->gamble->app()->database()->select("upgrade")
            ->column(["dateline", "userID", "won", "tickets", "tickets_deposited"])
            ->where(["userID" => $userID])
            ->order("dateline", "asc")->fetchAssoc();

        $result = [];

        foreach($games as $game)
        {
            if($game["won"])
            {
                $result[] = [
                    "value" => 1 - ($game["tickets_deposited"] / $game["tickets"]),
                    "dateline" => $game["dateline"]
                ];
            }
            else
            {
                $result[] = [
                    "value" => -1 * ($game["tickets_deposited"] / $game["tickets"]),
                    "dateline" => $game["dateline"]
                ];
            }
        }

        return $result;
    }
}