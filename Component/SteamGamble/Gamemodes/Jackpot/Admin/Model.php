<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Gamemodes\Jackpot\Admin;

use App\Admin\Tools\AjaxStatistics;
use Component\SteamGamble\Gamemodes\Jackpot\Statistic;

class Model extends \App\Router\Model
{
    public function userExists($userID)
    {
        return $this->database->select("user")->where(["id" => $userID])->exists();
    }

    public function activeJackpotExists($jackpotID)
    {
        return $this->database->select("jackpot")->where(["id" => $jackpotID, "status" => 0])->exists();
    }

    public function jackpotExists($jackpotID)
    {
        return $this->database->select("jackpot")->where(["id" => $jackpotID, "status" => 1])->exists();
    }

    public function getJackpotHistory(Statistic $statistic, $page, $size = 50, $userID = NULL, $fetchitems = false)
    {
        if(is_numeric($userID))
        {
            $count = $this->database->select("jackpot_participant")
                ->column(["jackpot.id"])
                ->leftJoin("jackpot", ["jackpot.id[=]jackpot_participant.jackpotID"])
                ->where(["userID" => $userID, "jackpot.status" => 1])->group("jackpotID")
                ->numRows();
        }
        else
        {
            $count = $this->database->select("jackpot")->where(["status" => 1])->numRows();
        }

        $result = $statistic->getJackpotHistory($page, $size, $userID, $fetchitems);

        return ["results" => $result, "total" => ceil($count / $size), "current" => $page];
    }

    public function getJackpotSettingID($jackpotID)
    {
        $jackpot = $this->database->select("jackpot")->where(["id" => $jackpotID])->fetchRow();

        return $jackpot["settingID"];
    }

    public function getJackpotSettings()
    {
        return $this->database->select("jackpot_setting")->fetchAssoc();
    }

    public function getJackpotSetting($settingID)
    {
        return $this->database->select("jackpot_setting")->where(["id" => $settingID])->fetchRow();
    }

    public function jackpotSettingExists($settingID)
    {
        return $this->database->select("jackpot_setting")->where(["id" => $settingID])->exists();
    }

    public function createJackpotSetting()
    {
        return $this->database->insert("jackpot_setting")->set(["deleteable" => 1, "name" => "Jackpot"])->getID();
    }

    public function updateJackpotSetting($settingID, $values)
    {
        $this->database->update("jackpot_setting")->set($values)->where(["id" => $settingID]);
    }

    public function deleteJackpotSetting($settingID)
    {
        $setting = $this->getJackpotSetting($settingID);

        if($setting["deleteable"] == 0)
        {
            return;
        }

        $this->database->delete("jackpot_setting")->where(["id" => $settingID]);
    }

    public function getGameCountStats(AjaxStatistics $statistic)
    {
        $this->database->query("SET time_zone = '".$statistic->getTimezone()."'");

        $data = $this->database->select("jackpot")->column([
            "*" => ["function" => "count", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(finished), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where(["finished[>=]" => $statistic->getStartTime(), "finished[<=]" => $statistic->getStopTime(), "status" => 1])
            ->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->language->get(["steamgamble", "jackpot", "label_gamecount"]));
    }

    public function getGameSizeStats(AjaxStatistics $statistic)
    {
        $this->database->query("SET time_zone = '".$statistic->getTimezone()."'");

        $data = $this->database->select("jackpot")->column([
            ["str" => "(SUM(tickets) / COUNT(*)) / 100", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(finished), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where(["finished[>=]" => $statistic->getStartTime(), "finished[<=]" => $statistic->getStopTime(), "status" => 1])
            ->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->language->get(["steamgamble", "jackpot", "label_gamesize"]));
    }

    public function getPlayerCountStats(AjaxStatistics $statistic)
    {
        $this->database->query("SET time_zone = '".$statistic->getTimezone()."'");

        $data = $this->database->select("jackpot_participant")->column([
            "*" => ["function" => "count", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(joined), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where(["joined[>=]" => $statistic->getStartTime(), "joined[<=]" => $statistic->getStopTime()])
            ->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->language->get(["steamgamble", "jackpot", "label_playercount"]));
    }

    public function getJackpotWinningPercentage($jackpotID)
    {
        $coinflip = $this->database->select("jackpot")->where(["id" => $jackpotID])->fetchRow();

        return $coinflip["percentage"];
    }
}