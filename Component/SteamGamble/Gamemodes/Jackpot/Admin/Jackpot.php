<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Admin\Tools\AjaxStatistics;
use App\Router\Controller;
use Component\SteamGamble\Classes\Core;
use Component\SteamGamble\GambleFactory;
use Component\SteamGamble\Gamemodes\Jackpot\Admin\Model;
use Component\SteamGamble\Gamemodes\Jackpot\Statistic;

class Jackpot extends Controller
{
    private $admin;
    /**
     * @var Model
     */
    private $model;

    /**
     * @var \Component\Steam\Core
     */
    private $steam;
    /**
     * @var Core
     */
    private $gamble;
    /**
     * @var Statistic
     */
    private $statistic;

    public static function __info__()
    {
        return [
            "uniqname" => "jackpot", "setting" => ["fees" => "^(?:100|[1-9]?[0-9])$"],
            "permission" => ["view" => "bool", "stats" => "bool", "setting" => "bool", "percentage" => "bool"], "visible" => true,
            "icon" => "circle-o-notch", "tracking" => true, "priority" => 5,
            "requirement" => [["application", "general", "admin_control_panel"], ["controller", "jackpot", "view"]]
        ];
    }

    public function __init__()
    {
        $this->model = $this->getModel("Component/SteamGamble/Gamemodes/Jackpot/Admin/Model");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));
        $this->theme->registerFolder("Component/SteamGamble/Gamemodes/Jackpot/Admin/Template", "Admin/Jackpot/");
        $this->authAdmin();

        $this->gamble = GambleFactory::getGambleCore();
        $this->steam = $this->gamble->steam();
        $this->statistic = $this->gamble->statistic()->gamemode("jackpot");
    }

    public function main()
    {
        $template = $this->theme->draw("Admin/Jackpot/main.tpl");

        $template->assignVar("settings", $this->model->getJackpotSettings());

        return response($template, $this->request);
    }

    public function active($settingID)
    {
        $template = $this->theme->draw("Admin/Jackpot/active.tpl");

        $template->assignVar("settingID", $settingID);

        return response($template, $this->request);
    }

    public function history()
    {
        $template = $this->theme->draw("Admin/Jackpot/history.tpl");

        if($this->request->has("jackpotID", true, v()->integer()))
        {
            $jackpotID = $this->request->input("jackpotID");

            if($this->model->jackpotExists($jackpotID))
            {
                redirect("/admin/jackpot/game/".$jackpotID, $this->request);
            }
            else
            {
                $this->printError($this->language->get(["jackpot", "jackpot_not_found"]));
            }
        }

        $page = $this->request->input("page", 1, false, v()->integer()->min(1));

        $jackpots = $this->model->getJackpotHistory($this->statistic, $page);

        $template->assignVar("jackpots", $jackpots);
        $template->assignVar("pageurl", "/admin/jackpot/history?page=%p");

        return response($template, $this->request);
    }

    public function stats()
    {
        if($this->getPermission("stats") != 1)
        {
            redirect("/admin/jackpot", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("Admin/Jackpot/stats.tpl");

        return response($template, $this->request);
    }

    public function editsetting($settingID)
    {
        if($this->getPermission("setting") != 1 || !$this->model->jackpotSettingExists($settingID))
        {
            redirect("/admin/jackpot", $this->request)->error($this->language->getMessage("permission"));
        }

        if(count($this->request->all()) > 5)
        {
            $values = ["min_deposit_value" => 0, "max_deposit_value" => 0, "max_deposit_items" => 10, "multiple_deposit" => 0, "gametime" => 90, "max_items" => 100, "min_players" => 2, "name" => "Jackpot"];

            $values["name"] = $this->request->input("name", $values["name"]);
            $values["min_deposit_value"] = intval($this->request->input("min_deposit_value", $values["min_deposit_value"]) * 100);
            $values["max_deposit_value"] = intval($this->request->input("max_deposit_value", $values["max_deposit_value"]) * 100);
            $values["max_deposit_items"] = intval($this->request->input("max_deposit_items", $values["max_deposit_items"]));
            $values["multiple_deposit"] = $this->request->input("multiple_deposit", $values["multiple_deposit"]) ? 1 : 0;
            $values["gametime"] = intval($this->request->input("gametime", $values["gametime"]));
            $values["max_items"] = intval($this->request->input("max_items", $values["max_items"]));
            $values["min_players"] = intval($this->request->input("min_players", $values["min_players"]));

            $this->model->updateJackpotSetting($settingID, $values);

            redirect("/admin/jackpot/editsetting/".$settingID, $this->request)->info("Your changes have been saved!");
        }

        $template = $this->theme->draw("Admin/Jackpot/edit.tpl");

        $template->assignVar("settingID", $settingID);
        $template->assignVar("setting", $this->model->getJackpotSetting($settingID));

        return response($template, $this->request);
    }

    public function createsetting()
    {
        if($this->getPermission("setting") != 1)
        {
            redirect("/admin/jackpot", $this->request)->error($this->language->getMessage("permission"));
        }

        $settingID = $this->model->createJackpotSetting();

        redirect("/admin/jackpot/editsetting/".$settingID, $this->request)->info("A public jackpot was created! Please specify the settings!");
    }

    public function deletesetting($settingID)
    {
        if($this->getPermission("setting") != 1 || !$this->model->jackpotSettingExists($settingID))
        {
            redirect("/admin/jackpot", $this->request)->error($this->language->getMessage("permission"));
        }

        $this->model->deleteJackpotSetting($settingID);

        redirect("/admin/jackpot/", $this->request)->info("The public jackpot was successfully deleted!");
    }

    public function game($jackpotID)
    {
        $template = $this->theme->draw("Admin/Jackpot/jackpot.tpl");

        if(!$this->model->jackpotExists($jackpotID))
        {
            redirect("/admin/jackpot/history", $this->request)->error($this->language->get(["jackpot", "jackpot_not_found"]));
        }

        $jackpot = $this->statistic->getJackpot($jackpotID);

        $template->assignVar("jackpot", $jackpot);

        return response($template, $this->request);
    }

    public function user($userID)
    {
        if($this->getPermission("view") != 1)
        {
            return "";
        }

        $template = $this->theme->draw("Admin/Jackpot/user.tpl");

        $template->assignVar("userID", $userID);

        return response($template, $this->request);
    }


    public function ajaxhistory($userID)
    {
        $this->request->session()->closeSessionWrite();

        $page = $this->request->input("page", 1, false, v()->integer()->min(1));
        $jackpots = $this->model->getJackpotHistory($this->statistic, $page, 10, $userID);

        $template = $this->theme->draw("Admin/Jackpot/history_user.tpl");

        $template->assignVar("userID", $userID);
        $template->assignVar("jackpots", $jackpots);

        $json = ["success" => true, "content" => $template->parse(), "current" => $page, "total" => $jackpots["total"]];

        return response(json_encode($json, JSON_HEX_QUOT | JSON_HEX_TAG), $this->request)->contentType("text/json");
    }


    public function playercountstats()
    {
        if($this->getPermission("stats") == 1)
        {
            $statistic = new AjaxStatistics($this->request, 0);

            $this->request->session()->closeSessionWrite();
            $this->model->getPlayerCountStats($statistic);

            return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
        }

        return response(json_encode(["success" => false]), $this->request)->contentType("text/json");
    }

    public function gamecountstats()
    {
        if($this->getPermission("stats") == 1)
        {
            $statistic = new AjaxStatistics($this->request, 0);

            $this->request->session()->closeSessionWrite();
            $this->model->getGameCountStats($statistic);

            return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
        }

        return response(json_encode(["success" => false]), $this->request)->contentType("text/json");
    }

    public function gamesizestats()
    {
        if($this->getPermission("stats") == 1)
        {
            $statistic = new AjaxStatistics($this->request, 0);

            $this->request->session()->closeSessionWrite();
            $this->model->getGameSizeStats($statistic);

            return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
        }

        return response(json_encode(["success" => false]), $this->request)->contentType("text/json");
    }

    public function percentage($jackpotID)
    {
        $result = ["success" => false];

        if($this->getPermission("percentage") == 1 && is_numeric($jackpotID) && $this->model->activeJackpotExists($jackpotID))
        {
            $result = ["success" => true, "percentage" => $this->model->getJackpotWinningPercentage($jackpotID)];
        }

        return response(json_encode($result), $this->request)->contentType("text/json");
    }
}