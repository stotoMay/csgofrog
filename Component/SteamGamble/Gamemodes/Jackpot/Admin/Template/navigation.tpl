<ul class="nav nav-tabs">
    <li class="{if $path.2 == ""}active{/if}"><a href="/admin/jackpot">Public Jackpots</a></li>
    <li class="{if $path.2 == "history"}active{/if}"><a href="/admin/jackpot/history">History</a></li>
    {if permission("stats", "controller", "jackpot") == 1}
        <li class="{if $path.2 == "stats"}active{/if}"><a href="/admin/jackpot/stats">Statistics</a></li>
    {/if}
</ul>