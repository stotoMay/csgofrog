<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>Jackpot {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ul class="breadcrumb">
    <li><a href="/admin/jackpot">Jackpot</a></li>
    <li class="active">{$setting.name}</li>
</ul>

<div class="panel panel-default">
    <div class="panel-heading">Edit {$setting.name}</div>
    <div class="panel-body">
        <form method="post" action="/admin/jackpot/editsetting/{$settingID}">
            <table class="table table-borderless">
                <tr>
                    <td>Jackpot name which is displayed</td>
                    <td><input class="form-control" type="text" name="name" value="{$setting.name}"></td>
                </tr>
                <tr>
                    <td>Min deposit value (0 = no limit)</td>
                    <td><input class="form-control" type="text" name="min_deposit_value" value="{divide($setting.min_deposit_value, 100)}">
                    </td>
                </tr>
                <tr>
                    <td>Max deposit value (0 = no limit)</td>
                    <td><input class="form-control" type="text" name="max_deposit_value" value="{divide($setting.max_deposit_value, 100)}">
                    </td>
                </tr>
                <tr>
                    <td>Max deposit items of a user each jackpot</td>
                    <td><input class="form-control" type="number" name="max_deposit_items" value="{$setting.max_deposit_items}"></td>
                </tr>
                <tr>
                    <td>One user can deposit multiple times into the jackpot</td>
                    <td>
                        {if $setting.multiple_deposit == 1}
                            <input type="radio" name="multiple_deposit" value="1" checked>
                            Enabled
                            <input type="radio" name="multiple_deposit" value="0">
                            Disabled
                        {else}
                            <input type="radio" name="multiple_deposit" value="1">
                            Enabled
                            <input type="radio" name="multiple_deposit" value="0" checked>
                            Disabled
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td>Duration until the jackpot ends if more than 'min_players' players joined (in seconds)</td>
                    <td><input class="form-control" type="number" name="gametime" value="{$setting.gametime}"></td>
                </tr>
                <tr>
                    <td>Max items of each jackpot (jackpot ends without waiting for the timer if this limit is exceeded)</td>
                    <td><input class="form-control" type="number" name="max_items" value="{$setting.max_items}"></td>
                </tr>
                <tr>
                    <td>Timer starts after this amount of unique players have joined the jackpot</td>
                    <td><input class="form-control" type="number" name="min_players" value="{$setting.min_players}"></td>
                </tr>
            </table>
            <input type="submit" class="btn btn-primary" value="Submit"> <input type="hidden" name="token" value="{token()}">
        </form>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>