<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>Jackpots {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="Admin/Jackpot/navigation.tpl"}
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Min Deposit</th>
                    <th>Max Deposit</th>
                    <th>Gametime</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {foreach $settings as $setting}
                    <tr>
                        <td>{$setting.name}</td>
                        <td>${number_format(divide($setting.min_deposit_value, 100))}</td>
                        <td>
                            {if $setting.max_deposit_value == "0"}
                                NO LIMIT
                            {else}
                                ${number_format(divide($setting.max_deposit_value, 100))}
                            {/if}
                        </td>
                        <td>{$setting.gametime}s</td>
                        <td>
                            <a href="/admin/jackpot/active/{$setting.id}" class="btn btn-default">Show Current Jackpot</a>
                        </td>
                        {if permission("setting", "controller", "jackpot") == 1}
                            <td>
                                <a href="/admin/jackpot/editsetting/{$setting.id}" class="btn btn-default">Edit Settings</a>
                            </td>
                        {else}
                            <td></td>
                        {/if}
                        {if $setting.deleteable == 1 && permission("setting", "controller", "jackpot") == 1}
                            <td><a href="/admin/jackpot/deletesetting/{$setting.id}" class="btn btn-primary">Delete</a>
                            </td>
                        {else}
                            <td></td>
                        {/if}
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>

        {if permission("setting", "controller", "jackpot") == 1}
            <a href="/admin/jackpot/createsetting" class="btn btn-primary">Create New Public Jackpot</a>
            <br>
            <br>
        {/if}
    </div>
</div>


{includetemplate="footer.tpl"}
</body>
</html>