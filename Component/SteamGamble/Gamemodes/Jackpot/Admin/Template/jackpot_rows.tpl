<div class="table-responsive">
<table class="table">
    <thead>
        <tr>
            <th>Jackpot ID</th>
            <th>Setting ID</th>
            <th>Players</th>
            <th>Value</th>
            <th>Winner</th>
            <th>Created</th>
            <th>Ended</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        {foreach $jackpots.results as $jackpot}
            <tr class="{if $jackpot.winner.userID == $userID}success{/if}">
                <td>#{$jackpot.jackpotID}</td>
                <td><a href="/admin/jackpot/editsetting/{$jackpot.settingID}">#{$jackpot.settingID}</a></td>
                <td>
                    {foreach $jackpot.participants as $participant}
                        <a href="/admin/user/view/{$participant.user.userID}" style="font-size:0;">
                            <img src="{$participant.user.avatar}"
                                 title="{$participant.user.personaname} [${number_format(divide($participant.tickets, 100))} | {number_format(multiply(divide($participant.tickets, $jackpot.tickets), 100))}%]"
                                 style="width:32px;height:32px;" data-toggle="tooltip">
                        </a>&nbsp;
                    {/foreach}
                </td>
                <td>${number_format(divide($jackpot.tickets, 100))}</td>
                <td>
                    <a href="/admin/user/view/{$jackpot.winner.userID}"><img src="{$jackpot.winner.avatar}" style="width:32px;height:32px;" title="{$jackpot.winner.personaname}" data-toggle="tooltip"></a>
                </td>
                <td>{datetimeformat($jackpot.created)}</td>
                <td>{datetimeformat($jackpot.finished)}</td>
                <td><a href="/admin/jackpot/game/{$jackpot.jackpotID}" class="btn btn-default">View</a></td>
            </tr>
        {/foreach}
    </tbody>
</table>
</div>