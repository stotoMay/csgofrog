<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>Jackpot Statistics {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="Admin/Jackpot/navigation.tpl"}
    </div>
    <div class="panel-body">
        <h1>Game Count</h1>
        {statistics("/admin/jackpot/gamecountstats")}
        <h1>Players</h1>
        {statistics("/admin/jackpot/playercountstats")}
        <h1>Average Pot Size</h1>
        {statistics("/admin/jackpot/gamesizestats")}
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>