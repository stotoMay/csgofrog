<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>Jackpot History {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="Admin/Jackpot/navigation.tpl"}
    </div>
    <div class="panel-body">
        <table class="table table-bordered">
            <tr>
                <td></td>
                <td>Today</td>
                <td>All Time</td>
            </tr>
            <tr>
                <td>Game count</td>
                <td>{$gamblestats->gamemode("jackpot")->getGameCount(midnight())}</td>
                <td>{$gamblestats->gamemode("jackpot")->getGameCount(0)}</td>
            </tr>
            <tr>
                <td>Maximum Prize</td>
                <td>${number_format($gamblestats->gamemode("jackpot")->getBiggestValue(midnight()))}</td>
                <td>${number_format($gamblestats->gamemode("jackpot")->getBiggestValue(0))}</td>
            </tr>
        </table>

        <form method="post" action="/admin/jackpot/history">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="{language("jackpot", "jackpotID")}" value="" name="jackpotID">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
            <input type="hidden" name="token" value="{token()}">
        </form>

        <hr>

        {includetemplate="Admin/Jackpot/jackpot_rows.tpl"}

        <div class="center-block">
            {pagination($jackpots.total, $jackpots.current, $pageurl)}
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>