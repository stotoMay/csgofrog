$(document).ready(function () {
    var websocket = new WebsocketHandler(g_serverip, g_serverauth);

    websocket.addListener("onConnectionClose", function () {
        print_error(g_lang_lostconnection);
    });

    websocket.addListener("onJackpotLoad", function (data) {
        startJackpot(data);

        for (var i = 0; i < data["players"].length; i++) {
            addParticipant(data["players"][i])
        }
        if (data["winner"]) {
            endJackpot(data["winner"]);
        }
        if(data["starttime"])
        {
            startTimer(data["starttime"])
        }
    });

    websocket.addListener("onJackpotTimerStart", startTimer);
    websocket.addListener("onJackpotPlayerJoin", addParticipant);
    websocket.addListener("onJackpotEnd", endJackpot);
    websocket.addListener("onJackpotStart", startJackpot);

    websocket.sendMessage("set_gamemode", {"gamemode": "jackpot", "gameID": "public:" + g_jackpot_settingID});

    var jackpot = null;
    var tickets = 0;
    var playercount = 0;
    var itemcount = 0;

    var countdown = 60;
    var timer = null;

    function startTimer(data) {
        var countdown = parseInt(data) + parseInt(jackpot["gametime"]) + timefix - Math.round(Date.now() / 1000);

        timer = setInterval(function () {
            if(countdown == 0)
            {
                return;
            }

            countdown--;

            $("#stat-timer").html(countdown+"s");
        }, 1000);
    }

    function addParticipant(data) {
        tickets += parseInt(data["tickets"]);
        playercount += 1;
        itemcount += data["items"].length;

        $("#stat-value").html("$"+(tickets/100).toFixed(2));
        $("#stat-itemcount").html(itemcount+" / "+jackpot["max_items"]);
        $("#stat-playercount").html(playercount);

        var $participant = $("#sample_participant").clone().attr("id", "").show();
        var $body = $(document.createElement("div")).addClass("panel-body").html($participant);
        var $panel = $(document.createElement("div")).addClass("panel").addClass("panel-default").html($body);

        $("#jackpot-players").append($panel);

        $participant.find(".participant-image").attr("src", data["user"]["avatarfull"]);

        var $ahref = $(document.createElement("a")).html(data["user"]["personaname"]).attr("href", "/admin/user/view/"+data["user"]["userID"]);
        $participant.find(".participant-username").html($ahref);

        $participant.find(".participant-deposited").html("$"+ (data["tickets"]/100).toFixed(2));
        $participant.find(".participant-chance").attr("data-tickets", data["tickets"]).html(((data["tickets"]/tickets)*100).toFixed(2)+"%");
        $participant.find(".participant-ticket-start").html(tickets-data["tickets"]+1);
        $participant.find(".participant-ticket-end").html(tickets);
        $participant.find(".participant-joined").html(new Date(parseInt(data["joined"])*1000));

        $(".participant-chance").each(function () {
            var $this = $(this);

            $this.html(((parseInt($this.attr("data-tickets"))/tickets)*100).toFixed(2)+"%")
        });

        for(var i = 0; i < data["items"].length; i++)
        {
            $participant.find(".participant-items").append(renderItem(data["items"][i], 72, 72, true));
        }
    }

    function startJackpot(data) {
        jackpot = data;
        tickets = 0;
        playercount = 0;
        countdown = parseInt(data["gametime"]);

        clearInterval(timer);

        $(".jackpotID").html(data["jackpotID"]);
        $("#stat-value").html("$0.00");
        $("#stat-timer").html(data["gametime"]+"s");
        $("#stat-itemcount").html("0 / "+data["max_items"]);
        $("#stat-playercount").html("0");
        $("#stat-winner").html("");
        $("#jackpot-players").html("");

        $("#stat-roundhash").html(data["roundhash"]);

        $.ajax({
            "success": function (resp) {
                if(resp["success"] === false)
                {
                    return;
                }

                $("#stat-percentage").html(resp["percentage"]);
            },
            "url": "/admin/jackpot/percentage/"+data["jackpotID"],
            "dataType": "json"
        });
    }

    function endJackpot(data) {
        var $ahref = $(document.createElement("a")).html(data["user"]["personaname"]).attr("href", "/admin/user/view/"+data["user"]["userID"]);
        $("#stat-winner").html($ahref);

        $(".winner-button").addClass("disabled");
        $("#stat-secret").html(data["secret"]);
        $("#stat-percentage").html(data["percentage"]);
    }
});