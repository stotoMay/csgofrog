<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" type="text/css" href="/Component/Steam/Admin/Template/Static/CSS/steam.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js"></script>
    <script src="/Component/Steam/Admin/Template/Static/JScript/steam.js"></script>
    <script src="/Component/SteamWeb/Admin/Template/JScript/websocket.js"></script>

    <script type="text/javascript">
        var g_serverauth = "{getServerAuth()}";
        var g_serverip = '{setting("serverip", "component", "SteamWeb")}';
        var g_jackpot_settingID = {$settingID};
        var g_lang_lostconnection = '{language("steamweb", "message", "lostconnection")}';
    </script>

    <script src="/Component/SteamGamble/Gamemodes/Jackpot/Admin/Template/JScript/active.js"></script>

    <title>Jackpot [{$settingID}] {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ul class="breadcrumb">
    <li><a href="/admin/jackpot">Jackpot</a></li>
    <li class="active">Active #<span class="jackpotID">0</span></li>
</ul>

<div class="panel panel-default">
    <div class="panel-heading">Jackpot #<span class="jackpotID">0</span></div>
    <div class="panel-body">
        <table class="table table-bordered">
            <tr>
                <td>Value</td>
                <td id="stat-value">0.00</td>
            </tr>
            <tr>
                <td>Timer</td>
                <td id="stat-timer">0s</td>
            </tr>
            <tr>
                <td>Items</td>
                <td id="stat-itemcount">0 / 0</td>
            </tr>
            <tr>
                <td>Players</td>
                <td id="stat-playercount">0</td>
            </tr>
        </table>

        <table class="table table-bordered">
            <tr>
                <td>Winner</td>
                <td id="stat-winner"></td>
            </tr>
            <tr>
                <td>Winning Percentage</td>
                <td id="stat-percentage"></td>
            </tr>
            <tr>
                <td>Secret</td>
                <td id="stat-secret"></td>
            </tr>
            <tr>
                <td>Roundhash</td>
                <td id="stat-roundhash"></td>
            </tr>
        </table>

        <div id="jackpot-players">

        </div>

        <div id="sample_participant" style="display:none;">
            <div class="col-md-3">
                <div class="center-block">
                    <img src="#" class="participant-image img img-thumbnail" />
                </div>
            </div>
            <div class="col-md-9">
                <table class="table table-bordered">
                    <tr>
                        <td>Username</td>
                        <td class="participant-username"></td>
                    </tr>
                    <tr>
                        <td>Deposit</td>
                        <td class="participant-deposited"></td>
                    </tr>
                    <tr>
                        <td>Winner Chance</td>
                        <td class="participant-chance"></td>
                    </tr>
                    <tr>
                        <td>Ticket Start</td>
                        <td class="participant-ticket-start"></td>
                    </tr>
                    <tr>
                        <td>Ticket End</td>
                        <td class="participant-ticket-end"></td>
                    </tr>
                    <tr>
                        <td>Joined</td>
                        <td class="participant-joined"></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-12">
                <br>
                <div class="participant-items"></div>
            </div>
        </div>

    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>