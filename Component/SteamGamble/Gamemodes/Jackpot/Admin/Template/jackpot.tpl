<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css" />

    <title>Jackpot #{$jackpot.jackpotID} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ul class="breadcrumb">
    <li><a href="/admin/jackpot">Jackpot</a></li>
    <li><a href="/admin/jackpot/history">History</a></li>
    <li class="active">#{$jackpot.jackpotID}</li>
</ul>

<div class="panel panel-default">
    <div class="panel-body">
        {if $jackpot.withdrawID}
            {renderWithdrawStatus($jackpot.withdrawID)}
        {/if}

        <table class="table table-bordered">
            <tr>
                <td>Jackpot ID</td>
                <td>{$jackpot.jackpotID}</td>
            </tr>
            <tr>
                <td>Setting ID</td>
                <td><a href="/admin/jackpot/editsetting/{$jackpot.settingID}">#{$jackpot.settingID}</a></td>
            </tr>
            <tr>
                <td>Value</td>
                <td>${number_format(divide($jackpot.tickets, 100))}</td>
            </tr>
            <tr>
                <td>Created</td>
                <td>{datetimeformat($jackpot.created)}</td>
            </tr>
            <tr>
                <td>Started</td>
                <td>{datetimeformat($jackpot.started)}</td>
            </tr>
            <tr>
                <td>Finished</td>
                <td>{datetimeformat($jackpot.finished)}</td>
            </tr>
        </table>

        <table class="table table-bordered">
            <tr>
                <td>Min Deposit Value</td>
                <td>${number_format(divide($jackpot.min_deposit_value, 100))}</td>
            </tr>
            <tr>
                <td>Max Deposit Value</td>
                <td>
                    {if $jackpot.max_deposit_value == 0}
                        NO LIMIT
                    {else}
                        ${number_format(divide($jackpot.max_deposit_value, 100))}
                    {/if}
                </td>
            </tr>
            <tr>
                <td>Max Deposit Items</td>
                <td>{$jackpot.max_deposit_items}</td>
            </tr>
            <tr>
                <td>Multiple Deposit</td>
                <td>{$jackpot.multiple_deposit}</td>
            </tr>
            <tr>
                <td>Gametime</td>
                <td>{$jackpot.gametime}</td>
            </tr>
            <tr>
                <td>Max Items</td>
                <td>{$jackpot.max_items}</td>
            </tr>
            <tr>
                <td>Min Players</td>
                <td>{$jackpot.min_players}</td>
            </tr>
            <tr>
                <td>Password</td>
                <td>{$jackpot.password}</td>
            </tr>
        </table>

        <table class="table table-bordered">
            <tr>
                <td>Winner</td>
                <td><a href="/admin/user/view/{$jackpot.winner.user.userID}">{$jackpot.winner.user.personaname}</a></td>
            </tr>
            <tr>
                <td>Hash</td>
                <td>{$jackpot.hash}</td>
            </tr>
            <tr>
                <td>Percentage</td>
                <td>{$jackpot.percentage}</td>
            </tr>
            <tr>
                <td>Secret</td>
                <td>{$jackpot.secret}</td>
            </tr>
            <tr>
                <td>Winning Ticket</td>
                <td>{$jackpot.winner.ticket}</td>
            </tr>
        </table>

        {foreach $jackpot.participants as $participant}
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-3">
                        <div class="center-block"><img src="{$participant.user.avatarfull}" class="img img-thumbnail"></div>
                    </div>
                    <div class="col-md-9">
                        <table class="table table-bordered">
                            <tr class="{if $participant.user.userID == $jackpot.winner.user.userID}success{/if}">
                                <td>Username</td>
                                <td><a href="/admin/user/view/{$participant.user.userID}">{$participant.user.personaname}</a></td>
                            </tr>
                            <tr>
                                <td>Deposited</td>
                                <td>${number_format(divide($participant.tickets, 100))}</td>
                            </tr>
                            <tr>
                                <td>Winning Probability</td>
                                <td>{number_format($participant.chance)}%</td>
                            </tr>
                            <tr>
                                <td>Ticket Start</td>
                                <td>{$participant.ticket_start}</td>
                            </tr>
                            <tr>
                                <td>Ticket End</td>
                                <td>{$participant.ticket_end}</td>
                            </tr>
                            <tr>
                                <td>Joined</td>
                                <td>{datetimeformat($participant.joined)}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <br>
                        {foreach $participant.items as $item}
                            {renderItem($item)}
                        {/foreach}
                    </div>
                </div>
            </div>
        {/foreach}
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>