<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Gamemodes\Jackpot;

class Game extends \Component\SteamGamble\Parent\Game
{
    private $cache = [];

    public function start($gametime, $min_deposit_value, $max_deposit_value, $max_deposit_items, $max_items, $min_players, $multiple_deposit = true, $password = NULL, $settingID = NULL)
    {
        $servertoken = str_random(16, "0123456789abcdef");
        $percentage = mt_rand(1, 1000000000) / 1000000000;
        $hash = sha1($servertoken.":".$percentage);

        $jackpotID = $this->gamble->app()->database()->insert("jackpot")->set([
            "secret" => $servertoken, "percentage" => $percentage, "hash" => $hash, "created" => time(),
            "status" => 0, "gametime" => $gametime, "min_deposit_value" => $min_deposit_value,
            "max_deposit_value" => $max_deposit_value,
            "max_deposit_items" => $max_deposit_items, "multiple_deposit" => $multiple_deposit ? 1 : 0,
            "password" => $password,
            "max_items" => $max_items, "min_players" => $min_players, "settingID" => $settingID
        ])->getID();

        return [
            "jackpotID" => $jackpotID, "hash" => $hash, "gametime" => $gametime,
            "min_deposit_value" => $min_deposit_value,
            "max_deposit_value" => $max_deposit_value, "password" => $password, "max_items" => $max_items,
            "min_players" => $min_players,
            "max_deposit_items" => $max_deposit_items, "multiple_deposit" => $multiple_deposit ? true : false
        ];
    }

    /**
     * @param int $settingID
     *
     * @return array|false
     * @throws \App\Database\Exception\ParseException
     */
    public function startPublicJackpot($settingID)
    {
        $setting = $this->gamble->app()->database()->select("jackpot_setting")->where(["id" => $settingID])->fetchRow();

        if(!is_array($setting))
        {
            return false;
        }

        return $this->start(
            $setting["gametime"], $setting["min_deposit_value"], $setting["max_deposit_value"], $setting["max_deposit_items"],
            $setting["max_items"], $setting["min_players"], $setting["multiple_deposit"], NULL, $settingID
        );
    }

    /**
     * @param int $jackpotID
     *
     * @return array
     * @throws \App\Database\Exception\ParseException
     */
    public function end($jackpotID)
    {
        $jackpot = $this->getJackpotRow($jackpotID);
        if(!is_array($jackpot))
        {
            return [];
        }

        $winner = $this->getWinnerParticipant($jackpotID, $percentage, $ticket, $totaltickets);
        if(count($winner) == 0)
        {
            return [];
        }
        $winner_user = $this->gamble->app()->database()->select("user")->where(["id" => $winner["userID"]])->fetchRow();

        if($jackpot["status"] == 0)
        {
            $this->gamble->app()->database()->update("jackpot")->set([
                "status" => 1, "finished" => time(), "winnerID" => $winner["userID"], "tickets" => $totaltickets
            ])->where(["id" => $jackpotID]);

            //fetch all items in jackpot
            $items = $this->gamble->app()->database()->select("jackpot_item")->where(["jackpotID" => $jackpotID,
                "bonus" => 0])
                ->order("tickets", "desc")->fetchAssoc();


            //this vars we need to calc the fees
            $fees = $this->gamble->app()->setting()->getController("jackpot", "fees") / 100;

            $fees = $fees * $totaltickets;

            $fee_items = [];
            $fee_price = 0;
            $fee_limit = ((0.2 * count($items)) < 5) ? 5 : (((0.2 * count($items)) > 15) ? 15 : 0.2 * count($items));

            $fee_min_price = 0;
            if($this->app->database()->select("fee")->where(["status" => 0, "price[<=]" => 15])->numRows() > 300)
            {
                $fee_min_price = 15;
            }

            //update fees
            foreach($items as $item)
            {
                //item is needed for fees && max fee items are reached && item min value 0.05 && the winner didnt deposit the item
                if(($item["tickets"] + $fee_price) <= $fees && count($fee_items) < $fee_limit && $item["tickets"] > $fee_min_price)
                {
                    $fee_price += $item["tickets"];
                    $fee_items[] = $item;
                    $this->gamble->app()->database()->update("jackpot_item")->set(["fee" => 1])
                        ->where(["id" => $item["id"]]);

                    $this->gamble->app()->database()->insert("fee")->set([
                        "botID" => $item["botID"], "appID" => $item["appID"], "contextID" => $item["contextID"],
                        "market_name" => $item["market_name"], "assetID" => $item["assetID"],
                        "amount" => $item["amount"],
                        "status" => 0, "dateline" => time(), "price" => $item["tickets"]
                    ]);
                }
            }
        }

        return [
            "jackpotID" => $jackpotID, "secret" => $jackpot["secret"], "percentage" => $percentage,
            "ticket" => $ticket, "is_trading" => $winner["is_trading"] ? true : false,
            "user" => $this->gamble->games()->getUserData($winner["userID"])
        ];
    }

    /**
     * @param int $jackpotID
     * @param int $percentage
     * @param int $winningticket
     * @param int $totaltickets
     *
     * @return array
     * @throws \App\Database\Exception\ParseException
     */
    public function getWinnerParticipant($jackpotID, &$percentage = 0, &$winningticket = 0, &$totaltickets = 0)
    {
        $jackpot = $this->getJackpotRow($jackpotID);
        if(!is_array($jackpot))
        {
            return [];
        }

        $winner = [];
        $percentage = $jackpot["percentage"];
        $winningpercentage = $percentage;

        $participants = $this->gamble->app()->database()->select("jackpot_participant")
            ->where(["jackpotID" => $jackpot["id"]])->order("id", "asc")->fetchAssoc();

        $totaltickets = 0;
        foreach($participants as $participant)
        {
            $totaltickets += $participant["tickets"];
        }
        $winningticket = ceil($winningpercentage * $totaltickets);

        $endticket = 0;
        foreach($participants as $participant)
        {
            $startticket = $endticket + 1;
            $endticket += $participant["tickets"];
            if($startticket <= $winningticket && $endticket >= $winningticket)
            {
                $winner = $participant;
                break;
            }
        }

        return $winner;
    }

    /**
     * @param $jackpotID
     *
     * @return array
     * @throws \App\Database\Exception\ParseException
     */
    public function getJackpotRow($jackpotID)
    {
        if(!is_array($this->cache[$jackpotID]))
        {
            $this->cache[$jackpotID] = $this->gamble->app()->database()->select("jackpot")->where(["id" => $jackpotID])
                ->fetchRow();
        }

        return $this->cache[$jackpotID];
    }

    /**
     * @return array
     * @throws \App\Database\Exception\ParseException
     */
    public function getActivePublicJackpots()
    {
        $settings = $this->gamble->app()->database()->select("jackpot_setting")->fetchAssoc();
        $data = [];

        foreach($settings as $setting)
        {
            $publicIDs = $this->gamble->app()->database()->select("jackpot")->where([
                "password" => NULL, "winnerID" => NULL, "status" => 0, "settingID" => $setting["id"]
            ])->order("created", "desc")->limit(0, 1)->fetchAssoc();

            if(count($publicIDs) == 0)
            {
                $d = $this->startPublicJackpot($setting["id"]);
                $jackpotID = $d["jackpotID"];
            }
            else
            {
                $jackpotID = $publicIDs[0]["id"];
            }

            $data[] = $this->getActiveJackpotData($jackpotID);
        }

        return $data;
    }

    /**
     * @param int $jackpotID
     *
     * @return array
     * @throws \App\Database\Exception\ParseException
     */
    public function getActiveJackpotData($jackpotID)
    {
        $jackpot = $this->gamble->app()->database()->select("jackpot")->where(["id" => $jackpotID, "winnerID" => NULL, "status" => 0])
            ->fetchRow();

        if(!is_array($jackpot) || $jackpot["status"] != 0)
        {
            return [];
        }

        $bonus = [];
        $participants = [];
        $pquery = $this->gamble->app()->database()->select("jackpot_participant")->column([
            "user.steamID" => "steamID", "jackpot_participant.userID" => "userID",
            "jackpot_participant.id" => "participantID", "jackpot_participant.tickets" => "tickets",
            "jackpot_participant.joined" => "joined"
        ])->leftJoin("user", ["user.id[=]jackpot_participant.userID"])
            ->where(["jackpot_participant.jackpotID" => $jackpotID])->order("jackpot_participant.joined", "asc")
            ->fetchAssoc();

        foreach($pquery as $prow)
        {
            $participants[$prow["participantID"]] = [
                "user" => $this->gamble->games()->getUserData($prow["userID"]),
                "items" => [], "tickets" => $prow["tickets"], "joined" => $prow["joined"]
            ];
        }

        $iquery = $this->gamble->app()->database()->select("jackpot_item")->column([
            "appID" => "appID", "market_name" => "market_name", "id" => "id", "participantID" => "participantID",
            "amount" => "amount", "tickets" => "tickets", "bonus" => "bonus", "data" => "data"
        ])->where(["jackpotID" => $jackpotID])->fetchAssoc();

        $items = [];
        foreach($iquery as $irow)
        {
            $data = @json_decode($irow["data"], true);

            $items[$irow["id"]] = array_merge($irow, ["data" => is_array($data) ? $data : []]);

            if($irow["bonus"] == 1)
            {
                $bonus[] = &$items[$irow["id"]];
            }
            else
            {
                $participants[$irow["participantID"]]["items"][] = &$items[$irow["id"]];
            }
        }

        $this->gamble->steam()->item()->filling($items);

        $lwinner = $this->gamble->app()->database()->select("jackpot")->column([
            "jackpot.winnerID" => "userID", "user.steamID" => "steamID"
        ])->leftJoin("user", ["jackpot.winnerID[=]user.id"])->where([
            "jackpot.id[<]" => $jackpotID, "winnerID[!]" => NULL
        ])->order("jackpot.id", "desc")->limit(0, 1)->fetchRow();

        $lastwinner = NULL;
        if(is_array($lwinner))
        {
            $lastwinner = $this->gamble->games()->getUserData($lwinner["userID"]);
        }

        return [
            "jackpotID" => $jackpotID, "hash" => $jackpot["hash"], "gametime" => $jackpot["gametime"],
            "participants" => array_values($participants), "min_deposit_value" => $jackpot["min_deposit_value"],
            "max_deposit_value" => $jackpot["max_deposit_value"], "password" => $jackpot["password"],
            "max_deposit_items" => $jackpot["max_deposit_items"], "multiple_deposit" => $jackpot["multiple_deposit"],
            "max_items" => $jackpot["max_items"], "min_players" => $jackpot["min_players"],
            "bonus" => $bonus, "started" => $jackpot["started"], "lastwinner" => $lastwinner,
            "settingID" => $jackpot["settingID"]
        ];
    }

}