<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Gamemodes\Jackpot;

use App\HTTP\Request;

class Websocket extends \Component\SteamGamble\Parent\Websocket
{
    public function startpublicjackpot(Request $request, $settingID)
    {
        /**
         * @var Game $jackpot
         */
        $jackpot = $this->gamble->games()->gamemode("jackpot");

        $response = $jackpot->startPublicJackpot($settingID);

        if($response === false)
        {
            return ["success" => false];
        }
        else
        {
            return ["success" => true, "response" => $response];
        }
    }

    public function endjackpot(Request $request, $jackpotID)
    {
        $response = ["success" => false];
        /**
         * @var Game $jackpot
         */
        $jackpot = $this->gamble->games()->gamemode("jackpot");
        $winner = $jackpot->end($jackpotID);

        if(isset($winner["user"]["userID"]))
        {
            $response = ["success" => true, "response" => $winner];
        }

        return $response;
    }

    public function activejackpot()
    {
        /**
         * @var Game $jackpot
         */
        $jackpot = $this->gamble->games()->gamemode("jackpot");
        $data = $jackpot->getActivePublicJackpots();

        return ["success" => true, "response" => ["public" => $data, "private" => []]];
    }

    public function activegames()
    {
        return ["success" => true, "response" => $this->app->database()->select("jackpot")->where(["status" => 0])->fetchAssoc()];
    }

    public function end(Request $request, $jackpotID)
    {
        /**
         * @var Game $jackpot
         */
        $jackpot = $this->gamble->games()->gamemode("jackpot");
        $data = $jackpot->end($jackpotID);

        return ["success" => true, "response" => $data];
    }
}