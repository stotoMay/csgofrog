<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Gamemodes\Jackpot;

class Statistic extends \Component\SteamGamble\Parent\Statistic
{
    public function getGameCount($time = 0)
    {
        $count = $this->gamble->app()->database()->select("jackpot")
            ->where(["finished[>=]" => $time, "status" => 1])->numRows();

        return $count;
    }

    public function getBiggestValue($time = 0)
    {
        $jackpot = $this->gamble->app()->database()->select("jackpot")
            ->where(["status" => 1, "password" => NULL, "finished[>=]" => $time])
            ->order("tickets", "desc")->limit(0, 1)->fetchRow();

        return $jackpot["tickets"] / 100;
    }

    public function getJackpotHistory($page, $size = 50, $userID = NULL, $fetchitems = false)
    {
        if(isset($userID))
        {
            $jackpotlist = $this->app->database()->select("jackpot_participant")
                ->column(["jackpotID", "settingID", "jackpot.created", "started", "finished", "winnerID", "jackpot.tickets"])
                ->leftJoin("jackpot", ["jackpot.id[=]jackpot_participant.jackpotID"])
                ->where(["jackpot.status" => 1, "jackpot_participant.userID" => $userID])
                ->group("jackpot.id")
                ->order("finished", "desc")->limit(($page-1)*$size, $size)
                ->fetchAssoc();
        }
        else
        {
            $jackpotlist = $this->app->database()->select("jackpot")->where(["status" => 1])
                ->column(["id" => "jackpotID", "settingID", "created", "started", "finished", "winnerID", "tickets"])
                ->order("finished", "desc")->limit(($page-1)*$size, $size)
                ->fetchAssoc();
        }

        $jackpotIDs = [];
        $userIDs = [];

        $jackpots = [];
        $users = [];

        foreach($jackpotlist as $jackpot)
        {
            $jackpot["participants"] = [];
            $jackpot["items"] = [];

            $jackpotIDs[] = $jackpot["jackpotID"];
            $jackpots[$jackpot["jackpotID"]] = $jackpot;
        }

        if(count($jackpotIDs) == 0)
        {
            return [];
        }

        $participants = $this->app->database()->select("jackpot_participant")
            ->column(["userID", "steamID", "tickets", "jackpotID", "jackpot_participant.id"])
            ->leftJoin("user", ["user.id[=]jackpot_participant.userID"])
            ->where(["jackpotID" => $jackpotIDs])->fetchAssoc();

        foreach($participants as $participant)
        {
            if(!is_int(array_search($participant["userID"], $userIDs)))
            {
                $userIDs[] = $participant["userID"];
                $users[$participant["userID"]] = array_merge([
                    "userID" => $participant["userID"]
                ], $this->steam->api()->user()->getPlayerSummaries($participant["steamID"])[0]);
            }

            $jackpots[$participant["jackpotID"]]["participants"][$participant["id"]] = ["tickets" => $participant["tickets"], "items" => [], "user" => $users[$participant["userID"]]];
        }

        if($fetchitems === true)
        {
            $items = $this->app->database()->select("jackpot_item")->where(["jackpotID" => $jackpotIDs])->fetchAssoc();
            $this->steam->item()->filling($items);

            usort($items, function($a, $b) {
                return $b["tickets"] - $a["tickets"];
            });

            foreach($items as $item)
            {
                $jackpots[$item["jackpotID"]]["participants"][$item["participantID"]]["items"][] = $item;
            }
        }

        foreach($jackpots as $key => $jackpot)
        {
            $jackpots[$key]["winner"] = $users[$jackpot["winnerID"]];
            $jackpots[$key]["participants"] = array_values($jackpots[$key]["participants"]);
        }

        return $jackpots;
    }

    public function getJackpot($jackpotID)
    {
        $jackpot = $this->app->database()->select("jackpot")->where(["id" => $jackpotID, "status" => 1])->fetchRow();

        if(!is_array($jackpot))
        {
            return [];
        }

        $participants = $this->app->database()->select("jackpot_participant")
            ->column(["userID", "steamID", "tickets", "jackpot_participant.joined", "is_trading", "jackpot_participant.id"])
            ->leftJoin("user", ["user.id[=]jackpot_participant.userID"])
            ->where(["jackpotID" => $jackpotID])->fetchAssoc();

        $winnertickets = 0;
        $totaltickets = 0;
        foreach($participants as $participant)
        {
            $jackpot["participants"][$participant["id"]] = [
                "user" => array_merge(["userID" => $participant["userID"]], $this->gamble->steam()->api()->user()->getPlayerSummaries($participant["steamID"])[0]),
                "items" => [], "tickets" => $participant["tickets"], "chance" => round(($participant["tickets"] / $jackpot["tickets"]) * 100, 2),
                "ticket_start" => $totaltickets + 1, "ticket_end" => $totaltickets + $participant["tickets"], "joined" => $participant["joined"]
            ];
            $totaltickets += $participant["tickets"];

            if($participant["userID"] == $jackpot["winnerID"])
            {
                $winnertickets += $participant["tickets"];
                $jackpot["winner"] = $jackpot["participants"][$participant["id"]];
            }
        }

        $jackpot["winner"]["chance"] = $winnertickets / $jackpot["tickets"];
        $jackpot["winner"]["ticket"] = ceil($jackpot["percentage"] * $jackpot["tickets"]);

        $items = $this->app->database()->select("jackpot_item")->where(["jackpotID" => $jackpotID])->fetchAssoc();
        $this->steam->item()->filling($items);

        usort($items, function($a, $b) {
            return $a["tickets"] - $b["tickets"];
        });

        $bonus = [];
        foreach($items as $item)
        {
            $item["price"] = $item["tickets"] / 100;

            if($item["bonus"] == 1)
            {
                $bonus[] = $item;
            }
            else
            {
                $jackpot["participants"][$item["participantID"]]["items"][] = $item;
            }
        }

        $jackpot["participants"] = array_values($jackpot["participants"]);
        $jackpot["bonus"] = $bonus;
        $jackpot["itemcount"] = count($items);
        $jackpot["jackpotID"] = $jackpot["id"];

        return $jackpot;
    }

    public function getUserProfitGraph($userID)
    {
        $games = $this->app->database()->select("jackpot_participant")
            ->column(["jackpot_participant.tickets" => ["function" => "sum", "alias" => "deposited"], "jackpot.tickets", "jackpot.finished", "jackpot.winnerID"])
            ->leftJoin("jackpot", ["jackpot.id[=]jackpot_participant.jackpotID"])
            ->where(["status" => 1, "jackpot_participant.userID" => $userID])
            ->group("jackpotID")->order("finished", "asc")->fetchAssoc();

        $result = [];

        foreach($games as $game)
        {
            if($game["winnerID"] == $userID)
            {
                $result[] = [
                    "value" => ($game["tickets"] - $game["deposited"]) / 100,
                    "dateline" => $game["finished"]
                ];
            }
            else
            {
                $result[] = [
                    "value" => (-1*$game["deposited"])/100,
                    "dateline" => $game["finished"]
                ];
            }
        }

        return $result;
    }

    public function getUserLuckyGraph($userID)
    {
        $games = $this->app->database()->select("jackpot_participant")
            ->column(["jackpot_participant.tickets" => ["function" => "sum", "alias" => "deposited"], "jackpot.tickets", "jackpot.finished", "jackpot.winnerID"])
            ->leftJoin("jackpot", ["jackpot.id[=]jackpot_participant.jackpotID"])
            ->where(["status" => 1, "jackpot_participant.userID" => $userID])
            ->group("jackpotID")->order("finished", "asc")->fetchAssoc();

        $result = [];

        foreach($games as $game)
        {
            if($game["winnerID"] == $userID)
            {
                $result[] = [
                    "value" => 1-($game["deposited"]/$game["tickets"]),
                    "dateline" => $game["finished"]
                ];
            }
            else
            {
                $result[] = [
                    "value" => -1*($game["deposited"]/$game["tickets"]),
                    "dateline" => $game["finished"]
                ];
            }
        }

        return $result;
    }
}