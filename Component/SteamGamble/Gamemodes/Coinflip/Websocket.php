<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Gamemodes\Coinflip;

use App\HTTP\Request;

class Websocket extends \Component\SteamGamble\Parent\Websocket
{
    public function startcoinflip()
    {
        /**
         * @var Game $coinflip
         */
        $coinflip = $this->gamble->games()->gamemode("coinflip");

        return ["success" => true, "response" => $coinflip->start()];
    }

    public function endcoinflip(Request $request, $coinflipID)
    {
        $response = ["success" => false];
        /**
         * @var Game $coinflip
         */
        $coinflip = $this->gamble->games()->gamemode("coinflip");
        $winner = $coinflip->end($coinflipID);

        if(isset($winner["user"]["userID"]))
        {
            $response = ["success" => true, "response" => $winner];
        }

        return $response;
    }

    public function activecoinflips()
    {
        /**
         * @var Game $coinflip
         */
        $coinflip = $this->gamble->games()->gamemode("coinflip");
        $data = $coinflip->getActiveGames();

        return ["success" => true, "response" => $data];
    }

    public function activegames()
    {
        return ["success" => true,
            "response" => $this->app->database()->select("coinflip")->where(["status" => 0])->fetchAssoc()];
    }

    public function end(Request $request, $coinflipID)
    {
        /**
         * @var Game $coinflip
         */
        $coinflip = $this->gamble->games()->gamemode("coinflip");
        $data = $coinflip->end($coinflipID);

        return ["success" => true, "response" => $data];
    }
}