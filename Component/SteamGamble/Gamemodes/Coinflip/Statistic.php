<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Gamemodes\Coinflip;

class Statistic extends \Component\SteamGamble\Parent\Statistic
{
    public function getGameCount($time = 0)
    {
        $count = $this->gamble->app()->database()->select("coinflip")->where(["finished[>=]" => $time, "status" => 1])->numRows();

        return $count;
    }

    public function getBiggestValue($time = 0)
    {
        $jackpot = $this->gamble->app()->database()->select("coinflip")
            ->where(["status" => 1,"finished[>=]" => $time])
            ->order("tickets_1", "desc")->limit(0, 1)->fetchRow();

        return ($jackpot["tickets_1"] + $jackpot["tickets_2"]) / 100;
    }

    public function getCoinflipHistory($page, $size = 50, $userID = NULL, $fetchitems = false)
    {
        $query = $this->app->database()->select("coinflip")->order("finished", "desc")->limit(($page-1)*$size, $size);

        $condition = ["status" => 1];
        if(is_numeric($userID))
        {
            $condition["OR"] = ["player_1" => $userID, "player_2" => $userID];
        }
        $query->where($condition);

        $coinflips = $query->fetchAssoc();

        $coinflipIDs = [];
        $userIDs = [];

        foreach($coinflips as $coinflip)
        {
            $userIDs[] = $coinflip["player_1"];
            $userIDs[] = $coinflip["player_2"];
            $coinflipIDs[] = $coinflip["id"];
        }

        if(count($coinflipIDs) == 0)
        {
            return [];
        }

        $userIDs = array_unique($userIDs);

        $userlist = $this->app->database()->select("user")->column(["id", "steamID"])->where(["id" => $userIDs])->fetchAssoc();
        $users = [];
        foreach($userlist as $user)
        {
            $users[$user["id"]] = array_merge(["userID" => $user["id"]], $this->steam->api()->user()->getPlayerSummaries($user["steamID"])[0]);
        }

        $items = [];
        if($fetchitems === true)
        {
            $itemlist = $this->app->database()->select("coinflip_item")->where(["coinflipID" => $coinflipIDs])->fetchAssoc();
            $this->steam->item()->filling($itemlist);

            foreach($coinflipIDs as $coinflipID)
            {
                $items[$coinflipID] = [];
            }

            foreach($itemlist as $item)
            {
                $items[$item["coinflipID"]][] = $item;
            }

            foreach($items as $coinflipID => $itemlist)
            {
                usort($items[$coinflipID], function($a, $b) {
                    return $b["tickets"] - $a["tickets"];
                });
            }
        }

        $result = [];
        foreach($coinflips as $coinflip)
        {
            $data = $coinflip;

            $data["winner"] = $users[$coinflip["winnerID"]];
            $data["coinflipID"] = $data["id"];

            $data["player_1"] = ["tickets" => $coinflip["tickets_1"], "items" => [], "user" => $users[$coinflip["player_1"]]];
            $data["player_2"] = ["tickets" => $coinflip["tickets_2"], "items" => [], "user" => $users[$coinflip["player_2"]]];
            $data["tickets"] = $data["tickets_1"] + $data["tickets_2"];
            unset($data["tickets_1"], $data["tickets_2"]);

            if($fetchitems === true)
            {
                foreach($items[$coinflip["id"]] as $item)
                {
                    $item["price"] = $item["tickets"] / 100;

                    if($coinflip["player_1"] == $item["userID"])
                    {
                        $data["player_1"]["items"][] = $item;
                    }
                    elseif($coinflip["player_2"] == $item["userID"])
                    {
                        $data["player_2"]["items"][] = $item;
                    }
                }
            }

            $result[] = $data;
        }

        return $result;
    }

    public function getCoinflip($coinflipID)
    {
        $coinflip = $this->app->database()->select("coinflip")->where(["id" => $coinflipID, "status" => 1])->fetchRow();

        if(!is_array($coinflip))
        {
            return false;
        }

        $coinflip["coinflipID"] = $coinflip["id"];

        $items = $this->app->database()->select("coinflip_item")->where(["coinflipID" => $coinflipID])->fetchAssoc();
        $this->steam->item()->filling($items);

        $userlist = $this->app->database()->select("user")->column(["id", "steamID"])->where(["id" => [$coinflip["player_1"], $coinflip["player_2"]]])->fetchAssoc();
        $users = [];
        foreach($userlist as $user)
        {
            $users[$user["id"]] = array_merge(["userID" => $user["id"]], $this->steam->api()->user()->getPlayerSummaries($user["steamID"])[0]);
        }

        $coinflip["player_1"] = [
            "tickets" => $coinflip["tickets_1"], "items" => [], "user" => $users[$coinflip["player_1"]],
            "ticket_start" => 1, "ticket_end" => $coinflip["tickets_1"]
        ];
        $coinflip["player_2"] = [
            "tickets" => $coinflip["tickets_2"], "items" => [], "user" => $users[$coinflip["player_2"]],
            "ticket_start" => $coinflip["tickets_1"] + 1, "ticket_end" => $coinflip["tickets_1"] + $coinflip["tickets_2"]
        ];

        foreach($items as $item)
        {
            $item["price"] = $item["tickets"] / 100;

            if($coinflip["player_1"]["user"]["userID"] == $item["userID"])
            {
                $coinflip["player_1"]["items"][] = $item;
            }
            elseif($coinflip["player_2"]["user"]["userID"] == $item["userID"])
            {
                $coinflip["player_2"]["items"][] = $item;
            }
        }

        $coinflip["winner"] = [
            "user" => $users[$coinflip["winnerID"]],
            "ticket" => ceil($coinflip["percentage"]*($coinflip["tickets_1"]+$coinflip["tickets_2"]))
        ];
        $coinflip["tickets"] = $coinflip["tickets_1"] + $coinflip["tickets_2"];

        unset($coinflip["tickets_1"], $coinflip["tickets_2"], $coinflip["winnerID"]);

        return $coinflip;
    }

    public function getUserProfitGraph($userID)
    {
        $games = $this->gamble->app()->database()->select("coinflip")
            ->column(["finished", "winnerID", "player_1", "tickets_1", "tickets_2"])
            ->where(["status" => 1, "OR" => ["player_1" => $userID, "player_2" => $userID]])
            ->order("finished", "asc")->fetchAssoc();

        $result = [];

        foreach($games as $game)
        {
            if($game["winnerID"] == $userID)
            {
                $result[] = [
                    "value" => ($game["player_1"] == $userID ? $game["tickets_2"] : $game["tickets_1"]) / 100,
                    "dateline" => $game["finished"]
                ];
            }
            else
            {
                $result[] = [
                    "value" => ($game["player_1"] == $userID ? -1 * $game["tickets_1"] : -1 * $game["tickets_2"]) / 100,
                    "dateline" => $game["finished"]
                ];
            }
        }

        return $result;
    }

    public function getUserLuckyGraph($userID)
    {
        $games = $this->gamble->app()->database()->select("coinflip")
            ->column(["id", "finished", "winnerID", "player_1", "tickets_1", "tickets_2"])
            ->where(["status" => 1, "OR" => ["player_1" => $userID, "player_2" => $userID]])
            ->order("finished", "asc")->fetchAssoc();

        $result = [];

        foreach($games as $game)
        {
            if($game["winnerID"] == $userID)
            {
                $result[] = [
                    "value" => ($game["player_1"] == $userID ? $game["tickets_2"] : $game["tickets_1"]) / ($game["tickets_1"] + $game["tickets_2"]),
                    "dateline" => $game["finished"]
                ];
            }
            else
            {
                $result[] = [
                    "value" => ($game["player_1"] == $userID ? -1 * $game["tickets_1"] : -1 * $game["tickets_2"]) / ($game["tickets_1"] + $game["tickets_2"]),
                    "dateline" => $game["finished"]
                ];
            }
        }

        return $result;
    }
}