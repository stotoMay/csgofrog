<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Gamemodes\Coinflip;

class Game extends \Component\SteamGamble\Parent\Game
{
    private $cache;

    public function start()
    {
        $servertoken = str_random(16, "0123456789abcdef");
        $percentage = mt_rand(1, 1000000000) / 1000000000;
        $hash = sha1($servertoken.":".$percentage);

        $coinflipID = $this->gamble->app()->database()->insert("coinflip")->set([
            "secret" => $servertoken, "percentage" => $percentage, "hash" => $hash, "created" => time(),
            "status" => 0
        ])->getID();

        return [
            "coinflipID" => $coinflipID, "hash" => $hash
        ];
    }

    public function end($coinflipID)
    {
        $coinflip = $this->getCoinflip($coinflipID);

        if(empty($coinflip["player_1"]) || empty($coinflip["player_2"]))
        {
            return false;
        }

        $winning_ticket = $coinflip["percentage"] * ($coinflip["tickets_1"] + $coinflip["tickets_2"]);

        if($winning_ticket <= $coinflip["tickets_1"])
        {
            $winnerID = $coinflip["player_1"];
        }
        else
        {
            $winnerID = $coinflip["player_2"];
        }

        $fee_percentage = $this->app->setting()->get("fees", "controller", "coinflip") / 100;
        $fees = $fee_percentage * ($coinflip["tickets_1"] + $coinflip["tickets_2"]);

        $items = $this->app->database()->select("coinflip_item")->where(["coinflipID" => $coinflipID])
            ->order("tickets", "desc")->fetchAssoc();

        $fee_items = [];
        $fee_price = 0;
        $fee_limit = ((0.2 * count($items)) < 5) ? 5 : (((0.2 * count($items)) > 15) ? 15 : 0.2 * count($items));

        $fee_min_price = 0;
        if($this->app->database()->select("fee")->where(["status" => 0, "price[<=]" => 15])->numRows() > 300)
        {
            $fee_min_price = 15;
        }

        foreach($items as $item)
        {
            if(($item["tickets"] + $fee_price) <= $fees && count($fee_items) < $fee_limit && $item["tickets"] > $fee_min_price)
            {
                $fee_price += $item["tickets"];
                $fee_items[] = $item;
                $this->gamble->app()->database()->update("coinflip_item")->set(["fee" => 1])
                    ->where(["id" => $item["id"]]);

                $this->gamble->app()->database()->insert("fee")->set([
                    "botID" => $item["botID"], "appID" => $item["appID"], "contextID" => $item["contextID"],
                    "market_name" => $item["market_name"], "assetID" => $item["assetID"],
                    "amount" => $item["amount"], "price" => $item["tickets"],
                    "status" => 0, "dateline" => time()
                ]);
            }
        }

        $this->app->database()->update("coinflip")->set(["finished" => time(), "winnerID" => $winnerID, "status" => 1])
            ->where(["id" => $coinflipID]);

        return [
            "coinflipID" => $coinflipID, "user" => $this->gamble->games()->getUserData($winnerID),
            "percentage" => $coinflip["percentage"], "secret" => $coinflip["secret"], "finished" => time()
        ];
    }

    public function getActiveGames()
    {
        $this->app->database()->delete("coinflip")->where(["player_1" => NULL, "player_2" => NULL]);
        $coinflips = $this->app->database()->select("coinflip")->where(["finished" => NULL, "status" => 0])->fetchAssoc();

        $result = [];
        foreach($coinflips as $coinflip)
        {
            $player_1 = [];
            $player_2 = [];

            if($coinflip["player_1"])
            {
                $items = $this->app->database()->select("coinflip_item")->where(["coinflipID" => $coinflip["id"],
                    "userID" => $coinflip["player_1"]])->fetchAssoc();
                $this->steam->item()->filling($items);

                foreach($items as $key => $item)
                {
                    $data = @json_decode($item["data"], true);
                    $items[$key]["data"] = is_array($data) ? $data : [];
                }

                $user = $this->app->database()->select("user")->where(["id" => $coinflip["player_1"]])->fetchRow();
                $player_1 = ["tickets" => $coinflip["tickets_1"], "items" => $items, "user" => $this->gamble->games()->getUserData($user["id"])];
            }

            if($coinflip["player_2"])
            {
                $items = $this->app->database()->select("coinflip_item")->where(["coinflipID" => $coinflip["id"],
                    "userID" => $coinflip["player_2"]])->fetchAssoc();
                $this->steam->item()->filling($items);

                foreach($items as $key => $item)
                {
                    $data = @json_decode($item["data"], true);
                    $items[$key]["data"] = is_array($data) ? $data : [];
                }

                $user = $this->app->database()->select("user")->where(["id" => $coinflip["player_2"]])->fetchRow();
                $player_2 = ["tickets" => $coinflip["tickets_2"], "items" => $items,
                    "user" => $this->gamble->games()->getUserData($user["id"])];
            }

            $result[] = [
                "coinflipID" => $coinflip["id"], "player_1" => $player_1, "player_2" => $player_2,
                "tickets" => $coinflip["tickets_1"] + $coinflip["tickets_2"], "hash" => $coinflip["hash"]
            ];
        }

        return $result;
    }

    private function getCoinflip($coinflipID)
    {
        if(!isset($this->cache[(string)$coinflipID]))
        {
            $this->cache[(string)$coinflipID] = $this->app->database()->select("coinflip")
                ->where(["id" => $coinflipID])->fetchRow();
        }

        return $this->cache[(string)$coinflipID];
    }
}