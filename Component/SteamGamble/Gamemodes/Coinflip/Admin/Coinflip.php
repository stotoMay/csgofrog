<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Admin\Tools\AjaxStatistics;
use App\Router\Controller;
use Component\Steam\SteamFactory;
use Component\SteamGamble\Classes\Core;
use Component\SteamGamble\GambleFactory;
use Component\SteamGamble\Gamemodes\Coinflip\Admin\Model;
use Component\SteamGamble\Gamemodes\Coinflip\Statistic;

class Coinflip extends Controller
{
    private $admin;
    /**
     * @var Model
     */
    private $model;
    /**
     * @var Statistic
     */
    private $statistic;
    /**
     * @var \Component\Steam\Core $steam
     */
    private $steam;
    /**
     * @var Core $gamble
     */
    private $gamble;

    public static function __info__()
    {
        return [
            "uniqname" => "coinflip", "setting" => [
                "min_deposit_value" => "^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$", "max_deposit_items" => "int",
                "ticket_range" => "^(?:100|[1-9]?[0-9])$", "fees" => "^(?:100|[1-9]?[0-9])$", "max_coinflips" => "int",
            ], "permission" => ["view" => "bool", "stats" => "bool", "percentage" => "bool"], "icon" => "superscript", "tracking" => true,
            "requirement" => [["application", "general", "admin_control_panel"], ["controller", "coinflip", "view"]], "priority" => 5,
        ];
    }

    public function __init__()
    {
        $this->model = $this->getModel("Component/SteamGamble/Gamemodes/Coinflip/Admin/Model");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);

        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));
        $this->theme->registerFolder("Component/SteamGamble/Gamemodes/Coinflip/Admin/Template", "Admin/Coinflip/");

        $this->authAdmin();

        $this->steam = SteamFactory::getSteamCore();
        $this->gamble = GambleFactory::getGambleCore();

        $this->statistic = $this->gamble->statistic()->gamemode("coinflip");
    }

    public function main()
    {
        $template = $this->theme->draw("Admin/Coinflip/main.tpl");

        return response($template, $this->request);
    }

    public function history()
    {
        $template = $this->theme->draw("Admin/Coinflip/history.tpl");

        if($this->request->has("coinflipID", true, v()->integer()))
        {
            $coinflipID = $this->request->input("coinflipID");

            if($this->model->coinflipExists($coinflipID))
            {
                redirect("/admin/coinflip/game/".$coinflipID, $this->request);
            }
            else
            {
                $this->printError($this->language->get(["coinflip", "coinflip_not_found"]));
            }
        }

        $page = $this->request->input("page", 1, false, v()->integer()->min(1));

        $coinflips = $this->model->getCoinflipHistory($this->statistic, $page);

        $template->assignVar("coinflips", $coinflips);
        $template->assignVar("pageurl", "/admin/coinflip/history?page=%p");

        return response($template, $this->request);
    }

    public function stats()
    {
        if($this->getPermission("stats") != 1)
        {
            redirect("/admin/coinflip", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("Admin/Coinflip/stats.tpl");

        return response($template, $this->request);
    }

    public function game($coinflipID)
    {
        $template = $this->theme->draw("Admin/Coinflip/coinflip.tpl");

        if(!$this->model->coinflipExists($coinflipID))
        {
            redirect("/admin/coinflip/history", $this->request)->error($this->language->get(["coinflip",
                "coinflip_not_found"]));
        }

        $coinflip = $this->statistic->getCoinflip($coinflipID);

        $template->assignVar("coinflip", $coinflip);

        return response($template, $this->request);
    }

    public function user($userID)
    {
        if($this->getPermission("view") != 1)
        {
            return "";
        }

        $template = $this->theme->draw("Admin/Coinflip/user.tpl");

        $template->assignVar("userID", $userID);

        return response($template, $this->request);
    }


    public function ajaxhistory($userID)
    {
        $this->request->session()->closeSessionWrite();

        $page = $this->request->input("page", 1, false, v()->integer()->min(1));
        $coinflips = $this->model->getCoinflipHistory($this->statistic, $page, 10, $userID);

        $template = $this->theme->draw("Admin/Coinflip/history_user.tpl");

        $template->assignVar("userID", $userID);
        $template->assignVar("coinflips", $coinflips);

        $json = ["success" => true, "content" => $template->parse(), "current" => $page, "total" => $coinflips["total"]];

        return response(json_encode($json, JSON_HEX_QUOT | JSON_HEX_TAG), $this->request)->contentType("text/json");
    }


    public function gamecountstats()
    {
        if($this->getPermission("stats") == 1)
        {
            $statistic = new AjaxStatistics($this->request, 0);

            $this->request->session()->closeSessionWrite();
            $this->model->getGameCountStats($statistic);

            return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
        }

        return response(json_encode(["success" => false]), $this->request)->contentType("text/json");
    }

    public function gamesizestats()
    {
        if($this->getPermission("stats") == 1)
        {
            $statistic = new AjaxStatistics($this->request, 0);

            $this->request->session()->closeSessionWrite();
            $this->model->getGameSizeStats($statistic);

            return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
        }

        return response(json_encode(["success" => false]), $this->request)->contentType("text/json");
    }


    public function percentage($coinflipID)
    {
        $result = ["success" => false];

        if($this->getPermission("percentage") == 1 && is_numeric($coinflipID) && $this->model->activeCoinflipExists($coinflipID))
        {
            $result = ["success" => true, "percentage" => $this->model->getCoinflipWinningPercentage($coinflipID)];
        }

        return response(json_encode($result), $this->request)->contentType("text/json");
    }
}