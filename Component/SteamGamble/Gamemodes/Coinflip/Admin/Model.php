<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Gamemodes\Coinflip\Admin;

use App\Admin\Tools\AjaxStatistics;
use Component\SteamGamble\Gamemodes\Coinflip\Statistic;

class Model extends \App\Router\Model
{
    public function coinflipExists($coinflipID)
    {
        return $this->database->select("coinflip")->where(["id" => $coinflipID, "status" => 1])->exists();
    }

    public function activeCoinflipExists($coinflipID)
    {
        return $this->database->select("coinflip")->where(["id" => $coinflipID, "status" => 0])->exists();
    }

    public function getCoinflipHistory(Statistic $statistic, $page, $size = 50, $userID = NULL, $fetchitems = false)
    {
        $condition = ["status" => 1];
        if(is_numeric($userID))
        {
            $condition["OR"] = ["player_1" => $userID, "player_2" => $userID];
        }

        $count = $this->database->select("coinflip")->where($condition)->numRows();
        $result = $statistic->getCoinflipHistory($page, $size, $userID, $fetchitems);

        return ["results" => $result, "total" => ceil($count / $size), "current" => $page];
    }

    public function getGameCountStats(AjaxStatistics $statistic)
    {
        $this->database->query("SET time_zone = '".$statistic->getTimezone()."'");

        $data = $this->database->select("coinflip")->column([
            "*" => ["function" => "count", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(finished), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where(["finished[>=]" => $statistic->getStartTime(), "finished[<=]" => $statistic->getStopTime(), "status" => 1])
            ->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->language->get(["steamgamble", "coinflip", "label_gamecount"]));
    }

    public function getGameSizeStats(AjaxStatistics $statistic)
    {
        $this->database->query("SET time_zone = '".$statistic->getTimezone()."'");

        $data = $this->database->select("coinflip")->column([
            ["str" => "(SUM(tickets_1) / COUNT(*)) / 100", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(finished), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where(["finished[>=]" => $statistic->getStartTime(), "finished[<=]" => $statistic->getStopTime(), "status" => 1])
            ->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->language->get(["steamgamble", "coinflip", "label_gamesize"]));
    }

    public function getCoinflipWinningPercentage($coinflipID)
    {
        $coinflip = $this->database->select("coinflip")->where(["id" => $coinflipID])->fetchRow();

        return $coinflip["percentage"];
    }
}