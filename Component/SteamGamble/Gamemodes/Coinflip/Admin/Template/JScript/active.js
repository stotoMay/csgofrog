$(document).ready(function () {
    var websocket = new WebsocketHandler(g_serverip, g_serverauth);

    websocket.addListener("onConnectionClose", function () {
        print_error(g_lang_lostconnection);
    });

    websocket.addListener("onConnectionCreate", function () {
        websocket.sendMessage("set_gamemode", "coinflip")
    });

    websocket.addListener("onCoinflipLoad", function (data) {
        $("#coinflips").html("");

        for(var i = 0; i < data.length; i++)
        {
            var coinflip = data[i];

            createCoinflip(coinflip);

            if(coinflip["player_1"] && coinflip["player_2"])
            {
                addPlayer(coinflip["coinflipID"], coinflip["player_2"], 2)
            }

            if(coinflip["player_joining"])
            {
                addPlayerJoining(coinflip["coinflipID"], coinflip["player_joining"], coinflip["disabletime"])
            }

            if(coinflip["winner"])
            {
                endCoinflip(coinflip["coinflipID"], coinflip["winner"]);
            }
        }
    });

    websocket.addListener("onCoinflipCreate", createCoinflip);
    websocket.addListener("onCoinflipPlayerJoin", function (data) {
        addPlayer(data["coinflipID"], data, $("#coinflip-"+data["coinflipID"]).attr("data-position"));
    });
    websocket.addListener("onCoinflipDisable", function (data) {
        addPlayerJoining(data["coinflipID"], data["user"], data["disabletime"]);
    });
    websocket.addListener("onCoinflipEnable", removePlayerJoining);
    websocket.addListener("onCoinflipEnd", function (data) {
        endCoinflip(data["coinflipID"], data);
    });

    function createCoinflip(data) {
        var position = data["player_1"] === null ? 2 : 1;
        var $coinflip = $("#sample_coinflip").clone().attr("id", "coinflip-"+data["coinflipID"]).attr("data-position", position == 2 ? 1 : 2);

        var $coinflips = $("#coinflips");
        var coinflips = $coinflips.find(".coinflip");

        var i = 0;
        var is_added = false;

        for(i = 0; i < coinflips.length; i++)
        {
            var tickets = parseInt($(coinflips[i]).attr("data-tickets"));

            if(parseInt(data["tickets"]) > tickets)
            {
                is_added = true;
                $(coinflips[i]).before($coinflip);
                break;
            }
        }

        if(is_added === false)
        {
            $coinflips.append($coinflip);
        }

        $coinflip.attr("data-tickets", data["tickets"]);
        $coinflip.find(".coinflip-id").html("#"+data["coinflipID"]);
        $coinflip.find(".coinflip-value").html("$"+(data["tickets"]/100).toFixed(2));
        $coinflip.find(".coinflip-roundhash").html(data["roundhash"]);

        addPlayer(data["coinflipID"], data["player_"+position], position);

        $.ajax({
            "success": function (resp) {
                if(resp["success"] === false)
                {
                    return;
                }

                $coinflip.find(".coinflip-percentage").html(resp["percentage"]);
            },
            "url": "/admin/coinflip/percentage/"+data["coinflipID"],
            "dataType": "json"
        });

        $coinflip.fadeIn();
    }

    function addPlayerJoining(coinflipID, user, disabletime) {
        var $coinflip = $("#coinflip-"+coinflipID);

        var $ahref = $(document.createElement("a")).attr("href", "/admin/user/view/"+user["userID"]).html(user["personaname"]);

        var $span = $(document.createElement("span"));
        var countdown = parseInt(disabletime) + 90 + timefix - Math.round(Date.now() / 1000);

        function timerFunction() {
            if(countdown < 0)
            {
                return;
            }

            $span.html(" ("+countdown+"s)");
            countdown--;

            setTimeout(timerFunction, 1000);
        }

        timerFunction();

        $coinflip.find(".coinflip-joining").html($ahref).append($span);
    }

    function removePlayerJoining(coinflipID) {
        $("#coinflip-"+coinflipID).find(".coinflip-joining").html("");
    }

    function addPlayer(coinflipID, data, position) {
        var $coinflip = $("#coinflip-"+coinflipID);

        var $ahref = $(document.createElement("a")).attr("href", "/admin/user/view/"+data["user"]["userID"]).html(data["user"]["personaname"]);
        var $img = $(document.createElement("img")).attr("src", data["user"]["avatar"]);
        $coinflip.find(".player-" + position + "-username").append($img).append(" ").append($ahref);
        $coinflip.find(".player-" + position + "-value").html("$"+(data["tickets"]/100).toFixed(2));
        
        if(position == 2)
        {
            $coinflip.find(".player-" + position + "-ticket-start").html(parseInt($coinflip.attr("data-tickets")) + 1);
            $coinflip.find(".player-" + position + "-ticket-end").html(parseInt($coinflip.attr("data-tickets")) + parseInt(data["tickets"]));
        }
        else
        {
            $coinflip.find(".player-" + position + "-ticket-start").html(1);
            $coinflip.find(".player-" + position + "-ticket-end").html(parseInt($coinflip.attr("data-tickets")));
        }

        $coinflip.find(".player-" + position + "").show();

        removePlayerJoining(coinflipID);

        var $items = $coinflip.find(".player-" + position + "-items");
        for(var i = 0; i < data["items"].length; i++)
        {
            $items.append(renderItem(data["items"][i], null, null, true))
        }
    }

    function endCoinflip(coinflipID, data) {
        var $ahref = $(document.createElement("a")).attr("href", "/admin/user/view/"+data["user"]["userID"]).html(data["user"]["personaname"]);

        var $coinflip = $("#coinflip-"+coinflipID);

        $coinflip.find(".coinflip-joining").html("");
        $coinflip.find(".coinflip-winner").html($ahref);
        $coinflip.find(".coinflip-secret").html(data["secret"]);
        $coinflip.find(".coinflip-percentage").html(data["percentage"]);

        setTimeout(function () {
            $("#coinflip-"+coinflipID).remove();
        }, 20000);
    }
});
