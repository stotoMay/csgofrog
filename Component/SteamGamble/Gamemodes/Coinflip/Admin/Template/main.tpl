<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" type="text/css" href="/Component/Steam/Admin/Template/Static/CSS/steam.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js"></script>
    <script src="/Component/Steam/Admin/Template/Static/JScript/steam.js"></script>
    <script src="/Component/SteamWeb/Admin/Template/JScript/websocket.js"></script>

    <script type="text/javascript">
        var g_serverauth = "{getServerAuth()}";
        var g_serverip = '{setting("serverip", "component", "SteamWeb")}';
        var g_lang_lostconnection = '{language("steamweb", "message", "lostconnection")}';
    </script>

    <script src="/Component/SteamGamble/Gamemodes/Coinflip/Admin/Template/JScript/active.js?t=2"></script>

    <title>Coinflips {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="Admin/Coinflip/navigation.tpl"}
    </div>
    <div class="panel-body">
        <div id="coinflips">

        </div>

        <div class="panel panel-default coinflip" id="sample_coinflip" style="display:none;">
            <div class="panel-body">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <tr>
                            <td>CoinflipID</td>
                            <td class="coinflip-id"></td>
                        </tr>
                        <tr>
                            <td>Value</td>
                            <td class="coinflip-value"></td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <td>Player Joining</td>
                            <td class="coinflip-joining"></td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <td>Winner</td>
                            <td class="coinflip-winner"></td>
                        </tr>
                        <tr>
                            <td>Winning Percentage</td>
                            <td class="coinflip-percentage"></td>
                        </tr>
                        <tr>
                            <td>Secret</td>
                            <td class="coinflip-secret"></td>
                        </tr>
                        <tr>
                            <td>Roundhash</td>
                            <td class="coinflip-roundhash"></td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered">
                        <tr>
                            <td>Username</td>
                            <td class="player-1-username"></td>
                        </tr>
                        <tr>
                            <td>Value</td>
                            <td class="player-1-value"></td>
                        </tr>
                        <tr>
                            <td>Ticket Start</td>
                            <td class="player-1-ticket-start"></td>
                        </tr>
                        <tr>
                            <td>Ticket End</td>
                            <td class="player-1-ticket-end"></td>
                        </tr>
                    </table>
                    <div class="player-1-items">

                    </div>
                </div>
                <div class="col-md-6 player-2" style="display:none">
                    <table class="table table-bordered">
                        <tr>
                            <td>Username</td>
                            <td class="player-2-username"></td>
                        </tr>
                        <tr>
                            <td>Value</td>
                            <td class="player-2-value"></td>
                        </tr>
                        <tr>
                            <td>Ticket Start</td>
                            <td class="player-2-ticket-start"></td>
                        </tr>
                        <tr>
                            <td>Ticket End</td>
                            <td class="player-2-ticket-end"></td>
                        </tr>
                    </table>
                    <div class="player-2-items">

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

    {includetemplate="footer.tpl"}
</body>
</html>