<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>Coinflip History {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="Admin/Coinflip/navigation.tpl"}
    </div>
    <div class="panel-body">
        <table class="table table-bordered">
            <tr>
                <td></td>
                <td>Today</td>
                <td>All Time</td>
            </tr>
            <tr>
                <td>Game count</td>
                <td>{$gamblestats->gamemode("coinflip")->getGameCount(midnight())}</td>
                <td>{$gamblestats->gamemode("coinflip")->getGameCount(0)}</td>
            </tr>
            <tr>
                <td>Maximum Prize</td>
                <td>${number_format($gamblestats->gamemode("coinflip")->getBiggestValue(midnight()))}</td>
                <td>${number_format($gamblestats->gamemode("coinflip")->getBiggestValue(0))}</td>
            </tr>
        </table>

        <form method="post" action="/admin/coinflip/history">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="{language("coinflip", "coinflipID")}" value="" name="coinflipID">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
            <input type="hidden" name="token" value="{token()}">
        </form>

        <hr>

        {includetemplate="Admin/Coinflip/coinflip_rows.tpl"}

        <div class="center-block">
            {pagination($coinflips.total, $coinflips.current, $pageurl)}
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>