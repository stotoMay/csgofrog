<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>Coinflip Statistics {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="Admin/Coinflip/navigation.tpl"}
    </div>
    <div class="panel-body">
        <h1>Game Count</h1>
        {statistics("/admin/coinflip/gamecountstats")}
        <h1>Average Game Size</h1>
        {statistics("/admin/coinflip/gamesizestats")}
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>