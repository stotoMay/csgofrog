<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css" />

    <title>Coinflip #{$coinflip.coinflipID} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ul class="breadcrumb">
    <li><a href="/admin/coinflip">Coinflip</a></li>
    <li><a href="/admin/coinflip/history">History</a></li>
    <li class="active">#{$coinflip.coinflipID}</li>
</ul>

<div class="panel panel-default">
    <div class="panel-body">
        {if $coinflip.withdrawID}
            {renderWithdrawStatus($coinflip.withdrawID)}
        {/if}

        <table class="table table-bordered">
            <tr>
                <td>Coinflip ID</td>
                <td>{$coinflip.coinflipID}</td>
            </tr>
            <tr>
                <td>Value</td>
                <td>${number_format(divide($coinflip.tickets, 100))}</td>
            </tr>
            <tr>
                <td>Created</td>
                <td>{datetimeformat($coinflip.created)}</td>
            </tr>
            <tr>
                <td>Finished</td>
                <td>{datetimeformat($coinflip.finished)}</td>
            </tr>
        </table>

        <table class="table table-bordered">
            <tr>
                <td>Winner</td>
                <td><a href="/admin/user/view/{$coinflip.winner.user.userID}">{$coinflip.winner.user.personaname}</a></td>
            </tr>
            <tr>
                <td>Hash</td>
                <td>{$coinflip.hash}</td>
            </tr>
            <tr>
                <td>Percentage</td>
                <td>{$coinflip.percentage}</td>
            </tr>
            <tr>
                <td>Secret</td>
                <td>{$coinflip.secret}</td>
            </tr>
            <tr>
                <td>Winning Ticket</td>
                <td>{$coinflip.winner.ticket}</td>
            </tr>
        </table>

        <div class="col-md-6">
            <div class="center-block">
                <img src="{$coinflip.player_1.user.avatarfull}" class="img-thumbnail">
                <br><br>
                <a href="/admin/user/view/{$coinflip.player_1.user.userID}">{$coinflip.player_1.user.personaname}</a>
                <br><br>
            </div>

            <table class="table table-bordered">
                <tr>
                    <td>Value</td>
                    <td>${number_format(divide($coinflip.player_1.tickets, 100))}</td>
                </tr>
                <tr>
                    <td>Winning Probability</td>
                    <td>{number_format(multiply(divide($coinflip.player_1.tickets, $coinflip.tickets), 100))}%</td>
                </tr>
                <tr>
                    <td>Tickets</td>
                    <td>{$coinflip.player_1.ticket_start} - {$coinflip.player_1.ticket_end}</td>
                </tr>
            </table>

            <div class="panel panel-default">
                <div class="panel-body">
                    {foreach $coinflip.player_1.items as $item}
                        {renderItem($item)}
                    {/foreach}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="center-block">
                <img src="{$coinflip.player_2.user.avatarfull}" class="img-thumbnail">
                <br><br>
                <a href="/admin/user/view/{$coinflip.player_2.user.userID}">{$coinflip.player_2.user.personaname}</a>
                <br><br>
            </div>

            <table class="table table-bordered">
                <tr>
                    <td>Value</td>
                    <td>${number_format(divide($coinflip.player_2.tickets, 100))}</td>
                </tr>
                <tr>
                    <td>Winning Probability</td>
                    <td>{number_format(multiply(divide($coinflip.player_2.tickets, $coinflip.tickets), 100))}%</td>
                </tr>
                <tr>
                    <td>Tickets</td>
                    <td>{$coinflip.player_2.ticket_start} - {$coinflip.player_2.ticket_end}</td>
                </tr>
            </table>

            <div class="panel panel-default">
                <div class="panel-body">
                    {foreach $coinflip.player_2.items as $item}
                        {renderItem($item)}
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>