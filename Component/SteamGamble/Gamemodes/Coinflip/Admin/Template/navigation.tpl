<ul class="nav nav-tabs">
    <li class="{if $path.2 == ""}active{/if}"><a href="/admin/coinflip">Active</a></li>
    <li class="{if $path.2 == "history"}active{/if}"><a href="/admin/coinflip/history">History</a></li>
    {if permission("stats", "controller", "coinflip") == 1}
        <li class="{if $path.2 == "stats"}active{/if}"><a href="/admin/coinflip/stats">Statistics</a></li>
    {/if}
</ul>