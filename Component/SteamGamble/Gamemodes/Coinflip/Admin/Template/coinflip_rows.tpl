<div class="table-responsive">
<table class="table">
    <thead>
        <tr>
            <th>Coinflip ID</th>
            <th>Players</th>
            <th>Value</th>
            <th>Winner</th>
            <th>Created</th>
            <th>Ended</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        {foreach $coinflips.results as $coinflip}
            <tr class="{if $coinflip.winner.userID == $userID}success{/if}">
                <td>#{$coinflip.coinflipID}</td>
                <td>
                    <a href="/admin/user/view/{$coinflip.player_1.user.userID}"><img src="{$coinflip.player_1.user.avatar}" title="{$coinflip.player_1.user.personaname}" style="width:32px;height:32px;" data-toggle="tooltip"></a> vs
                    <a href="/admin/user/view/{$coinflip.player_2.user.userID}"><img src="{$coinflip.player_2.user.avatar}" title="{$coinflip.player_2.user.personaname}" style="width:32px;height:32px;" data-toggle="tooltip"></a>
                </td>
                <td>${number_format(divide($coinflip.player_1.tickets, 100))}</td>
                <td>
                    <a href="/admin/user/view/{$coinflip.winner.userID}"><img src="{$coinflip.winner.avatar}" style="width:32px;height:32px;" title="{$coinflip.winner.personaname}" data-toggle="tooltip"></a>
                </td>
                <td>{datetimeformat($coinflip.created)}</td>
                <td>{datetimeformat($coinflip.finished)}</td>
                <td><a href="/admin/coinflip/game/{$coinflip.coinflipID}" class="btn btn-default">View</a></td>
            </tr>
        {/foreach}
    </tbody>
</table>
</div>