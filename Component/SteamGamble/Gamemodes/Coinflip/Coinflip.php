<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Gamemodes\Coinflip;

use App\Admin\Tools\AjaxStatistics;
use Component\Steam\Item\ItemList;
use Component\SteamGamble\Parent\Gamemode;

class Coinflip extends Gamemode
{
    public function init()
    {
        if(!class_exists('App\\Admin\\Controller\\Coinflip', false))
        {
            include(base_dir("Component/SteamGamble/Gamemodes/Coinflip/Admin/Coinflip.php"));
            $this->app->router()->initController('\\App\\Admin\\Controller\\Coinflip');
        }

        $this->app->language()->mergeFile(__DIR__."/Language/English.lang");
    }

    /**
     * @param ItemList $items
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function onBotInventoryLoad($items)
    {
        $assetIDs = [];
        foreach($items as $item)
        {
            $assetIDs[] = $item->getAssetID();
        }

        $coinflipitems = $this->gamble->app()->database()->select("coinflip_item")
            ->column(["appID", "contextID", "assetID"])
            ->where(["assetID" => $assetIDs, "fee" => 0, "status" => 0])->fetchAssoc();

        foreach($coinflipitems as $item)
        {
            $items->removeAsset($item["appID"], $item["contextID"], $item["assetID"]);
        }
    }

    /**
     * @param int $botID
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function onSteamBotDelete($botID)
    {
        $coinflipIDs = $this->app->database()->select("coinflip_item")
            ->column(["coinflipID"])
            ->where(["botID" => $botID, "status" => 0])
            ->group("coinflipID")->fetchAssoc();

        foreach($coinflipIDs as $coinflipID)
        {
            $this->app->database()->update("coinflip_item")->set(["status" => 1])
                ->where(["coinflipID" => $coinflipID["coinflipID"], "botID[!]" => $botID]);
            $this->app->database()->update("coinflip_item")->set(["status" => 2])
                ->where(["coinflipID" => $coinflipID["coinflipID"], "botID" => $botID]);

            $this->app->database()->update("coinflip")->set(["status" => 2])
                ->where(["id" => $coinflipID["coinflipID"]]);
        }
    }

    public function totalprofit()
    {
        $items = $this->gamble->app()->database()->select("coinflip_item")
            ->column(["tickets" => ["function" => "sum", "alias" => "value"]])
            ->where(["fee" => 1])->fetchRow();

        $value = intval($items["value"]);

        return $value / 100;
    }

    public function todayprofit()
    {
        $items = $this->gamble->app()->database()->select("coinflip_item")
            ->column(["tickets" => ["function" => "sum", "alias" => "value"]])
            ->where(["fee" => 1, "dateline[>=]" => time() - 3600 * 24])->fetchRow();

        $value = intval($items["value"]);

        return $value / 100;
    }

    public function userprofitgraph($userID)
    {
        /**
         * @var Statistic $statistic
         */
        $statistic = $this->gamble->statistic()->gamemode("coinflip");

        return $statistic->getUserProfitGraph($userID);
    }

    public function userluckygraph($userID)
    {
        /**
         * @var Statistic $statistic
         */
        $statistic = $this->gamble->statistic()->gamemode("coinflip");

        return $statistic->getUserLuckyGraph($userID);
    }

    public function profitgraph(AjaxStatistics $statistic)
    {
        $data = $this->app->database()->select("coinflip_item")->column([
            ["str" => "SUM(tickets) / 100", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(dateline), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where(["dateline[>=]" => $statistic->getStartTime(), "dateline[<=]" => $statistic->getStopTime(), "fee" => 1])
            ->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->app->language()->get(["steamgamble", "coinflip", "label_profit"]));

        return $data;
    }

    public function profitofusergraph(AjaxStatistics $statistic, $userIDs)
    {
        $data = $this->app->database()->select("coinflip_item")
            ->column([
                ["str" => "SUM(tickets) / 100", "alias" => "value"],
                ["str" => "DATE_FORMAT(FROM_UNIXTIME(dateline), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
            ])
            ->leftJoin("coinflip", ["coinflip_item.coinflipID[=]coinflip.id"])
            ->where([
                "coinflip_item.dateline[>=]" => $statistic->getStartTime(),
                "coinflip_item.dateline[<=]" => $statistic->getStopTime(),
                "coinflip_item.fee" => 1, "coinflip.winnerID" => $userIDs
            ])
            ->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->app->language()->get(["steamgamble", "coinflip", "label_profit"]));

        return $data;
    }

    public function pageUserProfile($args, $userID)
    {
        $controller = new \App\Admin\Controller\Coinflip($args[2], $args[3], $args[0], $args[4], $args[1]);

        $template = "";
        if(method_exists($controller, "user") && is_callable([$controller, "user"]))
        {
            $template = (string)$controller->user($userID);
        }

        return $template;
    }
}