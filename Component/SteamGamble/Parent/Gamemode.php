<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Parent;

use Component\SteamGamble\Classes\Core;

class Gamemode
{
    protected $gamble;
    protected $app;
    protected $steam;

    public function __construct(Core $gamble)
    {
        $this->gamble = $gamble;
        $this->app = $gamble->app();
        $this->steam = $gamble->steam();
    }
}