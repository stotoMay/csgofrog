<div class="panel panel-default">
    <div class="panel-heading">Profit</div>
    <div class="panel-body">
        {statistics(string("/admin/gamblestatus/referralprofitstats/", $referralID))}
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Deposited</div>
    <div class="panel-body">
        {statistics(string("/admin/gamblestatus/referraldepositedstats/", $referralID))}
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Bets</div>
    <div class="panel-body">
        {statistics(string("/admin/gamblestatus/referralbetstats/", $referralID))}
    </div>
</div>