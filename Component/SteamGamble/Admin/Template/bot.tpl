{if permission("trades", "controller", "steamgamble") == 1}
    <div class="panel panel-default">
        <div class="panel-heading">Trades</div>
        <div class="panel-body">
            {statistics(string("/admin/gamblestatus/tradestats/", $botID))}
        </div>
    </div>
{/if}