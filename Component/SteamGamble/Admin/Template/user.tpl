{if permission("trades", "controller", "steamgamble") == 1}
    <div class="panel panel-default">
        <div class="panel-heading">Transactions</div>
        <div class="panel-body">
            {pagination_ajax(string("/admin/gamblestatus/transactions/", $user.userID))}
        </div>
    </div>
{/if}

{if permission("stats", "controller", "steamgamble") == 1}
    <div class="panel panel-default">
        <div class="panel-heading">Profit Graph</div>
        <div class="panel-body">
            <div id="profitgraph-container"></div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Lucky Graph</div>
        <div class="panel-body">
            <div id="luckygraph-container"></div>
        </div>
    </div>

    <script type="text/javascript">
        renderDefaultGraph($("#profitgraph-container"), {json_encode($profitgraph)});
        renderDefaultGraph($("#luckygraph-container"), {json_encode($luckygraph)});
    </script>

    <div class="panel panel-default">
        <div class="panel-heading">Bets</div>
        <div class="panel-body">
            {statistics(string("/admin/gamblestatus/betstats/", $user.userID))}
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Deposited</div>
        <div class="panel-body">
            {statistics(string("/admin/gamblestatus/depositstats/", $user.userID))}
        </div>
    </div>
{/if}