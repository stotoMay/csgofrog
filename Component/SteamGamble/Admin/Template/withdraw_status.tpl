{if $withdraw.status == 0}
    <div class="alert alert-info">
        {language("steamgamble", "withdraw_status_0")}
        <a href="/admin/gamblestatus/withdraw/{$withdraw.id}" class="alert-link">{language("steamgamble", "withdraw_status_details")}</a>
    </div>
{elseif $withdraw.status == 1}
    <div class="alert alert-info">
        {language("steamgamble", "withdraw_status_1")}
        <a href="/admin/gamblestatus/withdraw/{$withdraw.id}" class="alert-link">{language("steamgamble", "withdraw_status_details")}</a>
    </div>
{elseif $withdraw.status == 2}
    <div class="alert alert-success">
        {language("steamgamble", "withdraw_status_2")}
        <a href="/admin/gamblestatus/withdraw/{$withdraw.id}" class="alert-link">{language("steamgamble", "withdraw_status_details")}</a>
    </div>
{elseif $withdraw.status == 3}
    <div class="alert alert-warning">
        {language("steamgamble", "withdraw_status_3")}
        <a href="/admin/gamblestatus/withdraw/{$withdraw.id}" class="alert-link">{language("steamgamble", "withdraw_status_details")}</a>
    </div>
{elseif $withdraw.status == 4}
    <div class="alert alert-info">
        {language("steamgamble", "withdraw_status_4")}
        <a href="/admin/gamblestatus/withdraw/{$withdraw.id}" class="alert-link">{language("steamgamble", "withdraw_status_details")}</a>
    </div>
{elseif $withdraw.status == 5}
    {if $withdraw.parentID}
        <div class="alert alert-danger">
            {language("steamgamble", "withdraw_status_5_1")}
            <a href="/admin/gamblestatus/withdraw/{$withdraw.id}" class="alert-link">{language("steamgamble", "withdraw_status_refund")}</a>
        </div>
    {else}
        <div class="alert alert-danger">
            {language("steamgamble", "withdraw_status_5_0")}
            <a href="/admin/gamblestatus/withdraw/{$withdraw.id}" class="alert-link">{language("steamgamble", "withdraw_status_refund")}</a>
        </div>
    {/if}
{elseif $withdraw.status == 6}
    {if $withdraw.parentID}
        <div class="alert alert-warning">
            {language("steamgamble", "withdraw_status_6_1")}
            <a href="/admin/gamblestatus/withdraw/{$withdraw.id}" class="alert-link">{language("steamgamble", "withdraw_status_details")}</a>
        </div>
    {else}
        <div class="alert alert-warning">
            {language("steamgamble", "withdraw_status_6_0")}
            <a href="/admin/gamblestatus/withdraw/{$withdraw.id}" class="alert-link">{language("steamgamble", "withdraw_status_details")}</a>
        </div>
    {/if}
{elseif $withdraw.status == 7}
    <div class="alert alert-danger">
        {language("steamgamble", "withdraw_status_7")}
        <a href="/admin/gamblestatus/withdraw/{$withdraw.id}" class="alert-link">{language("steamgamble", "withdraw_status_details")}</a>
    </div>
{elseif $withdraw.status == 8}
    <div class="alert alert-success">
        {language("steamgamble", "withdraw_status_8")}
        <a href="/admin/gamblestatus/withdraw/{$withdraw.id}" class="alert-link">{language("steamgamble", "withdraw_status_details")}</a>
    </div>
{/if}