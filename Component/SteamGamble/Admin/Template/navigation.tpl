<ul class="nav nav-tabs">
    {if permission("profit", "controller", "steamgamble") == 1}
        <li class="{if $path.2 == ""}active{/if}"><a href="/admin/steamgamble/">{language("steamgamble", "page_profit")}</a></li>
    {/if}
    {if permission("request_fees", "component", "SteamGamble") == 1}
        <li class="{if $path.2 == "fee"}active{/if}"><a href="/admin/steamgamble/fee">{language("steamgamble", "page_fee")}</a></li>
    {/if}
    {if permission("stats", "controller", "steamgamble") == 1}
        <li class="{if $path.2 == "stats"}active{/if}"><a href="/admin/steamgamble/stats/">{language("steamgamble", "page_stats")}</a></li>
    {/if}
    {if permission("trades", "controller", "steamgamble") == 1}
        <li class="{if $path.2 == "trades"}active{/if}"><a href="/admin/steamgamble/trades/">{language("steamgamble", "page_trades")}</a></li>
    {/if}
</ul>