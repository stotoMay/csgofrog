<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css" />

    <title>Deposit #{$deposit.id} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ul class="breadcrumb">
    <li><a href="/admin/user">Users</a></li>
    <li><a href="/admin/user/view/{$user.userID}">{$user.personaname}</a></li>
    <li><a href="/admin/user/view/{$user.userID}">Deposits</a></li>
    <li class="active">#{$depositID}</li>
</ul>

<div class="panel panel-primary">
    <div class="panel-heading">#{$deposit.id}</div>
    <div class="panel-body">
        {renderDepositStatus($deposit)}
        <table class="table table-bordered">
            <tr>
                <td>User</td>
                <td><a href="/admin/user/view/{$user.userID}">{$user.personaname}</a></td>
            </tr>
            <tr>
                <td>Bot ID</td>
                <td><a href="/admin/steambotconfig/account/{$deposit.botID}">#{$deposit.botID}</a></td>
            </tr>
            <tr>
                <td>Gamemode</td>
                <td>{$deposit.gamemode}</td>
            </tr>
            <tr>
                <td>Game ID</td>
                <td>{$deposit.gameID}</td>
            </tr>
            <tr>
                <td>Security Token</td>
                <td>{$deposit.token}</td>
            </tr>
            <tr>
                <td>Value</td>
                <td>${number_format(divide($deposit.value, 100))}</td>
            </tr>
            <tr>
                <td>Sent</td>
                <td>{datetimeformat($deposit.dateline)}</td>
            </tr>
            <tr>
                <td>Accepted</td>
                <td>
                    {if $deposit.accepted}
                        {datetimeformat($deposit.accepted)}
                    {/if}
                </td>
            </tr>
            <tr>
                <td>Steam Offer ID</td>
                <td><a href="/admin/steambotconfig/checkoffer/{$deposit.botID}/{$deposit.offerID}">{$deposit.offerID}</a></td>
            </tr>
        </table>

        <div class="panel panel-default">
            <div class="panel-body">
                {foreach $deposit.items as $item}
                    {renderItem($item)}
                {/foreach}
            </div>
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>