$(document).ready(function () {
    var websocket = new WebsocketHandler(g_serverip, g_serverauth);

    var $feerquestbutton = $("#feerequest-button");
    var $unselectbutton = $("#feeremove-button");
    var $tradevalue = $(".tradevalue");

    var requestbuttontext = $feerquestbutton.html();
    var unselectbuttontext = $unselectbutton.html();

    var selected = [];
    var sending = false;
    var tradevalue = 0;

    websocket.addListener("onConnectionClose", function () {
        print_error(g_lang_lostconnection);
    });

    websocket.addListener("onFeeRequestSuccess", function (data) {
        $("#fee-items-selected").html("");
        $feerquestbutton.width("width", "").html(requestbuttontext).addClass("disabled");
        $tradevalue = $(".tradevalue").html("0.00");
        tradevalue = 0;
        selected = [];
        sending = false;
        print_info(g_lang_fee_success);
    });

    websocket.addListener("onFeeRequestError", function (data) {
        $feerquestbutton.width("width", "").html(requestbuttontext).removeClass("disabled");
        $tradevalue = $(".tradevalue");
        sending = false;
        print_error(g_lang_fee_error);
    });

    var shifted;
    $(document).on('keyup keydown', function(e){shifted = e.shiftKey;});

    function toSelected() {
        if(sending) { return; }

        var $this = $(this);
        var id = $this.attr("data-itemid");

        var moving = [$this];
        var i;

        if(shifted === true)
        {
            var $feeitems = $(".fee-item");
            for(i = 0; i < $feeitems.length; i++)
            {
                var $item = $($feeitems[i]);

                if($item.attr("data-itemid") == id)
                {
                    break;
                }

                moving.push($item);
            }
        }

        for(i = 0; i < moving.length; i++)
        {
            $this = moving[i];
            id = $this.attr("data-itemid");

            selected.push(id);

            $feerquestbutton.removeClass("disabled");
            $unselectbutton.removeClass("disabled");

            $this.unbind("click").bind("click", toUnselected);
            $("#fee-items-selected").append($this);

            tradevalue += Math.abs(Math.round(parseFloat($this.attr("data-price"))*100));
            $tradevalue.html((tradevalue/100).toFixed(2));
        }
    }

    function toUnselected() {
        if(sending) { return; }

        var $this = $(this);
        var id = $this.attr("data-itemid");

        selected.splice(selected.indexOf(id), 1);
        if (selected.length == 0) {
            $feerquestbutton.addClass("disabled");
            $unselectbutton.addClass("disabled");
        }

        $this.unbind("click").bind("click", toSelected);
        var itemvalue = Math.abs(Math.round(parseFloat($this.attr("data-price"))*100));

        $("#fee-item-container-"+id).html($this);

        tradevalue -= itemvalue;
        $tradevalue.html((tradevalue/100).toFixed(2));
    }

    $(".fee-item").click(toSelected);

    $feerquestbutton.click(function () {
        if ($feerquestbutton.hasClass("disabled")) {
            return false;
        }

        $feerquestbutton.css("width", $feerquestbutton.innerWidth()).html('<i class="fa fa-spin fa-circle-o-notch"></i>').addClass("disabled");

        sending = true;
        websocket.sendMessage("requestfees", {"items": selected, "tradeurl": $("#tradeurl").val()});
    });

    $unselectbutton.click(function () {
        if ($unselectbutton.hasClass("disabled")) {
            return false;
        }

        $unselectbutton.css("width", $unselectbutton.innerWidth()).html('<i class="fa fa-spin fa-circle-o-notch"></i>').addClass("disabled");

        $.ajax({
            url: "/admin/steamgamble/unselectfeeitems",
            dataType: "json",

            method: "post",
            data: {
                "token": g_sessionid,
                "items": selected.join(":")
            },

            success: function (rsp) {
                if (rsp["success"] == true) {
                    $("#fee-items-selected").html("");
                    $tradevalue.html("0.00");
                    tradevalue = 0;
                    selected = [];
                    print_info(rsp["message"]);
                    $unselectbutton.css("width", "").html(unselectbuttontext).addClass("disabled");
                }
                else
                {
                    print_error(rsp["message"]);
                    $unselectbutton.css("width", "").html(unselectbuttontext).remove("disabled");
                }
            }
        });
    });

});