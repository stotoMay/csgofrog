var websocket = new WebsocketHandler(g_serverip, g_serverauth);
var $button = $({});
var btntext = "";

websocket.addListener("onConnectionClose", function () {
    print_error(g_lang_lostconnection);
});

websocket.addListener("onRefundSuccess", function (data) {
    $button.html(btntext).removeClass("disabled");
    print_info(g_lang_refund_success)
});

websocket.addListener("onRefundError", function (data) {
    $button.html(btntext).removeClass("disabled");
    print_error(g_lang_refund_error)
});

function refundTrade($btn, withdrawID) {
    btntext = $btn.html();
    $button = $btn;

    websocket.sendMessage("refund", withdrawID);
    $button.html('<i class="fa fa-spin fa-circle-o-notch"></i>').addClass("disabled");
}