<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js"></script>

    <script type="text/javascript">
        var g_serverauth = "{getServerAuth()}";
        var g_serverip = '{setting("serverip", "component", "SteamWeb")}';

        var g_lang_fee_success = '{language("steamgamble", "message", "fee_trade_success")}';
        var g_lang_fee_error = '{language("steamgamble", "message", "fee_trade_error")}';

        var g_lang_lostconnection = '{language("steamgamble", "message", "lostconnection")}';
    </script>

    <script src="/Component/SteamWeb/Admin/Template/JScript/websocket.js"></script>
    <script src="/Component/SteamGamble/Admin/Template/JScript/fee.js?t=2"></script>

    <title>{language("steamgamble", "title_fee")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="SteamGamble/navigation.tpl"}
    </div>
    <div class="panel-body">
        <div class="panel panel-default">
            <div class="panel-body" id="fee-items" style="height: 300px; overflow-y: auto;">
                {foreach $feeitems as $item}
                    <div id="fee-item-container-{$item.feeID}" style="width:74px;height:74px;display:inline-block;">
                        <div data-itemid="{$item.feeID}" data-price="{$item.price}" class="fee-item">
                            {renderItem($item, 73, 73, false)}
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>

        <input type="text" class="form-control" value="{$tradeurl}" id="tradeurl" style="margin-bottom: 20px;">

        <div class="panel panel-default">
            <div class="panel-body" id="fee-items-selected" style="height: 300px; overflow-y: auto;">

            </div>
        </div>

        <div class="col-sm-6 center-block">
            <a href="javascript:" id="feerequest-button" class="btn btn-primary btn-margin disabled">{language("steamgamble", "fee_sendoffer")} ($<span class="tradevalue">0.00</span>)</a>
        </div>
        <div class="col-sm-6 center-block">
            <a href="javascript:" id="feeremove-button" class="btn btn-primary btn-margin disabled">{language("steamgamble", "fee_unselect")}</a>
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>