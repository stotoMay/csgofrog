{if $deposit.status == 0}
    <div class="alert alert-info">
        {language("steamgamble", "deposit_status_0")}
        <a href="/admin/gamblestatus/deposit/{$deposit.id}" class="alert-link">{language("steamgamble", "deposit_status_details")}</a>
    </div>
{elseif $deposit.status == 1}
    <div class="alert alert-success">
        {language("steamgamble", "deposit_status_1")}
        <a href="/admin/gamblestatus/deposit/{$deposit.id}" class="alert-link">{language("steamgamble", "deposit_status_details")}</a>
    </div>
{elseif $deposit.status == 2}
    <div class="alert alert-warning">
        {language("steamgamble", "deposit_status_2")}
        <a href="/admin/gamblestatus/deposit/{$deposit.id}" class="alert-link">{language("steamgamble", "deposit_status_details")}</a>
    </div>
{/if}