<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("steamgamble", "title_stats")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="SteamGamble/navigation.tpl"}
    </div>
    <div class="panel-body">
        <h1>Trades</h1>
        <div class="panel panel-default">
            <div class="panel-body">
                {statistics("/admin/gamblestatus/tradestats")}
            </div>
        </div>

        <h1>Deposited</h1>
        <div class="panel panel-default">
            <div class="panel-body">
                {statistics("/admin/gamblestatus/depositstats")}
            </div>
        </div>

        <h1>Bets</h1>
        <div class="panel panel-default">
            <div class="panel-body">
                {statistics("/admin/gamblestatus/betstats")}
            </div>
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>