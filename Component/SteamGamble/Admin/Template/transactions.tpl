<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th>Type</th>
                <th>ID</th>
                <th>Bot ID</th>
                <th>Value</th>
                <th>Sent</th>
                <th>Accepted</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            {foreach $transactions as $transaction}
                <tr class="{$transaction.status_level}">
                    <td>
                        {if $transaction.mode == "withdraw"}
                            Withdraw
                        {else}
                            Deposit
                        {/if}
                    </td>
                    <td>#{$transaction.id}</td>
                    <td><a href="/admin/steambotconfig/{$transaction.botID}">#{$transaction.botID}</a></td>
                    <td>${number_format(divide($transaction.value, 100))}</td>
                    <td>{datetimeformat($transaction.dateline)}</td>
                    <td>
                        {if $transaction.accepted}
                            {datetimeformat($transaction.accepted)}
                        {/if}
                    </td>
                    <td>
                        {if $transaction.mode == "withdraw"}
                            <a href="/admin/gamblestatus/withdraw/{$transaction.id}" class="btn btn-default">View Details</a>
                        {else}
                            <a href="/admin/gamblestatus/deposit/{$transaction.id}" class="btn btn-default">View Details</a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</div>