<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <link rel="stylesheet" href="/Component/Steam/Admin/Template/Static/CSS/steam.css" type="text/css" />

    {if permission("refund", "component", "SteamWeb") == 1}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js"></script>
        <script type="text/javascript">
            var g_serverauth = "{getServerAuth()}";
            var g_serverip = '{setting("serverip", "component", "SteamWeb")}';
            var g_lang_lostconnection = '{language("steamweb", "message", "lostconnection")}';

            var g_lang_refund_success = '{language("steamgamble", "message", "refund_success")}';
            var g_lang_refund_error = '{language("steamgamble", "message", "refund_error")}';
        </script>
        <script src="/Component/SteamWeb/Admin/Template/JScript/websocket.js"></script>
        <script src="/Component/SteamGamble/Admin/Template/JScript/refund.js"></script>
    {/if}

    <title>Withdraw #{$withdraws.id} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ul class="breadcrumb">
    <li><a href="/admin/user">Users</a></li>
    <li><a href="/admin/user/view/{$user.userID}">{$user.personaname}</a></li>
    <li><a href="/admin/user/view/{$user.userID}">Withdraws</a></li>
    <li class="active">#{$withdrawID}</li>
</ul>

{if permission("refund", "component", "SteamWeb") == 1}
    {if $withdraws.lateststatus == 4 || $withdraws.lateststatus == 5 || $withdraws.lateststatus == 7}
        <a href="javascript:" onclick="refundTrade($(this), '{$withdraws.id}');" class="btn btn-danger btn-block">Refund</a>
        <div style="display:block;margin-bottom:20px;"></div>
    {/if}

    {if $withdraws.lateststatus == 6 && $withdraws.parentID}
        <a href="javascript:" onclick="refundTrade($(this), '{$withdraws.id}');" class="btn btn-danger btn-block">Refund</a>
        <div style="display:block;margin-bottom:20px;"></div>
    {/if}
{/if}

<div class="panel panel-primary">
    <div class="panel-heading">#{$withdraws.id}</div>
    <div class="panel-body">
        {renderWithdrawStatus($withdraws)}

        <table class="table table-bordered">
            <tr>
                <td>User</td>
                <td><a href="/admin/user/view/{$user.userID}">{$user.personaname}</a></td>
            </tr>
            <tr>
                <td>Bot ID</td>
                <td><a href="/admin/steambotconfig/account/{$withdraws.botID}">#{$withdraws.botID}</a></td>
            </tr>
            <tr>
                <td>Security Token</td>
                <td>{$withdraws.token}</td>
            </tr>
            <tr>
                <td>Value</td>
                <td>${number_format(divide($withdraws.value, 100))}</td>
            </tr>
            <tr>
                <td>Message</td>
                <td>{$withdraws.message}</td>
            </tr>
            <tr>
                <td>Created</td>
                <td>{datetimeformat($withdraws.dateline)}</td>
            </tr>
            <tr>
                <td>Sent</td>
                <td>
                    {if $withdraws.sent}
                        {datetimeformat($withdraws.sent)}
                    {/if}
                </td>
            </tr>
            <tr>
                <td>Accepted</td>
                <td>
                    {if $withdraws.accepted}
                        {datetimeformat($withdraws.accepted)}
                    {/if}
                </td>
            </tr>
            <tr>
                <td>Steam Offer ID</td>
                <td>{$withdraws.offerID}</td>
            </tr>
        </table>

        <div class="panel panel-default">
            <div class="panel-body">
                {foreach $withdraws.items as $item}
                    {renderItem($item)}
                {/foreach}
            </div>
        </div>
    </div>
</div>

{foreach $withdraws.history as $withdraw}
    <div class="panel panel-default">
        <div class="panel-heading">#{$withdraw.id}</div>
        <div class="panel-body">
            {renderWithdrawStatus($withdraw)}

            <table class="table table-bordered">
                <tr>
                    <td>Security Token</td>
                    <td>{$withdraw.token}</td>
                </tr>
                <tr>
                    <td>Created</td>
                    <td>{datetimeformat($withdraw.dateline)}</td>
                </tr>
                <tr>
                    <td>Sent</td>
                    <td>
                        {if $withdraw.sent}
                            {datetimeformat($withdraw.sent)}
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td>Steam Offer ID</td>
                    <td>{$withdraw.offerID}</td>
                </tr>
            </table>
        </div>
    </div>
{/foreach}

{includetemplate="footer.tpl"}
</body>
</html>