<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("steamgamble", "title_trades")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="SteamGamble/navigation.tpl"}
    </div>
    <div class="panel-body">
        <form action="/admin/steamgamble/trades" method="post">
            <div class="input-group">
                <input type="text" class="form-control" placeholder='Enter Time' name="searchtime">
                <div class="input-group-btn ">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
            <input type="hidden" name="token" value="{token()}"> <input type="hidden" name="mode" value="{$mode}">
        </form>
        <hr>

        <div class="table-responsive">
            <table class="table table-striped table-smallpadding">
                <thead>
                    <tr>
                        <th></th>
                        <th>Trade ID</th>
                        <th></th>
                        <th>User</th>
                        <th>Value</th>
                        <th>Bot ID</th>
                        <th>Sent</th>
                        <th>Accepted</th>
                    </tr>
                </thead>
                <tbody>
                {foreach $trades.results as $trade}
                    <tr class="{$trade.status_level}">
                        <td>
                            {if $trade.mode == "deposit"}
                                Deposit
                            {else}
                                Withdraw
                            {/if}
                        </td>
                        <td>#{$trade.id}</td>
                        <td><img src="{$trade.user.avatar}" ></td>
                        <td><a href="/admin/user/view/{$trade.user.userID}">{$trade.user.personaname}</a></td>
                        <td>${number_format(divide($trade.value, 100))}</td>
                        <td><a href="/admin/steambotconfig/{$trade.botID}">#{$trade.botID}</a></td>
                        <td>{datetimeformat($trade.dateline)}</td>
                        <td>
                            {if $trade.accepted}
                                {datetimeformat($trade.accepted)}
                            {/if}
                        </td>
                        <td>
                            {if $trade.mode == "deposit"}
                                <a href="/admin/gamblestatus/deposit/{$trade.id}" class="btn btn-default">View Details</a>
                            {else}
                                <a href="/admin/gamblestatus/withdraw/{$trade.id}" class="btn btn-default">View Details</a>
                            {/if}
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
        <div class="center-block">
            {pagination($trades.total, $trades.current, $pageurl)}
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>