<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("typeID", "controller", "steamgamble")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        {includetemplate="SteamGamble/navigation.tpl"}
    </div>
    <div class="panel-body">
        <table class="table table-bordered table-marginless">
            <tr>
                <td>{language("steamgamble", "totalprofit")} </td>
                <td>${number_format($totalprofit)}</td>
            </tr>
            <tr>
                <td>{language("steamgamble", "todayprofit")} </td>
                <td>${number_format($todayprofit)}</td>
            </tr>
        </table>

        <div style="display:block;margin-bottom:20px;"></div>

        <div class="panel panel-default">
            <div class="panel-body">
                {statistics("/admin/gamblestatus/profitstats")}
            </div>
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>