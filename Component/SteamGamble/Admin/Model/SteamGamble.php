<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Admin\Model;

use App\Router\Model;
use Component\SteamGamble\Classes\Core;

class SteamGamble extends Model
{
    public function getUser($userID)
    {
        return $this->database->select("user")->where(["id" => $userID])->fetchRow();
    }

    public function getFeeItems()
    {
        $items = $this->database->select("fee")->column([
            "appID" => "appID", "market_name" => "market_name", "id" => "feeID", "price"
        ])->where(["status" => 0])->order("price", "desc")->fetchAssoc();

        foreach($items as $key => $val)
        {
            $items[$key]["price"] /= 100;
        }

        return $items;
    }

    public function fetchFeeItems($itemlist)
    {
        $items = $this->database->select("fee")->where(["id" => $itemlist])->fetchAssoc();

        return $items;
    }

    public function deleteFeeItems($itemlist)
    {
        $this->database->delete("fee")->where(["id" => $itemlist]);
    }

    public function createFeeItem($item)
    {
        $this->database->insert("fee")->set([
            "botID" => $item["botID"], "appID" => $item["appID"], "contextID" => $item["contextID"],
            "assetID" => $item["assetID"], "market_name" => $item["market_name"], "amount" => $item["amount"],
            "dateline" => time()
        ]);
    }

    public function getTradeHistory(Core $gamble, $page = 1, $size = 50, $timestamp = NULL)
    {
        if(!isset($timestamp))
        {
            $timestamp = time();
        }

        $total = $this->database->select("withdraw")->where(["dateline[<=]" => $timestamp])->numRows();
        $total += $this->database->select("deposit")->where(["dateline[<=]" => $timestamp])->numRows();

        $transactions = $gamble->statistic()->getTradeHistory($page, $size, false, false);

        foreach($transactions as $key => $transaction)
        {
            if($transaction["mode"] == "withdraw")
            {
                if($transaction["status"] == 2 || $transaction["status"] == 8)
                {
                    $transactions[$key]["status_level"] = "success";
                }
                elseif($transaction["status"] == 5 || $transaction["status"] == 7)
                {
                    $transactions[$key]["status_level"] = "danger";
                }
                elseif($transaction["status"] == 3 || $transaction["status"] == 6)
                {
                    $transactions[$key]["status_level"] = "warning";
                }
                elseif($transaction["status"] == 4)
                {
                    $transactions[$key]["status_level"] = "info";
                }
                else
                {
                    $transactions[$key]["status_level"] = "";
                }
            }
            elseif($transaction["mode"] == "deposit")
            {
                if($transaction["status"] == 0)
                {
                    $transactions[$key]["status_level"] = "info";
                }
                elseif($transaction["status"] == 1)
                {
                    $transactions[$key]["status_level"] = "success";
                }
                else
                {
                    $transactions[$key]["status_level"] = "";
                }
            }
        }

        return ["results" => $transactions, "current" => $page, "total" => ceil($total / $size)];
    }

    public function searchTradePage($time, $size = 50)
    {
        $count = $this->database->select("withdraw")->where(["dateline[<=]" => $time])->numRows();
        $count += $this->database->select("deposit")->where(["dateline[<=]" => $time])->numRows();

        return intval(floor($count/$size) + 1);
    }
}