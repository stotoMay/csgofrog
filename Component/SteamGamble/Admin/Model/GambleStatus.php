<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Admin\Model;

use App\Admin\Tools\AjaxStatistics;
use App\Router\Model;
use Component\SteamGamble\Classes\Core;

class GambleStatus extends Model
{
    public function getWithdrawUser($withdrawID)
    {
        $withdraw = $this->database->select("withdraw")->where(["withdraw.id" => $withdrawID])
            ->column(["withdraw.userID", "user.steamID"])
            ->leftJoin("user", ["withdraw.userID[=]user.id"])
            ->fetchRow();

        if(empty($withdraw["userID"]))
        {
            return NULL;
        }

        return $withdraw;
    }

    public function getDepositUser($withdrawID)
    {
        $deposit = $this->database->select("deposit")->where(["deposit.id" => $withdrawID])
            ->column(["deposit.userID", "user.steamID"])
            ->leftJoin("user", ["deposit.userID[=]user.id"])
            ->fetchRow();

        if(empty($deposit["userID"]))
        {
            return NULL;
        }

        return $deposit;
    }

    public function getUserTransactions(Core $gamble, $userID, $page = 1, $size = 50)
    {
        $total = $this->database->select("withdraw")->where(["userID" => $userID])->numRows();
        $total += $this->database->select("deposit")->where(["userID" => $userID])->numRows();

        $transactions = $gamble->statistic()->getTransactions($userID, $page, $size, false, false);

        foreach($transactions as $key => $transaction)
        {
            if($transaction["mode"] == "withdraw")
            {
                if($transaction["status"] == 2 || $transaction["status"] == 8)
                {
                    $transactions[$key]["status_level"] = "success";
                }
                elseif($transaction["status"] == 5 || $transaction["status"] == 7)
                {
                    $transactions[$key]["status_level"] = "danger";
                }
                elseif($transaction["status"] == 3 || $transaction["status"] == 6)
                {
                    $transactions[$key]["status_level"] = "warning";
                }
                elseif($transaction["status"] == 4)
                {
                    $transactions[$key]["status_level"] = "info";
                }
                else
                {
                    $transactions[$key]["status_level"] = "";
                }
            }
            elseif($transaction["mode"] == "deposit")
            {
                if($transaction["status"] == 0)
                {
                    $transactions[$key]["status_level"] = "info";
                }
                elseif($transaction["status"] == 1)
                {
                    $transactions[$key]["status_level"] = "success";
                }
                else
                {
                    $transactions[$key]["status_level"] = "";
                }
            }
        }

        return ["results" => $transactions, "current" => $page, "total" => ceil($total / $size)];
    }

    public function getTradeStats(AjaxStatistics $statistic, $botID = NULL)
    {
        $this->database->query("SET time_zone = '".$statistic->getTimezone()."'");

        $condition = ["dateline[>=]" => $statistic->getStartTime(), "dateline[<=]" => $statistic->getStopTime()];
        if(isset($botID))
        {
            $condition["botID"] = $botID;
        }

        $depositquery = $this->database->select("deposit")->column([
            ["str" => "COUNT(*)", "alias" => "tradecount"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(dateline), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where($condition)->group("date");

        $withdrawquery = $this->database->select("withdraw")->column([
            ["str" => "COUNT(*)", "alias" => "tradecount"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(dateline), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where($condition)->group("date");

        $data = $this->database->select([$depositquery, $withdrawquery], "tb")
            ->column(["date", "tradecount" => ["function" => "sum", "alias" => "value"]])
            ->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->language->get(["steamgamble", "label_tradestats"]));
    }

    public function getDepositStats(AjaxStatistics $statistic, $userID = NULL)
    {
        $this->database->query("SET time_zone = '".$statistic->getTimezone()."'");

        $condition = ["accepted[>=]" => $statistic->getStartTime(), "accepted[<=]" => $statistic->getStopTime(), "status" => 1];
        if(isset($userID))
        {
            $condition["userID"] = $userID;
        }

        $data = $this->database->select("deposit")->column([
            ["str" => "SUM(value) / 100", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(accepted), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where($condition)->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->language->get(["steamgamble", "label_depositstats"]));
    }

    public function getBetStats(AjaxStatistics $statistic, $userID = NULL)
    {
        $this->database->query("SET time_zone = '".$statistic->getTimezone()."'");

        $condition = ["status" => 1, "accepted[>=]" => $statistic->getStartTime(), "accepted[<=]" => $statistic->getStopTime()];
        if(isset($userID))
        {
            $condition["userID"] = $userID;
        }

        $data = $this->database->select("deposit")->column([
            ["str" => "COUNT(*)", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(accepted), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where($condition)->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->language->get(["steamgamble", "label_betstats"]));
    }

    public function getReferralUsers($referralID)
    {
        $userIDs = [];
        $users = $this->database->select("user")->column(["id"])->where(["referralID" => $referralID])->fetchAssoc();

        foreach($users as $user)
        {
            $userIDs[] = $user["id"];
        }

        return $userIDs;
    }
}