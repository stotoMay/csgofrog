<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Router\Controller;

use Component\Steam\Bot\Bot;
use Component\Steam\Core;
use Component\Steam\SteamFactory;
use Component\Steam\User\User;
use Component\SteamGamble\GambleFactory;

class SteamGamble extends Controller
{
    /**
     * @var \Component\SteamGamble\Admin\Model\SteamGamble
     */
    private $model;
    private $admin;
    /**
     * @var Core
     */
    private $steam;
    /**
     * @var \Component\SteamGamble\Classes\Core
     */
    private $gamble;

    public static function __info__()
    {
        return [
            "uniqname" => "steamgamble", "tracking" => true, "priority" => 20,
            "permission" => ["view" => "bool", "profit" => "bool", "trades" => "bool", "stats" => "bool"],
            "icon" => "money",
            "requirement" => [["application", "general", "admin_control_panel"], ["controller", "steamgamble", "view"]]
        ];
    }

    public function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("Component/SteamGamble/Admin/Model/SteamGamble");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));

        $this->steam = SteamFactory::getSteamCore();
        $this->gamble = GambleFactory::getGambleCore();
    }

    public function main()
    {
        if($this->getPermission("profit") != 1)
        {
            redirect("/admin/steamgamble/fee", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("SteamGamble/profit.tpl");

        $totalprofit = 0;
        foreach($this->gamble->games()->setGameHook("totalprofit") as $profit)
        {
            $totalprofit += $profit;
        }

        $todayprofit = 0;
        foreach($this->gamble->games()->setGameHook("todayprofit") as $profit)
        {
            $todayprofit += $profit;
        }

        $template->assignVar("totalprofit", $totalprofit);
        $template->assignVar("todayprofit", $todayprofit);

        return response($template, $this->request);
    }

    public function fee()
    {
        if($this->browser->user()->permission()->getComponent("SteamWeb", "request_fees") != 1)
        {
            redirect("/admin/steamgamble/stats", $this->request)->error($this->language->getMessage("permission"));
        }

        $feeitems = $this->model->getFeeItems();
        $this->steam->item()->filling($feeitems);

        $template = $this->theme->draw("SteamGamble/fee.tpl");

        $template->assignVar("feeitems", $feeitems);

        $user = new User($this->browser->user()->getColumn("steamID"));
        $template->assignVar("tradeurl", $this->steam->api()->user()->buildTradeURL($this->browser->user()
            ->getColumn("tradetoken")));

        return response($template, $this->request);
    }

    public function items2fees()
    {
        if($this->browser->user()->permission()->getComponent("SteamWeb", "request_fees") != 1)
        {
            redirect("/admin/steamgamble/stats", $this->request)->error($this->language->getMessage("permission"));
        }

        $bots = $this->app->database()->select("steam_bot")->column(["id"])->fetchAssoc();

        foreach($bots as $botdata)
        {
            $bot = Bot::getInstance($botdata["id"]);

            $inventory = $bot->action()->getOwnInventory(730, 2);

            $args = [$botdata["id"], &$inventory];
            $this->app->component()->setHook("onBotInventoryLoad", $args);
            $inventory = $inventory->toArray(false);
            $this->steam->item()->filling($inventory);

            print ("<br><br>");

            foreach($inventory as $key => $item)
            {
                if($key <= 15)
                {
                    continue;
                }

                $setdata = [
                    "botID" => $botdata["id"], "appID" => $item["appID"], "contextID" => $item["contextID"],
                    "market_name" => $item["market_name"], "assetID" => $item["assetID"], "amount" => 1,
                    "price" => $item["market_price"] * 100, "status" => 0, "dateline" => time()
                ];

                $this->app->database()->insert("fee")->set($setdata);
                print_r($setdata);
                print("<br>");
            }
        }
    }

    public function removefromfees()
    {
        if($this->browser->user()->permission()->getComponent("SteamWeb", "request_fees") != 1)
        {
            redirect("/admin/steamgamble/stats", $this->request)->error($this->language->getMessage("permission"));
        }

        $bots = $this->app->database()->select("fee")->where(["status" => 0])->group("botID")->column(["botID" => "botID"])->fetchAssoc();
        foreach($bots as $botdata)
        {
            $bot = \Component\Steam\Bot\Bot::getInstance($botdata["botID"]);

            $items = $this->app->database()->select("fee")->where(["status" => 0, "botID" => $botdata["botID"]])->fetchAssoc();

            $inventory = $bot->action()->getOwnInventory("730", "2")->toArray(false);

            foreach($items as $item)
            {
                foreach($inventory as $i)
                {
                    if($item["appID"] == $i["appID"] && $item["contextID"] == $i["contextID"] && $item["assetID"] == $i["assetID"])
                    {
                        continue 2;
                    }
                }

                $this->app->database()->update("fee")->set(["status" => 1, "userID" => 1])->where(["id" => $item["id"]]);
                echo "fee item #".$item["id"]." is not in bot anymore and is removed from the fee section<br>";
            }
        }
    }

    public function unselectfeeitems()
    {
        $itemlist = array_clear(explode(":", $this->request->input("items")));

        if(count($itemlist) == 0 || $this->browser->user()->permission()->getComponent("SteamWeb", "request_fees") != 1)
        {
            $response = ["success" => false, "message" => $this->language->getMessage("permission")];
        }
        else
        {
            $items = $this->model->fetchFeeItems($itemlist);

            if(count($items) == count($itemlist))
            {
                $this->model->deleteFeeItems($itemlist);

                $response = ["success" => true, "message" => $this->language->get(["steamgamble", "message", "unselect_success"])];
            }
            else
            {
                $response = ["success" => false, "message" => $this->language->get(["steamgamble", "message", "unselect_error"])];
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }

    public function stats()
    {
        if($this->getPermission("stats") != 1)
        {
            redirect("/admin/steamgamble", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("SteamGamble/stats.tpl");

        return response($template, $this->request);
    }

    public function trades()
    {
        if($this->getPermission("trades") != 1)
        {
            redirect("/admin/dashboard", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("SteamGamble/trades.tpl");

        $page = $this->request->input("page", 1, false, v()->integer()->min(1));
        $timestamp = $this->request->input("timestamp", time(), false, v()->integer()->positive());

        if($this->request->has("searchtime"))
        {
            $time = strtotime($this->request->input("searchtime"));

            if($time !== false)
            {
                $time -= $this->request->timezoneOffset();
                $page = $this->model->searchTradePage($time);
                $timestamp = time();
            }
            else
            {
                $this->printError($this->language->get(["steamweb", "message", "invalid_time"]));
            }
        }

        if($page == 1)
        {
            $timestamp = time();
        }

        $template->assignVar("pageurl", "/admin/steamgamble/trades?page=%p&timestamp=".$timestamp);
        $template->assignVar("trades", $this->model->getTradeHistory($this->gamble, $page, 50, $timestamp));

        return response($template, $this->request);
    }

    public function bot($botID)
    {
        $template = $this->theme->draw("SteamGamble/bot.tpl");

        $template->assignVar("botID", $botID);

        return response($template, $this->request);
    }

    public function user($userID)
    {
        $template = $this->theme->draw("SteamGamble/user.tpl");

        $user = $this->model->getUser($userID);

        if(!is_array($user))
        {
            return "";
        }

        $steamuser = new User($user["steamID"]);
        $steamdata = array_merge(["userID" => $user["id"]], $this->steam->api()->user()->getPlayerSummaries($user["steamID"])[0]);

        $template->assignVar("user", $steamdata);
        $template->assignVar("tradeurl", $this->steam->api()->user()->buildTradeURL($user["tradetoken"]));

        $profitgraph = $this->gamble->statistic()->getUserProfitGraph($userID, NULL, 250);
        $luckygraph = $this->gamble->statistic()->getUserLuckyGraph($userID, NULL, 250);

        $template->assignVar("profitgraph", $profitgraph);
        $template->assignVar("luckygraph", $luckygraph);

        return response($template, $this->request);
    }

    public function referral($referralID)
    {
        $template = $this->theme->draw("SteamGamble/referral.tpl");

        $template->assignVar("referralID", $referralID);

        return response($template, $this->request);
    }
}