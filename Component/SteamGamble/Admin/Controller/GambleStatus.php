<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Admin\Tools\AjaxStatistics;
use App\Router\Controller;

use Component\Steam\Core;
use Component\Steam\SteamFactory;
use Component\SteamGamble\GambleFactory;

class GambleStatus extends Controller
{
    /**
     * @var \Component\SteamGamble\Admin\Model\GambleStatus
     */
    private $model;
    private $admin;
    /**
     * @var Core
     */
    private $steam;
    /**
     * @var \Component\SteamGamble\Classes\Core
     */
    private $gamble;

    public static function __info__()
    {
        return [
            "uniqname" => "gamblestatus", "tracking" => true, "visible" => false
        ];
    }

    public function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("Component/SteamGamble/Admin/Model/GambleStatus");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));

        $this->steam = SteamFactory::getSteamCore();
        $this->gamble = GambleFactory::getGambleCore();
    }

    public function withdraw($withdrawID)
    {
        $template = $this->theme->draw("SteamGamble/withdraw.tpl");

        $user = $this->model->getWithdrawUser($withdrawID);

        if(empty($user))
        {
            redirect("/admin/steamgamble", $this->request);
        }

        $user = array_merge($user, $this->steam->api()->user()->getPlayerSummaries($user["steamID"])[0]);

        $template->assignVar("user", $user);
        $template->assignVar("withdrawID", $withdrawID);

        $withdraw = $this->gamble->statistic()->getWithdrawTrade($withdrawID);

        usort($withdraw["items"], function($a, $b) {
            return $b["price"]*100-$a["price"]*100;
        });

        $template->assignVar("withdraws", $withdraw);

        return response($template, $this->request);
    }

    public function deposit($depositID)
    {
        $template = $this->theme->draw("SteamGamble/deposit.tpl");

        $user = $this->model->getDepositUser($depositID);

        if(empty($user))
        {
            redirect("/admin/steamgamble", $this->request);
        }

        $user = array_merge($user, $this->steam->api()->user()->getPlayerSummaries($user["steamID"])[0]);

        $template->assignVar("user", $user);
        $template->assignVar("depositID", $depositID);

        $deposit = $this->gamble->statistic()->getDepositTrade($depositID);

        usort($deposit["items"], function($a, $b) {
            return $b["price"]*100-$a["price"]*100;
        });

        $template->assignVar("deposit", $deposit);

        return response($template, $this->request);
    }

    public function transactions($userID)
    {
        if($this->browser->user()->permission()->get("trades", "controller", "steamgamble") == 1)
        {
            $this->request->session()->closeSessionWrite();

            $page = $this->request->input("page", 1, false, v()->integer());
            $transactions = $this->model->getUserTransactions($this->gamble, $userID, $page, 10);

            $template = $this->theme->draw("SteamGamble/transactions.tpl");

            $template->assignVar("userID", $userID);
            $template->assignVar("transactions", $transactions["results"]);

            $json = ["success" => true, "content" => $template->parse(), "current" => $page, "total" => $transactions["total"]];
        }
        else
        {
            $json = ["success" => false];
        }

        return response(json_encode($json, JSON_HEX_QUOT | JSON_HEX_TAG), $this->request)->contentType("text/json");
    }

    public function tradestats($botID = NULL)
    {
        if($this->browser->user()->permission()->get("stats", "controller", "steamgamble") == 1)
        {
            $statistic = new AjaxStatistics($this->request, 0);

            $this->request->session()->closeSessionWrite();
            $this->model->getTradeStats($statistic, $botID);

            return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
        }

        return response(json_encode(["success" => false]), $this->request)->contentType("text/json");
    }

    public function depositstats($userID = NULL)
    {
        if($this->browser->user()->permission()->get("stats", "controller", "steamgamble") == 1)
        {
            $statistic = new AjaxStatistics($this->request, 0);

            $this->request->session()->closeSessionWrite();
            $this->model->getDepositStats($statistic, $userID);

            return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
        }

        return response(json_encode(["success" => false]), $this->request)->contentType("text/json");
    }

    public function betstats($userID = NULL)
    {
        if($this->browser->user()->permission()->get("stats", "controller", "steamgamble") == 1)
        {
            $statistic = new AjaxStatistics($this->request, 0);

            $this->request->session()->closeSessionWrite();
            $this->model->getBetStats($statistic, $userID);

            return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
        }

        return response(json_encode(["success" => false]), $this->request)->contentType("text/json");
    }

    public function profitstats($referralID = NULL)
    {
        if($this->browser->user()->permission()->get("profit", "controller", "steamgamble") == 1)
        {
            $statistic = new AjaxStatistics($this->request);

            $this->request->session()->closeSessionWrite();
            $this->app->database()->query("SET time_zone = '".$statistic->getTimezone()."'");

            if(isset($referralID))
            {
                $params = [$statistic, $this->model->getReferralUsers($referralID)];
                $graphs = $this->gamble->games()->setGameHook("profitofusergraph", $params);
            }
            else
            {
                $params = [$statistic];
                $graphs = $this->gamble->games()->setGameHook("profitgraph", $params);
            }

            $data = [];
            foreach($graphs as $graph)
            {
                foreach($graph as $row)
                {
                    if(isset($data[$row["date"]]))
                    {
                        $data[$row["date"]] += $row["value"];
                    }
                    else
                    {
                        $data[$row["date"]] = $row["value"];
                    }
                }
            }

            $totalprofit = [];
            foreach($data as $date => $value)
            {
                $totalprofit[] = ["date" => $date, "value" => $value];
            }

            usort($totalprofit, function($a, $b) {
                return strcasecmp($a['date'], $b['date']);
            });

            $statistic->addGraphData($totalprofit, $this->language->get(["steamgamble", "label_profit"]));

            return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
        }

        return response(json_encode(["success" => false]), $this->request)->contentType("text/json");
    }


    public function referralprofitstats($referralID)
    {
        return $this->profitstats($referralID);
    }

    public function referraldepositedstats($referralID)
    {
        if($this->browser->user()->permission()->get("stats", "controller", "steamgamble") == 1)
        {
            $statistic = new AjaxStatistics($this->request, 0);

            $this->request->session()->closeSessionWrite();
            $this->model->getDepositStats($statistic, $this->model->getReferralUsers($referralID));

            return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
        }

        return response(json_encode(["success" => false]), $this->request)->contentType("text/json");
    }

    public function referralbetstats($referralID)
    {
        if($this->browser->user()->permission()->get("stats", "controller", "steamgamble") == 1)
        {
            $statistic = new AjaxStatistics($this->request, 0);

            $this->request->session()->closeSessionWrite();
            $this->model->getBetStats($statistic, $this->model->getReferralUsers($referralID));

            return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
        }

        return response(json_encode(["success" => false]), $this->request)->contentType("text/json");
    }
}