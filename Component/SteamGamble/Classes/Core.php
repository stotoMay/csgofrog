<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Classes;

use App\Core\App;

class Core
{
    private $app;
    private $steam;
    private $web;

    private $statistic;
    private $games;

    public function __construct(App $app, \Component\Steam\Core $steam, \Component\SteamWeb\Classes\Core $web)
    {
        $this->app = $app;
        $this->steam = $steam;
        $this->web = $web;

        $this->statistic = new Statistic($this);
        $this->games = new Games($this);
    }

    public function statistic()
    {
        return $this->statistic;
    }

    public function games()
    {
        return $this->games;
    }

    public function app()
    {
        return $this->app;
    }

    public function steam()
    {
        return $this->steam;
    }

    public function web()
    {
        return $this->web;
    }
}