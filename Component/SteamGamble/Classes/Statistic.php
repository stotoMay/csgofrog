<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Classes;

use Component\SteamGamble\Exception\GamemodeNotFoundException;

class Statistic
{
    private $gamble;

    private $gamemodes;

    /**
     * @param Core $gamble
     */
    public function __construct(Core $gamble)
    {
        $this->gamble = $gamble;
    }

    /**
     * @param string $gamemode
     *
     * @return object
     *
     * @throws GamemodeNotFoundException
     */
    public function gamemode($gamemode)
    {
        if(!isset($this->gamemodes[strtolower($gamemode)]))
        {
            throw new GamemodeNotFoundException();
        }

        return $this->gamemodes[strtolower($gamemode)];
    }

    /**
     * @param string $gamemode
     * @param object $class
     */
    public function registerGamemode($gamemode, $class)
    {
        $this->gamemodes[strtolower($gamemode)] = $class;
    }

    /**
     * @return int
     */
    public function getRegisteredPlayerCount()
    {
        return $this->gamble->app()->database()->select("user")->numRows();
    }

    /**
     * @param $userID
     * @param int $page
     * @param int $size
     * @param bool $fetchitems
     * @param bool $successonly
     *
     * @return array
     */
    public function getTransactions($userID, $page = 1, $size = 50, $fetchitems = true, $successonly = true)
    {
        $depositquery = $this->gamble->app()->database()->select("deposit")
            ->column([["str" => "'deposit'", "alias" => "mode"], "id", "botID", "offerID", "value", "status", "dateline", "accepted"]);
        $withdrawquery = $this->gamble->app()->database()->select("withdraw")
            ->column([["str" => "'withdraw'", "alias" => "mode"], "id", "botID", "offerID", "value", "status", "dateline" => "dateline", "accepted"]);

        if($successonly === true)
        {
            $depositquery->where(["userID" => $userID, "status" => 1]);
            $withdrawquery->where(["userID" => $userID, "status" => 2]);
        }
        else
        {
            $depositquery->where(["userID" => $userID]);
            $withdrawquery->where(["userID" => $userID]);
        }

        $transactions = $this->gamble->app()->database()->select([$depositquery, $withdrawquery], "trade")
            ->order("dateline", "desc")
            ->limit(($page-1)*$size, $size)->fetchAssoc();

        if($fetchitems)
        {
            $withdrawIDs = [];
            $depositIDs = [];

            $depositItems = [];
            $withdrawItems = [];

            foreach($transactions as $transaction)
            {
                if($transaction["mode"] == "deposit")
                {
                    $depositIDs[] = $transaction["id"];
                    $depositItems[$transaction["id"]] = [];
                }
                elseif($transaction["mode"] == "withdraw")
                {
                    $withdrawIDs[] = $transaction["id"];
                    $withdrawItems[$transaction["id"]] = [];
                }
            }

            if(count($depositIDs) > 0)
            {
                $itemlist = $this->gamble->app()->database()->select("deposit_item")->where(["depositID" => $depositIDs])->fetchAssoc();
                $this->gamble->steam()->item()->filling($itemlist);

                foreach($itemlist as $item)
                {
                    $item["price"] = $item["price"] / 100;
                    $depositItems[$item["depositID"]][] = $item;
                }
            }

            if(count($withdrawIDs) > 0)
            {
                $itemlist = $this->gamble->app()->database()->select("withdraw_item")->where(["withdrawID" => $withdrawIDs])->fetchAssoc();
                $this->gamble->steam()->item()->filling($itemlist);

                foreach($itemlist as $item)
                {
                    $item["price"] = $item["price"] / 100;
                    $withdrawItems[$item["withdrawID"]][] = $item;
                }
            }

            foreach($transactions as $key => $transaction)
            {
                if($transaction["mode"] == "deposit")
                {
                    $transactions[$key]["items"] = $depositItems[$transaction["id"]];
                }
                elseif($transaction["mode"] == "withdraw")
                {
                    $transactions[$key]["items"] = $withdrawItems[$transaction["id"]];
                }
            }
        }

        return $transactions;
    }

    public function getTradeHistory($page = 1, $size = 50, $fetchitems = true, $successonly = true, $timestamp = NULL)
    {
        if(!isset($timestamp))
        {
            $timestamp = time();
        }

        $depositquery = $this->gamble->app()->database()->select("deposit")
            ->column([["str" => "'deposit'", "alias" => "mode"], "id", "botID", "offerID", "value", "status", "dateline", "accepted", "userID"]);
        $withdrawquery = $this->gamble->app()->database()->select("withdraw")
            ->column([["str" => "'withdraw'", "alias" => "mode"], "id", "botID", "offerID", "value", "status", "sent" => "dateline", "accepted", "userID"]);

        if($successonly === true)
        {
            $depositquery->where(["status" => 1, "dateline[<=]" => $timestamp]);
            $withdrawquery->where(["status" => 2, "dateline[<=]" => $timestamp]);
        }
        else
        {
            $depositquery->where(["dateline[<=]" => $timestamp]);
            $withdrawquery->where(["dateline[<=]" => $timestamp]);
        }

        $transactions = $this->gamble->app()->database()->select([$depositquery, $withdrawquery], "trade")
            ->order("dateline", "desc")
            ->limit(intval(($page-1)*$size), $size)->fetchAssoc();

        $userIDs = [];
        foreach($transactions as $transaction)
        {
            if(is_int(array_search($transaction["userID"], $userIDs)))
            {
                continue;
            }

            $userIDs[] = $transaction["userID"];
        }

        $userlist = $this->gamble->app()->database()->select("user")
            ->column(["id", "steamID"])->where(["id" => $userIDs])->fetchAssoc();
        $users = [];
        foreach($userlist as $user)
        {
            $users[$user["id"]] = array_merge(["userID" => $user["id"]], $this->gamble->steam()->api()->user()->getPlayerSummaries($user["steamID"])[0]);
        }

        foreach($transactions as $key => $transaction)
        {
            $transactions[$key]["user"] = $users[$transaction["userID"]];
        }

        if($fetchitems)
        {
            $withdrawIDs = [];
            $depositIDs = [];

            $depositItems = [];
            $withdrawItems = [];

            foreach($transactions as $transaction)
            {
                if($transaction["mode"] == "deposit")
                {
                    $depositIDs[] = $transaction["id"];
                    $depositItems[$transaction["id"]] = [];
                }
                elseif($transaction["mode"] == "withdraw")
                {
                    $withdrawIDs[] = $transaction["id"];
                    $withdrawItems[$transaction["id"]] = [];
                }
            }

            if(count($depositIDs) > 0)
            {
                $itemlist = $this->gamble->app()->database()->select("deposit_item")->where(["depositID" => $depositIDs])->fetchAssoc();
                $this->gamble->steam()->item()->filling($itemlist);

                foreach($itemlist as $item)
                {
                    $item["price"] = $item["price"] / 100;
                    $depositItems[$item["depositID"]][] = $item;
                }
            }

            if(count($withdrawIDs) > 0)
            {
                $itemlist = $this->gamble->app()->database()->select("withdraw_item")->where(["withdrawID" => $withdrawIDs])->fetchAssoc();
                $this->gamble->steam()->item()->filling($itemlist);

                foreach($itemlist as $item)
                {
                    $item["price"] = $item["price"] / 100;
                    $withdrawItems[$item["withdrawID"]][] = $item;
                }
            }

            foreach($transactions as $key => $transaction)
            {
                if($transaction["mode"] == "deposit")
                {
                    $transactions[$key]["items"] = $depositItems[$transaction["id"]];
                }
                elseif($transaction["mode"] == "withdraw")
                {
                    $transactions[$key]["items"] = $withdrawItems[$transaction["id"]];
                }
            }
        }

        return $transactions;
    }

    /**
     * @param $withdrawID
     *
     * @return false|array
     */
    public function getWithdrawTrade($withdrawID)
    {
        $before = [];
        $withdraw = $this->gamble->app()->database()->select("withdraw")->where(["id" => $withdrawID])->fetchRow();
        $after = [];

        if(empty($withdraw))
        {
            return false;
        }

        $status = $withdraw["status"];

        $tmp = $withdraw;
        while(true)
        {
            if(empty($tmp["parentID"]))
            {
                break;
            }
            else
            {
                $tmp = $this->gamble->app()->database()->select("withdraw")->where(["id" => $tmp["parentID"]])->fetchRow();

                $before[] = $tmp;
            }
        }

        $tmp = $withdraw;
        while(true)
        {
            $tmp = $this->gamble->app()->database()->select("withdraw")->where(["parentID" => $tmp["id"]])->fetchRow();

            if(empty($tmp))
            {
                break;
            }

            $after[] = $tmp;
            $status = $tmp["status"];
        }

        $withdraws = array_reverse(array_merge($before, [$withdraw], $after));

        $lastwithdraw = $withdraws[0];
        $lastwithdraw["items"] = [];
        unset($withdraws[0]);

        $itemlist = $this->gamble->app()->database()->select("withdraw_item")->where(["withdrawID" => $lastwithdraw["id"]])->fetchAssoc();
        $this->gamble->steam()->item()->filling($itemlist);

        foreach($itemlist as $item)
        {
            $item["price"] = $item["price"] / 100;
            $lastwithdraw["items"][] = $item;
        }

        $lastwithdraw["lateststatus"] = $status;
        $lastwithdraw["history"] = array_values($withdraws);

        return $lastwithdraw;
    }

    /**
     * @param $depositID
     *
     * @return false|array
     */
    public function getDepositTrade($depositID)
    {
        $deposit = $this->gamble->app()->database()->select("deposit")->where(["id" => $depositID])->fetchRow();

        if(!is_array($deposit))
        {
            return false;
        }

        $deposit["items"] = [];

        $itemlist = $this->gamble->app()->database()->select("deposit_item")->where(["depositID" => $depositID])->fetchAssoc();
        $this->gamble->steam()->item()->filling($itemlist);

        foreach($itemlist as $item)
        {
            $item["price"] = $item["price"] / 100;
            $deposit["items"][] = $item;
        }

        return $deposit;
    }

    public function getUserProfitGraph($userID, $start = NULL, $maxlength = NULL)
    {
        $params = [$userID];
        $profitgraphrows = $this->gamble->games()->setGameHook("userprofitgraph", $params);

        $profitgraphvalues = [];
        if(!isset($start))
        {
            $profitgraphvalues[] = 0;
        }

        $profitgraph = [];
        $lastvalue = 0;

        foreach($profitgraphrows as $row)
        {
            $profitgraph = array_merge($profitgraph, $row);
        }

        usort($profitgraph, function($a, $b) {
            return $a["dateline"] - $b["dateline"];
        });

        foreach($profitgraph as $row)
        {
            $lastvalue += $row["value"];

            if(isset($start) && $start <= $row["dateline"])
            {
                continue;
            }

            $profitgraphvalues[] = $lastvalue;
        }

        $profitgraphdata = [];
        $length = count($profitgraphvalues);

        if(!isset($maxlength))
        {
            $maxlength = $length;
        }

        $lastkey = 0;

        foreach(range(0, $length-1,  ceil($length/$maxlength)) as $key)
        {
            $profitgraphdata[] = $profitgraphvalues[$key];
            $lastkey = $key;
        }

        if($lastkey != $length-1)
        {
            $profitgraphdata[] = $profitgraphvalues[$length-1];
        }

        return $profitgraphdata;
    }

    public function getUserLuckyGraph($userID, $start = NULL, $maxlength = NULL)
    {
        $params = [$userID];
        $luckygraphrows = $this->gamble->games()->setGameHook("userluckygraph", $params);

        $luckygraphvalues = [];
        if(!isset($start))
        {
            $luckygraphvalues[] = 0;
        }

        $luckygraph = [];
        $lastvalue = 0;

        foreach($luckygraphrows as $row)
        {
            $luckygraph = array_merge($luckygraph, $row);
        }

        usort($luckygraph, function($a, $b) {
            return $a["dateline"] - $b["dateline"];
        });

        foreach($luckygraph as $row)
        {
            $lastvalue += $row["value"];

            if(isset($start) && $start <= $row["dateline"])
            {
                continue;
            }

            $luckygraphvalues[] = $lastvalue;
        }

        $luckygraphdata = [];
        $length = count($luckygraphvalues);
        $lastkey = 0;

        if(!isset($maxlength))
        {
            $maxlength = $length;
        }

        foreach(range(0, $length-1,  ceil($length/$maxlength)) as $key)
        {
            $luckygraphdata[] = $luckygraphvalues[$key];
            $lastkey = $key;
        }

        if($lastkey != $length-1)
        {
            $luckygraphdata[] = $luckygraphvalues[$length-1];
        }

        return $luckygraphdata;
    }
}