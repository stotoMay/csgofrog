<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble\Classes;

use Component\SteamGamble\Exception\GamemodeException;
use Component\SteamGamble\Exception\GamemodeNotFoundException;

class Games
{
    private $gamble;

    private $gamemodes;
    private $game;

    public function __construct(Core $gamble)
    {
        $this->gamble = $gamble;
        $this->gamemodes = [];
        $this->game = [];

        $folder = "/Component/SteamGamble/Gamemodes/";
        $gamemodes = get_dir_folders(base_dir($folder));

        foreach($gamemodes as $gamemode)
        {
            $this->initGamemode($gamemode, $folder.$gamemode."/");
        }
    }

    /**
     * @param string $gamemode
     * @param string $folder
     *
     * @throws GamemodeException
     * @throws \App\Core\Exception\InvalidArgumentException
     */
    public function initGamemode($gamemode, $folder)
    {
        if(!file_exists(base_dir($folder.$gamemode.".php")))
        {
            return;
        }

        $namespace = convert_folder_namespace($folder).'\\';
        $mainclass = $namespace.$gamemode;

        if(!class_exists($mainclass))
        {
            throw new GamemodeException("main class of '".$gamemode."' not found");
        }

        if(get_parent_class($mainclass) != 'Component\\SteamGamble\\Parent\\Gamemode')
        {
            throw new GamemodeException("invalid main class of '".$gamemode."'");
        }


        $mainclass = new $mainclass($this->gamble);

        if(is_callable([$mainclass, "init"]))
        {
            call_user_func([$mainclass, "init"]);
        }

        $this->gamemodes[strtolower($gamemode)] = $mainclass;

        $websocketclass = $namespace."Websocket";
        if(class_exists($websocketclass) && get_parent_class($websocketclass) == 'Component\\SteamGamble\\Parent\\Websocket')
        {
            $websocket = new $websocketclass($this->gamble);

            $methodBlacklist = ["__construct"];
            $methods = get_class_methods($websocketclass);
            array_remove($methods, $methodBlacklist);

            foreach($methods as $method)
            {
                $this->gamble->web()->websocket()->registerInterfaceMethod($method, [$websocket, $method], $gamemode);
            }
        }

        $gameclass = $namespace."Game";
        if(class_exists($gameclass) && get_parent_class($gameclass) == 'Component\\SteamGamble\\Parent\\Game')
        {
            $game = new $gameclass($this->gamble);
            $this->game[strtolower($gamemode)] = $game;
        }

        $statisticclass = $namespace."Statistic";
        if(class_exists($statisticclass) && get_parent_class($statisticclass) == 'Component\\SteamGamble\\Parent\\Statistic')
        {
            $statistic = new $statisticclass($this->gamble);
            $this->gamble->statistic()->registerGamemode($gamemode, $statistic);
        }
    }

    /**
     * @param string $gamemode
     *
     * @return object
     *
     * @throws GamemodeNotFoundException
     */
    public function gamemode($gamemode)
    {
        if(!isset($this->game[strtolower($gamemode)]))
        {
            throw new GamemodeNotFoundException();
        }

        return $this->game[strtolower($gamemode)];
    }

    /**
     * @param string $game
     *
     * @return bool
     */
    public function isGameLoaded($game)
    {
        return isset($this->gamemodes[strtolower($game)]);
    }

    /**
     * @param string $name
     * @param array $params
     *
     * @return array
     */
    public function setGameHook($name, &$params = [])
    {
        $results = [];
        foreach($this->gamemodes as $gamemode => $core)
        {
            if(!is_callable([$core, $name]))
            {
                continue;
            }

            $results[] = call_user_func_array([$core, $name], $params);
        }

        return $results;
    }

    /**
     * @param $userID
     *
     * @return array
     */
    public function getUserData($userID)
    {
        $user = $this->gamble->app()->database()->select("user")
            ->column(["user.id" => "userID", "steamID", "group.name" => "rank", "group.color"])
            ->leftJoin("group", ["group.id[=]user.groupID"])
            ->where(["user.id" => $userID])->fetchRow();

        if($user["color"] == "#000000")
        {
            $user["color"] = NULL;
        }

        return array_merge($this->gamble->steam()->api()->user()->getPlayerSummaries($user["steamID"])[0], $user);
    }
}