<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace Component\SteamGamble;

use App\Component\Parent\Component;
use App\Core\App;
use App\HTTP\Browser;
use App\Theme\Theme;
use Component\Steam\Core;
use Component\Steam\Item\ItemList;

class SteamGamble extends Component
{
    /**
     * @var \Component\SteamGamble\Classes\Core
     */
    private $gamble;

    public static function info()
    {
        return [
            "setting" => [
                "min_item_price" => "^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$", "forbidden_matches" => "textarea",
                "allow_cases" => "bool", "max_deposit_trades" => "int"
            ],
            "language" => ["english" => "Language/English.lang"]
        ];
    }

    public function onEnable()
    {
        $controllerList = ["SteamGamble", "GambleStatus"];

        foreach($controllerList as $controller)
        {
            if(!class_exists('App\\Admin\\Controller\\'.$controller, false))
            {
                include(base_dir("Component/SteamGamble/Admin/Controller/".$controller.".php"));
                $this->app->router()->initController('\\App\\Admin\\Controller\\'.$controller);
            }
        }

        $this->app->permission()->registerPermission("request_fees", "bool", "component", "SteamWeb", 0);
        $this->app->permission()->registerPermission("refund", "bool", "component", "SteamWeb", 0);
    }

    public function onApplicationFinish()
    {

    }

    /**
     * @param $steam Core
     */
    public function onSteamWebLoad($steam, $web)
    {
        GambleFactory::buildCore($this->app, $steam, $web);

        $this->gamble = GambleFactory::getGambleCore();
    }

    /**
     * @param Theme $theme
     *
     * @throws \App\Core\Exception\FolderNotFoundException
     * @throws \App\Core\Exception\InvalidArgumentException
     * @throws \App\Theme\Exception\FolderNotFoundException
     */
    public function onPageload($theme)
    {
        $theme->assignVar("gamblestats", $this->gamble->statistic());
        $theme->registerFolder("Component/SteamGamble/Admin/Template", "SteamGamble/");

        $theme->assignFunction("renderWithdrawStatus", function($args, $withdrawID)
        {
            /**
             * @var Theme $theme
             */
            $theme = $args[0];
            /**
             * @var App $app
             */
            $app = $args[1];

            $template = $theme->draw("SteamGamble/withdraw_status.tpl");

            if(!is_array($withdrawID))
            {
                $withdraw = $app->database()->select("withdraw")->where(["id" => $withdrawID])->fetchRow();
                while($withdraw)
                {
                    $query = $app->database()->select("withdraw")->where(["parentID" => $withdraw["id"]])->fetchRow();

                    if(empty($query))
                    {
                        break;
                    }
                    else
                    {
                        $withdraw = $query;
                    }
                }
            }
            else
            {
                $withdraw = $withdrawID;
            }

            $template->assignVar("withdraw", $withdraw);

            return $template->parse();
        });

        $theme->assignFunction("renderDepositStatus", function($args, $depositID)
        {
            /**
             * @var Theme $theme
             */
            $theme = $args[0];
            /**
             * @var App $app
             */
            $app = $args[1];

            $template = $theme->draw("SteamGamble/deposit_status.tpl");

            if(!is_array($depositID))
            {
                $deposit = $app->database()->select("deposit")->where(["id" => $depositID])->fetchRow();
            }
            else
            {
                $deposit = $depositID;
            }

            $template->assignVar("deposit", $deposit);

            return $template->parse();
        });
    }

    /**
     * @param int $botID
     * @param ItemList $items
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function onBotInventoryLoad($botID, &$items)
    {
        $assetIDs = [];

        foreach($items as $item)
        {
            $assetIDs[] = $item->getAssetID();
        }

        if(count($assetIDs) > 0)
        {
            $botitems = $this->app->database()->select("market")
                ->column(["appID", "contextID", "assetID"])
                ->where(["assetID" => $assetIDs])->fetchAssoc();
            $botitems = array_merge($botitems, $this->app->database()->select("withdraw_item")
                ->column(["appID", "contextID", "assetID"])
                ->where(["assetID" => $assetIDs, "status" => 0])->fetchAssoc());
            $botitems = array_merge($botitems, $this->app->database()->select("storage")
                ->column(["appID", "contextID", "assetID"])
                ->where(["assetID" => $assetIDs])->fetchAssoc());
            $botitems = array_merge($botitems, $this->app->database()->select("fee")
                ->column(["appID", "contextID", "assetID"])
                ->where(["assetID" => $assetIDs, "status" => 0])->fetchAssoc());

            foreach($botitems as $item)
            {
                $items->removeAsset($item["appID"], $item["contextID"], $item["assetID"]);
            }
        }

        $deposititems = $this->app->database()->select("deposit_item")->column(["appID", "market_name"])
            ->leftJoin("deposit", ["deposit.id[=]deposit_item.depositID"])
            ->where(["deposit.botID" => $botID, "OR" => ["deposit.status" => 0,
                "AND" => ["deposit.status" => 1, "deposit.dateline[>=]" => time() - 300]]])
            ->group("market_name")->fetchAssoc();

        foreach($deposititems as $item)
        {
            $items->removeMarketItem($item["appID"], $item["market_name"]);
        }

        $args = [$items];
        $this->gamble->games()->setGameHook("onBotInventoryLoad", $args);
    }

    /**
     * @param int $botID
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function onSteamBotDelete($botID)
    {
        $withdraws = $this->app->database()->select("withdraw")
            ->column(["id"])->where(["botID" => $botID, "status" => [0, 1]])->fetchAssoc();

        foreach($withdraws as $withdraw)
        {
            $this->app->database()->update("withdraw")->set(["status" => 7])->where(["id" => $withdraw["id"]]);
            $this->app->database()->update("withdraw_item")->set(["status" => 5])
                ->where(["withdrawID" => $withdraw["id"]]);
        }

        $this->app->database()->update("deposit")->set(["status" => 2])->where(["botID" => $botID, "status" => 0]);
        $this->app->database()->update("fee")->set(["status" => 1])->where(["status" => 0, "botID" => $botID]);

        $this->app->database()->delete("storage")->where(["botID" => $botID]);
        $this->app->database()->delete("market")->where(["botID" => $botID]);

        $args = [$botID];
        $this->gamble->games()->setGameHook("onSteamBotDelete", $args);
    }

    public function pageSteamUserProfile($args, $userID)
    {
        $controller = new \App\Admin\Controller\SteamGamble($args[2], $args[3], $args[0], $args[4], $args[1]);

        $template = "";
        if(method_exists($controller, "user") && is_callable([$controller, "user"]))
        {
            $template = (string)$controller->user($userID);
        }

        $params = [$args, $userID];
        $templates = array_merge([$template], $this->gamble->games()->setGameHook("pageUserProfile", $params));

        return implode("\n", $templates);
    }

    public function pageBotAccount($args, $botID)
    {
        $controller = new \App\Admin\Controller\SteamGamble($args[2], $args[3], $args[0], $args[4], $args[1]);

        if(method_exists($controller, "bot") && is_callable([$controller, "bot"]))
        {
            return (string) $controller->bot($botID);
        }
        else
        {
            return "";
        }
    }

    public function pageReferralStats($args, $referralID)
    {
        $controller = new \App\Admin\Controller\SteamGamble($args[2], $args[3], $args[0], $args[4], $args[1]);

        if(method_exists($controller, "referral") && is_callable([$controller, "referral"]))
        {
            return (string) $controller->referral($referralID);
        }
        else
        {
            return "";
        }
    }
}