<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Router;

use App\HTTP\Browser;
use App\HTTP\Request;
use App\Core\App;
use App\Router\Exception\HTTPException;
use App\Theme\Theme;

class Router
{
    private $app;
    private $routes;

    private $controllerList;
    private $classList;

    /**
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->routes = [];
        $this->app = $app;

        $this->controllerList = [];
        $this->classList = [];
    }

    /**
     * @param array $routes
     *
     * @throws \App\Core\Exception\FolderNotFoundException
     * @throws \App\Core\Exception\InvalidArgumentException
     */
    public function setup($routes)
    {
        if(!is_array($routes))
        {
            return;
        }

        $routes["namespace"] = is_array($routes["namespace"]) ? $routes["namespace"] : [];
        $routes["controller"] = is_array($routes["controller"]) ? $routes["controller"] : [];

        foreach($routes["namespace"] as $path => $value)
        {
            $this->bindNamspace($path, $value["namespace"], $value["view"]);
        }

        foreach($routes["controller"] as $path => $value)
        {
            $this->bindController($path, $value["class"], $value["view"]);
        }
    }

    /**
     * @param string $path
     * @param string $namespace
     * @param string $view
     *
     * @throws \App\Core\Exception\FolderNotFoundException
     * @throws \App\Core\Exception\InvalidArgumentException
     */
    public function bindNamspace($path, $namespace, $view)
    {
        $files = get_dir_files(base_dir(convert_namespace_folder($namespace)));

        foreach($files as $f)
        {
            $this->initController('\\'.convert_folder_namespace($namespace).'\\'.str_replace(".php", "", $f));
        }

        $path = array_clear(explode("/", $path));
        $keys = array_between($path, "path");

        if(count($keys) == 0)
        {
            $this->routes["namespace"] = '\\'.convert_folder_namespace($namespace);
            $this->routes["view"] = filter_folder($view);
        }
        else
        {
            array_set($this->routes, $keys, ["namespace" => '\\'.convert_folder_namespace($namespace),
                "view" => filter_folder($view)]);
        }
    }

    /**
     * @param string $path
     * @param string $controller
     * @param string $view
     *
     * @throws \App\Core\Exception\InvalidArgumentException
     */
    public function bindController($path, $controller, $view)
    {
        $controller = convert_folder_namespace($controller);

        if(!class_exists($controller))
        {
            return;
        }

        $this->initController($controller);

        $path = array_clear(explode("/", $path));
        $keys = array_between($path, "path");

        array_set($this->routes, $keys, [
            "controller" => '\\'.convert_folder_namespace($controller), "view" => filter_folder($view)
        ]);
    }

    /**
     * @param string $classname
     */
    public function initController($classname)
    {
        if(!class_exists($classname))
        {
            return;
        }

        if(get_parent_class($classname) == 'App\\Router\\Controller')
        {
            if(method_exists($classname, "__info__") && is_callable([$classname, "__info__"]))
            {
                $info = call_user_func([$classname, "__info__"]);

                if(!is_array($info) || empty($info["uniqname"]))
                {
                    return;
                }

                $uniqname = $info["uniqname"];

                $info["setting"] = is_array($info["setting"]) ? $info["setting"] : [];
                $info["permission"] = is_array($info["permission"]) ? $info["permission"] : [];

                foreach($info["setting"] as $setting => $value)
                {
                    if(is_array($value))
                    {
                        $this->app->setting()->registerSetting($setting, $value[0], "controller", $uniqname, $value[1]);
                    }
                    else
                    {
                        $this->app->setting()->registerSetting($setting, $value, "controller", $uniqname);
                    }
                }

                foreach($info["permission"] as $permission => $value)
                {
                    if(is_array($value))
                    {
                        $this->app->permission()
                            ->registerPermission($permission, $value[0], "controller", $uniqname, $value[1]);
                    }
                    else
                    {
                        $this->app->permission()->registerPermission($permission, $value, "controller", $uniqname);
                    }
                }

                $this->controllerList[$uniqname] = $classname;
            }

            $this->classList[] = $classname;
        }
    }

    /**
     * @return array
     */
    public function getControllerList()
    {
        return $this->controllerList;
    }

    public function getClassList()
    {
        return $this->classList;
    }

    /**
     * @param Request $request
     *
     * @return string
     * @throws \App\Database\Exception\ParseException
     */
    public function resolve(Request $request)
    {
        if($this->app->setting()->get("ssl", "application", "general") == 1 && !$request->isSecureUrl())
        {
            redirect($request->domain("https").implode("/", $request->path()), $request);
        }
        elseif($this->app->setting()->get("ssl", "application", "general") != 1 && $request->isSecureUrl())
        {
            redirect($request->domain("http").implode("/", $request->path()), $request);
        }

        $path = $request->path();

        if($this->app->setting()->get("maintenance", "application", "general") == 1 && $path[0] != "admin")
        {
            return $this->app->language()->get("maintenance");
        }

        if($this->app->database()->select("ban_ip")->where(["ip" => $request->ip(), "OR" => ["expire" => NULL, "expire[>=]" => time()]])->exists())
        {
            return $this->app->language()->get("ip_banned");
        }

        $route = $this->routes;
        foreach($path as $key => $value)
        {
            if(isset($route["path"][$value]))
            {
                $route = $route["path"][$value];
                unset($path[$key]);
            }
            else
            {
                break;
            }
        }

        $path = array_values($path);

        if(isset($route["namespace"]))
        {
            $controller = $route["namespace"].'\\'.(empty($path[0]) ? "Main" : $path[0]);
            $action = empty($path[1]) ? "main" : $path[1];
            unset($path[0]);
            unset($path[1]);
            $args = array_values($path);
        }
        else
        {
            $controller = $route["controller"];
            $action = empty($path[0]) ? "main" : $path[0];
            unset($path[0]);
            $args = array_values($path);
        }

        $view = $route["view"];

        if(!class_exists($controller))
        {
            $classes = get_declared_classes();
            $key = array_search(strtolower($controller), array_map("strtolower", $classes));
            if(is_int($key))
            {
                $controller = $classes[$key];
            }
        }

        try
        {
            return $this->drawController($request, $controller, $action, $args, $view);
        }
        catch(HTTPException $e)
        {
            return $this->drawError($e->getCode(), $request);
        }
    }

    /**
     * @param Request $request
     * @param string $controller
     * @param string $action
     * @param array $args
     * @param string $view
     *
     * @return mixed
     * @throws HTTPException
     */
    public function drawController(Request $request, $controller, $action, $args, $view)
    {
        $browser = new Browser($request, $this->app);

        if(class_exists($controller) && get_parent_class($controller) == 'App\\Router\\Controller' && $action[0] != "_")
        {
            $message = new Message($request);

            $themeArgs = [$this->app, $request, $browser, $message];
            $theme = new Theme($view, base_dir($this->app->config()->getThemeCacheDirectory()), $themeArgs);

            $this->assignThemeFunctions($theme);

            $theme->assignVar("path", $request->path());
            $theme->assignVar("message", $message);
            $theme->assignVar("browser", $browser);

            $cookienames = $this->app->config()->getCookieNames();
            foreach($cookienames as $key => $value)
            {
                $cookienames[$key] = $this->app->config()->getCookiePrefix().$value;
            }
            $theme->assignVar("cookiename", $cookienames);

            $hookargs = [$theme, $request, $browser];
            $this->app->component()->setHook("onPageload", $hookargs);
        }
        else
        {
            throw new HTTPException(404, "invalid controller or action");
        }

        /**
         * @var \App\Router\Controller $class
         */
        $class = new $controller($request, $browser, $theme, $message, $this->app);

        if(!is_callable([$class, $action]))
        {
            throw new HTTPException(404, "action not callable");
        }

        if(method_exists($class, "__call"))
        {
            $parameterCount = 0;
        }
        else
        {
            $method = new \ReflectionMethod($class, $action);

            $parameterCount = $method->getNumberOfRequiredParameters();
        }

        if($parameterCount <= count($args))
        {
            $result = call_user_func_array([$class, $action], $args);

            if(is_object($result) && method_exists($result, "__tostring"))
            {
                if(get_class($result) == 'App\\Router\\Response')
                {
                    $result = call_user_func([$result, "output"]);
                }
                else
                {
                    $result = call_user_func([$result, "__tostring"]);
                }
            }

            if(method_exists($controller, "__info__") && is_callable([$controller, "__info__"]))
            {
                $info = call_user_func([$controller, "__info__"]);

                if(is_array($info) && is_array($info["requirement"]) && !$browser->user()->permission()->hasRequirements($info["requirement"]))
                {
                    throw new HTTPException(403, "permission denied");
                }

                if(is_array($info) && $info["tracking"] === true || (is_array($info["tracking"]) && is_int(array_search($action, $info["tracking"]))))
                {
                    $referer = NULL;

                    if(strpos($request->referer(), $request->domain()) === false)
                    {
                        $referer = $request->referer();
                    }

                    $this->app->database()->insert("activity")->set([
                        "browserID" => $browser->getBrowserID(),
                        "userID" => $browser->isLogin() ? $browser->getUserID() : NULL,
                        "request" => implode("/", $request->path()), "referer" => $referer, "dateline" => time()
                    ]);
                }
            }

            $this->app->component()->setHook("onApplicationFinish");

            return $result;
        }

        throw new HTTPException(404, "page not found");
    }

    public function drawError($statusCode, Request $request)
    {
        $browser = new Browser($request, $this->app);
        $theme = new Theme($this->app->config()->getErrorTemplateDirectory(), base_dir($this->app->config()->getThemeCacheDirectory()), [$this->app, $request, $browser]);

        $this->assignThemeFunctions($theme);

        return response($theme->draw($statusCode.".tpl"), $request)->statusCode($statusCode)->__tostring();
    }

    private function assignThemeFunctions(Theme $theme)
    {
        $themefuncs = get_dir_files(base_dir("/App/Theme/Function"));
        foreach($themefuncs as $file)
        {
            $name = str_replace(".php", "", $file);
            $closure = include(base_dir("/App/Theme/Function/".$file));

            if(is_callable($closure))
            {
                $theme->assignFunction($name, $closure);
            }
        }

        return $theme;
    }
}