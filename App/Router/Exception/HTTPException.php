<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Router\Exception;

use Exception;
use Throwable;

class HTTPException extends Exception
{
    public function __construct($code = 404, $message = "", Throwable $previous = NULL)
    {
        parent::__construct($message, $code, $previous);
    }
}