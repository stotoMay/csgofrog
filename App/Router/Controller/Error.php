<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Router\Controller;

use App\Router\Controller;

class Error extends Controller
{
    public function __init__()
    {

    }

    function display($code)
    {
        $code = (int)$code;
        $allowedCodes = [400, 401, 402, 403, 404, 500];
        if(!is_int(array_search($code, $allowedCodes)))
        {
            redirect("/error/display/404", $this->request);
        }

        return response($this->theme->draw($code.".tpl"), $this->request)->statusCode($code);
    }
}