<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Router;

use App\Core\App;
use App\HTTP\Browser;
use App\HTTP\Request;
use App\Router\Exception\ModelNotFoundException;
use App\Theme\Theme;

abstract class Controller
{
    protected $config;
    protected $app;

    protected $request;
    protected $language;
    protected $theme;
    protected $message;
    protected $logger;

    protected $setting;
    protected $permission;

    protected $user;
    protected $browser;

    public function __construct(Request $request, Browser $browser, Theme $theme, Message $message, App $app)
    {
        $this->app = $app;
        $this->config = $app->config();

        $this->request = $request;
        $this->language = $app->language();
        $this->theme = $theme;
        $this->message = $message;
        $this->logger = $app->logger();

        $this->setting = $app->setting();
        $this->permission = $app->permission();

        $this->browser = $browser;

        $this->__init__();
    }

    /**
     * @param string $classname
     *
     * @return object
     *
     * @throws ModelNotFoundException
     */
    protected function getModel($classname)
    {
        $class = 'App\Admin'.real_class($classname);

        if(class_exists($class) && get_parent_class($class) == 'App\\Router\\Model')
        {
            return new $class($this->app);
        }
        else
        {
            throw new ModelNotFoundException();
        }
    }

    /**
     * @param string $name
     *
     * @return string
     */
    protected function getSetting($name)
    {
        return $this->setting->getController($this->getID(), $name);
    }

    /**
     * @param string $name
     *
     * @return null|string
     */
    protected function getPermission($name)
    {
        if(!$this->browser->isLogin())
        {
            return NULL;
        }

        return $this->browser->user()->permission()->getController($this->getID(), $name);
    }


    /**
     * @param string $msg
     */
    protected function printInfo($msg)
    {
        if(is_array($msg))
        {
            foreach($msg as $m)
            {
                $this->message->addInfo($m);
            }

            return;
        }

        $this->message->addInfo($msg);
    }

    /**
     * @param string|array $msg
     */
    protected function printError($msg)
    {
        if(is_array($msg))
        {
            foreach($msg as $m)
            {
                $this->message->addError($m);
            }

            return;
        }

        $this->message->addError($msg);
    }


    protected function auth($redirecturl = "/")
    {
        if(!$this->browser->isLogin())
        {
            redirect($redirecturl, $this->request)->error($this->language->getMessage("session_expired"))->keepFormat();
        }

        if($this->browser->user()->isBanned())
        {
            $this->browser->destroyLogin();

            redirect($redirecturl, $this->request)->error($this->language->getMessage("banned"))->keepFormat();
        }
    }

    protected function authAdmin($redirecturl = "/", $authurl = "/")
    {
        $this->auth($authurl);

        if($this->browser->user()->permission()->get("admin_control_panel", "application", "general") == 1)
        {
            if($this->request->session()->has("adminsession"))
            {
                return;
            }
            else
            {
                redirect("/admin/?redirect=".rawurlencode(implode("/", $this->request->path())), $this->request)->error($this->language->getMessage("session_expired"));
            }
        }

        redirect($redirecturl, $this->request)->error($this->language->getMessage("permission"));
    }

    /**
     * @return string
     */
    private function getID()
    {
        if(method_exists(get_class($this), "__info__"))
        {
            $info = call_user_func([get_class($this), "__info__"]);
        }
        else
        {
            $info = [];
        }

        return $info["uniqname"];
    }

    protected function __init__()
    {

    }
}