<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Router;

use App\HTTP\Request;

class Redirect
{
    private $dst;
    private $request;

    private $info;
    private $error;

    public function __construct($dst, Request $request)
    {
        $this->dst = $dst;
        $this->request = $request;

        $this->info = [];
        $this->error = [];
    }

    public function __destruct()
    {
        $this->request->session()->flash("error", $this->error);
        $this->request->session()->flash("info", $this->info);

        set_hook("onApplicationFinish");

        header("location: ".$this->dst);
        exit();
    }

    /**
     * @return Redirect
     */
    public function keepFormat()
    {
        if(empty($this->request->format()))
        {
            return $this;
        }

        $dst = explode("/", $this->dst);
        $last = array_last($dst);
        if(empty($last))
        {
            $filename = "main.".$this->request->format();
        }
        elseif(is_int(strpos($last, ".")))
        {
            $filename = $last;
        }
        else
        {
            $filename = $last.".".$this->request->format();
        }

        end($dst);
        $dst[key($dst)] = $filename;
        reset($dst);
        $this->dst = implode("/", $dst);

        return $this;
    }

    /**
     * @param string $msg
     *
     * @return Redirect
     */
    public function info($msg)
    {
        if(is_array($msg))
        {
            foreach($msg as $m)
            {
                $this->info($m);
            }

            return $this;
        }

        $this->info[] = $msg;

        return $this;
    }

    /**
     * @param string|array $msg
     *
     * @return Redirect
     */
    public function error($msg)
    {
        if(is_array($msg))
        {
            foreach($msg as $m)
            {
                $this->error($m);
            }

            return $this;
        }

        $this->error[] = $msg;

        return $this;
    }
}