<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Router;

use App\HTTP\Request;

class Message
{
    private $request;

    private $info;
    private $error;

    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->error = $request->session()->get("error", []);
        $this->info = $request->session()->get("info", []);
    }

    public function getInfo()
    {
        return $this->info;
    }

    public function getError()
    {
        return $this->error;
    }

    public function addInfo($msg)
    {
        $this->info[] = $msg;
    }

    public function addError($msg)
    {
        $this->error[] = $msg;
    }

    public function flash()
    {
        $this->request->session()->flash("error", $this->error);
        $this->request->session()->flash("info", $this->info);

        $this->error = [];
        $this->info = [];
    }
}