<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Router;

use App\Core\App;

abstract class Model
{
    private $app;

    protected $config;

    protected $database;
    protected $tcache;
    protected $mcache;

    protected $setting;
    protected $permission;
    protected $language;

    /**
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->config = $app->config();

        $this->database = $app->database();
        $this->tcache = $app->tcache();
        $this->mcache = $app->mcache();

        $this->setting = $app->setting();
        $this->permission = $app->permission();
        $this->language = $app->language();
    }

    protected function encrypt($str)
    {
        return $this->app->crypt()->encrypt($str);
    }

    protected function decrypt($str)
    {
        return $this->app->crypt()->decrypt($str);
    }
}