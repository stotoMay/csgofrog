<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

/**
 * @param string $agent
 *
 * @return array
 */
function get_browser_type($agent = NULL)
{
    $bname = 'Unknown';
    $platform = 'Unknown';
    $ub = "Unknown";

    if(preg_match('/linux/i', $agent))
    {
        $platform = 'Linux';
    }
    elseif(preg_match('/iPhone/i', $agent))
    {
        $platform = 'iPhone';
    }
    elseif(preg_match('/iPad/i', $agent))
    {
        $platform = 'iPad';
    }
    elseif(preg_match('/macintosh|mac os x/i', $agent))
    {
        $platform = 'Mac';
    }
    elseif(preg_match('/windows|win32/i', $agent))
    {
        $platform = 'Windows';
    }

    if(preg_match('/MSIE/i', $agent) && !preg_match('/Opera/i', $agent))
    {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    }
    elseif(preg_match('/Firefox/i', $agent))
    {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    }
    elseif(preg_match('/Chrome/i', $agent))
    {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    }
    elseif(preg_match('/Safari/i', $agent))
    {
        $bname = 'Apple Safari';
        $ub = "Safari";
    }
    elseif(preg_match('/Opera/i', $agent))
    {
        $bname = 'Opera';
        $ub = "Opera";
    }
    elseif(preg_match('/Netscape/i', $agent))
    {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

    $known = ['Version', $ub, 'other'];
    $pattern = '#(?<browser>'.join('|', $known).')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    preg_match_all($pattern, $agent, $matches);

    $i = count($matches['browser']);
    if($i != 1)
    {
        if(strripos($agent, "Version") < strripos($agent, $ub))
        {
            $version = $matches['version'][0];
        }
        else
        {
            $version = $matches['version'][1];
        }
    }
    else
    {
        $version = $matches['version'][0];
    }

    if($version == NULL || $version == "")
    {
        $version = "?";
    }

    return [
        'name' => $bname,
        'version' => $version,
        'platform' => $platform
    ];
}

/**
 * @param string $email
 * @param string $header
 * @param string $message
 * @param string $sender
 */
function admin_mail($email, $header, $message, $sender = NULL)
{
    if(!isset($sender))
    {
        $sender = $_SERVER['SERVER_NAME'];
    }

    $h = 'From: '.$sender.' <noreply@'.$_SERVER['SERVER_NAME'].'>'."\r\n".'Reply-To: noreply@'.$_SERVER['SERVER_NAME']."\r\n".'X-Mailer: PHP/'.phpversion();

    mail($email, $header, $message, $h);
}