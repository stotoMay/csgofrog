<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

/**
 * @param string $str
 *
 * @return string
 *
 * @throws \App\Core\Exception\InvalidArgumentException
 */
function convert_folder_namespace($str)
{
    return substr(substr(str_replace("/", '\\', filter_folder($str)), 1), 0, -1);
}

/**
 * @param string $str
 *
 * @return string
 *
 * @throws \App\Core\Exception\InvalidArgumentException
 */
function convert_namespace_folder($str)
{
    return filter_folder(str_replace('\\', "/", $str));
}

/**
 * @param string $str
 *
 * @return string
 */
function real_class($str)
{
    if($str{0} != "/")
    {
        $result = '\\'.str_replace("/", '\\', $str);
    }
    else
    {
        $result = str_replace("/", '\\', $str);
    }

    return $result;
}

/**
 * @param string $path
 *
 * @return string
 */
function base_dir($path)
{
    $realpath = realpath(__DIR__."/../../".$path);

    if(empty($realpath) || !starts_with($realpath, realpath(__DIR__."/../../")))
    {
        return false;
    }

    return $realpath;
}

/**
 * @return string
 */
function str_token()
{
    $token = uniqid(rand(0, 1000)).rand(0, 1000);

    return strtoupper(sha1($token));
}

/**
 * @param int $length
 * @param string $characters
 *
 * @return string
 */
function str_random($length = 10, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
{
    $charactersLength = strlen($characters);
    $randomString = '';
    for($i = 0; $i < $length; $i++)
    {
        $randomString .= $characters[mt_rand(0, $charactersLength - 1)];
    }

    return $randomString;
}

/**
 * @param $haystack
 * @param array $needles
 *
 * @return int
 */
function strposa($haystack, $needles = [])
{
    foreach($needles as $key => $needle)
    {
        $res = strpos($haystack, $needle);
        if(is_int($res))
        {
            return $key;
        }
    }

    return false;
}

/**
 * @param string $haystack
 * @param string $needle
 *
 * @return bool
 */
function starts_with($haystack, $needle)
{
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

/**
 * @param string $haystack
 * @param string $needle
 *
 * @return bool
 */
function ends_with($haystack, $needle)
{
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}