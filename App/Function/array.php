<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

/**
 * @param array $array1
 * @param array $array2
 *
 * @return array
 */
function merge_deep_array(array $array1, array $array2)
{
    foreach($array2 as $key => $value)
    {
        if(is_array($array1[$key]) && is_array($value))
        {
            $array1[$key] = merge_deep_array($array1[$key], $value);
        }
        else
        {
            $array1[$key] = $value;
        }
    }

    return $array1;
}

/**
 * @param array $array
 * @param array $path
 * @param string mixed
 */
function array_add(array &$array, $path, $value)
{
    if(!is_array($path))
    {
        $path = [$path];
    }

    foreach($path as $key)
    {
        $array = &$array[$key];
    }

    $array[] = $value;
}

/**
 * @param array $array
 * @param array $path
 *
 * @return mixed
 */
function array_get(array $array, $path = [])
{
    if(!is_array($path))
    {
        $path = [$path];
    }

    foreach($path as $key)
    {
        if(!isset($array[$key]))
        {
            return NULL;
        }

        $array = $array[$key];
    }

    return $array;
}

/**
 * @param array $array
 * @param array $path
 * @param mixed $value
 */
function array_set(array &$array, $path, $value)
{
    if(!is_array($path))
    {
        $path = [$path];
    }

    foreach($path as $key)
    {
        if(!is_array($array[$key]))
        {
            $array[$key] = [];
        }

        $array = &$array[$key];
    }

    $array = $value;
}

/**
 * @param array $array
 * @param array $path
 */
function array_unset(array &$array, $path)
{
    if(!is_array($path))
    {
        return;
    }

    foreach($path as $key)
    {
        $array = &$array[$key];
    }
    $array = NULL;
}

/**
 * @param array $array
 *
 * @return mixed
 */
function array_last(array $array)
{
    return end($array);
}

/**
 * @param array $array
 *
 * @return mixed
 */
function array_first(array $array)
{
    return reset($array);
}

/**
 * @param array $array1
 * @param array $array2
 *
 * @return bool
 */
function array_compare(array $array1, array $array2)
{
    foreach($array1 as $k => $v)
    {
        if($array2[$k] != $v)
        {
            return false;
        }
    }

    foreach($array2 as $k => $v)
    {
        if($array1[$k] != $v)
        {
            return false;
        }
    }

    return true;
}

/**
 * @param array $array
 * @param array $contains
 *
 * @return bool
 */
function array_contains(array $array, array $contains)
{
    foreach($contains as $k => $v)
    {
        if($array[$k] != $v)
        {
            return false;
        }
    }

    return true;
}

/**
 * @param array $array
 *
 * @return array
 */
function array_clear(array $array)
{
    if(!is_array($array))
    {
        return [];
    }

    foreach($array as $key => $value)
    {
        if($value === "" || $value === NULL)
        {
            unset($array[$key]);
        }
    }

    return array_values($array);
}

/**
 * @param array $array
 * @param mixed $key
 *
 * @return array
 */
function array_between(array $array, $key)
{
    $result = [];
    foreach($array as $value)
    {
        $result[] = $key;
        $result[] = $value;
    }

    return $result;
}

/**
 * @param array $array
 * @param string|array $needle
 * @param bool $assoc
 *
 * @return mixed
 */
function array_remove(&$array, $needle, $assoc = false)
{
    if(is_array($needle) && !$assoc)
    {
        foreach($needle as $value)
        {
            array_remove($array, $value);
        }

        return $array;
    }

    foreach($array as $key => $value)
    {
        if((is_array($needle) && $assoc && array_compare($value, $needle)) || $needle == $value)
        {
            unset($array[$key]);
        }
    }

    return $array;
}