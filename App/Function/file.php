<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

/**
 * @param string $file
 *
 * @return string
 *
 * @throws \App\Core\Exception\FileNotFoundException
 */
function file_get($file)
{
    if(!file_exists($file)) {
        if (substr($file,0,1) === '/') {
            $file = str_replace('//','\\', $file);
            $file = str_replace('/','', $file);
        }
    }
    if(!file_exists($file))
    {
        throw new \App\Core\Exception\FileNotFoundException($file);
    }

    return file_get_contents($file);
}

/**
 * @param string $file
 * @param string $str
 *
 * @throws \App\Core\Exception\FileNotFoundException
 */
function file_put($file, $str)
{
    file_put_contents($file, $str);
}

/**
 * @param string $folder
 *
 * @return array
 *
 * @throws \App\Core\Exception\FolderNotFoundException
 */
function get_dir_files($folder)
{
    if(!is_readable($folder)) {
        if ($folder === '/C:\Users\Admin\Desktop\frenchfrog_empty_data\Language\English/') {
            $folder = 'C:\Users\Admin\Desktop\frenchfrog_empty_data\Language\English';
        }

        if (substr($folder,0,1) === '/') {
            $folder = trim($folder, '/');
            $folder = str_replace('/','\\', $folder);
        }
    }
    if(!is_readable($folder))
    {
        throw new \App\Core\Exception\FolderNotFoundException($folder);
    }

    $list = scandir($folder);
    $result = [];

    foreach($list as $name)
    {
        if(strpos($name, ".") !== false && strlen($name) > 2)
        {
            $result[] = $name;
        }
    }

    return $result;
}

/**
 * @param string $folder
 *
 * @return array
 *
 * @throws \App\Core\Exception\FolderNotFoundException
 */
function get_dir_folders($folder)
{
    if(!is_readable($folder)) {
        if ($folder === '/C:\Users\Admin\Desktop\frenchfrog_empty_data\Language\English/') {
            $folder = 'C:\Users\Admin\Desktop\frenchfrog_empty_data\Language\English';
        }
        if (substr($folder,0,1) === '/') {
            $folder = trim($folder, '/');
            $folder = str_replace('/','\\', $folder);
        }
    }

//        $folder = realpath($folder);
    if(!is_readable($folder))
    {
        throw new \App\Core\Exception\FolderNotFoundException($folder);
    }

    $list = scandir($folder);
    $result = [];

    foreach($list as $name)
    {
        if(strpos($name, ".") === false)
        {
            $result[] = $name;
        }
    }

    return $result;
}

/**
 * @param string $str
 *
 * @return string
 *
 * @throws \App\Core\Exception\InvalidArgumentException
 */
function filter_folder($str)
{
//    var_dump($str, '<br>');
//    $str = realpath($str);
    if(!is_string($str))
    {
        throw new \App\Core\Exception\InvalidArgumentException();
    }
//    var_dump($str);
    if(empty($str))
    {
        return "/";
    }

    if($str[0] != "/")
    {
        $result = "/".$str;
    }

    if(substr($str, -1, 1) != "/")
    {
        $result = isset($result) ? $result."/" : $str."/";
    }

    if(!isset($result))
    {
        $result = $str;
    }

    return $result;
}
