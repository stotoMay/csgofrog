<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

function base32_decode($input)
{
    if(empty($input))
    {
        return '';
    }

    $base32chars = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
        'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z', '2', '3', '4', '5', '6', '7',
        '=',
    ];

    $base32charsFlipped = array_flip($base32chars);

    $paddingCharCount = substr_count($input, $base32chars[32]);
    $allowedValues = [6, 4, 3, 1, 0];

    if(!in_array($paddingCharCount, $allowedValues))
    {
        return false;
    }

    for($i = 0; $i < 4; ++$i)
    {
        if($paddingCharCount == $allowedValues[$i] && substr($input, -($allowedValues[$i])) != str_repeat($base32chars[32], $allowedValues[$i]))
        {
            return false;
        }
    }

    $input = str_replace('=', '', $input);
    $input = str_split($input);
    $binaryString = '';

    for($i = 0; $i < count($input); $i = $i + 8)
    {
        $x = '';
        if(!in_array($input[$i], $base32chars))
        {
            return false;
        }
        for($j = 0; $j < 8; ++$j)
        {
            $x .= str_pad(base_convert(@$base32charsFlipped[@$input[$i + $j]], 10, 2), 5, '0', STR_PAD_LEFT);
        }

        $eightBits = str_split($x, 8);
        for($z = 0; $z < count($eightBits); ++$z)
        {
            $binaryString .= (($y = chr(base_convert($eightBits[$z], 2, 10))) || ord($y) == 48) ? $y : '';
        }
    }

    return $binaryString;
}

function base32_encode($data)
{
    $chars = '0123456789abcdefghjkmnpqrstvwxyz';
    $mask = 0b11111;

    $dataSize = strlen($data);
    $res = '';
    $remainder = 0;
    $remainderSize = 0;

    for($i = 0; $i < $dataSize; $i++)
    {
        $b = ord($data[$i]);
        $remainder = ($remainder << 8) | $b;
        $remainderSize += 8;
        while($remainderSize > 4)
        {
            $remainderSize -= 5;
            $c = $remainder & ($mask << $remainderSize);
            $c >>= $remainderSize;
            $res .= $chars[$c];
        }
    }
    if($remainderSize > 0)
    {
        $remainder <<= (5 - $remainderSize);
        $c = $remainder & $mask;
        $res .= $chars[$c];
    }

    return $res;
}