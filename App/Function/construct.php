<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

/**
 * @return App\Validator\Validator
 */
function v()
{
    return new App\Validator\Validator();
}

/**
 * @param mixed $str
 * @param App\HTTP\Request $request
 *
 * @return App\HTTP\Response
 */
function response($str, $request)
{
    return new App\HTTP\Response($str, $request);
}

/**
 * @param string $token
 * @param App\Core\App $app
 * @param App\HTTP\Request $request
 * @param App\HTTP\Browser $browser
 *
 * @return \App\Captcha\Validator
 */
function captcha($token, $app, $request, $browser)
{
    return new \App\Captcha\Validator($token, $app, $request, $browser);
}

/**
 * @param string $path
 * @param \App\HTTP\Request $request
 *
 * @return \App\Router\Redirect
 */
function redirect($path, $request)
{
    return new \App\Router\Redirect($path, $request);
}

/**
 * @param string $name
 * @param array $params
 */
function set_hook($name, $params = [])
{
    $app = \App\Bootstrap\Factory::getApplication();
    $app->component()->setHook($name, $params);
}