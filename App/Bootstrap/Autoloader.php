<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Bootstrap;

use App\Bootstrap\Exception\AutoloadException;
use App\Bootstrap\Exception\ClassNotExistsException;

class Autoloader
{
    private $libraries;
    private $classes;

    public function __construct()
    {
        spl_autoload_register([$this, "autoloader"]);

        $this->libraries = [];
        $this->classes = [];
    }

    /**
     * @param string $class
     *
     * @throws AutoloadException
     */
    public function autoloader($class)
    {
        try
        {
            $this->includeClass($class);
        }
        catch(ClassNotExistsException $e)
        {
            foreach($this->libraries as $namespace => $path)
            {
                if(!starts_with($class, $namespace))
                {
                    continue;
                }

                $file = base_dir($path."/".str_replace('\\', "/", $class).".php");

                if(!file_exists($file))
                {
                    continue;
                }

                include($file);
            }

            foreach($this->classes as $classname => $path)
            {
                if($class != $classname)
                {
                    continue;
                }

                $file = base_dir($path);

                if(!file_exists($file))
                {
                    continue;
                }

                include($file);
            }
        }
    }

    /**
     * @param string $namespace
     * @param string $path
     */
    public function addLibrary($namespace, $path)
    {
        $this->libraries[$namespace] = $path;
    }

    /**
     * @param array $libaries
     */
    public function addLibraries($libaries)
    {
        $this->libraries = array_merge($this->libraries, $libaries);
    }

    /**
     * @param string $classname
     * @param string $path
     */
    public function addClass($classname, $path)
    {
        $this->classes[$classname] = $path;
    }

    /**
     * @param array $classes
     */
    public function addClasses($classes)
    {
        $this->classes = array_merge($this->classes, $classes);
    }

    /**
     * @param string $name
     *
     * @throws ClassNotExistsException
     */
    public function includeClass($name)
    {
        $file = base_dir(str_replace('\\', "/", $name).".php");

        if(file_exists($file))
        {
            include($file);
        }
        else
        {
            throw new ClassNotExistsException();
        }
    }

    /**
     * @param string $namespace
     *
     * @return array
     * @throws \App\Core\Exception\FolderNotFoundException
     * @throws \App\Core\Exception\InvalidArgumentException
     */
    public function includeNameSpace($namespace)
    {
        $folder = base_dir(convert_namespace_folder($namespace));
        $files = get_dir_files($folder);

        $classes = [];
        foreach($files as $file)
        {
            include($folder."/".$file);
            $classes[] = str_replace(".php", "", $file);
        }

        return $classes;
    }
}