<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Bootstrap;

use App\Bootstrap\Exception\InvalidConfigException;
use App\Logger\Logger;

class Config
{
    const DEBUG_MODE = true;
    const LOGGING_LEVEL = Logger::ALL;

    private $config;

    public function __construct($_CONFIG)
    {
        $this->config = $_CONFIG;
    }

    public function getDatabaseConfig()
    {
        if(!is_array($this->config["database"]))
        {
            throw new InvalidConfigException("no database data in config");
        }

        return $this->config["database"];
    }

    public function getCryptKey()
    {
        if(!isset($this->config["cryptkey"]))
        {
            throw new InvalidConfigException("no cryptkey in config");
        }

        return $this->config["cryptkey"];
    }


    public function areXHeadersEnabled()
    {
        if($this->config["xheaders"] === true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function isIPAccessAllowed()
    {
        return !!$this->config["ipaccess"];
    }

    public function isTwoFactorProtectionEnabled()
    {
        if($this->config["twofactorprotection"] === true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getErrorTemplateDirectory()
    {
        if(!isset($this->config["errortemplates"]))
        {
            return "/App/Admin/Template/Error/";
        }

        $folder = filter_folder($this->config["errortemplates"]);
        $errors = [400, 401, 402, 403, 404, 500];

        if(base_dir($folder) === false)
        {
            return "/App/Admin/Template/Error/";
        }

        foreach($errors as $error)
        {
            if(file_exists(base_dir($folder.$error.".tpl")))
            {
                continue;
            }

            return "/App/Admin/Template/Error/";
        }

        return $folder;
    }


    public function getLanguageDirectory()
    {
        return filter_folder($this->config["language"]);
    }

    public function getComponentDirectory()
    {
        return filter_folder($this->config["component"]);
    }

    public function getMemoryCacheDirectory()
    {
        return filter_folder($this->config["cache"]["memory"]);
    }

    public function getThemeCacheDirectory()
    {
        return filter_folder($this->config["cache"]["theme"]);
    }

    public function getTemporaryCacheDirectory()
    {
        return filter_folder($this->config["cache"]["temporary"]);
    }

    public function getUploadDirectory()
    {
        return filter_folder($this->config["cache"]["upload"]);
    }

    public function getSessionDirectory()
    {
        return filter_folder($this->config["cache"]["session"]);
    }


    public function getBrowserCookieName()
    {
        return isset($this->config["cookie"]["browserid"]) ? $this->config["cookie"]["browserid"] : "browserID";
    }

    public function getLoginCookieName()
    {
        return isset($this->config["cookie"]["loginid"]) ? $this->config["cookie"]["loginid"] : "loginID";
    }

    public function getSessionCookieName()
    {
        return isset($this->config["cookie"]["sessionid"]) ? $this->config["cookie"]["sessionid"] : "sessionID";
    }

    public function getLanguageCookieName()
    {
        return isset($this->config["cookie"]["language"]) ? $this->config["cookie"]["language"] : "language";
    }

    public function getTimezoneOffsetCookieName()
    {
        return isset($this->config["cookie"]["timezone"]) ? $this->config["cookie"]["timezone"] : "timezoneOffset";
    }

    public function getCookieNames()
    {
        return [
            "browserid" => $this->getBrowserCookieName(),
            "loginid" => $this->getLoginCookieName(),
            "sessionid" => $this->getSessionCookieName(),
            "language" => $this->getLanguageCookieName(),
            "timezone" => $this->getTimezoneOffsetCookieName()
        ];
    }

    public function getCookiePrefix()
    {
        return (string)$this->config["cookie"]["prefix"];
    }
}
