<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

ini_set("display_errors", 1);
error_reporting(E_ALL & ~E_NOTICE);

if(version_compare(phpversion(), '7.0.0', '<')) {
    exit("php version isn't high enough");
}

// include functions
$fileList = ["array", "construct", "file", "function", "string", "convert"];
foreach($fileList as $file)
{
    include(__DIR__."/../Function/".$file.".php");
}

// make sure that default timezone is GMT+0
date_default_timezone_set("Europe/London");

// include the autoloader and the factory
include(base_dir("App/Bootstrap/Autoloader.php"));
include(base_dir("App/Bootstrap/Factory.php"));

// default global config vars
$_CONFIG = [];
$_ROUTES = ["controller" => [], "namespace" => []];
$_LIBRARIES = [];
$_CLASSES = [];

// include config files
include(base_dir("App/Config.php"));
include(base_dir("App/Libraries.php"));
include(base_dir("App/Routes.php"));

// start session
session_name(isset($_CONFIG["cookie"]["sessionid"]) ? $_CONFIG["cookie"]["sessionid"] : "sessionID");
session_start();

// print pretty errors
set_exception_handler(function($e)
{
    /**
     * @var Exception $e
     */
    $theme = new App\Theme\Theme(\App\Bootstrap\Factory::getConfig()->getErrorTemplateDirectory());

    if(get_class($e) == 'App\\Router\\Exception\\HTTPException')
    {
        if($e->getCode() >= 400 && $e->getCode() <= 404 && is_int($e->getCode()))
        {
            $template = $theme->draw($e->getCode().".tpl");

            exit($template->parse());
        }
    }

    $template = $theme->draw("500.tpl");

    if(\App\Bootstrap\Config::DEBUG_MODE)
    {
        $template->assignVar("details", [
            "message" => empty($e->getMessage()) ? get_class($e) : $e->getMessage(), "file" => $e->getFile(),
            "line" => $e->getLine(), "number" => $e->getCode(), "trace" => $e->getTraceAsString()
        ]);

        $template->assignVar("debug_mode", true);
    }
    else
    {
        $template->assignVar("debug_mode", false);
    }

    exit($template->parse());
});

// make sure that PHP-Errors are printed pretty with trace as well
set_error_handler(function($errno, $errstr, $errfile, $errline)
{
    throw new ErrorException($errstr, $errno, 1, $errfile, $errline);
}, E_ALL & ~E_NOTICE);


// build Application using the vars from the config files
App\Bootstrap\Factory::buildApplication($_CONFIG, $_ROUTES, $_LIBRARIES, $_CLASSES);
