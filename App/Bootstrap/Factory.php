<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Bootstrap;

use App\Core\App;

class Factory
{
    private static $app;
    private static $autoloader;
    private static $config;

    /**
     * @return App
     */
    public static function getApplication()
    {
        return self::$app;
    }

    /**
     * @return Autoloader
     */
    public static function getAutoloader()
    {
        return self::$autoloader;
    }

    /**
     * @return Config
     */
    public static function getConfig()
    {
        return self::$config;
    }

    public static function buildApplication($_CONFIG, $_ROUTES, $_LIBRARIES, $_CLASSES)
    {
        self::$autoloader = new Autoloader();

        self::$autoloader->addLibraries($_LIBRARIES);
        self::$autoloader->addClasses($_CLASSES);

        self::$config = new Config($_CONFIG);
        $app = new App(self::$config);

        foreach($_ROUTES["namespace"] as $path => $value)
        {
            $app->router()->bindNamspace($path, $value["namespace"], $value["view"]);
        }

        foreach($_ROUTES["controller"] as $path => $value)
        {
            $app->router()->bindController($path, $value["class"], $value["view"]);
        }

        $app->router()->bindNamspace("/admin/", "App/Admin/Controller", "/App/Admin/Template/");
        $app->router()->bindController("/error/", "App/Router/Controller/Error", self::$config->getErrorTemplateDirectory());

        self::$app = $app;
    }
}