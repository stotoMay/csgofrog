<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Language;

use App\Language\Exception\ParseException;

class Parser
{
    /**
     * @param string $str
     *
     * @return array
     *
     * @throws ParseException
     */
    public static function parse($str)
    {
        if(empty($str))
        {
            return [];
        }

        $lines = self::splitString($str);
        $result = [];

        $keys = [];
        $spaces = [];
        $s = false;

        foreach($lines as $line)
        {
            if(!is_int(strpos($line, ":")))
            {
                continue;
            }

            $scount = self::countSpaces($line);
            if($s)
            {
                $spaces[] = $scount;
                $s = false;
            }

            while($scount < array_last($spaces))
            {
                array_pop($keys);
                array_pop($spaces);
            }

            $l = self::parseLine($line);

            if(empty($l["value"]))
            {
                $keys[] = $l["key"];
                $s = true;
            }
            else
            {
                $tmp = $keys;
                $tmp[] = $l["key"];

                array_set($result, $tmp, $l["value"]);
            }
        }

        return $result;
    }

    /**
     * @param string $line
     *
     * @return int
     */
    private static function countSpaces($line)
    {
        $last_letter = " ";
        $count = 0;
        while($last_letter == " ")
        {
            $last_letter = $line{$count};
            if($last_letter == " ")
            {
                $count++;
            }
        }

        return $count;
    }

    /**
     * @param string $line
     *
     * @return array
     */
    private static function parseLine($line)
    {
        $key = NULL;
        $value = NULL;

        foreach(str_split($line) as $chr)
        {
            if(!is_string($key) && $chr == " ")
            {
                continue;
            }

            if($chr == "\n" || $chr == "\r")
            {
                break;
            }

            if(is_string($value))
            {
                $value .= $chr;
            }
            else
            {
                if($chr == ":")
                {
                    $value = "";
                }
                else
                {
                    $key .= $chr;
                }
            }
        }

        return ["key" => $key, "value" => self::removeSpaces($value)];
    }

    /**
     * @param string $line
     *
     * @return string
     */
    private static function removeSpaces($line)
    {
        $result = NULL;
        foreach(str_split($line) as $chr)
        {
            if(!isset($result) && $chr != " ")
            {
                $result = "";
            }

            if(is_string($result))
            {
                $result .= $chr;
            }
        }

        return $result;
    }

    /**
     * @param string $str
     *
     * @return array
     */
    private static function splitString($str)
    {
        return array_clear(preg_split('/$\R?^/m', $str));
    }
}