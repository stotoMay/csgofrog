<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Language;

use App\Core\App;

class Language
{
    private $app;
    private $folder;

    private $lang;

    /**
     * @param string $folder
     * @param App $app
     *
     * @throws \App\Core\Exception\InvalidArgumentException
     */
    public function __construct($folder, App $app)
    {
        $this->folder = filter_folder($folder);
        $this->app = $app;

        $this->lang = [];

        $this->loopFolder($this->folder);
    }

    public function loopFolder($folder)
    {
//        $folder = realpath($folder);
        $folder = filter_folder($folder);
        $files = get_dir_files($folder);

        foreach($files as $f)
        {
            if(strpos($f, ".lang") !== false)
            {
                $this->mergeFile($folder."/".$f);
            }
        }

        $folders = get_dir_folders($folder);
        foreach($folders as $f)
        {
            $this->loopFolder($folder.$f."/");
        }
    }

    /**
     * @param string $path
     *
     * @throws Exception\ParseException
     * @throws \App\Core\Exception\FileNotFoundException
     */
    public function mergeFile($path)
    {
        $file = file_get($path);

        $this->lang = merge_deep_array($this->lang, Parser::parse($file));
    }


    /**
     * @param array|string $key
     * @param array $args
     *
     * @return array|string
     */
    public function get($key, $args = NULL)
    {
        if(!is_array($key))
        {
            $key = [$key];
        }

        return $this->output(array_get($this->lang, $key), array_last($key), $args);
    }


    /**
     * @param string $name
     * @param string $type
     * @param string $typeID
     *
     * @return string
     */
    public function getSetting($name, $type, $typeID)
    {
        return $this->output($this->lang["setting"][$type][$typeID][$name], $name);
    }

    /**
     * @param string $name
     * @param string $type
     * @param string $typeID
     *
     * @return string
     */
    public function getPermission($name, $type, $typeID)
    {
        return $this->output($this->lang["permission"][$type][$typeID][$name], $name);
    }


    /**
     * @param string $name
     * @param array $args
     * @param string $type
     *
     * @return string
     */
    public function getValidatorMessage($name, $args = NULL, $type = "single")
    {
        return $this->output($this->lang["validation"][$type][$name], $name, $args);
    }

    /**
     * @param string $name
     * @param array $args
     *
     * @return string
     */
    public function getMessage($name, $args = NULL)
    {
        return $this->output($this->lang["message"][$name], $name, $args);
    }

    /**
     * @param string $file
     * @param array $args
     *
     * @return string
     * @throws \App\Core\Exception\FileNotFoundException
     */
    public function getText($file, $args = NULL)
    {
        $realpath = $this->folder.$file;
        if(file_exists($realpath))
        {
            return $this->output(file_get($realpath), $file, $args);
        }
        else
        {
            return NULL;
        }
    }


    /**
     * @param string $str
     * @param string $default
     * @param array $args
     *
     * @return array|string
     */
    private function output($str, $default, $args = NULL)
    {
        if(is_array($str))
        {
            return $str;
        }

        if(empty($str))
        {
            return $default;
        }
        else
        {
            if(!is_array($args))
            {
                return $str;
            }

            return vsprintf($str, $args);
        }
    }
}