<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\User;

use App\Bootstrap\Factory;
use App\User\Exception\UserNotFoundException;

class User
{
    private $id;
    private $user;

    private $permission;

    /**
     * @param int $userID
     *
     * @throws UserNotFoundException
     */
    public function __construct($userID)
    {
        $app = Factory::getApplication();
        $this->id = $userID;

        $this->user = $app->tcache()->get($userID, "user");

        if(!is_array($this->user))
        {
            $this->user = $app->database()->select("user")->where(["id" => $userID])->fetchRow();

            if(is_array($this->user))
            {
                $app->tcache()->set($userID, $this->user, "user");
            }
            else
            {
                throw new UserNotFoundException();
            }
        }
    }

    /**
     * @return Permission
     */
    public function permission()
    {
        if(!is_object($this->permission))
        {
            $this->permission = new Permission($this);
        }

        return $this->permission;
    }

    /**
     * @return int
     */
    public function getUserID()
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function getColumn($name)
    {
        return $this->user[$name];
    }

    /**
     * @return array
     */
    public function getRow()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->user["username"];
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->user["email"];
    }

    /**
     * @return int
     */
    public function getMainGroup()
    {
        return $this->user["groupID"];
    }

    /**
     * @return array
     * @throws \App\Database\Exception\ParseException
     */
    public function getSubGroups()
    {
        $app = Factory::getApplication();

        $list = $app->tcache()->get($this->id, "user_subgroups");
        if(!is_array($list))
        {
            $list = $app->database()->select("usergroup")->where(["userID" => $this->id])->order("priority", "asc")
                ->fetchAssoc();

            $app->tcache()->set($this->id, $list, "user_subgroups");
        }

        return $list;
    }

    /**
     * @return int
     * @throws \App\Database\Exception\ParseException
     */
    public function getLastAction()
    {
        $app = Factory::getApplication();

        $lastaction = $app->tcache()->get($this->id, "user_lastaction");

        if(empty($lastaction))
        {
            $lastaction = $app->database()->select("session")->column(["browser.lastaction" => "lastaction"])
                ->leftJoin("browser", ["session.browserID[=]browser.id"])
                ->where(["session.userID" => $this->id])
                ->order(["browser.lastaction"], "desc")->fetchRow()["lastaction"];

            $app->tcache()->set($this->id, $lastaction, "user_lastaction");
        }

        return $lastaction;
    }

    /**
     * @return bool
     * @throws \App\Database\Exception\ParseException
     */
    public function isBanned()
    {
        $app = Factory::getApplication();

        $banned = $app->tcache()->get($this->id, "user_banned");
        if(empty($banned))
        {
            $banned = $app->database()->select("ban")->where(["userID" => $this->id,
                "OR" => ["expire[>=]" => time(), "expire" => NULL]])
                ->exists();

            $app->tcache()->set($this->id, $banned, "user_banned");
        }

        return $banned;
    }

    /**
     * @param string $username
     *
     * @return User
     * @throws UserNotFoundException
     * @throws \App\Database\Exception\ParseException
     */
    public static function find($username)
    {
        $app = Factory::getApplication();

        $find = $app->database()->select("user")->where(["username" => $username])->fetchRow();

        $app->tcache()->set($find["id"], $find, "user");

        if(is_array($find))
        {
            return new User($find["id"]);
        }
        else
        {
            throw new UserNotFoundException();
        }
    }
}