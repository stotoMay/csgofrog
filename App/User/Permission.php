<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\User;

use App\Bootstrap\Factory;

class Permission
{
    private $user;

    private $cache;
    private $permission;

    /**
     * @param User $user
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function __construct(User $user)
    {
        $app = Factory::getApplication();

        $this->user = $user;
        $this->permission = $app->permission()->getPermissionList();

        $subgroups = $user->getSubGroups();
        $maingroup = $user->getMainGroup();

        $groupList = [];
        $loadList = [];

        $cache = $app->tcache()->get("group_permission");
        if(!is_array($cache))
        {
            $cache = [];
        }

        foreach($subgroups as $subgroup)
        {
            if(!is_array($cache[$subgroup["groupID"]]))
            {
                $loadList[] = $subgroup["groupID"];
                $cache[$subgroup["groupID"]] = [];
            }

            $groupList[] = $subgroup["groupID"];
        }

        if(!is_array($cache[$maingroup]))
        {
            $loadList[] = $maingroup;
            $cache[$maingroup] = [];
        }

        $groupList[] = $maingroup;
        $userPermission = [];

        $pvalues = $app->tcache()->get($this->user->getUserID(), "user_permission");
        if(!is_array($pvalues))
        {
            if(count($loadList) > 0)
            {
                $pvalues = $app->database()->select("permission")->where([
                    "OR" => [
                        "userID" => $user->getUserID(), "groupID" => $loadList
                    ]
                ])->fetchAssoc();
            }
            else
            {
                $pvalues = $app->database()->select("permission")->where(["userID" => $user->getUserID()])
                    ->fetchAssoc();
            }

            $app->tcache()->set($this->user->getUserID(), $pvalues, "user_permission");
        }

        foreach($pvalues as $pvalue)
        {
            if(!empty($pvalue["groupID"]))
            {
                $cache[$pvalue["groupID"]][$pvalue["type"]][$pvalue["typeID"]][$pvalue["name"]] = $pvalue["value"];
            }
            else
            {
                $userPermission[$pvalue["type"]][$pvalue["typeID"]][$pvalue["name"]] = $pvalue["value"];
            }
        }

        $permission = [];
        foreach($groupList as $group)
        {
            if(!is_array($cache[$group]))
            {
                continue;
            }

            $permission = merge_deep_array($permission, $cache[$group]);
        }
        $permission = merge_deep_array($permission, $userPermission);

        $app->tcache()->set("group_permission", $cache);
        $this->cache = $permission;
    }

    /**
     * @param string $name
     * @param string $type
     * @param string $typeID
     *
     * @return string
     */
    public function get($name, $type, $typeID)
    {
        $p = $this->cache[$type][$typeID][$name];
        if(empty($p))
        {
            return $this->permission[$type][$typeID][$name]["default"];
        }
        else
        {
            return $p;
        }
    }

    /**
     * @param array $permissions
     *
     * @return bool
     */
    public function hasRequirements($permissions)
    {
        if(!is_array($permissions))
        {
            return false;
        }

        if(count($permissions) == 3 && !is_array($permissions[0]))
        {
            $permissions = [$permissions];
        }

        foreach($permissions as $permission)
        {
            if(count($permission) != 3)
            {
                return false;
            }

            if($this->get($permission[2], $permission[0], $permission[1]) == 1)
            {
                continue;
            }

            return false;
        }

        return true;
    }

    /**
     * @param string $component
     * @param string $name
     *
     * @return string
     */
    public function getComponent($component, $name)
    {
        return $this->get($name, "component", $component);
    }

    /**
     * @param string $controller
     * @param string $name
     *
     * @return string
     */
    public function getController($controller, $name)
    {
        return $this->get($name, "controller", $controller);
    }

    /**
     * @param string $type
     * @param string $typeID
     *
     * @return array
     */
    public function getPermissions($type, $typeID)
    {
        if(!is_array($this->permission[$type][$typeID]))
        {
            return [];
        }

        $p = [];
        foreach($this->permission[$type][$typeID] as $pname => $pvalue)
        {
            $p[$pname] = isset($this->cache[$type][$typeID][$pname]) ? $this->cache[$type][$typeID][$pname] : $pvalue["default"];
        }

        return $p;
    }
}