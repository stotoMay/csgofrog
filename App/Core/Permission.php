<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Core;

class Permission
{
    private $app;

    private $permission;

    /**
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * @param string $name
     * @param string $input
     * @param string $type
     * @param string $typeID
     * @param string $default
     */
    public function registerPermission($name, $input, $type, $typeID, $default = NULL)
    {
        $this->permission[$type][$typeID][$name] = ["input" => $input, "default" => $default];
    }

    /**
     * @param string $type
     * @param string $typeID
     *
     * @return array
     */
    public function getPermissionList($type = NULL, $typeID = NULL)
    {
        if(isset($type))
        {
            if(isset($typeID))
            {
                return $this->permission[$type][$typeID];
            }
            else
            {
                return $this->permission[$type];
            }
        }
        else
        {
            return $this->permission;
        }
    }
}