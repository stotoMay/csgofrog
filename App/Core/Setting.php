<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Core;

class Setting
{
    public $app;

    private $cache;
    private $setting;

    /**
     * @param App $app
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function __construct(App $app)
    {
        $this->app = $app;

        $settings = $app->database()->select("setting")->column(["type", "typeID", "name", "value"])->fetchAssoc();

        foreach($settings as $s)
        {
            $this->cache[$s["type"]][$s["typeID"]][$s["name"]] = $s["value"];
        }
    }

    /**
     * @param string $name
     * @param string $input
     * @param string $type
     * @param string $typeID
     * @param string $default
     */
    public function registerSetting($name, $input, $type, $typeID, $default = NULL)
    {
        $this->setting[$type][$typeID][$name] = ["input" => $input, "default" => $default];
    }

    /**
     * @param string $type
     * @param string $typeID
     *
     * @return array
     */
    public function getSettingList($type = NULL, $typeID = NULL)
    {
        if(isset($type))
        {
            if(isset($typeID))
            {
                return $this->setting[$type][$typeID];
            }
            else
            {
                return $this->setting[$type];
            }
        }
        else
        {
            return $this->setting;
        }
    }

    /**
     * @param string $name
     * @param string $type
     * @param string $typeID
     *
     * @return string
     */
    public function get($name, $type, $typeID)
    {
        if(!isset($this->cache[$type][$typeID][$name]))
        {
            return $this->setting[$type][$typeID][$name]["default"];
        }
        else
        {
            return $this->cache[$type][$typeID][$name];
        }
    }

    /**
     * @param string $component
     * @param string $name
     *
     * @return string
     */
    public function getComponent($component, $name)
    {
        return $this->get($name, "component", $component);
    }

    /**
     * @param string $controller
     * @param string $name
     *
     * @return string
     */
    public function getController($controller, $name)
    {
        return $this->get($name, "controller", $controller);
    }

}