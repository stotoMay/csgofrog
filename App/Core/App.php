<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Core;

use App\Bootstrap\Config;
use App\Router\Router;
use App\Component\Component;
use App\Language\Language;
use App\Tools\Crypt;
use App\Tools\Task;
use App\Logger\DBLogger;
use App\Storage\MCache;
use App\Storage\TCache;
use App\Database\Database;

class App
{
    private $config;

    private $database;
    private $setting;
    private $permission;
    private $crypt;
    private $language;

    private $router;
    private $task;
    private $logger;

    private $mcache;
    private $tcache;

    private $component;

    /**
     * @param Config $config
     *
     * @throws \App\Bootstrap\Exception\InvalidConfigException
     */
    public function __construct(Config $config)
    {
        $this->config = $config;

        $dbconfig = $config->getDatabaseConfig();
        $this->database = new Database($dbconfig["driver"], $dbconfig["database"], $dbconfig["host"], $dbconfig["port"], $dbconfig["user"], $dbconfig["password"], $dbconfig["prefix"]);

        $this->setting = new Setting($this);
        $this->permission = new Permission($this);
        $this->language = new Language(base_dir($config->getLanguageDirectory().$this->setting()->getController("admin_lang", "selected")."/"), $this);
        $this->crypt = new Crypt($config->getCryptKey());

        $this->setting->registerSetting("dateformat", "text", "application", "time", "d-m-Y");
        $this->setting->registerSetting("timeformat", "text", "application", "time", "H:i:s");
        $this->setting->registerSetting("datetimeformat", "text", "application", "time", "d-m-Y, H:i:s");
        $this->setting->registerSetting("maintenance", "bool", "application", "general", 0);
        $this->setting->registerSetting("name", "text", "application", "general", "websitename");
        $this->setting->registerSetting("adminmail", "email", "application", "general");
        $this->setting->registerSetting("numberdelimiter", "text", "application", "general", ".");
        $this->setting->registerSetting("ssl", "bool", "application", "general", 0);
        $this->setting->registerSetting("browser_language", "bool", "application", "general", 0);

        $this->permission()->registerPermission("admin_control_panel", "bool", "application", "general", 0);

        $this->router = new Router($this);
        $this->task = new Task($this);

        $this->logger = new DBLogger("app", $this->database());
        $this->logger->setLevel(Config::LOGGING_LEVEL);

        $this->mcache = new MCache(base_dir($config->getMemoryCacheDirectory()));
        $this->tcache = new TCache();
        $this->component = new Component($config->getComponentDirectory(), $this);

        $this->component()->setHook("onApplicationBuild");
    }

    /**
     * @return Database
     */
    public function database()
    {
        return $this->database;
    }

    /**
     * @return Router
     */
    public function router()
    {
        return $this->router;
    }

    /**
     * @return Component
     */
    public function component()
    {
        return $this->component;
    }

    /**
     * @return \App\Core\Setting
     */
    public function setting()
    {
        return $this->setting;
    }

    /**
     * @return \App\Core\Permission
     */
    public function permission()
    {
        return $this->permission;
    }

    /**
     * @return Language
     */
    public function language()
    {
        return $this->language;
    }

    /**
     * @return Task
     */
    public function task()
    {
        return $this->task;
    }

    /**
     * @return DBLogger
     */
    public function logger()
    {
        return $this->logger;
    }

    /**
     * @return MCache
     */
    public function mcache()
    {
        return $this->mcache;
    }

    /**
     * @return TCache
     */
    public function tcache()
    {
        return $this->tcache;
    }

    /**
     * @return Crypt
     */
    public function crypt()
    {
        return $this->crypt;
    }

    /**
     * @return Config
     */
    public function config()
    {
        return $this->config;
    }
}