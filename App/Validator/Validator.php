<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator;

/**
 * Class Validator
 *
 * @method Validator alnum()
 * @method Validator alpha()
 * @method Validator between($min, $max)
 * @method Validator boolean()
 * @method Validator date()
 * @method Validator domain()
 * @method Validator email()
 * @method Validator endsWith($letter)
 * @method Validator equals($equal)
 * @method Validator even()
 * @method Validator floatnumber()
 * @method Validator hexcolor()
 * @method Validator input($input)
 * @method Validator integer()
 * @method Validator lowercase()
 * @method Validator max($max)
 * @method Validator min($min)
 * @method Validator negative()
 * @method Validator notEmpty()
 * @method Validator noWhitespaces()
 * @method Validator numeric()
 * @method Validator password()
 * @method Validator positive()
 * @method Validator regex($regex)
 * @method Validator textstring()
 * @method Validator uppercase()
 * @method Validator username()
 * @method Validator database()
 * @method Validator ip()
 */
class Validator
{
    private $ruleList = [];
    private $argsList = [];

    /**
     * @param mixed $string
     *
     * @return bool
     */
    public function validate($string)
    {
        /**
         * @var \App\Validator\Rule $v
         */
        try
        {
            foreach($this->ruleList as $v)
            {
                if(is_object($v) && method_exists($v, "validate"))
                {
                    $v->validate($string);
                }
            }

            return true;
        }
        catch(Exception\RuleException $e)
        {
            return false;
        }
    }

    /**
     * @param mixed $string
     *
     * @return bool
     *
     * @throws Exception\ValidateException
     */
    public function assert($string)
    {
        /**
         * @var \App\Validator\Rule $v
         */
        $errorList = [];
        foreach($this->ruleList as $name => $v)
        {
            if(is_object($v) && method_exists($v, "validate"))
            {
                try
                {
                    $v->validate($string);
                }
                catch(Exception\RuleException $e)
                {
                    $errorList[$name] = $this->argsList[$name];
                }
            }
        }

        if(count($errorList) > 0)
        {
            throw new Exception\ValidateException($string, $errorList);
        }
        else
        {
            return true;
        }
    }

    /**
     * @param $string
     *
     * @return true|Exception\validateException
     */
    public function analyse($string)
    {
        try
        {
            $this->assert($string);
        }
        catch(Exception\ValidateException $e)
        {
            return $e;
        }

        return true;
    }

    public function __call($name, $args)
    {
        $classname = '\\App\\Validator\\Rule\\'.$name;

        if(class_exists($classname))
        {
            /**
             * @var \App\Validator\Rule $class
             */
            if(is_array($args) && count($args) > 0)
            {
                $class = new $classname($this, $args);
            }
            else
            {
                $class = new $classname($this);
            }

            if(get_parent_class($class) == 'App\\Validator\\Rule')
            {
                $this->ruleList[$name] = $class;
                $this->argsList[$name] = $args;
            }
            else
            {
                throw new Exception\CallException("invalid parent class of ".$name);
            }
        }
        else
        {
            throw new Exception\CallException("class not found of ".$name);
        }

        return $this;
    }

    public function rule_used($name)
    {
        return is_object($this->ruleList[$name]);
    }
}


