<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Exception;

use App\Language\Language;
use App\Theme\Template;
use Exception;

class ValidateException extends Exception
{
    private $errorList = [];
    private $string;

    /**
     * @param string $string
     * @param array $errorList
     */
    public function __construct($string, $errorList)
    {
        $this->errorList = $errorList;
        $this->string = $string;
    }

    /**
     * @param Template $template
     * @param Language $lang
     *
     * @return string
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\Theme\Exception\ParseException
     * @throws \App\Theme\Exception\TemplateNotFoundException
     */
    public function getFullMessage(Template $template, Language $lang)
    {
        if(count($this->errorList) == 1)
        {
            reset($this->errorList);

            $name = key($this->errorList);
            $args = $this->getArgs($this->errorList[$name]);

            $template->assignVar("errormessage", $lang->getValidatorMessage($name, $args));

            return $template->parse();
        }
        else
        {
            $errorlist = [];
            foreach($this->errorList as $name => $args)
            {
                $errorlist[] = $lang->getValidatorMessage($name, $this->getArgs($args), "multiple");
            }

            $template->assignVar("errormessage", $lang->getMessage("validator_header", $this->getArgs([])));
            $template->assignVar("errorlist", $errorlist);

            return $template->parse();
        }
    }

    /**
     * @param array $array
     * @param Language $lang
     *
     * @return array
     */
    public function findMessage($array = NULL, Language $lang = NULL)
    {
        if(!is_array($array))
        {
            $result = [];
            foreach($this->errorList as $name => $args)
            {
                if(is_object($lang))
                {
                    $result[] = $lang->getValidatorMessage($name, $this->getArgs($args));
                }
                else
                {
                    $result[] = $name;
                }
            }

            return $result;
        }

        $result = [];
        foreach($array as $key => $value)
        {
            if(is_int($key) && isset($this->errorList[$value]))
            {
                $result[$value] = is_object($lang) ? $lang->getValidatorMessage($value, $this->getArgs($this->errorList[$value])) : $value;
            }
            if(is_string($key) && isset($this->errorList[$key]))
            {
                $result[$key] = vsprintf($value, $this->getArgs($this->errorList[$key]));
            }
        }

        return $result;
    }

    /**
     * @param array $args
     *
     * @return array
     */
    private function getArgs($args)
    {
        $result = ["'".htmlentities($this->string, ENT_QUOTES)."'"];

        if(!is_array($args))
        {
            return $result;
        }

        foreach($args as $arg)
        {
            $result[] = "'".htmlentities($arg, ENT_QUOTES)."'";
        }

        return $result;
    }
}