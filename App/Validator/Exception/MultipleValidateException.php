<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Exception;

use App\Language\Language;
use App\Theme\Template;
use Exception;

class MultipleValidateException extends Exception
{
    private $errors;

    /**
     * @param array $errors
     */
    public function __construct($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @param Template $template
     * @param Language $lang
     *
     * @return array
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\Theme\Exception\ParseException
     * @throws \App\Theme\Exception\TemplateNotFoundException
     */
    public function getFullMessages(Template $template, Language $lang)
    {
        $messages = [];

        foreach($this->errors as $error)
        {
            /**
             * @var ValidateException $error
             */
            $messages[] = $error->getFullMessage($template, $lang);
        }

        return $messages;
    }
}