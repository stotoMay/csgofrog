<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator;

abstract class Rule
{
    /**
     * @var Validator
     */
    protected $validator;

    protected $args;

    /**
     * @param Validator $validator
     * @param array $args
     */
    public function __construct(Validator $validator, $args = [])
    {
        $this->validator = $validator;
        $this->args = $args;
        $this->init();
    }

    abstract public function init();

    abstract public function validate($str);
}