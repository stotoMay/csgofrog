<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator;

use App\Validator\Exception\MultipleValidateException;

class MultipleValidation
{
    private $validators;

    public function __construct()
    {
        $this->validators = [];
    }

    public function length()
    {
        return count($this->validators);
    }

    /**
     * @param Validator $validator
     * @param string $string
     *
     * @return MultipleValidation
     */
    public function append(Validator $validator, $string)
    {
        $this->validators[] = [$validator, $string];

        return $this;
    }

    /**
     * @return bool
     *
     * @throws MultipleValidateException
     */
    public function assert()
    {
        $errors = [];

        foreach($this->validators as $validator)
        {
            /**
             * @var Validator $v
             */
            $v = $validator[0];
            $result = $v->analyse($validator[1]);

            if($result === true)
            {
                continue;
            }

            $errors[] = $result;
        }

        if(count($errors) > 0)
        {
            throw new MultipleValidateException($errors);
        }

        return true;
    }

    /**
     * @return bool
     */
    public function validate()
    {
        foreach($this->validators as $validator)
        {
            /**
             * @var Validator $v
             */
            $v = $validator[0];

            if($v->validate($validator[1]))
            {
                continue;
            }

            return false;
        }

        return true;
    }
}