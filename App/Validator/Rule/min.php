<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;
use App\Validator\Exception\RuleException;

class min extends Rule
{
    public function init()
    {

    }

    public function validate($string)
    {
        if($this->validator->rule_used("date"))
        {
            if(!is_numeric($this->args[0]))
            {
                $min = strtotime($this->args[0]);

                if($min === false)
                {
                    throw new RuleException();
                }
            }
            else
            {
                $min = $this->args[0];
            }

            if(strtotime($string) === false)
            {
                throw new RuleException();
            }
            else
            {
                $checktime = strtotime($string);
            }

            if($checktime >= $min)
            {
                return true;
            }
            else
            {
                throw new RuleException();
            }
        }
        elseif(($this->validator->rule_used("integer") || $this->validator->rule_used("floatnumber") || $this->validator->rule_used("numeric")) && is_numeric($this->args[0]))
        {
            if($string >= $this->args[0])
            {
                return true;
            }
            else
            {
                throw new RuleException();
            }
        }
        elseif(is_array($string))
        {
            if(count($string) >= $this->args[0])
            {
                return true;
            }
            else
            {
                throw new RuleException();
            }
        }
        else
        {
            if(strlen($string) >= $this->args[0])
            {
                return true;
            }
            else
            {
                throw new RuleException();
            }
        }
    }
}