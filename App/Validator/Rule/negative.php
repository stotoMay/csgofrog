<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;

class negative extends Rule
{
    public function init()
    {
        $this->validator->max(0);
    }

    public function validate($string)
    {
        return true;
    }
}