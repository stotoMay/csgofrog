<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;
use App\Validator\Exception\RuleException;

class alnum extends Rule
{
    public function init()
    {

    }

    public function validate($str)
    {
        if(preg_match('/^[a-zA-Z0-9 ]+$/s', $str))
        {
            return true;
        }
        else
        {
            throw new RuleException();
        }
    }
}