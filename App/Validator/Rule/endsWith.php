<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;
use App\Validator\Exception\RuleException;

class endsWith extends Rule
{
    public function init()
    {

    }

    public function validate($string)
    {
        $lt = substr($string, -1, 1);
        if($lt == $this->args[0])
        {
            return true;
        }
        else
        {
            throw new RuleException();
        }
    }
}