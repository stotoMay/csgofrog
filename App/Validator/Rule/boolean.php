<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;
use App\Validator\Exception\RuleException;

class boolean extends Rule
{
    public function init()
    {

    }

    public function validate($string)
    {
        $list = ["1", "0", "yes", "no", "on", "off"];

        if(is_int(array_search((string)$string, $list)) || is_bool($string))
        {
            return true;
        }
        else
        {
            throw new RuleException();
        }
    }
}