<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;
use App\Validator\Exception\RuleException;

class url extends Rule
{
    public function init()
    {

    }

    public function validate($string)
    {
        if(preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $string) && preg_match("/^.{1,253}$/", $string) && preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $string))
        {
            return true;
        }
        else
        {
            throw new RuleException();
        }
    }
}