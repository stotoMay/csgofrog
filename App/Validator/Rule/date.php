<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;
use App\Validator\Exception\RuleException;

class date extends Rule
{
    public function init()
    {

    }

    public function validate($string)
    {
        if(strtotime($string) !== false)
        {
            return true;
        }
        else
        {
            throw new RuleException();
        }
    }
}