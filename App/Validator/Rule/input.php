<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;

class input extends Rule
{
    public function init()
    {
        if($this->args[0] == "int")
        {
            $this->validator->integer();
        }
        elseif($this->args[0] == "numeric")
        {
            $this->validator->numeric();
        }
        elseif($this->args[0] == "float")
        {
            $this->validator->floatnumber();
        }
        elseif($this->args[0] == "int")
        {
            $this->validator->integer();
        }
        elseif($this->args[0] == "email")
        {
            $this->validator->email();
        }
        elseif($this->args[0] == "date")
        {
            $this->validator->date();
        }
        elseif($this->args[0] == "password")
        {
            $this->validator->password();
        }
        elseif($this->args[0] == "username")
        {
            $this->validator->username();
        }
        elseif($this->args[0] == "bool")
        {
            $this->validator->boolean();
        }
        elseif($this->args[0] != "text" && $this->args[0] != "textarea")
        {
            $this->validator->regex($this->args[0]);
        }
    }

    public function validate($string)
    {
        return true;
    }
}