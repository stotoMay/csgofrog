<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;
use App\Validator\Exception\RuleException;

class password extends Rule
{
    public function init()
    {

    }

    public function validate($string)
    {
        if(preg_match('#(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$#', $string))
        {
            return true;
        }
        else
        {
            throw new RuleException();
        }
    }
}