<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;
use App\Validator\Exception\RuleException;

class uppercase extends Rule
{
    public function init()
    {

    }

    public function validate($string)
    {
        if(ctype_upper($string))
        {
            return true;
        }
        else
        {
            throw new RuleException();
        }
    }
}