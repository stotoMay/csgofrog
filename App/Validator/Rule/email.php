<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;
use App\Validator\Exception\RuleException;

class email extends Rule
{
    public function init()
    {

    }

    public function validate($string)
    {
        if(!empty($string) && filter_var($string, FILTER_VALIDATE_EMAIL) !== false)
        {
            return true;
        }
        else
        {
            throw new RuleException();
        }
    }
}