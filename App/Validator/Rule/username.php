<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;

class username extends Rule
{
    public function init()
    {
        $this->validator->textstring();
        $this->validator->notEmpty();
        $this->validator->min(4);
        $this->validator->max(15);
        $this->validator->alnum();
    }

    public function validate($string)
    {
        return true;
    }
}