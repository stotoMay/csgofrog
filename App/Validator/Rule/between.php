<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;

class between extends Rule
{
    public function init()
    {
        $this->validator->min($this->args[0]);
        $this->validator->max($this->args[1]);
    }

    public function validate($string)
    {
        return true;
    }
}