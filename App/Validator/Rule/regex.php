<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;
use App\Validator\Exception\RuleException;

class regex extends Rule
{
    public function init()
    {

    }

    public function validate($string)
    {
        if(preg_match("#".$this->args[0]."#", $string) === 1)
        {
            return true;
        }
        else
        {
            throw new RuleException();
        }
    }
}