<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;
use App\Validator\Exception\RuleException;

class database extends Rule
{
    public function init()
    {

    }

    public function validate($string)
    {
        if(!is_int(strpos($string, "'")) && !is_int(strpos($string, '"')))
        {
            return true;
        }
        else
        {
            throw new RuleException();
        }
    }
}