<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;
use App\Validator\Exception\RuleException;

class even extends Rule
{
    public function init()
    {

    }

    public function validate($string)
    {
        if(intval($string) % 2 == 0)
        {
            return true;
        }
        else
        {
            throw new RuleException();
        }
    }
}