<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;
use App\Validator\Exception\RuleException;

class equals extends Rule
{
    public function init()
    {

    }

    public function validate($string)
    {
        if($string == $this->args[0])
        {
            return true;
        }
        else
        {
            throw new RuleException();
        }
    }
}