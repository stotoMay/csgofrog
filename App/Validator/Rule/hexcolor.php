<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Validator\Rule;

use App\Validator\Rule;
use App\Validator\Exception\RuleException;

class hexcolor extends Rule
{
    public function init()
    {

    }

    public function validate($str)
    {
        if($str{0} == "#")
        {
            $str = substr($str, 1);
        }

        if(strlen($str) == 6 && preg_match('/^[a-fA-f0-9]+$/s', $str))
        {
            return true;
        }
        else
        {
            throw new RuleException();
        }
    }
}