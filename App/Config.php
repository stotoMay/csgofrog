<?php
$_CONFIG = [
    "database" => [
        "driver" => 'MySQL',
        "host" => "127.0.0.1",
        "port" => 3306,
        "user" => "root",
        "password" => "",
        "database" => "csgofrog",
        "prefix" => ""
    ],
    "language" => "/Language/",
    "component" => "/Component/",
    "cache" => [
        "memory" => "/Storage/Memory/",
		"theme" => "/Storage/Theme/",
        "temporary" => "/Storage/Temporary/",
        "upload" => "/Storage/Upload/",
        "session" => "/Storage/Session/"
    ],
    "cookie" => [
        "prefix" => "",
        "language" => "language",
        "browserid" => "browserID",
        "sessionid" => "sessionID",
        "loginid" => "loginSecure",
        "timezone" => "timezoneOffset"
    ],
    "cryptkey" => "testcryptkeyabcdefghijklmnopqrst",
    "errortemplates" => "/Template/Error/",
    "xheaders" => false,
    "twofactorprotection" => true,
    "ipaccess" => false
];
