<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Model;

use App\Router\Model;

class Setting extends Model
{
    public function setSetting($name, $value, $type, $typeID)
    {
        $this->database->replace("setting")->set([
            "name" => $name, "value" => $value, "type" => $type, "typeID" => $typeID, "updated" => time()
        ])->where([
            "name" => $name, "type" => $type, "typeID" => $typeID
        ]);
    }

    public function getSettingTypes()
    {
        $types = $this->setting->getSettingList();
        $result = [];

        foreach($types as $type => $value)
        {
            $children = [];

            foreach($value as $typeID => $settings)
            {
                $settingcount = 0;

                foreach($settings as $setting)
                {
                    if($setting["input"] == "none")
                    {
                        continue;
                    }

                    $settingcount++;
                }

                if($settingcount == 0)
                {
                    continue;
                }

                $children[] = [
                    "name" => $typeID, "translation" => $this->language->get(["typeID", $type, $typeID]),
                    "count" => $settingcount
                ];
            }

            if(count($children) == 0)
            {
                continue;
            }

            $result[] = ["name" => $type, "translation" => $this->language->get(["type", $type]),
                "children" => $children];
        }

        return $result;
    }

    public function getSettingList($type, $typeID)
    {
        $settings = $this->setting->getSettingList($type, $typeID);
        $result = [];
        foreach($settings as $setting => $value)
        {
            if($value["input"] != "none")
            {
                $result[] = [
                    "name" => $setting, "input" => $value["input"],
                    "value" => $this->setting->get($setting, $type, $typeID),
                    "description" => $this->language->getSetting($setting, $type, $typeID)
                ];
            }
        }

        return $result;
    }
}