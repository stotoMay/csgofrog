<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Model;

use App\Router\Model;

class Group extends Model
{
    public function getGroupList()
    {
        $groups = $this->database->select("group")->fetchAssoc();

        foreach($groups as $key => $group)
        {
            $membercount = $this->database->select("user")
                ->leftJoin("usergroup", ["usergroup.userID[=]user.id"])
                ->where(["OR" => ["usergroup.groupID" => $group["id"], "user.groupID" => $group["id"]]])->numRows();

            $groups[$key]["membercount"] = $membercount;
        }

        return $groups;
    }

    public function getGroup($groupID)
    {
        return $this->database->select("group")->where(["id" => $groupID])->fetchRow();
    }

    public function getMembers($groupID, $page, $pagesize)
    {
        $query = $this->database->select("user")
            ->leftJoin("usergroup", ["usergroup.userID[=]user.id"])
            ->where(["OR" => ["usergroup.groupID" => $groupID, "user.groupID" => $groupID]]);

        $membercount = $query->numRows();
        $members = $query->order("id")->limit(($page - 1) * $pagesize, $pagesize)->fetchAssoc();

        return ["pages" => ceil($membercount / $pagesize), "current" => $page, "results" => $members];
    }

    public function deleteGroup($groupID)
    {
        $rows = $this->database->delete("group")->where(["id" => $groupID, "deleteable" => 1])->getAffectedRows();

        if($rows > 0)
        {
            $this->database->delete("usergroup")->where(["groupID" => $groupID]);

            $this->database->update("user")->set(["groupID" => 1])->where(["groupID" => $groupID]);

            $this->database->delete("permission")->where(["groupID" => $groupID]);
        }
    }

    public function updateGroup($groupID, $name, $color)
    {
        $this->database->update("group")->set(["name" => $name, "color" => $color])->where(["id" => $groupID]);
    }

    public function createGroup($name, $color)
    {
        $this->database->insert("group")->set([
            "name" => $name, "color" => $color, "deleteable" => 1, "created" => time()
        ]);
    }

    public function getGroupName($groupID)
    {
        $row = $this->database->select("group")
            ->column(["name"])->where(["id" => $groupID])
            ->fetchRow();

        return $row["name"];
    }

    public function groupExists($groupID)
    {
        return $this->database->select("group")->where(["id" => $groupID])->exists();
    }
}