<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Model;

use App\Router\Model;

class Language extends Model
{
    public function getLanguageList()
    {
        return get_dir_folders(base_dir($this->config->getLanguageDirectory()));
    }

    public function select($name)
    {
        $exists = $this->database->select("setting")->where([
            "name" => "selected", "type" => "controller", "typeID" => "admin_lang"
        ])->exists();

        if($exists)
        {
            $this->database->update("setting")->set(["value" => $name, "updated" => time()])
                ->where(["name" => "selected", "type" => "controller", "typeID" => "admin_lang"]);
        }
        else
        {
            $this->database->insert("setting")->set([
                "name" => "selected", "value" => $name, "type" => "controller", "typeID" => "admin_lang",
                "updated" => time()
            ]);
        }
    }
}