<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Model;

use App\Router\Model;

class Ban extends Model
{
    private $user = [];

    public function getUserBanList($match, $page = 1, $size = 50)
    {
        $query = $this->database->select("ban")->column([
            "ban.id", "user.username", "ban.expire", "ban.bannerID", "ban.userID", "ban.created"
        ])->leftJoin("user", ["ban.userID[=]user.id"])->where([
            "OR" => ["ban.expire[>=]" => time(), "ban.expire" => NULL], "user.username[~~]" => "%".$match."%"
        ]);

        $pagecount = ceil(($query->numRows()) / $size);
        $banlist = $query->limit(($page - 1) * $size, $size)->fetchAssoc();

        $result = [];
        foreach($banlist as $ban)
        {
            $result[] = [
                "id" => $ban["id"], "username" => $ban["username"], "expire" => $ban["expire"],
                "banner" => $this->getUser($ban["bannerID"]), "userID" => $ban["userID"], "created" => $ban["created"]
            ];
        }

        return ["results" => $result, "pages" => $pagecount, "current" => $page];
    }

    public function getBanIDfromUserID($userID)
    {
        $row = $this->database->select("ban")->column(["id"])
            ->where(["userID" => $userID, "OR" => ["expire" => NULL, "expire[>=]" => time()]])->fetchRow();

        return $row["id"];
    }

    public function getUserBanDetails($id)
    {
        $details = $this->database->select("ban")->column([
            "ban.id", "user.username", "ban.expire", "ban.bannerID", "ban.created", "ban.reason", "ban.userID"
        ])->leftJoin("user", ["ban.userID[=]user.id"])->where(["OR" => ["ban.expire[>=]" => time(),
            "ban.expire" => NULL], "ban.id" => $id])
            ->fetchRow();

        $details["banner"] = $this->getUser($details["bannerID"]);

        return $details;
    }

    public function userBanExists($id)
    {
        return $this->database->select("ban")->where(["id" => $id])->exists();
    }

    public function deleteUserBan($banID)
    {
        $this->database->update("ban")->set(["expire" => time() - 1])->where(["id" => $banID]);
    }

    public function banUser($userID, $expire, $reason, $bannerID)
    {
        $this->database->insert("ban")->set([
            "userID" => $userID, "expire" => $expire, "reason" => $reason, "bannerID" => $bannerID,
            "created" => time()
        ]);
    }

    public function getUser($userID)
    {
        if(!is_array($this->user[$userID]))
        {
            $this->user[$userID] = $this->database->select("user")->where(["id" => $userID])->fetchRow();
        }

        return $this->user[$userID];
    }

    public function getIPBanList($match, $page = 1, $size = 50)
    {
        $query = $this->database->select("ban_ip")->where([
            "OR" => ["expire[>=]" => time(), "expire" => NULL], "ip[~~]" => "%".$match."%"
        ]);

        $pagecount = ceil(($query->numRows()) / $size);
        $banlist = $query->limit(($page - 1) * $size, $size)->fetchAssoc();

        $result = [];
        foreach($banlist as $ban)
        {
            $result[] = [
                "id" => $ban["id"], "ip" => $ban["ip"], "expire" => $ban["expire"],
                "banner" => $this->getUser($ban["bannerID"]), "created" => $ban["created"]
            ];
        }

        return ["results" => $result, "pages" => $pagecount, "current" => $page];
    }

    public function ipBanExists($id)
    {
        return $this->database->select("ban_ip")->where(["id" => $id])->exists();
    }

    public function deleteIPBan($banID)
    {
        $this->database->update("ban_ip")->set(["expire" => time() - 1])->where(["id" => $banID]);
    }

    public function banIP($ip, $expire, $bannerID)
    {
        $this->database->insert("ban_ip")->set([
            "ip" => $ip, "expire" => $expire, "bannerID" => $bannerID,
            "created" => time()
        ]);
    }

    public function isIpBanned($ip)
    {
        return $this->database->select("ban_ip")->where(["ip" => $ip, "OR" => ["expire" => NULL, "expire[>=]" => time()]])->exists();
    }
}