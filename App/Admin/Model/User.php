<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Model;

use App\Admin\Tools\AjaxStatistics;
use App\Router\Model;

class User extends Model
{
    public function getPageUserList($page, $match = NULL, $pagesize = 50)
    {
        if($match)
        {
            $resultcount = $this->database->select("user")
                ->where(["username[~~]" => "%".htmlspecialchars($match, ENT_QUOTES)."%"])->numRows();
            $results = $this->database->select("user")
                ->where(["username[~~]" => "%".htmlspecialchars($match, ENT_QUOTES)."%"])
                ->column(["user.id", "username", "group.name" => "groupname", "user.created", "color"])
                ->leftJoin("group", ["user.groupID[=]group.id"])->order("username", "asc")
                ->limit(($page - 1) * $pagesize, $pagesize)->fetchAssoc();
        }
        else
        {
            $resultcount = $this->database->select("user")->numRows();
            $results = $this->database->select("user")->order("id", "desc")->limit(($page - 1) * $pagesize, $pagesize)
                ->column(["user.id", "username", "group.name" => "groupname", "user.created", "color"])
                ->leftJoin("group", ["user.groupID[=]group.id"])->fetchAssoc();
        }

        return [
            "pages" => ceil($resultcount / $pagesize), "results" => $results, "current" => $page
        ];
    }

    public function getUser($userID)
    {
        return $this->database->select("user")->where(["id" => $userID])->fetchRow();
    }

    public function getBrowser($browserID)
    {
        $info = $this->database->select("browser")->where(["id" => $browserID])->fetchRow();

        if(isset($info["useragent"]))
        {
            $info["browser"] = get_browser_type($info["useragent"]);
        }

        $info["useragent"] = htmlspecialchars($info["useragent"], ENT_QUOTES);

        return $info;
    }

    public function getActivity($type, $id, AjaxStatistics $statistic)
    {
        $this->database->query("SET time_zone = '".$statistic->getTimezone()."'");

        $innerquery = $this->database->select("activity")->column([
            ["str" => "(dateline DIV 600) * 600", "alias" => "requesttime"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(dateline), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where([
            ($type == "browser" ? "browserID" : "userID") => $id,
            "dateline[>=]" => $statistic->getStartTime(), "dateline[<=]" => $statistic->getStopTime()
        ])->group("requesttime")->order("date");

        $data = $this->database->select($innerquery, "tb")
            ->column(["date", ["str" => "(COUNT(*) / ".intval($statistic->getAverageStepTime()/600).") * 100", "alias" => "value"]])
            ->group("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->language->get(["admin", "user", "label_activity"]));
    }

    public function getGroupList()
    {
        return $this->database->select("group")->fetchAssoc();
    }

    public function getSubGroups($userID)
    {
        $subgroups = $this->database->select("usergroup")->where(["userID" => $userID])->fetchAssoc();

        $result = [];
        foreach($subgroups as $subgroup)
        {
            $result[] = $subgroup["groupID"];
        }

        return $result;
    }

    public function getActiveSessions($userID)
    {
        $sessions = $this->database->select("session")
            ->column(["browser.ip", "browser.useragent", "lastaction", "session.id", "session.expire",
                "session.created", "browser.id" => "browserID"])
            ->where(["userID" => $userID, "expire[>=]" => time()])
            ->leftJoin("browser", ["browser.id[=]session.browserID"])
            ->order("lastaction", "desc")
            ->fetchAssoc();

        foreach($sessions as $key => $session)
        {
            $browser = get_browser_type($session["useragent"]);
            $sessions[$key]["browser"] = $browser["name"]." (".$browser["platform"].")";
        }

        return $sessions;
    }

    public function getBrowserSessions($browserID)
    {
        $sessions = $this->database->select("session")
            ->column(["userID", "username", "session.created", "session.expire", "session.id" => "sessionID"])
            ->leftJoin("user", ["user.id[=]session.userID"])
            ->where(["session.browserID" => $browserID])
            ->order("session.id", "asc")
            ->fetchAssoc();

        return $sessions;
    }

    public function getBrowsers($userID)
    {
        $browsers = $this->database->select("session")
            ->column(["browser.ip", "browser.useragent", "browser.lastaction", "browser.id", "browser.dateline"])
            ->where(["userID" => $userID])
            ->leftJoin("browser", ["browser.id[=]session.browserID"])
            ->order("lastaction", "desc")
            ->group("browser.id")
            ->fetchAssoc();

        foreach($browsers as $key => $browser)
        {
            $info = get_browser_type($browser["useragent"]);
            $browsers[$key]["browser"] = $info["name"]." (".$info["platform"].")";
        }

        return $browsers;
    }

    public function removeSession($sessionID)
    {
        $affected_rows = $this->database->update("session")->set(["expire" => time() - 1])->where(["id" => $sessionID])
            ->getAffectedRows();

        return $affected_rows > 0;
    }

    public function setPassword($userID, $password)
    {
        $this->database->update("user")->set(["password" => password_hash($password, PASSWORD_BCRYPT)])
            ->where(["id" => $userID]);
    }

    public function setSecret($userID, $secret)
    {
        $this->database->update("user")->set(["shared_secret" => $this->encrypt($secret)])->where(["id" => $userID]);
    }

    public function setMainGroup($userID, $groupID)
    {
        $this->database->update("user")->set(["groupID" => $groupID])->where(["id" => $userID]);
    }

    public function setSubGroups($userID, $groupIDs)
    {
        if(!is_array($groupIDs))
        {
            return;
        }

        $this->database->delete("usergroup")->where(["userID" => $userID]);

        foreach($groupIDs as $groupID)
        {
            $this->database->insert("usergroup")->set([
                "userID" => $userID, "groupID" => $groupID, "priority" => 50
            ]);
        }
    }

    public function groupExists($groupID)
    {
        return $this->database->select("group")->where(["id" => $groupID])->exists();
    }

    public function userExists($userID)
    {
        return $this->database->select("user")->where(["id" => $userID])->exists();
    }
}