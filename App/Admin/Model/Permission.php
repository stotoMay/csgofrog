<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Model;

use App\Router\Model;

class Permission extends Model
{
    public function getPermissionTypes()
    {
        $types = $this->permission->getPermissionList();
        $result = [];

        foreach($types as $type => $value)
        {
            $children = [];

            foreach($value as $typeID => $permissions)
            {
                $permissioncount = 0;

                foreach($permissions as $permission)
                {
                    if($permission["input"] == "none")
                    {
                        continue;
                    }

                    $permissioncount++;
                }

                if($permissioncount == 0)
                {
                    continue;
                }

                $children[] = [
                    "name" => $typeID, "translation" => $this->language->get(["typeID", $type, $typeID]),
                    "count" => $permissioncount
                ];
            }

            if(count($children) == 0)
            {
                continue;
            }

            $result[] = ["name" => $type, "translation" => $this->language->get(["type", $type]),
                "children" => $children];
        }

        return $result;
    }

    public function getPermissionList($idtype, $id, $type, $typeID)
    {
        $permissions = $this->permission->getPermissionList($type, $typeID);

        if($idtype == "user")
        {
            $values = $this->database->select("permission")->where([
                "type" => $type, "typeID" => $typeID, "userID" => $id
            ])->fetchAssoc();
        }
        else
        {
            $values = $this->database->select("permission")->where([
                "type" => $type, "typeID" => $typeID, "groupID" => $id
            ])->fetchAssoc();
        }

        $defaultList = [];
        $pvalue = [];
        foreach($values as $value)
        {
            if(!isset($value["value"]))
            {
                $pvalue[$value["name"]] = $permissions[$value["name"]]["default"];
            }
            else
            {
                $pvalue[$value["name"]] = $value["value"];
                $defaultList[$value["name"]] = $value["value"];
            }
        }

        $result = [];
        foreach($permissions as $permission => $value)
        {
            $result[] = [
                "name" => $permission, "input" => $value["input"], "value" => $pvalue[$permission],
                "default" => !isset($defaultList[$permission]),
                "description" => $this->language->getPermission($permission, $type, $typeID)
            ];
        }

        return $result;
    }

    public function setPermission($idtype, $id, $name, $value, $type, $typeID)
    {
        $idname = $idtype == "user" ? "userID" : "groupID";

        $exists = $this->database->select("permission")->where([
            "name" => $name, $idname => $id, "type" => $type, "typeID" => $typeID
        ])->exists();

        if($exists)
        {
            $this->database->update("permission")->set(["value" => $value, "updated" => time()])
                ->where(["name" => $name, $idname => $id, "type" => $type, "typeID" => $typeID]);
        }
        else
        {
            $this->database->insert("permission")->set([
                "name" => $name, "value" => $value, "type" => $type, "typeID" => $typeID, $idname => $id,
                "updated" => time()
            ]);
        }
    }

    public function deletePermission($idtype, $id, $name, $type, $typeID)
    {
        $idname = $idtype == "user" ? "userID" : "groupID";

        $this->database->delete("permission")->where([
            "name" => $name, $idname => $id, "type" => $type, "typeID" => $typeID
        ]);
    }

    public function getUsername($userID)
    {
        $row = $this->database->select("user")
            ->column(["username"])
            ->where(["id" => $userID])->fetchRow();

        return $row["username"];
    }

    public function getGroupname($groupID)
    {
        $row = $this->database->select("group")
            ->column(["name"])
            ->where(["id" => $groupID])->fetchRow();

        return $row["name"];
    }

    public function group_exists($groupID)
    {
        return $this->database->select("group")->where(["id" => $groupID])->exists();
    }

    public function user_exists($userID)
    {
        return $this->database->select("user")->where(["id" => $userID])->exists();
    }
}