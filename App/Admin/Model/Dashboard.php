<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Model;

use App\Admin\Tools\AjaxStatistics;
use App\Router\Model;

class Dashboard extends Model
{
    public function getNewUserStats(AjaxStatistics $statistic)
    {
        $this->database->query("SET time_zone = '".$statistic->getTimezone()."'");

        $data = $this->database->select("user")->column([
            ["str" => "COUNT(*)", "alias" => "value"],
            ["str" => "DATE_FORMAT(FROM_UNIXTIME(created), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]
        ])->where(["created[>=]" => $statistic->getStartTime(), "created[<=]" => $statistic->getStopTime()])
            ->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->language->get(["admin", "dashboard", "label_newusers"]));
    }

    public function getActivityStats(AjaxStatistics $statistic)
    {
        $this->database->query("SET time_zone = '".$statistic->getTimezone()."'");

        $innerquery = $this->database->select("activity")
            ->column([["str" => "DATE_FORMAT(FROM_UNIXTIME(dateline), '".$statistic->getMySQLTimeFormat()."')", "alias" => "date"]])
            ->where(["userID[!]" => NULL])->group(["userID", "date"]);

        $data = $this->database->select($innerquery, "tb")
            ->column(["date", "*" => ["function" => "count", "alias" => "value"]])
            ->group("date")->order("date")->fetchAssoc();

        $statistic->addGraphData($data, $this->language->get(["admin", "dashboard", "label_activity"]));
    }
}