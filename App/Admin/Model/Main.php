<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Model;

use App\Router\Model;

class Main extends Model
{
    public function getUser($userID)
    {
        return $this->database->select("user")->where(["id" => $userID])->fetchRow();
    }

    public function getUserSecret($userID)
    {
        $user = $this->database->select("user")->column(["shared_secret"])
            ->where(["id" => $userID])->fetchRow();

        return $this->decrypt($user["shared_secret"]);
    }

    public function isCodeUsed($userID, $authcode)
    {
        return $this->database->select("attempt")
            ->where(["userID" => $userID, "authtoken" => $authcode, "dateline[>=]" => time() - 30 * 8])
            ->exists();
    }

    public function isLoginBlocked($userID)
    {
        $attempts = $this->database->select("attempt")->column(["success"])
            ->where(["userID" => $userID, "dateline[>=]" => time() - 60 * 5])
            ->fetchAssoc();

        $count = 0;
        foreach($attempts as $attempt)
        {
            if($attempt["success"] == 1)
            {
                $count = 0;
            }
            else
            {
                $count++;
            }
        }

        return $count > 5;
    }

    public function addLoginAttempt($browserID, $userID, $success = false, $authtoken = NULL, $mode = NULL)
    {
        $this->database->insert("attempt")->set([
            "mode" => $mode, "browserID" => $browserID, "userID" => $userID,
            "authtoken" => $authtoken, "success" => $success ? 1 : 0, "dateline" => time()
        ]);
    }
}