<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Router\Controller;
use App\Validator\Exception\MultipleValidateException;
use App\Validator\MultipleValidation;

class Group extends Controller
{
    /**
     * @var \App\Admin\Model\Group
     */
    private $model;
    private $admin;

    public static function __info__()
    {
        return [
            "uniqname" => "admin_group",
            "permission" => ["view" => "bool", "add" => "bool", "edit" => "bool", "permission" => "bool",
                "delete" => "bool"],  "priority" => 1,
            "icon" => "group", "tracking" => ["main", "add", "edit", "members"],
            "requirement" => [["application", "general", "admin_control_panel"], ["controller", "admin_group", "view"]]
        ];
    }

    protected function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("App/Admin/Model/Group");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));
    }

    public function main()
    {
        return response($this->theme->draw("Group/main.tpl")
            ->assignVar("groups", $this->model->getGroupList()), $this->request);
    }

    public function members($groupID)
    {
        if($this->request->has("page", false, v()->integer()))
        {
            $page = intval($this->request->input("page", 1, false));
        }
        else
        {
            $page = 1;
        }

        if($page < 1)
        {
            $page = 1;
        }

        $group = $this->model->getGroup($groupID);

        $template = $this->theme->draw("Group/member.tpl");

        $template->assignVar("members", $this->model->getMembers($groupID, $page, 50));
        $template->assignVar("pageurl", "/admin/group/members/".$groupID."?page=%p");
        $template->assignVar("group", $group);

        return response($template, $this->request);
    }

    public function add()
    {
        if($this->getPermission("add") != 1)
        {
            redirect("/admin/group", $this->request)->info($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("Group/add.tpl");

        if($this->request->has("name") && $this->request->has("color"))
        {
            $name = $this->request->input("name");
            $color = $this->request->input("color");

            $validator = new MultipleValidation();

            try
            {
                $validator->append(v()->alnum()->noWhitespaces(), $name)
                    ->append(v()->hexcolor(), $color)
                    ->assert();

                $this->model->createGroup($name, $color);

                redirect("/admin/group/", $this->request)->info($this->language->get([
                    "admin", "group", "message", "add"
                ]));
            }
            catch(MultipleValidateException $e)
            {
                $errors = $e->getFullMessages($this->theme->draw("validator.tpl"), $this->language);
                $this->printError($errors);
            }
        }

        return response($template, $this->request);
    }

    public function edit($groupID)
    {
        if($this->getPermission("edit") != 1 || !$this->model->groupExists($groupID))
        {
            redirect("/admin/group", $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("Group/edit.tpl");
        $group = $this->model->getGroup($groupID);

        if($this->request->has("name") && $this->request->has("color"))
        {
            $name = $this->request->input("name");
            $color = $this->request->input("color");

            $validator = new MultipleValidation();

            try
            {
                $validator->append(v()->alnum()->noWhitespaces(), $name)
                    ->append(v()->hexcolor(), $color)
                    ->assert();

                $this->model->updateGroup($groupID, $name, $color);

                redirect("/admin/group/", $this->request)->info($this->language->get([
                    "admin", "group", "message", "update"
                ]));
            }
            catch(MultipleValidateException $e)
            {
                $errors = $e->getFullMessages($this->theme->draw("validator.tpl"), $this->language);
                $this->printError($errors);
            }
        }

        $template->assignVar("name", $group["name"])->assignVar("color", $group["color"])
            ->assignVar("groupID", $groupID);

        return response($template, $this->request);
    }

    public function delete()
    {
        if(!$this->request->has("groupID", true, v()->integer()))
        {
            redirect("/admin/group", $this->request)->error($this->language->getMessage("standard_error"));
        }

        $groupID = $this->request->input("groupID");

        if($this->getPermission("delete") != 1 || !$this->model->groupExists($groupID))
        {
            redirect("/admin/group", $this->request)->error($this->language->getMessage("permission"));
        }

        $group = $this->model->getGroup($groupID);

        if($group["deleteable"] == 1)
        {
            $this->model->deleteGroup($groupID);

            redirect("/admin/group", $this->request)->info($this->language->get([
                "admin", "group", "message", "delete"
            ]));
        }
        else
        {
            redirect("/admin/group", $this->request)->error($this->language->getMessage("standard_error"));
        }
    }
}