<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Router\Controller;
use App\User\User;
use App\Validator\Exception\MultipleValidateException;
use App\Validator\MultipleValidation;

class Ban extends Controller
{
    /**
     * @var \App\Admin\Model\Ban
     */
    private $model;
    private $admin;

    public static function __info__()
    {
        return [
            "uniqname" => "admin_ban", "permission" => ["ban" => "bool", "delete" => "bool",], "icon" => "ban",
            "tracking" => true, "priority" => 3,
            "requirement" => [["application", "general", "admin_control_panel"], ["controller", "admin_ban", "ban"]]
        ];
    }

    protected function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("App/Admin/Model/Ban");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));
    }

    public function main()
    {
        $template = $this->theme->draw("Ban/userlist.tpl");

        $match = $this->request->input("search", "", false, v()->textstring());
        $page = $this->request->input("page", 1, false, v()->integer());

        $banlist = $this->model->getUserBanList($match, $page, 50);

        if(count($banlist["results"]) == 0 && !empty($match))
        {
            $this->printError($this->language->getMessage("no_users_found"));
        }

        $template->assignVar("banlist", $banlist);
        $template->assignVar("pageurl", "/admin/ban?search=".rawurlencode($match)."&page=%p");
        $template->assignVar("search", $match);

        return response($template, $this->request);
    }

    public function iplist()
    {
        $template = $this->theme->draw("Ban/iplist.tpl");

        $match = $this->request->input("search", "", false, v()->textstring());
        $page = $this->request->input("page", 1, false, v()->integer());

        $banlist = $this->model->getIPBanList($match, $page, 50);

        if(count($banlist["results"]) == 0 && !empty($match))
        {
            $this->printError($this->language->getMessage("no_ips_found"));
        }

        $template->assignVar("banlist", $banlist);
        $template->assignVar("pageurl", "/admin/ban/iplist?search=".rawurlencode($match)."&page=%p");
        $template->assignVar("search", $match);

        return response($template, $this->request);
    }

    public function ip()
    {
        $template = $this->theme->draw("Ban/ip.tpl");

        $ip = NULL;
        if($this->request->has("ip", true, v()->ip()) && $this->request->has("time", true, v()->integer()) && $this->request->has("factor", true, v()->integer()))
        {
            $ip = $this->request->input("ip");
            $time = $this->request->input("time", 0);
            $factor = $this->request->input("factor");

            if(!$this->model->isIpBanned($ip))
            {
                if($factor == 0 || $time == 0)
                {
                    $time = NULL;
                }
                else
                {
                    $time = time() + $time * $factor;
                }

                $this->model->banIP($ip, $time, $this->browser->getUserID());

                redirect("/admin/ban/iplist", $this->request)->info($this->language->get([
                    "admin", "ban", "message", "ip_banned"
                ]));
            }
            else
            {
                $this->printError($this->language->get(["admin", "ban", "message", "ip_already_banned"]));
            }
        }

        return response($template, $this->request);
    }

    public function user($userID)
    {
        $user = $this->model->getUser($userID);

        if(!is_array($user) || $this->getPermission("ban") != 1)
        {
            redirect("/admin/ban", $this->request)->error($this->language->getMessage("permission"));
        }

        if($this->request->has("time", true, v()->integer()) && $this->request->has("factor", true) && $this->request->has("reason", true, v()->textstring()))
        {
            $reason = $this->request->input("reason");
            $time = $this->request->input("time");
            $factor = $this->request->input("factor");

            $u = new User($userID);

            if(!$u->isBanned())
            {
                if($factor == 0 || $time == 0)
                {
                    $time = NULL;
                }
                else
                {
                    $time = time() + $time * $factor;
                }

                $this->model->banUser($userID, $time, $reason, $this->browser->getUserID());

                redirect("/admin/user/view/".$u->getUserID(), $this->request)->info($this->language->get([
                    "admin", "ban", "message", "user_banned"
                ], [$u->getUserName()]));
            }
            else
            {
                $this->printError($this->language->get(["admin", "ban", "message", "user_already_banned"]));
            }
        }

        $template = $this->theme->draw("Ban/user.tpl")->assignVar("user", $user);

        return response($template->parse(), $this->request);
    }

    public function details($id)
    {
        if(!$this->model->userBanExists($id))
        {
            redirect("/admin/ban", $this->request)->error($this->language->getMessage("standard_error"));
        }

        $template = $this->theme->draw("Ban/details.tpl")->assignVar("ban", $this->model->getUserBanDetails($id));

        return response($template, $this->request);
    }

    public function view($userID)
    {
        if(!is_numeric($userID))
        {
            redirect("/admin/ban/", $this->request)->error($this->language->getMessage("standard_error"));
        }

        $banID = $this->model->getBanIDfromUserID($userID);

        if(empty($banID))
        {
            redirect("/admin/ban/", $this->request)->error($this->language->getMessage("standard_error"));
        }

        redirect("/admin/ban/details/".$banID, $this->request);
    }

    public function delete()
    {
        if($this->getPermission("delete") != 1)
        {
            redirect("/admin/ban", $this->request)->error($this->language->getMessage("permission"));
        }

        if(!$this->request->has("banID", true, v()->integer()))
        {
            redirect("/admin/ban", $this->request)->error($this->language->getMessage("standard_error"));
        }

        $id = $this->request->input("banID");

        if($this->request->input("mode", "user") == "ip")
        {
            if(!$this->model->ipBanExists($id))
            {
                redirect("/admin/ban", $this->request)->error($this->language->getMessage("standard_error"));
            }

            $this->model->deleteIPBan($id);

            redirect("/admin/ban/iplist", $this->request)->info($this->language->get(["admin", "ban", "message", "ip_unbanned"]));
        }
        else
        {
            if(!$this->model->userBanExists($id))
            {
                redirect("/admin/ban", $this->request)->error($this->language->getMessage("standard_error"));
            }

            $this->model->deleteUserBan($id);

            redirect("/admin/ban", $this->request)->info($this->language->get(["admin", "ban", "message", "user_unbanned"]));
        }
    }
}