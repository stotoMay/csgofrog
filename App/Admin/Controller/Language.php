<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Router\Controller;

class Language extends Controller
{
    /**
     * @var \App\Admin\Model\Language
     */
    private $model;
    private $admin;

    public static function __info__()
    {
        return [
            "uniqname" => "admin_lang", "permission" => ["change" => "bool",], "setting" => ["selected" => "none"],
            "icon" => "language", "tracking" => true, "priority" => 4,
            "requirement" => [["application", "general", "admin_control_panel"], ["controller", "admin_lang", "change"]]
        ];
    }

    protected function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("App/Admin/Model/Language");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));
    }

    public function main()
    {
        $template = $this->theme->draw("Language/main.tpl")->assignVar("languages", $this->model->getLanguageList())
            ->assignVar("selected", $this->getSetting("selected"));

        return response($template, $this->request);
    }

    public function select($name)
    {
        if($this->getPermission("change") != 1)
        {
            redirect("/admin/language", $this->request)->error($this->language->getMessage("permission"));
        }

        if(file_exists(base_dir($this->config->getLanguageDirectory().$name)))
        {
            $this->model->select($name);

            redirect("/admin/language", $this->request)->info($this->language->get([
                "admin", "language", "message", "selected"
            ]));
        }

        redirect("/admin/language", $this->request)->error($this->language->getMessage("standard_error"));
    }
}