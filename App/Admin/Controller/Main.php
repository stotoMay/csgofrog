<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Router\Controller;
use App\Tools\GoogleAuthenticator;

class Main extends Controller
{
    /**
     * @var \App\Admin\Model\Main
     */
    private $model;

    protected function __init__()
    {
        $this->model = $this->getModel("App/Admin/Model/Main");
        $this->auth();

        if($this->browser->user()->permission()->get("admin_control_panel", "application", "general") != 1)
        {
            redirect("/", $this->request);
        }
    }

    public function main()
    {
        $template = $this->theme->draw("Main/main.tpl");

        if($this->request->has("redirect", false))
        {
            $template->assignVar("redirect", $this->request->input("redirect", NULL, false));
        }

        $template->assignVar("twofactor", $this->app->config()->isTwoFactorProtectionEnabled());

        if($this->request->session()->has("adminsession"))
        {
            redirect("/admin/dashboard", $this->request);
        }

        if($this->request->has("password", true, v()->textstring()))
        {
            $user = $this->model->getUser($this->browser->getUserID());

            if($this->model->isLoginBlocked($this->browser->getUserID()))
            {
                redirect("/admin", $this->request)->error($this->language->get(["admin", "main", "message", "login_banned"]));
            }

            if(password_verify($this->request->input("password"), $user["password"]) && !empty($user["password"]))
            {
                if($this->request->has("redirect", false, v()->textstring()))
                {
                    $url = "/".implode("/", explode("/", rawurldecode($this->request->input("redirect"))));
                }
                else
                {
                    $url = "/admin/dashboard";
                }

                if($this->app->config()->isTwoFactorProtectionEnabled())
                {
                    $secret = $this->model->getUserSecret($this->browser->getUserID());

                    if(!empty($secret))
                    {
                        $code = $this->request->input("code", "", true, v()->textstring());
                        $googleAuthenticator = new GoogleAuthenticator($secret);

                        if($googleAuthenticator->verifyCode($code, 3))
                        {
                            if($this->model->isCodeUsed($this->browser->getUserID(), $code))
                            {
                                redirect("/admin", $this->request)->error($this->language->get(["admin", "main", "message", "twofactorcode_already_used"]));
                            }

                            $this->model->addLoginAttempt($this->browser->getBrowserID(), $this->browser->getUserID(), true, $code, "admin");

                            $this->request->session()->put("adminsession", true);

                            redirect($url, $this->request)->info($this->language->get([
                                "admin", "main", "message", "login_success"
                            ]));
                        }
                    }
                }
                else
                {
                    $this->model->addLoginAttempt($this->browser->getBrowserID(), $this->browser->getUserID(), true, NULL, "admin");

                    $this->request->session()->put("adminsession", true);

                    redirect($url, $this->request)->info($this->language->get([
                        "admin", "main", "message", "login_success"
                    ]));
                }
            }

            $this->model->addLoginAttempt($this->browser->getBrowserID(), $this->browser->getUserID(), false, NULL, "admin");

            $this->printError($this->language->get(["admin", "main", "message", "login_error"]));
        }

        return response($template, $this->request);
    }
}