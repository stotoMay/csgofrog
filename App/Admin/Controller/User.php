<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Admin\Tools\AjaxStatistics;
use App\Router\Controller;
use App\Router\Exception\HTTPException;
use App\Tools\GoogleAuthenticator;

class User extends Controller
{
    /**
     * @var \App\Admin\Model\User
     */
    private $model;
    private $admin;

    public static function __info__()
    {
        return [
            "uniqname" => "admin_user",
            "permission" => [
                "profile" => "bool", "search" => "bool", "group" => "bool", "email" => "bool",
                "password" => "bool", "session" => "bool", "permission" => "bool"
            ], "priority" => 0,
            "icon" => "user", "tracking" => ["main", "page", "view", "browser"],
            "requirement" => [["application", "general", "admin_control_panel"], ["controller", "admin_user", "search"]]
        ];
    }

    protected function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("App/Admin/Model/User");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));
    }

    public function main()
    {
        return $this->page(1);
    }

    public function page($page)
    {
        if(!is_numeric($page) || $page < 1)
        {
            $page = 1;
        }

        $template = $this->theme->draw("User/page.tpl");

        $match = NULL;
        if($this->request->has("search", false) && is_string($this->request->input("search", NULL, false)))
        {
            $match = $this->request->input("search", NULL, false);
        }
        $template->assignVar("match", $match);

        $userlist = $this->model->getPageUserList($page, $match, 50);
        if(!empty($match) && count($userlist["results"]) == 1)
        {
            redirect("/admin/user/view/".$userlist["results"][0]["id"], $this->request);
        }

        if(count($userlist["results"]) == 0)
        {
            $this->printError($this->language->getMessage("no_users_found"));
        }

        $template->assignVar("userlist", $userlist);
        if($match)
        {
            $template->assignVar("pageurl", "/admin/user/page/%p?search=".rawurlencode($match));
        }
        else
        {
            $template->assignVar("pageurl", "/admin/user/page/%p");
        }

        return response($template, $this->request);
    }

    public function view($userID)
    {
        $user = $this->model->getUser($userID);

        if(!is_array($user))
        {
            redirect("/admin/user", $this->request)->error($this->language->getMessage("standard_error"));
        }

        $userclass = new \App\User\User($userID);

        if($userclass->permission()->get("profile", "controller", "admin_user") == 1 && $this->getPermission("profile") != 1)
        {
            throw new HTTPException(500);
        }

        $template = $this->theme->draw("User/view.tpl")->assignVar("user", $user)
            ->assignVar("groups", $this->model->getGroupList())->assignVar("user", $user)
            ->assignVar("maingroup", $user["groupID"])
            ->assignVar("subgroups", $this->model->getSubGroups($user["id"]))
            ->assignVar("sessions", $this->model->getActiveSessions($user["id"]))
            ->assignVar("browsers", $this->model->getBrowsers($user["id"]))
            ->assignVar("isBanned", $userclass->isBanned());

        return response($template, $this->request);
    }

    public function browser($browserID)
    {
        if($this->getPermission("session") != 1)
        {
            redirect("/admin/user", $this->request)->error($this->language->getMessage("permission"));
        }

        $browser = $this->model->getBrowser($browserID);

        if(!is_array($browser))
        {
            redirect("/admin/user", $this->request)->error($this->language->getMessage("standard_error"));
        }

        $template = $this->theme->draw("User/browser.tpl")->assignVar("browserID", $browserID)
            ->assignVar("info", $browser)->assignVar("sessions", $this->model->getBrowserSessions($browserID));

        return response($template, $this->request);
    }

    public function activity($type, $id)
    {
        if(intval($id) != $id || $this->getPermission("session") != 1)
        {
            $result = ["success" => false];

            return response(json_encode($result), $this->request)->contentType("text/json");
        }

        $this->request->session()->closeSessionWrite();

        $statistic = new AjaxStatistics($this->request, 0, 100);

        if($type == "browser")
        {
            $this->model->getActivity("browser", $id, $statistic);
        }
        else
        {
            $this->model->getActivity("user", $id, $statistic);
        }

        return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
    }


    public function deletesession()
    {
        if(!$this->request->has("sessionID", true, v()->integer()))
        {
            redirect("/admin/user/", $this->request)->error($this->language->getMessage("standard_error"));
        }

        $redirecturl = "/admin/user";
        if($this->request->has("userID", true, v()->integer()))
        {
            $redirecturl = "/admin/user/view/".$this->request->input("userID");
        }
        elseif($this->request->has("browserID", true, v()->integer()))
        {
            $redirecturl = "/admin/user/browser/".$this->request->input("browserID");
        }

        $sessionID = $this->request->input("sessionID");

        if($this->getPermission("session") != 1)
        {
            redirect($redirecturl, $this->request)->error($this->language->getMessage("permission"));
        }

        if($this->model->removeSession($sessionID))
        {
            redirect($redirecturl, $this->request)->info($this->language->get(["admin", "user", "message",
                "session_deleted"]));
        }

        redirect($redirecturl, $this->request)->error($this->language->getMessage("standard_error"));
    }

    public function sendemail()
    {
        if(!$this->request->has("userID", true, v()->integer()))
        {
            redirect("/admin/user/", $this->request)->error($this->language->getMessage("standard_error"));
        }

        $user = $this->model->getUser($this->request->input("userID"));

        if($this->getPermission("email") != 1)
        {
            redirect("/admin/user/view/".$user["id"], $this->request)->error($this->language->getMessage("permission"));
        }

        if($this->request->has("email-header") && $this->request->has("email-text") && is_array($user))
        {
            if(empty($user["email"]))
            {
                redirect("/admin/user/view/".$user["id"], $this->request)->error($this->language->get(["admin", "user",
                    "message", "email_missing"]));
            }

            admin_mail($user["email"], $this->request->input("email-header"), $this->request->input("email-text"));

            redirect("/admin/user/view/".$user["id"], $this->request)->info($this->language->get(["admin", "user",
                "message", "email_sent"]));
        }

        redirect("/admin/user/view/".$user["id"], $this->request)->error($this->language->getMessage("standard_error"));
    }

    public function changepassword()
    {
        if(!$this->request->has("userID", true, v()->integer()))
        {
            redirect("/admin/user/", $this->request)->error($this->language->getMessage("standard_error"));
        }

        $user = $this->model->getUser($this->request->input("userID"));

        if($this->getPermission("password") != 1)
        {
            redirect("/admin/user/view/".$user["id"], $this->request)->error($this->language->getMessage("permission"));
        }

        if($this->request->has("password") && $this->request->has("password_repeat") && is_array($user))
        {
            $password = $this->request->input("password");

            if($password == $this->request->input("password_repeat") && !empty($password) && is_string($password))
            {
                $this->model->setPassword($user["id"], $password);

                redirect("/admin/user/view/".$user["id"], $this->request)->info($this->language->get(["admin", "user",
                    "message", "password_changed"]));
            }
            else
            {
                redirect("/admin/user/view/".$user["id"], $this->request)->error($this->language->get(["admin", "user",
                    "message", "wrong_password_repeat"]));
            }
        }

        redirect("/admin/user/view/".$user["id"], $this->request)->error($this->language->getMessage("standard_error"));
    }

    public function setgroup()
    {
        if(!$this->request->has("userID", true, v()->integer()))
        {
            redirect("/admin/user/", $this->request)->error($this->language->getMessage("standard_error"));
        }

        $user = $this->model->getUser($this->request->input("userID"));

        if($this->getPermission("group") != 1)
        {
            redirect("/admin/user/view/".$user["id"], $this->request)->error($this->language->getMessage("permission"));
        }

        if($this->request->has("maingroup") && is_array($user))
        {
            $maingroup = $this->request->input("maingroup");
            $subgroups = $this->request->input("subgroup");

            if(!is_array($subgroups))
            {
                $subgroups = [];
            }

            if($this->model->groupExists($maingroup) && (count($subgroups) == 0 || $this->model->groupExists($subgroups)))
            {
                $this->model->setMainGroup($user["id"], $maingroup);
                $this->model->setSubGroups($user["id"], $subgroups);

                redirect("/admin/user/view/".$user["id"], $this->request)->info($this->language->get(["admin", "user", "message", "group_changed"]));
            }
        }

        redirect("/admin/user/view/".$user["id"], $this->request)->error($this->language->getMessage("standard_error"));
    }

    public function generatesecret()
    {
        if(!$this->request->has("userID", true, v()->integer()))
        {
            $response = ["success" => false];
        }
        else
        {
            $userID = $this->request->input("userID");

            if($this->model->userExists($userID))
            {
                $secret = GoogleAuthenticator::generateSecret(32);
                $authenticator = new GoogleAuthenticator($secret);

                $this->model->setSecret($userID, $secret);

                $response = ["success" => true, "secret" => $secret, "qr" => $authenticator->getQRCodeGoogleUrl($this->request->servername())];
            }
            else
            {
                $response = ["success" => false];
            }
        }

        return response(json_encode($response), $this->request)->contentType("text/json");
    }
}