<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Admin\Tools\AjaxStatistics;
use App\Router\Controller;

class Dashboard extends Controller
{
    /**
     * @var \App\Admin\Model\Dashboard
     */
    private $model;
    /**
     * @var Admin
     */
    private $admin;

    public static function __info__()
    {
        return [
            "uniqname" => "admin_dashboard", "icon" => "dashboard", "priority" => 100,
            "tracking" => true,
            "requirement" => [["application", "general", "admin_control_panel"]]
        ];
    }

    protected function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("App/Admin/Model/Dashboard");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));
    }

    public function main()
    {
        $template = $this->theme->draw("Dashboard/main.tpl");

        return response($template, $this->request);
    }

    public function newusers()
    {
        $statistic = new AjaxStatistics($this->request, 0);

        $this->request->session()->closeSessionWrite();
        $this->model->getNewUserStats($statistic);

        return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
    }

    public function activity()
    {
        $statistic = new AjaxStatistics($this->request, 0);

        $this->request->session()->closeSessionWrite();
        $this->model->getActivityStats($statistic);

        return response($statistic->renderJsonReponse(), $this->request)->contentType("text/json");
    }
}