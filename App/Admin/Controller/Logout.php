<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Router\Controller;

class Logout extends Controller
{
    public function __init__()
    {
        $this->authAdmin();
    }

    public function main()
    {
        $this->request->session()->forget("adminsession");

        session_destroy();
        session_start();

        redirect("/", $this->request)->info($this->language->get(["admin", "main", "message", "logout"]));
    }
}