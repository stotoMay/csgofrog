<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Router\Controller;
use App\Validator\Exception\MultipleValidateException;
use App\Validator\MultipleValidation;

class Setting extends Controller
{
    /**
     * @var \App\Admin\Model\Setting
     */
    private $model;
    private $admin;

    public static function __info__()
    {
        return [
            "uniqname" => "admin_setting", "permission" => ["edit" => "bool"], "icon" => "cogs", "tracking" => true,
            "requirement" => [["application", "general", "admin_control_panel"],
                ["controller", "admin_setting", "edit"]], "priority" => 2,
        ];
    }

    protected function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("App/Admin/Model/Setting");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));
    }

    public function main()
    {
        $template = $this->theme->draw("Setting/main.tpl")->assignVar("types", $this->model->getSettingTypes());

        return response($template, $this->request);
    }

    public function edit($type, $typeID)
    {
        if(empty($type) || empty($typeID) || $this->getPermission("edit") != 1)
        {
            redirect("/admin/setting", $this->request)->error($this->language->getMessage("permission"));
        }

        $settings = $this->setting->getSettingList($type, $typeID);

        $post = $this->request->all();
        if(is_array($post) && count($post) > 0)
        {
            $validators = new MultipleValidation();

            foreach($post as $key => $value)
            {
                if(!isset($settings[$key]))
                {
                    continue;
                }

                if(v()->input($settings[$key]["input"])->validate($value))
                {
                    $this->model->setSetting($key, $value, $type, $typeID);
                }
                else
                {
                    $validators->append(v()->input($settings[$key]["input"]), $value);
                }
            }

            try
            {
                $validators->assert();

                redirect("/admin/setting", $this->request)->info($this->language->get(["admin", "setting", "message",
                    "setting_updated"]));
            }
            catch(MultipleValidateException $e)
            {
                redirect("/admin/setting/edit/".$type."/".$typeID, $this->request)
                    ->error($e->getFullMessages($this->theme->draw("validator.tpl"), $this->language));
            }
        }

        $template = $this->theme->draw("Setting/edit.tpl")
            ->assignVar("settings", $this->model->getSettingList($type, $typeID))->assignVar("type", $type)
            ->assignVar("typeID", $typeID)
            ->assignVar("heading", $this->language->get(["typeID", $type, $typeID]));

        return response($template, $this->request);
    }
}