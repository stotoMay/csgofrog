<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Controller;

use App\Admin\Admin;
use App\Router\Controller;
use App\Validator\Exception\MultipleValidateException;
use App\Validator\MultipleValidation;

class Permission extends Controller
{
    /**
     * @var \App\Admin\Model\Permission
     */
    private $model;
    private $admin;

    public static function __info__()
    {
        return [
            "tracking" => true, "requirement" => [["application", "general", "admin_control_panel"]]
        ];
    }

    protected function __init__()
    {
        $this->authAdmin();
        $this->model = $this->getModel("App/Admin/Model/Permission");
        $this->admin = new Admin($this->request, $this->browser, $this->app, $this->theme);
        $this->theme->assignVar("navigation", $this->admin->getNavigation(get_class($this)));
    }

    public function select($type, $id)
    {
        if($this->browser->user()->permission()->getController("admin_".$type, "permission") != 1 || ($type != "user" && $type != "group"))
        {
            redirect("/admin/".$type, $this->request)->error($this->language->getMessage("permission"));
        }

        $template = $this->theme->draw("Permission/select.tpl");

        $template->assignVar("idtype", $type);
        $template->assignVar("id", $id);
        $template->assignVar("types", $this->model->getPermissionTypes());

        if($type == "user")
        {
            $template->assignVar("username", $this->model->getUsername($id));
        }
        else
        {
            $template->assignVar("groupname", $this->model->getGroupname($id));
        }

        return response($template, $this->request);
    }

    public function edit($idtype, $id, $type, $typeID)
    {
        if($this->browser->user()->permission()->getController("admin_".$idtype, "permission") != 1 || ($idtype != "user" && $idtype != "group"))
        {
            redirect("/admin/".$idtype, $this->request)->error($this->language->getMessage("permission"));
        }

        $post = $this->request->all();
        if(is_array($post) && count($post) > 0)
        {
            $validators = new MultipleValidation();

            $permissions = $this->permission->getPermissionList($type, $typeID);
            foreach($post as $permission => $value)
            {
                if(!isset($permissions[$permission]))
                {
                    continue;
                }

                if($this->request->has($permission."-default") && $this->request->input($permission."-default") == "1")
                {
                    $this->model->deletePermission($idtype, $id, $permission, $type, $typeID);
                }
                elseif(v()->input($permissions[$permission]["input"])->validate($value))
                {
                    $this->model->setPermission($idtype, $id, $permission, $value, $type, $typeID);
                }
                else
                {
                    $validators->append(v()->input($permissions[$permission]["input"]), $value);
                }
            }

            try
            {
                $validators->assert();

                if($idtype == "group")
                {
                    redirect("/admin/permission/select/".$idtype."/".$id, $this->request)->info($this->language->get(["admin",
                        "permission", "message", "permission_group_updated"]));
                }
                else
                {
                    redirect("/admin/permission/select/".$idtype."/".$id, $this->request)->info($this->language->get(["admin",
                        "permission", "message", "permission_user_updated"]));
                }
            }
            catch(MultipleValidateException $e)
            {
                redirect("/admin/permission/edit/".$idtype."/".$id."/".$type."/".$typeID, $this->request)
                    ->error($e->getFullMessages($this->theme->draw("validator.tpl"), $this->language));
            }

            redirect("/admin/permission/select/".$idtype."/".$id, $this->request)->info($this->language->getMessage("permission_updated"));
        }

        $template = $this->theme->draw("Permission/permission.tpl")->assignVar("idtype", $idtype)
            ->assignVar("id", $id)->assignVar("type", $type)->assignVar("typeID", $typeID)
            ->assignVar("permissions", $this->model->getPermissionList($idtype, $id, $type, $typeID));

        if($idtype == "user")
        {
            $template->assignVar("username", $this->model->getUsername($id));
        }
        else
        {
            $template->assignVar("groupname", $this->model->getGroupname($id));
        }

        return response($template, $this->request);
    }
}