<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin\Tools;

use App\HTTP\Request;

class AjaxStatistics
{
    private $request;

    private $timezone;
    private $steptype;

    private $starttime;
    private $stoptime;

    private $response;

    public function __construct(Request $request, $min = null, $max = null)
    {
        $this->request = $request;

        $this->timezone = intval($this->request->input("timezone", 0, false, v()->integer()->min(-12)->max(12)));
        $this->starttime = intval($this->request->input("starttime", 0, false, v()->integer()));
        $this->stoptime = intval($this->request->input("stoptime", time(), false, v()->integer()));
        $this->steptype = $this->request->input("steptype", "Y-m-d-H", false, v()->textstring());

        $allowedtypes = ["Y-m-d-H", "Y-m-d",  "Y-m", "H", "w"];

        if(!is_int(array_search($this->steptype, $allowedtypes)))
        {
            $this->steptype = $allowedtypes[0];
        }

        $this->response = ["success" => true, "datasets" => [], "labels" => $this->steptype, "min" => $min, "max" => $max];
    }

    public function getTimezone()
    {
        $padded = str_pad(abs($this->timezone), 2, '0', STR_PAD_LEFT);
        if($this->timezone < 0)
        {
            $timezone = "-".$padded.":00";
        }
        else
        {
            $timezone = "+".$padded.":00";
        }

        return $timezone;
    }

    public function getTimezoneOffset()
    {
        return $this->timezone*3600;
    }

    public function getStartTime()
    {
        return $this->starttime;
    }

    public function getStopTime()
    {
        return $this->stoptime;
    }

    public function getPHPTimeFormat()
    {
        return $this->steptype;
    }

    public function getAverageStepTime()
    {
        $types = ["Y-m-d-H" => 3600, "Y-m-d" => 3600*24,  "Y-m" => intval(3600*24*30.42), "H" => 3600*24*7*10, "w" => 3600*24*7*10];

        return $types[$this->steptype];
    }

    public function getMySQLTimeFormat()
    {
        $types = ["Y-m-d-H" => "%Y-%m-%d-%H", "Y-m-d" => "%Y-%m-%d",  "Y-m" => "%Y-%m", "H" => "%H", "w" => "%w"];

        return $types[$this->steptype];
    }

    public function addGraphData($rows, $label)
    {
        $data = [];

        foreach($rows as $row)
        {
            $data[$row["date"]] = $row["value"];
        }

        $this->response["datasets"] = array_merge([["data" => $data, "label" => $label]], $this->response["datasets"]);
    }

    public function renderJsonReponse()
    {
        return json_encode($this->response);
    }
}