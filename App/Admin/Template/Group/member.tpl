<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("admin", "group", "title_member")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/group">{language("admin", "group", "breadcrumb_groups")}</a></li>
    <li class="breadcrumb-item"><a href="/admin/group">{$group.name}</a></li>
    <li class="breadcrumb-item active">{language("admin", "group", "breadcrumb_members")}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-heading">{$group.name}</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>{language("admin", "user", "userid")}</th>
                    <th>{language("admin", "user", "list_username")}</th>
                    <th>{language("admin", "user", "list_registered")}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {foreach $members.results as $user}
                    <tr>
                        <td>#{$user.id}</td>
                        <td>{$user.username}</td>
                        <td>{datetimeformat($user.created)}</td>
                        <td>
                            <a href="/admin/user/view/{$user.id}" class="btn btn-default">{language("admin", "user", "list_details")}</a>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
        <div class="center-block">{pagination($members.pages, $members.current, $pageurl)}</div>
    </div>
</div>
{includetemplate="footer.tpl"}
</body>
</html>