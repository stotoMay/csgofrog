<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("admin", "group", "title_add")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/group">{language("admin", "group", "breadcrumb_groups")}</a></li>
    <li class="breadcrumb-item active">{language("admin", "group", "breadcrumb_add")}</li>
</ol>


<div class="panel panel-default">
    <div class="panel-heading">Add Group</div>
    <div class="panel-body">
        <div class="table-responsive">
            <form method="post" action="/admin/group/add">
                <table class="table table-borderless">
                    <tr>
                        <td><label for="name">{language("admin", "group", "groupname")}</label></td>
                        <td><input name="name" id="name" type="text" class="form-control"></td>
                    </tr>
                    <tr>
                        <td><label for="color">{language("admin", "group", "groupcolor")}</label></td>
                        <td><input name="color" id="color" type="color" class="form-control"></td>
                    </tr>
                </table>
                <input type="submit" value='{language("submit")}' class="btn btn-primary">
                <input type="hidden" name="token" value="{token()}">
            </form>
        </div>
    </div>
</div>
{includetemplate="footer.tpl"}
</body>
</html>