<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("admin", "group", "title_edit")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/group">{language("admin", "group", "breadcrumb_groups")}</a></li>
    <li class="breadcrumb-item"><a href="/admin/group">{$name}</a></li>
    <li class="breadcrumb-item active">{language("admin", "group", "breadcrumb_edit")}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-heading">Edit Group</div>
    <div class="panel-body">
        <div class="table-responsive">
            <form method="post" action="/admin/group/edit/{$groupID}">
                <table class="table table-borderless">
                    <tr>
                        <td><label for="name">{language("admin", "group", "groupname")}</label></td>
                        <td><input name="name" class="form-control" value="{$name}" id="name" type="text"></td>
                    </tr>
                    <tr>
                        <td><label for="color">{language("admin", "group", "groupcolor")}</label></td>
                        <td><input name="color" class="form-control" id="color" value="{$color}" type="color"></td>
                    </tr>
                </table>
                <input type="submit" class="btn btn-primary" value='{language("submit")}'>
                <input type="hidden" name="token" value="{token()}">
            </form>
        </div>
    </div>
</div>
{includetemplate="footer.tpl"}
</body>
</html>