<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("admin", "group", "title_main")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}
<div class="panel panel-default">
    <div class="panel-heading">{language("admin", "group", "groups")}</div>
    <div class="panel-body">
        <div class="table-responsive">
            <form method="post" action="/admin/group/delete">
                <table class="table">
                    <thead>
                    <tr>
                        <th>{language("admin", "group", "groupid")}</th>
                        <th>{language("admin", "group", "name")}</th>
                        <th>{language("admin", "group", "color")}</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    {foreach $groups as $group}
                        <tr>
                            <td><span>#{$group.id}</span></td>
                            <td><span>{$group.name}</span></td>
                            <td><span style="color:{$group.color}">{$group.color}</span></td>
                            {if permission("permission", "controller", "admin_group") == 1}
                                <td>
                                    <a href="/admin/permission/select/group/{$group.id}" class="btn btn-default">{language("admin", "group", "permissions")}</a>
                                </td>
                            {/if}
                            <td>
                                <a href="/admin/group/members/{$group.id}" class="btn btn-default {if $group.membercount == 0}disabled{/if}">{language("admin", "group", "members")}</a>
                            </td>
                            {if permission("edit", "controller", "admin_group") == 1}
                                <td>
                                    <a href="/admin/group/edit/{$group.id}" class="btn btn-default">{language("admin", "group", "edit")}</a>
                                </td>
                            {/if}
                            {if permission("delete", "controller", "admin_group") == 1 && $group.deleteable == "1"}
                                <td>
                                    <button value="{$group.id}" name="groupID" class="btn btn-primary">{language("admin", "group", "delete")}</button>
                                </td>
                            {else}
                                <td></td>
                            {/if}
                        </tr>
                    {/foreach}
                </table>
                <input type="hidden" name="token" value="{token()}">
            </form>
        </div>
        {if permission("add", "controller", "admin_group") == "1"}
            <a href="/admin/group/add" class="btn btn-default">{language("admin", "group", "addgroup")}</a>
        {/if}
    </div>
</div>
{includetemplate="footer.tpl"}
</body>
</html>