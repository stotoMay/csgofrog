function print_error(msg) {
    var $e = $(document.createElement("div"));
    $e.attr("class", "alert alert-danger");
    $e.html(msg);
    $("#messages").append($e);

    setTimeout(function () {
        $e.fadeOut();
    }, 10000);
}

function print_info(msg) {
    var $e = $(document.createElement("div"));
    $e.attr("class", "alert alert-success");
    $e.html(msg);
    $("#messages").append($e);

    setTimeout(function () {
        $e.fadeOut();
    }, 10000);
}

function setTimeOffset() {
    var now = new Date();
    var later = new Date();

    var jan = new Date(now.getFullYear(), 0, 1);
    var jul = new Date(later.getFullYear(), 6, 1);
    var offset = Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());

    later.setTime(now.getTime() + 365 * 24 * 60 * 60 * 1000);
    document.cookie = g_timezoneCookieName+'=' + (-60 * offset) + '; expires=' + later + '; path=/';
}

function padTime(t) {
    if ((t + "").length == 1) {
        return "0" + t;
    }

    return t;
}

function showConfirmDialog(message) {
    var deferred = $.Deferred();
    var $dialog = $("#dialog");
    var $body = $("body");

    var $confirmbutton = $("#dialog-confirm");
    var $cancelbutton = $("#dialog-cancel");
    var $dialogmessage = $("#dialog-message");

    $dialog.css({
        "position": "absolute",
        "top": "0", "left": "0", "right": "0", "bottom": "0",
        "background": "url('/App/Admin/Template/Static/Image/black30.png')"
    });
    $dialogmessage.html(message);

    var fnConfirm = function () {
        deferred.resolve();
    };
    var fnCancel = function () {
        deferred.reject();
    };
    $body.on("keypress", "input", function (args) {
        if (args.keyCode == 13) {
            deferred.resolve();
        }
    });

    $confirmbutton.click(fnConfirm);
    $cancelbutton.click(fnCancel);

    deferred.always(function () {
        $confirmbutton.unbind("click");
        $cancelbutton.unbind("click");
        $dialog.hide();
    });
    $dialog.show();

    return deferred.promise();
}

function renderStatsGraph($container, statspage) {
    var pagecount = 1;

    var currentDate = new Date();
    var timezone = currentDate.getTimezoneOffset() / (-60);

    var steptype = null;
    var starttime = 0;
    var stoptime = 0;
    var pagesize = 0;

    var $canvas = $(document.createElement("canvas")).css({"width": "100%"});
    var $ctx = $canvas[0].getContext("2d");

    var $button_group_pages = $(document.createElement("div")).addClass("btn-group").addClass("btn-group-justified").css("margin-top", 20);
    var $button_day = $(document.createElement("div")).addClass("btn").addClass("btn-default").addClass("active").html("Hour").click(function () {
        updateChartConfig($(this), "Y-m-d-H", 1);
    });
    var $button_week = $(document.createElement("div")).addClass("btn").addClass("btn-default").html("Day").click(function () {
        updateChartConfig($(this), "Y-m-d", 1);
    });
    var $button_month = $(document.createElement("div")).addClass("btn").addClass("btn-default").html("Month").click(function () {
        updateChartConfig($(this), "Y-m", 1);
    });

    var $button_group_general = $(document.createElement("div")).addClass("btn-group").addClass("btn-group-justified").css("margin-top", 20);
    var $button_daytime = $(document.createElement("div")).addClass("btn").addClass("btn-default").html("Daytime Average (Last 10 Weeks)").click(function () {
        updateChartConfig($(this), "H");
    });
    var $button_weekday = $(document.createElement("div")).addClass("btn").addClass("btn-default").html("Weekday Average (Last 10 Weeks)").click(function () {
        updateChartConfig($(this), "w");
    });

    var $button_selected = $button_day;

    var $button_previous = $(document.createElement("div")).addClass("btn").addClass("btn-default").html("&laquo;").click(function () {
        pagecount += 1;
        $button_next.removeClass("disabled");
        updateChartConfig($button_selected, steptype, pagecount);
    });
    var $button_next = $(document.createElement("div")).addClass("btn").addClass("btn-default").html("&raquo;").click(function () {
        if (pagecount <= 1) {
            return;
        }

        pagecount -= 1;

        if (pagecount <= 1) {
            $button_next.addClass("disabled")
        }

        updateChartConfig($button_selected, steptype, pagecount);
    }).addClass("disabled");
    var $button_export = $(document.createElement("div")).addClass("btn").addClass("btn-primary").addClass("btn-block").html("Export to CSV").click(function () {
        exportChartData(steptype);
    });

    $button_group_pages.append($button_previous).append($button_month).append($button_week).append($button_day).append($button_next);
    $button_group_general.append($button_daytime).append($button_weekday);
    $container.append($canvas).append($button_export).append($button_group_general).append($button_group_pages);

    var chart = new Chart($ctx, {
        type: 'line',
        data: {"labels": [], "datasets": []},
        options: {"responsive": true}
    });

    function disableButtons() {
        $container.find(".btn").addClass("disabled");
    }

    function enableButtons(arePagesEnabled) {
        $container.find(".btn").removeClass("disabled");

        if(arePagesEnabled === false)
        {
            $button_next.addClass("disabled");
            $button_previous.addClass("disabled");
        }
        else
        {
            $button_next.removeClass("disabled");
            $button_previous.removeClass("disabled");
        }

        if(pagecount == 1) {
            $button_next.addClass("disabled");
        }
    }

    function translateLabel(_label, _steptype) {
        var split = (""+_label).split("-");
        var translation = "";

        if (_steptype == "Y-m") {
            translation = _label;
        }
        else if (_steptype == "Y-m-d") {
            translation = split[2]+"."+split[1]+"."+split[0];
        }
        else if(_steptype == "w")
        {
            var weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            translation = weekdays[parseInt(_label)];
        }
        else if(_steptype == "H")
        {
            translation = padTime(_label)+":00";
        }
        else {
            translation = split[2]+"."+split[1]+"."+split[0]+" "+padTime(split[3])+":00";
        }

        return translation;
    }

    function prepareValue(_value, _steptype) {
        var v = 0;
        if(_steptype == "w")
        {
            v = parseFloat(_value) / 10;
        }
        else if(_steptype == "H")
        {
            v = parseFloat(_value) / (7 * 10);
        }
        else
        {
            v = parseFloat(_value);
        }

        return v;
    }

    function updateChartConfig($button, _steptype, _pagecount) {
        var startDate, endDate;

        if (typeof _pagecount === "undefined") {
            pagecount = 1;
        }
        else {
            pagecount = parseInt(_pagecount);
        }

        var pagesEnabled = true;

        var currentDate = new Date();

        if (_steptype == "Y-m") {
            steptype = _steptype;
            pagesize = 12;

            startDate = new Date(currentDate.getFullYear(), currentDate.getMonth() - (pagecount * pagesize) + 1, 1);

            endDate = new Date(startDate.getTime());
            endDate.setMonth(startDate.getMonth() + pagesize);
            endDate.setSeconds(endDate.getSeconds() - 1);

            starttime = Math.ceil(startDate.getTime() / 1000);
            stoptime = Math.floor(endDate.getTime() / 1000);
        }
        else if (_steptype == "Y-m-d") {
            steptype = _steptype;
            pagesize = 21;

            startDate = new Date();
            startDate.setHours(0, 0, 0, 0);
            startDate.setDate(startDate.getDate() - pagecount * pagesize + 1);

            endDate = new Date(startDate.getTime());
            endDate.setDate(endDate.getDate() + pagesize);
            endDate.setSeconds(endDate.getSeconds() - 1);

            starttime = Math.ceil(startDate.getTime() / 1000);
            stoptime = Math.floor(endDate.getTime() / 1000);
        }
        else if(_steptype == "w")
        {
            steptype = _steptype;
            pagesize = 7;
            pagesEnabled = false;

            starttime = Math.floor(currentDate.getTime() / 1000) - 3600 * 24 * 7 * 10;
            stoptime = Math.floor(currentDate.getTime() / 1000);
        }
        else if(_steptype == "H")
        {
            steptype = _steptype;
            pagesize = 24;
            pagesEnabled = false;

            starttime = Math.floor(currentDate.getTime() / 1000) - 3600 * 24 * 7 * 10;
            stoptime = Math.floor(currentDate.getTime() / 1000);
        }
        else {
            steptype = "Y-m-d-H";
            pagesize = 24;

            startDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), currentDate.getHours() - pagecount * pagesize + 1);

            endDate = new Date(startDate.getTime());
            endDate.setHours(startDate.getHours() + pagesize);
            endDate.setSeconds(endDate.getSeconds() - 1);

            starttime = Math.ceil(startDate.getTime() / 1000);
            stoptime = Math.floor(endDate.getTime() / 1000);
        }

        $container.find(".btn.active").removeClass("active");
        $button.addClass("active");
        $button_selected = $button;

        disableButtons();

        $.ajax({
            "data": {
                "steptype": steptype,
                "starttime": starttime,
                "stoptime": stoptime,
                "timezone": timezone
            },
            "success": function (data) {
                if (data["success"] !== true) {
                    return;
                }

                var iterDate = new Date(starttime * 1000);
                var i, n;

                var translatedLabels = [];
                var labels = [];

                for (i = 0; i < pagesize; i++) {
                    if (steptype == "Y-m") {
                        labels.push(iterDate.getFullYear() + "-" + padTime(iterDate.getMonth() + 1));
                        iterDate.setMonth(iterDate.getMonth() + 1)
                    }
                    else if (steptype == "Y-m-d") {
                        labels.push(iterDate.getFullYear() + "-" + padTime(iterDate.getMonth() + 1) + "-" + padTime(iterDate.getDate()));
                        iterDate.setDate(iterDate.getDate() + 1);
                    }
                    else if (steptype == "w")
                    {
                        labels.push((i + 1) % 7);
                    }
                    else if (steptype == "H")
                    {
                        labels.push(padTime(i));
                    }
                    else {
                        labels.push(iterDate.getFullYear() + "-" + padTime(iterDate.getMonth() + 1) + "-" + padTime(iterDate.getDate()) + "-" + padTime(iterDate.getHours()));
                        iterDate.setHours(iterDate.getHours() + 1)
                    }
                }

                for (i = 0; i < labels.length; i++)
                {
                    translatedLabels.push(translateLabel(labels[i], steptype));
                }

                var datasets = [];
                var colors = [[41,128,185], [142,68,173], [44,62,80], [127,140,141]];

                for (i = 0; i < data["datasets"].length; i++) {
                    var dataset = {
                        "label": data["datasets"][i]["label"], "data": [],
                        "backgroundColor": "rgba("+colors[i % colors.length][0]+","+colors[i % colors.length][1]+","+colors[i % colors.length][2]+",0.2)",
                        "borderColor": "rgba("+colors[i % colors.length][0]+","+colors[i % colors.length][1]+","+colors[i % colors.length][2]+",0.8)"
                    };

                    for (n = 0; n < labels.length; n++) {
                        if (labels[n] in data["datasets"][i]["data"]) {
                            dataset["data"].push(prepareValue(data["datasets"][i]["data"][labels[n]], steptype));
                        }
                        else {
                            dataset["data"].push(0);
                        }
                    }

                    datasets.push(dataset);
                }

                if (typeof data["min"] == "number") {
                    chart.options.scales.yAxes[0].ticks.min = parseInt(data["min"])
                }

                if (typeof data["max"] == "number") {
                    chart.options.scales.yAxes[0].ticks.max = parseInt(data["max"])
                }

                chart.config.data = {"labels": translatedLabels, "datasets": datasets};
                chart.update();

                enableButtons(pagesEnabled);
            },
            "url": statspage, "dataType": "json"
        });
    }

    function exportChartData(_steptype) {
        if (_steptype == "Y-m") {
            steptype = _steptype;
        }
        else if (_steptype == "Y-m-d") {
            steptype = _steptype;
        }
        else if (_steptype == "H") {
            steptype = _steptype;
        }
        else if (_steptype == "w") {
            steptype = _steptype;
        }
        else {
            steptype = "Y-m-d-H";
        }

        disableButtons();

        $.ajax({
            "data": {
                "steptype": steptype,
                "starttime": 0,
                "stoptime": Math.ceil(Date.now() / 1000),
                "timezone": timezone
            },
            "success": function (data) {
                if(data["success"] !== true)
                {
                    return;
                }

                var csvContent = "data:text/csv;charset=utf-8,";
                var i;

                for(i = 0; i < data["datasets"].length; i++)
                {
                    csvContent += "\"Date\";\"" + data["datasets"][i]["label"]+"\"\n";
                    for(var date in data["datasets"][i]["data"])
                    {
                        if (data["datasets"][i]["data"].hasOwnProperty(date)) {
                            var val = prepareValue(data["datasets"][i]["data"][date], steptype);

                            val = (""+val).replace(".", ",");

                            csvContent += "\"" + translateLabel(date, steptype) + "\";\"" + val + "\"\n"
                        }
                    }
                }

                var $link = $(document.createElement("a")).attr({"href": encodeURI(csvContent), "download": "graph.csv", "target": "_blank"});
                document.body.appendChild($link[0]);
                $link[0].click();

                enableButtons(true)
            },
            "url": statspage, "dataType": "json"
        });
    }

    updateChartConfig($button_day, "Y-m-d-H", 1);
}

function renderDefaultGraph($container, values) {
    var $canvas = $(document.createElement("canvas")).css({"width": "100%"});
    var $ctx = $canvas[0].getContext("2d");

    $container.append($canvas);

    var labels = [];
    for (var i = 0; i < values.length; i++) {
        labels.push("");
    }

    var opts = {"responsive": true};

    if (values.length > 100) {
        opts["elements"] = {point: {radius: 0}};
    }

    new Chart($ctx, {
        type: 'line',
        data: {
            "labels": labels, "datasets": [{"label": "", "data": values, "backgroundColor": "rgba(41,128,185,0.2)", "borderColor": "rgba(41,128,185,0.8)"}]
        },
        options: opts
    });
}

function renderAjaxPagination($container, contentpage) {
    var $content = $(document.createElement("div")).css({"display": "block", "position": "relative"});
    var $pagination = $(document.createElement("div")).css({"display": "block", "text-align": "center"});

    $container.append($content).append($pagination);

    var page = 1;
    var totalpages = 1;

    function loadContentData() {
        var $overlay = $(document.createElement("div")).css({
            "position": "absolute", "background-color": "rgba(255,255,255,0.8)",
            "top": 0, "left": 0, "right": 0, "bottom": 0
        });
        $content.append($overlay);

        $pagination.find("li.pagination-active").addClass("disabled");

        $.ajax({
            "url": contentpage, "dataType": "json",
            "data": {
                "page": page
            },
            "complete": function () {
                $pagination.find("li.pagination-active").removeClass("disabled");
                $overlay.fadeOut();
            },
            "success": updateContent
        });
    }

    function setPage(p) {
        if (isNaN(parseInt(p))) {
            return;
        }

        page = p;
        loadContentData();
    }

    function updateContent(data) {
        if (!data["success"]) {
            return;
        }

        totalpages = data["total"];
        page = parseInt(data["current"]);
        $content.html(data["content"]);

        $content.find('[data-toggle="tooltip"]').tooltip();

        updatePagination()
    }

    function updatePagination() {
        var $ul = $(document.createElement("ul")).addClass("pagination");
        var spacer = false;

        var $li, $a;

        $a = $(document.createElement("a")).attr("href", "javascript:").html("&laquo;");
        $li = $(document.createElement("li")).html($a).addClass("disabled");
        $ul.append($li);

        if (page > 1) {
            $li.removeClass("disabled").click(function () {
                setPage(parseInt(page) - 1)
            });
        }

        for (var i = 1; i <= totalpages; i++) {
            if (i < 3 || (i > page - 3 && i < page + 3) || i > totalpages - 2) {
                spacer = false;

                $a = $(document.createElement("a")).html(i).attr("href", "javascript:").attr("data-page", i);
                $li = $(document.createElement("li")).addClass("pageination-active").html($a);

                if (page == i) {
                    $li.addClass("active");
                }

                $ul.append($li);

                $a.click(function () {
                    setPage(parseInt($(this).attr("data-page")));
                })
            }
            else if (spacer === false) {
                spacer = true;

                $a = $(document.createElement("a")).html("...");
                $li = $(document.createElement("li")).addClass("disabled").html($a);

                $ul.append($li);
            }
        }

        $a = $(document.createElement("a")).attr("href", "javascript:").html("&raquo;");
        $li = $(document.createElement("li")).html($a).addClass("disabled");
        $ul.append($li);

        if (page < totalpages) {
            $li.removeClass("disabled").click(function () {
                setPage(parseInt(page) + 1)
            });
        }

        $pagination.html($ul);
    }

    loadContentData();
}