<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("admin","user","device")} #{$browserID} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/user">{language("admin", "user", "breadcrumb_users")}</a></li>
    <li class="breadcrumb-item active">{language("admin","user","device")} #{$browserID}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-heading">{language("admin","user","info")}</div>
    <div class="panel-body">
        <table class="table table-bordered table-marginless">
            <tr>
                <td>{language("admin","user","browser")}</td>
                <td>{$info.browser.name} ({$info.browser.version})</td>
            </tr>
            <tr>
                <td>{language("admin","user","platform")}</td>
                <td>{$info.browser.platform}</td>
            </tr>
            <tr>
                <td>{language("admin","user","ip")}</td>
                <td>{$info.ip}</td>
            </tr>
            <tr>
                <td>{language("admin","user","useragent")}</td>
                <td>{htmlspecialchars($info.useragent)}</td>
            </tr>
            <tr>
                <td>{language("admin","user","firstvisit")}</td>
                <td>{datetimeformat($info.dateline)}</td>
            </tr>
            <tr>
                <td>{language("admin","user","lastaction")}</td>
                <td>{datetimeformat($info.lastaction)}</td>
            </tr>
        </table>
    </div>
</div>

{if count($sessions) > 0}
    <div class="panel panel-default">
        <div class="panel-heading">{language("admin","user","header_session")}</div>
        <div class="panel-body">
            <div class="table-responsive">
                <form method="post" action="/admin/user/deletesession">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{language("admin", "user", "sessionid")}</th>
                            <th>{language("admin", "user", "username")}</th>
                            <th>{language("admin", "user", "created")}</th>
                            <th>{language("admin", "user", "expire")}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $sessions as $session}
                            <tr>
                                <td>#{$session.sessionID}</td>
                                <td><a href="/admin/user/view/{$session.userID}">{$session.username}</a></td>
                                <td>{datetimeformat($session.created)}</td>
                                <td>{datetimeformat($session.expire)}</td>
                                {if $session.expire > time()}
                                    <td>
                                        <button value="{$session.sessionID}" name="sessionID" class="btn btn-primary">{language("admin", "user", "delete_session")}</button>
                                    </td>
                                {/if}
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>

                    <input type="hidden" name="token" value="{token()}">
                    <input type="hidden" name="browserID" value="{$browserID}">
                </form>
            </div>
        </div>
    </div>
{/if}

<div class="panel panel-default">
    <div class="panel-heading">{language("admin","user","header_activity")}</div>
    <div class="panel-body">
        {statistics(string("/admin/user/activity/browser/", $browserID))}
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>