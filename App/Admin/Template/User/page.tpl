<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("typeID", "controller", "admin_user")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default">
    <div class="panel-heading">{language("admin", "user", "panel_header")}</div>
    <div class="panel-body">
        <form action="/admin/user/page/1" method="post" role="search">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="{language("search")}" value="{htmlspecialchars($match)}" name="search" id="srch-term">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
            <input type="hidden" name="token" value="{token()}">
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>{language("admin", "user", "userid")}</th>
                    <th>{language("admin", "user", "list_group")}</th>
                    <th>{language("admin", "user", "list_username")}</th>
                    <th>{language("admin", "user", "list_registered")}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {foreach $userlist.results as $user}
                    <tr>
                        <td>#{$user.id}</td>
                        <td>
                            <div style="color: {$user.color}">{$user.groupname}</div>
                        </td>
                        <td>{$user.username}</td>
                        <td>{datetimeformat($user.created)}</td>
                        <td>
                            <a href="/admin/user/view/{$user.id}" class="btn btn-default">{language("admin", "user", "list_details")}</a>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
        <div class="center-block">{pagination($userlist.pages, $userlist.current, $pageurl)}</div>
    </div>
</div>
{includetemplate="footer.tpl"}
</body>
</html>