<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <script type="text/javascript">
        var g_userID = "{$user.id}";
    </script>

    {noparse}
        <script type="text/javascript">
            $(document).ready(function () {
                var $button = $("#authenticator-button");
                var $result = $("#authenticator-result");

                $button.click(function () {
                    $.ajax({
                        url: "/admin/user/generatesecret",
                        dataType: "json",

                        method: "post",
                        data: {
                            "userID": g_userID, "token": g_sessionid
                        },

                        success: function (data) {
                            if(!data["success"])
                            {
                                return;
                            }

                            var $img = $(document.createElement("img")).attr("src", data["qr"]);

                            $result.hide().html($img);
                            $result.append("<br>");
                            $result.append(data["secret"]);
                            $result.append("<br><br>");

                            $result.slideDown();
                        },

                        complete: function () {
                            $button.html(buttontext);
                        }
                    });

                    var buttontext = $button.html();

                    $button.css("width", $button.innerWidth()).html('<i class="fa fa-spin fa-circle-o-notch"></i>');
                    $button.addClass("disabled")
                });
            });
        </script>
    {/noparse}

    <title>{$user.username} - {language("typeID", "controller", "admin_user")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/user">{language("admin", "user", "breadcrumb_users")}</a></li>
    <li class="breadcrumb-item active">{$user.username}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            {if permission("ban", "controller", "admin_ban") == "1"}
                <div class="col-xs-6 center-block">
                    {if $isBanned}
                        <a href="/admin/ban/view/{$user.id}" class="btn btn-default">{language("admin", "user", "bandetails")}</a>
                    {else}
                        <a href="/admin/ban/user/{$user.id}" class="btn btn-default">{language("admin", "user", "banuser")}</a>
                    {/if}
                </div>
            {/if}
            {if permission("permission", "controller", "admin_user") == "1"}
                <div class="col-xs-6 center-block">
                    <a href="/admin/permission/select/user/{$user.id}" class="btn btn-default">{language("admin", "user", "permission")}</a>
                </div>
            {/if}
        </div>
    </div>
</div>

{if permission("group", "controller", "admin_user") == "1"}
    <div class="panel panel-default">
        <div class="panel-heading">{language("admin","user","header_group")}</div>
        <div class="panel-body">
            <form method="post" action="/admin/user/setgroup">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="maingroup">{language("admin", "user", "maingroup")}</label>
                            <select id="maingroup" class="form-control" name="maingroup">
                                {foreach $groups as $group}
                                    {if $group.id == $maingroup}
                                        <option value="{$group.id}" selected>{$group.name}</option>
                                    {else}
                                        <option value="{$group.id}">{$group.name}</option>
                                    {/if}
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label for="subgroup">{language("admin", "user", "subgroup")}</label>
                            <select id="subgroup" class="form-control" name="subgroup[]" multiple>
                                {foreach $groups as $group}
                                    {if array_search($group.id,$subgroups)}
                                        <option value="{$group.id}" selected>{$group.name}</option>
                                    {else}
                                        <option value="{$group.id}">{$group.name}</option>
                                    {/if}
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary" value='{language("submit")}'>

                <input type="hidden" name="token" value="{token()}" />
                <input type="hidden" name="userID" value="{$user.id}" />
            </form>
        </div>
    </div>
{/if}

{if permission("password", "controller", "admin_user") == "1"}
    <div class="panel panel-default">
        <div class="panel-heading">{language("admin","user","header_password")}</div>
        <div class="panel-body">
            <form method="post" action="/admin/user/changepassword">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3"><label for="password">{language("admin", "user", "password")}</label>
                        </div>
                        <div class="col-md-9">
                            <input class="form-control" id="password" type="password" name="password"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="password_repeat">{language("admin", "user", "password_repeat")}</label>
                        </div>
                        <div class="col-md-9">
                            <input class="form-control" id="password_repeat" type="password" name="password_repeat">
                        </div>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary" value='{language("submit")}'>
                <input type="hidden" name="token" value="{token()}" />
                <input type="hidden" name="userID" value="{$user.id}" />
            </form>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">{language("admin","user","header_twofactor")}</div>
        <div class="panel-body">
            <div class="center-block">
                <div id="authenticator-result"></div>
                <a href="javascript:" class="btn btn-primary" id="authenticator-button">{language("admin","user","resetsecret")}</a>
            </div>
        </div>
    </div>
{/if}

{if permission("session", "controller", "admin_user") == "1"}
    <div class="panel panel-default">
        <div class="panel-heading">{language("admin","user","header_activity")}</div>
        <div class="panel-body">
            {statistics(string("/admin/user/activity/user/", $user.id))}
        </div>
    </div>
    {if count($sessions) > 0}
        <div class="panel panel-default">
            <div class="panel-heading">{language("admin","user","header_session")}</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <form method="post" action="/admin/user/deletesession">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>{language("admin", "user", "sessionid")}</th>
                                <th>{language("admin", "user", "deviceid")}</th>
                                <th>{language("admin", "user", "ip")}</th>
                                <th>{language("admin", "user", "browser")}</th>
                                <th>{language("admin", "user", "created")}</th>
                                <th>{language("admin", "user", "expire")}</th>
                                <th>{language("admin", "user", "lastaction")}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach $sessions as $session}
                                <tr>
                                    <td>#{$session.id}</td>
                                    <td>#{$session.browserID}</td>
                                    <td>{$session.ip}</td>
                                    <td>{$session.browser}</td>
                                    <td>{datetimeformat($session.created)}</td>
                                    <td>{datetimeformat($session.expire)}</td>
                                    <td>{datetimeformat($session.lastaction)}</td>
                                    <td>
                                        <button value="{$session.id}" name="sessionID" class="btn btn-primary">{language("admin", "user", "delete_session")}</button>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>

                        <input type="hidden" name="token" value="{token()}">
                        <input type="hidden" name="userID" value="{$user.id}">
                    </form>
                </div>
            </div>
        </div>
    {/if}

    {if count($browsers) > 0}
        <div class="panel panel-default">
            <div class="panel-heading">{language("admin","user","header_device")}</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{language("admin", "user", "deviceid")}</th>
                            <th>{language("admin", "user", "ip")}</th>
                            <th>{language("admin", "user", "browser")}</th>
                            <th>{language("admin", "user", "firstvisit")}</th>
                            <th>{language("admin", "user", "lastaction")}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $browsers as $browser}
                            <tr>
                                <td>#{$browser.id}</td>
                                <td>{$browser.ip}</td>
                                <td>{$browser.browser}</td>
                                <td>{datetimeformat($browser.dateline)}</td>
                                <td>{datetimeformat($browser.lastaction)}</td>
                                <td>
                                    <a href="/admin/user/browser/{$browser.id}" class="btn btn-default">{language("admin", "user", "list_details")}</a>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    {/if}
{/if}

{if permission("email", "controller", "admin_user") == "1" && $user.email != ""}
    <div class="panel panel-default">
        <div class="panel-heading">{language("admin","user","header_email")}</div>
        <div class="panel-body">
            <form method="post" action="/admin/user/sendemail">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="email-header">{language("admin", "user", "email_subject")}</label></div>
                        <div class="col-md-9">
                            <input id="email-header" class="form-control" type="text" name="email-header"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="email-text">{language("admin", "user", "email_text")}</label></div>
                        <div class="col-sm-9">
                            <textarea name="email-text" rows="7" class="form-control" id="email-text"></textarea>
                        </div>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary" value='{language("submit")}'>
                <input type="hidden" name="token" value="{token()}" />
                <input type="hidden" name="userID" value="{$user.id}" />
            </form>
        </div>
    </div>
{/if}

{sethook("pageUserProfile", $user)}

{includetemplate="footer.tpl"}
</body>
</html>