<!DOCTYPE>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("admin","main","login")}</title>
</head>
<body>
<div class="container">
    <div style="max-width: 450px; margin: 20px auto;">
        {foreach $message->getError() as $msg}
            <div class="alert alert-danger">{$msg}</div>
        {/foreach}
        {foreach $message->getInfo() as $msg}
            <div class="alert alert-success">{$msg}</div>
        {/foreach}
        <div class="panel panel-default">
            <div class="panel-heading">{language("admin","main","admin_panel")}</div>
            <div class="panel-body">
                <form method="post" action="/admin/{if $redirect}?token={token()}&redirect={rawurlencode($redirect)}{/if}">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
                        <input type="password" class="form-control" placeholder="{language("admin","main","password")}" name="password">
                    </div>
                    <br>
                    {if $twofactor}
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-tablet"></i></span>
                            <input type="number" class="form-control" placeholder="{language("admin","main","twofactor")}" name="code">
                        </div>
                        <br>
                    {/if}
                    <button class="btn btn-primary btn-block btn-signin" type="submit">{language("admin", "main", "login")}</button>
                    <input type="hidden" name="token" value="{token()}">
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>