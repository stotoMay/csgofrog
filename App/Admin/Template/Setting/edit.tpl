<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("typeID", "controller", "admin_setting")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/setting">{language("admin", "setting", "breadcrumb_settings")}</a></li>
    <li class="breadcrumb-item"><a href="/admin/setting">{language("type", $type)}</a></li>
    <li class="breadcrumb-item active">{$heading}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-heading">{$heading}</div>
    <div class="panel-body">
        <div class="table-responsive">
            <form method="post" action="/admin/setting/edit/{$type}/{$typeID}">
                <table class="table table-borderless">
                    {foreach $settings as $setting}
                        <tr>
                            <td>{$setting.description}</td>
                            <td>{input($setting.input, $setting.name, $setting.value)}</td>
                        </tr>
                    {/foreach}
                </table>
                <input type="hidden" name="token" value="{token()}" />
                <button type="submit" class="btn btn-primary">{language("submit")}</button>
            </form>
        </div>
    </div>
</div>
{includetemplate="footer.tpl"}
</body>
</html>