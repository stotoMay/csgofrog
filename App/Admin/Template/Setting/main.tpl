<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("typeID", "controller", "admin_setting")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

{foreach $types as $type}
    <div class="panel panel-default">
        <div class="panel-heading">{$type.translation}</div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-borderless table-marginless table-smallpadding">
                    {foreach $type.children as $id}
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;<a href="/admin/setting/edit/{$type.name}/{$id.name}">{$id.translation}</a>
                            </td>
                            <td style="text-align: right;">
                                ({$id.count} {language("admin", "setting", "settingcount")})
                            </td>
                        </tr>
                    {/foreach}
                </table>
            </div>
        </div>
    </div>
{/foreach}

{includetemplate="footer.tpl"}
</body>
</html>