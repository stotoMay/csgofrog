<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("admin", "ban", "title_details")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/ban">{language("admin", "ban", "breadcrumb_bans")}</a></li>
    <li class="breadcrumb-item"><a href="/admin/user/view/{$ban.userID}">{$ban.username}</a></li>
    <li class="breadcrumb-item active">{language("admin", "ban", "breadcrumb_details")}</li>
</ol>


<div class="panel panel-default">
    <div class="panel-heading">{language("admin", "ban", "details")}</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <td>{language("admin", "ban", "details", "username")}</td>
                    <td><a href="/admin/user/view/{$ban.userID}">{$ban.username}</a></td>
                </tr>
                <tr>
                    <td>{language("admin", "ban", "details", "created")}</td>
                    <td>{datetimeformat($ban.created)}</td>
                </tr>
                <tr>
                    <td>{language("admin", "ban", "details", "banner")}</td>
                    <td><a href="/admin/user/view/{$ban.banner.id}">{$ban.banner.username}</a></td>
                </tr>
                <tr>
                    <td>{language("admin", "ban", "details", "expire")}</td>
                    <td>
                        {if $ban.expire}
                            {datetimeformat($ban.expire)}
                        {else}
                            {language("admin", "ban", "expire_never")}
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td>{language("admin", "ban", "details", "reason")}</td>
                    <td>{$ban.reason}</td>
                </tr>
            </table>
            <form method="post" action="/admin/ban/delete">
                <button name="banID" value="{$ban.id}" class="btn btn-primary">{language("admin", "ban", "delete")}</button>
                <input type="hidden" name="token" value="{token()}">
            </form>
        </div>
    </div>
</div>
{includetemplate="footer.tpl"}
</body>
</html>