<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("admin", "ban", "title_ip")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/ban/iplist">{language("admin", "ban", "breadcrumb_ips")}</a></li>
    <li class="breadcrumb-item active">{language("admin", "ban", "breadcrumb_ban")}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-heading">{language("admin", "ban", "title_ip")}</div>
    <div class="panel-body">
        <div class="table-responsive">
            <form method="post" action="/admin/ban/ip">
                <table class="table table-borderless">
                    <tr>
                        <td>
                            <label for="ip">{language("admin","ban","details","ip")}</label>
                        </td>
                        <td colspan="2">
                            <input id="ip" type="text" name="ip" class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="time">{language("admin","ban","details","expire")}</label>
                        </td>
                        <td>
                            <input id="time" type="number" name="time" class="form-control">
                        </td>
                        <td>
                            <select id="factor" name="factor" class="form-control">
                                <option value="0" selected>{language("admin","ban","time","permanent")}</option>
                                <option value="1">{language("admin","ban","time","seconds")}</option>
                                <option value="60">{language("admin","ban","time","minutes")}</option>
                                <option value="3600">{language("admin","ban","time","hours")}</option>
                                <option value="86400">{language("admin","ban","time","days")}</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <input type="submit" class="btn btn-primary" value='{language("submit")}'>
                <input type="hidden" name="token" value="{token()}">
            </form>
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>