<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("admin", "ban", "title_main")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}
<div class="panel panel-default panel-tabs">
    <div class="panel-heading">
        <ul class="nav nav-tabs">
            <li><a href="/admin/ban/">{language("admin", "ban", "ban_user")}</a></li>
            <li class="active"><a href="/admin/ban/iplist">{language("admin", "ban", "ban_ip")}</a></li>
        </ul>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <a href="/admin/ban/ip" class="btn btn-default btn-block">{language("admin", "ban", "action_ban_ip")}</a>

            <hr>

            <form action="/admin/ban" method="post" role="search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="{language("search")}" value="{htmlspecialchars($search)}" name="search" id="srch-term">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
                <input type="hidden" name="token" value="{token()}">
            </form>

            <hr>

            <form method="post" action="/admin/ban/delete">
                <table class="table">
                    <thead>
                    <tr>
                        <th>{language("admin", "ban", "banid")}</th>
                        <th>{language("admin", "ban", "ip")}</th>
                        <th>{language("admin", "ban", "created")}</th>
                        <th>{language("admin", "ban", "expire")}</th>
                        <th>{language("admin", "ban", "banner")}</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    {foreach $banlist.results as $ban}
                        <tr>
                            <td>#{$ban.id}</td>
                            <td>{$ban.ip}</td>
                            <td>{datetimeformat($ban.created)}</td>
                            <td>
                                {if $ban.expire}
                                    {datetimeformat($ban.expire)}
                                {else}
                                    {language("admin", "ban", "expire_never")}
                                {/if}
                            </td>
                            <td><a href="/admin/user/view/{$ban.banner.id}">{$ban.banner.username}</a></td>
                            <td>
                                <button href="/admin/ban/delete/{$ban.id}" class="btn btn-primary" name="banID" value="{$ban.id}">{language("admin", "ban", "delete")}</button>
                            </td>
                        </tr>
                    {/foreach}
                </table>
                <input type="hidden" name="mode" value="ip">
                <input type="hidden" name="token" value="{token()}">
            </form>
            <div class="center-block">{pagination($banlist.pages, $banlist.current, $pageurl)}</div>
        </div>
    </div>
</div>
{includetemplate="footer.tpl"}
</body>
</html>