<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("admin", "ban", "title_user")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/user">{language("admin", "ban", "breadcrumb_users")}</a></li>
    <li class="breadcrumb-item active"><a href="/admin/user/view/{$user.id}">{$user.username}</a></li>
    <li class="breadcrumb-item active">{language("admin", "ban", "breadcrumb_ban")}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-heading">{$user.username}</div>
    <div class="panel-body">
        <div class="table-responsive">
            <form method="post" action="/admin/ban/user/{$user.id}">
                <table class="table table-borderless">
                    <tr>
                        <td>
                            <label for="time">{language("admin","ban","details","expire")}</label><label for="factor"></label>
                        </td>
                        <td>
                            <input id="time" type="number" name="time" class="form-control">
                        </td>
                        <td>
                            <select id="factor" name="factor" class="form-control">
                                <option value="0" selected>{language("admin","ban","time","permanent")}</option>
                                <option value="1">{language("admin","ban","time","seconds")}</option>
                                <option value="60">{language("admin","ban","time","minutes")}</option>
                                <option value="3600">{language("admin","ban","time","hours")}</option>
                                <option value="86400">{language("admin","ban","time","days")}</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="reason">{language("admin","ban","details","reason")}</label></td>
                        <td colspan="2"><textarea name="reason" id="reason" class="form-control"></textarea></td>
                    </tr>
                </table>
                <input type="submit" class="btn btn-primary" value='{language("submit")}'>
                <input type="hidden" name="token" value="{token()}">
            </form>
        </div>
    </div>
</div>

{includetemplate="footer.tpl"}
</body>
</html>