<ul class="pagination">
    {if count($buttons) > "0"}
        <li class="{if $current == 1}disabled{/if}">
            {if $current == 1}
                <a href="javascript:">&laquo;</a>
            {else}
                <a href="{$url_last}">&laquo;</a>
            {/if}
        </li>
        {foreach $buttons as $button}
            {if $button.type == "spacer"}
                <li class="disabled"><a href="#">...</a></li>
            {else}
                <li class="{if $button.active}active{/if}">
                    <a href="{$button.url}">{$button.name}</a>
                </li>
            {/if}
        {/foreach}
        <li class="{if $current == $pages}disabled{/if}">
            {if $current == $pages}
                <a href="javascript:">&raquo;</a>
            {else}
                <a href="{$url_next}">&raquo;</a>
            {/if}
        </li>
    {/if}
</ul>