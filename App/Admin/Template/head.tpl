<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<script type="text/javascript">
    var g_sessionid = "{token()}";
    var timefix = Math.round(Date.now() / 1000) - {time()};
    var g_timezoneCookieName = "{$cookiename.timezone}";
</script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/App/Admin/Template/Static/CSS/custom.css?t=1" />

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/App/Admin/Template/Static/JScript/custom.js" type="text/javascript"></script>

<script>
    $(document).ready(function () {
        $(".admin-statistics").each(function () {
            var $this = $(this);

            renderStatsGraph($this, $this.attr("data-statspage"))
        });

        $(".admin-ajax-pagination").each(function () {
            var $this = $(this);

            renderAjaxPagination($this, $this.attr("data-contentpage"))
        });

        setTimeOffset();
    });
</script>
