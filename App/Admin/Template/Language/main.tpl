<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("admin", "language", "title_main")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}
<div class="panel panel-default">
    <div class="panel-heading">{language("admin", "language", "languages")}</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-borderless table-marginless">
                {foreach $languages as $language}
                    <tr>
                        <td>{$language}</td>
                        {if permission("change", "controller", "admin_lang") == "1" && $language != $selected}
                            <td style="text-align:right;">
                                <a href="/admin/language/select/{$language}" class="btn btn-primary">{language("admin","language","select")}</a>
                            </td>
                        {else}
                            <td style="text-align:right;">
                                <a href="/admin/language/select/{$language}" class="btn btn-primary disabled">{language("admin","language","select")}</a>
                            </td>
                        {/if}
                    </tr>
                {/foreach}
            </table>
        </div>
    </div>
</div>
{includetemplate="footer.tpl"}
</body>
</html>