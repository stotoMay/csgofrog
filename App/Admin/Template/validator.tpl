{$errormessage}

{if count($errorlist) > 0}
    <ul>
        {foreach $errorlist as $error}
            <li>{$error}</li>
        {/foreach}
    </ul>
{/if}