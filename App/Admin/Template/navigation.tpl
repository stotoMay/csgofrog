<nav class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>

    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
            {foreach $navigation as $n}
                {if $n.selected}
                    <li class="active">
                        <a href="{$n.path}"><i class="fa fa-fw fa-{$n.icon}"></i> {$n.name}</a>
                    </li>
                {else}
                    <li>
                        <a href="{$n.path}"><i class="fa fa-fw fa-{$n.icon}"></i> {$n.name}</a>
                    </li>
                {/if}
            {/foreach}
            <li>
                <hr>
            </li>
            <li>
                <a href="/admin/logout"><i class="fa fa-fw fa-power-off"></i> Logout ({$browser->user()->getUserName()})</a>
            </li>
            <li><br></li>
        </ul>
    </div>
</nav>
<div class="content-wrapper">
    <div class="content-container container-fluid">
        <div id="messages">
            {foreach $message->getError() as $m}
                <div class="alert alert-danger">{$m}</div>
            {/foreach}
            {foreach $message->getInfo() as $m}
                <div class="alert alert-success">{$m}</div>
            {/foreach}
        </div>