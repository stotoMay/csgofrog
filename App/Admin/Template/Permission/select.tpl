<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("typeID", "controller", "admin_permission")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    {if $idtype == "user"}
        <li class="breadcrumb-item"><a href="/admin/user">{language("admin", "permission", "breadcrumb_users")}</a></li>
        <li class="breadcrumb-item"><a href="/admin/user/view/{$id}">{$username}</a></li>
    {else}
        <li class="breadcrumb-item"><a href="/admin/group">{language("admin", "permission", "breadcrumb_groups")}</a></li>
        <li class="breadcrumb-item"><a href="/admin/group">{$groupname}</a></li>
    {/if}
    <li class="breadcrumb-item active">{language("admin", "permission", "breadcrumb_permissions")}</li>
</ol>

{foreach $types as $type}
    <div class="panel panel-default">
        <div class="panel-heading">{$type.translation}</div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-marginless table-borderless table-smallpadding">
                    {foreach $type.children as $child}
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;<a href="/admin/permission/edit/{$idtype}/{$id}/{$type.name}/{$child.name}">{$child.translation}</a>
                            </td>
                            <td style="text-align: right;">({$child.count} {language("admin", "permission", "permissioncount")})
                            </td>
                        </tr>
                    {/foreach}
                </table>
            </div>
        </div>
    </div>
{/foreach}
{includetemplate="footer.tpl"}
</body>
</html>