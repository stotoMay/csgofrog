<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("typeID", "controller", "admin_permission")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<ol class="breadcrumb">
    {if $idtype == "user"}
        <li class="breadcrumb-item"><a href="/admin/user">{language("admin", "permission", "breadcrumb_users")}</a></li>
        <li class="breadcrumb-item"><a href="/admin/user/view/{$id}">{$username}</a></li>
    {else}
        <li class="breadcrumb-item"><a href="/admin/group">{language("admin", "permission", "breadcrumb_groups")}</a></li>
        <li class="breadcrumb-item"><a href="/admin/group">{$groupname}</a></li>
    {/if}
    <li class="breadcrumb-item"><a href="/admin/permission/select/{$idtype}/{$id}">{language("admin", "permission", "breadcrumb_permissions")}</a></li>
    <li class="breadcrumb-item"><a href="/admin/permission/select/{$idtype}/{$id}">{language("type", $type)}</a></li>
    <li class="breadcrumb-item active">{language("typeID", $type, $typeID)}</li>
</ol>

<div class="panel panel-default">
    <div class="panel-heading">{language("typeID", $type, $typeID)}</div>
    <div class="panel-body">
        <div class="table-responsive">
            <form method="post" action="/admin/permission/edit/{$idtype}/{$id}/{$type}/{$typeID}">
                <table class="table table-borderless">
                    {foreach $permissions as $permission}
                        <tr>
                            <td>{$permission.description}</td>
                            {if $permission.input == "bool"}
                                <td>
                                    <select class="form-control" name="{$permission.name}" onchange="$('#{$permission.name}-default').val($(this).val() == 'default' ? '1' : '0')">
                                        <option value="1" {if $permission.value == 1}selected{/if}>{language("input", "bool", "true")}</option>
                                        <option value="0" {if $permission.value == 0}selected{/if}>{language("input", "bool", "false")}</option>
                                        <option value="default" {if $permission.default == 1}selected{/if}>{language("admin","permission","default")}</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="hidden" id="{$permission.name}-default" name="{$permission.name}-default" value="{if $permission.default == 1}1{else}0{/if}">
                                </td>
                            {else}
                                <td>{input($permission.input,$permission.name,$permission.value)}</td>
                                <td>
                                    <input type="checkbox" id="{$permission.name}-default" name="{$permission.name}-default" value="1" {if $permission.default == 1}checked{/if}>
                                    {language("admin","permission","default")}
                                </td>
                            {/if}
                        </tr>
                    {/foreach}
                </table>
                <input type="hidden" name="token" value="{token()}" />
                <input type="submit" class="btn btn-primary" value='{language("submit")}' />
            </form>
        </div>
    </div>
</div>
{includetemplate="footer.tpl"}
</body>
</html>