<!DOCTYPE html>
<html>
<head>
    {includetemplate="head.tpl"}

    <title>{language("admin", "dashboard", "title_main")} {language("admin", "admin_title")}</title>
</head>
<body>
{includetemplate="navigation.tpl"}

<div class="panel panel-default">
    <div class="panel-heading">{language("admin", "dashboard", "header_newusers")}</div>
    <div class="panel-body">
        {statistics("/admin/dashboard/newusers")}
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">{language("admin", "dashboard", "header_activity")}</div>
    <div class="panel-body">
        {statistics("/admin/dashboard/activity")}
    </div>
</div>


{includetemplate="footer.tpl"}
</body>
</html>