<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Admin;

use App\Core\App;
use App\HTTP\Browser;
use App\HTTP\Request;
use App\Theme\Theme;

class Admin
{
    private $request;
    private $browser;
    private $app;
    private $theme;

    private $inputs;

    /**
     * @param Request $request
     * @param Browser $browser
     * @param App $app
     * @param Theme $theme
     *
     * @throws \App\Core\Exception\FileNotFoundException
     */
    public function __construct(Request $request, Browser $browser, App $app, Theme $theme)
    {
        $this->request = $request;
        $this->app = $app;
        $this->theme = $theme;
        $this->browser = $browser;

        $this->theme->assignFunction("input", [$this, "renderInput"]);
        $this->theme->assignFunction("pagination", [$this, "renderPagination"]);
        $this->theme->assignFunction("pagination_ajax", [$this, "renderAjaxPagination"]);
        $this->theme->assignFunction("statistics", [$this, "renderStats"]);
        $this->inputs = $this->parseInputXML($this->theme->getFile("input.xml"));
    }

    /**
     * @param string $selected
     *
     * @return array
     */
    public function getNavigation($selected)
    {
        if(!$this->browser->isLogin())
        {
            return [];
        }

        $namespace = ["App", "Admin", "Controller"];
        $list = $this->app->router()->getControllerList();

        $nav = [];

        foreach($list as $id => $class)
        {
            $class = array_clear(explode('\\', $class));
            $name = array_last($class);

            if(!array_compare($class, array_merge($namespace, [$name])))
            {
                continue;
            }

            $icon = "square";
            $priority = 0;
            if(method_exists(implode('\\', $class), "__info__") && is_callable([implode('\\', $class), "__info__"]))
            {
                $info = call_user_func([implode('\\', $class), "__info__"]);

                if($info["visible"] === false)
                {
                    continue;
                }

                if(is_array($info["requirement"]) && !$this->browser->user()->permission()->hasRequirements($info["requirement"]))
                {
                    continue;
                }

                if(!empty($info["icon"]))
                {
                    $icon = $info["icon"];
                }

                if(!empty($info["priority"]))
                {
                    $priority = $info["priority"];
                }
            }

            $nav[] = [
                "name" => $this->app->language()->get(["typeID", "controller", $id]),
                "path" => "/admin/".strtolower($name), "selected" => $selected == implode('\\', $class),
                "icon" => $icon, "priority" => $priority
            ];
        }

        usort($nav, function($a, $b)
        {
            return $b["priority"] - $a["priority"];
        });

        return $nav;
    }

    /**
     * @param array $args
     * @param string $inputname
     * @param string $name
     * @param string $value
     *
     * @return string
     *
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\Theme\Exception\ParseException
     * @throws \App\Theme\Exception\TemplateNotFoundException
     */
    public function renderInput($args, $inputname, $name, $value)
    {
        if(isset($this->inputs[strtolower($inputname)]))
        {
            $template = $this->theme->parse($this->inputs[strtolower($inputname)]);
        }
        else
        {
            $template = $this->theme->parse($this->inputs["regex"]);

            $template->assignVar("regex", $inputname);
        }

        $template->assignVar("input", ["name" => $name, "value" => $value]);

        return $template->parse();
    }

    public function renderPagination($args, $pages, $current, $url)
    {
        $template = $this->theme->draw("pagination.tpl");

        $buttons = [];
        for($i = 1; $i <= $pages; $i++)
        {
            if($i < 3 || ($i > $current - 3 && $i < $current + 3) || $i > $pages - 2)
            {
                $buttons[] = ["name" => $i, "url" => str_replace("%p", $i, $url), "active" => $i == $current,
                    "type" => "button"];
            }
            elseif(array_last($buttons)["type"] != "spacer")
            {
                $buttons[] = ["name" => "...", "type" => "spacer"];
            }
        }

        $template->assignVar("pages", $pages)
            ->assignVar("current", $current)
            ->assignVar("url", $url)
            ->assignVar("buttons", $buttons)
            ->assignVar("url_last", str_replace("%p", $current - 1, $url))
            ->assignVar("url_next", str_replace("%p", $current + 1, $url));

        return $template->parse();
    }

    public function renderAjaxPagination($args, $contentpage)
    {
        $template = $this->theme->draw("pagination_ajax.tpl");

        $template->assignVar("contentpage", $contentpage);

        return $template->parse();
    }

    public function renderStats($args, $statspage)
    {
        $template = $this->theme->draw("stats.tpl");

        $template->assignVar("statspage", $statspage);

        return $template->parse();
    }

    /**
     * @param string $string
     *
     * @return array
     */
    public function parseInputXML($string)
    {
        $p = xml_parser_create();
        xml_parse_into_struct($p, $string, $vals, $index);
        xml_parser_free($p);

        $tmp = [];

        foreach($vals as $line)
        {
            if($line["type"] == "complete" && $line["level"] == 2 && is_string($line["value"]))
            {
                $tmp[strtolower($line["tag"])] = $line["value"];
            }
        }

        return $tmp;
    }
}