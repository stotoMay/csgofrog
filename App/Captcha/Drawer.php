<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Captcha;

class Drawer
{
    private $string;

    public function __construct($string)
    {
        $this->string = $string;
    }

    public function __tostring()
    {
        return $this->render();
    }

    public function render()
    {
        $letters = $this->string;

        $img = imagecreate(400, 100);

        imagecolorallocate($img, rand(250, 255), rand(250, 255), rand(250, 255));

        $linecolor = imagecolorallocate($img, rand(0, 255), rand(0, 255), rand(0, 255));
        $lines_top_bottom = rand(5, 15);
        $lines_left_right = rand(2, 5);
        $circle_count = rand(5, 20);
        $count = 0;
        while($circle_count > $count)
        {
            $circle_color = imagecolorallocate($img, rand(120, 255), rand(120, 255), rand(120, 255));
            $circle_height = rand(20, 40);

            imagefilledellipse($img, rand(0, 400), rand(0, 100), $circle_height, $circle_height, $circle_color);

            $count += 1;
        }

        $count = 0;

        while($lines_top_bottom > $count)
        {
            imageline($img, rand(0, 400), 0, rand(0, 400), 100, $linecolor);
            $count += 1;
        }

        $count = 0;

        while($lines_left_right > $count)
        {
            imageline($img, 0, rand(0, 100), 400, rand(0, 100), $linecolor);
            $count += 1;
        }

        $count = 0;

        while($count <= strlen($letters))
        {
            $codeletter = $letters[$count];
            $x = $count * 70 + 70;
            $lettercolor = imagecolorallocate($img, rand(0, 200), rand(0, 200), rand(0, 200));;

            imagettftext($img, rand(30, 80), rand(-30, 30), $x, 80, $lettercolor, __DIR__."/captcha.ttf", $codeletter);

            $count++;
        }

        ob_start();

        imagepng($img);
        $imagedata = ob_get_contents();

        ob_end_clean();
        imagedestroy($img);

        return $imagedata;
    }
}