<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Captcha;

use App\Core\App;
use App\HTTP\Browser;
use App\HTTP\Request;

class Validator
{
    private $token;

    private $app;
    private $request;
    private $browser;

    /**
     * @param string $token
     * @param App $app
     * @param Request $request
     * @param Browser $browser
     */
    public function __construct($token, App $app, Request $request, Browser $browser)
    {
        $this->token = $token;

        $this->app = $app;
        $this->request = $request;
        $this->browser = $browser;
    }

    /**
     * @return string
     * @throws \App\Database\Exception\ParseException
     */
    public function getString()
    {
        return $this->app->database()->select("captcha")->where([
            "hash" => sha1($this->token), "browserID" => $this->browser->getBrowserID(),
            "used" => 0, "expire[>=]" => $this->request->time()
        ])->fetchRow()["imagestring"];
    }

    /**
     * @param string $str
     *
     * @return bool
     * @throws \App\Database\Exception\ParseException
     */
    public function validate($str)
    {
        if(!empty($str) && strtolower($this->getString()) == strtolower($str))
        {
            $this->app->database()->update("captcha")->set(["used" => 1])->where([
                "hash" => sha1($this->token),
                "browserID" => $this->browser->getBrowserID(), "used" => 0,
                "expire[>=]" => $this->request->time()
            ]);

            return true;
        }
        else
        {
            return false;
        }
    }
}