<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Database\Parent;

abstract class Result
{
    abstract function fetchNextRow();

    abstract function numRows();

    abstract function getLastID();

    abstract function getAffectedRows();
}