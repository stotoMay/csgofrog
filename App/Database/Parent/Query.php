<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Database\Parent;

use App\Database\Database;
use App\Database\Exception\ParseException;
use App\Database\Query\Select;

abstract class Query
{
    protected $table;
    protected $database;

    protected $query;

    protected $executed;

    /**
     * @param string|array|Select $table
     * @param string $alias
     * @param Database $database
     *
     * @throws ParseException
     */
    public function __construct($table, $alias, Database $database)
    {
        if(!is_string($table) && !is_array($table) && (!is_object($table) || !get_class($table) == "App\\Database\\Query\\Select"))
        {
            throw new ParseException("invalid tablename type ".gettype($table).": expected string, array or object of type Select");
        }

        $this->table = $table;

        $this->database = $database;
        $this->query = [];

        if(isset($alias))
        {
            $this->query["alias"] = $alias;
        }

        $this->executed = false;
    }

    public function __destruct()
    {
        if(!$this->executed)
        {
            $this->execute();
        }
    }

    protected function destroy()
    {
        $this->executed = true;
    }

    abstract public function execute();

    /**
     * @param string $type
     * @param string $table
     * @param array $condition
     * @param string $alias
     *
     * @return $this
     *
     * @throws ParseException
     */
    protected function join($type, $table, $condition, $alias = NULL)
    {
        if(is_string($table) && is_array($condition) && count($condition) > 0)
        {
            $this->query["join"][] = [
                "condition" => $condition, "type" => $type, "table" => $this->clear($table), "alias" => $alias
            ];
        }
        else
        {
            if(!is_string($table))
            {
                throw new ParseException("join: invalid table name type ".gettype($table).": expected string");
            }
            elseif(!is_array($condition))
            {
                throw new ParseException("join: invalid condition type ".gettype($condition).": expected array");
            }
            else
            {
                throw new ParseException("join: empty join condition");
            }
        }

        return $this;
    }

    /**
     * @param array $arr
     * @param string $glue
     *
     * @return string
     *
     * @throws ParseException
     */
    protected function parseCondition(array $arr, $glue = "and")
    {
        $glues = ["and", "or"];
        $operators = [
            "=" => "=", "!" => "!=", ">" => ">", "<" => "<", ">=" => ">=", "<=" => "<=", "~~" => "LIKE",
            "!~~" => "NOT LIKE", "~" => "LIKE", "!~" => "NOT LIKE"
        ];

        if(!is_string($glue))
        {
            throw new ParseException("condition: invalid glue type ".gettype($glue).": expected string");
        }

        if(!is_int(array_search(strtolower($glue), $glues)))
        {
            throw new ParseException("condition: invalid glue '".$glue."'");
        }

        $condition = [];
        foreach($arr as $key => $value)
        {
            if(is_string($key) && is_int(array_search(strtolower($key), $glues)) && is_array($value))
            {
                $condition[] = "(".$this->parseCondition($value, $key).")";
            }
            elseif(is_int($key) && is_string($value))
            {
                $split = $this->parseColumnJoin($value);

                if(isset($operators[$split["operator"]]) && !empty($split["column1"]) && !empty($split["column2"]))
                {
                    $condition[] = $this->quoteColumn($split["column1"])." ".$operators[$split["operator"]]." ".$this->quoteColumn($split["column2"]);
                }
                else
                {
                    throw new ParseException("condition: failed to parse '".$value."'");
                }
            }
            elseif(is_int($key) && is_array($value))
            {
                $condition[] = "(".$this->parseCondition($value, "and").")";
            }
            else
            {
                if(!is_string($key))
                {
                    throw new ParseException("condition: invalid column line type ".gettype($key).": expected string");
                }

                $split = $this->parseColumnLine($key);
                $column = $split["column"];
                $operator = $split["operator"];

                if(empty($column))
                {
                    throw new ParseException("condition: empty column name");
                }

                if(!isset($operators[$operator]))
                {
                    throw new ParseException("condition: invalid operator '".$operator."''");
                }

                if(is_array($value))
                {
                    $tmp = [];
                    foreach($value as $cv)
                    {
                        if(!is_string($cv) && !is_numeric($cv) && $cv !== NULL)
                        {
                            throw new ParseException("condition: invalid value type ".gettype($cv)." in 'IN CLAUSE': expected string or number or NULL");
                        }

                        $tmp[] = $this->database->quote($cv, $operator);
                    }

                    $condition[] = " ".$this->quoteColumn($column)." ".($operator == "!" ? "NOT IN" : "IN")." (".implode(", ", $tmp).")";
                }
                else
                {
                    if($value === NULL)
                    {
                        $condition[] = $this->quoteColumn($column)." ".(($operator == "!") ? "IS NOT NULL" : "IS NULL");
                    }
                    elseif(is_string($value) || is_numeric($value))
                    {
                        $condition[] = $this->quoteColumn($column)." ".$operators[$operator]." ".$this->database->quote($value, $operator);
                    }
                    else
                    {
                        throw new ParseException("condition: invalid value type ".gettype($value).": expected string or number or NULL");
                    }
                }
            }
        }

        if(count($condition) == 0)
        {
            throw new ParseException("condition: empty condition");
        }

        return implode(" ".strtoupper($glue)." ", $condition);
    }

    /**
     * @param $line
     *
     * @return array
     */
    public function parseColumnLine($line)
    {
        $column = "";
        $operator = "";
        $open = false;

        $letters = str_split($line);

        foreach($letters as $letter)
        {
            if($letter == "[")
            {
                $open = true;
            }
            elseif($letter == "]")
            {
                $open = false;
            }
            elseif($open)
            {
                $operator .= $letter;
            }
            else
            {
                $column .= $letter;
            }
        }

        if(empty($operator))
        {
            $operator = "=";
        }

        return ["column" => $column, "operator" => $operator];
    }

    /**
     * @param $line
     *
     * @return array
     */
    public function parseColumnJoin($line)
    {
        $split = ["", "", ""];
        $state = 0;

        $letters = str_split($line);

        foreach($letters as $letter)
        {
            if($letter == "[")
            {
                $state = 1;
                continue;
            }
            elseif($letter == "]")
            {
                $state = 2;
                continue;
            }

            $split[$state] .= $letter;
        }

        return ["column1" => $split[0], "column2" => $split[2], "operator" => $split[1]];
    }

    /**
     * @param string $str
     *
     * @return string
     */
    protected function quoteColumn($str)
    {
        if($str == "*")
        {
            return $str;
        }

        $split = explode(".", $str);

        if(count($split) == 2)
        {
            return $this->quoteTable($split[0]).".`".$split[1]."`";
        }
        else
        {
            return "`".$split[0]."`";
        }
    }

    /**
     * @param string $str
     *
     * @return string
     */
    protected function quoteTable($str)
    {
        if($str == "*")
        {
            return $str;
        }

        return "`".$this->database->getTablePrefix().$str."`";
    }

    /**
     * @param string $string
     *
     * @return string
     */
    protected function clear($string)
    {
        return $string;
    }
}
