<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Database\Parent;

use App\Database\Driver\MySQL\Result;

abstract class Connection
{
    public function __construct($database, $host, $port, $user, $password)
    {
        $this->connect($database, $host, $port, $user, $password);
    }

    public function __destruct()
    {
        $this->disconnect();
    }

    abstract public function connect($database, $host, $port, $user, $password);

    abstract public function disconnect();

    /**
     * @param $query
     *
     * @return Result
     */
    abstract public function query($query);

    abstract public function quote($string, $operator = NULL);

    abstract public function escape($string, $replace = true);
}