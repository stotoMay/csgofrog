<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Database\Driver\MySQL;

class Result extends \App\Database\Parent\Result

{
    private $connection;
    private $result;

    /**
     * Result constructor.
     *
     * @param \mysqli_result $result
     * @param \mysqli $connection
     */
    public function __construct($result, \mysqli $connection)
    {
        $this->connection = $connection;
        $this->result = $result;
    }

    public function __destruct()
    {
        if(!is_object($this->result))
        {
            return;
        }

        $this->result->free();
    }

    public function fetchNextRow()
    {
        return $this->result->fetch_assoc();
    }

    public function numRows()
    {
        return $this->result->num_rows;
    }

    public function getAffectedRows()
    {
        return $this->connection->affected_rows;
    }

    public function getLastID()
    {
        return $this->connection->insert_id;
    }
}