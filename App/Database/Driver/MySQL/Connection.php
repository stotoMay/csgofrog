<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Database\Driver\MySQL;

use App\Database\Exception\ConnectException;

use App\Database\Exception\QueryException;

use mysqli;

class Connection extends \App\Database\Parent\Connection
{
    /**
     * @var mysqli $conn
     */
    private $conn = NULL;

    private $log = [];

    /**
     * @param string $database
     * @param string $host
     * @param int $port
     * @param string $user
     * @param string $password
     *
     * @throws ConnectException
     */
    public function connect($database, $host, $port, $user, $password)
    {
        $this->conn = mysqli_init();

        $this->conn->options(MYSQLI_OPT_CONNECT_TIMEOUT, 5);
        $this->conn->real_connect($host, $user, $password, $database, $port);

        if($this->conn->connect_errno)
        {
            $this->conn = NULL;

            throw new ConnectException($this->conn->connect_error, $this->conn->connect_errno);
        }

        $this->conn->set_charset("utf8mb4");
    }

    public function disconnect()
    {
        if($this->conn)
        {
            $this->conn->close();
            $this->conn = NULL;
        }
    }

    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param string $query
     *
     * @return mixed
     *
     * @throws QueryException
     */
    public function query($query)
    {
        $result = $this->conn->query($query);

        if(!empty($this->conn->error))
        {
            throw new QueryException($this->conn->error, $this->conn->errno);
        }

        $this->log[] = $query;

        return new Result($result, $this->conn);
    }

    /**
     * @param string $string
     * @param string $operator
     *
     * @return string
     */
    public function quote($string, $operator = NULL)

    {
        if(is_int($string))
        {
            return $string;
        }
        elseif(!isset($string))
        {
            return "NULL";
        }
        else
        {
            if($operator == "~" || $operator == "!~")
            {
                $string = str_replace("%", '\\%', $string);
                $string = str_replace("_", '\\_', $string);

                return "'".$this->conn->real_escape_string($string)."'";
            }
            else
            {
                return "'".$this->conn->real_escape_string($string)."'";
            }
        }
    }

    /**
     * @param $string
     * @param bool $replace
     *
     * @return string
     */
    public function escape($string, $replace = true)
    {
        if($replace)
        {
            $string = str_replace("%", '\\%', $string);
            $string = str_replace("_", '\\_', $string);
        }

        return $this->conn->real_escape_string($string);
    }
}