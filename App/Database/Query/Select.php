<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Database\Query;

use App\Database\Driver\MySQL\Result;
use App\Database\Parent\Query;
use App\Database\Exception\ParseException;

class Select extends Query
{
    /**
     * @param array $condition
     *
     * @return Replace|Update|Select
     *
     * @throws ParseException
     */
    public function where($condition)
    {
        if(is_array($condition) && count($condition) > 0)
        {
            $this->query["where"] = $condition;
        }
        else
        {
            if(!is_array($condition))
            {
                throw new ParseException("where: invalid condition type ".gettype($condition).": expected array");
            }
            else
            {
                throw new ParseException("where: condition is empty");
            }
        }

        return $this;
    }

    /**
     * @param string $table
     * @param array $condition
     * @param string $alias
     *
     * @return Select
     *
     * @throws ParseException
     */
    public function leftJoin($table, $condition, $alias = NULL)
    {
        return $this->join("left", $table, $condition, $alias);
    }

    /**
     * @param string $table
     * @param array $condition
     * @param string $alias
     *
     * @return Select
     *
     * @throws ParseException
     */
    public function rightJoin($table, $condition, $alias = NULL)
    {
        return $this->join("right", $table, $condition, $alias);
    }

    /**
     * @param string $table
     * @param array $condition
     * @param string $alias
     *
     * @return Select
     *
     * @throws ParseException
     */
    public function innerJoin($table, $condition, $alias = NULL)
    {
        return $this->join("inner", $table, $condition, $alias);
    }

    /**
     * @param array|string $columns
     *
     * @return Select
     *
     * @throws ParseException
     */
    public function column($columns)
    {
        if(is_array($columns) && count($columns) > 0)
        {
            $list = [];
            foreach($columns as $column => $value)
            {
                if(is_int($column) && is_string($value))
                {
                    $split = explode(".", $value);
                    if(count($split) == 2)
                    {
                        $column = $value;
                        $value = $split[1];
                    }
                    else
                    {
                        $column = $value;
                    }
                }
                elseif(is_int($column) && is_array($value) && isset($value["column"]))
                {
                    $column = $value["column"];
                }

                if(is_array($value))
                {
                    if(isset($value["str"]))
                    {
                        if(!is_string($value["str"]))
                        {
                            throw new ParseException("column: invalid column string type ".gettype($value["str"]).": expected string");
                        }

                        $list[] = ["str" => $value["str"], "alias" => $value["alias"]];

                        continue;
                    }

                    if(!isset($value["alias"]) || !isset($value["function"]))
                    {
                        throw new ParseException("column: key 'alias' or 'function' missing");
                    }

                    $list[] = ["column" => $this->clear($column), "alias" => $value["alias"], "function" => $value["function"]];
                }
                else
                {
                    $list[] = ["column" => $this->clear($column), "alias" => $value];
                }
            }

            $this->query["column"] = $list;
        }
        elseif(is_string($columns))
        {
            $this->query["columnstr"] = $columns;
        }
        else
        {
            throw new ParseException("column: invalid column type ".gettype($columns).": expected array or string");
        }

        return $this;
    }

    /**
     * @param array|string $column
     * @param string $mode
     *
     * @return Select
     *
     * @throws ParseException
     */
    public function order($column, $mode = "asc")
    {
        if(is_string($column))
        {
            $column = [$column => $mode];
        }

        if(is_array($column))
        {
            foreach($column as $c => $m)
            {
                if(!is_string($c) || !is_string($m))
                {
                    throw new ParseException("order: invalid column array");
                }

                $this->query["order"][] = ["columns" => $c, "mode" => $m];
            }
        }
        else
        {
            throw new ParseException("order: invalid column type ".gettype($column).": expected string or array");
        }

        return $this;
    }

    /**
     * @param int $a start / limit
     * @param int $b limit
     *
     * @return Select
     *
     * @throws ParseException
     */
    public function limit($a, $b = NULL)
    {
        if(!is_int($a) || (isset($b) && !is_int($b)))
        {
            throw new ParseException("limit: invalid type a (".gettype($a).") or b (".gettype($b)."): expected int");
        }
        elseif(is_int($a) && !isset($b))
        {
            $b = $a;
            $a = 0;
        }

        $this->query["limit"] = ["start" => $a, "rows" => $b];

        return $this;
    }

    /**
     * @param array|string $column
     *
     * @return Select
     *
     * @throws ParseException
     */
    public function group($column)
    {
        if(is_string($column))
        {
            $this->query["group"] = [$column];
        }
        elseif(is_array($column) && count($column) > 0)
        {
            $this->query["group"] = [];

            foreach($column as $value)
            {
                if(!is_string($value))
                {
                    throw new ParseException("group: invalid column type ".gettype($value).": expected string");
                }

                $this->query["group"][] = $value;
            }
        }
        else
        {
            throw new ParseException("group: invalid column type ".gettype($column).": expected string");
        }

        return $this;
    }

    /**
     * @param int $a start row number or max rows
     * @param int $b max rows
     *
     * @return array
     * @throws ParseException
     */
    public function fetchAssoc($a = NULL, $b = NULL)
    {
        $query = $this->query;

        if(isset($a) || isset($b))
        {
            $this->limit($a, $b);
        }

        $rows = $this->execute();
        $result = [];

        while($row = $rows->fetchNextRow())
        {
            $result[] = $row;
        }

        $this->query = $query;

        return $result;
    }

    /**
     * @return array
     * @throws ParseException
     */
    public function fetchRow()
    {
        $query = $this->query;

        if(is_array($this->query["limit"]))
        {
            $this->limit($this->query["limit"]["start"], 1);
        }
        else
        {
            $this->limit(0, 1);
        }

        $result = $this->execute()->fetchNextRow();

        $this->query = $query;

        return $result;
    }

    /**
     * @return int
     * @throws ParseException
     */
    public function numRows()
    {
        if(isset($this->query["group"]))
        {
            return $this->database->select($this, "tb")->numRows();
        }

        $query = $this->query;

        $this->column(["*" => ["function" => "count", "alias" => "rowcount"]]);
        $row = $this->fetchRow();

        $this->query = $query;

        return $row["rowcount"];
    }

    /**
     * @return bool
     * @throws ParseException
     */
    public function exists()
    {
        return $this->numRows() > 0;
    }

    /**
     * @return Result
     * @throws ParseException
     */
    public function execute()
    {
        $this->executed = true;

        return $this->database->getDatabaseDriver()->query($this->buildQuery());
    }

    /**
     * @return string
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function buildQuery()
    {
        $str = "SELECT ";
        if(isset($this->query["columnstr"]))
        {
            $str .= $this->query["columnstr"]." ";
        }
        elseif(is_array($this->query["column"]))
        {
            $l = [];
            foreach($this->query["column"] as $column)
            {
                if(isset($column["str"]))
                {
                    $tmp = $column["str"];
                }
                elseif(isset($column["function"]))
                {
                    $tmp = strtoupper($column["function"])."(".$this->quoteColumn($column["column"]).")";
                }
                else
                {
                    $tmp = $this->quoteColumn($column["column"]);
                }

                if(isset($column["alias"]))
                {
                    $tmp .= " AS ".$this->quoteColumn($column["alias"]);
                }

                $l[] = $tmp;
            }

            $str .= implode(", ", $l)." ";
        }
        else
        {
            $str .= "* ";
        }

        if(is_string($this->table))
        {
            $str .= "FROM ".$this->quoteTable($this->table)." ";
        }
        elseif(is_object($this->table) && get_class($this->table) == 'App\\Database\\Query\\Select')
        {
            $str .= "FROM (".$this->table->buildQuery().") ";
            $this->table->destroy();
        }
        elseif(is_array($this->table) && count($this->table) > 0)
        {
            $queries = [];
            foreach($this->table as $select)
            {
                if(!is_object($select) || !get_class($select) == 'App\\Database\\Query\\Select')
                {
                    throw new ParseException("invalid select table");
                }

                $select->destroy();

                $queries[] = "(".$select->buildQuery().")";
            }

            if(count($queries) == 0)
            {
                throw new ParseException("empty select table array or elements of invalid type");
            }

            $str .= "FROM (".implode(" UNION ALL ", $queries).") ";
        }
        else
        {
            throw new ParseException("invalid table type ".gettype($this->table).": expected string array or Select object");
        }

        if(isset($this->query["alias"]))
        {
            $str .= "AS ".$this->quoteTable($this->query["alias"])." ";
        }

        if(is_array($this->query["join"]))
        {
            foreach($this->query["join"] as $join)
            {
                $str .= strtoupper($join["type"])." JOIN ".$this->quoteTable($join["table"])." ";

                if(isset($join["alias"]))
                {
                    $str .= "AS ".$this->quoteTable($join["alias"])." ";
                }

                $str .= "ON (".$this->parseCondition($join["condition"]).") ";
            }
        }

        if(is_array($this->query["where"]))
        {
            $str .= "WHERE ".$this->parseCondition($this->query["where"])." ";
        }

        if(isset($this->query["group"]))
        {
            $grouplist = [];
            foreach($this->query["group"] as $value)
            {
                $grouplist[] = $this->quoteColumn($value);
            }

            $str .= "GROUP BY ".implode(", ", $grouplist)." ";
        }

        if(is_array($this->query["order"]))
        {
            $l = [];
            foreach($this->query["order"] as $val)
            {
                $l[] = $this->quoteColumn($val["columns"])." ".strtoupper($val["mode"]);
            }
            $str .= "ORDER BY ".implode(", ", $l)." ";
        }

        if(is_array($this->query["limit"]))
        {
            $str .= "LIMIT ".$this->query["limit"]["start"].", ".$this->query["limit"]["rows"]." ";
        }

        return $str;
    }
}