<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Database\Query;

use App\Database\Parent\Query;

class Replace extends Query
{
    private $select;
    private $update;
    private $insert;

    public function __construct($table, $alias, $database)
    {
        parent::__construct($table, $alias, $database);

        $this->select = new Select($table, $alias, $database);
        $this->update = new Update($table, $alias, $database);
        $this->insert = new Insert($table, $alias, $database);
    }

    /**
     * @param array $condition
     *
     * @return $this
     * @throws \App\Database\Exception\ParseException
     */
    public function where($condition)
    {
        $this->select->where($condition);
        $this->update->where($condition);

        return $this;
    }

    /**
     * @param array $data
     *
     * @return $this
     * @throws \App\Database\Exception\ParseException
     */
    public function set($data)
    {
        $this->insert->set($data);
        $this->update->set($data);

        return $this;
    }

    /**
     * @return int
     * @throws \App\Database\Exception\ParseException
     */
    public function getID()
    {
        return $this->execute();
    }

    /**
     * @param string $idname
     *
     * @return int
     * @throws \App\Database\Exception\ParseException
     */
    public function execute($idname = "id")
    {
        $row = $this->select->fetchRow();
        if(is_array($row))
        {
            $this->insert->destroy();
            $this->update->execute();

            $result = $row[$idname];
        }
        else
        {
            $this->update->destroy();

            $result = $this->insert->getID();
        }

        return $result;
    }
}