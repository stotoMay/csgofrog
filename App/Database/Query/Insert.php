<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Database\Query;

use App\Database\Driver\MySQL\Result;
use App\Database\Exception\ParseException;
use App\Database\Parent\Query;

class Insert extends Query
{
    private $chunksize = 100;

    /**
     * @param array $data
     *
     * @return Insert|Replace|Update
     *
     * @throws ParseException
     */
    public function set($data)
    {
        if(is_array($data) && count($data) > 0)
        {
            $this->query["set"] = $data;
        }
        else
        {
            if(!is_array($data))
            {
                throw new ParseException("set: invalid set type ".gettype($data).": expected array");
            }
            else
            {
                throw new ParseException("set: empty set data");
            }

        }

        return $this;
    }

    /**
     * @return Result
     * @throws ParseException
     */
    public function execute()
    {
        $this->executed = true;

        $result = NULL;
        $queries = $this->buildQuery();

        foreach($queries as $query)
        {
            $result = $this->database->getDatabaseDriver()->query($query);
        }

        return $result;
    }

    /**
     * @return int
     * @throws ParseException
     */
    public function getID()
    {
        return $this->execute()->getLastID();
    }

    /**
     * @return array
     *
     * @throws ParseException
     */
    private function buildQuery()
    {
        if(!is_array($this->query["set"]))
        {
            throw new ParseException("insert query needs data to insert");
        }

        if(count(array_filter(array_keys($this->query["set"]), 'is_string')) == 0)
        {
            $chunks = array_chunk($this->query["set"], $this->chunksize);
        }
        else
        {
            $chunks = [[$this->query["set"]]];
        }

        $queries = [];

        foreach($chunks as $rows)
        {
            $queries[] = $this->buildRowQuery($rows);
        }

        return $queries;
    }

    /**
     * @param array $rows
     *
     * @return string
     *
     * @throws ParseException
     */
    private function buildRowQuery($rows)
    {
        if(!is_string($this->table))
        {
            throw new ParseException("invalid table name type ".gettype($this->table).": expected string");
        }

        $str = "INSERT INTO ".$this->quoteTable($this->table)." ";

        if(isset($this->query["alias"]))
        {
            $str .= "AS ".$this->quoteTable($this->query["alias"])." ";
        }

        $keys = [];
        $values = [];

        foreach($rows as $row)
        {
            $insertrow = [];
            foreach($row as $column => $value)
            {
                if(!is_int(array_search($column, $keys)))
                {
                    $keys[] = $column;
                    foreach($values as $i => $v)
                    {
                        $values[$i][] = NULL;
                    }
                }
            }

            foreach($keys as $key)
            {
                $insertrow[] = isset($row[$key]) ? $this->database->quote($row[$key]) : $this->database->quote(NULL);
            }

            $values[] = $insertrow;
        }

        foreach($keys as $key => $column)
        {
            $keys[$key] = $this->quoteColumn($column);
        }

        $valuelist = [];
        foreach($values as $value)
        {
            $valuelist[] = "(".implode(",", $value).")";
        }

        $str .= "(".implode(",", $keys).") VALUES ".implode(", ", $valuelist);

        return $str;
    }
}