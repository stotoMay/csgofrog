<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Database\Query;

use App\Database\Exception\ParseException;
use App\Database\Parent\Query;

class Delete extends Query
{
    /**
     * @param array $condition
     *
     * @return Delete
     *
     * @throws ParseException
     */
    public function where($condition)
    {
        if(is_array($condition) && count($condition) > 0)
        {
            $this->query["where"] = $condition;
        }
        else
        {
            if(!is_array($condition))
            {
                throw new ParseException("where: invalid condition type ".gettype($condition).": expected array");
            }
            else
            {
                throw new ParseException("where: where condition is empty");
            }
        }

        return $this;
    }

    /**
     * @return \App\Database\Driver\MySQL\Result
     * @throws ParseException
     */
    public function execute()
    {
        $this->executed = true;

        return $this->database->getDatabaseDriver()->query($this->buildQuery());
    }

    /**
     * @return int
     * @throws ParseException
     */
    public function getAffectedRows()
    {
        return $this->execute()->getAffectedRows();
    }

    /**
     * @return string
     *
     * @throws ParseException
     */
    private function buildQuery()
    {
        if(!is_string($this->table))
        {
            throw new ParseException("invalid table name type ".gettype($this->table).": expected string");
        }

        if(!is_array($this->query["where"]))
        {
            throw new ParseException("delete query needs a condition");
        }

        $str = "DELETE FROM ".$this->quoteTable($this->table)." ";

        if(isset($this->query["alias"]))
        {
            $str .= "AS ".$this->quoteTable($this->query["alias"])." ";
        }

        $str .= "WHERE ".$this->parseCondition($this->query["where"]);

        return $str;
    }


}