<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Database\Query;

use App\Database\Exception\ParseException;
use App\Database\Parent\Query;

class Update extends Query
{
    /**
     * @param array $condition
     *
     * @return Replace|Update|Select
     *
     * @throws ParseException
     */
    public function where($condition)
    {
        if(is_array($condition) && count($condition) > 0)
        {
            $this->query["where"] = $condition;
        }
        else
        {
            if(!is_array($condition))
            {
                throw new ParseException("where: invalid condition type ".gettype($condition).": expected array");
            }
            else
            {
                throw new ParseException("where: where condition is empty");
            }
        }

        return $this;
    }

    /**
     * @param array $data
     *
     * @return Insert|Replace|Update
     *
     * @throws ParseException
     */
    public function set($data)
    {
        if(is_array($data) && count($data) > 0)
        {
            $this->query["set"] = $data;
        }
        else
        {
            if(!is_array($data))
            {
                throw new ParseException("set: invalid set type ".gettype($data).": expected array");
            }
            else
            {
                throw new ParseException("set: empty set data");
            }

        }

        return $this;
    }

    /**
     * @param string $table
     * @param array $condition
     * @param string $alias
     *
     * @return Update
     *
     * @throws ParseException
     */
    public function leftJoin($table, $condition, $alias = NULL)
    {
        return $this->join("left", $table, $condition, $alias);
    }

    /**
     * @param string $table
     * @param array $condition
     * @param string $alias
     *
     * @return Update
     *
     * @throws ParseException
     */
    public function rightJoin($table, $condition, $alias = NULL)
    {
        return $this->join("right", $table, $condition, $alias);
    }

    /**
     * @param string $table
     * @param array $condition
     * @param string $alias
     *
     * @return Update
     *
     * @throws ParseException
     */
    public function innerJoin($table, $condition, $alias = NULL)
    {
        return $this->join("inner", $table, $condition, $alias);
    }

    /**
     * @return \App\Database\Driver\MySQL\Result
     * @throws ParseException
     */
    public function execute()
    {
        $this->executed = true;

        return $this->database->getDatabaseDriver()->query($this->buildQuery());
    }

    /**
     * @return int
     * @throws ParseException
     */
    public function getAffectedRows()
    {
        return $this->execute()->getAffectedRows();
    }

    /**
     * @return string
     *
     * @throws ParseException
     */
    private function buildQuery()
    {
        if(!is_string($this->table))
        {
            throw new ParseException("invalid table name type ".gettype($this->table).": expected string");
        }

        $str = "UPDATE ".$this->quoteTable($this->table)." ";

        if(!is_array($this->query["set"]) || count($this->query["set"]) == 0)
        {
            throw new ParseException("update query needs data");
        }

        if(isset($this->query["alias"]))
        {
            $str .= "AS ".$this->quoteTable($this->query["alias"])." ";
        }

        if(is_array($this->query["join"]))
        {
            foreach($this->query["join"] as $join)
            {
                $str .= strtoupper($join["type"])." JOIN ".$this->quoteTable($join["table"])." ";

                if(isset($join["alias"]))
                {
                    $str .= "AS ".$this->quoteTable($join["alias"])." ";
                }

                $str .= "ON (".$this->parseCondition($join["condition"]).") ";
            }
        }

        $l = [];
        foreach($this->query["set"] as $column => $value)
        {
            $l[] = $this->quoteColumn($column)." = ".$this->database->quote($value)." ";
        }

        $str .= "SET ".implode(", ", $l);

        if(is_array($this->query["where"]))
        {
            $str .= " WHERE ".$this->parseCondition($this->query["where"])." ";
        }

        return $str;
    }
}