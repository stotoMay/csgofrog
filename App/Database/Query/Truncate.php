<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Database\Query;

use App\Database\Exception\ParseException;
use App\Database\Parent\Query;

class Truncate extends Query
{
    /**
     * @return \App\Database\Driver\MySQL\Result
     * @throws ParseException
     */
    public function execute()
    {
        $this->executed = true;

        return $this->database->getDatabaseDriver()->query($this->buildQuery());
    }

    /**
     * @return int
     * @throws ParseException
     */
    public function getAffectedRows()
    {
        return $this->execute()->getAffectedRows();
    }

    /**
     * @return string
     *
     * @throws ParseException
     */
    private function buildQuery()
    {
        if(!is_string($this->table))
        {
            throw new ParseException("invalid table name type ".gettype($this->table).": expected string");
        }

        $str = "TRUNCATE ".$this->quoteTable($this->table);

        return $str;
    }


}