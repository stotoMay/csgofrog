<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Database;

use App\Database\Exception\ConnectException;
use App\Database\Exception\DriverException;
use App\Database\Query\Delete;
use App\Database\Query\Insert;
use App\Database\Query\Replace;
use App\Database\Query\Select;
use App\Database\Query\Truncate;
use App\Database\Query\Update;

class Database
{
    /**
     * @var Parent\Connection
     */
    private $driver;

    private $prefix;

    /**
     * @param string $driver
     * @param string $database
     * @param string $host
     * @param int $port
     * @param string $user
     * @param string $password
     * @param string $prefix
     *
     * @throws ConnectException
     * @throws DriverException
     */
    public function __construct($driver, $database, $host, $port, $user, $password, $prefix = "")
    {
        $driver = 'App\\Database\\Driver\\'.$driver.'\\Connection';

        if(class_exists($driver) && get_parent_class($driver) == 'App\\Database\\Parent\\Connection')
        {
            $this->driver = new $driver($database, $host, $port, $user, $password);
        }
        else
        {
            throw new DriverException();
        }

        $this->prefix = $prefix;
    }

    public function __destruct()
    {
        if(is_object($this->driver))
        {
            $this->driver->disconnect();
        }
    }

    /**
     * @param string|array|Select $table
     * @param string $alias
     *
     * @return Select
     */
    public function select($table, $alias = NULL)
    {
        return new Select($table, $alias, $this);
    }

    /**
     * @param string $table
     * @param string $alias
     *
     * @return Update
     */
    public function update($table, $alias = NULL)
    {
        return new Update($table, $alias, $this);
    }

    /**
     * @param string $table
     * @param string $alias
     *
     * @return Replace
     */
    public function replace($table, $alias = NULL)
    {
        return new Replace($table, $alias, $this);
    }

    /**
     * @param string $table
     * @param string $alias
     *
     * @return Delete
     */
    public function delete($table, $alias = NULL)
    {
        return new Delete($table, $alias, $this);
    }

    /**
     * @param string $table
     * @param string $alias
     *
     * @return Truncate
     */
    public function truncate($table, $alias = NULL)
    {
        return new Truncate($table, $alias, $this);
    }

    /**
     * @param string $table
     * @param string $alias
     *
     * @return Insert
     */
    public function insert($table, $alias = NULL)
    {
        return new Insert($table, $alias, $this);
    }


    /**
     * @return string
     */
    public function getTablePrefix()
    {
        return $this->prefix;
    }

    /**
     * @return Parent\Connection
     */
    public function getDatabaseDriver()
    {
        return $this->driver;
    }


    /**
     * @param string $querystring
     *
     * @return mixed
     */
    public function query($querystring)
    {
        return $this->driver->query($querystring);
    }

    /**
     * @param string $string
     * @param string $operator
     *
     * @return string
     */
    public function quote($string, $operator = NULL)
    {
        return $this->driver->quote($string, $operator);
    }

    /**
     * @param string $string
     * @param bool $replace
     *
     * @return string
     */
    public function escape($string, $replace = false)
    {
        return $this->driver->escape($string, $replace);
    }
}