<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\DOM;

class XMLDocument extends Document
{
    public function __construct($xml)
    {
        $this->document = new \DOMDocument();

        $this->document->recover = true;
        $this->document->strictErrorChecking = false;

        libxml_use_internal_errors(true);

        $this->document->loadXML($xml);
    }
}