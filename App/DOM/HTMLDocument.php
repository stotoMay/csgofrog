<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\DOM;

class HTMLDocument extends Document
{
    public function __construct($html)
    {
        $this->document = new \DOMDocument();

        $this->document->recover = true;
        $this->document->strictErrorChecking = false;

        libxml_use_internal_errors(true);

        $html = $this->fixamps($html);
        $this->document->loadHTML($html);
    }

    /**
     * @param string $html
     * @param int $offset
     *
     * @return string mixed
     */
    private function fixamps($html, $offset = 0)
    {
        $positionAmp = strpos($html, '&', $offset);
        $positionSemiColumn = strpos($html, ';', $positionAmp + 1);

        $string = substr($html, $positionAmp, $positionSemiColumn - $positionAmp + 1);

        if($positionAmp !== false)
        {
            if($positionSemiColumn === false)
            {
                $html = substr_replace($html, '&amp;', $positionAmp, 1);
            }
            elseif(preg_match('/&(#[0-9]+|[A-Z|a-z|0-9]+);/', $string) === 0)
            {
                $html = substr_replace($html, '&amp;', $positionAmp, 1);
                $html = $this->fixamps($html, $positionAmp + 5);
            }
            else
            {
                $html = $this->fixamps($html, $positionAmp + 1);
            }
        }

        return $html;
    }
}