<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\DOM;

class Document
{
    /**
     * @var \DOMDocument
     */
    protected $document;

    /**
     * @param string $selector
     *
     * @return Element
     */
    public function find($selector)
    {
        $selectors = explode(" ", $selector);

        $elements = $this->finder($this->document->childNodes, $selectors[0]);

        unset($selectors[0]);
        $element = new Element($elements, $this);

        foreach($selectors as $s)
        {
            $element = $element->find($s);
        }

        return $element;
    }

    /**
     * @return string
     */
    public function html()
    {
        return $this->document->C14N();
    }

    /**
     * @return string
     */
    public function text()
    {
        return $this->document->nodeValue;
    }

    /**
     * @return \DOMDocument
     */
    public function document()
    {
        return $this->document;
    }

    /**
     * @param object $node
     * @param string $selection
     *
     * @return array
     */
    public function finder($node, $selection)
    {
        $elements = [];

        if(get_class($node) == "DOMNodeList")
        {
            foreach($node as $n)
            {
                $elements = array_merge($elements, $this->finder($n, $selection));
            }

            return $elements;
        }
        elseif(get_class($node) == "DOMElement")
        {
            /**
             * @var \DOMElement $node
             */
            if($node->hasAttribute("id") && $selection[0] == "#")
            {
                if($node->getAttribute("id") == substr($selection, 1))
                {
                    $elements[] = $node;
                }
            }
            elseif($node->hasAttribute("class") && $selection[0] == ".")
            {
                $classes = explode(" ", $node->getAttribute("class"));

                if(is_int(array_search(substr($selection, 1), $classes)))
                {
                    $elements[] = $node;
                }
            }
            elseif($selection == $node->tagName)
            {
                $elements[] = $node;
            }


            if($node->hasChildNodes() && ($selection[0] != "#" || count($elements) == 0))
            {
                return array_merge($elements, $this->finder($node->childNodes, $selection));
            }
        }

        return $elements;
    }
}