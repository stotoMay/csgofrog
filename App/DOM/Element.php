<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\DOM;

use App\DOM\Exception\NodeNotFoundException;

class Element
{
    /**
     * @var array
     */
    private $elements;
    /**
     * @var Document
     */
    private $document;

    /**
     * @param array $elements
     * @param Document $document
     */
    public function __construct($elements, $document)
    {
        $this->elements = $elements;
        $this->document = $document;
    }

    /**
     * @param string $selection
     *
     * @return Element
     */
    public function find($selection)
    {
        $elements = [];
        foreach($this->elements as $element)
        {
            $elements = array_merge($elements, $this->document->finder($element, $selection));
        }

        return new Element($elements, $this->document);
    }


    /**
     * @param string $name
     *
     * @return string
     */
    public function attr($name)
    {
        if(count($this->elements) == 0)
        {
            return "";
        }

        /**
         * @var \DOMElement $element
         */
        $element = $this->elements[0];

        if(!$element->hasAttribute($name))
        {
            return "";
        }

        return $element->getAttribute($name);
    }

    /**
     * @param string $name
     * @param string $default
     *
     * @return array|string
     */
    public function css($name = NULL, $default = "")
    {
        $tags = array_clear(explode(";", str_replace(" ", "", $this->attr("style"))));
        $css = [];

        foreach($tags as $tag)
        {
            $n = explode(":", $tag);
            $css[strtolower($n[0])] = $n[1];
        }

        if(is_string($name))
        {
            return isset($css[strtolower($name)]) ? $css[strtolower($name)] : $default;
        }
        else
        {
            return $css;
        }
    }


    /**
     * @return string
     */
    public function html()
    {
        if(count($this->elements) == 0)
        {
            return "";
        }

        /**
         * @var \DOMElement $element
         */
        $element = $this->elements[0];

        if(!$element->hasChildNodes())
        {
            return "";
        }

        $html = "";
        foreach($element->childNodes as $child)
        {
            /**
             * @var \DOMNode $child
             */
            $html .= $child->C14N();
        }

        return $html;
    }

    /**
     * @return string
     */
    public function text()
    {
        if(count($this->elements) == 0)
        {
            return "";
        }

        /**
         * @var \DOMElement $element
         */
        $element = $this->elements[0];

        return $element->nodeValue;
    }

    /**
     * @param int $index
     *
     * @return array|Element
     *
     * @throws NodeNotFoundException
     */
    public function item($index = NULL)
    {
        if(!isset($index))
        {
            $list = [];
            foreach($this->elements as $element)
            {
                $list[] = new Element([$element], $this->document);
            }

            return $list;
        }

        if(isset($this->elements[$index]))
        {
            return new Element([$this->elements[$index]], $this->document);
        }

        throw new NodeNotFoundException();
    }
}