<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Logger;

use App\Database\Database;

class DBLogger extends Logger
{
    private $database;
    private $name;

    /**
     * @param string $name
     * @param Database $database
     */
    public function __construct($name, Database $database)
    {
        $this->database = $database;
        $this->name = $name;
    }


    /**
     * @param string $msg
     * @param mixed $data
     *
     * @return int
     * @throws \App\Database\Exception\ParseException
     */
    public function error($msg, $data = NULL)
    {
        return $this->log($msg, $data, 3);
    }

    /**
     * @param string $msg
     * @param mixed $data
     *
     * @return int
     * @throws \App\Database\Exception\ParseException
     */
    public function warning($msg, $data = NULL)
    {
        return $this->log($msg, $data, 2);
    }

    /**
     * @param string $msg
     * @param mixed $data
     *
     * @return int
     * @throws \App\Database\Exception\ParseException
     */
    public function info($msg, $data = NULL)
    {
        return $this->log($msg, $data, 1);
    }

    /**
     * @param string $msg
     * @param mixed $data
     *
     * @return int
     * @throws \App\Database\Exception\ParseException
     */
    public function debug($msg, $data = NULL)
    {
        return $this->log($msg, $data, 0);
    }

    /**
     * @param int $lines
     *
     * @return array
     * @throws \App\Database\Exception\ParseException
     */
    public function getLog($lines = 100)
    {
        return $this->database->select("log")->order("dateline", "desc")->limit(0, $lines)->fetchAssoc();
    }

    /**
     * @param string $message
     * @param mixed $data
     * @param int $level
     *
     * @return int
     * @throws \App\Database\Exception\ParseException
     */
    private function log($message, $data = NULL, $level = 1)
    {
        if($level < $this->level)
        {
            return 0;
        }

        $logID = $this->database->insert("log")->set([
            "name" => $this->name, "level" => $level, "message" => $message,
            "data" => var_export($data, true), "dateline" => time()
        ])->getID();

        return $logID;
    }
}