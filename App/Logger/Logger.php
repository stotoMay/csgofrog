<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Logger;

abstract class Logger
{
    const ALL = 3;
    const WARNING = 2;
    const ERROR = 1;
    const NONE = 0;

    protected $level = 3;

    public abstract function error($msg, $data = NULL);

    public abstract function info($msg, $data = NULL);

    public abstract function debug($msg, $data = NULL);

    public abstract function warning($msg, $data = NULL);

    public abstract function getLog($limit = NULL);

    /**
     * @param int $int
     */
    public function setLevel($int)
    {
        $this->level = $int;
    }
}