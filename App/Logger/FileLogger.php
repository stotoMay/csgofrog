<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Logger;

use App\Logger\Exception\FileNotFoundException;

class FileLogger extends Logger
{
    private $file;

    /**
     * @param string $file
     *
     * @throws FileNotFoundException
     */
    public function __construct($file)
    {
        if(!file_exists($file) || !is_writable($file))
        {
            throw new FileNotFoundException();
        }

        $this->file = $file;
    }


    /**
     * @param string $msg
     * @param mixed $data
     *
     * @return int
     */
    public function error($msg, $data = NULL)
    {
        return $this->log($msg, $data, 3);
    }

    /**
     * @param string $msg
     * @param mixed $data
     *
     * @return int
     */
    public function warning($msg, $data = NULL)
    {
        return $this->log($msg, $data, 2);
    }

    /**
     * @param string $msg
     * @param mixed $data
     *
     * @return int
     */
    public function info($msg, $data = NULL)
    {
        return $this->log($msg, $data, 1);
    }

    /**
     * @param string $msg
     * @param mixed $data
     *
     * @return int
     */
    public function debug($msg, $data = NULL)
    {
        return $this->log($msg, $data, 0);
    }

    /**
     * @param int $lines
     *
     * @return array
     */
    public function getLog($lines = 100)
    {
        $logs = [];
        $fp = fopen($this->file, "r");
        $count = 0;
        $tmp = "";

        while(!feof($fp))
        {
            $line = fgets($fp, 4096);

            if($line[0] == "~")
            {
                array_push($logs, $tmp);
                $count++;
                $tmp = "";
            }

            $tmp .= $line;

            if($count > $lines)
            {
                array_shift($logs);
                $count--;
            }
        }

        fclose($fp);

        return $logs;
    }

    /**
     * @param string $message
     * @param mixed $data
     * @param int $level
     *
     * @return int
     */
    private function log($message, $data = NULL, $level = 1)
    {
        if($level < $this->level)
        {
            return 0;
        }

        $levelName = ["DEBUG", "INFO", "WARNING", "ERROR"];

        $line = "~[".date("d-m-Y H:i:s").".".str_replace("0.", "", explode(" ", microtime())[0])."][".$levelName[$level]."]: ".$message.PHP_EOL;

        if(isset($data))
        {
            $line .= var_export($data, true).PHP_EOL;
        }

        return file_put_contents($this->file, $line, FILE_APPEND);
    }
}