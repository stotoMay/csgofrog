<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Storage;

class TCache
{
    private $cache;

    public function __construct()
    {
        $this->cache = [];
    }

    /**
     * @param string $key
     * @param string $class
     *
     * @return mixed
     */
    public function get($key, $class = "global")
    {
        return $this->cache[$class][$key];
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param string $class
     */
    public function set($key, $value, $class = "global")
    {
        $this->cache[$class][$key] = $value;
    }

    /**
     * @param string $key
     * @param string $class
     *
     * @return bool
     */
    public function exists($key, $class = "global")
    {
        return isset($this->cache[$class][$key]);
    }
}