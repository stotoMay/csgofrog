<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Storage;

use App\Storage\Exception\MCacheException;

class MCache
{
    private $folder;

    /**
     * @param string $folder
     *
     * @throws MCacheException
     * @throws \App\Core\Exception\InvalidArgumentException
     */
    public function __construct($folder)
    {

        $this->folder = filter_folder($folder);

        if(!file_exists($folder))
        {
            throw new MCacheException();
        }
    }

    /**
     * @param string $key
     * @param mixed $data
     * @param int $expire
     * @param string $class
     */
    public function store($key, $data, $expire = NULL, $class = NULL)
    {
        $filename = $this->encodeFileName($key).".cache";
        $filedata = serialize(["data" => $data, "expire" => $expire]);

        if(isset($class))
        {
            $foldername = $this->encodeFileName($class)."/";

            if(!file_exists($this->folder.$foldername))
            {
                mkdir($this->folder.$foldername);
            }

            file_put($this->folder.$foldername.$filename, $filedata);
        }
        else
        {
            file_put($this->folder.$filename, $filedata);
        }
    }

    /**
     * @param string $key
     * @param string $class
     *
     * @return mixed
     */
    public function retrieve($key, $class = NULL)
    {
        $file = $this->encodeFileName($key).".cache";
        if(isset($class))
        {
            $file = $this->encodeFileName($class)."/".$file;
        }

        if(file_exists($this->folder.$file))
        {
            $data = unserialize(file_get($this->folder.$file));

            if(is_int($data["expire"]) && $data["expire"] >= time())
            {
                return $data["data"];
            }
            elseif(!is_int($data["expire"]))
            {
                return $data["data"];
            }
            else
            {
                return NULL;
            }
        }
        else
        {
            return NULL;
        }
    }

    /**
     * @param string $key
     * @param string $class
     *
     * @return bool
     */
    public function is_cached($key, $class = NULL)
    {
        if(isset($class))
        {
            return file_exists($this->folder.base64_encode($class)."/".base64_encode($key).".cache");
        }
        else
        {
            return file_exists($this->folder.base64_encode($key).".cache");
        }
    }

    /**
     * @param $str
     *
     * @return string
     */
    private function encodeFileName($str)
    {
        return str_replace("/", "_", str_replace("+", "-", base64_encode($str)));
    }
}