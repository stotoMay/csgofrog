<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Theme;

use App\Storage\MCache;
use App\Theme\Exception\FolderNotFoundException;
use App\Theme\Exception\TemplateNotFoundException;

class Theme
{
    private $templateList;

    private $vars;
    private $funcs;

    private $args;
    private $folder;

    private $cache;

    /**
     * @param string $folder
     * @param string $cache
     * @param array $args
     *
     * @throws FolderNotFoundException
     * @throws \App\Core\Exception\FolderNotFoundException
     * @throws \App\Core\Exception\InvalidArgumentException
     */
    public function __construct($folder, $cache = NULL, $args = NULL)
    {
        if(!is_array($args))
        {
            $args = [$this];
        }
        else
        {
            $args = array_merge([$this], $args);
        }

        $this->args = $args;
        $this->folder = $folder;

        $this->vars = [];
        $this->funcs = [];

        $this->assignVar("folder", $folder);
        $this->registerFolder($folder);

        if(isset($cache))
        {
            $this->cache = new MCache($cache);
        }
    }

    /**
     * @return MCache
     */
    public function cache()
    {
        return $this->cache;
    }

    /**
     * @param string $folder
     * @param string $prefix
     *
     * @return bool
     * @throws FolderNotFoundException
     * @throws \App\Core\Exception\FolderNotFoundException
     * @throws \App\Core\Exception\InvalidArgumentException
     */
    public function registerFolder($folder, $prefix = "")
    {
        $folder = filter_folder($folder);

        if(!base_dir($folder))
        {
            throw new FolderNotFoundException();
        }

        $folders = get_dir_folders(base_dir($folder));

        foreach($folders as $f)
        {
            $this->registerFolder($folder.$f, $prefix.$f."/");
        }

        $files = get_dir_files(base_dir($folder));
        foreach($files as $f)
        {
            if(!is_int(strpos($f, ".tpl")))
            {
                continue;
            }

            $this->registerFile($prefix.$f, $folder.$f);
        }

        return true;
    }

    /**
     * @param string $templateName
     * @param string $file
     */
    public function registerFile($templateName, $file)
    {
        if(file_exists(base_dir($file)))
        {
            $this->templateList[$templateName] = $file;
        }
    }

    /**
     * @param string $tpl
     *
     * @return string
     *
     * @throws TemplateNotFoundException
     */
    public function getFilePath($tpl)
    {
        if(!isset($this->templateList[$tpl]))
        {
            throw new TemplateNotFoundException();
        }

        return $this->templateList[$tpl];
    }

    /**
     * @param $tpl
     *
     * @return Template
     * @throws TemplateNotFoundException
     * @throws \App\Core\Exception\FileNotFoundException
     */
    public function draw($tpl)
    {
        return $this->parse(file_get(base_dir($this->getFilePath($tpl))), $tpl);
    }

    /**
     * @param string $code
     * @param string $identifer
     *
     * @return Template
     */
    public function parse($code, $identifer = NULL)
    {
        $template = new Template($code, $this, $this->args, $identifer);

        $template->assignVar($this->vars);
        foreach($this->funcs as $n => $closure)
        {
            $template->assignFunction($n, $closure);
        }

        return $template;
    }

    /**
     * @param string $file
     *
     * @return string
     * @throws \App\Core\Exception\FileNotFoundException
     */
    public function getFile($file)
    {
        return file_get(base_dir($this->folder.$file));
    }

    /**
     * @param string $name
     * @param mixed $value
     *
     * @return Theme
     */
    public function assignVar($name, $value = NULL)
    {
        if(is_array($name))
        {
            foreach($name as $k => $v)
            {
                $this->assignVar($k, $v);
            }

            return $this;
        }

        $this->vars[$name] = $value;

        return $this;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function assignFunction($name, $value)
    {
        if(is_callable($value))
        {
            $this->funcs[$name] = $value;
        }
    }
}