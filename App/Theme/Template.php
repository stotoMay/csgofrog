<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Theme;

class Template extends Parser
{
    private $str;

    /**
     * @param string $str
     * @param Theme $theme
     * @param array $args
     * @param string $identifer
     */
    public function __construct($str, Theme $theme, array $args = [], $identifer = NULL)
    {
        $this->str = $str;
        $this->theme = $theme;
        $this->args = $args;

        $this->identifer = $identifer;
    }

    /**
     * @return string
     * @throws Exception\ParseException
     * @throws Exception\TemplateNotFoundException
     * @throws \App\Core\Exception\FileNotFoundException
     */
    public function __tostring()
    {
        return $this->parse();
    }

    /**
     * @return string
     * @throws Exception\ParseException
     * @throws Exception\TemplateNotFoundException
     * @throws \App\Core\Exception\FileNotFoundException
     */
    public function parse()
    {
        return $this->parseSplit($this->splitStr($this->str));
    }
}