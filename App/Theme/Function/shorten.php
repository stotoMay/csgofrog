<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function($args, $str, $max = 18)
{
    $result = [];

    $split = explode(" ", $str);

    foreach($split as $s)
    {
        if(strlen($s) >= $max)
        {
            $result[] = implode(" ", str_split($s, $max));
        }
        else
        {
            $result[] = $s;
        }
    }

    return implode(" ", $result);
};