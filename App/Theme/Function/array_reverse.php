<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function($args, $a)
{
    if(!is_array($a))
    {
        return $a;
    }

    return array_reverse($a);
};