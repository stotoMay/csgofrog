<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function()
{
    $args = func_get_args();
    $mul = $args[1];

    unset($args[0]);
    unset($args[1]);

    foreach($args as $arg)
    {
        $mul *= $arg;
    }

    return $mul;
};