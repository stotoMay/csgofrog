<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function($args, $time)
{
    /**
     * @var \App\Core\App $app
     */
    $app = $args[1];

    /**
     * @var App\HTTP\Request $request
     */
    $request = $args[2];

    if(is_object($app))
    {
        return date($app->setting()->get("timeformat", "application", "time"), $time + $request->timezoneOffset());
    }
    else
    {
        return time() + $request->timezoneOffset();
    }
};