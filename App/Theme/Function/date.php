<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function($args, $input, $time = NULL)
{
    if(!isset($time))
    {
        $time = time();
    }

    /**
     * @var App\HTTP\Request $request
     */
    $request = $args[2];

    return date($input, $time + $request->timezoneOffset());
};