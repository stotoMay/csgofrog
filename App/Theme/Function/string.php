<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function()
{
    $args = func_get_args();
    unset($args[0]);
    $str = "";

    foreach($args as $arg)
    {
        $str .= (string)$arg;
    }

    return $str;
};