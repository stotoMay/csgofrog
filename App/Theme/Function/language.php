<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function($args)
{
    /**
     * @var App\Core\App $app
     */
    $app = $args[1];

    $array = func_get_args();
    unset($array[0]);
    $array = array_values($array);

    return $app->language()->get($array);
};