<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function($args, $input)
{
    if($input < 0)
    {
        return (-1 * $input);
    }
    else
    {
        return $input;
    }
};