<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function($args)
{
    /**
     * @var App\HTTP\Browser $browser
     */
    $browser = $args[3];

    if(is_object($browser))
    {
        return $browser->isLogin();
    }
    else
    {
        return false;
    }
};