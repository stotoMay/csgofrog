<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function($args, $needle, $array)
{
    if(!is_array($array) || !is_string($needle))
    {
        return false;
    }

    return is_int(array_search($needle, $array));
};