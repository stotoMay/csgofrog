<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function($args, $usertimezone = true)
{
    /**
     * @var App\HTTP\Request $request
     */
    $request = $args[2];

    if($usertimezone)
    {
        return strtotime("midnight", time() + $request->timezoneOffset()) - $request->timezoneOffset();
    }
    else
    {
        return strtotime("midnight", time());
    }
};