<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function($args, $seconds)
{
    if(!is_numeric($seconds))
    {
        return "";
    }

    if($seconds < 60)
    {
        return $seconds."s";
    }
    elseif($seconds < 3600)
    {
        $s = $seconds % 60;
        $m = ($seconds - $s) / 60;

        if($s == 0)
        {
            return $m."m";
        }

        return $m."m ".$s."s";
    }
    else
    {
        $rest = $seconds % 3600;
        $h = ($seconds - $rest) / 3600;
        $s = $rest % 60;
        $m = ($rest - $s) / 60;

        if($m == 0 && $s == 0)
        {
            return $h."h";
        }

        if($s == 0)
        {
            return $h."h ".$m."m";
        }

        return $h."h ".$m."m ".$s."s";
    }
};