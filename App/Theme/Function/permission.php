<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function($args, $name, $type, $typeID)
{
    /**
     * @var App\HTTP\Browser $browser
     */
    $browser = $args[3];

    if(is_object($browser) && $browser->isLogin())
    {
        return $browser->user()->permission()->get($name, $type, $typeID);
    }
    else
    {
        return NULL;
    }
};