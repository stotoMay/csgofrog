<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function()
{
    $args = func_get_args();
    /**
     * @var \App\Core\App $app
     */
    $app = $args[0][1];

    $hookname = $args[1];
    unset($args[1]);
    $params = array_values($args);

    $result = "";
    $templates = $app->component()->setHook($hookname, $params);

    foreach($templates as $template)
    {
        $result .= $template;
    }

    return $result;
};