<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function()
{
    $args = func_get_args();
    unset($args[0]);
    $sum = $args[1];
    unset($args[1]);

    foreach($args as $arg)
    {
        $sum -= $arg;
    }

    return $sum;
};