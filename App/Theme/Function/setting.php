<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function($args, $name, $type, $typeID)
{
    /**
     * @var App\Core\App $app
     */
    $app = $args[1];

    if(is_object($app))
    {
        return $app->setting()->get($name, $type, $typeID);
    }
    else
    {
        return NULL;
    }
};