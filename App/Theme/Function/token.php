<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function($args)
{
    /**
     * @var App\HTTP\Request $request
     */
    $request = $args[2];

    if(is_object($request))
    {
        return $request->token();
    }
    else
    {
        return NULL;
    }
};