<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

return function($args, $column)
{
    /**
     * @var App\HTTP\Browser $browser
     */
    $browser = $args[3];

    if(is_object($browser) && $browser->isLogin())
    {
        return $browser->user()->getColumn($column);
    }
    else
    {
        return NULL;
    }
};