<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Theme;

use App\Theme\Exception\ParseException;

class Parser
{
    private $vars = [];
    private $funcs = [];

    /**
     * @var Theme
     */
    protected $theme;
    /**
     * @var array
     */
    protected $args;
    /**
     * @var string
     */
    protected $identifer;

    /**
     * @param string|array $name
     * @param mixed $value
     *
     * @return $this
     */
    public function assignVar($name, $value = NULL)
    {
        if(is_array($name))
        {
            foreach($name as $k => $v)
            {
                $this->assignVar($k, $v);
            }

            return $this;
        }

        $this->vars[$name] = $value;

        return $this;
    }

    /**
     * @param string $name
     * @param mixed $function
     *
     * @return $this
     */
    public function assignFunction($name, $function)
    {
        if(is_callable($function))
        {
            $this->funcs[$name] = $function;
        }

        return $this;
    }

    /**
     * @param string $str
     *
     * @return array
     */
    protected function splitStr($str)
    {
        $letters = str_split($str, 1);
        $split = [];
        $keys = [];
        $noparse = false;
        $command = false;

        $linecount = 0;

        $tmp = NULL;
        foreach($letters as $letter)
        {
            if($command && ($letter == "\n" || $letter == "\r"))
            {
                if(isset($tmp))
                {
                    array_set($split, array_merge($keys, [$linecount]), $tmp);
                    $linecount++;
                    $tmp = NULL;
                }
            }

            if($letter == "{")
            {
                $command = true;

                if(isset($tmp))
                {
                    array_set($split, array_merge($keys, [$linecount]), $tmp);
                    $linecount++;
                    $tmp = NULL;
                }
            }

            $tmp .= $letter;

            if($letter == "}")
            {
                if(is_int(strpos($tmp, "{if")) || is_int(strpos($tmp, "{foreach")) || is_int(strpos($tmp, "{cache")) || $tmp == "{noparse}")
                {
                    if(!$noparse)
                    {
                        $linecount += 1;
                        $keys[] = $linecount;
                    }

                    $linecount += 1;

                    array_set($split, array_merge($keys, [$linecount]), $tmp);
                    $linecount++;

                    if($tmp == "{noparse}")
                    {
                        $noparse = true;
                    }
                }
                elseif(($tmp == "{/foreach}" || $tmp == "{/if}" || $tmp == "{/cache}") && !$noparse)
                {
                    array_pop($keys);
                }
                elseif($tmp == "{/noparse}")
                {
                    $noparse = false;
                    array_pop($keys);
                }
                else
                {
                    array_set($split, array_merge($keys, [$linecount]), $tmp);
                    $linecount++;
                }

                $command = false;
                $tmp = NULL;
            }
        }

        array_set($split, array_merge($keys, [$linecount]), $tmp);

        return $split;
    }

    /**
     * @param array $arr
     * @param array $vars
     *
     * @return string
     * @throws Exception\TemplateNotFoundException
     * @throws ParseException
     * @throws \App\Core\Exception\FileNotFoundException
     */
    protected function parseSplit($arr, $vars = [])
    {
        $output = "";

        foreach($arr as $linecount => $line)
        {
            if(is_array($line))
            {
                $command = reset($line);
                $commandKey = key($line);

                if($command == "{noparse}")
                {
                    unset($line[$commandKey]);

                    foreach($line as $v)
                    {
                        $output .= $v;
                    }
                }
                elseif(starts_with($command, "{foreach"))
                {
                    unset($line[$commandKey]);

                    $loop = $this->parseLoop($command, $vars);

                    foreach($loop["var"] as $k => $v)
                    {
                        $tmpVar = $vars;
                        if(isset($loop["keyname"]))
                        {
                            $tmpVar[$loop["keyname"]] = $k;
                        }
                        $tmpVar[$loop["valuename"]] = $v;

                        $output .= $this->parseSplit($line, $tmpVar);
                    }
                }
                elseif(starts_with($command, "{if"))
                {
                    unset($line[$commandKey]);

                    $tmpSplit = [];
                    $condition = $this->parseCondition($command, $vars) ? true : false;

                    foreach($line as $value)
                    {
                        if(!$condition && count($tmpSplit) > 0)
                        {
                            break;
                        }

                        if($value == "{else}")
                        {
                            $condition = $condition ? false : true;
                        }
                        elseif(is_string($value) && starts_with($value, "{elseif"))
                        {
                            $condition = ($this->parseCondition($value, $vars) && !$condition) ? true : false;
                        }
                        else
                        {
                            if($condition)
                            {
                                $tmpSplit[] = $value;
                            }
                        }
                    }

                    $output .= $this->parseSplit($tmpSplit, $vars);
                }
                elseif(starts_with($command, "{cache"))
                {
                    if(!isset($this->identifer))
                    {
                        throw new ParseException("could not cache part because template file could not identified");
                    }

                    $duration = str_replace(["{cache", "}", " "], "", $command);

                    if(!is_numeric($duration) || intval($duration) != $duration)
                    {
                        throw new ParseException("invalid cache duration '".$duration."'");
                    }

                    if($this->theme->cache()->is_cached($linecount, $this->identifer))
                    {

                        $cachedData = $this->theme->cache()->retrieve($linecount, $this->identifer);

                        if($cachedData !== NULL)
                        {
                            $output .= $cachedData;

                            continue;
                        }
                    }

                    unset($line[$commandKey]);

                    $tmpoutput = $this->parseSplit($line, $vars);
                    $this->theme->cache()->store($linecount, $tmpoutput, time() + $duration, $this->identifer);

                    $output .= $tmpoutput;
                }
                else
                {
                    $output .= $this->parseSplit($line, $vars);
                }
            }
            else
            {
                $output .= ($line{0} == "{" && substr($line, -1, 1) == "}") ? $this->parseLine($line, $vars) : $line;
            }
        }

        return $output;
    }

    /**
     * @param string $str
     * @param array $vars
     *
     * @return string
     * @throws Exception\TemplateNotFoundException
     * @throws ParseException
     * @throws \App\Core\Exception\FileNotFoundException
     */
    protected function parseLine($str, $vars = [])
    {
        $line = trim($str, "{} ");

        if($line[0] == "$")
        {
            $letters = str_split(substr($line, 1));
            $parts = [""];
            $key = 0;
            $brackets = 0;
            $quotation = false;

            foreach($letters as $i => $letter)
            {
                if($letter == "(" && $quotation === false)
                {
                    $brackets++;
                }
                elseif($letter == ")" && $quotation === false)
                {
                    $brackets--;
                }
                elseif($letter == '"' || $letter == "'")
                {
                    if($quotation === false)
                    {
                        $quotation = $letter;
                    }
                    elseif($quotation == $letter)
                    {
                        $quotation = false;
                    }
                }
                elseif($letter == "." && $brackets == 0 && $quotation === false)
                {
                    $key++;
                    $parts[$key] = "";
                }
                elseif($letter == "-" && $letters[$i + 1] == ">" && $brackets == 0 && $quotation === false)
                {
                    $key++;
                    $parts[$key] = "";
                }

                $parts[$key] .= $letter;
            }

            $var = $this->getVar($parts[0], $vars);

            foreach($parts as $part)
            {
                if($part[0] == ".")
                {
                    if(!is_array($var))
                    {
                        $var = NULL;
                        break;
                    }

                    $var = $var[substr($part, 1)];
                }
                elseif($part[0] == "-" && $part[1] == ">")
                {
                    if(!is_object($var))
                    {
                        throw new ParseException("none object method call");
                    }

                    if(is_int(strpos($part, "(")))
                    {
                        $fn = $this->splitFunction(substr($part, 2));
                        if(method_exists($var, $fn["name"]))
                        {
                            $params = [];
                            foreach($fn["params"] as $a)
                            {
                                $params[] = $this->parseLine($a, $vars);
                            }

                            $var = call_user_func_array([$var, $fn["name"]], $params);
                        }
                        else
                        {
                            throw new ParseException("method ".$fn["name"]." does not exist");
                        }
                    }
                    else
                    {
                        $attrname = substr($part, 2);
                        $var = $var->$attrname;
                    }
                }
            }

            return $var;
        }
        elseif($line[0] == '"' && $line[strlen($line) - 1] == '"')
        {
            return trim($line, '"');
        }
        elseif($line[0] == "'" && $line[strlen($line) - 1] == "'")
        {
            return trim($line, "'");
        }
        elseif(starts_with($line, 'includetemplate="') && ends_with($line, '"'))
        {
            $tmpVars = $this->vars;
            foreach($vars as $k => $v)
            {
                $tmpVars[$k] = $v;
            }

            $tpl = $this->theme->draw(str_replace('includetemplate="', "", substr($line, 0, -1)))->assignVar($tmpVars);
            foreach($this->funcs as $name => $closure)
            {
                $this->assignFunction($name, $closure);
            }

            return $tpl->parse();
        }
        elseif(is_int(strpos($line, "(")) && is_int(strpos($line, ")")))
        {
            $fn = $this->splitFunction($line);
            $params = [];
            foreach($fn["params"] as $a)
            {
                $params[] = $this->parseLine($a, $vars);
            }

            return $this->getFunction($fn["name"], $params);
        }
        elseif(is_numeric($line))
        {
            return $line;
        }
        elseif($line == "true")
        {
            return true;
        }
        elseif($line == "false")
        {
            return false;
        }
        else
        {
            throw new ParseException("invalid command '".$line."'");
        }
    }

    /**
     * @param $str
     *
     * @return array
     */
    protected function parseTuple($str)
    {
        $letters = str_split($str, 1);

        $brackets = 0;
        $quotation = false;
        $args = [];

        $tmp = NULL;
        foreach($letters as $letter)
        {
            if($quotation === false && ($letter == '"' || $letter == "'"))
            {
                $quotation = $letter;
            }

            if($quotation !== false)
            {
                if($quotation == $letter)
                {
                    $quotation = false;
                }

                $tmp .= $letter;
                continue;
            }

            if($letter == " ")
            {
                continue;
            }
            elseif($letter == "(")
            {
                if($brackets >= 1)
                {
                    $tmp .= $letter;
                }
                $brackets++;
            }
            elseif($letter == ")")
            {
                if($brackets > 1)
                {
                    $tmp .= $letter;
                }
                elseif($brackets == 1)
                {
                    $args[] = $tmp;
                    $tmp = NULL;

                    return array_clear($args);
                }

                $brackets--;
            }
            elseif($letter == ",")
            {
                if($brackets == 1)
                {
                    $args[] = $tmp;
                    $tmp = NULL;
                }
                else
                {
                    $tmp .= $letter;
                }
            }
            else
            {
                $tmp .= $letter;
            }
        }

        return array_clear($args);
    }

    /**
     * @param string $str
     * @param array $vars
     *
     * @return bool
     * @throws Exception\TemplateNotFoundException
     * @throws ParseException
     * @throws \App\Core\Exception\FileNotFoundException
     */
    protected function parseCondition($str, $vars = [])
    {
        $line = str_replace(["{if", "{elseif"], "", trim($str, "} "));
        $operators = ['==', '!=', '>=', '<=', '<', '>'];

        $split = $this->splitLine($line);

        $operator = NULL;
        $compare_1 = NULL;
        $compare_2 = NULL;
        $glue = NULL;

        $result = false;
        $lastkey = count($split) - 1;

        foreach($split as $key => $part)
        {
            if(strlen($part) <= 2 && is_int(array_search($part, $operators)))
            {
                $operator = $part;
                continue;
            }

            if($part != "||" && $part != "&&")
            {
                if(!isset($compare_1))
                {
                    $compare_1 = $part;
                }
                elseif(!isset($compare_2))
                {
                    $compare_2 = $part;
                }
                else
                {
                    throw new ParseException("invalid condition '".$line."'");
                }
            }

            if($part == "||" || $part == "&&" || $key == $lastkey)
            {
                if(!isset($compare_1))
                {
                    throw new ParseException("invalid condition '".$line."'");
                }

                if(!isset($operator) || !isset($compare_2))
                {
                    if(!isset($glue))
                    {
                        $result = $this->parseLine($compare_1, $vars) ? true : false;
                    }
                    else
                    {
                        $result = $glue == "or" ? $result || $this->parseLine($compare_1, $vars) : $result && $this->parseLine($compare_1, $vars);
                    }
                }
                else
                {
                    if(!isset($glue))
                    {
                        $result = $this->compare($compare_1, $compare_2, $operator, $vars);
                    }
                    else
                    {
                        $result = $glue == "or" ? $result || $this->compare($compare_1, $compare_2, $operator, $vars) : $result && $this->compare($compare_1, $compare_2, $operator, $vars);
                    }
                }

                $operator = NULL;
                $compare_1 = NULL;
                $compare_2 = NULL;

                $glue = ($part == "||") ? "or" : "and";
                continue;
            }
        }

        return $result;
    }

    /**
     * @param $str
     * @param array $vars
     *
     * @return array
     * @throws Exception\TemplateNotFoundException
     * @throws ParseException
     * @throws \App\Core\Exception\FileNotFoundException
     */
    function parseLoop($str, $vars = [])
    {
        $line = str_replace("{foreach", "", rtrim($str, "} "));
        $split = $this->splitLine($line);

        if($split[1] != "as")
        {
            throw new ParseException("invalid loop '".$line."'");
        }

        if(count($split) == 5 && $split[3] == "=>")
        {
            $keyname = ltrim($split[2], "$");
            $valuename = ltrim($split[4], "$");
        }
        else
        {
            $keyname = NULL;
            $valuename = ltrim($split[2], "$");
        }

        $var = $this->parseLine($split[0], $vars);

        if(!is_array($var) && !is_object($var))
        {
            $var = [];
        }

        return ["var" => $var, "keyname" => $keyname, "valuename" => $valuename];
    }

    /**
     * @param $str
     *
     * @return array
     */
    protected function splitFunction($str)
    {
        $name = "";
        $count = 0;
        foreach(str_split($str) as $letter)
        {
            if($letter == " ")
            {
                continue;
            }
            if($letter == "(")
            {
                return ["name" => $name, "params" => $this->parseTuple(substr($str, $count))];
            }

            $name .= $letter;
            $count++;
        }

        return [];
    }

    /**
     * @param $str
     *
     * @return array
     */
    protected function splitLine($str)
    {
        $line = trim($str, "{} ");

        $quotation = false;
        $brackets = 0;
        $tmp = "";

        $split = [];
        $letters = str_split($line);

        foreach($letters as $letter)
        {
            if($quotation === false && $letter == " " && $brackets == 0)
            {
                if($tmp == "")
                {
                    continue;
                }

                $split[] = $tmp;
                $tmp = "";
                continue;
            }

            if($quotation === false && ($letter == '"' || $letter == "'"))
            {
                $quotation = $letter;
            }
            elseif($quotation !== false && $letter == $quotation)
            {
                $quotation = false;
            }
            elseif($quotation === false && $letter == "(")
            {
                $brackets++;
            }
            elseif($quotation === false && $letter == ")")
            {
                $brackets--;
            }

            $tmp .= $letter;
        }

        if($tmp !== "")
        {
            $split[] = $tmp;
        }

        return $split;
    }

    /**
     * @param array $key
     * @param array $vars
     *
     * @return mixed
     */
    protected function getVar($key, $vars = [])
    {
        $tmpVars = $this->vars;
        foreach($vars as $k => $v)
        {
            $tmpVars[$k] = $v;
        }

        return array_get($tmpVars, $key);
    }

    /**
     * @param string $name
     * @param array $args
     *
     * @return mixed
     *
     * @throws ParseException
     */
    protected function getFunction($name, $args)
    {
        if(!is_callable($this->funcs[$name]))
        {
            throw new ParseException("function '".$name."' doesnt exists");
        }

        $closureargs = [$this->args];

        foreach($args as $value)
        {
            $closureargs[] = $value;
        }

        if(is_array($this->funcs[$name]))
        {
            $f = new \ReflectionMethod($this->funcs[$name][0], $this->funcs[$name][1]);
        }
        else
        {
            $f = new \ReflectionFunction($this->funcs[$name]);
        }

        if(count($args) < ($f->getNumberOfRequiredParameters() - 1))
        {
            throw new ParseException("function '".$name."' exspects '".($f->getNumberOfParameters() - 1)."' parameters. You just entered '".count($args)."'");
        }

        return call_user_func_array($this->funcs[$name], $closureargs);
    }

    /**
     * @param string $line_1
     * @param string $line_2
     * @param string $operator
     * @param array $vars
     *
     * @return bool
     * @throws Exception\TemplateNotFoundException
     * @throws ParseException
     * @throws \App\Core\Exception\FileNotFoundException
     */
    protected function compare($line_1, $line_2, $operator = "==", $vars = [])
    {
        $compare_1 = $this->parseLine($line_1, $vars);
        $compare_2 = $this->parseLine($line_2, $vars);
        $result_bool = false;

        if($operator == '==')
        {
            $result_bool = $compare_1 == $compare_2;
        }
        elseif($operator == '!=')
        {
            $result_bool = $compare_1 != $compare_2;
        }
        elseif($operator == '>=')
        {
            $result_bool = $compare_1 >= $compare_2;
        }
        elseif($operator == '<=')
        {
            $result_bool = $compare_1 <= $compare_2;
        }
        elseif($operator == '>')
        {
            $result_bool = $compare_1 > $compare_2;
        }
        elseif($operator == '<')
        {
            $result_bool = $compare_1 < $compare_2;
        }

        return $result_bool;
    }
}