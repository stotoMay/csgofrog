<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Tools;

use phpseclib\Crypt\AES;

class Crypt
{
    private $key;
    /**
     * @var AES
     */
    private $aes;

    public function __construct($key)
    {
        $this->key = $key;
    }

    public function loadKey()
    {
        if(isset($this->aes))
        {
            return;
        }

        $this->aes = new AES(2);
        $this->aes->setKey($this->key);
    }

    public function decrypt($str)
    {
        $this->loadKey();

        return $this->aes->decrypt(base64_decode($str));
    }

    public function encrypt($str)
    {
        $this->loadKey();

        return base64_encode($this->aes->encrypt($str));
    }
}