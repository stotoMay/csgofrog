<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Tools;

use \App\HTTP\Exception\UrlException;

class Request
{
    private $url;

    private $header;
    private $cookies;
    private $options;
    private $follow;

    private $executed = false;

    private $response_header;
    private $response_content;
    private $response_cookies;

    private $request_info;

    /**
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;

        $urlfragments = parse_url($url);

        $this->header = [
            "Accept" => "*/*", "Cache-Control" => "no-cache",
            "Content-Type" => "application/x-www-form-urlencoded; charset=utf-8", "Host" => $urlfragments["host"],
            "User-Agent" => "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13",
            "Accept-Language" => "en-US,en;q=0.5"
        ];
        $this->options = [];
        $this->cookies = [];
        $this->follow = true;

        $this->setOption([
            CURLOPT_RETURNTRANSFER => true, CURLOPT_HEADER => true, CURLOPT_VERBOSE => true,
            CURLOPT_FOLLOWLOCATION => false, CURLOPT_COOKIEFILE => ""
        ]);
    }

    public function __toString()
    {
        return $this->getResponseContent();
    }


    /**
     * @return string
     */
    public function getResponseContent()
    {
        return $this->execute()->response_content;
    }

    /**
     * @return array
     */
    public function getResponseCookies()
    {
        return $this->execute()->response_cookies;
    }

    /**
     * @return array
     */
    public function getResponseHeader()
    {
        return $this->execute()->response_header;
    }

    /**
     * @return string
     */
    public function getDestination()
    {
        return $this->execute()->request_info["url"];
    }

    /**
     * @return array
     */
    public function getInfo()
    {
        return $this->execute()->request_info;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->execute()->request_info["http_code"];
    }

    /**
     * @param array $cookies
     *
     * @return Request
     *
     * @throws UrlException
     */
    public function execute($cookies = [])
    {
        if(!$this->isExecuted())
        {
            $this->header["Cookie"] = $this->dumpCookies();
            $this->setOption(CURLOPT_HTTPHEADER, $this->dumpHeader());

            $stream = curl_init($this->url);

            foreach($this->options as $key => $value)
            {
                curl_setopt($stream, $key, $value);
            }

            $response = curl_exec($stream);


            if($response === false)
            {
                throw new UrlException(curl_error($stream), curl_errno($stream));
            }

            $this->request_info = curl_getinfo($stream);

            if(is_int($this->request_info["header_size"]))
            {
                $this->response_header = $this->parseHeader(substr($response, 0, $this->request_info["header_size"]));
                $this->response_content = substr($response, $this->request_info["header_size"]);
                $this->response_cookies = $this->parseCookies($this->response_header["set-cookie"]);
            }
            else
            {
                $this->response_content = $response;
            }

            $this->executed = true;

            $cookies = array_merge($cookies, $this->response_cookies);

            if(isset($this->response_header["location"]) && $this->follow)
            {
                $this->url = $this->response_header["location"];
                $this->executed = false;

                $this->setCookies(array_merge($this->cookies, $cookies));

                return $this->execute($cookies);
            }

            $this->response_header["set-cookie"] = [];
            foreach($cookies as $key => $value)
            {
                $this->response_header["set-cookie"][] = $key."=".$value.";";
            }

            $this->response_cookies = $cookies;
        }

        return $this;
    }

    /**
     * @return Request
     *
     * @throws UrlException
     */
    public function retry()
    {
        $this->clear();
        $this->execute();

        return $this;
    }

    /**
     * @return Request
     */
    public function clear()
    {
        $this->executed = false;

        return $this;
    }


    /**
     * @return bool
     */
    public function isExecuted()
    {
        return $this->executed;
    }


    /**
     * @return Request
     */
    public function disableOutput()
    {
        $this->setOption(CURLOPT_RETURNTRANSFER, false);

        return $this;
    }

    /**
     * @return Request
     */
    public function disableFollowLocation()
    {
        $this->follow = false;

        return $this;
    }


    /**
     * @param array $array
     *
     * @return Request
     */
    public function setCookies($array)
    {
        $this->cookies = array_merge($this->cookies, $array);

        return $this;
    }

    /**
     * @param string $agent
     *
     * @return Request
     */
    public function setUserAgent($agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13')
    {
        $this->header["User-Agent"] = $agent;

        return $this;
    }

    /**
     * @param array $data
     *
     * @return Request
     */
    public function setPostData($data)
    {
        $this->setOption([CURLOPT_POST => true, CURLOPT_POSTFIELDS => http_build_query($data)]);

        return $this;
    }

    /**
     * @param string $value
     *
     * @return Request
     */
    public function setReferer($value)
    {
        $this->header["Referer"] = $value;

        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     *
     * @return Request
     */
    public function setHeader($name, $value)
    {
        $this->header[$name] = $value;

        return $this;
    }

    /**
     * @param string|array $key
     * @param string $value
     *
     * @return Request
     */
    public function setOption($key, $value = NULL)
    {
        if(is_array($key))
        {
            foreach($key as $k => $v)
            {
                $this->setOption($k, $v);
            }

            return $this;
        }

        $this->options[$key] = $value;

        return $this;
    }

    private function parseCookies($arr)
    {
        $cookieList = [];

        if(!is_array($arr))
        {
            $arr = [$arr];
        }

        foreach($arr as $cookie)
        {
            $split = explode("=", $cookie);

            if(count($split) < 2)
            {
                return [];
            }

            $cookieList[trim($split[0])] = trim(rawurldecode(explode(";", $split[1])[0]));
        }

        return $cookieList;
    }

    private function dumpCookies()
    {
        $cookie_string = "";

        foreach($this->cookies as $key => $value)
        {
            $cookie_string .= $key."=".rawurlencode($value).";";
        }

        return $cookie_string;
    }

    /**
     * @param string $str
     *
     * @return array
     */
    private function parseHeader($str)
    {
        $headers = [];
        $key = '';

        foreach(explode("\n", $str) as $i => $h)
        {
            $h = explode(':', $h, 2);

            if(isset($h[1]))
            {
                if(!isset($headers[$h[0]]))
                {
                    $headers[$h[0]] = trim($h[1]);
                }
                elseif(is_array($headers[$h[0]]))
                {
                    $headers[$h[0]] = array_merge($headers[$h[0]], [trim($h[1])]);
                }
                else
                {
                    $headers[$h[0]] = array_merge([$headers[$h[0]]], [trim($h[1])]);
                }

                $key = $h[0];
            }
            else
            {
                if(substr($h[0], 0, 1) == "\t")
                {
                    $headers[$key] .= "\r\n\t".trim($h[0]);
                }
                elseif(!$key)
                {
                    $headers[0] = trim($h[0]);
                }
            }
        }

        $result = [];
        foreach($headers as $key => $value)
        {
            $result[strtolower($key)] = $value;
        }

        return $result;
    }

    private function dumpHeader()
    {
        $headers = [];

        foreach($this->header as $key => $value)
        {
            $headers[] = $key.": ".$value;
        }

        return $headers;
    }
}