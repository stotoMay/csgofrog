<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Tools;

class GoogleAuthenticator
{
    private $secret;
    private $codelength;

    /**
     * @param string $secret
     * @param int $codelength
     */
    public function __construct($secret, $codelength = 6)
    {
        $this->secret = $secret;
        $this->codelength = $codelength;
    }

    /**
     * @param int|null $timeSlice
     *
     * @return string
     */
    public function generateCode($timeSlice = NULL)
    {
        if($timeSlice === NULL)
        {
            $timeSlice = floor(time() / 30);
        }

        $secretkey = base32_decode($this->secret);

        $time = chr(0).chr(0).chr(0).chr(0).pack('N*', $timeSlice);
        $hm = hash_hmac('SHA1', $time, $secretkey, true);
        $offset = ord(substr($hm, -1)) & 0x0F;
        $hashpart = substr($hm, $offset, 4);

        $value = unpack('N', $hashpart);
        $value = $value[1];
        $value = $value & 0x7FFFFFFF;

        $modulo = pow(10, $this->codelength);

        return str_pad($value % $modulo, $this->codelength, '0', STR_PAD_LEFT);
    }

    /**
     * @param string $name
     * @param string $title
     * @param array $params
     *
     * @return string
     */
    public function getQRCodeGoogleUrl($name, $title = NULL, $params = [])
    {
        $width = !empty($params['width']) && (int)$params['width'] > 0 ? (int)$params['width'] : 200;
        $height = !empty($params['height']) && (int)$params['height'] > 0 ? (int)$params['height'] : 200;
        $level = !empty($params['level']) && array_search($params['level'], ['L', 'M', 'Q', 'H']) !== false ? $params['level'] : 'M';

        $urlencoded = urlencode('otpauth://totp/'.$name.'?secret='.$this->secret.'');
        if(isset($title))
        {
            $urlencoded .= urlencode('&issuer='.urlencode($title));
        }

        return 'https://chart.googleapis.com/chart?chs='.$width.'x'.$height.'&chld='.$level.'|0&cht=qr&chl='.$urlencoded.'';
    }

    /**
     * @param string $code
     * @param int $discrepancy
     * @param int $currentTimeSlice
     *
     * @return bool
     */
    public function verifyCode($code, $discrepancy = 1, $currentTimeSlice = NULL)
    {
        if($currentTimeSlice === NULL)
        {
            $currentTimeSlice = floor(time() / 30);
        }

        if(strlen($code) != 6)
        {
            return false;
        }

        for($i = -$discrepancy; $i <= $discrepancy; ++$i)
        {
            $calculatedCode = $this->generateCode($currentTimeSlice + $i);
            if($this->timingSafeEquals($calculatedCode, $code))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $safeString
     * @param string $userString
     *
     * @return bool
     */
    private function timingSafeEquals($safeString, $userString)
    {
        $safeLen = strlen($safeString);
        $userLen = strlen($userString);

        if($userLen != $safeLen)
        {
            return false;
        }

        $result = 0;

        for($i = 0; $i < $userLen; ++$i)
        {
            $result |= (ord($safeString[$i]) ^ ord($userString[$i]));
        }

        return $result === 0;
    }

    /**
     * @param int $secretLength
     *
     * @return string
     */
    public static function generateSecret($secretLength = 32)
    {
        $validChars = [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
            'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
            'Y', 'Z', '2', '3', '4', '5', '6', '7',
            '=',
        ];


        $secret = '';
        $rnd = false;

        if(function_exists('random_bytes'))
        {
            $rnd = random_bytes($secretLength);
        }
        elseif(function_exists('mcrypt_create_iv'))
        {
            $rnd = mcrypt_create_iv($secretLength, MCRYPT_DEV_URANDOM);
        }
        elseif(function_exists('openssl_random_pseudo_bytes'))
        {
            $rnd = openssl_random_pseudo_bytes($secretLength, $cryptoStrong);
            if(!$cryptoStrong)
            {
                $rnd = false;
            }
        }
        if($rnd !== false)
        {
            for($i = 0; $i < $secretLength; ++$i)
            {
                $secret .= $validChars[ord($rnd[$i]) & 31];
            }
        }
        else
        {
            return NULL;
        }

        return $secret;
    }
}
