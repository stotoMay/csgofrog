<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Tools;

class ItemList implements \Iterator
{
    private $items;
    private $current;
    private $length;

    public function __construct()
    {
        $this->current = 0;
        $this->items = [];
        $this->length = 0;
    }

    /**
     * @param mixed $obj
     */
    public function append($obj)
    {
        $this->items[] = $obj;
        $this->length++;
    }

    /**
     * @param mixed $a
     */
    public function prepend($a)
    {
        $this->items = array_merge([$a], $this->items);
        $this->length++;
    }

    public function length()
    {
        return count($this->items);
    }

    public function removeLast()
    {
        array_pop($this->items);
        $this->length--;
    }

    public function removeFirst()
    {
        array_shift($this->items);
        $this->length--;
    }

    public function removeEveryItem()
    {
        $this->items = [];
        $this->length = 0;
    }

    public function remove($key)
    {
        unset($this->items[$key]);
        $this->items = array_values($this->items);
        $this->length--;
    }

    public function getFirstItem()
    {
        if(count($this->items) == 0)
        {
            return NULL;
        }

        return array_first($this->items);
    }

    public function getLastItem()
    {
        if(count($this->items) == 0)
        {
            return NULL;
        }

        return array_last($this->items);
    }


    public function rewind()
    {
        $this->current = 0;
    }

    public function current()
    {
        return $this->items[$this->current];
    }

    public function key()
    {
        return $this->current;
    }

    public function next()
    {
        if($this->current < $this->length)
        {
            $this->current++;
        }

        return $this->current();
    }

    public function valid()
    {
        return $this->current < $this->length;
    }
}