<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Tools;

use App\Core\App;

class Task
{
    private $app;
    private $cache;

    /**
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * @param int $runtime
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function run($runtime = NULL)
    {
        $taskquery = $this->app->database()->select("task")->where(["expire[<=]" => time()])->fetchAssoc();

        $time = 0;
        foreach($taskquery as $task)
        {
            $time += $task["runtime"];
            $this->cache[$task["id"]] = $task;

            if(is_int($runtime))
            {
                if($time <= $runtime)
                {
                    $this->execute($task["id"]);
                }
                else
                {
                    $time -= $task["runtime"];
                }
            }
            else
            {
                $this->execute($task["id"]);
            }
        }
    }

    /**
     * @param string $name
     * @param array $args
     * @param int $runtime
     * @param int $time
     * @param int $start
     * @param bool $repeat
     * @param string $component
     *
     * @return int
     * @throws \App\Database\Exception\ParseException
     */
    public function add($name, $args = NULL, $runtime = 1, $time = 3600, $start = NULL, $repeat = false, $component = NULL)
    {
        if(!isset($start))
        {
            $start = time();
        }

        return $this->app->database()->insert("task")->set([
            "name" => $name, "args" => base64_encode(serialize($args)), "created" => time(),
            "intervall" => $time, "expire" => $start, "runtime" => $runtime, "repeat" => $repeat,
            "componentID" => $component
        ])->getID();
    }

    /**
     * @param int $taskID
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function execute($taskID)
    {
        if(!isset($this->cache[$taskID]))
        {
            $this->cache[$taskID] = $this->app->database()->select("task")->where(["id" => $taskID])->fetchRow();
        }

        if(is_array($this->cache[$taskID]) && count($this->cache[$taskID]) > 0)
        {
            $task = $this->cache[$taskID];

            $this->app->component()
                ->task($task["component"], $task["name"], unserialize(base64_decode($task["args"])));

            if($task["repeat"] == 1)
            {
                $this->app->database()->update("task")->set(["expire" => time() + $task["intervall"]])
                    ->where(["id" => $taskID]);
            }
        }
    }

    /**
     * @param int $taskID
     *
     * @return bool
     * @throws \App\Database\Exception\ParseException
     */
    public function remove($taskID)
    {
        $this->app->database()->delete("task")->where(["id" => $taskID]);

        return true;
    }
}