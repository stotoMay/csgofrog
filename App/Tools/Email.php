<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Tools;

class Email
{
    private $request;
    /**
     * @var int $_wrap
     */
    protected $_wrap = 78;
    /**
     * @var array $_to
     */
    protected $_to = [];
    /**
     * @var string $_subject
     */
    protected $_subject;
    /**
     * @var string $_message
     */
    protected $_message;
    /**
     * @var array $_headers
     */
    protected $_headers = [];
    /**
     * @var string $_parameters
     */
    protected $_params;
    /**
     * @var array $_attachments
     */
    protected $_attachments = [];
    /**
     * @var string $_uid
     */
    protected $_uid;


    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->_to = [];
        $this->_headers = [];
        $this->_subject = NULL;
        $this->_message = NULL;
        $this->_wrap = 78;
        $this->_params = NULL;
        $this->_attachments = [];
        $this->_uid = md5(uniqid(time()));
    }

    /**
     * @param string $email
     * @param string $name
     *
     * @return Email
     */
    public function setTo($email, $name)
    {
        $this->_to[] = $this->formatHeader((string)$email, (string)$name);

        return $this;
    }

    /**
     * @return array
     */
    public function getTo()
    {
        return $this->_to;
    }

    /**
     * @param string $subject
     *
     * @return Email
     */
    public function setSubject($subject)
    {
        $this->_subject = $this->encodeUtf8(
            $this->filterOther((string)$subject)
        );

        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->_subject;
    }

    /**
     * @param string $message
     *
     * @return Email
     */
    public function setTextMessage($message)
    {
        $this->_message = str_replace("\n.", "\n..", (string)$message);

        return $this;
    }

    public function setHTMLMessage($message)
    {
        $this->addGenericHeader('Content-Type', 'text/html; charset="utf-8"');
        $this->_message = str_replace("\n.", "\n..", (string)$message);

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->_message;
    }

    /**
     * @param string $path
     * @param string $filename
     *
     * @return Email
     */
    public function addAttachment($path, $filename = NULL)
    {
        $filename = empty($filename) ? basename($path) : $filename;
        $this->_attachments[] = [
            'path' => $path,
            'file' => $filename,
            'data' => $this->getAttachmentData($path)
        ];

        return $this;
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function getAttachmentData($path)
    {
        $filesize = filesize($path);
        $handle = fopen($path, "r");
        $attachment = fread($handle, $filesize);
        fclose($handle);

        return chunk_split(base64_encode($attachment));
    }

    /**
     * @param string $email
     * @param string $name
     *
     * @return Email
     */
    public function setFrom($email, $name)
    {
        $this->addMailHeader('From', (string)$email, (string)$name);

        return $this;
    }

    /**
     * @param string $header
     * @param string $email
     * @param string $name
     *
     * @return Email
     */
    public function addMailHeader($header, $email = NULL, $name = NULL)
    {
        $address = $this->formatHeader((string)$email, (string)$name);
        $this->_headers[] = sprintf('%s: %s', (string)$header, $address);

        return $this;
    }

    /**
     * @param string $header
     * @param mixed $value
     *
     * @return Email
     */
    public function addGenericHeader($header, $value)
    {
        $this->_headers[] = sprintf(
            '%s: %s',
            (string)$header,
            (string)$value
        );

        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->_headers;
    }

    /**
     * @param string $additionalParameters
     *
     * @return Email
     */
    public function setParameters($additionalParameters)
    {
        $this->_params = (string)$additionalParameters;

        return $this;
    }

    /**
     * @return string
     */
    public function getParameters()
    {
        return $this->_params;
    }

    /**
     * @param int $wrap
     *
     * @return Email
     */
    public function setWrap($wrap = 78)
    {
        $wrap = (int)$wrap;
        if($wrap < 1)
        {
            $wrap = 78;
        }
        $this->_wrap = $wrap;

        return $this;
    }

    /**
     * @return int
     */
    public function getWrap()
    {
        return $this->_wrap;
    }

    /**
     * @return bool
     */
    public function hasAttachments()
    {
        return !empty($this->_attachments);
    }

    /**
     * @return string
     */
    public function assembleAttachmentHeaders()
    {
        $head = [];
        $head[] = "MIME-Version: 1.0";
        $head[] = "Content-Type: multipart/mixed; boundary=\"{$this->_uid}\"";

        return join(PHP_EOL, $head);
    }

    /**
     * @return string
     */
    public function assembleAttachmentBody()
    {
        $body = [];
        $body[] = "This is a multi-part message in MIME format.";
        $body[] = "--{$this->_uid}";
        $body[] = "Content-type:text/html; charset=\"utf-8\"";
        $body[] = "Content-Transfer-Encoding: 7bit";
        $body[] = "";
        $body[] = $this->_message;
        $body[] = "";
        $body[] = "--{$this->_uid}";
        foreach($this->_attachments as $attachment)
        {
            $body[] = $this->getAttachmentMimeTemplate($attachment);
        }

        return implode(PHP_EOL, $body);
    }

    /**
     * @param array $attachment
     *
     * @return string
     */
    public function getAttachmentMimeTemplate($attachment)
    {
        $file = $attachment['file'];
        $data = $attachment['data'];
        $head = [];
        $head[] = "Content-Type: application/octet-stream; name=\"{$file}\"";
        $head[] = "Content-Transfer-Encoding: base64";
        $head[] = "Content-Disposition: attachment; filename=\"{$file}\"";
        $head[] = "";
        $head[] = $data;
        $head[] = "";
        $head[] = "--{$this->_uid}";

        return implode(PHP_EOL, $head);
    }

    /**
     * @return boolean
     *
     * @throws \RuntimeException
     */
    public function send()
    {
        $to = $this->getToForSend();
        $headers = $this->getHeadersForSend();
        if(empty($to))
        {
            throw new \RuntimeException(
                'Unable to send, no To address has been set.'
            );
        }
        if($this->hasAttachments())
        {
            $message = $this->assembleAttachmentBody();
            $headers .= PHP_EOL.$this->assembleAttachmentHeaders();
        }
        else
        {
            $message = $this->getWrapMessage();
        }

        return mail($to, $this->_subject, $message, $headers, $this->_params);
    }

    /**
     * @param string $email
     * @param string $name
     *
     * @return string
     */
    public function formatHeader($email, $name = NULL)
    {
        $email = $this->filterEmail($email);
        if(empty($name))
        {
            return $email;
        }
        $name = $this->encodeUtf8($this->filterName($name));

        return sprintf('"%s" <%s>', $name, $email);
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public function encodeUtf8($value)
    {
        $value = trim($value);
        if(preg_match('/(\s)/', $value))
        {
            return $this->encodeUtf8Words($value);
        }

        return $this->encodeUtf8Word($value);
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public function encodeUtf8Word($value)
    {
        return sprintf('=?UTF-8?B?%s?=', base64_encode($value));
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public function encodeUtf8Words($value)
    {
        $words = explode(' ', $value);
        $encoded = [];
        foreach($words as $word)
        {
            $encoded[] = $this->encodeUtf8Word($word);
        }

        return join($this->encodeUtf8Word(' '), $encoded);
    }

    /**
     * @param string $email
     *
     * @return string
     */
    public function filterEmail($email)
    {
        $rule = [
            "\r" => '',
            "\n" => '',
            "\t" => '',
            '"' => '',
            ',' => '',
            '<' => '',
            '>' => ''
        ];
        $email = strtr($email, $rule);
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);

        return $email;
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public function filterName($name)
    {
        $rule = [
            "\r" => '',
            "\n" => '',
            "\t" => '',
            '"' => "'",
            '<' => '[',
            '>' => ']',
        ];
        $filtered = filter_var(
            $name,
            FILTER_SANITIZE_STRING,
            FILTER_FLAG_NO_ENCODE_QUOTES
        );

        return trim(strtr($filtered, $rule));
    }

    /**
     * @param string $data
     *
     * @return string
     */
    public function filterOther($data)
    {
        return filter_var($data, FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW);
    }

    /**
     * @return string
     */
    public function getHeadersForSend()
    {
        if(empty($this->_headers))
        {
            return '';
        }

        return join(PHP_EOL, $this->_headers);
    }

    /**
     * @return string
     */
    public function getToForSend()
    {
        if(empty($this->_to))
        {
            return '';
        }

        return join(', ', $this->_to);
    }

    /**
     * @return string
     */
    public function getWrapMessage()
    {
        return wordwrap($this->_message, $this->_wrap);
    }
}