<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Component;

use App\Component\Exception\LoadException;
use App\Core\App;

class Component
{
    private $app;
    private $folder;

    private $componentList;
    private $hookList;
    private $info;

    /**
     * @param string $folder
     * @param App $app
     *
     * @throws LoadException
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\Core\Exception\FolderNotFoundException
     * @throws \App\Core\Exception\InvalidArgumentException
     * @throws \App\Language\Exception\ParseException
     */
    public function __construct($folder, App $app)
    {
        $this->app = $app;
        $this->folder = filter_folder($folder);

        if(!is_readable(base_dir($folder)))
        {
            throw new LoadException();
        }

        $folders = get_dir_folders(base_dir($folder));
        foreach($folders as $f)
        {
            $this->initComponent($f);
        }
    }

    public function __destruct()
    {
        foreach($this->componentList as $component => $class)
        {
            $this->callMethod($component, "onDisable");
        }
    }

    /**
     * @param string $component
     *
     * @throws LoadException
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\Core\Exception\InvalidArgumentException
     * @throws \App\Language\Exception\ParseException
     */
    public function initComponent($component)
    {
//        var_dump($this->folder,$component);exit;
        $mainclass = '\\'.convert_folder_namespace($this->folder).'\\'.$component.'\\'.$component;
        if(!class_exists($mainclass))
        {
            throw new LoadException($mainclass." not found");
        }

        if(get_parent_class($mainclass) == 'App\\Component\\Parent\\Component' && method_exists($mainclass, "info"))
        {
            $info = $mainclass::info();

            /**
             * @var Parent\Component $c
             */
            $c = new $mainclass($this->app);

            $methodBlacklist = ["info", "onEnable", "onDisable", "__construct"];

            foreach(get_class_methods($c) as $method)
            {
                if(!is_int(array_search($method, $methodBlacklist)) && $method{0} != "_")
                {
                    $this->hookList[$method][] = $component;
                }
            }

            $info["setting"] = is_array($info["setting"]) ? $info["setting"] : [];
            $info["permission"] = is_array($info["permission"]) ? $info["permission"] : [];

            foreach($info["setting"] as $setting => $value)
            {
                if(is_array($value))
                {
                    $this->app->setting()->registerSetting($setting, $value[0], "component", $component, $value[1]);
                }
                else
                {
                    $this->app->setting()->registerSetting($setting, $value, "component", $component);
                }
            }

            foreach($info["permission"] as $permission => $value)
            {
                if(is_array($value))
                {
                    $this->app->permission()
                        ->registerPermission($permission, $value[0], "component", $component, $value[1]);
                }
                else
                {
                    $this->app->permission()->registerPermission($permission, $value, "component", $component);
                }
            }

            $this->componentList[$component] = $c;
            $this->callMethod($component, "onEnable");

            $langfile = $this->getLanguageFile($component);
            if(!empty($langfile))
            {
                $this->app->language()->mergeFile(base_dir($langfile));
            }
        }
        else
        {
            throw new LoadException($mainclass." is not valid");
        }
    }

    /**
     * @param string $component
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function resetComponent($component)
    {
        $this->app->database()->delete("permission")->where(["type" => "component", "typeID" => $component]);

        $this->app->database()->delete("setting")->where(["type" => "component", "typeID" => $component]);

        $this->callMethod($component, "onClear");
    }

    /**
     * @param string $name
     * @param array $args
     *
     * @return array
     */
    public function setHook($name, &$args = [])
    {
        if(!is_array($this->hookList[$name]))
        {
            return [];
        }

        $result = [];

        foreach($this->hookList[$name] as $component)
        {
            $result[] = $this->callMethod($component, $name, $args);
        }

        return $result;
    }

    /**
     * @param string $component
     * @param string $name
     * @param array $args
     */
    public function task($component, $name, $args = [])
    {
        $args = [$name, $args];
        $this->callMethod($component, "task", $args);
    }

    /**
     * @param string $component
     *
     * @return array
     * @throws \App\Core\Exception\InvalidArgumentException
     */
    public function getInfo($component)
    {
        if(isset($this->info[$component]))
        {
            return $this->info[$component];
        }

        $mainclass = convert_folder_namespace($this->folder).'\\'.$component.'\\'.$component;

        if(class_exists($mainclass) && method_exists($mainclass, "info"))
        {
            $info = $mainclass::info();

            if(is_array($info))
            {
                $this->info[$component] = $info;

                return $info;
            }
        }

        $this->info[$component] = [];

        return [];
    }

    /**
     * @param string $component
     * @param string $language
     *
     * @return string
     * @throws \App\Core\Exception\InvalidArgumentException
     */
    public function getLanguageFile($component, $language = NULL)
    {
        $info = $this->getInfo($component);

        if(!is_array($info["language"]) || count($info) == 0)
        {
            return NULL;
        }

        $selected = $language;
        if(!isset($language))
        {
            $selected = $this->app->setting()->get("selected", "app", "language");
        }

        foreach($info["language"] as $lang => $file)
        {
            if($lang == $selected)
            {
                return $this->folder.$component."/".$file;
            }
        }

        if(!isset($language))
        {
            return $this->folder.$component."/".array_first($info["language"]);
        }
        else
        {
            return NULL;
        }
    }

    /**
     * @return array
     */
    public function getComponentList()
    {
        $componentList = [];
        foreach($this->componentList as $component => $object)
        {
            $componentList[] = $component;
        }

        return $componentList;
    }

    /**
     * @param string $component
     *
     * @return bool
     */
    public function isLoaded($component)
    {
        return is_object($this->componentList[$component]);
    }

    /**
     * @param string $component
     *
     * @return bool
     * @throws \App\Core\Exception\InvalidArgumentException
     */
    public function hasSetting($component)
    {
        $info = $this->getInfo($component);

        if(is_array($info["setting"]) && count($info["setting"]) > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @param string $component
     *
     * @return bool
     * @throws \App\Core\Exception\InvalidArgumentException
     */
    public function hasPermission($component)
    {
        $info = $this->getInfo($component);

        if(is_array($info["permission"]) && count($info["permission"]) > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @param string $component
     * @param string $method
     * @param array $args
     *
     * @return mixed
     */
    private function callMethod($component, $method, &$args = [])
    {
        if(!is_array($args))
        {
            return NULL;
        }

        if(is_object($this->componentList[$component]) && method_exists($this->componentList[$component], $method))
        {
            $m = new \ReflectionMethod($this->componentList[$component], $method);
            while(count($args) <= $m->getNumberOfRequiredParameters())
            {
                $args[] = NULL;
            }

            return call_user_func_array([$this->componentList[$component], $method], $args);
        }

        return NULL;
    }
}