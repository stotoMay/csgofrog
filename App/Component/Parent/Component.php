<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\Component\Parent;

use App\Core\App;

abstract class Component
{
    protected $app;

    /**
     * @param App $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * @param string $name
     *
     * @return string|int
     */
    public function getSetting($name)
    {
        return $this->app->setting()->getComponent(array_last(array_clear(explode('\\', get_class($this)))), $name);
    }
}