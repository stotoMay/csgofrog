<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\HTTP;

use App\Theme\Template;

class Response
{
    private $request;

    private $str;

    private $header;
    private $status;

    /**
     * @param string|Template $str
     * @param Request $request
     *
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\Theme\Exception\ParseException
     * @throws \App\Theme\Exception\TemplateNotFoundException
     */
    public function __construct($str, Request $request)
    {

        if(is_object($str) && is_callable([$str, "__tostring"]))
        {
            if(get_class($str) == 'App\\Theme\\Template')
            {
                $str = $str->parse();
            }
            else
            {
                $str = $str->__tostring();
            }
        }

        $this->str["html"] = ["text" => (string)$str, "content-type" => "text/html"];
        $this->request = $request;

        $this->status = 200;
        $this->header = [];
    }

    /**
     * @return string
     */
    public function output()
    {
        if(is_array($this->str[$this->request->format()]))
        {
            $page = $this->str[$this->request->format()];
        }
        else
        {
            $page = array_first($this->str);
        }

        if(!isset($this->header["content-type"]))
        {
            $this->header("Content-Type", $page["content-type"]);
        }

        foreach($this->header as $k => $v)
        {
            header($k.": ".$v);
        }

        http_response_code($this->status);
        $page = $page["text"];

        return $page;
    }

    public function __tostring()
    {
        return $this->output();
    }

    /**
     * @param string $key
     * @param string $value
     *
     * @return Response
     */
    public function header($key, $value)
    {
        $this->header[strtolower($key)] = $value;

        return $this;
    }

    /**
     * @param string $format
     * @param string $header
     * @param string $response
     *
     * @return Response
     */
    public function format($format, $header, $response)
    {
        $this->str[$format] = ["text" => $response, "content-type" => $header];

        return $this;
    }

    /**
     * @param string $mimetype
     *
     * @return Response
     */
    public function contentType($mimetype)
    {
        $this->header("Content-Type", $mimetype);

        return $this;
    }

    /**
     * @param int $code
     *
     * @return Response
     */
    public function statusCode($code)
    {
        $this->status = $code;

        return $this;
    }


    /**
     * @param string $filename
     *
     * @return Response
     */
    public function download($filename)
    {
        $this->header("Content-Disposition", 'attachment; filename="'.$filename.'"');

        return $this;
    }
}