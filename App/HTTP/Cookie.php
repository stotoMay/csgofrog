<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\HTTP;

use App\Bootstrap\Config;

class Cookie
{
    private $cookie;
    private $request;
    private $config;

    /**
     * @param array $cookie
     * @param Request $request
     * @param Config $config
     */
    public function __construct(&$cookie, Request $request, Config $config)
    {
        $this->cookie = &$cookie;
        $this->request = $request;
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function all()
    {
        if(empty($this->config->getCookiePrefix()))
        {
            return $this->cookie;
        }

        $cookies = $this->cookie;
        foreach($cookies as $key => $value)
        {
            if(starts_with($key, $this->config->getCookiePrefix()))
            {
                unset($cookies[$key]);
                $cookies[substr($key, strlen($this->config->getCookiePrefix()))] = $value;
            }
        }

        return $cookies;
    }

    /**
     * fetch and deletes it afterwards
     *
     * @param $key
     * @param null $default
     *
     * @return mixed
     */
    public function pull($key, $default = NULL)
    {
        $cookie = $this->cookie[$this->cookiename($key)];

        $this->forget($key);

        if(empty($cookie))
        {
            return $default;
        }
        else
        {
            return $cookie;
        }
    }

    /**
     * @param string $key
     * @param string $default
     *
     * @return mixed
     */
    public function get($key, $default = NULL)
    {
        if(!isset($this->cookie[$this->cookiename($key)]))
        {
            return $default;
        }
        else
        {
            return $this->cookie[$this->cookiename($key)];
        }
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function has($key)
    {
        return isset($this->cookie[$this->cookiename($key)]);
    }

    /**
     * sets the cookie
     *
     * @param string $key
     * @param mixed $value
     * @param int $time
     * @param bool $httponly
     */
    public function put($key, $value, $time = NULL, $httponly = NULL)
    {
        if($this->has($key))
        {
            $this->forget($key);
        }

        $this->cookie[$this->cookiename($key)] = $value;
        setcookie($this->cookiename($key), $value, $time, "/", NULL, NULL, $httponly);
    }

    /**
     * deletes the cookie
     *
     * @param string $key
     */
    public function forget($key)
    {
        if($this->has($key))
        {
            unset($this->cookie[$this->cookiename($key)]);
            setcookie($this->cookiename($key), "deleted", $this->request->time() - 1, "/");
        }
    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function cookiename($name)
    {
        return $this->config->getCookiePrefix().$name;
    }
}