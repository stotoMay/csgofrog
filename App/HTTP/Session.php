<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\HTTP;

use App\Bootstrap\Config;

class Session
{
    private $flashed;
    private $session;
    private $merged;

    private $request;
    private $config;
    private $ip;

    /**
     * @param array $session
     * @param Request $request
     * @param Config $config
     */
    public function __construct(&$session, Request $request, Config $config)
    {
        $this->request = $request;
        $this->config = $config;

        $this->ip = new IP(base_dir($config->getSessionDirectory()), $this->request);

        $this->session = &$session;
        $this->flashed = $session["flash"];
        $this->merged = merge_deep_array((array)$this->flashed, (array)$this->session);

        unset($session["flash"]);
    }

    /**
     * @return IP
     */
    public function ip()
    {
        return $this->ip;
    }

    public function closeSessionWrite()
    {
        session_write_close();
    }

    public function openSessionWrite()
    {
        ini_set('session.use_only_cookies', false);
        ini_set('session.use_cookies', false);
        ini_set('session.use_trans_sid', false);
        ini_set('session.cache_limiter', NULL);

        session_start();
    }

    /**
     * get session by key
     *
     * @param array|string $key
     * @param mixed $default
     *
     * @return mixed
     */
    public function get($key, $default = NULL)
    {
        if(!is_array($key))
        {
            $key = [$key];
        }

        $value = array_get($this->merged, $key);

        if(empty($value))
        {
            return $default;
        }
        else
        {
            return $value;
        }
    }

    /**
     * get all session data
     *
     * @return array
     */
    public function all()
    {
        return $this->merged;
    }

    /**
     * checks whether key exists or not
     *
     * @param string $key
     *
     * @return bool
     */
    public function has($key)
    {
        if(!is_array($key))
        {
            $key = [$key];
        }

        $value = array_get($this->merged, $key);

        return !empty($value);
    }

    /**
     * sets session
     *
     * @param array|string $key
     * @param mixed $value
     */
    public function put($key, $value)
    {
        if(!is_array($key))
        {
            $key = [$key];
        }

        array_set($this->session, $key, $value);
    }

    /**
     * gets and deletes afterwards
     *
     * @param array|string $key
     * @param mixed $default
     *
     * @return mixed
     */
    public function pull($key, $default = NULL)
    {
        if(!is_array($key))
        {
            $key = [$key];
        }

        $value = array_get($this->session, $key);
        $this->forget($key);

        if(empty($value))
        {
            return $default;
        }
        else
        {
            return $value;
        }
    }

    /**
     * deletes key
     *
     * @param array|string $key
     */
    public function forget($key)
    {
        if(!is_array($key))
        {
            $key = [$key];
        }

        array_unset($this->session, $key);
    }


    /**
     * flash data for the next pageload
     *
     * @param array|string $key
     * @param mixed $value
     */
    public function flash($key, $value)
    {
        if(!is_array($key))
        {
            $key = [$key];
        }

        $k = ["flash"];
        $key = array_merge($k, $key);

        array_set($this->session, $key, $value);
    }

    /**
     * keep data flashed
     *
     * @param array|string $keys
     */
    public function keep($keys)
    {
        if(!is_array($keys))
        {
            $keys = [$keys];
        }

        foreach($keys as $k)
        {
            $value = array_get($this->flashed, $k);

            if(!empty($value))
            {
                $this->flash($k, $this->get($k));
            }
        }
    }

    /**
     * flash the flashed data again
     */
    public function reflash()
    {
        foreach($this->flashed as $k => $v)
        {
            $this->flash($k, $v);
        }
    }
}