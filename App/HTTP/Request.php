<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\HTTP;

use App\Bootstrap\Config;
use App\Bootstrap\Factory;
use App\Router\Exception\HTTPException;
use App\Validator\Validator;

class Request
{
    private $request;
    private $client;
    private $config;

    private $cookie;
    private $session;

    private $session_name;
    private $request_verified;

    private $path;
    private $format;

    /**
     * @param array $request
     * @param array $client
     * @param array $session
     * @param array $cookie
     * @param Config $config
     */
    public function __construct($request, $client, &$session, &$cookie, Config $config)
    {
        $this->config = $config;

        $this->request = is_array($request) ? $request : [];
        $this->client = is_array($client) ? $client : [];

        if(!$this->config->isIPAccessAllowed() && $this->client["SERVER_NAME"] == $this->client["SERVER_ADDR"])
        {
            throw new HTTPException(403, "salut");
        }

        if($this->config->areXHeadersEnabled() && empty($this->client["HTTP_X_FORWARDED_FOR"]))
        {
            throw new HTTPException(403, "salut25");
        }

        $this->cookie = new Cookie($cookie, $this, $config);
        $this->session = new Session($session, $this, $config);

        $this->session_name = $config->getSessionCookieName();

        if(count($request) > 0)
        {
            $this->request_verified = $request["token"] == $this->token();

            unset($this->request["token"]);
        }

        $path = array_clear(explode("/", $client["REQUEST_URI"]));
        $path[count($path) - 1] = explode("?", array_last($path))[0];

        $last = explode(".", array_last($path));

        if(count($last) > 1)
        {
            $this->format = $last[1];
            $path[count($path) - 1] = $last[0];
        }

        $this->path = $path;
    }

    /**
     * @return Session
     */
    public function session()
    {
        return $this->session;
    }

    /**
     * @return Cookie
     */
    public function cookie()
    {
        return $this->cookie;
    }

    public function config()
    {
        return $this->config;
    }

    /**
     * @return array
     */
    public function path()
    {
        return $this->path;
    }

    /**
     * @return int
     */
    public function time()
    {
        return $this->client["REQUEST_TIME"];
    }

    /**
     * @return string
     */
    public function referer()
    {
        return $this->client["HTTP_REFERER"];
    }

    /**
     * @param string $protocol
     *
     * @return string
     */
    public function domain($protocol = NULL)
    {
        if(!isset($protocol))
        {
            $protocol = $this->isSecureUrl() ? "https" : "http";
        }

        return $protocol.'://'.$this->client["SERVER_NAME"]."/";
    }

    /**
     * @return string
     */
    public function servername()
    {
        return $this->client["SERVER_NAME"];
    }

    /**
     * @return string
     */
    public function ip()
    {
        if($this->config->areXHeadersEnabled() && isset($this->client["HTTP_X_FORWARDED_FOR"]))
        {
            return htmlspecialchars($this->client["HTTP_X_FORWARDED_FOR"]);
        }
        else
        {
            return $this->client["REMOTE_ADDR"];
        }
    }


    /**
     * @return string
     */
    public function useragent()
    {
        return $this->client["HTTP_USER_AGENT"];
    }

    /**
     * @return bool
     */
    public function isSecureUrl()
    {
        if($this->config->areXHeadersEnabled() && isset($this->client["HTTP_X_FORWARDED_PROTO"]))
        {
            return strtolower($this->client["HTTP_X_FORWARDED_PROTO"]) == "https";
        }
        else
        {
            return strtolower($this->client["REQUEST_SCHEME"]) == "https";
        }
    }

    /**
     * @return string
     */
    public function format()
    {
        return $this->format;
    }

    /**
     * @param string $name
     * @param string $default
     * @param bool $secure
     * @param Validator $validator
     *
     * @return mixed
     */
    public function input($name, $default = NULL, $secure = true, $validator = NULL)
    {
        if($secure && !$this->request_verified)
        {
            return $default;
        }

        if(is_object($validator) && get_class($validator) == 'App\\Validator\\Validator' && !$validator->validate($this->request[$name]))
        {
            return $default;
        }

        if(empty($this->request[$name]))
        {
            return $default;
        }
        else
        {
            return $this->request[$name];
        }
    }

    /**
     * @param string $name
     * @param bool $secure
     * @param Validator $validator
     *
     * @return bool
     */
    public function has($name, $secure = true, $validator = NULL)
    {
        if($secure && !$this->request_verified)
        {
            return false;
        }

        if(is_object($validator) && get_class($validator) == 'App\\Validator\\Validator' && !$validator->validate($this->request[$name]))
        {
            return false;
        }

        return isset($this->request[$name]);
    }

    /**
     * @param bool $secure
     *
     * @return array
     */
    public function all($secure = true)
    {
        if($secure && !$this->request_verified)
        {
            return [];
        }

        return $this->request;
    }


    /**
     * @return bool
     */
    public function request_verified()
    {
        return $this->request_verified;
    }

    /**
     * @return string
     */
    public function token()
    {
        return $this->cookie()->get($this->session_name);
    }

    public function timezoneOffset()
    {
        if(!$this->cookie->has($this->config->getTimezoneOffsetCookieName()))
        {
            return 0;
        }

        $offset = $this->cookie->get($this->config->getTimezoneOffsetCookieName());

        if(!is_numeric($offset) || $offset != intval($offset))
        {
            return 0;
        }

        if(abs($offset) > 12 * 3600)
        {
            return 0;
        }

        return intval($offset);
    }


    public function ignoreUserAbort()
    {
        ignore_user_abort();
    }

    /**
     * @return Request
     */
    public static function createFromGlobals()
    {
        $config = Factory::getConfig();

        return new Request(array_merge($_POST, $_FILES, $_GET), $_SERVER, $_SESSION, $_COOKIE, $config);
    }
}