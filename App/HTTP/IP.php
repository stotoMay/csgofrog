<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\HTTP;

use App\Storage\MCache;

class IP
{
    private $mcache;
    private $request;

    private $data;

    public function __construct($folder, Request $request)
    {
        $this->mcache = new MCache($folder);
        $this->request = $request;

        if($this->mcache->is_cached($this->request->ip()))
        {
            $this->data = $this->mcache->retrieve($this->request->ip());
        }

        if(!is_array($this->data))
        {
            $this->data = [];
        }
    }

    private function save()
    {
        if(isset($this->data))
        {
            $this->mcache->store($this->request->ip(), $this->data, $this->request->time() + 3600 * 24);
        }
    }


    public function pull($key, $default = NULL)
    {
        $value = $this->get($key, $default);
        $this->forget($key);

        return $value;
    }

    public function put($key, $value)
    {
        if(!is_array($key))
        {
            $key = [$key];
        }

        array_set($this->data, $key, $value);

        $this->save();
    }

    public function get($key, $default = NULL)
    {
        if(!is_array($key))
        {
            $key = [$key];
        }

        $value = array_get($this->data, $key);

        if(empty($value))
        {
            return $default;
        }
        else
        {
            return $value;
        }
    }

    public function forget($key)
    {
        if(!is_array($key))
        {
            $key = [$key];
        }

        array_unset($this->data, $key);

        $this->save();
    }
}