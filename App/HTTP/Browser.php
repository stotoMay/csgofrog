<?php
/**
 * @author Fabian Emilius <fabian.emilius@gmail.com>
 * @copyright 2014-2018 Fabian Emilius
 */

namespace App\HTTP;

use App\Core\App;
use App\User\User;

class Browser
{
    private $request;
    private $app;

    private $browserID;
    /**
     * @var User
     */
    private $user;

    private $cookie_message;
    private $is_bot;
    private $language;

    public function __construct(Request $request, App $app)
    {
        $this->request = $request;
        $this->app = $app;

        $this->is_bot = false;
        $this->cookie_message = false;
        $this->language = $this->app->setting()->get("selected", "controller", "admin_lang");

        if($this->request->cookie()->has($this->app->config()->getLanguageCookieName()))
        {
            $this->setLanguage($this->request->cookie()->get($this->app->config()->getLanguageCookieName()));
        }
    }

    public function init()
    {
        if(is_numeric($this->browserID))
        {
            return;
        }

        if(is_int(strpos($this->request->useragent(), "http")))
        {
            $this->browserID = 0;
            $this->cookie_message = false;
            $this->is_bot = true;

            return;
        }

        $browserHash = $this->request->cookie()->get($this->app->config()->getBrowserCookieName());
        $this->cookie_message = true;

        if(!empty($browserHash))
        {
            if($this->request->cookie()->has($this->app->config()->getLoginCookieName()))
            {
                $browserArr = $this->app->database()->select("browser")->column([
                    "browser.id" => "bID", "session.userID" => "uID", "browser.cookie" => "cookie"
                ])->where(["browser.hash" => sha1($browserHash)])->leftJoin("session", [
                    "session.browserID[=]browser.id",
                    "session.hash" => sha1($this->request->cookie()->get($this->app->config()->getLoginCookieName())),
                    "OR" => ["session.expire" => NULL, "session.expire[>=]" => $this->request->time()]
                ])->fetchRow();

                $this->browserID = $browserArr["bID"];
                if(is_numeric($browserArr["uID"]))
                {
                    $this->user = new User($browserArr["uID"]);

                    if($this->user->isBanned())
                    {
                        $this->destroyLogin();
                        $this->user = NULL;
                    }
                }

                if($browserArr["cookie"] == 1)
                {
                    $this->cookie_message = false;
                }
            }
            else
            {
                $browserArr = $this->app->database()->select("browser")->where(["hash" => sha1($browserHash)])
                    ->fetchRow();

                $this->browserID = $browserArr["id"];

                if($browserArr["cookie"] == 1)
                {
                    $this->cookie_message = false;
                }
            }

            if(isset($this->browserID))
            {
                $this->app->database()->update("browser")->set([
                    "ip" => $this->request->ip(), "useragent" => $this->request->useragent(),
                    "lastaction" => $this->request->time()
                ])->where(["id" => $this->browserID]);
            }
        }

        if(empty($this->browserID))
        {
            $browserHash = str_token();

            $this->request->cookie()->put($this->app->config()
                ->getBrowserCookieName(), $browserHash, ($this->request->time() + 3600 * 24 * 365 * 5), true);

            $this->browserID = $this->app->database()->insert("browser")->set([
                "hash" => sha1($browserHash), "ip" => $this->request->ip(),
                "useragent" => $this->request->useragent(), "cookie" => 0,
                "lastaction" => $this->request->time(), "dateline" => $this->request->time()
            ])->getID();
        }
    }

    /**
     * @return int
     */
    public function getBrowserID()
    {
        $this->init();

        return $this->browserID;
    }

    /**
     * @return int
     */
    public function getUserID()
    {
        $this->init();

        return $this->user->getUserID();
    }

    /**
     * @return User
     */
    public function user()
    {
        $this->init();

        return $this->user;
    }

    /**
     * @param int $userID
     * @param int $expire
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function createLogin($userID, $expire = NULL)
    {
        $this->init();
        $this->destroyLogin();

        if(!isset($expire))
        {
            $expire = $this->request->time() + 3600 * 24;
        }

        if(is_numeric($userID) && is_numeric($expire))
        {
            $token = str_token();
            $this->request->cookie()->put($this->app->config()->getLoginCookieName(), $token, $expire, true);

            $this->app->database()->insert("session")->set([
                "userID" => $userID, "browserID" => $this->browserID, "hash" => sha1($token),
                "expire" => $expire, "created" => $this->request->time()
            ]);

            $this->user = new User($userID);
        }
    }

    /**
     * @return bool
     * @throws \App\Database\Exception\ParseException
     */
    public function destroyLogin()
    {
        $this->init();

        $this->request->cookie()->forget($this->app->config()->getLoginCookieName());

        $this->app->database()->update("session")->set(["expire" => $this->request->time() - 1])
            ->where(["browserID" => $this->browserID, "expire[>=]" => $this->request->time()]);

        return true;
    }

    /**
     * @param string $language
     *
     * @return bool
     * @throws \App\Core\Exception\FileNotFoundException
     * @throws \App\Core\Exception\InvalidArgumentException
     * @throws \App\Language\Exception\ParseException
     */
    public function setLanguage($language)
    {
        if($this->app->setting()->get("browser_language", "application", "general") != 1)
        {
            return false;
        }

        if(!is_string($language) || is_int(strpos($language, "/")))
        {
            return false;
        }

        $defaultfolder = $this->app->config()->getLanguageDirectory().$language."/";

        if(!is_readable(base_dir($defaultfolder)))
        {
            return false;
        }

        $this->language = $language;
        $this->app->language()->loopFolder(base_dir($defaultfolder));

        $componentList = $this->app->component()->getComponentList();
        foreach($componentList as $component)
        {
            $file = $this->app->component()->getLanguageFile($component, $language);

            if(!file_exists(base_dir($file)))
            {
                continue;
            }

            $this->app->language()->mergeFile(base_dir($file));
        }

        return true;
    }

    /**
     * @param int $userID
     *
     * @throws \App\Database\Exception\ParseException
     */
    public function clearSessions($userID = NULL)
    {
        $this->init();

        if(!is_numeric($userID))
        {
            $userID = $this->getUserID();
        }

        if(is_numeric($userID))
        {
            $this->app->database()->update("session")->set(["expire" => $this->request->time() - 1])
                ->where(["userID" => $userID, "browserID[!]" => $this->getBrowserID()]);
        }
    }

    /**
     * @return bool
     */
    public function isLogin()
    {
        $this->init();

        return is_object($this->user);
    }

    /**
     * @return bool
     */
    public function isCookieMessageShown()
    {
        $this->init();

        return $this->cookie_message;
    }

    /**
     * @return bool
     */
    public function isBot()
    {
        $this->init();

        return $this->is_bot;
    }

    /**
     * @return string
     * @throws \App\Database\Exception\ParseException
     */
    public function captcha()
    {
        $this->init();

        if(isset($this->captcha))
        {
            return $this->captcha;
        }

        $this->captcha = str_token();

        $this->app->database()->insert("captcha")->set([
            "hash" => sha1($this->captcha), "imagestring" => str_random(4),
            "browserID" => $this->getBrowserID(), "expire" => time() + 3600 * 24,
            "used" => 0
        ]);

        return $this->captcha;
    }
}